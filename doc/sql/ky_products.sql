USE [ky]
GO

/****** Object:  Table [dbo].[ky_products]    Script Date: 2021/6/28 18:08:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ky_products](
	[pd_id] [int] NOT NULL,
	[add_time] [datetime] NOT NULL,
 CONSTRAINT [PK_ky_products] PRIMARY KEY CLUSTERED 
(
	[pd_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

