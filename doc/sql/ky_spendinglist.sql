USE [ky]
GO

/****** Object:  Table [dbo].[ky_spendinglist]    Script Date: 2021/6/28 18:07:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ky_spendinglist](
	[sl_id] [int] IDENTITY(1,1) NOT NULL,
	[p_id] [int] NOT NULL,
 CONSTRAINT [PK_ky_spendinglist] PRIMARY KEY CLUSTERED 
(
	[sl_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


