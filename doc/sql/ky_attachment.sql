USE [ky]
GO

/****** Object:  Table [dbo].[ky_attachment]    Script Date: 2021/6/28 18:09:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ky_attachment](
	[am_id] [int] NOT NULL,
	[file_name] [varchar](100) NOT NULL,
	[file_path] [varchar](100) NOT NULL,
	[p_id] [int] NOT NULL,
	[sort] [int] NULL,
	[add_time] [datetime] NOT NULL,
	[status] [int] NULL,
 CONSTRAINT [PK_ky_attachment] PRIMARY KEY CLUSTERED 
(
	[am_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

