USE [ky]
GO

/****** Object:  Table [dbo].[ky_projects]    Script Date: 2021/6/28 18:08:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ky_projects](
	[p_id] [int] IDENTITY(1,1) NOT NULL,
	[project_number] [varchar](100) NULL,
	[project_title] [varchar](300) NOT NULL,
	[dptid] [int] NOT NULL,
	[owner] [varchar](50) NOT NULL,
	[tel] [varchar](50) NULL,
	[start_time] [varchar](50) NULL,
	[end_time] [varchar](50) NULL,
	[project_type] [int] NULL,
	[add_time] [datetime] NOT NULL,
	[status] [int] NOT NULL,
	[fee_xiada] [varchar](50) NULL,
	[fee_peitao] [varchar](50) NULL,
	[fee_zongji] [varchar](50) NULL,
	[remark] [varchar](500) NULL,
 CONSTRAINT [PK_projects] PRIMARY KEY CLUSTERED 
(
	[p_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ky_projects] ADD  CONSTRAINT [DF_projects_status]  DEFAULT ((0)) FOR [status]
GO

