﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="DTcms.Web.vote.index" %>

<!DOCTYPE html>
<!--HTML5 doctype-->
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <title>问卷调研</title>
    <meta name="keywords" content="问卷调研" />
    <meta name="description" content="问卷调研" />
    <link rel="stylesheet" type="text/css" href="/css/pagination.css" />
    <link rel="stylesheet" type="text/css" href="/templates/mobile/css/style.css" />
    <script type="text/javascript" charset="utf-8" src="/templates/mobile/js/zepto.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="/templates/mobile/js/weui.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="/templates/mobile/js/common.js"></script>
</head>

<body>
    <div class="page">
        <!--页面头部-->
        <div class="header">
            <a class="back" href="javascript:history.back();">
                <i class="iconfont icon-arrow-left"></i>
            </a>
            <h3>问卷调研</h3>
            <div class="right">
                <a class="weui-cell_access weui-cell_link" onclick="showDialogBox('#category-box');">
                    <i class="iconfont icon-nav"></i>
                </a>
            </div>
        </div>
        <!--/页面头部-->

        <!--页面内容-->
        <div class="page__bd">
            <!--资讯列表-->
            <div class="weui-panel weui-panel_access" style="margin-top: 10px;">
                <div class="weui-panel__hd" style="font-weight: bold; color: black; font-size: initial;">关于基因检测咨询投票</div>
                <div class="weui-panel__bd">
                    <div class="txt-list">
                        <ul>
                            <!--取得一个分页DataTable-->

                            <li>
                                <a href="list.aspx?vid=1">

                                    <h2>关于基因检测咨询投票</h2>
                                    <div class="note">
                                        <p>开始时间：2020年1月1日 结束时间：2020年1月31日</p>
                                        <p>
                                            <i class="hot">
                                                <script type="text/javascript" src="{config.webpath}tools/submit_ajax.ashx?action=view_article_click&channel_id={dr[channel_id]}&id={dr[id]}"></script>
                                                次</i>
                                            <i class="date">发布日期：2020年1月5日</i>
                                        </p>
                                    </div>
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
            <!--/资讯列表-->

            <!--分页页码-->
            <div class="page-list">
                
                <div class='weui-cell__hd'>
                    <input type='radio' class='weui-check' name='q1' id='q1_3' value='3' />
                    <span class='weui-icon-checked'></span>
                </div>
                <div class='weui-cell__bd'>
                    一般
                </div>
            </div>
            <!--/分页页码-->

            <!--版权信息-->

            <!--/版权信息-->

            <!--底部导航-->

            <!--/底部导航-->
        </div>
        <!--/页面内容-->
    </div>

    <!--类别容器-->

    <!--类别容器-->

</body>
