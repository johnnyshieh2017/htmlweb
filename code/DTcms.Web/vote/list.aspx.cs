﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DTcms.Common;
using Newtonsoft.Json;

namespace DTcms.Web.vote
{
    public partial class list : Page
    {
        protected string vid = "";
        protected DTcms.Model.vote vote_model = new Model.vote();
        DTcms.BLL.vote vote_bll = new BLL.vote();
        DTcms.BLL.vote_subject subject_bll = new BLL.vote_subject();
        DTcms.BLL.vote_item item_bll = new BLL.vote_item();
        DTcms.BLL.vote_item_log itemlog_bll = new BLL.vote_item_log();
        protected string jsonstr = "[]";
        protected void Page_Load(object sender, EventArgs e)
        {
            vid = DTRequest.GetQueryInt("vid").ToString();
            if (!IsPostBack)
            {
                vote_model = vote_bll.GetModel(DTRequest.GetQueryInt("vid"));


                BindList();

                MakeClient();


                if (vote_model.status != 1)
                {
                    this.formSubmitBtn.Disabled = true;
                    this.formSubmitBtn.InnerText = "问卷已关闭";
                }
            }
        }

        void BindList()
        {
            var list = subject_bll.GetModelList( " vote_id="+ vid, " sort_id asc");
            rptList.DataSource = list;
            rptList.DataBind();

            jsonstr = JsonConvert.SerializeObject(list);
        }


        void MakeClient()
        {
            HttpCookie cookie = Request.Cookies["openId"];

            if (null == cookie)
            {
                string openId = Guid.NewGuid().ToString("N");
                Session["openId"] = openId;
                cookie = new HttpCookie("openId");
                cookie.Values["openId"] = openId;
                cookie.Expires = DateTime.Now.AddDays(7);
                Response.Cookies.Add(cookie);
            }
            else
            {

                cookie.Expires = DateTime.Now.AddDays(7);
                Session["openId"] = cookie.Values["openId"];

                if (itemlog_bll.GetRecordCount(" openid='" + cookie.Values["openId"] + "' and vote_id=" + vid) > 0)
                {
                    this.formSubmitBtn.Disabled = true;
                    this.formSubmitBtn.InnerText = "问卷已投！";
                }
            }
        }

        protected string GetItems(string subject_id,string type,string subjecttitle)
        {
            string strResult = "";
            var list= item_bll.GetModelList(" vote_subject_id="+ subject_id, " sort_id asc");

            if (type == "1")
            {
                strResult += "<div class='weui-cells weui-cells_radio'>";
            }
            else if (type == "2")
            {
                strResult += "<div class='weui-cells weui-cells_checkbox'>";
            }
            else if (type == "3")
            {
                strResult += "<div class='weui-cell weui-cells_radio'>";
                strResult += "<div class='weui-cell__bd'>";
                strResult += "<textarea class=\"weui-textarea\" id='q"+ subject_id + "' maxlength=\"200\" placeholder=\"请填写\" rows=\"3\"></textarea>";

                strResult += "</div>";
            }

            foreach (var item in list)
            {
                if (type == "1")
                {
                    strResult += "<label class='weui-cell weui-check__label' for='q" + subject_id + "_" + item.id + "'>";
                    strResult += "<div class='weui-cell__hd'>";
                    strResult += "<input type='radio' class='weui-check' name='q" + subject_id + "' id='q" + subject_id + "_" + item.id + "' value='" + item.id + "' required tips='1、" + subjecttitle + ",请勾选1个' />";
                    strResult += "<span class='weui-icon-checked'></span>";
                    strResult += "</div>";
                    strResult += "<div class='weui-cell__bd'>";
                    strResult += item.title;
                    strResult += "</div>";
                    strResult += "</label>";
                }
                else if (type == "2")
                {
                    strResult += "<label class='weui-cell weui-check__label' for='q" + subject_id + "_" + item.id + "'>";
                    strResult += "<div class='weui-cell__hd'>";
                    strResult += "<input type='checkbox' class='weui-check' name='q" + subject_id + "' id='q" + subject_id + "_" + item.id + "' value='" + item.id + "' required tips='1、" + subjecttitle + ",请勾选1个' />";
                    strResult += "<span class='weui-icon-checked'></span>";
                    strResult += "</div>";
                    strResult += "<div class='weui-cell__bd'>";
                    strResult += item.title;
                    strResult += "</div>";
                    strResult += "</label>";
                }
                else
                {
                  
                    strResult += "<div class='weui - cell__bd'>";
                    strResult += "<textarea class=\"weui - textarea\" maxlength=\"200\" placeholder=\"请填写\" rows=\"3\"></textarea>";
                  
                    strResult += "</div>";
                }
            }

            strResult += "</div>";

            return strResult;
        }
    }
}