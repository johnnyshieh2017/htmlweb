﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="list.aspx.cs" Inherits="DTcms.Web.vote.list" %>

<!DOCTYPE html>
<!--HTML5 doctype-->
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <title>问卷调研</title>
    <meta name="keywords" content="问卷调研" />
    <meta name="description" content="问卷调研" />
    <link rel="stylesheet" type="text/css" href="/css/pagination.css" />
    <link rel="stylesheet" type="text/css" href="/templates/mobile/css/style.css" />
    <link href="questionnaire.css" rel="stylesheet" />
    <script type="text/javascript" charset="utf-8" src="/templates/mobile/js/zepto.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="/templates/mobile/js/weui.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="/templates/mobile/js/common.js"></script>
    <script type="text/javascript">
        var jsons = <%=jsonstr%>;

        function check()
        {
            
            var errorstr="";
           

            for (i = 0; i < jsons.length; i++) { 
                console.log(jsons[i].title);
                if(jsons[i].type=="1")
                {
                    //$(":radio[name='"+jsons[i].id+"']").each(function(){
                        
                    //});
                    //console.log($(":input[name='q"+jsons[i].id+"']:checked").val());
                    console.log($("[name='q"+jsons[i].id+"']:checked").val());

                    var rr = $("[name='q"+jsons[i].id+"']:checked").val();
                    if(rr==undefined)
                    {
                        //alert('请选择：'+jsons[i].title);
                        errorstr+='请选择：'+jsons[i].title+"</br>";
                       
                    }
                }
                else if(jsons[i].type=="2")
                {
                    console.log("checkbox:"+$("[name='q"+jsons[i].id+"']:checked").val());
                    var cbxrr=$("[name='q"+jsons[i].id+"']:checked").val();
                    if(cbxrr==undefined)
                    {
                        errorstr+='请选择：'+jsons[i].title+"</br>";
                    }
                }
                else
                {
                    console.log("textarea:"+$("#q"+jsons[i].id+"").val());
                    var textrr=$("#q"+jsons[i].id+"").val();
                    if(textrr=="")
                    {
                        errorstr+='请填写：'+jsons[i].title+"</br>";
                    }
                }

            }

            if(errorstr!="")
            {
                var s = {title:errorstr};
               
                weui.dialog({
                    title:'提示',
                    content:errorstr,
                    buttons: [{
                        label: '确定',
                        type: 'primary',
                        onClick: function () { }
                    }]
                });

                return false ;
            }
            else
            {
                return true;
            }
        }

        function save_result()
        {
            
            if(check())
            {
                var json_result=[];
                //alert('true');
                for (i = 0; i < jsons.length; i++) 
                {
                   

                    if(jsons[i].type=="1")
                    {
                        var rr = $("[name='q"+jsons[i].id+"']:checked")
                        console.log("单选型");
                        console.log(rr);
                        rr.each(function(i,item){

                            var row = {};
                            console.log(item.value);
                            row.type=jsons[i].type;
                            row.qid=jsons[i].id;
                            row.itemid=item.value;
                            row.itemvalue=item.value;
                            json_result.push(row);

                        });
                        
                    }
                    else if(jsons[i].type=="2")
                    {
                        var cbxrr=$("[name='q"+jsons[i].id+"']:checked");
                        console.log("多选型");
                        console.log(cbxrr);
                        cbxrr.each(function(i,item){

                            var row = {};
                            console.log(item.value);
                            row.type=jsons[i].type;
                            row.qid=jsons[i].id;
                            row.itemid=item.value;
                            row.itemvalue=item.value;
                            json_result.push(row);
                        });

                    }
                    else
                    {
                        var row = {};
                        var textrr=$("#q"+jsons[i].id+"").val();
                        console.log("问答型_值");
                        console.log(textrr);
                        row.type=jsons[i].type;
                        row.qid=jsons[i].id;
                        row.itemid=jsons[i].id;
                        row.itemvalue=textrr;

                        json_result.push(row);
                    }

                    
                }

                console.log(json_result);

                $.ajax({
                    url: 'vote.ashx',  //上传的地址
                    type: "post",
                    data: { "vote_id":"<%=vote_model.id%>","json_result": JSON.stringify(json_result)},
                    beforeSend:function(){
                        $("#formSubmitBtn").attr({ disabled: "disabled" });
                    },
                    success: function (data) {
                        console.log(JSON.parse(data));
                       
                        var p = JSON.parse(data);
                        //mui.alert('欢迎使用Hello MUI', '提示', function () {
                        //    location.href
                        //});
                        if(p.ResultCode==1)
                        {
                            weui.dialog({
                                title:'提示',
                                content:'提交成功！',
                                buttons: [{
                                    label: '确定',
                                    type: 'primary',
                                    onClick: function () { 
                                        location.href='list.aspx?vid=<%=vote_model.id%>';
                                    }
                                }]
                            });
                        }
                        else
                        {
                             weui.dialog({
                                title:'提示',
                                content:'系统异常！请稍后再试。',
                                buttons: [{
                                    label: '确定',
                                    type: 'primary',
                                    onClick: function () { 
                                        location.href='list.aspx?vid=<%=vote_model.id%>';
                                    }
                                }]
                            });
                        }
                    },
                    complete: function () {
                        $("#formSubmitBtn").removeAttr("disabled");
                    },
                    error: function (err) {
                        console.log(err)
                    }

                })

            }
            else
            {
                //alert('false');
            }
        }
    </script>
</head>

<body>
    <div class="page">
        <!--页面头部-->
        <div class="header">
            <a class="back" href="javascript:history.back();">
                <i class="iconfont icon-arrow-left"></i>
            </a>
            <h3>问卷调研</h3>
            <div class="right">
                <a class="weui-cell_access weui-cell_link" onclick="showDialogBox('#category-box');">
                    <i class="iconfont icon-nav"></i>
                </a>
            </div>
        </div>
        <!--/页面头部-->

        <!--页面内容-->
        <div class="page__bd">
            <!--资讯列表-->
            <div class="weui-panel weui-panel_access" style="margin-top: 10px;">
                <div class="weui-panel__hd" style="font-weight: bold; color: black; font-size: initial;text-align:center"><%=vote_model.title %><%=vote_model.status!=1?"<font color='red'>[已关闭]</font>":"" %></div>
                <div class="weui-panel__bd">
                    <div class="txt-list">
                        <ul>
                            <!--取得一个分页DataTable-->

                            <li>
                                <a href="">

                                    <h2 style="white-space:normal"><%=vote_model.content %></h2>
                                    <div class="note">
                                        <p>开始时间：<%=vote_model.start_time.ToString("yyyy-MM-dd") %> 结束时间：<%=vote_model.end_time.ToString("yyyy-MM-dd") %></p>
                                       <%-- <p>
                                            <i class="hot"></i>
                                            <i class="date"></i>
                                        </p>--%>
                                    </div>
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
            <!--/资讯列表-->

  <%--          <div class="weui-cells__title">1、你对《流浪地球》这部电影感觉怎么样?</div>
            <div class='weui-cells weui-cells_radio'>
                <label class='weui-cell weui-check__label' for='q1_1'>
                    <div class='weui-cell__hd'>
                        <input type='radio' class='weui-check' name='q1' id='q1_1' value='1' required tips='1、你对《流浪地球》这部电影感觉怎么样?,请勾选1个' />
                        <span class='weui-icon-checked'></span>
                    </div>
                    <div class='weui-cell__bd'>
                        很满意
                    </div>
                </label>
                <label class='weui-cell weui-check__label' for='q1_2'>
                    <div class='weui-cell__hd'>
                        <input type='radio' class='weui-check' name='q1' id='q1_2' value='2' checked />
                        <span class='weui-icon-checked'></span>
                    </div>
                    <div class='weui-cell__bd'>
                        满意
                    </div>
                </label>
                <label class='weui-cell weui-check__label' for='q1_3'>
                    <div class='weui-cell__hd'>
                        <input type='radio' class='weui-check' name='q1' id='q1_3' value='3' />
                        <span class='weui-icon-checked'></span>
                    </div>
                    <div class='weui-cell__bd'>
                        一般
                    </div>
                </label>
                <label class='weui-cell weui-check__label' for='q1_4'>
                    <div class='weui-cell__hd'>
                        <input type='radio' class='weui-check' name='q1' id='q1_4' value='4' />
                        <span class='weui-icon-checked'></span>
                    </div>
                    <div class='weui-cell__bd'>
                        不满意
                    </div>
                </label>
                <label class='weui-cell weui-check__label' for='q1_5'>
                    <div class='weui-cell__hd'>
                        <input type='radio' class='weui-check' name='q1' id='q1_5' value='5' />
                        <span class='weui-icon-checked'></span>
                    </div>
                    <div class='weui-cell__bd'>
                        很不满意
                    </div>
                </label>

            </div>

            <div class="weui-cells__title">2、你最喜欢的明星是?</div>
            <div class='weui-cells weui-cells_checkbox'>
                <label class='weui-cell weui-check__label' for='q2_1'>
                    <div class='weui-cell__hd'>
                        <input type='checkbox' class='weui-check' name='q2' value='1' id='q2_1' required pattern='{1,}' tips='2、你的兴趣爱好是?,请之少勾选1个' />
                        <i class='weui-icon-checked'></i>
                    </div>
                    <div class='weui-cell__bd'>
                        看书
                    </div>
                </label>
                <label class='weui-cell weui-check__label' for='q2_2'>
                    <div class='weui-cell__hd'>
                        <input type='checkbox' class='weui-check' name='q2' value='2' id='q2_2' />
                        <i class='weui-icon-checked'></i>
                    </div>
                    <div class='weui-cell__bd'>
                        看电影
                    </div>
                </label>
                <label class='weui-cell weui-check__label' for='q2_3'>
                    <div class='weui-cell__hd'>
                        <input type='checkbox' class='weui-check' name='q2' value='3' id='q2_3' />
                        <i class='weui-icon-checked'></i>
                    </div>
                    <div class='weui-cell__bd'>
                        旅游
                    </div>
                </label>
                <label class='weui-cell weui-check__label' for='q2_4'>
                    <div class='weui-cell__hd'>
                        <input type='checkbox' class='weui-check' name='q2' value='4' id='q2_4' />
                        <i class='weui-icon-checked'></i>
                    </div>
                    <div class='weui-cell__bd'>
                        跑步
                    </div>
                </label>
                <label class='weui-cell weui-check__label' for='q2_5'>
                    <div class='weui-cell__hd'>
                        <input type='checkbox' class='weui-check' name='q2' value='5' id='q2_5' />
                        <i class='weui-icon-checked'></i>
                    </div>
                    <div class='weui-cell__bd'>
                        打玩戏
                    </div>
                </label>

            </div>

            <div class="weui-cells__title">3、你的其他意见?</div>
            <div class='weui-cell weui-cells_radio'>
                <div class="weui-cell__bd">
                    <textarea class="weui-textarea" maxlength="200" placeholder="请描述其他意见" rows="3"></textarea>

                </div>

            </div>--%>

            <asp:Repeater ID="rptList" runat="server">
                <ItemTemplate>
                    <div class="weui-cells__title"><%#Eval("sort_id") %>、<%#Eval("title") %></div>
                    <%#GetItems(Eval("id").ToString(),Eval("type").ToString(),Eval("title").ToString()) %>
                </ItemTemplate>
            </asp:Repeater>

            <div class="weui-btn-area">
                <button id="formSubmitBtn" runat="server" class="weui-btn weui-btn_primary" style="width:50%" onclick="save_result();">提交</button>
            </div>

            <!--分页页码-->
            <div class="page-list">
            </div>
            <!--/分页页码-->

            <!--版权信息-->

            <!--/版权信息-->

            <!--底部导航-->

            <!--/底部导航-->
        </div>
        <!--/页面内容-->
    </div>

    <!--类别容器-->

    <!--类别容器-->

</body>
