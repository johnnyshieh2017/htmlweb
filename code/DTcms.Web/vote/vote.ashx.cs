﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.SessionState;

namespace DTcms.Web.vote
{
    /// <summary>
    /// vote 的摘要说明
    /// </summary>
    public class vote : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {

            //Thread.Sleep(15000);


            JsonResult<int> res = new JsonResult<int>();

            res.JsonEntity = null;
            res.ResultCode = 0;
            res.ResultMessage = "";

            if (context.Session["openId"] == null)
            {
                res.ResultCode = -1;
                res.ResultMessage = "openId无效";
               


            }
            else
            {
               

                try
                {
                    DTcms.BLL.vote_item item_bll = new BLL.vote_item();
                    DTcms.BLL.vote_item_log itemlog_bll = new BLL.vote_item_log();

                    string openid = context.Session["openId"].ToString();
                    string ip = context.Request.UserHostAddress;
                    string jsonstr = context.Request.Form["json_result"];
                    string vote_id = context.Request.Form["vote_id"];
                    DateTime addtime = System.DateTime.Now;
                    var list = JsonConvert.DeserializeObject<List<voteLog>>(jsonstr);
                    foreach (var item in list)
                    {
                        DTcms.Model.vote_item_log model = new Model.vote_item_log();
                        model.vote_id = int.Parse(vote_id);
                        model.vote_subject_id = item.qid;
                        model.vote_item_id = item.itemid;
                        model.user_id = 0;
                        model.openid = openid;
                        model.ip = ip;
                        model.add_time = addtime;
                        model.vote_item_value = item.itemvalue;
                        itemlog_bll.Add(model);
                        if (item.Type == 1|| item.Type==2)
                        {//单选或多选
                            var itemmodel = item_bll.GetModel(item.itemid);
                            itemmodel.num = itemmodel.num + 1;
                            item_bll.Update(itemmodel);
                        }
                      
                    }

                    res.ResultCode = 1;
                    res.ResultMessage = "成功!";

                }
                catch (Exception ex)
                {
                    res.ResultCode = -2;
                    res.ResultMessage = ex.Message;
                }
                

            }


            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }

    public class voteLog
    {
        public int Type {
            get;set;
        }
        public int qid {
            get;set;
        }

        public int itemid {
            get;set;
        }
        public string itemvalue {
            get;set;
        }
    }

    public class JsonResult<T>
    {

        public List<T> JsonEntity { get; set; }

        /// <summary>
        /// 无错误 1 面向用户的错误信息 0
        /// </summary>
        public int ResultCode { get; set; }

        /// <summary>
        /// 错误信息
        /// </summary>
        public string ResultMessage { get; set; }

    }


}