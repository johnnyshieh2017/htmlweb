﻿using DTcms.API.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DTcms.Web
{
    public partial class form1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["oauth_access_token"] != null)
            {
                Dictionary<string, object> dic = weixin_helper.get_info(Session["oauth_access_token"].ToString(), Session["oauth_openid"].ToString());
                if (dic == null)
                {
                    Response.Write("出错了！");
                    return;
                }
                else
                {
                    this.txtName.Value = dic["nickname"].ToString();
                    this.txtid.Value = dic["openid"].ToString();
                }
            }
        }
    }
}