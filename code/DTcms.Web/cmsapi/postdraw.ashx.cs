﻿
using DTcms.Common;
using DTcms.Web.cmsapi.Models;
using DTcms.Web.UI;
using Newtonsoft.Json;
using NLog;
using qcloudsms_csharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace DTcms.Web.cmsapi
{
    /// <summary>
    /// postdraw 的摘要说明
    /// </summary>
    public class postdraw : IHttpHandler, IRequiresSessionState
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public void ProcessRequest(HttpContext context)
        {

            //取得处事类型
            string action = DTRequest.GetQueryString("action");


            switch (action)
            {
                case "user_verify_mobile": // 根据手机号码 返回 抽奖用户编号
                    user_verify_mobile(context);
                    break;
                case "user_draw":
                    user_draw(context);//抽奖
                    break;
                case "user_order":
                    user_order(context);//领奖
                    break;
                case "user_myorder":
                    user_myorder(context);//我的奖品
                    break;
                case "draw_lottery":
                    draw_lottery(context);//活动信息
                    break;
                case "draw_pond":
                    draw_pond(context);//获得中奖人名单
                    break;
            }

          
        }

        private void user_verify_mobile(HttpContext context)
        {
            BLL.draw_pond pondbll = new BLL.draw_pond();
            BLL.draw_lotteryList lotterybll = new BLL.draw_lotteryList();
            JsonResult<int> res = new JsonResult<int>();

            string mobile = Utils.ToHtml(DTRequest.GetFormString("mobile").Trim());
            int lid = DTRequest.GetFormInt("lid");


            var lottery = lotterybll.GetModel(lid);
            if (lottery != null&& lottery.status==1)
            {
                var pond =  pondbll.GetModelList(" lotteryid="+ lid + " and telephone='"+ mobile + "'");
                List<int> uidlist = new List<int>();
                if (pond != null && pond.Count > 0)
                {
                    uidlist.Add(pond[0].id);
                }
                res.ResultCode = 1;
                res.ResultMessage = "OK";
                res.JsonEntity = uidlist;
            }
            else
            {
                res.ResultCode = 0;
                res.ResultMessage = "抽奖活动已失效";
            }



            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();

        }

        private void user_draw(HttpContext context)
        {
            BLL.draw_pond pondbll = new BLL.draw_pond();
            BLL.draw_lotteryList lotterybll = new BLL.draw_lotteryList();
            BLL.draw_Prize prizebll = new BLL.draw_Prize();
            JsonResult<PondResult> res = new JsonResult<PondResult>();
            List<PondResult> PondResultlist = new List<Models.PondResult>();
            int uid = DTRequest.GetFormInt("uid");
            int lid = DTRequest.GetFormInt("lid");


            var lottery = lotterybll.GetModel(lid);
            if (lottery != null && lottery.status == 1)
            {
                PondResult r = new PondResult();
                var pond = pondbll.GetModelList(" lotteryid=" + lid + " and id=" + uid + "");
                if (pond != null && pond.Count > 0)
                {
                    if (pond[0].status == 2)
                    {//已抽奖
                        r.PrizeID = 0;
                        r.PondName = "";
                        r.PondTitle = "";
                        res.ResultCode = 0;
                        res.ResultMessage = "已抽奖";
                    }
                    else if (pond[0].status == 3)
                    {//已领奖
                        r.PrizeID = 0;
                        r.PondName = "";
                        r.PondTitle = "";   
                        res.ResultCode = 0;
                        res.ResultMessage = "已领奖";
                    }
                    else if (pond[0].status == 1)
                    {//中奖未抽，开始抽
                        r.PrizeID = pond[0].Prizeid;
                        r.PondName = "";
                        r.PondTitle = "";
                        var prize = prizebll.GetModel(r.PrizeID);
                        if (prize != null)
                        {
                            r.PondName = prize.Prize_name;
                            r.PondTitle = prize.Prize_title;
                            r.imgurl = prize.imgurl;
                        }

                        //更新领奖时间
                        pond[0].updatetime = System.DateTime.Now;
                        pond[0].status = 2;//0 未中 、1中奖未领取 2已开奖（点击开奖）  3 已领取（填写领奖表单）
                        pondbll.Update(pond[0]);

                        //PondResultlist.Add(r);
                        // res.JsonEntity = PondResultlist;
                        res.ResultCode = 1;
                        res.ResultMessage = "OK";

                    }
                    else
                    {//参与抽奖未中奖
                     //
                        r.PrizeID = 0;
                        r.PondName = "";
                        r.PondTitle = "";
                        res.ResultCode = 1;
                        res.ResultMessage = "OK";
                    }

                

                }
                else
                {
                    r.PrizeID = 0;
                    r.PondName = "";
                    r.PondTitle = "";
                    res.ResultCode = 1;
                    res.ResultMessage = "OK";
                }

                PondResultlist.Add(r);
                res.JsonEntity = PondResultlist;


            }
            else
            {
                res.ResultCode = 0;
                res.ResultMessage = "抽奖活动已失效";
            }



            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }


        private void user_myorder(HttpContext context)
        {
            int lid = DTRequest.GetFormInt("lid");//活动场次 id
            string mobile = Utils.ToHtml(DTRequest.GetFormString("mobile").Trim());
            BLL.draw_pond pondbll = new BLL.draw_pond();

            JsonResult<PondResult> res = new JsonResult<PondResult>();
            List<PondResult> PondResultlist = new List<Models.PondResult>();

            var pondlist = pondbll.GetModelList(" lotteryid=" + lid + " and telephone='" + mobile + "'");
            if (pondlist != null && pondlist.Count > 0)
            {
                PondResult r = new PondResult();
                BLL.draw_Prize prizebll = new BLL.draw_Prize();
                r.PrizeID = pondlist[0].Prizeid;
                var prize = prizebll.GetModel(r.PrizeID);
                if (prize != null)
                {
                    r.imgurl = prize.imgurl;
                    r.PondName = prize.Prize_name;
                    r.PondTitle = prize.Prize_title;
                    r.PondStatus = pondlist[0].status;//0 未中 、1中奖未领取 2已开奖（点击开奖）  3 已领取（填写领奖表单）

                    PondResultlist.Add(r);
                    res.JsonEntity = PondResultlist;
                    res.ResultCode = 1;
                    res.ResultMessage = "OK";
                }
                else
                {
                    r.PondName = "";
                    r.PondTitle = "";
                    r.PondStatus = 0;

                    PondResultlist.Add(r);
                    res.JsonEntity = PondResultlist;
                    res.ResultCode = 1;
                    res.ResultMessage = "OK";
                }

              
            }
            else
            {
                res.ResultCode = 1;
                res.ResultMessage = "null";
            }
            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }
        
        /// <summary>
        /// 领奖(填写表单)
        /// </summary>
        /// <param name="context"></param>
        private void user_order(HttpContext context)
        {
            // 
            int lid = DTRequest.GetFormInt("lid");//活动场次 id
            int uid = DTRequest.GetFormInt("uid");// 领奖券 id
            string mobile = Utils.ToHtml(DTRequest.GetFormString("mobile").Trim());

            string hospital = Utils.ToHtml(DTRequest.GetFormString("hospital").Trim());
            string department = Utils.ToHtml(DTRequest.GetFormString("department").Trim());
            string truename = Utils.ToHtml(DTRequest.GetFormString("truename").Trim());
            string linkmobile = Utils.ToHtml(DTRequest.GetFormString("linkmobile").Trim());
            string address = Utils.ToHtml(DTRequest.GetFormString("address").Trim());

            BLL.draw_pond pondbll = new BLL.draw_pond();
            JsonResult<int> res = new JsonResult<int>();
            List<PondResult> PondResultlist = new List<Models.PondResult>();

            res.JsonEntity = new List<int> { 0 };
            var pond = pondbll.GetModel(uid);
            if (pond != null && pond.lotteryid == lid)
            {
                if (pond.status == 2)
                {
                    //更新领奖字段
                    //update
                    pond.updatetime = System.DateTime.Now;
                    pond.status = 3;
                    pond.hospital = hospital;
                    pond.department = department;
                    pond.truename = truename;
                    pond.linkmobile = linkmobile;
                    pondbll.Update(pond);

                    res.JsonEntity = new List<int> { 1 };
                    res.ResultCode = 1;
                    res.ResultMessage = "ok";
                }
                else
                {
                    res.ResultCode = 0;
                    res.ResultMessage = "已领奖";
                }
            }
            else
            {
                res.ResultCode = 0;
                res.ResultMessage = "无效奖券";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }

        private void draw_lottery(HttpContext context)
        {
            int lid = DTRequest.GetFormInt("lid");//活动场次 id
            BLL.draw_lotteryList lotterybll = new BLL.draw_lotteryList();
            BLL.draw_Prize prizebll = new BLL.draw_Prize();
            JsonResult<lotteryDetail> res = new JsonResult<lotteryDetail>();
            List<lotteryDetail> ldlist = new List<lotteryDetail>();
            lotteryDetail ld = new lotteryDetail();


            var lottery = lotterybll.GetModel(lid);
            if (lottery != null && lottery.status == 1)
            {
                ld.lottery = lottery;//
                if (lottery.begin_date > System.DateTime.Now)
                {
                    res.ResultCode = 0;
                    res.ResultMessage = "活动未开始";
                }
                else
                {
                    if (lottery.end_date < System.DateTime.Now)
                    {
                        res.ResultCode = 0;
                        res.ResultMessage = "活动已结束";
                    }
                    else
                    {
                       
                        ld.pondlist = prizebll.GetModelList(" lotteryid=" + lid);
                        ldlist.Add(ld);

                        res.JsonEntity = ldlist;
                        res.ResultCode = 1;
                        res.ResultMessage = "OK";
                        context.Response.ContentType = "application/json";
                        context.Response.Write(JsonConvert.SerializeObject(res));
                        context.Response.End();
                    }

                }

              

            }

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }


        private void draw_pond(HttpContext context)
        {
            int lid = DTRequest.GetFormInt("lid");//活动场次 id

            BLL.draw_pond pondbll = new BLL.draw_pond();
            JsonResult<PersonPondResult> res = new JsonResult<PersonPondResult>();
            List<PersonPondResult> PondResultlist = new List<Models.PersonPondResult>();
            var pondlist = pondbll.GetModelList("   lotteryid=" + lid + " and status>1 and Prizeid>0");

            BLL.draw_Prize prizebll = new BLL.draw_Prize();
            foreach (var item in pondlist)
            {
                PersonPondResult r = new PersonPondResult();

                var prize = prizebll.GetModel(item.Prizeid);
                r.PrizeID = item.Prizeid;
                r.PondName = prize.Prize_name;
                r.PondTitle = prize.Prize_title;
                r.PondStatus = item.status;
                r.Mobile = item.telephone;
                r.Uptime = item.updatetime.ToString();
                PondResultlist.Add(r);
            }

            res.JsonEntity = PondResultlist;
            res.ResultCode = 1;
            res.ResultMessage = "OK";

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}