﻿using DTcms.Common;
using DTcms.Model;
using DTcms.Web.cmsapi.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace DTcms.Web.cmsapi
{
    /// <summary>
    /// postcmscomment 的摘要说明
    /// </summary>
    public class postcmscomment : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            JsonResult<int> res = new JsonResult<int>();
            try
            {
                int channel_id = DTRequest.GetFormInt("channel_id");
                int news_id = DTRequest.GetFormInt("news_id");

                string content = DTRequest.GetFormString("content");
                string img_content= DTRequest.GetFormString("img_content");

                int parent_id = DTRequest.GetFormInt("parent_id");
                int user_id = DTRequest.GetFormInt("user_id");

                string user_ip = DTRequest.GetFormString("user_ip");

                if (content.Length > 200 || content.Length < 1)
                {
                    throw new Exception("评论内容格式有误");
                }

                BLL.article_comment bll = new BLL.article_comment();

                Model.article_comment model = new article_comment();
                model.article_id = news_id;
                model.channel_id = channel_id;
                model.content = content;
                model.is_lock = 0;
                model.is_reply = 0;
                model.site_id = 1;
                model.user_id = user_id;
                model.user_ip = user_ip;
                model.add_time = System.DateTime.Now;
                model.parent_id = parent_id;
                model.img_content = img_content;
               var r=  bll.Add(model);
                //if (model.img_content != null)
                //{
                //    DTcms.BLL.comment_albums bllcomm = new BLL.comment_albums();
                //    comment_albums mod = new comment_albums();
                //    mod.article_comment_id = 
                //}
                res.JsonEntity = null;
                res.ResultCode = 1;
                res.ResultCount = r;
                res.ResultMessage = "成功";

              
            }
            catch (Exception ex)
            {
                res.ResultCode = -2;
                res.ResultMessage = ex.Message;
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}