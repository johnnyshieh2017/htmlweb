﻿using DTcms.Common;
using DTcms.Model;
using DTcms.Web.cmsapi.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace DTcms.Web.cmsapi
{
    /// <summary>
    /// cmscomment 的摘要说明
    /// </summary>
    public class cmscomment : IHttpHandler
    {
        DTcms.BLL.comment_albums bllcom = new BLL.comment_albums();
        BLL.article_comment bll = new BLL.article_comment();
        public void ProcessRequest(HttpContext context)
        {
            int channel_id = DTRequest.GetQueryInt("channel_id");
            int news_id = DTRequest.GetQueryInt("news_id");
            int pagesize = DTRequest.GetQueryInt("pagesize");

            if(pagesize==0)
            {
                pagesize = 50;
            }
            int page = DTRequest.GetQueryInt("page");

            //int ordertype = DTRequest.GetQueryInt("ordertype");
            //string action = DTRequest.GetQueryString("action");
            string strWhere = " article_id="+ news_id + " and channel_id="+ channel_id + " and  parent_id=0";
            string filedOrder = " add_time asc";
            JsonResult<DTcms.Model.article_comment_ext> res = new JsonResult<article_comment_ext>();
            int record_count = 0;
            try
            {
                DataSet list = bll.GetList(pagesize, page, strWhere, filedOrder, out record_count);

                if (list != null && list.Tables[0] != null)
                {
                    List<article_comment_ext> listmodel = new List<article_comment_ext>();
                    for (int i = 0; i < list.Tables[0].Rows.Count; i++)
                    {
                        article_comment_ext article_comment = new article_comment_ext();
                        article_comment.id = int.Parse(list.Tables[0].Rows[i]["id"].ToString());
                        article_comment.content = list.Tables[0].Rows[i]["content"].ToString();
                        article_comment.add_time = DateTime.Parse(list.Tables[0].Rows[i]["add_time"].ToString());
                        article_comment.parent_id = int.Parse(list.Tables[0].Rows[i]["parent_id"].ToString());
                        article_comment.user_id = int.Parse(list.Tables[0].Rows[i]["user_id"].ToString());
                        article_comment.article_id = int.Parse(list.Tables[0].Rows[i]["article_id"].ToString());
                        article_comment.channel_id = int.Parse(list.Tables[0].Rows[i]["channel_id"].ToString());
                        article_comment.img_content = list.Tables[0].Rows[i]["img_content"].ToString(); //GetCommentImg(article_comment.id, article_comment.channel_id);
                        article_comment.children = DataTabletoModel(bll.GetList(0, " parent_id="+ article_comment.id, " add_time desc"));
                        listmodel.Add(article_comment);
                    }
                    res.JsonEntity = listmodel;
                    res.ResultCount = listmodel.Count;
                    res.ResultCode = 1;
                    res.ResultMessage = "成功";
                }
                else
                {
                    res.ResultCount = 0;
                    res.ResultCode = 1;
                    res.ResultMessage = "成功";
                }
            }
            catch(Exception EX)
            {
                res.ResultCount = 0;
                res.ResultCode = -2;
                res.ResultMessage = "ERROR:"+ EX.Message;
            }
            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }
        private List<article_comment> DataTabletoModel(DataSet ds)
        {
            List<article_comment> list = new List<article_comment>();
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    article_comment article_comment = new article_comment();
                    article_comment.id = int.Parse(ds.Tables[0].Rows[i]["id"].ToString());
                    article_comment.content = ds.Tables[0].Rows[i]["content"].ToString();
                    article_comment.add_time = DateTime.Parse(ds.Tables[0].Rows[i]["add_time"].ToString());
                    article_comment.parent_id = int.Parse(ds.Tables[0].Rows[i]["parent_id"].ToString());
                    article_comment.user_id = int.Parse(ds.Tables[0].Rows[i]["user_id"].ToString());
                    article_comment.article_id = int.Parse(ds.Tables[0].Rows[i]["article_id"].ToString());
                    article_comment.channel_id = int.Parse(ds.Tables[0].Rows[i]["channel_id"].ToString());
                    article_comment.img_content = ds.Tables[0].Rows[i]["img_content"].ToString();

                    list.Add(article_comment);
                }

            }

            return list;
        }
        private string GetCommentImg(int commentid,int channel_id)
        {
            var list = bllcom.GetModelList(" article_comment_id="+ commentid + " and channel_id="+ channel_id);
            if (list != null && list.Count > 0)
            {
                return list[0].original_path;
            }
            else
            {
                return "";
            }
        }

        public void GetComment(int pagesize,int pageIndex,string strWhere,string filedOrder, HttpContext context)
        {
           
        }
        public void PostComment()
        {

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}