﻿using DTcms.Web.cmsapi.Models;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DTcms.Web.cmsapi
{
    /// <summary>
    /// postcmscommentimg 的摘要说明
    /// </summary>
    public class postcmscommentimg : IHttpHandler
    {
        private Logger logger = LogManager.GetCurrentClassLogger();
        public void ProcessRequest(HttpContext context)
        {
            JsonResult<UploadPath> res = new JsonResult<UploadPath>();
            try
            {
                logger.Info("postcmscommentimg开始");

                HttpFileCollection hfc = context.Request.Files;
            string saveName = System.DateTime.Now.ToString("yyyyMMddHHmmssffff");
            string extention = ".png";

            HttpPostedFile hpf = hfc[0];

            //文件保存目录路径
            String savePath = context.Server.MapPath("/upload/comment/");

            String filePath = savePath + saveName + extention;
           
            List<UploadPath> imglist = new List<UploadPath>();
           
                hpf.SaveAs(filePath);
                res.ResultCode = 1;
                UploadPath info = new UploadPath();
                info.filepath = "/upload/comment/" + saveName + extention;
                imglist.Add(info);
                res.JsonEntity = imglist;

              
               
            }
            catch (Exception ex)
            {

                res.ResultCode = -2;
                res.ResultMessage = ex.Message;
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();

        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }

    public class UploadPath
    {
        public string filepath {
            get;
            set;
        }
    }
}