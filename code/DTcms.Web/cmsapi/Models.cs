﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DTcms.Web.cmsapi.Models
{

    public class lotteryDetail
    {
        public lotteryDetail()
        {
            this.lottery = new Model.draw_lotteryList();
            this.pondlist = new List<Model.draw_Prize>();
        }
        public Model.draw_lotteryList lottery {get;set;}

        public List<Model.draw_Prize> pondlist { get; set; }
    }

    public class PondResult
    {
        public string imgurl { get; set; }
        public int PondStatus { get; set; }

      //  public int PondLevel { get; set; }

        public string PondTitle { get; set; }

        public string PondName { get; set; }

        public int PrizeID { get; set; }

    }
    public class PersonPondResult
    {
        public string Uptime { get; set; }
        public string Mobile { get; set; }
        public int PondStatus { get; set; }

        //  public int PondLevel { get; set; }

        public string PondTitle { get; set; }

        public string PondName { get; set; }

        public int PrizeID { get; set; }

    }

    public class JsonResult<T>
    {

        public List<T> JsonEntity { get; set; }

        /// <summary>
        /// 无错误 1 面向用户的错误信息 0
        /// </summary>
        public int ResultCode { get; set; }

        public int ResultCount { get; set; }
        /// <summary>
        /// 错误信息
        /// </summary>
        public string ResultMessage { get; set; }

        public Dictionary<string,string> ResultExt { get; set; }

    }
    public class channel_article_news
    {
        public channel_article_news()
        {

        }
        private int _id;
        //private int _site_id = 0;
        private int _channel_id = 0;
        private int _category_id = 0;
        private string _title;
        private string _link_url;
        private string _img_url;
        private string _seo_title;
        private string _seo_keywords;
        private string _seo_description;
        private string _tags;
        private string _zhaiyao;
        private string _content;
        private int _sort_id = 99;
        private int _click = 0;
        private int _status = 0;

        private int _is_top = 0;
        private int _is_red = 0;
        private int _is_hot = 0;
        private int _like_count = 0;
        private DateTime _add_time = DateTime.Now;
        private int _comment;

        public int comment
        {
            set { _comment = value; }
            get { return _comment; }
        }
        /// <summary>
        /// 扩展字段字典
        /// </summary>
        private Dictionary<string, string> _fields;
        public Dictionary<string, string> fields
        {
            get { return _fields; }
            set { _fields = value; }
        }

        //  private DateTime? _update_time;
        /// 
        /// </summary>
        public int id
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        //public int site_id
        //{
        //    set { _site_id = value; }
        //    get { return _site_id; }
        //}
        /// <summary>
        /// 
        /// </summary>
        public int channel_id
        {
            set { _channel_id = value; }
            get { return _channel_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int category_id
        {
            set { _category_id = value; }
            get { return _category_id; }
        }
        /// <summary>
		/// 
		/// </summary>
		public string title
        {
            set { _title = value; }
            get { return _title; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string link_url
        {
            set { _link_url = value; }
            get { return _link_url; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string img_url
        {
            set { _img_url = value; }
            get { return _img_url; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string seo_title
        {
            set { _seo_title = value; }
            get { return _seo_title; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string seo_keywords
        {
            set { _seo_keywords = value; }
            get { return _seo_keywords; }
        }
        /// <summary>
        /// 
        ///</summary>
        public string seo_description
        {
            set { _seo_description = value; }
            get { return _seo_description; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string tags
        {
            set { _tags = value; }
            get { return _tags; }
        }
        //      /// <summary>
        ///// 
        ///// </summary>
        public string zhaiyao
        {
            set { _zhaiyao = value; }
            get { return _zhaiyao; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string content
        {
            set { _content = value; }
            get { return _content; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int sort_id
        {
            set { _sort_id = value; }
            get { return _sort_id; }
        }
        //      /// <summary>
        ///// 
        ///// </summary>
        public int click
        {
            set { _click = value; }
            get { return _click; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int status
        {
            set { _status = value; }
            get { return _status; }
        }


        /// <summary>
        /// 是否置顶
        /// </summary>
        public int is_top
        {
            set { _is_top = value; }
            get { return _is_top; }
        }
        /// <summary>
        /// 是否推荐
        /// </summary>
        public int is_red
        {
            set { _is_red = value; }
            get { return _is_red; }
        }
        /// <summary>
        /// 是否热门
        /// </summary>
        public int is_hot
        {
            set { _is_hot = value; }
            get { return _is_hot; }
        }

        /// <summary>
		/// 
		/// </summary>
		public DateTime add_time
        {
            set { _add_time = value; }
            get { return _add_time; }
        }
        /// <summary>
        /// 
        /// </summary>
        //public DateTime? update_time
        //{
        //    set { _update_time = value; }
        //    get { return _update_time; }
        //}
        /// <summary>
        /// 喜欢数量
        /// </summary>
        public int like_count
        {
            set { _like_count = value; }
            get { return _like_count; }
        }

        /// <summary>
        /// 图片相册
        /// </summary>
        private List<Model.article_albums> _albums;
        public List<Model.article_albums> albums
        {
            set { _albums = value; }
            get { return _albums; }
        }

        private List<Model.article_comment_ext> _commentlist;
        public List<Model.article_comment_ext> commentlist
        {
            set { _commentlist = value; }
            get { return _commentlist; }
        }
    }
}