﻿using Aliyun.Acs.Core;
using Aliyun.Acs.Core.Profile;
using Aliyun.Acs.Dysmsapi.Model.V20170525;
using DTcms.Common;
using DTcms.Web.UI;
using NLog;
using qcloudsms_csharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace DTcms.Web.cmsapi
{
    /// <summary>
    /// Handler2 的摘要说明
    /// </summary>
    public class Handler2 : IHttpHandler, IRequiresSessionState
    {
        Model.userconfig userConfig = new BLL.userconfig().loadConfig();
        Model.sysconfig sysConfig = new BLL.sysconfig().loadConfig();
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public void ProcessRequest(HttpContext context)
        {
            //取得处事类型
            string action = DTRequest.GetQueryString("action");


            switch (action)
            {
                case "user_verify_smscode": // 注册时获取手机验证码
                    user_verify_smscode(context);
                    break;
                case "user_register":
                    user_register(context);
                    break;
                case "user_login":
                    user_login(context);
                    break;
                case "user_loginverify_smscode": // 登录时获取手机验证码
                    user_loginverify_smscode(context);
                    break;
            }

        }
        #region 用户注册


        private void user_register(HttpContext context)
        {
            int site_id = 1;// DTRequest.GetQueryInt("site_id"); //当前站点ID
            string code = DTRequest.GetFormString("code").Trim();//已发送的手机验证码
            string username = Utils.ToHtml(DTRequest.GetFormString("username").Trim());
            string password = Utils.Number(6); //DTRequest.GetFormString("txtPassword").Trim();
            string email = Utils.ToHtml(DTRequest.GetFormString("txtEmail").Trim());
            string mobile = Utils.ToHtml(DTRequest.GetFormString("mobile").Trim());
            string userip = DTRequest.GetIP();

            string hospital = Utils.ToHtml(DTRequest.GetFormString("hospital").Trim());
            string department = Utils.ToHtml(DTRequest.GetFormString("department").Trim());
            string truename = Utils.ToHtml(DTRequest.GetFormString("truename").Trim());

            if (string.IsNullOrEmpty(hospital) || string.IsNullOrEmpty(department) || string.IsNullOrEmpty(truename))
            {
                context.Response.Write("{\"status\":0, \"msg\":\"对不起，医院名称、科室、真实姓名不能为空！\"}");
                return;
            }

            //检查是否开启会员功能
            if (sysConfig.memberstatus == 0)
            {
                context.Response.Write("{\"status\":0, \"msg\":\"对不起，会员功能已关闭，无法注册！\"}");
                return;
            }
            if (userConfig.regstatus == 0)
            {
                context.Response.Write("{\"status\":0, \"msg\":\"对不起，系统暂不允许注册新用户！\"}");
                return;
            }
            //检查用户输入信息是否为空
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
            {
                context.Response.Write("{\"status\":0, \"msg\":\"对不起，用户名和密码不能为空！\"}");
                return;
            }
            //如果开启手机注册则要验证手机
            if (userConfig.regstatus == 2 && string.IsNullOrEmpty(mobile))
            {
                context.Response.Write("{\"status\":0, \"msg\":\"错误：手机号码不能为空！\"}");
                return;
            }
            //如果开启邮箱注册则要验证邮箱
            if (userConfig.regstatus == 3 && string.IsNullOrEmpty(email))
            {
                context.Response.Write("{\"status\":0, \"msg\":\"对不起，电子邮箱不能为空！\"}");
                return;
            }
            //检查用户名
            BLL.users bll = new BLL.users();
            if (bll.Exists(username))
            {
                context.Response.Write("{\"status\":0, \"msg\":\"对不起，该用户名已经存在！\"}");
                return;
            }
            //如果开启手机登录要验证手机
            if (userConfig.mobilelogin == 1 && !string.IsNullOrEmpty(mobile))
            {
                if (bll.ExistsMobile(mobile))
                {
                    context.Response.Write("{\"status\":0, \"msg\":\"对不起，该手机号码已被使用！\"}");
                    return;
                }
            }
            //如果开启邮箱登录要验证邮箱
            if (userConfig.emaillogin == 1 && !string.IsNullOrEmpty(email))
            {
                if (bll.ExistsEmail(email))
                {
                    context.Response.Write("{\"status\":0, \"msg\":\"对不起，该电子邮箱已被使用！\"}");
                    return;
                }
            }
            //检查同一IP注册时隔
            if (userConfig.regctrl > 0)
            {
                if (bll.Exists(userip, userConfig.regctrl))
                {
                    context.Response.Write("{\"status\":0, \"msg\":\"对不起，同IP在" + userConfig.regctrl + "小时内禁止重复注册！\"}");
                    return;
                }
            }
            //检查默认组别是否存在
            Model.user_groups modelGroup = new BLL.user_groups().GetDefault();
            if (modelGroup == null)
            {
                context.Response.Write("{\"status\":0, \"msg\":\"用户尚未分组，请联系网站管理员！\"}");
                return;
            }
            string result2 = verify_sms_code(context, code);

            logger.Info(result2);

            if (result2 != "success")
            {
                context.Response.Write(result2);
                return;
            }

            #region 保存用户注册信息
            Model.users model = new Model.users();
            model.site_id = site_id;
            model.group_id = modelGroup.id;
            model.user_name = username;
            model.salt = Utils.GetCheckCode(6); //获得6位的salt加密字符串
            model.password = DESEncrypt.Encrypt(password, model.salt);
            model.email = email;
            model.mobile = mobile;
            model.reg_ip = userip;
            model.reg_time = DateTime.Now;
            model.hospital = hospital;
            model.department = department;
            model.truename = truename;
            //设置用户状态
            if (userConfig.regstatus == 3)
            {
                model.status = 1; //待验证
            }
            else if (userConfig.regverify == 1)
            {
                model.status = 2; //待审核
            }
            else
            {
                model.status = 0; //正常
            }
            #endregion
            //开始写入数据库
            logger.Info("开始写入数据库");

            model.id = bll.Add(model);
            if (model.id < 1)
            {
                context.Response.Write("{\"status\":0, \"msg\":\"系统故障，请联系网站管理员！\"}");
                return;
            }
            else
            {
                //注册成功
                context.Response.Write("{\"status\":1, \"msg\":\"注册成功！\",\"userid\":" + model.id + "}");
                return;
            }
        }
        #endregion

        #region 用户登录OK=====================================
        private void user_login(HttpContext context)
        {
            int site_id = 1;// DTRequest.GetQueryInt("site_id");
            string username = DTRequest.GetFormString("username");
            string password = DTRequest.GetFormString("txtPassword");
            string remember = DTRequest.GetFormString("chkRemember");
            string logincode = DTRequest.GetFormString("code");
            //检查站点是否正确
            //string sitepath = new BLL.sites().GetBuildPath(site_id);
            //if (site_id == 0 || string.IsNullOrEmpty(sitepath))
            //{
            //    context.Response.Write("{\"status\": 0, \"msg\": \"错误提示：站点传输参数不正确！\"}");
            //    return;
            //}
            //检查手机号（用户名），验证码
            if (string.IsNullOrEmpty(logincode) || string.IsNullOrEmpty(username))
            {
                context.Response.Write("{\"status\": 0, \"msg\": \"温馨提示：请输入手机号码和验证码！\"}");
                return;
            }

            if (context.Session[username + "SMS_LOGIN_CODE"] != null)
            {


                if (logincode == context.Session[username + "SMS_LOGIN_CODE"].ToString())
                {//手机验证码正确

                    logger.Info("logincode:" + logincode);

                    BLL.users bll = new BLL.users();
                    var model = bll.GetModel(username);
                    //Model.users model = bll.GetModel(username, password, userConfig.emaillogin, userConfig.mobilelogin, true);
                    if (model == null)
                    {
                        context.Response.Write("{\"status\":0, \"msg\":\"错误提示：手机号码有误，请重试！\"}");
                        return;
                    }
                    //检查用户是否通过验证
                    if (model.status == 1) //待验证
                    {
                        if (userConfig.regverify == 1)
                        {
                            //context.Response.Write("{\"status\":1, \"url\":\""
                            //   + new Web.UI.BasePage().getlink(sitepath, new Web.UI.BasePage().linkurl("register", "?action=sendmail&username=" + Utils.UrlEncode(model.user_name))) + "\", \"msg\":\"会员尚未通过验证！\"}");

                            context.Response.Write("{\"status\":0, \"msg\":\"错误提示：会员尚未通过验证！\"}");
                        }
                        else
                        {
                            // context.Response.Write("{\"status\":1, \"url\":\"" +
                            //    new Web.UI.BasePage().getlink(sitepath, new Web.UI.BasePage().linkurl("register", "?action=sendsms&username=" + Utils.UrlEncode(model.user_name))) + "\", \"msg\":\"会员尚未通过验证！\"}");
                            context.Response.Write("{\"status\":0, \"msg\":\"错误提示：会员尚未通过验证！\"}");
                        }
                        return;
                    }
                    else if (model.status == 2) //待审核
                    {
                        // context.Response.Write("{\"status\":1, \"url\":\""
                        //     + new Web.UI.BasePage().getlink(sitepath, new Web.UI.BasePage().linkurl("register", "?action=verify&username=" + Utils.UrlEncode(model.user_name))) + "\", \"msg\":\"会员尚未通过审核！\"}");
                        context.Response.Write("{\"status\":0, \"msg\":\"错误提示：会员尚未通过审核！\"}");
                        return;
                    }

                    logger.Info("检查用户每天登录是否获得积分");
                    //检查用户每天登录是否获得积分
                    if (!new BLL.user_login_log().ExistsDay(model.user_name) && userConfig.pointloginnum > 0)
                    {
                        new BLL.user_point_log().Add(model.id, model.user_name, userConfig.pointloginnum, "每天登录获得积分", true); //增加用户登录积分
                        //model = bll.GetModel(username, password, userConfig.emaillogin, userConfig.mobilelogin, true); //更新一下用户信息
                    }
                    context.Session[DTKeys.SESSION_USER_INFO] = model;
                    context.Session.Timeout = 45;
                    //记住登录状态下次自动登录
                    if (remember.ToLower() == "true")
                    {
                        //Utils.WriteCookie(DTKeys.COOKIE_USER_NAME_REMEMBER, "DTcms", model.user_name, 43200);
                        //Utils.WriteCookie(DTKeys.COOKIE_USER_PWD_REMEMBER, "DTcms", model.password, 43200);
                    }
                    else
                    {
                        logger.Info("防止Session提前过期begin");
                        //防止Session提前过期
                        //Utils.WriteCookie(DTKeys.COOKIE_USER_NAME_REMEMBER, "DTcms", model.user_name);
                        //Utils.WriteCookie(DTKeys.COOKIE_USER_PWD_REMEMBER, "DTcms", model.password);
                        logger.Info("防止Session提前过期end");
                    }

                    try
                    {
                        logger.Info("属性：" + model.id + model.user_name);
                        //写入登录日志
                        new BLL.user_login_log().Add(model.id, model.user_name, "会员登录");
                        //返回URL
                        context.Response.Write("{\"status\":1, \"msg\":\"会员登录成功！\",\"userid\":" + model.id + "}");
                        return;
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex);
                        logger.Info("error:" + ex.Message);
                        context.Response.Write("{\"status\":0, \"msg\":\"error:" + ex.Message + "\"}");
                        return;
                    }
                }
                else
                {
                    context.Response.Write("{\"status\":0, \"msg\":\"手机验证码有误，请重新输入！\"}");
                    return;
                }

            }
            else
            {
                context.Response.Write("{\"status\":0, \"msg\":\"手机验证码有误，请重新获取验证码！\"}");
                return;
            }





        }
        #endregion

        #region 检查用户是否登录OK=============================
        private void user_check_login(HttpContext context)
        {
            //检查用户是否登录
            Model.users model = new BasePage().GetUserInfo();
            if (model == null)
            {
                context.Response.Write("{\"status\":0, \"username\":\"匿名用户\"}");
                return;
            }
            context.Response.Write("{\"status\":1, \"username\":\"" + model.user_name + "\"}");
        }
        #endregion

        #region 登录 发送手机短信验证码OK===========================
        private void user_loginverify_smscode(HttpContext context)
        {
            string mobile = Utils.ToHtml(DTRequest.GetString("mobile"));
            string chkcode = DTRequest.GetString("chkcode");

            logger.Info("登录 发送手机短信验证码," + mobile + "," + chkcode);
            if (string.IsNullOrEmpty(chkcode))
            {
                context.Response.Write("{\"status\":0, \"msg\":\"验证码不能为空！\"}");
                return;
            }
            else
            {
                if (context.Session["REG_V_CODE"] != null)
                {
                    if (chkcode.ToLower() != Convert.ToString(context.Session["REG_V_CODE"]).ToLower())
                    {
                        context.Response.Write("{\"status\":0, \"msg\":\"验证码不正确，请重新输入！\"}");
                        return;
                    }
                }
                else
                {
                    context.Response.Write("{\"status\":0, \"msg\":\"系统错误，请刷新后重试！\"}");
                    return;
                }
            }
            //检查手机
            if (string.IsNullOrEmpty(mobile))
            {
                context.Response.Write("{\"status\":0, \"msg\":\"发送失败，请填写手机号码！\"}");
                return;
            }
            else
            {
                BLL.users bll = new BLL.users();
                if (!bll.Exists(mobile))
                {//以手机号码为用户名 验证手机是否存在
                    context.Response.Write("{\"status\":0, \"msg\":\"请您先注册！\"}");
                    return;
                }

            }


            logger.Info("SMS_LOGIN_CODE");

            if (context.Session[mobile + "SMS_LOGIN_CODE"] != null)
            {
                logger.Info("SMS_LOGIN_CODE01");
                //string sms_code_send_time = context.Session[mobile]
                DateTime dt = (DateTime)context.Session["LOGIN" + mobile];
                if (dt.AddMinutes(1) > System.DateTime.Now)
                {
                    context.Response.Write("{\"status\":0, \"msg\":\"已发送短信，请稍后后再试！\"}");
                    return;
                }
                else
                {

                }
            }

            logger.Info("SMS_LOGIN_CODE02");
            string strcode = Utils.Number(6); //随机验证码
                                            

            int ret = ali_smsvcode_send(mobile, strcode);

            if (ret == 0)
            {
                context.Response.Write("{\"status\":0, \"msg\":\"发送失败，e1001。\"}");
                return;
            }

            context.Session[mobile + "SMS_LOGIN_CODE"] = strcode;
            //Utils.WriteCookie(DTKeys.COOKIE_USER_MOBILE, mobile, userConfig.regsmsexpired); //规定时间内无重复发送
            context.Session["LOGIN" + mobile] = System.DateTime.Now;// userConfig.regsmsexpired;

            context.Response.Write("{\"status\":1, \"msg\":\"手机验证码发送成功！\"}");
            return;
        }
        #endregion

        #region 注册 发送手机短信验证码OK===========================
        private void user_verify_smscode(HttpContext context)
        {
            string mobile = Utils.ToHtml(DTRequest.GetString("mobile"));
            string chkcode = DTRequest.GetString("chkcode");
            if (string.IsNullOrEmpty(chkcode))
            {
                context.Response.Write("{\"status\":0, \"msg\":\"验证码不能为空！\"}");
                return;
            }
            else
            {
                if (context.Session["REG_V_CODE"] != null)
                {
                    if (chkcode.ToLower() != Convert.ToString(context.Session["REG_V_CODE"]).ToLower())
                    {
                        context.Response.Write("{\"status\":0, \"msg\":\"发送失败，验证码不正确！\"}");
                        return;
                    }
                }
                else
                {
                    context.Response.Write("{\"status\":0, \"msg\":\"系统错误，请刷新后重试！\"}");
                    return;
                }
            }
            //检查手机
            if (string.IsNullOrEmpty(mobile))
            {
                context.Response.Write("{\"status\":0, \"msg\":\"发送失败，请填写手机号码！\"}");
                return;
            }
            else
            {
                BLL.users bll = new BLL.users();
                if (bll.Exists(mobile))
                {//以手机号码为用户名 验证手机是否存在
                    context.Response.Write("{\"status\":0, \"msg\":\"手机号码已经注册，请登录！\"}");
                    return;
                }

            }



            string result = send_verify_sms_code(context, mobile);
            if (result != "success")
            {
                context.Response.Write(result);
                return;
            }
            // context.Response.Write("{\"status\":1, \"time\":\"" + userConfig.regsmsexpired + "\", \"msg\":\"手机验证码发送成功！\"}");
            context.Response.Write("{\"status\":1, \"msg\":\"手机验证码发送成功！\"}");
            return;
        }
        #endregion

        #region 发送手机短信验证码OK===========================
        private string send_verify_sms_code(HttpContext context, string mobile)
        {
            //检查手机
            if (string.IsNullOrEmpty(mobile))
            {
                return "{\"status\":0, \"msg\":\"发送失败，请填写手机号码！\"}";
            }

            if (context.Session[mobile] != null)
            {
                //string sms_code_send_time = context.Session[mobile]
                DateTime dt = (DateTime)context.Session[mobile];
                if (dt.AddMinutes(userConfig.regsmsexpired) > System.DateTime.Now)
                {
                    return "{\"status\":0,  \"msg\":\"已发送短信，" + userConfig.regsmsexpired + "分钟后再试！\"}";
                }
            }


            string strcode = Utils.Number(6); //随机验证码

            //int appid = int.Parse(System.Configuration.ConfigurationManager.AppSettings["SMS_appid"].ToString()); //1400239746;
            //// 短信应用 SDK AppKey
            //string appkey = System.Configuration.ConfigurationManager.AppSettings["SMS_appkey"].ToString();// "05db653f1ac1c553025fab31c983ee4d";
            //string templateid = System.Configuration.ConfigurationManager.AppSettings["SMS_templateid"].ToString();
            //string smsSign = System.Configuration.ConfigurationManager.AppSettings["SMS_sign"].ToString();//SMS_sign

            //SmsSingleSender ssender = new SmsSingleSender(appid, appkey);


            //string[] msgcontent = { strcode, userConfig.regsmsexpired.ToString() };
            //var sresult = ssender.sendWithParam("86", mobile, int.Parse(templateid), msgcontent
            //    , smsSign, "", "");

            int ret = ali_smsvcode_send(mobile, strcode);

            if (ret==0)
            {
                return "{\"status\":0, \"msg\":\"发送失败! \"}";
            }


            //写入SESSION，保存验证码
            context.Session[DTKeys.SESSION_SMS_CODE] = strcode;
            //Utils.WriteCookie(DTKeys.COOKIE_USER_MOBILE, mobile, userConfig.regsmsexpired); //规定时间内无重复发送
            context.Session[mobile] = System.DateTime.Now;// userConfig.regsmsexpired;
            return "success";
        }
        #endregion

        #region 校检手机验证码OK===============================
        private string verify_sms_code(HttpContext context, string strcode)
        {
            if (string.IsNullOrEmpty(strcode))
            {
                return "{\"status\":0, \"msg\":\"对不起，请输入验证码！\"}";
            }
            if (context.Session[DTKeys.SESSION_SMS_CODE] == null)
            {
                return "{\"status\":0, \"msg\":\"对不起，验证码超时或已过期！\"}";
            }
            if (strcode.ToLower() != (context.Session[DTKeys.SESSION_SMS_CODE].ToString()).ToLower())
            {
                return "{\"status\":0, \"msg\":\"您输入的验证码与系统的不一致！\"}";
            }
            context.Session[DTKeys.SESSION_SMS_CODE] = null;
            return "success";
        }
        #endregion

        #region 阿里云-短信发送
        /// <summary>
        /// 阿里云-短信发送
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        private int ali_smsvcode_send(string mobile, string code)
        {
            string product = "Dysmsapi";//短信API产品名称
            string domain = "dysmsapi.aliyuncs.com";//短信API产品域名
            string accessId = System.Configuration.ConfigurationManager.AppSettings["alidayu_sms_accessId"].ToString(); ;
            string accessSecret = System.Configuration.ConfigurationManager.AppSettings["alidayu_sms_accessSecret"].ToString();

            string SignName = System.Configuration.ConfigurationManager.AppSettings["alidayu_sms_signname"].ToString();
            string TemplateCode = System.Configuration.ConfigurationManager.AppSettings["alidayu_sms_templatecode"].ToString();

            string regionIdForPop = "cn-hangzhou";

            IClientProfile profile = DefaultProfile.GetProfile(regionIdForPop, accessId, accessSecret);
            DefaultProfile.AddEndpoint(regionIdForPop, regionIdForPop, product, domain);
            IAcsClient acsClient = new DefaultAcsClient(profile);
            SendSmsRequest request = new SendSmsRequest();


            try
            {
                //request.SignName = "上云预发测试";//"管理控制台中配置的短信签名（状态必须是验证通过）"
                //request.TemplateCode = "SMS_71130001";//管理控制台中配置的审核通过的短信模板的模板CODE（状态必须是验证通过）"
                //request.RecNum = "13567939485";//"接收号码，多个号码可以逗号分隔"
                //request.ParamString = "{\"name\":\"123\"}";//短信模板中的变量；数字需要转换为字符串；个人用户每个变量长度必须小于15个字符。"
                //SingleSendSmsResponse httpResponse = client.GetAcsResponse(request);
                request.PhoneNumbers = mobile;// "1350000000";
                request.SignName = SignName;// "xxxxxx";
                request.TemplateCode = TemplateCode;// "SMS_xxxxxxx";
                request.TemplateParam = "{\"code\":\"" + code + "\"}";//数字验证码
                request.OutId = "";
                //请求失败这里会抛ClientException异常
                SendSmsResponse sendSmsResponse = acsClient.GetAcsResponse(request);
                if (sendSmsResponse.Code == "OK")
                {
                    return 1;
                }
                else
                {
                    logger.Debug("sendSmsResponse.Code_no_ok_msg：" + sendSmsResponse.Message);
                    return 0;

                }

            }
            catch (Exception ex)
            {
                logger.Error("ali_smsvcode_send_catch_error:" + ex.Message);
                return 0;
            }
            return 0;
        }
        #endregion

        #region 腾讯云——短信发送
        /// <summary>
        /// 腾讯云——短信发送
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="strcode"></param>
        /// <returns></returns>
        private int qcloud_smsvcode_send(string mobile, string strcode)
        {
            int appid = int.Parse(System.Configuration.ConfigurationManager.AppSettings["SMS_appid"].ToString()); //1400239746;
            // 短信应用 SDK AppKey
            string appkey = System.Configuration.ConfigurationManager.AppSettings["SMS_appkey"].ToString();// "05db653f1ac1c553025fab31c983ee4d";
            string templateid = System.Configuration.ConfigurationManager.AppSettings["SMS_templateid"].ToString();
            string smsSign = System.Configuration.ConfigurationManager.AppSettings["SMS_sign"].ToString();//SMS_sign

            SmsSingleSender ssender = new SmsSingleSender(appid, appkey);


            string[] msgcontent = { strcode, userConfig.regsmsexpired.ToString() };
            var sresult = ssender.sendWithParam("86", mobile, int.Parse(templateid), msgcontent
                , smsSign, "", "");

            if (sresult.result != 0)
            {
                logger.Debug("qcloud_smsvcode_send_ssender_error" + sresult.errMsg);
                return 0;
            }
            else
            {
                return 1;
            }

        }
        #endregion


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}