﻿using DTcms.Common;
using DTcms.Web.cmsapi.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace DTcms.Web.cmsapi
{
    /// <summary>
    /// Handler1 的摘要说明
    /// </summary>
    public class Handler1 : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            JsonResult<channel_article_news> res = new JsonResult<channel_article_news>();
            try
            {


                int channel_id = DTRequest.GetQueryInt("channel_id");
                int category_id = DTRequest.GetQueryInt("category_id");
                int pagesize = DTRequest.GetQueryInt("pagesize");
                int page = DTRequest.GetQueryInt("page");
                int ordertype = DTRequest.GetQueryInt("ordertype");
                //string keywords = DTRequest.GetQueryString("keywords");
                int record_count = 0;

                string keywords = DTRequest.GetQueryString("keywords");

                BLL.article articlebll = new BLL.article();
                BLL.article_comment commentbll = new BLL.article_comment();
                string strorder = " add_time desc ";
                if (ordertype == 1)
                {
                    strorder = " sort_id desc ";
                }
                else if (ordertype == 2)
                {
                    strorder = " sort_id asc ";
                }
                string strwhere = " status!=2 ";
                if (keywords.Trim() != "" && keywords.Trim().Length < 10)
                {
                    strwhere += " and title like '%" + keywords.Trim() + "%'";

                }
                var listdataset = articlebll.GetList(channel_id, category_id, pagesize, page, strwhere, strorder, out record_count);
                List<channel_article_news> modellist = new List<channel_article_news>();
                if (listdataset != null && listdataset.Tables.Count > 0)
                {

                    #region 扩展字段信息===================
                   

                   
                    #endregion

                    var list = listdataset.Tables[0];
                    //DataRow dr = null;
                    for (int i = 0; i < list.Rows.Count; i++)
                    {
                        channel_article_news model = new channel_article_news();
                        model.id = int.Parse(list.Rows[i]["id"].ToString());
                        model.channel_id = int.Parse(list.Rows[i]["channel_id"].ToString());
                        model.category_id = int.Parse(list.Rows[i]["category_id"].ToString());
                        model.title = list.Rows[i]["title"].ToString();
                        model.link_url = list.Rows[i]["link_url"].ToString();
                        model.img_url = list.Rows[i]["img_url"].ToString();
                        model.content = list.Rows[i]["content"].ToString();
                        model.sort_id = int.Parse(list.Rows[i]["sort_id"].ToString());
                        model.status = int.Parse(list.Rows[i]["status"].ToString());
                        model.add_time = DateTime.Parse(list.Rows[i]["add_time"].ToString());
                        model.zhaiyao = list.Rows[i]["zhaiyao"].ToString();
                        model.click = int.Parse(list.Rows[i]["click"].ToString());
                        model.comment = commentbll.GetCount("channel_id="+ model.channel_id+ " and article_id="+ model.id);

                        model.is_top = int.Parse(list.Rows[i]["is_top"].ToString());
                        model.is_red= int.Parse(list.Rows[i]["is_red"].ToString());
                        model.is_hot= int.Parse(list.Rows[i]["is_hot"].ToString());
                        // dr = list.Rows[i];
                        //for (int j = 0; j < dr.Table.Columns.Count; i++)
                        //{
                        //    if (fieldDic.ContainsKey(dr.Table.Columns[j].ColumnName) && dr != null)
                        //    {
                        //        fieldDic[dr.Table.Columns[j].ColumnName] = dr[j].ToString();
                        //    }
                        //}
                        Dictionary<string, string> fieldDic = new Dictionary<string, string>();   //new DAL.article_attribute_field(databaseprefix).GetFields(model.channel_id);//扩展字段字典
                        fieldDic.Add("hospital", "");
                        fieldDic.Add("doctitle", "");
                        fieldDic.Add("author_img", ""); 

                        if (list.Rows[i].Table.Columns.Contains("hospital"))
                        {
                            fieldDic["hospital"] = list.Rows[i]["hospital"].ToString();
                        }
                        if (list.Rows[i].Table.Columns.Contains("doctitle"))
                        {
                            fieldDic["doctitle"] = list.Rows[i]["doctitle"].ToString();
                        }
                        if (list.Rows[i].Table.Columns.Contains("author_img"))
                        {
                            fieldDic["author_img"] = list.Rows[i]["author_img"].ToString();
                        }
                        model.fields = fieldDic;

                        //DataRow row
                        //for (int i = 0; i < row.Table.Columns.Count; i++)
                        //{
                        //    if (fieldDic.ContainsKey(row.Table.Columns[i].ColumnName) && row[i] != null)
                        //    {
                        //        fieldDic[row.Table.Columns[i].ColumnName] = row[i].ToString();
                        //    }
                        //}
                        //model.fields = fieldDic;

                        modellist.Add(model);
                    }
                }
                res.ResultCount = modellist.Count;
                res.ResultCode = 1;
                res.ResultMessage = "成功";
                res.JsonEntity = modellist; 

            }
            catch (Exception ex)
            {
                res.ResultCode = -2;
                res.ResultMessage = ex.Message;
            }
            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}