﻿using DTcms.Common;
using DTcms.Model;
using DTcms.Web.cmsapi.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DTcms.Web.cmsapi
{
    /// <summary>
    /// getnewsinfo 的摘要说明
    /// </summary>
    public class getnewsinfo : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            JsonResult<DTcms.Model.article> res = new JsonResult<article>();
            try
            {
                int channel_id = DTRequest.GetQueryInt("channel_id");
                int news_id = DTRequest.GetQueryInt("news_id");
                int record_count = 0;
                BLL.article articlebll = new BLL.article();
                List<article> modellist = new List<article>();
                var newsinfo = articlebll.GetModel(channel_id, news_id);

                if (newsinfo != null)
                {
                    newsinfo.click = newsinfo.click + 1;
                    articlebll.Update(newsinfo);
                    res.ResultCount = 1;
                  
                    modellist.Add(newsinfo);
                }



                res.JsonEntity = modellist;
                res.ResultCode = 1;
                res.ResultMessage = "成功";
            }
            catch (Exception ex)
            {
                res.ResultCode = -2;
                res.ResultMessage = ex.Message;
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}