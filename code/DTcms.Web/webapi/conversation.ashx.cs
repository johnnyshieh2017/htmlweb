﻿using DTcms.Common;
using DTcms.Web.api.Models;

using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DTcms.Web.webapi
{
    /// <summary>
    /// conversation 的摘要说明
    /// </summary>
    public class conversation : userauthority, IHttpHandler
    {
        private Logger logger = LogManager.GetCurrentClassLogger();
        public void ProcessRequest(HttpContext context)
        {
            //取得处事类型
            string action = DTRequest.GetQueryString("action");


            switch (action)
            {
                case "get": // 
                    get(context);
                    break;
                case "update"://
                    update(context);
                    break;
                case "create":
                    create(context);
                    break;
            }
        }

        private void get(HttpContext context)
        {
            int userid = DTcms.Common.DTRequest.GetFormInt("userid");
            string  msid = DTcms.Common.DTRequest.GetFormString("sessionid");
            int status = DTcms.Common.DTRequest.GetFormIntValue("status", -1);
            int pagesize = DTcms.Common.DTRequest.GetFormIntValue("pagesize", 10);
            int page = DTRequest.GetFormIntValue("page", 1);

            api.Models.JsonResult<Model.View_zx_messages> res = new JsonResult<Model.View_zx_messages>();

            //BLL.yjr_zx_messages message = new BLL.yjr_zx_messages();
            BLL.View_zx_messages messagebll = new BLL.View_zx_messages();
            string strwhere = " 1=1";
            string strorder = " createtime desc "; 
            
            if (msid != "")
            {
                strwhere += " and sessionid='" + msid + "'";// + msid;
            }
            if (status != -1)
            {
                strwhere += " and status="+status;
            }

            int recordcount = 0;
            var list = messagebll.GetModelList(pagesize, page, strwhere, strorder, out recordcount);
            res.JsonEntity = list;
            res.ResultCode = 1;
            res.ResultCount = list.Count;
            res.ResultMessage = "sucess";

            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("recordcount", recordcount.ToString());
            res.ResultExt = dic;
            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }

        private void docget(HttpContext context)
        {
            int doctorid = DTcms.Common.DTRequest.GetFormInt("doctorid");
            string msid = DTcms.Common.DTRequest.GetFormString("msid");
            int status = DTcms.Common.DTRequest.GetFormIntValue("status", -1);
            int pagesize = DTcms.Common.DTRequest.GetFormIntValue("pagesize", 10);
            int page = DTRequest.GetFormIntValue("page", 1);

            api.Models.JsonResult<Model.yjr_zx_messages> res = new JsonResult<Model.yjr_zx_messages>();

            BLL.yjr_zx_messages message = new BLL.yjr_zx_messages();

            string strwhere = " doctorid=" + doctorid + " ";
            string strorder = " createtime desc ";
            if (msid != "")
            {
                strwhere += " and sessionid='" + msid + "'";// + msid;
            }
            if (status != -1)
            {
                strwhere += " and status=" + status;
            }

            int recordcount = 0;
            var list = message.GetModelList(pagesize, page, strwhere, strorder, out recordcount);
            res.JsonEntity = list;
            res.ResultCode = 1;
            res.ResultCount = list.Count;
            res.ResultMessage = "sucess";

            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("recordcount", recordcount.ToString());
            res.ResultExt = dic;
            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }

        private void update(HttpContext context)
        {
            
        }

        private void create(HttpContext context)
        {
            JsonResult<string> res = new JsonResult<string>();
            int userid = DTcms.Common.DTRequest.GetFormInt("userid");
            string sessionid = DTcms.Common.DTRequest.GetFormString("sessionid");
            string content = DTcms.Common.DTRequest.GetFormString("content");
            int owner = DTcms.Common.DTRequest.GetFormInt("owner");
            BLL.yjr_zx_messages messagebll = new BLL.yjr_zx_messages();
            if (messagebll.GetRecordCount("sessionid='" + sessionid + "'") < 1)
            {
                response_error("会话不存在");
            }
            else
            {
                int recordcount = 0;
                var message = messagebll.GetModelList(2, 1, "sessionid='" + sessionid + "' ", "createtime desc", out recordcount).FirstOrDefault();
                Model.yjr_zx_messages newmessage = new Model.yjr_zx_messages();
                newmessage = message;
                newmessage.createtime = System.DateTime.Now;
                newmessage.owner = owner;
                newmessage.zx_content = content;
                newmessage.zx_number = message.zx_number + 1;
                //
               var r = messagebll.Add(newmessage);

                //发布新的回复或提问后，更改订单进行状态
                BLL.yjr_zx_order msorderbll = new BLL.yjr_zx_order();
                var msorder = msorderbll.GetModelList("ms_sessionid='"+ sessionid + "' and status!=9 ").FirstOrDefault();
                //msorder.status = owner;//此处与owner一致，利于判断待提问还是待回答
                msorderbll.Update(msorder);

                res.ResultCode = 1;
                res.ResultCount = r;
                res.ResultMessage = "sucess";

                context.Response.ContentType = "application/json";
                context.Response.Write(JsonConvert.SerializeObject(res));
                context.Response.End();
            }
        }

        private void response_error(string error)
        {
            JsonResult<string> res = new JsonResult<string>();
            res.ResultCode = 0;
            res.ResultCount = 0;
            res.ResultMessage = error;
            HttpContext context = HttpContext.Current;
            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}