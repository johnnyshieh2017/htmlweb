﻿using DTcms.Common;
using DTcms.Web.api.Models;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace DTcms.Web.webapi
{
    /// <summary>
    /// member 的摘要说明
    /// </summary>
    public class member : userauthority,IHttpHandler
    {

        private static Logger logger = LogManager.GetCurrentClassLogger();
        public void ProcessRequest(HttpContext context)
        {
            //取得处事类型
            string action = DTRequest.GetQueryString("action");


            switch (action)
            {
                case "get": // 获取个人信息
                    get(context);
                    break;
              
                case "update"://更新个人信息
                    update(context);
                    break;
                case "uploadimg"://上传图片
                    uploadimg(context);
                    break;

                case "uploadimgbase64"://上传图片
                    uploadimgbase64(context);
                    break;
            }
        }

        private void get(HttpContext context)
        {
            //
            int userid = DTcms.Common.DTRequest.GetFormInt("userid");// //userid

            JsonResult<memberjson> res = new JsonResult<memberjson>();

            BLL.users bll = new BLL.users();

            var user = bll.GetModel(userid);
            if (user != null)
            {
                res.JsonEntity = new List<memberjson>() { new memberjson(userid, user.nick_name, user.mobile, user.avatar, user.sex) };
                res.ResultCode = 1;
                res.ResultCount = 1;
                res.ResultMessage = "sucess";
            }
            else
            {
                res.ResultCode = 0;
                res.ResultCount = 0;
                res.ResultMessage = "fails";
            }
            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();

        }

        private void update(HttpContext context)
        {
            int userid = DTcms.Common.DTRequest.GetFormInt("userid");// //userid
            string avatar = DTcms.Common.DTRequest.GetFormString("avatar");
            string nick_name = DTcms.Common.DTRequest.GetFormString("nick_name");
            string sex = DTcms.Common.DTRequest.GetFormString("sex");


            JsonResult <memberjson> res = new JsonResult<memberjson>();

            BLL.users bll = new BLL.users();

            var user = bll.GetModel(userid);
            if (user != null)
            {

                if (nick_name != "")
                {
                    user.nick_name=nick_name;
                }
                if (avatar != "")
                {
                    user.avatar = avatar;
                }
                if (sex != "")
                {
                    user.sex = sex;
                }

                var result = bll.Update(user) == true ? 1 : 0;

                if (user.hospital == "2"&& result==1)
                {
                    var uvd = new BLL.yjr_doctor_vsuser().GetModelList("userid="+user.id).FirstOrDefault();
                    if (uvd != null)
                    {
                        BLL.yjr_doctor docbll = new BLL.yjr_doctor();
                        var doc = docbll.GetModel(int.Parse(uvd.docid));
                        if (doc != null)
                        {
                            doc.logoimg = user.avatar;
                            docbll.Update(doc);
                        }
                    }
                }

                res.JsonEntity = new List<memberjson>() { new memberjson(userid, user.nick_name, user.mobile, user.avatar, user.sex) };
                res.ResultCode = 1;
                res.ResultCount = 1;
                res.ResultMessage = "sucess";
            }
            else
            {
                res.ResultCode = 0;
                res.ResultCount = 0;
                res.ResultMessage = "fails";
            }
            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }

        private void uploadimg(HttpContext context)
        {
            JsonResult<SaveImageResult> res = new JsonResult<SaveImageResult>();
            string userid = DTRequest.GetFormString("userid");
            try
            {
                
                HttpFileCollection hfc = context.Request.Files;
                string saveName = System.DateTime.Now.ToString("yyyyMMddHHmmssffff");
                string extention = ".png";

                HttpPostedFile hpf = hfc[0];

                //文件保存目录路径
                String savepath = context.Server.MapPath("/upload/user"+ userid + "/");
                if (!Directory.Exists(savepath))
                    Directory.CreateDirectory(savepath);

                String filePath = savepath + saveName + extention;

                List<SaveImageResult> imglist = new List<SaveImageResult>();

                hpf.SaveAs(filePath);
                res.ResultCode = 1;
                SaveImageResult info = new SaveImageResult();
                info.ReturnPath = "/upload/user"+ userid + "/" + saveName + extention;
                imglist.Add(info);
                res.JsonEntity = imglist;



            }
            catch (Exception ex)
            {

                res.ResultCode = -2;
                res.ResultMessage = ex.Message;
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }

        private void uploadimgbase64(HttpContext context)
        {
            JsonResult<SaveImageResult> res = new JsonResult<SaveImageResult>();
            string userid = DTRequest.GetFormString("userid");
            string base64str = DTRequest.GetFormString("b64str");
            string savepath = "/upload/user" + userid + "/";

            if (!Directory.Exists(savepath))
                Directory.CreateDirectory(savepath);

            string saveName = System.DateTime.Now.ToString("yyyyMMddHHmmssffff");
            string extention = ".png";
            SaveImageResult result = api.UtilsTool.H5ImgUpload.SaveBase64StrToImage(base64str, "", savepath, saveName, extention, 635810);

            List<SaveImageResult> imglist = new List<SaveImageResult>();
            imglist.Add(result);

            res.ResultCode = result.ErrorCode == 0 ? 1 : 0;
            res.ResultMessage = result.ErrorMessage;

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}