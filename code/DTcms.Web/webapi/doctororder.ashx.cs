﻿using DTcms.Common;
using DTcms.Model;
using DTcms.Web.api.Models;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace DTcms.Web.webapi
{
    /// <summary>
    /// doctororder 的摘要说明
    /// </summary>
    public class doctororder : userauthority, IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            //取得处事类型
            string action = DTRequest.GetQueryString("action");


            switch (action)
            {
                case "freeconfirm": // do
                    freeconfirm(context);
                    break;
                case "getfreelist": // do
                    getfreelist(context);
                    break;
                //case "getfreecount": // do
                //    getfreelist(context);
                //    break;
            }
        }

        

        private void getfreelist(HttpContext context)
        {
            //
            int doctor_id = DTcms.Common.DTRequest.GetFormInt("doctorid");
            int orderid = DTcms.Common.DTRequest.GetFormInt("orderid");
            int status = DTcms.Common.DTRequest.GetFormIntValue("status", -1);
            int pagesize = DTcms.Common.DTRequest.GetFormIntValue("pagesize", 10);
            int page = DTRequest.GetFormIntValue("page", 1);

            JsonResult<Model.View_zx_order> res = new JsonResult<Model.View_zx_order>();
            //BLL.yjr_zx_order bll = new BLL.yjr_zx_order();
            BLL.View_zx_order VBLL = new BLL.View_zx_order();

            string strwhere = " service_type=-1 ";
            if (doctor_id != 0)
            {
                strwhere += " and  doctor_id=" + doctor_id + " ";
            }
            else
            {
                strwhere += " and  doctor_id=0 ";
            }
            string strorder = " createtime desc ";
            if (orderid != 0)
            {
                strwhere += " and id=" + orderid;
            }
            if (status != -1)
            {
                strwhere += " and status=" + status;
            }

            int recordcount = 0;
            var list = VBLL.GetModelList(pagesize, page, strwhere, strorder, out recordcount);// bll.GetModelList(pagesize,page, strwhere, strorder,out recordcount);
            res.JsonEntity = list;
            res.ResultCode = 1;
            res.ResultCount = list.Count;
            res.ResultMessage = "sucess";

            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("recordcount", recordcount.ToString());
            res.ResultExt = dic;
            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }

        private void freeconfirm(HttpContext context)
        {
            JsonResult<string> res = new JsonResult<string>();
            int docid = DTRequest.GetFormInt("doctorid");
            int orderid = DTcms.Common.DTRequest.GetFormInt("orderid");
            int userid = DTcms.Common.DTRequest.GetFormInt("userid");

            if (docid != 0)
            {
                BLL.yjr_zx_order bll = new BLL.yjr_zx_order();
                var model = bll.GetModel(orderid);
                if (model != null&&model.doctor_id==0&&model.service_type==-1)
                {
                    model.doctor_id = docid;
                    model.status = 1;
                    bll.Update(model);

                    BLL.yjr_zx_messages msgbll = new BLL.yjr_zx_messages();

                    var list = msgbll.GetModelList("sessionid='"+ model .ms_sessionid+ "'");
                    foreach (var item in list)
                    {
                        item.doctorid = docid;
                        msgbll.Update(item);
                    }

                    res.ResultCode = 1;
                    res.ResultCount = 1;
                    res.ResultMessage = "sucess";

                    context.Response.ContentType = "application/json";
                    context.Response.Write(JsonConvert.SerializeObject(res));
                    context.Response.End();
                }
                else
                {
                    response_error("订单编号有误！");
                }
            }
            else
            {
                response_error("医生编号有误！");
            }
        }

        
        
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}