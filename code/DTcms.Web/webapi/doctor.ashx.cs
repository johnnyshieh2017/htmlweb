﻿using DTcms.Common;
using DTcms.Model;
using DTcms.Web.api.Models;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace DTcms.Web.webapi
{
    /// <summary>
    /// doctor 的摘要说明
    /// </summary>
    public class doctor : IHttpHandler, IRequiresSessionState
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public void ProcessRequest(HttpContext context)
        {
         

            //取得处事类型
            string action = DTRequest.GetQueryString("action");


            switch (action)
            {
                case "get": // 获取医生个人信息
                   get(context);
                    break;
                case "create"://创建医生信息
                   create(context);
                    break;
                case "update"://更新医生信息
                   update(context);
                    break;
            }
        }


        #region 获取医生用户信息
        private void get(HttpContext context)
        {
            JsonResult<Model.yjr_doctor> res = new JsonResult<Model.yjr_doctor>();
            int docid = DTRequest.GetFormInt("doctorid");

            BLL.yjr_doctor bll = new BLL.yjr_doctor();
            var doc = bll.GetModel(docid);
            if (doc != null)
            {
                doc.rate = "100%";
                doc.bookcount = new Random().Next(20,50).ToString();
                doc.responsetime = "30";

                res.JsonEntity = new List<Model.yjr_doctor> { doc };
                res.ResultCode = 1;
                res.ResultCount = 1;
                res.ResultMessage = "sucess!";
            }
            else
            {
                res.ResultCode = 0;
                res.ResultCount = 0;
                res.ResultMessage = "fails!";

             
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.ApplicationInstance.CompleteRequest();

        }
        #endregion

        #region 用户信息更新
        private void update(HttpContext context)
        {
            JsonResult<userjson> res = new JsonResult<userjson>();

            

            int userid = DTRequest.GetFormInt("userid");
            string name = DTRequest.GetFormString("name").Trim();
            string gender = DTRequest.GetFormString("gender");
            int hospital = DTRequest.GetFormInt("hospitalid");
            int department = DTRequest.GetFormInt("departmentid");
            string department_name = DTRequest.GetFormString("departmentname");
            int title = DTRequest.GetFormInt("title");
            string intro = DTRequest.GetFormString("intro");
            string speciality = DTRequest.GetFormString("speciality");

            string idcard = DTRequest.GetFormString("idcard");
            string idcardpic_front = DTRequest.GetFormString("idcardpic_front");
            string idcardpic_back = DTRequest.GetFormString("idcardpic_back");

            string doc_license = DTRequest.GetFormString("doc_license");//医生执业证
            string doc_qualification = DTRequest.GetFormString("doc_qualification");//医生资格证
            string doc_titlepic = DTRequest.GetFormString("doc_titlepic");//专业技术资格证（职称证）

            #region 登录验证

            string token = DTRequest.GetFormString("usertoken");
            //string userid = DTRequest.GetFormString("userid");

            //logger.Debug("userauthority_token:" + token);
            //logger.Debug("userauthority_userid:" + userid);
            //logger.Debug("userauthority_action:" + DTRequest.GetQueryString("action"));

            if (token != "" && userid != 0)
            {
                BLL.yjr_user_uservstoken bll_uvt = new BLL.yjr_user_uservstoken();
                var model = bll_uvt.GetModel(userid);
                if (model != null)
                {
                    if (model.token == token && model.expires > System.DateTime.Now)
                    {

                    }
                    else
                    {
                        context.Response.ContentType = "application/json";

                        res.ResultCode = 0;
                        res.ResultMessage = "登录超时或未登录";
                        context.Response.Write(JsonConvert.SerializeObject(res));
                        context.Response.End();
                    }
                }
            }
            else
            {
                context.Response.ContentType = "application/json";

                res.ResultCode = 0;
                res.ResultMessage = "未登录";
                context.Response.Write(JsonConvert.SerializeObject(res));
                context.Response.End();
            }

            #endregion


            BLL.yjr_doctor_vsuser bll = new BLL.yjr_doctor_vsuser();
            var vsuser = bll.GetModelList("userid="+userid.ToString()).FirstOrDefault();
            if (vsuser != null)
            {
                BLL.yjr_doctor docbll = new BLL.yjr_doctor();
                var doc = docbll.GetModel(int.Parse(vsuser.docid));

                if (doc != null)
                {
                    if (name != "")
                    {
                        doc.name = name;
                    }

                    if (gender != "")
                    {
                        doc.gender = gender;
                    }

                    if (hospital != 0)
                    {
                        doc.hospitalid = hospital;
                    }

                    if (department != 0)
                    {
                        doc.departmentid = department;
                    }

                    if (department_name != "")
                    {
                        doc.department_name = department_name;
                    }

                    if (title != 0)
                    {
                        doc.title = title;
                    }

                    if (intro != "")
                    {
                        doc.intro = intro;
                    }

                    if (speciality != "")
                    {
                        doc.speciality = speciality;
                    }



                    if (idcard != "")
                    {
                        doc.idcard = idcard;
                    }
                    if (idcardpic_front != "")
                    {
                        doc.idcardpic_front = idcardpic_front;
                    }
                    if (idcardpic_back != "")
                    {
                        doc.idcardpic_back = idcardpic_back;
                    }
                    if (doc_license != "")
                    {
                        doc.doc_license = doc_license;
                    }
                    if (doc_qualification != "")
                    {
                        doc.doc_qualification = doc_qualification;
                    }
                    if (doc_titlepic != "")
                    {
                        doc.doc_titlepic = doc_titlepic;
                    }

                    try
                    {
                        var result = docbll.Update(doc) == true ? 1 : 0;
                        res.ResultCode = result;
                    }
                    catch(Exception ex)
                    {
                        logger.Error(ex);
                        logger.Error("error:" + ex.Message);
                        //context.Response.Write("{\"status\":0, \"msg\":\"error:" + ex.Message + "\"}");

                        res.ResultCode = 0;
                        res.ResultMessage = "系统异常，请稍后再试";
                        context.Response.ContentType = "application/json";
                        context.Response.Write(JsonConvert.SerializeObject(res));
                        context.ApplicationInstance.CompleteRequest();
                    }
                                         
                }
                else
                {
                    res.ResultCode = 0;
                    res.ResultMessage = "信息不存在！";
                }

            }
            else
            {
                res.ResultCode = 0;
                res.ResultMessage = "信息不存在！";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.ApplicationInstance.CompleteRequest();
            
        }
        #endregion

        #region 创建用户(医生)信息
        private void create(HttpContext context)
        {
            JsonResult<userjson> res = new JsonResult<userjson>();
            int userid = DTRequest.GetFormInt("userid");
            string name = DTRequest.GetFormString("name").Trim();
            string gender = DTRequest.GetFormString("gender");
            int hospital = DTRequest.GetFormInt("hospitalid");
            int department = DTRequest.GetFormInt("departmentid");
            string department_name = DTRequest.GetFormString("departmentname");
            int title = DTRequest.GetFormInt("title");
            string intro = DTRequest.GetFormString("intro");
            string speciality = DTRequest.GetFormString("speciality");

            string idcard = DTRequest.GetFormString("idcard");
            string idcardpic_front = DTRequest.GetFormString("idcardpic_front");
            string idcardpic_back = DTRequest.GetFormString("idcardpic_back");

            string doc_license = DTRequest.GetFormString("doc_license");//医生执业证
            string doc_qualification = DTRequest.GetFormString("doc_qualification");//医生资格证
            string doc_titlepic = DTRequest.GetFormString("doc_titlepic");//专业技术资格证（职称证）
            string mobile = DTRequest.GetFormString("mobile");


            #region 登录验证

            string token = DTRequest.GetFormString("usertoken");
            //string userid = DTRequest.GetFormString("userid");

            //logger.Debug("userauthority_token:" + token);
            //logger.Debug("userauthority_userid:" + userid);
            //logger.Debug("userauthority_action:" + DTRequest.GetQueryString("action"));

            if (token != "" && userid != 0)
            {
                BLL.yjr_user_uservstoken bll_uvt = new BLL.yjr_user_uservstoken();
                var model = bll_uvt.GetModel(userid);
                if (model != null)
                {
                    if (model.token == token && model.expires > System.DateTime.Now)
                    {

                    }
                    else
                    {
                        context.Response.ContentType = "application/json";

                        res.ResultCode = 0;
                        res.ResultMessage = "登录超时或未登录";
                        context.Response.Write(JsonConvert.SerializeObject(res));
                        context.Response.End();
                    }
                }
            }
            else
            {
                context.Response.ContentType = "application/json";

                res.ResultCode = 0;
                res.ResultMessage = "未登录";
                context.Response.Write(JsonConvert.SerializeObject(res));
                context.Response.End();
            }

            #endregion

            //BLL.yjr_doctor bll = new BLL.yjr_doctor();
            BLL.yjr_doctor_vsuser bll = new BLL.yjr_doctor_vsuser();
            var vsuser = bll.GetModelList("userid=" + userid.ToString()).FirstOrDefault();
            if (vsuser == null)
            {
                BLL.yjr_doctor blldoc = new BLL.yjr_doctor();
                Model.yjr_doctor doc_model = new Model.yjr_doctor();
                doc_model.name = name;
                doc_model.title = title;
                doc_model.gender = gender;
                doc_model.groupid = 0;
                doc_model.intro = intro;
                doc_model.speciality = speciality;
                doc_model.logoimg = "/upload/default-head.png";//默认头像
                doc_model.hospitalid = hospital;
                doc_model.departmentid = department;
                doc_model.department_name = department_name;
                doc_model.mobile = mobile;
                doc_model.position_id = 1;//暂时固定为医生
                doc_model.idcard = idcard;
                doc_model.idcardpic_front = idcardpic_front;
                doc_model.idcardpic_back = idcardpic_back;
                doc_model.doc_license = doc_license;
                doc_model.doc_qualification = doc_qualification;
                doc_model.doc_titlepic = doc_titlepic;
                doc_model.siteid = 1;
                doc_model.sort = 99;
                doc_model.status = 0; // 接口创建的医生信息 默认未0 未审核状态
                doc_model.createtime = System.DateTime.Now;
                var docid = blldoc.Add(doc_model);

                Model.yjr_doctor_vsuser vsmodel = new Model.yjr_doctor_vsuser();
                vsmodel.docid = docid.ToString();
                vsmodel.userid = userid.ToString();
                vsmodel.add_time = System.DateTime.Now;
                var result = bll.Add(vsmodel) == true ? 1 : 0;
                res.ResultCode = result;
             
            }
            else
            {
                res.ResultCode = 0;
                res.ResultMessage = "信息已存在，请更新！";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.ApplicationInstance.CompleteRequest();

        }
        #endregion

     

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }

}