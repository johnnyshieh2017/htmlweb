﻿using DTcms.Common;
using DTcms.Web.api.Models;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace DTcms.Web.api.Models
{
    public class commonmodel
    {

    }

    public class userjson
    {
        public userjson(int _userid, string _usertoken,int _usertype,int _has,int _doctorid)
        {
            userid = _userid;
            usertoken = _usertoken;
            usertype = _usertype;
            doctorid = _doctorid;
            hascompleted = _has;
        }
        public int userid { get; set; }
        public string usertoken { get; set; }
        public int usertype { get; set; }
        public int doctorid { get; set; }
        public int hascompleted { get; set; }
    }

    public class JsonResult<T>
    {

        public List<T> JsonEntity { get; set; }

        /// <summary>
        /// 无错误 1 面向用户的错误信息 0
        /// </summary>
        public int ResultCode { get; set; }

        public int ResultCount { get; set; }
        /// <summary>
        /// 错误信息
        /// </summary>
        public string ResultMessage { get; set; }

        public Dictionary<string, string> ResultExt { get; set; }

    }

    public class memberjson
    {
        public memberjson(int _userid, string _nick_name, string _mobile, string _avatar, string _sex)
        {
            userid = _userid;
            nick_name = _nick_name;
            mobile = _mobile;
            avatar = _avatar;
            sex = _sex;
            
        }
        public int userid { get; set; }
        public string nick_name { get; set; }
        public string mobile { get; set; }
        public string avatar { get; set; }
        public string sex { get; set; }
    }


    public class SaveImageResult
    {
        /// <summary>
        /// 0：无错误 1：面向用户的错误信息
        /// </summary>
        public int ErrorCode { get; set; }

        /// <summary>
        /// 错误信息
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// 返回用于存储的路径
        /// </summary>
        public string ReturnPath { get; set; }

    }

    public class UploadPath
    {
        public string filepath
        {
            get;
            set;
        }
    }
}

namespace DTcms.Web.api.UtilsTool
{
    public static class StringTool
    {
        public static string GetUniqueStr()
        {
            long i = 1;
            foreach (byte b in Guid.NewGuid().ToByteArray())
            {
                i *= ((int)b + 1);
            }
            string result=  string.Format("{0:x}", i - DateTime.Now.Ticks).ToUpper();

            return System.DateTime.Now.ToString("yyyyMMddHHmmssffff")+"_" + result;
        }
    }

    public static class H5ImgUpload
    {
        /// <summary>
        /// 将 html5 FileReader 读出来的base64格式图片转化成一定格式的图片存起来
        /// </summary>
        /// <param name="base64Str">开头为data:image/jpeg;base64, + base64图片字符串</param>
        /// <param name="domain">引用程序域名</param>
        /// <param name="savePath">根目录开始的保存路径： uploadfile\\201703 不存在该路径则会自动创建 </param>
        /// <param name="saveName">2017030278787.jpg 中"." 前一段</param>
        /// <param name="extension">.jpg</param>
        /// <param name="limitSize">限制字节长度</param>
        /// <returns>1：面向用户的错误信息  </returns>
        public static Models.SaveImageResult SaveBase64StrToImage(string base64Str, string domain, string savePath, string saveName, string extension, long limitSize)
        {
            Models.SaveImageResult result = new Models.SaveImageResult();
            string imgFullName = saveName + extension;  // 格式类似 20170903787878.jpg
            try
            {
                //不加域名，domain 和斜杠 / +"/"
                result.ReturnPath = savePath.Replace("\\", "/") + "/" + imgFullName;// 格式类似 http:axx.xxx.com/uploadfile/2017/03/201709044645545454.jpg
            }
            catch (Exception)
            {
                throw new Exception("参数savePath格式错误");
            }
            byte[] arr = null;
            try
            {
                arr = Convert.FromBase64String(base64Str.Substring(base64Str.IndexOf("base64,") + 7));//切除前面那段image标识
            }
            catch (Exception)
            {
                throw new Exception("参数base64Str格式错误");
            }

            long size = arr.LongLength;

            if (size <= limitSize)// 判断文件大小是否超出限制
            {
                string path = System.Web.HttpRuntime.AppDomainAppPath + savePath;// 获取物理路径

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                using (MemoryStream ms = new MemoryStream(arr))
                {
                    try
                    {

                        Bitmap bmp = new Bitmap(ms);// 此处如果传入的不是一张图片，这里会抛异常
                        bmp.Save(path + "\\" + imgFullName);

                        result.ErrorCode = 0;
                        result.ErrorMessage = "";
                    }
                    catch (Exception)
                    {
                        throw new Exception("图片保存失败,请检查传入参数");
                    }
                }
            }
            else
            {
                result.ErrorCode = 1;
                result.ErrorMessage = "图片超过限制大小";
            }
            if (result.ErrorCode != 0)// 如果有出错，返回路径变成空 
            {
                result.ReturnPath = "";
            }
            return result;
        }


    }


    public static class MD5Helper
    {
        public static string MD5Encrypt(string str)
        {
            string cl = str;
            string pwd = "";
            MD5 md5 = MD5.Create();//实例化一个md5对像
            // 加密后是一个字节类型的数组，这里要注意编码UTF8/Unicode等的选择　
            byte[] s = md5.ComputeHash(Encoding.UTF8.GetBytes(cl));
            // 通过使用循环，将字节类型的数组转换为字符串，此字符串是常规字符格式化所得
            for (int i = 0; i < s.Length; i++)
            {
                // 将得到的字符串使用十六进制类型格式。格式后的字符是小写的字母，如果使用大写（X）则格式后的字符是大写字符

                pwd = pwd + s[i].ToString("x2");

            }
            return pwd;
        }
    }
}

namespace DTcms.Web.webapi
{
    public class userauthority
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public userauthority()
        {

            var context = HttpContext.Current;

            string token = DTRequest.GetFormString("usertoken");
            string userid = DTRequest.GetFormString("userid");

            //logger.Debug("userauthority_token:" + token);
            //logger.Debug("userauthority_userid:" + userid);
            //logger.Debug("userauthority_action:" + DTRequest.GetQueryString("action"));

            if (token != "" && userid != "")
            {
                BLL.yjr_user_uservstoken bll = new BLL.yjr_user_uservstoken();
                var model = bll.GetModel(int.Parse(userid));
                if (model != null)
                {
                    if (model.token == token && model.expires > System.DateTime.Now)
                    {

                    }
                    else
                    {
                        context.Response.ContentType = "application/json";
                        JsonResult<int> res = new JsonResult<int>();
                        res.ResultCode = 0;
                        res.ResultMessage = "登录超时或未登录";
                        context.Response.Write(JsonConvert.SerializeObject(res));
                        context.Response.End();
                    }
                }
            }
            else
            {
                context.Response.ContentType = "application/json";
                JsonResult<int> res = new JsonResult<int>();
                res.ResultCode = 0;
                res.ResultMessage = "未登录";
                context.Response.Write(JsonConvert.SerializeObject(res));
                context.Response.End();
            }
        }

        public void response_error(string error)
        {
            JsonResult<string> res = new JsonResult<string>();
            res.ResultCode = 0;
            res.ResultCount = 0;
            res.ResultMessage = error;
            HttpContext context = HttpContext.Current;
            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }
    }

}
