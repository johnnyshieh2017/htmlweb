﻿using DTcms.Common;
using DTcms.Web.api.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DTcms.Web.webapi
{
    /// <summary>
    /// freeorder 的摘要说明
    /// </summary>
    public class freeorder : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            //取得处事类型
            string action = DTRequest.GetQueryString("action");


            switch (action)
            {

                case "getfreecount": // do
                    getfreecount(context);
                    break;
            }
        }

        private void getfreecount(HttpContext context)
        {
            JsonResult<string> res = new JsonResult<string>();

            BLL.View_zx_order bll = new BLL.View_zx_order();

            var Received_count = bll.Getrecordcount(" DATEDIFF(DAY,createtime,GETDATE())=0 and service_type=-1 ");

            var count = System.Configuration.ConfigurationManager.AppSettings["zx_freecount"] == null ? 15 : int.Parse(System.Configuration.ConfigurationManager.AppSettings["zx_freecount"]);

            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("count", count.ToString());
            dic.Add("t_count", (count - Received_count).ToString());

            res.ResultCode = 1;
            res.ResultMessage = "sucess";
            res.ResultExt = dic;

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}