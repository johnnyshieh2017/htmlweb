﻿using DTcms.API.Payment.wxpay;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DTcms.Web.webapi
{
    public partial class wx_notify_url : System.Web.UI.Page
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        protected void Page_Load(object sender, EventArgs e)
        {

            logger.Info("wx_notify_url开始");
            logger.Debug("wx_notify_url开始");
            logger.Error("wx_notify_url开始");
            logger.Warn("wx_notify_url开始");
            int site_payment_id = 4;
            WxPayData notifyData = JsApiPay.GetNotifyData(site_payment_id); //获取微信传过来的参数

            if (!notifyData.IsSet("transaction_id"))
            {
                //若transaction_id不存在，则立即返回结果给微信支付后台
                WxPayData res = new WxPayData();
                res.SetValue("return_code", "FAIL");
                res.SetValue("return_msg", "支付结果中微信订单号不存在");
                Response.Write(res.ToXml());

                logger.Debug("transaction_id不存在"+res.ToXml());

                return;
            }

            logger.Debug("微信通知返回的信息："+notifyData.ToXml());
            //获取订单信息
            string transaction_id = notifyData.GetValue("transaction_id").ToString(); //微信支付订单号
            string order_no = notifyData.GetValue("out_trade_no").ToString().ToUpper(); //商户订单号
            string total_fee = notifyData.GetValue("total_fee").ToString(); //获取总金额

            //查询订单，判断订单真实性
            if (!QueryOrder(transaction_id))
            {
                //若订单查询失败，则立即返回结果给微信支付后台
                WxPayData res = new WxPayData();
                res.SetValue("return_code", "FAIL");
                res.SetValue("return_msg", "订单查询失败");
                //Response.Write(res.ToXml());
                logger.Error(res.ToXml());
                return;
            }

            BLL.yjr_zx_order order_bll = new BLL.yjr_zx_order();
            var model = order_bll.GetModel(int.Parse(order_no));

            if (model == null)
            {
                WxPayData res = new WxPayData();
                res.SetValue("return_code", "FAIL");
                res.SetValue("return_msg", "该订单号不存在");
                //Response.Write(res.ToXml());
                logger.Error(res.ToXml());
                return;
            }
            if (model.pay_status == 1) //已付款
            {
                WxPayData res = new WxPayData();
                res.SetValue("return_code", "SUCCESS");
                res.SetValue("return_msg", "OK");
                //Response.Write(res.ToXml());
                logger.Error(res.ToXml());
                return;
            }
            if (model.service_fee != decimal.Parse(total_fee))
            {
                WxPayData res = new WxPayData();
                res.SetValue("return_code", "FAIL");
                res.SetValue("return_msg", "订单金额和支付金额不相符");
                //Response.Write(res.ToXml());
                logger.Error(res.ToXml());
                return;
            }
            if (model.pay_status == 0)
            {
                model.pay_status = 1;
                model.pay_time = System.DateTime.Now;
                model.status = 1;

                bool result = order_bll.Update(model);
                if (!result)
                {
                    WxPayData res = new WxPayData();
                    res.SetValue("return_code", "FAIL");
                    res.SetValue("return_msg", "修改订单状态失败");
                    //Response.Write(res.ToXml());
                    logger.Error(res.ToXml());
                    return;
                }
            }

            //返回成功通知
            WxPayData res1 = new WxPayData();
            res1.SetValue("return_code", "SUCCESS");
            res1.SetValue("return_msg", "OK");
            //Response.Write(res.ToXml());
            logger.Error(res1.ToXml());
            return;

        }

        //查询订单
        private bool QueryOrder(string transaction_id)
        {
            int site_payment_id = 4;
            WxPayData req = new WxPayData();
            req.SetValue("transaction_id", transaction_id);
            WxPayData res = JsApiPay.OrderQuery(site_payment_id, req);//
            if (res.GetValue("return_code").ToString() == "SUCCESS" &&
                res.GetValue("result_code").ToString() == "SUCCESS")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}