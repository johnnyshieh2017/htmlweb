﻿using DTcms.Common;
using DTcms.Model;
using DTcms.Web.api.Models;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace DTcms.Web.webapi
{
    /// <summary>
    /// comment 的摘要说明
    /// </summary>
    public class comment : IHttpHandler
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        DTcms.BLL.comment_albums bllcom = new BLL.comment_albums();
        BLL.article_comment bll = new BLL.article_comment();
        public void ProcessRequest(HttpContext context)
        {
            //取得处事类型
            string action = DTRequest.GetQueryString("action");


            switch (action)
            {
                case "get": // 获取评论信息
                    get(context);
                    break;

                case "create"://创建评论信息
                    create(context);
                    break;
            }
        }

        private void get(HttpContext context)
        {
            int channel_id = DTRequest.GetFormInt("channel_id");
            int news_id = DTRequest.GetFormInt("article_id");
            int pagesize = DTRequest.GetFormIntValue("pagesize",50);
            int page = DTRequest.GetFormIntValue("page",1);
     
            string strWhere = " article_id=" + news_id + " and channel_id=" + channel_id + " and  parent_id=0";
            string filedOrder = " add_time asc";
            JsonResult<DTcms.Model.article_comment_ext> res = new JsonResult<DTcms.Model.article_comment_ext>();
            int record_count = 0;
            try
            {
                DataSet list = bll.GetList(pagesize, page, strWhere, filedOrder, out record_count);

                if (list != null && list.Tables[0] != null)
                {
                    List<article_comment_ext> listmodel = new List<article_comment_ext>();
                    for (int i = 0; i < list.Tables[0].Rows.Count; i++)
                    {
                        article_comment_ext article_comment = new article_comment_ext();
                        article_comment.id = int.Parse(list.Tables[0].Rows[i]["id"].ToString());
                        article_comment.content = list.Tables[0].Rows[i]["content"].ToString();
                        article_comment.add_time = DateTime.Parse(list.Tables[0].Rows[i]["add_time"].ToString());
                        article_comment.parent_id = int.Parse(list.Tables[0].Rows[i]["parent_id"].ToString());
                        article_comment.user_id = int.Parse(list.Tables[0].Rows[i]["user_id"].ToString());
                        article_comment.article_id = int.Parse(list.Tables[0].Rows[i]["article_id"].ToString());
                        article_comment.channel_id = int.Parse(list.Tables[0].Rows[i]["channel_id"].ToString());
                        article_comment.img_content = list.Tables[0].Rows[i]["img_content"].ToString(); //GetCommentImg(article_comment.id, article_comment.channel_id);
                        article_comment.children = DataTabletoModel(bll.GetList(0, " parent_id=" + article_comment.id, " add_time desc"));
                        listmodel.Add(article_comment);
                    }
                    res.JsonEntity = listmodel;
                    res.ResultCount = listmodel.Count;
                    res.ResultCode = 1;
                    res.ResultMessage = "成功";
                }
                else
                {
                    res.ResultCount = 0;
                    res.ResultCode = 1;
                    res.ResultMessage = "成功";
                }
            }
            catch (Exception EX)
            {
                res.ResultCount = 0;
                res.ResultCode = -2;
                res.ResultMessage = "ERROR:" + EX.Message;
            }
            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }


        private void create(HttpContext context)
        {
            #region 验证登录
            string token = DTRequest.GetFormString("usertoken");
            string userid = DTRequest.GetFormString("userid");
            if (token != "" && userid != "")
            {
                BLL.yjr_user_uservstoken vsbll = new BLL.yjr_user_uservstoken();
                var vsmodel = vsbll.GetModel(int.Parse(userid));
                if (vsmodel != null)
                {
                    if (vsmodel.token == token && vsmodel.expires > System.DateTime.Now)
                    {
                        //获取接口参数
                        int channel_id = DTRequest.GetFormIntValue("channel_id");
                        int news_id = DTRequest.GetFormInt("article_id");

                        string content = DTRequest.GetFormString("content");
                        string img_content = DTRequest.GetFormString("img_content");

                        int parent_id = DTRequest.GetFormInt("parent_id");
                        int user_id = DTRequest.GetFormInt("userid");
                        string user_ip = DTRequest.GetFormString("user_ip");

                        if (content.Length > 200 || content.Length < 1)
                        {
                            throw new Exception("评论内容格式有误");
                        }

                        BLL.article_comment bll = new BLL.article_comment();

                        Model.article_comment model = new Model.article_comment();
                        model.article_id = news_id;
                        model.channel_id = channel_id;
                        model.content = content;
                        model.is_lock = 0;
                        model.is_reply = 0;
                        model.site_id = 1;
                        model.user_id = user_id;
                        model.user_ip = user_ip;
                        model.add_time = System.DateTime.Now;
                        model.parent_id = parent_id;
                        model.img_content = img_content;
                        var r = bll.Add(model);
                        api.Models.JsonResult<int> res = new api.Models.JsonResult<int>();
                        res.ResultCode = 1;
                        res.ResultCount = 1;
                        context.Response.ContentType = "application/json";
                        context.Response.Write(JsonConvert.SerializeObject(res));
                        context.Response.End();
                    }
                    else
                    {

                        api.Models.JsonResult<int> res = new api.Models.JsonResult<int>();
                        res.ResultCode = 0;
                        res.ResultMessage = "登录超时或未登录";
                        context.Response.ContentType = "application/json";
                        context.Response.Write(JsonConvert.SerializeObject(res));
                        context.Response.End();
                    }
                }
            }
            else
            {
                context.Response.ContentType = "application/json";
                api.Models.JsonResult<int> res = new api.Models.JsonResult<int>();
                res.ResultCode = 0;
                res.ResultMessage = "未登录";
                context.Response.Write(JsonConvert.SerializeObject(res));
                context.Response.End();
            }
            #endregion
        }


        private List<article_comment> DataTabletoModel(DataSet ds)
        {
            List<article_comment> list = new List<article_comment>();
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    article_comment article_comment = new article_comment();
                    article_comment.id = int.Parse(ds.Tables[0].Rows[i]["id"].ToString());
                    article_comment.content = ds.Tables[0].Rows[i]["content"].ToString();
                    article_comment.add_time = DateTime.Parse(ds.Tables[0].Rows[i]["add_time"].ToString());
                    article_comment.parent_id = int.Parse(ds.Tables[0].Rows[i]["parent_id"].ToString());
                    article_comment.user_id = int.Parse(ds.Tables[0].Rows[i]["user_id"].ToString());
                    article_comment.article_id = int.Parse(ds.Tables[0].Rows[i]["article_id"].ToString());
                    article_comment.channel_id = int.Parse(ds.Tables[0].Rows[i]["channel_id"].ToString());
                    article_comment.img_content = ds.Tables[0].Rows[i]["img_content"].ToString();

                    list.Add(article_comment);
                }

            }

            return list;
        }
        private string GetCommentImg(int commentid, int channel_id)
        {
            var list = bllcom.GetModelList(" article_comment_id=" + commentid + " and channel_id=" + channel_id);
            if (list != null && list.Count > 0)
            {
                return list[0].original_path;
            }
            else
            {
                return "";
            }
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}