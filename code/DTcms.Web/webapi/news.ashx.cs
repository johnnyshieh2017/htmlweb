﻿using DTcms.Common;
using DTcms.Model;
using DTcms.Web.api.Models;
using DTcms.Web.cmsapi.Models;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace DTcms.Web.webapi
{
    /// <summary>
    /// news 的摘要说明
    /// </summary>
    public class news : IHttpHandler
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        BLL.article_comment comment_bll = new BLL.article_comment();
        public void ProcessRequest(HttpContext context)
        {
            //取得处事类型
            string action = DTRequest.GetQueryString("action");

            switch (action)
            {
                case "get": // 获取信息
                    get(context);
                    break;

                case "create"://发布信息
                    create(context);
                    break;
                case "like":
                    givelike(context);
                    break;
            }
        }

        private void get(HttpContext context)
        {
                cmsapi.Models.JsonResult<channel_article_news> res = new cmsapi.Models.JsonResult<channel_article_news>();
                int channel_id = DTRequest.GetFormIntValue("channel_id");
                int category_id = DTRequest.GetFormIntValue("category_id");
                int article_id = DTRequest.GetFormIntValue("article_id");
                int pagesize = DTRequest.GetFormIntValue("pagesize",10);
                int page = DTRequest.GetFormIntValue("page",1);
                int ordertype = DTRequest.GetFormIntValue("ordertype");
                int doctorid = DTRequest.GetFormIntValue("doctorid");
                int dpid= DTRequest.GetFormIntValue("dpid");
                int record_count = 0;

                string keywords = DTRequest.GetFormString("keywords");


            //频道扩展字段 参数
            // userid
            //source
            //author 
            // 如需 在频道增加 hospital department 字段

            BLL.article articlebll = new BLL.article();
            BLL.article_comment commentbll = new BLL.article_comment();
            BLL.article_category catebll = new BLL.article_category();
            string strorder = " add_time desc ";
            if (ordertype == 1)
            {
                strorder = " sort_id desc ";
            }
            else if (ordertype == 2)
            {
                strorder = " sort_id asc ";
            }
            
            string strwhere = " status!=2 ";
            if (article_id != 0)
            {
                strwhere += " and id="+article_id;
            }
            if (keywords.Trim() != "" && keywords.Trim().Length < 10)
            {
                strwhere += " and (title like '%" + keywords.Trim() + "%' or zhaiyao like '%" + keywords.Trim() + "%')";
            }

            Dictionary<string, string> cdic = SetFieldValues(channel_id);
            if (doctorid != 0)
            {
               
                if (cdic.ContainsKey("userid"))
                {
                    strwhere += " and userid="+doctorid;
                }
                
            }
            if (dpid != 0)
            {
                if (cdic.ContainsKey("department_category"))
                {
                    strwhere += " and department_category='"+dpid+"'";
                }
            }



            var listdataset = articlebll.GetList(channel_id, category_id, pagesize, page, strwhere, strorder, out record_count);
            
            List<channel_article_news> modellist = new List<channel_article_news>();
            if (listdataset != null && listdataset.Tables.Count > 0)
            {
                var ppage = Math.Ceiling(Convert.ToDouble( record_count)/Convert.ToDouble(pagesize));
                if (page <= ppage)
                {
                    var list = listdataset.Tables[0];
                    for (int i = 0; i < list.Rows.Count; i++)
                    {
                        channel_article_news model = new channel_article_news();
                        model.id = int.Parse(list.Rows[i]["id"].ToString());
                        model.channel_id = int.Parse(list.Rows[i]["channel_id"].ToString());
                        model.category_id = int.Parse(list.Rows[i]["category_id"].ToString());
                        model.title = list.Rows[i]["title"].ToString();
                        model.link_url = list.Rows[i]["link_url"].ToString();
                        model.img_url = list.Rows[i]["img_url"].ToString();
                        model.content = list.Rows[i]["content"].ToString();
                        model.sort_id = int.Parse(list.Rows[i]["sort_id"].ToString());
                        model.status = int.Parse(list.Rows[i]["status"].ToString());
                        model.add_time = DateTime.Parse(list.Rows[i]["add_time"].ToString());
                        model.zhaiyao = list.Rows[i]["zhaiyao"].ToString();
                        model.click = int.Parse(list.Rows[i]["click"].ToString());

                        model.comment = commentbll.GetCount("channel_id=" + model.channel_id + " and article_id=" + model.id);
                        model.albums = articlebll.getalbums(channel_id, model.id);
                        model.is_top = int.Parse(list.Rows[i]["is_top"].ToString());
                        model.is_red = int.Parse(list.Rows[i]["is_red"].ToString());
                        model.is_hot = int.Parse(list.Rows[i]["is_hot"].ToString());
                        model.like_count = int.Parse(list.Rows[i]["like_count"].ToString());

                        #region 扩展字段信息===================
                        Dictionary<string, string> fieldDic = new Dictionary<string, string>();
                        fieldDic.Add("hospital", "");
                        fieldDic.Add("doctitle", "");
                        fieldDic.Add("author_img", "");
                        fieldDic.Add("source", "");
                        fieldDic.Add("author", "");
                        fieldDic.Add("userid", "");
                        fieldDic.Add("like_count", "");

                        fieldDic.Add("guests_introduction", "");
                        fieldDic.Add("company_introduction", "");
                        fieldDic.Add("department_category", "");
                        fieldDic.Add("company", "");
                        fieldDic.Add("open_indexpage", "");
                        fieldDic.Add("live_sign", "");

                        if (list.Rows[i].Table.Columns.Contains("hospital"))
                        {
                            fieldDic["hospital"] = list.Rows[i]["hospital"].ToString();
                        }
                        if (list.Rows[i].Table.Columns.Contains("doctitle"))
                        {
                            fieldDic["doctitle"] = list.Rows[i]["doctitle"].ToString();
                        }
                        if (list.Rows[i].Table.Columns.Contains("author_img"))
                        {
                            fieldDic["author_img"] = list.Rows[i]["author_img"].ToString();
                        }

                        if (list.Rows[i].Table.Columns.Contains("source"))
                        {
                            fieldDic["source"] = list.Rows[i]["source"].ToString();
                        }
                        if (list.Rows[i].Table.Columns.Contains("author"))
                        {
                            fieldDic["author"] = list.Rows[i]["author"].ToString();
                        }
                        if (list.Rows[i].Table.Columns.Contains("userid"))
                        {
                            fieldDic["userid"] = list.Rows[i]["userid"].ToString();
                        }
                        //if (list.Rows[i].Table.Columns.Contains("like_count"))
                        //{
                        //    fieldDic["like_count"] = list.Rows[i]["like_count"].ToString();
                        //}



                        if (list.Rows[i].Table.Columns.Contains("guests_introduction"))
                        {
                            fieldDic["guests_introduction"] = list.Rows[i]["guests_introduction"].ToString();
                        }
                        if (list.Rows[i].Table.Columns.Contains("company_introduction"))
                        {
                            fieldDic["company_introduction"] = list.Rows[i]["company_introduction"].ToString();
                        }
                        if (list.Rows[i].Table.Columns.Contains("department_category"))
                        {
                            fieldDic["department_category"] = list.Rows[i]["department_category"].ToString();
                        }
                        if (list.Rows[i].Table.Columns.Contains("company"))
                        {
                            fieldDic["company"] = list.Rows[i]["company"].ToString();
                        }
                        if (list.Rows[i].Table.Columns.Contains("open_indexpage"))
                        {
                            fieldDic["open_indexpage"] = list.Rows[i]["open_indexpage"].ToString();
                        }
                        if (list.Rows[i].Table.Columns.Contains("live_sign"))
                        {
                            fieldDic["live_sign"] = list.Rows[i]["live_sign"].ToString();
                        }
                        model.fields = fieldDic;
                        #endregion

                        var comment_count = 0;
                        model.commentlist = commentlist(int.Parse(list.Rows[i]["id"].ToString()), int.Parse(list.Rows[i]["channel_id"].ToString()), out comment_count);
                        
                        modellist.Add(model);
                        
                    }
                }

                if (article_id != 0)
                {
                    articlebll.updateclick(channel_id, article_id);

                }
               
            }

            if (category_id != 0)
            {
                var cate = catebll.GetModel(category_id);
                if (cate != null)
                {
                    Dictionary<string, string> ext = new Dictionary<string, string>();
                    ext.Add("category", cate.title);
                    res.ResultExt = ext;
                }
            }
            res.ResultCount = modellist.Count;
            res.ResultCode = 1;
            res.ResultMessage = "成功";
            res.JsonEntity = modellist;

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }


        private List<Model.article_comment_ext> commentlist(int news_id,int channel_id,out int record_count)
        {

            string strWhere = " article_id=" + news_id + " and channel_id=" + channel_id + " and  parent_id=0";
            string filedOrder = " add_time asc";

            DataSet list = comment_bll.GetList(50, 1, strWhere, filedOrder, out record_count);
            List<article_comment_ext> listmodel = new List<article_comment_ext>();
            if (list != null && list.Tables[0] != null)
            {
               
                for (int i = 0; i < list.Tables[0].Rows.Count; i++)
                {
                    article_comment_ext article_comment = new article_comment_ext();
                    article_comment.id = int.Parse(list.Tables[0].Rows[i]["id"].ToString());
                    article_comment.content = list.Tables[0].Rows[i]["content"].ToString();
                    article_comment.add_time = DateTime.Parse(list.Tables[0].Rows[i]["add_time"].ToString());
                    article_comment.parent_id = int.Parse(list.Tables[0].Rows[i]["parent_id"].ToString());
                    article_comment.user_id = int.Parse(list.Tables[0].Rows[i]["user_id"].ToString());
                    article_comment.article_id = int.Parse(list.Tables[0].Rows[i]["article_id"].ToString());
                    article_comment.channel_id = int.Parse(list.Tables[0].Rows[i]["channel_id"].ToString());
                    article_comment.img_content = list.Tables[0].Rows[i]["img_content"].ToString(); 
                    //GetCommentImg(article_comment.id, article_comment.channel_id);
                    //article_comment.children = DataTabletoModel(comment_bll.GetList(0, " parent_id=" + article_comment.id, " add_time desc"));
                    listmodel.Add(article_comment);
                }
               
            }

            return listmodel;
        }


        private List<article_comment> DataTabletoModel(DataSet ds)
        {
            List<article_comment> list = new List<article_comment>();
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    article_comment article_comment = new article_comment();
                    article_comment.id = int.Parse(ds.Tables[0].Rows[i]["id"].ToString());
                    article_comment.content = ds.Tables[0].Rows[i]["content"].ToString();
                    article_comment.add_time = DateTime.Parse(ds.Tables[0].Rows[i]["add_time"].ToString());
                    article_comment.parent_id = int.Parse(ds.Tables[0].Rows[i]["parent_id"].ToString());
                    article_comment.user_id = int.Parse(ds.Tables[0].Rows[i]["user_id"].ToString());
                    article_comment.article_id = int.Parse(ds.Tables[0].Rows[i]["article_id"].ToString());
                    article_comment.channel_id = int.Parse(ds.Tables[0].Rows[i]["channel_id"].ToString());
                    article_comment.img_content = ds.Tables[0].Rows[i]["img_content"].ToString();

                    list.Add(article_comment);
                }

            }

            return list;
        }
        private string GetCommentImg(int commentid, int channel_id)
        {
            DTcms.BLL.comment_albums bllcom = new BLL.comment_albums();
            var list = bllcom.GetModelList(" article_comment_id=" + commentid + " and channel_id=" + channel_id);
            if (list != null && list.Count > 0)
            {
                return list[0].original_path;
            }
            else
            {
                return "";
            }
        }

        private void create(HttpContext context)
        {
            #region 首先验证登录
            string token = DTRequest.GetFormString("usertoken");
            string userid = DTRequest.GetFormString("userid");
            if (token != "" && userid != "")
            {
                if (DTRequest.GetFormString("content").Trim() != "")
                {
                    BLL.yjr_user_uservstoken vsbll = new BLL.yjr_user_uservstoken();
                    var vsmodel = vsbll.GetModel(int.Parse(userid));
                    if (vsmodel != null)
                    {
                        if (vsmodel.token == token && vsmodel.expires > System.DateTime.Now)
                        {
                            var usermodel = new BLL.dt_users().GetModel(int.Parse(userid));

                            #region 验证通过发布资讯
                            cmsapi.Models.JsonResult<channel_article_news> res = new cmsapi.Models.JsonResult<channel_article_news>();
                            int channel_id = DTRequest.GetFormIntValue("channel_id");
                            int category_id = DTRequest.GetFormIntValue("category_id");
                            string title = DTRequest.GetFormString("title");
                            string content = DTRequest.GetFormString("content");
                            string[] albumArr = DTRequest.GetFormString("album").Split(',');

                            Model.article model = new Model.article();
                            BLL.article bll = new BLL.article();
                            Model.site_channel channel = new BLL.site_channel().GetModel(channel_id);

                            model.site_id = channel.site_id;
                            model.channel_id = channel.id;
                            model.category_id = category_id;
                            model.title = title;
                            model.content = content;
                            

                            model.fields = SetFieldValues(channel_id);// 保存扩展字段

                            if (model.fields.Keys.Contains("author_img"))
                            {
                                model.fields["author_img"] = usermodel!=null? usermodel.avatar:"";
                                
                            }
                            if (model.fields.Keys.Contains("author"))
                            {
                                model.fields["author"] = usermodel != null ? usermodel.nick_name : "";
                            }

                            List<Model.article_albums> ls = new List<Model.article_albums>();
                            if (albumArr != null && albumArr.Length > 0)
                            {
                                for (int i = 0; i < albumArr.Length; i++)
                                {
                                    if (albumArr[i].Length > 0)
                                    {
                                        ls.Add(new Model.article_albums { channel_id = channel_id, original_path = albumArr[i], thumb_path = albumArr[i] });
                                    }
                                }
                            }

                            model.albums = ls;

                            if (bll.Add(model) > 0)
                            {
                                res.ResultCount = 1;
                                res.ResultCode = 1;
                                res.ResultMessage = "sucess";
                            }
                            else
                            {
                                res.ResultCount = 0;
                                res.ResultCode = 0;
                                res.ResultMessage = "fails";
                            }

                            context.Response.ContentType = "application/json";
                            context.Response.Write(JsonConvert.SerializeObject(res));
                            context.Response.End();
                            #endregion
                        }
                        else
                        {
                            context.Response.ContentType = "application/json";
                            api.Models.JsonResult<int> res = new api.Models.JsonResult<int>();
                            res.ResultCode = 0;
                            res.ResultMessage = "登录超时或未登录";
                            context.Response.Write(JsonConvert.SerializeObject(res));
                            context.Response.End();
                        }
                    }
                }
                else
                {
                    context.Response.ContentType = "application/json";
                    api.Models.JsonResult<int> res = new api.Models.JsonResult<int>();
                    res.ResultCode = 0;
                    res.ResultMessage = "内容不能为空！";
                    context.Response.Write(JsonConvert.SerializeObject(res));
                    context.Response.End();
                }
            }
            else
            {
                context.Response.ContentType = "application/json";
                api.Models.JsonResult<int> res = new api.Models.JsonResult<int>();
                res.ResultCode = 0;
                res.ResultMessage = "未登录";
                context.Response.Write(JsonConvert.SerializeObject(res));
                context.Response.End();
            }
            #endregion
        }

        private Dictionary<string, string> SetFieldValues(int _channel_id)
        {
            DataTable dt = new BLL.article_attribute_field().GetList(_channel_id, "").Tables[0];
            Dictionary<string, string> dic = new Dictionary<string, string>();
            foreach (DataRow dr in dt.Rows)
            {
                dic.Add(dr["name"].ToString(), DTRequest.GetFormString(dr["name"].ToString()));
                
            }

            return dic;
        }


        /// <summary>
        /// 互动内容点赞
        /// </summary>
        /// <param name="context"></param>
        private void givelike(HttpContext context)
        {
            #region 验证登录==点赞
            string token = DTRequest.GetFormString("usertoken");
            string userid = DTRequest.GetFormString("userid");
            if (token != "" && userid != "")
            {
                BLL.yjr_user_uservstoken vsbll = new BLL.yjr_user_uservstoken();
                var vsmodel = vsbll.GetModel(int.Parse(userid));
                if (vsmodel != null)
                {
                    if (vsmodel.token == token && vsmodel.expires > System.DateTime.Now)
                    {
                        //获取接口参数
                        int channel_id = DTRequest.GetFormIntValue("channel_id");
                        int ac = DTRequest.GetFormIntValue("ac",1);//点赞或点踩
                        int article_id = DTRequest.GetFormIntValue("article_id");
                        BLL.article articlebll = new BLL.article();
                        api.Models.JsonResult<int> res = new api.Models.JsonResult<int>();
                        var art = articlebll.GetModel(channel_id, article_id);
                        if (art != null)
                        {
                            art.like_count = art.like_count + ac;
                            int r = articlebll.Update(art)==true?1:0;
                            res.ResultCode = r;
                            res.ResultCount = r;
                            res.ResultMessage = r==1?"sucess":"fails";
                        }
                        else
                        {
                            res.ResultCode = 0;
                            res.ResultCount = 0;
                            res.ResultMessage = "信息不存在";
                        }

                                             
                        context.Response.ContentType = "application/json";
                        context.Response.Write(JsonConvert.SerializeObject(res));
                        context.Response.End();
                    }
                    else
                    {
                        
                        api.Models.JsonResult<int> res = new api.Models.JsonResult<int>();
                        res.ResultCode = 0;
                        res.ResultMessage = "登录超时或未登录";
                        context.Response.ContentType = "application/json";
                        context.Response.Write(JsonConvert.SerializeObject(res));
                        context.Response.End();
                    }
                }
            }
            else
            {
                context.Response.ContentType = "application/json";
                api.Models.JsonResult<int> res = new api.Models.JsonResult<int>();
                res.ResultCode = 0;
                res.ResultMessage = "未登录";
                context.Response.Write(JsonConvert.SerializeObject(res));
                context.Response.End();
            }
            #endregion
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}