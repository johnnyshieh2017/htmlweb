﻿using DTcms.Common;
using DTcms.Model;
using DTcms.Web.cmsapi.Models;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace DTcms.Web.webapi
{
    /// <summary>
    /// doctorbusiness 的摘要说明
    /// </summary>
    public class doctorbusiness : IHttpHandler
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public void ProcessRequest(HttpContext context)
        {
            //取得处事类型
            string action = DTRequest.GetQueryString("action");


            switch (action)
            {
                case "get": // 获取医生业务
                    get(context);
                    break;
                case "create"://创建医生业务服务
                    create(context);
                    break;
                case "delete"://更新医生业务服务
                    delete(context);
                    break;
            }
        }

        private void create(HttpContext context)
        {
            JsonResult<string> res = new JsonResult<string>();
            int docid = DTRequest.GetFormInt("doctorid");
            int bid = DTRequest.GetFormInt("bid");//businessid
            //int ctype = DTRequest.GetFormInt("ctype"); //cost_type
            //int cost_interval = DTRequest.GetFormInt("interval");


            #region 登录验证

            string token = DTRequest.GetFormString("usertoken");
            string userid = DTRequest.GetFormString("userid");

            //logger.Debug("userauthority_token:" + token);
            //logger.Debug("userauthority_userid:" + userid);
            //logger.Debug("userauthority_action:" + DTRequest.GetQueryString("action"));

            if (token != "" && userid != "")
            {
                BLL.yjr_user_uservstoken bll_uvt = new BLL.yjr_user_uservstoken();
                var model = bll_uvt.GetModel(int.Parse(userid));
                if (model != null)
                {
                    if (model.token == token && model.expires > System.DateTime.Now)
                    {

                    }
                    else
                    {
                        context.Response.ContentType = "application/json";

                        res.ResultCode = 0;
                        res.ResultMessage = "登录超时或未登录";
                        context.Response.Write(JsonConvert.SerializeObject(res));
                        context.Response.End();
                    }
                }
            }
            else
            {
                context.Response.ContentType = "application/json";

                res.ResultCode = 0;
                res.ResultMessage = "未登录";
                context.Response.Write(JsonConvert.SerializeObject(res));
                context.Response.End();
            }

            #endregion


            BLL.yjr_zx_business business = new BLL.yjr_zx_business();
            if (business.Exists(bid))
            {
                BLL.yjr_doctor_vsbusiness vsbusiness = new BLL.yjr_doctor_vsbusiness();
                if (!vsbusiness.Exists(docid, bid))
                {
                    Model.yjr_doctor_vsbusiness model = new yjr_doctor_vsbusiness();
                    model.doctorid = docid;
                    model.businessid = bid;
                    model.add_time = System.DateTime.Now;
                    model.status = 1;
                    vsbusiness.Add(model);

                    res.JsonEntity = null;
                    res.ResultCode = 1;
                    res.ResultCount = 1;
                    res.ResultMessage = "sucess";
                }
                else
                {
                    res.ResultCode = 0;
                    res.ResultCount = 0;
                    res.ResultMessage = "已存在";
                }
            }
            else
            {
                res.ResultCode = 0;
                res.ResultCount = 0;
                res.ResultMessage = "businessid有误";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }

        private void delete(HttpContext context)
        {
            JsonResult<string> res = new JsonResult<string>();
            int docid = DTRequest.GetFormInt("doctorid");
            int bid = DTRequest.GetFormInt("bid");//businessid


            #region 登录验证

            string token = DTRequest.GetFormString("usertoken");
            string userid = DTRequest.GetFormString("userid");

            //logger.Debug("userauthority_token:" + token);
            //logger.Debug("userauthority_userid:" + userid);
            //logger.Debug("userauthority_action:" + DTRequest.GetQueryString("action"));

            if (token != "" && userid != "")
            {
                BLL.yjr_user_uservstoken bll_uvt = new BLL.yjr_user_uservstoken();
                var model = bll_uvt.GetModel(int.Parse(userid));
                if (model != null)
                {
                    if (model.token == token && model.expires > System.DateTime.Now)
                    {

                    }
                    else
                    {
                        context.Response.ContentType = "application/json";

                        res.ResultCode = 0;
                        res.ResultMessage = "登录超时或未登录";
                        context.Response.Write(JsonConvert.SerializeObject(res));
                        context.Response.End();
                    }
                }
            }
            else
            {
                context.Response.ContentType = "application/json";

                res.ResultCode = 0;
                res.ResultMessage = "未登录";
                context.Response.Write(JsonConvert.SerializeObject(res));
                context.Response.End();
            }

            #endregion


            BLL.yjr_doctor_vsbusiness vsbusiness = new BLL.yjr_doctor_vsbusiness();
            if (vsbusiness.Delete(docid, bid))
            {
                res.ResultCode = 1;
                res.ResultCount = 1;
                res.ResultMessage = "sucess";
            }
            else
            {
                res.ResultCode = 0;
                res.ResultCount = 0;
                res.ResultMessage = "fails";
            }
            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }

        private void get(HttpContext context)
        {
            JsonResult<Model.yjr_doctor_vsbusiness> res = new JsonResult<Model.yjr_doctor_vsbusiness>();
            int docid = DTRequest.GetFormInt("doctorid");
            int bid = DTRequest.GetFormInt("bid");

            


            BLL.yjr_doctor_vsbusiness vsbusiness = new BLL.yjr_doctor_vsbusiness();
            if (bid != 0)
            {
                var entity = vsbusiness.GetModel(docid, bid);
                if (entity != null)
                {
                    res.JsonEntity = new List<yjr_doctor_vsbusiness>() { entity };
                    res.ResultCode = 1;
                    res.ResultCount = 1;
                    res.ResultMessage = "sucess";
                }
                else
                {
                    res.JsonEntity = new List<yjr_doctor_vsbusiness>() { entity };
                    res.ResultCode = 0;
                    res.ResultCount = 0;
                    res.ResultMessage = "sucess";
                }

            }
            else
            {
                var list = vsbusiness.GetModelList(" doctorid=" + docid);

                res.JsonEntity = list;
                res.ResultCode = 1;
                res.ResultCount = list.Count;
                res.ResultMessage = "sucess";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}