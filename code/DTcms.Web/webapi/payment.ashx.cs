﻿using DTcms.API.Payment.wxpay;
using DTcms.Common;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DTcms.Web.webapi
{
    /// <summary>
    /// payment 的摘要说明
    /// </summary>
    public class payment : userauthority,IHttpHandler
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public void ProcessRequest(HttpContext context)
        {
            //JavaScript调用getBrandWCPayRequest接口

            //1、获取openid和access_token ，openid用户统一下单接口、access_token用户获取共享地址（可选）
            //2、通过统一下单接口 产生微信预支付订单得到订单id
            //3、组织微信浏览器调起jsapi支付所需的参数


            string action = DTRequest.GetQueryString("action");
            switch (action)
            {
                case "getcodeurl": // 获取信息
                    getcodeurl(context);
                    break;
                case "get_jsapi_param": // 获取信息
                    get_jsapi_param(context);
                    break;
                case "get_openid": // 获取信息
                    get_openid(context);
                    break;
            }
        }

        private void getcodeurl(HttpContext context)
        {
            api.Models.JsonResult<string> res = new api.Models.JsonResult<string>();
            BLL.yjr_zx_order bll = new BLL.yjr_zx_order();

            int id = DTRequest.GetFormInt("id");
            int amount = DTRequest.GetFormInt("amount");
            int site_payment_id = DTRequest.GetFormInt("site_payment_id");
            var model = bll.GetModel(id);
            if (model != null && model.pay_status == 0 && model.status == 0)
            {
                if (model.service_fee == amount)
                {
                    string url_param = "&";
                    string zx_order_payment_Redirect_url = System.Configuration.ConfigurationManager.AppSettings["zx_order_payment_Redirect_url"];

                    JsApiConfig jsApiConfig = new JsApiConfig(site_payment_id);
                    WxPayData data = new WxPayData();
                    data.SetValue("appid", jsApiConfig.AppId);
                    data.SetValue("redirect_uri", HttpUtility.UrlEncode(zx_order_payment_Redirect_url + url_param));
                    data.SetValue("response_type", "code");
                    data.SetValue("scope", "snsapi_base");
                    data.SetValue("state", id + "#wechat_redirect"); //传入订单号
                    string url = "https://open.weixin.qq.com/connect/oauth2/authorize?" + data.ToUrl();

                    res.JsonEntity = new List<string>() { url };
                    res.ResultCode = 1;
                    res.ResultCount = 1;
                    res.ResultMessage = "sucess!";

                }
                else
                {
                    res.ResultCode = 0;
                    res.ResultCount = 0;
                    res.ResultMessage = "金额有误!";
                }
            }
            else
            {
                res.ResultCode = 0;
                res.ResultCount = 0;
                res.ResultMessage = "订单不存在或已删除!";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }

        private void get_openid(HttpContext context)
        {

            string code = DTRequest.GetFormString("code");
            string appid = "";
            string secret = "";
            WxPayData data = new WxPayData();
            data.SetValue("appid", appid);
            data.SetValue("secret", secret);
            data.SetValue("code", code);
            data.SetValue("grant_type", "authorization_code");
            string url = "https://api.weixin.qq.com/sns/oauth2/access_token?" + data.ToUrl();

            //请求url以获取数据
            string result = HttpService.Get(url);
            Dictionary<string, object> dic = JsonHelper.DataRowFromJSON(result);
            string openid = (string)dic["openid"];
            
            Dictionary<string, string> ext = new Dictionary<string, string>();
            ext.Add("openid", openid);
            api.Models.JsonResult<string> res = new api.Models.JsonResult<string>();
            res.JsonEntity = new List<string>() { openid };
            res.ResultCode = 1;
            res.ResultCount = 1;
            res.ResultMessage = "sucess";
            res.ResultExt = ext;

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }
        private void get_jsapi_param(HttpContext context)
        {

            int site_payment_id = 4;// DTRequest.GetFormInt("site_payment_id");
                                    //string openid = DTRequest.GetFormString("openid");
                                    //int orderid= DTRequest.GetFormInt("orderid");
                                    //int userid = DTRequest.GetFormInt("userid");
            int orderid = DTRequest.GetFormInt("orderid");
            int userid = DTRequest.GetFormInt("userid");


            //string appid = "";
            //string secret = "";
            //WxPayData paydata = new WxPayData();
            //paydata.SetValue("appid", appid);
            //paydata.SetValue("secret", secret);
            //paydata.SetValue("code", code);
            //paydata.SetValue("grant_type", "authorization_code");
            //string url = "https://api.weixin.qq.com/sns/oauth2/access_token?" + paydata.ToUrl();

            ////请求url以获取数据
            //string result = HttpService.Get(url);
            //Dictionary<string, object> dic = JsonHelper.DataRowFromJSON(result);
            //string openid = (string)dic["openid"];
            api.Models.JsonResult<string> res = new api.Models.JsonResult<string>();

            try
            {
                var order_model = new BLL.yjr_zx_order().GetModelList("id=" + orderid).FirstOrDefault();

                var uservsopenid = new BLL.yjr_user_uservsopenid().GetModel(userid);

                if (order_model != null&&order_model.status==0&&uservsopenid!=null)
                {


                    //统一下单
                    string sendUrl = "https://api.mch.weixin.qq.com/pay/unifiedorder";
                    JsApiConfig jsApiConfig = new JsApiConfig(site_payment_id);
                    WxPayData data = new WxPayData();
                    data.SetValue("body", order_model.business_name); //商品描述
                    data.SetValue("detail", order_model.business_name); //商品详情
                    data.SetValue("out_trade_no", order_model.id.ToString()); //商户订单号
                    data.SetValue("total_fee", order_model.service_fee); //订单总金额,以分为单位
                    data.SetValue("trade_type", "JSAPI"); //交易类型
                    data.SetValue("openid", uservsopenid.openid); //公众账号ID
                    data.SetValue("appid", jsApiConfig.AppId); //公众账号ID
                    data.SetValue("mch_id", jsApiConfig.Partner); //商户号
                    data.SetValue("nonce_str", JsApiPay.GenerateNonceStr()); //随机字符串
                    data.SetValue("notify_url", get_notify_url(site_payment_id)); //异步通知url// jsApiConfig.Notify_url
                    data.SetValue("spbill_create_ip", DTRequest.GetIP()); //终端IP
                    data.SetValue("sign", data.MakeSign(jsApiConfig.Key)); //签名
                    string xml = data.ToXml(); //转换成XML
                    var startTime = DateTime.Now; //开始时间
                    string response = HttpService.Post(xml, sendUrl, false, 6); //发送请求
                    var endTime = DateTime.Now; //结束时间
                    int timeCost = (int)((endTime - startTime).TotalMilliseconds); //计算所用时间
                    WxPayData wxpayresult = new WxPayData();
                    wxpayresult.FromXml(response, jsApiConfig.Key);
                    JsApiPay.ReportCostTime(site_payment_id, sendUrl, timeCost, wxpayresult); //测速上报

                    //获取H5调起JS API参数
                    WxPayData jsApiParam = new WxPayData();
                    jsApiParam.SetValue("appId", wxpayresult.GetValue("appid"));
                    jsApiParam.SetValue("timeStamp", JsApiPay.GenerateTimeStamp());
                    jsApiParam.SetValue("nonceStr", JsApiPay.GenerateNonceStr());
                    jsApiParam.SetValue("package", "prepay_id=" + wxpayresult.GetValue("prepay_id"));
                    jsApiParam.SetValue("signType", "MD5");
                    jsApiParam.SetValue("paySign", jsApiParam.MakeSign(jsApiConfig.Key));
                    //wxJsApiParam = jsApiParam.ToJson();

                    Dictionary<string, string> ext = new Dictionary<string, string>();
                    ext.Add("wxJsApiParam", jsApiParam.ToJson());

                    res.ResultExt = ext;
                    res.ResultCode = 1;
                    res.ResultCount = 1;
                    res.ResultMessage = "sucess";
                    context.Response.ContentType = "application/json";
                    context.Response.Write(JsonConvert.SerializeObject(res));
                    context.Response.End();
                    //支付成功后跳转的URL
                    //returnUrl = new Web.UI.BasePage().linkurl("payment", "?action=succeed&order_no=" + order_no);
                }
                else
                {
                    res.ResultCode = 0;
                    res.ResultCount = 0;
                    res.ResultMessage = "订单信息有误";
                    context.Response.ContentType = "application/json";
                    context.Response.Write(JsonConvert.SerializeObject(res));
                    context.Response.End();
                }
            }
            catch (Exception ex)
            {
                Dictionary<string, string> ext = new Dictionary<string, string>();
               
                //api.Models.JsonResult<string> res = new api.Models.JsonResult<string>();
                //res.ResultCode = 0;
                //res.ResultCount = 0;
                //res.ResultMessage = ex.Message;// "";
                //context.Response.ContentType = "application/json";
                //context.Response.Write(JsonConvert.SerializeObject(res));
                //context.Response.End();
                // Response.Redirect(new Web.UI.BasePage().linkurl("error", "?msg=" + Utils.UrlEncode("调用JSAPI下单失败，请检查微信授权目录是否已注册！")));
                return;
            }

        }

        private string get_notify_url(int site_payment_id)
        {
            string redirect_url = string.Empty;
            string notify_url = string.Empty;

            Model.site_payment model = new BLL.site_payment().GetModel(site_payment_id); //站点支付方式
            //Model.payment payModel = new BLL.payment().GetModel(model.payment_id); //支付平台
            Model.sites siteModel = new BLL.sites().GetModel(model.site_id); //站点配置
            Model.sysconfig sysConfig = new BLL.sysconfig().loadConfig(); //系统配置

            //获取用户的OPENID回调地址及登录后的回调地址
            if (!string.IsNullOrEmpty(siteModel.domain.Trim()) && siteModel.is_default == 0) //如果有自定义域名且不是默认站点
            {
                redirect_url = "http://" + siteModel.domain + "/webapi/wx_redirect_url.aspx"; //获取用户的OPENID回调地址
                notify_url = "http://" + siteModel.domain + "/webapi/wx_notify_url.aspx"; //登录后的回调地址
            }
            else if (siteModel.is_default == 0) //不是默认站点也没有绑定域名
            {
                redirect_url = "http://" + HttpContext.Current.Request.Url.Authority.ToLower() + sysConfig.webpath + siteModel.build_path.ToLower()  +"/webapi/wx_redirect_url.aspx"; ;
                notify_url = "http://" + HttpContext.Current.Request.Url.Authority.ToLower() + sysConfig.webpath + siteModel.build_path.ToLower() + "/webapi/wx_notify_url.aspx";
            }
            else //否则使用当前域名
            {
                redirect_url = "http://" + HttpContext.Current.Request.Url.Authority.ToLower() + sysConfig.webpath + "/webapi/wx_redirect_url.aspx"; ;
                notify_url = "http://" + HttpContext.Current.Request.Url.Authority.ToLower() + sysConfig.webpath + "/webapi/wx_notify_url.aspx";
            }

            logger.Debug("统一下单，指定通知地址：" + notify_url);

            return notify_url;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}