﻿using DTcms.Common;
using DTcms.Web.cmsapi.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DTcms.Web.webapi
{
    /// <summary>
    /// commonm 的摘要说明
    /// </summary>
    public class commonm : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            //取得处事类型
            string action = DTRequest.GetQueryString("action");


            switch (action)
            {
                case "get_hospital": // 获取所有医院信息
                    get_hospital(context);
                    break;
                case "get_hospital_list":
                    get_hospital_list(context);
                    break;
                case "get_department":
                    get_department(context);
                    break;
                case "get_department_list":
                    get_department_list(context);
                    break;
                case "get_title_list":
                    get_title_list(context);
                    break;
                case "getlist":
                    get_list(context);
                    break;
            }

        }
        #region     医院科室
        private void get_hospital_list(HttpContext context)
        {
            JsonResult<Model.yjr_doc_hospital> res = new JsonResult<Model.yjr_doc_hospital>();
            BLL.yjr_doc_hospital bll = new BLL.yjr_doc_hospital();
            var list = bll.GetModelList("");

            res.JsonEntity = list;
            res.ResultCode = 1;
            res.ResultCount = list.Count;

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();

        }

        private void get_hospital(HttpContext context)
        {
            int id = DTcms.Common.DTRequest.GetFormInt("hid");

            JsonResult<Model.yjr_doc_hospital> res = new JsonResult<Model.yjr_doc_hospital>();
            BLL.yjr_doc_hospital bll = new BLL.yjr_doc_hospital();
            var list = bll.GetModel(id);

            res.JsonEntity = new List<Model.yjr_doc_hospital> { list };
            res.ResultCode = 1;
            res.ResultCount = 1;

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();

        }

        private void get_department(HttpContext context)
        {
            int id = DTcms.Common.DTRequest.GetFormInt("dpid");
            JsonResult<Model.yjr_doctor_group> res = new JsonResult<Model.yjr_doctor_group>();
            BLL.yjr_doctor_group bll = new BLL.yjr_doctor_group();
            var list = bll.GetModel(id);

            res.JsonEntity = new List<Model.yjr_doctor_group> { list };
            res.ResultCode = 1;
            res.ResultCount = 1;

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }

        private void get_department_list(HttpContext context)
        {
            int id = DTcms.Common.DTRequest.GetFormInt("hid");
            JsonResult<Model.yjr_doctor_group> res = new JsonResult<Model.yjr_doctor_group>();
            BLL.yjr_doctor_group bll = new BLL.yjr_doctor_group();

            string where = " hospatilid>0";
            if(id!=0)
            {

                where += " and hospatilid="+id;
            }
            var list = bll.GetModelList(where);

            res.JsonEntity = list;
            res.ResultCode = 1;
            res.ResultCount = 1;

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }
        #endregion

        #region 获取医生列表信息(含关键字检索、按医院id检索，科室id检索)
        private void get_list(HttpContext context)
        {
            JsonResult<Model.yjr_doctor> res = new JsonResult<Model.yjr_doctor>();

            int pagesize = DTcms.Common.DTRequest.GetFormIntValue("pagesize", 10);
            int page = DTRequest.GetFormIntValue("page", 1);
            int sort = DTRequest.GetFormIntValue("ordertype", 1);
            string keywords = DTRequest.GetFormString("keywords");
            int hid = DTRequest.GetFormInt("hid");
            int dpid = DTRequest.GetFormInt("dpid");

            string where = " status=1";
            if (hid != 0)
            {
                where += " and hospitalid=" + hid;
            }
            if (dpid != 0)
            {
                where += " and departmentid=" + dpid;
            }
            if (keywords.Trim() != "" && keywords.Trim().Length < 10)
            {
                where += " and (name like '%" + keywords.Trim() + "%' or hospitalname like '%" + keywords.Trim() + "%' or groupname like '%" + keywords.Trim() + "%' )";
            }
            string filedorder = sort == 1 ? "sort asc" : "sort desc";
            BLL.yjr_doctor bll = new BLL.yjr_doctor();
            int rowcount = 0;
            //int pagecount = 0;

            var list = bll.GetModelList(pagesize, page, where, filedorder, out rowcount);

            res.ResultCount = list.Count;
            res.ResultCode = 1;
            res.ResultMessage = "sucess";
            res.JsonEntity = list;

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }
        #endregion

        #region 其他字典配置

        private void get_title_list(HttpContext context)
        {
            JsonResult<Model.yjr_doctor_title> res = new JsonResult<Model.yjr_doctor_title>();
            BLL.yjr_doctor_title bll = new BLL.yjr_doctor_title();

            var list = bll.GetModelList("");

            res.JsonEntity = list;
            res.ResultCode = 1;
            res.ResultCount = 1;

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }

        private void get_page_tile(HttpContext context)
        {
            //JsonResult<Dictionary<string, string>> res = new JsonResult<Dictionary<string, string>>();
            //Dictionary<string, string> dic = new Dictionary<string, string>();
            //dic.Add("index_title", "");
        }

        #endregion

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}