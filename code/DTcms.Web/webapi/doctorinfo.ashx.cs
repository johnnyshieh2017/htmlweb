﻿using DTcms.Common;
using DTcms.Model;
using DTcms.Web.api.Models;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace DTcms.Web.webapi
{
    /// <summary>
    /// doctorinfo 的摘要说明
    /// </summary>
    public class doctorinfo : IHttpHandler
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public void ProcessRequest(HttpContext context)
        {
            //取得处事类型
            string action = DTRequest.GetQueryString("action");


            switch (action)
            {
                case "get": // 获取医生个人信息
                    get(context);
                    break;
               

            }
        }

        #region 获取医生用户信息
        private void get(HttpContext context)
        {
            JsonResult<Model.yjr_doctor> res = new JsonResult<Model.yjr_doctor>();
            int docid = DTRequest.GetFormInt("doctorid");

            BLL.yjr_doctor bll = new BLL.yjr_doctor();
            var doc = bll.GetModel(docid);
            if (doc != null)
            {
                doc.rate = "100%";
                doc.bookcount = new Random().Next(20, 50).ToString();
                doc.responsetime = "30";

                res.JsonEntity = new List<Model.yjr_doctor> { doc };
                res.ResultCode = 1;
                res.ResultCount = 1;
                res.ResultMessage = "sucess!";
            }
            else
            {
                res.ResultCode = 0;
                res.ResultCount = 0;
                res.ResultMessage = "fails!";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.ApplicationInstance.CompleteRequest();

        }
        #endregion

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}