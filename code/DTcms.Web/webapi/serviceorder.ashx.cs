﻿using DTcms.Common;
using DTcms.Web.api.UtilsTool;
using DTcms.Web.cmsapi.Models;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DTcms.Web.webapi
{
    /// <summary>
    /// serviceorder 的摘要说明
    /// </summary>
    public class serviceorder : userauthority,IHttpHandler
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public void ProcessRequest(HttpContext context)
        {
            //取得处事类型
            string action = DTRequest.GetQueryString("action");


            switch (action)
            {
                case "get": // 
                    get(context);
                    break;
                case "update"://
                    update(context);
                    break;
                case "create"://
                    create(context);
                    break;
                case "createfree":
                    createfree(context);
                    break;
                case "docget": // 
                    docget(context);
                    break;
                default:
                    response_error("action异常");
                    break;
            }
        }

        private void get(HttpContext context)
        {
            //
            int userid = DTcms.Common.DTRequest.GetFormInt("userid");
            int orderid = DTcms.Common.DTRequest.GetFormInt("orderid");
            int status = DTcms.Common.DTRequest.GetFormIntValue("status",-1);
            int pagesize = DTcms.Common.DTRequest.GetFormIntValue("pagesize", 10);
            int page = DTRequest.GetFormIntValue("page", 1);

            JsonResult<Model.View_zx_order> res = new JsonResult<Model.View_zx_order>();
            //BLL.yjr_zx_order bll = new BLL.yjr_zx_order();
            BLL.View_zx_order VBLL = new BLL.View_zx_order();

            string strwhere = " userid=" + userid+" ";
            string strorder = " createtime desc ";
            if (orderid != 0)
            {
                strwhere = " 1=1 ";
                strwhere += " and id="+orderid;
            }
            if (status != -1)
            {
                strwhere += " and status="+status;
            }

            int recordcount = 0;
            var list = VBLL.GetModelList(pagesize, page, strwhere, strorder, out recordcount);// bll.GetModelList(pagesize,page, strwhere, strorder,out recordcount);
            res.JsonEntity = list;
            res.ResultCode = 1;
            res.ResultCount = list.Count;
            res.ResultMessage = "sucess";

            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("recordcount", recordcount.ToString());
            res.ResultExt = dic;
            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }

        private void docget(HttpContext context)
        {
            //
            int doctor_id = DTcms.Common.DTRequest.GetFormInt("doctorid");
            int orderid = DTcms.Common.DTRequest.GetFormInt("orderid");
            int status = DTcms.Common.DTRequest.GetFormIntValue("status", -1);
            int pagesize = DTcms.Common.DTRequest.GetFormIntValue("pagesize", 10);
            int page = DTRequest.GetFormIntValue("page", 1);

            JsonResult<Model.View_zx_order> res = new JsonResult<Model.View_zx_order>();
            //BLL.yjr_zx_order bll = new BLL.yjr_zx_order();
            BLL.View_zx_order VBLL = new BLL.View_zx_order();

            string strwhere = " service_type!=-1 and doctor_id=" + doctor_id + " ";
            string strorder = " createtime desc ";
            if (orderid != 0)
            {
                strwhere += " and id=" + orderid;
            }
            if (status != -1)
            {
                strwhere += " and status=" + status;
            }

            int recordcount = 0;
            var list = VBLL.GetModelList(pagesize, page, strwhere, strorder, out recordcount);// bll.GetModelList(pagesize,page, strwhere, strorder,out recordcount);
            res.JsonEntity = list;
            res.ResultCode = 1;
            res.ResultCount = list.Count;
            res.ResultMessage = "sucess";

            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("recordcount", recordcount.ToString());
            res.ResultExt = dic;
            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }

        private void update(HttpContext context)
        {
            //

        }

        private void create(HttpContext context)
        {
            //
            int userid = DTcms.Common.DTRequest.GetFormInt("userid");
            int businessconfigid = DTcms.Common.DTRequest.GetFormInt("businessconfigid");
            int businessappointmentid= DTcms.Common.DTRequest.GetFormInt("businessappointmentid");
            int patientid = DTcms.Common.DTRequest.GetFormInt("patientid");
            int visited = DTcms.Common.DTRequest.GetFormInt("visited");
            string department = DTcms.Common.DTRequest.GetFormString("department");
            string hospital = DTcms.Common.DTRequest.GetFormString("hospital");
            string diagnosis_result = DTcms.Common.DTRequest.GetFormString("diagnosis");
            string content = DTcms.Common.DTRequest.GetFormString("content");
            string reportimg= DTcms.Common.DTRequest.GetFormString("reportimg");
            JsonResult<string> res = new JsonResult<string>();

          
            


                BLL.yjr_doctor_vsbusinessconfig config = new BLL.yjr_doctor_vsbusinessconfig();
            if (!config.Exists(businessconfigid))
            {
                response_error("服务不存在");

            }
            else
            {
                var config_model = config.GetModel(businessconfigid);
                if (businessappointmentid != 0)
                {
                    BLL.yjr_doctor_vsbusinessappointment appointment = new BLL.yjr_doctor_vsbusinessappointment();
                    if (!appointment.Exists(businessappointmentid))
                    {
                        response_error("服务时间点不存在");
                    }

                }


                Model.yjr_zx_order order = new Model.yjr_zx_order();
                string ms_sessionid = "b" + StringTool.GetUniqueStr();//唯一符号
                order.question_desc = content;
                order.ms_sessionid = ms_sessionid;
                order.doctor_id = config_model.doctorid;
                order.userid = userid;
                order.businessconfigid = config_model.id;
                order.businessappointmentid = businessappointmentid;
                order.service_fee = config_model.price;//金额（分）
                order.service_type = config_model.businessid;
                order.pay_status = 0;
                order.status = 0;
                order.createtime = System.DateTime.Now;
                order.pay_time = DateTime.Parse("1900-01-01");
                BLL.yjr_zx_order orderbll = new BLL.yjr_zx_order();
                var rorder = orderbll.Add(order);

                Model.yjr_zx_messages ms_model = new Model.yjr_zx_messages();
                ms_model.sessionid = ms_sessionid;
                ms_model.owner = 0;// 第一次下单，为患者的提问
                ms_model.userid = userid;
                ms_model.patientid = patientid;
                ms_model.visited_department = department;
                ms_model.visited_hospital = hospital;
                ms_model.visited_status = visited;
                ms_model.diagnosis_result = diagnosis_result;
                ms_model.reportimg = reportimg;
                ms_model.zx_content = content;
                ms_model.zx_number = 1;
                ms_model.doctorid = config_model.doctorid;

                ms_model.createtime = System.DateTime.Now;
                ms_model.status = 0;
                BLL.yjr_zx_messages msbll = new BLL.yjr_zx_messages();
                var r = msbll.Add(ms_model);


                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add("orderid", rorder.ToString());
                res.ResultExt = dic;
                res.ResultCode = 1;
                res.ResultCount = 1;
                res.ResultMessage = "sucess";

                context.Response.ContentType = "application/json";
                context.Response.Write(JsonConvert.SerializeObject(res));
                context.Response.End();

            }
          
          
        }

        private int freecoumt(int userid)
        {
            BLL.View_zx_order bll = new BLL.View_zx_order();

            var my_count = bll.Getrecordcount(" DATEDIFF(DAY,createtime,GETDATE())=0 and service_type=-1 and userid=" + userid);

            if (my_count == 0)
            {

                var Received_count = bll.Getrecordcount(" DATEDIFF(DAY,createtime,GETDATE())=0 and service_type=-1 ");

                var count = System.Configuration.ConfigurationManager.AppSettings["zx_freecount"] == null ? 15 : int.Parse(System.Configuration.ConfigurationManager.AppSettings["zx_freecount"]);

                return count - Received_count;
            }
            else
            {
                return 0;//  =====================
            }
        }

        private void createfree(HttpContext context)
        {
            int userid = DTcms.Common.DTRequest.GetFormInt("userid");
            int businessconfigid = DTcms.Common.DTRequest.GetFormInt("businessconfigid");
            int businessappointmentid = DTcms.Common.DTRequest.GetFormInt("businessappointmentid");
            int patientid = DTcms.Common.DTRequest.GetFormInt("patientid");
            int visited = DTcms.Common.DTRequest.GetFormInt("visited");
            string department = DTcms.Common.DTRequest.GetFormString("department");
            string hospital = DTcms.Common.DTRequest.GetFormString("hospital");
            string diagnosis_result = DTcms.Common.DTRequest.GetFormString("diagnosis");
            string content = DTcms.Common.DTRequest.GetFormString("content");
            string reportimg = DTcms.Common.DTRequest.GetFormString("reportimg");
            JsonResult<string> res = new JsonResult<string>();



            if (freecoumt(userid) > 0)
            {

                Model.yjr_zx_order order = new Model.yjr_zx_order();
                string ms_sessionid = "b" + StringTool.GetUniqueStr();//唯一符号
                order.question_desc = content;
                order.ms_sessionid = ms_sessionid;//义诊无订单 提交时无固定医生
                order.doctor_id = 0;
                order.userid = userid;
                order.businessconfigid = 0;
                order.businessappointmentid = 0;
                order.service_fee = 0;//金额（分）
                order.service_type = -1;
                order.pay_status = 0;
                order.status = 0;
                order.createtime = System.DateTime.Now;
                order.pay_time = DateTime.Parse("1900-01-01");
                BLL.yjr_zx_order orderbll = new BLL.yjr_zx_order();
                var rorder = orderbll.Add(order);

                Model.yjr_zx_messages ms_model = new Model.yjr_zx_messages();
                ms_model.sessionid = ms_sessionid;
                ms_model.owner = 0;// 第一次下单，为患者的提问
                ms_model.userid = userid;
                ms_model.patientid = patientid;
                ms_model.visited_department = department;
                ms_model.visited_hospital = hospital;
                ms_model.visited_status = visited;
                ms_model.diagnosis_result = diagnosis_result;
                ms_model.reportimg = reportimg;
                ms_model.zx_content = content;
                ms_model.zx_number = 1;
                ms_model.doctorid = 0;
                
                ms_model.createtime = System.DateTime.Now;
                ms_model.status = 0;
                BLL.yjr_zx_messages msbll = new BLL.yjr_zx_messages();
                var r = msbll.Add(ms_model);


                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add("orderid", rorder.ToString());
                res.ResultExt = dic;
                res.ResultCode = 1;
                res.ResultCount = 1;
                res.ResultMessage = "sucess";

                context.Response.ContentType = "application/json";
                context.Response.Write(JsonConvert.SerializeObject(res));
                context.Response.End();

            }
            else
            {
                response_error("今日免费名额已抢光");
            }
        }

        private void response_error(string error)
        {
            JsonResult<string> res = new JsonResult<string>();
            res.ResultCode = 0;
            res.ResultCount = 0;
            res.ResultMessage = error;
            HttpContext context = HttpContext.Current;
            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}