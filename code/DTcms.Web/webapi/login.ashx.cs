﻿using Aliyun.Acs.Core;
using Aliyun.Acs.Core.Profile;
using Aliyun.Acs.Dysmsapi.Model.V20170525;
using DTcms.Common;
using DTcms.Web.api.Models;

using Newtonsoft.Json;
using NLog;
using qcloudsms_csharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace DTcms.Web.webapi
{
    /// <summary>
    /// user 的摘要说明
    /// </summary>
    public class login : IHttpHandler, IRequiresSessionState
    {

        Model.userconfig userConfig = new BLL.userconfig().loadConfig();
        Model.sysconfig sysConfig = new BLL.sysconfig().loadConfig();
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public void ProcessRequest(HttpContext context)
        {

         
            //取得处事类型
            string action = DTRequest.GetQueryString("action");


            switch (action)
            {
                case "getcode": // 注册时获取手机验证码
                    user_verify_smscode(context);
                    break;
                case "create":
                    user_register(context);
                    break;
                case "get":
                    user_login(context);
                    break;
                case "loginout":
                    loginout(context);
                    break;
                case "changepassword":
                    changepassword(context);
                    break;
               
            }
        }

        #region 用户注册=====================================


        private void user_register(HttpContext context)
        {
            int site_id = 1;// DTRequest.GetQueryInt("site_id"); //当前站点ID
            string code = DTRequest.GetFormString("code").Trim();//已发送的手机验证码
            string username = Utils.ToHtml(DTRequest.GetFormString("username").Trim());
            string password = DTRequest.GetFormString("Password");// Utils.Number(6); //DTRequest.GetFormString("txtPassword").Trim();
            string email = Utils.ToHtml(DTRequest.GetFormString("email").Trim());
            string mobile = Utils.ToHtml(DTRequest.GetFormString("username").Trim()); ;// Utils.ToHtml(DTRequest.GetFormString("mobile").Trim());
            string usertype= Utils.ToHtml(DTRequest.GetFormString("usertype").Trim());

            string userip = DTRequest.GetIP();

            //string hospital = ""; //Utils.ToHtml(DTRequest.GetFormString("hospital").Trim());
            string department = "";// Utils.ToHtml(DTRequest.GetFormString("department").Trim());
            string truename = "";// Utils.ToHtml(DTRequest.GetFormString("truename").Trim());

            //if (string.IsNullOrEmpty(hospital) || string.IsNullOrEmpty(department) || string.IsNullOrEmpty(truename))
            //{
            //    context.Response.Write("{\"status\":0, \"msg\":\"对不起，医院名称、科室、真实姓名不能为空！\"}");
            //    return;
            //}

            logger.Info("username:" + username + ",code:"+code+",mobile:"+ mobile);

            JsonResult<string> res = new JsonResult<string>();

            string error_msg = "";

            //检查是否开启会员功能
            if (sysConfig.memberstatus == 0)
            {

                //context.Response.Write("{\"status\":0, \"msg\":\"对不起，会员功能已关闭，无法注册！\"}");
                //return;
                error_msg = "对不起，会员功能已关闭，无法注册!";
            }
            if (userConfig.regstatus == 0)
            {
                //context.Response.Write("{\"status\":0, \"msg\":\"对不起，系统暂不允许注册新用户！\"}");
                //return;
                error_msg = "对不起，系统暂不允许注册新用户!";
            }
            //检查用户输入信息是否为空
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
            {
                //context.Response.Write("{\"status\":0, \"msg\":\"对不起，用户名和密码不能为空！\"}");
                //return;
                error_msg = "对不起，用户名和密码不能为空!";
            }


            //检查用户名
            BLL.users bll = new BLL.users();
            if (bll.Exists(username))
            {
                //context.Response.Write("{\"status\":0, \"msg\":\"对不起，该用户名已经存在！\"}");
                //return;
                error_msg = "对不起，该用户名已经存在！";
            }
            //如果开启手机登录要验证手机
            if (userConfig.mobilelogin == 1 && !string.IsNullOrEmpty(mobile))
            {
                if (bll.ExistsMobile(mobile))
                {
                    //context.Response.Write("{\"status\":0, \"msg\":\"对不起，该手机号码已被使用！\"}");
                    //return;
                    error_msg = "对不起，该手机号码已被使用!";
                }
            }

            //检查同一IP注册时隔
            if (userConfig.regctrl > 0)
            {
                if (bll.Exists(userip, userConfig.regctrl))
                {
                    //context.Response.Write("{\"status\":0, \"msg\":\"对不起，同IP在" + userConfig.regctrl + "小时内禁止重复注册！\"}");
                    //return;
                    error_msg = "对不起，同IP在" + userConfig.regctrl + "小时内禁止重复注册!";
                }
            }
            //检查默认组别是否存在
            Model.user_groups modelGroup = new BLL.user_groups().GetDefault();
            if (modelGroup == null)
            {
                //context.Response.Write("{\"status\":0, \"msg\":\"用户尚未分组，请联系网站管理员！\"}");
                //return;
                error_msg = "用户尚未分组，请联系网站管理员!";
            }
            string result2 = verify_sms_code(context, code,mobile);//验证手机验证码

            logger.Info(result2);

            if (result2 != "success")
            {
                //context.Response.Write(result2);
                //return;
                error_msg = result2;
            }

            if (error_msg != "")
            {

                res.ResultCode = 0;
                res.ResultMessage = error_msg;

                context.Response.ContentType = "application/json";
                context.Response.Write(JsonConvert.SerializeObject(res));
                context.Response.End();
                return;
            }

            #region 保存用户注册信息
            Model.users model = new Model.users();
            model.site_id = site_id;
            model.group_id = modelGroup.id;
            model.user_name = username;
            model.salt = Utils.GetCheckCode(6); //获得6位的salt加密字符串
            model.password = DESEncrypt.Encrypt(password, model.salt);
            model.email = email;
            model.mobile = mobile;
            model.reg_ip = userip;
            model.reg_time = DateTime.Now;
            model.hospital = usertype;//区分用户类型
            model.department = department;
            model.truename = truename;
            model.nick_name = "会员1";
            model.avatar = "/upload/default-head.png";//默认头像地址
            //设置用户状态
            if (userConfig.regstatus == 3)
            {
                model.status = 1; //待验证
            }
            else if (userConfig.regverify == 1)
            {
                model.status = 2; //待审核
            }
            else
            {
                model.status = 0; //正常
            }
            #endregion
            //开始写入数据库
            logger.Info("开始写入数据库");

            model.id = bll.Add(model);
            if (model.id < 1)
            {
                //context.Response.Write("{\"status\":0, \"msg\":\"系统故障，请联系网站管理员！\"}");
                //return;

                error_msg = "系统故障，请联系网站管理员！";
                logger.Error(error_msg);

                res.ResultCode = 0;
                res.ResultMessage = error_msg;
            }
            else
            {
                //注册成功
                //context.Response.Write("{\"status\":1, \"msg\":\"注册成功！\",\"userid\":" + model.id + "}");
                //return;
                Dictionary<string, string> ext = new Dictionary<string, string>();
                ext.Add("userid", model.id.ToString());
                res.ResultExt = ext;
                res.ResultCode = 1;
                res.ResultMessage = "sucess";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
            //return;
        }
        #endregion

        #region 用户登录OK=====================================
        private void user_login(HttpContext context)
        {
            int site_id = 1;// DTRequest.GetQueryInt("site_id");
            string username = DTRequest.GetFormString("username");
            string password = DTRequest.GetFormString("Password");
            string remember = DTRequest.GetFormString("chkRemember");

            JsonResult<userjson> res = new JsonResult<userjson>();

            string error_msg = "";

            //检查手机号（用户名），密码
            if (string.IsNullOrEmpty(password) || string.IsNullOrEmpty(username))
            {
               
                error_msg = "温馨提示：请输入手机号码和密码！";
                res.ResultCode = 0;
                res.ResultMessage = error_msg;
                ResponseWriteJson(res);
                return;
            }

            BLL.users bll = new BLL.users();
            var model = bll.GetModel(username,password,0,0,true);

            if (model == null)
            {
               
                error_msg = "错误提示：手机号码或密码有误，请重试！";
                res.ResultCode = 0;
                res.ResultMessage = error_msg;
                ResponseWriteJson(res);
                return;

            }
            //检查用户是否通过验证
            if (model.status == 1) //待验证
            {
                res.ResultCode = 0;
                res.ResultMessage = "错误提示：会员尚未通过验证！";
                ResponseWriteJson(res);
                return;
            }
            else if (model.status == 2) //待审核
            {
                // context.Response.Write("{\"status\":1, \"url\":\""
                //     + new Web.UI.BasePage().getlink(sitepath, new Web.UI.BasePage().linkurl("register", "?action=verify&username=" + Utils.UrlEncode(model.user_name))) + "\", \"msg\":\"会员尚未通过审核！\"}");
                res.ResultCode = 0;
                res.ResultMessage = "错误提示：会员尚未通过审核！";
                ResponseWriteJson(res);
                return;
            }

            logger.Info("检查用户每天登录是否获得积分");
            //检查用户每天登录是否获得积分
            if (!new BLL.user_login_log().ExistsDay(model.user_name) && userConfig.pointloginnum > 0)
            {
                new BLL.user_point_log().Add(model.id, model.user_name, userConfig.pointloginnum, "每天登录获得积分", true); //增加用户登录积分                                                                                                     
            }
            context.Session[DTKeys.SESSION_USER_INFO] = model;
            context.Session.Timeout = 45;
           
            try
            {
                logger.Info("属性：" + model.id + model.user_name);
                //写入登录日志
                new BLL.user_login_log().Add(model.id, model.user_name, "会员登录");
                //返回URL
                //context.Response.Write("{\"status\":1, \"msg\":\"会员登录成功！\",\"userid\":" + model.id + "}");
                
                var token = update_usertoken(model.id);

                BLL.yjr_doctor_vsuser bll_vsuser = new BLL.yjr_doctor_vsuser();
                var vsuser = bll_vsuser.GetModelList("userid=" + model.id.ToString()).FirstOrDefault();
                int hascompleted = 0;
                int doctorid = 0;
                if (vsuser != null)
                {
                    var doctor = new BLL.yjr_doctor().GetModel(int.Parse(vsuser.docid));
                    if (doctor.status == 1)
                    {
                        hascompleted = 2;
                        doctorid = doctor.id;
                    }
                    else
                    {
                        hascompleted = 1;
                        doctorid = doctor.id;
                    }
                    
                }

                res.JsonEntity = new List<userjson> { new userjson(model.id, token, int.Parse(model.hospital), hascompleted, doctorid) };
                res.ResultCode = 1;
                res.ResultMessage = "会员登录成功！";
                ResponseWriteJson(res);

                return;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                logger.Error("error:" + ex.Message);
                //context.Response.Write("{\"status\":0, \"msg\":\"error:" + ex.Message + "\"}");

                res.ResultCode = 0;
                res.ResultMessage = "系统异常，请稍后再试";
                ResponseWriteJson(res);

                return;
            }

        }
       


        #endregion
        
        #region 校检手机验证码OK===============================
        private string verify_sms_code(HttpContext context, string strcode,string mobile)
        {
            if (string.IsNullOrEmpty(strcode))
            {
                return "对不起，请输入验证码！";// "{\"status\":0, \"msg\":\"对不起，请输入验证码！\"}";
            }
            //if (context.Session[DTKeys.SESSION_SMS_CODE] == null)
            //{
            //    return "对不起，验证码超时或已过期！";// "{\"status\":0, \"msg\":\"对不起，验证码超时或已过期！\"}";
            //}
            //if (strcode.ToLower() != (context.Session[DTKeys.SESSION_SMS_CODE].ToString()).ToLower())
            //{
            //    return "您输入的验证码不正确！"; //"{\"status\":0, \"msg\":\"您输入的验证码与系统的不一致！\"}";
            //}
            //context.Session[DTKeys.SESSION_SMS_CODE] = null;

            //DTcms.Common.CacheHelper.Insert(DTKeys.SESSION_SMS_CODE + mobile, strcode);

            if (DTcms.Common.CacheHelper.Get(DTKeys.SESSION_SMS_CODE + mobile) == null)
            {
                return "对不起，验证码超时或已过期！";
            }
            if (strcode.ToLower() != (DTcms.Common.CacheHelper.Get(DTKeys.SESSION_SMS_CODE + mobile).ToString()).ToLower())
            {
                return "您输入的验证码不正确！"; 
            }
            DTcms.Common.CacheHelper.Remove(DTKeys.SESSION_SMS_CODE + mobile);
            return "success";
        }
        #endregion


        #region 更新token
        private string update_usertoken(int userid)
        {
            BLL.yjr_user_uservstoken bll = new BLL.yjr_user_uservstoken();
            string token = Createtoken();

            if (bll.Exists(userid))
            {
                var model = bll.GetModel(userid);
                model.expires = System.DateTime.Now.AddDays(5);
                model.token = token;
                bll.Update(model);
            }
            else
            {
                Model.yjr_user_uservstoken model = new Model.yjr_user_uservstoken();
                model.userid = userid;
                model.expires = System.DateTime.Now.AddDays(5);
                model.token = token;
                bll.Add(model);
            }

            return token;
        }

        private string Createtoken()
        {
            Guid g = Guid.NewGuid();
            var token = Convert.ToBase64String(g.ToByteArray()).Replace("=", "").Replace("+", "").Replace("/", "");
            return token;
        }
        #endregion

        #region 注销登录
        private void loginout(HttpContext context)
        {
            string token = DTRequest.GetFormString("usertoken");
            int userid = DTRequest.GetFormInt("userid");


            JsonResult<string > res = new JsonResult<string>();
            
            if (token != "" && userid.ToString() != "")
            {
                BLL.yjr_user_uservstoken bll = new BLL.yjr_user_uservstoken();
                var model = bll.GetModel(userid);
                if (model != null)
                {
                    if (model.token == token && model.expires > System.DateTime.Now)
                    {
                        BLL.yjr_user_uservstoken vsbll = new BLL.yjr_user_uservstoken();
                        if (bll.Exists(userid))
                        {
                            var modelvs = vsbll.GetModel(userid);
                            modelvs.expires = System.DateTime.Now.AddDays(-5);
                            //model.token = token;
                            vsbll.Update(modelvs);
                            res.ResultCode = 1;
                            res.ResultCount = 1;
                            res.ResultMessage = "sucess";
                        }
                        else
                        {
                            res.ResultCode = 1;
                            res.ResultCount = 0;
                            res.ResultMessage = "OK";
                        }
                    }
                    else
                    {

                        res.ResultCode = 0;
                        res.ResultMessage = "登录超时或未登录";
                        context.Response.Write(JsonConvert.SerializeObject(res));
                        context.Response.End();
                    }
                }
            }
            else
            {
                context.Response.ContentType = "application/json";

                res.ResultCode = 0;
                res.ResultMessage = "未登录";
                context.Response.Write(JsonConvert.SerializeObject(res));
                context.Response.End();
            }

        }
        #endregion

        #region 修改密码
        private void changepassword(HttpContext context)
        {
            string token = DTRequest.GetFormString("usertoken");
            int userid = DTRequest.GetFormInt("userid");

            JsonResult<int> res = new JsonResult<int>();

            if (token != "" && userid.ToString() != "")
            {
                BLL.yjr_user_uservstoken bll = new BLL.yjr_user_uservstoken();
                var model = bll.GetModel(userid);
                if (model != null)
                {
                    if (model.token == token && model.expires > System.DateTime.Now)
                    {
                        string oldpass = DTRequest.GetFormString("oldpass");
                        string newpass = DTRequest.GetFormString("newpass");
                        string renewpass = DTRequest.GetFormString("renewpass");
                        var usermodel = new BLL.users().GetModel(userid);
                        if (DESEncrypt.Encrypt(oldpass.Trim(), usermodel.salt) != usermodel.password)
                        {
                            res.ResultCode = 0;
                            res.ResultMessage = "原密码不正确";
                            context.Response.ContentType = "application/json";
                            context.Response.Write(JsonConvert.SerializeObject(res));
                            context.Response.End();
                        }
                        else
                        {
                            if (newpass != renewpass)
                            {
                                res.ResultCode = 0;
                                res.ResultMessage = "两次密码不一致";
                                context.Response.ContentType = "application/json";
                                context.Response.Write(JsonConvert.SerializeObject(res));
                                context.Response.End();
                            }
                            else
                            {
                                usermodel.password = DESEncrypt.Encrypt(newpass.Trim(), usermodel.salt);
                                int r = new BLL.users().Update(usermodel) == true ? 1 : 0;
                                res.ResultCode = r;
                                res.ResultMessage = r == 1 ? "sucess" : "fails";
                                context.Response.ContentType = "application/json";
                                context.Response.Write(JsonConvert.SerializeObject(res));
                                context.Response.End();
                            }
                        }
                        //if(DESEncrypt.Encrypt(txtOldPassword.Text.Trim(), model.salt) != model.password)
                    }
                    else
                    {

                        res.ResultCode = 0;
                        res.ResultMessage = "登录超时或未登录";
                        context.Response.ContentType = "application/json";
                        context.Response.Write(JsonConvert.SerializeObject(res));
                        context.Response.End();
                    }
                }
                else
                {
                    res.ResultCode = 0;
                    res.ResultMessage = "未找到登录凭据";
                    context.Response.ContentType = "application/json";
                    context.Response.Write(JsonConvert.SerializeObject(res));
                    context.Response.End();
                }
            }
            else
            {
                context.Response.ContentType = "application/json";
               
                res.ResultCode = 0;
                res.ResultMessage = "未登录";

                context.Response.Write(JsonConvert.SerializeObject(res));
                context.Response.End();
            }
        }
        #endregion
                
        #region 发送短信验证码
        private void user_verify_smscode(HttpContext context)
        {
            JsonResult<string> res = new JsonResult<string>();
            //string error_msg = "";

            string mobile = Utils.ToHtml(DTRequest.GetString("mobile"));
            string chkcode = DTRequest.GetString("chkcode");
            if (string.IsNullOrEmpty(chkcode))
            {
                //context.Response.Write("{\"status\":0, \"msg\":\"验证码不能为空！\"}");
                res.ResultCode = 0;
                res.ResultMessage = "图形校验码不能为空！";
                context.Response.ContentType = "application/json";
                context.Response.Write(JsonConvert.SerializeObject(res));
                context.Response.End();
                return;
            }
            else
            {
               
                //验证图形验证码
                if (DTcms.Common.CacheHelper.Get("REG_V_CODE" + mobile) != null)
                {
                    if (chkcode.ToLower() != Convert.ToString(DTcms.Common.CacheHelper.Get("REG_V_CODE" + mobile)).ToLower())
                    {
                        //context.Response.Write("{\"status\":0, \"msg\":\"发送失败，验证码不正确！\"}");
                        res.ResultCode = 0;
                        res.ResultMessage = "图形校验码不正确！";
                        context.Response.ContentType = "application/json";
                        context.Response.Write(JsonConvert.SerializeObject(res));
                        context.Response.End();
                        return;
                    }
                    else
                    {

                    }
                }
                else
                {
                    //context.Response.Write("{\"status\":0, \"msg\":\"系统错误，请刷新后重试！\"}");
                    res.ResultCode = 0;
                    res.ResultMessage = "系统错误，请刷新后重试！";
                    context.Response.ContentType = "application/json";
                    context.Response.Write(JsonConvert.SerializeObject(res));
                    context.Response.End();
                    return;
                }

            }
            //检查手机
            if (string.IsNullOrEmpty(mobile) || mobile.Length != 11)
            {
                //context.Response.Write("{\"status\":0, \"msg\":\"发送失败，请填写手机号码！\"}");
                res.ResultCode = 0;
                res.ResultMessage = "系统错误，请填写正确的手机号码！";
                context.Response.ContentType = "application/json";
                context.Response.Write(JsonConvert.SerializeObject(res));
                context.Response.End();
                return;
            }
            else
            {
                BLL.users bll = new BLL.users();
                if (bll.Exists(mobile))
                {//以手机号码为用户名 验证手机是否存在
                 //context.Response.Write("{\"status\":0, \"msg\":\"手机号码已经注册，请登录！\"}");
                    res.ResultCode = 0;
                    res.ResultMessage = "手机号码已经注册，请登录";
                    context.Response.ContentType = "application/json";
                    context.Response.Write(JsonConvert.SerializeObject(res));
                    context.Response.End();
                    return;

                }

            }


           


            string strcode = Utils.Number(6); //随机验证码

            int ret = qcloud_smsvcode_send(mobile, strcode);// ali_smsvcode_send(mobile, strcode);

            if (ret == 0)
            {
                //return "{\"status\":0, \"msg\":\"发送失败! \"}";
                res.ResultCode = 0;
                res.ResultMessage = "发送失败，请稍后再试！";
                context.Response.ContentType = "application/json";
                context.Response.Write(JsonConvert.SerializeObject(res));
                context.Response.End();
            }
            else

            {

               
                //写入SESSION，保存验证码
                //context.Session[DTKeys.SESSION_SMS_CODE] = strcode;

                DTcms.Common.CacheHelper.Insert(DTKeys.SESSION_SMS_CODE + mobile, strcode,5);//短信验证码，5分钟有效

                logger.Info("调试短信验证码：insert完毕，get测试(mobile)：" + DTcms.Common.CacheHelper.Get(DTKeys.SESSION_SMS_CODE + mobile));

                res.ResultCode = 1;
                res.ResultMessage = "发送成功！";
                context.Response.ContentType = "application/json";
                context.Response.Write(JsonConvert.SerializeObject(res));
                context.Response.End();
            }
        }
        #endregion

        #region 阿里云-短信发送
        /// <summary>
        /// 阿里云-短信发送
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        private int ali_smsvcode_send(string mobile, string code)
        {
            string product = "Dysmsapi";//短信API产品名称
            string domain = "dysmsapi.aliyuncs.com";//短信API产品域名
            string accessId = System.Configuration.ConfigurationManager.AppSettings["alidayu_sms_accessId"].ToString(); ;
            string accessSecret = System.Configuration.ConfigurationManager.AppSettings["alidayu_sms_accessSecret"].ToString();

            string SignName = System.Configuration.ConfigurationManager.AppSettings["alidayu_sms_signname"].ToString();
            string TemplateCode = System.Configuration.ConfigurationManager.AppSettings["alidayu_sms_templatecode"].ToString();

            string regionIdForPop = "cn-hangzhou";

            IClientProfile profile = DefaultProfile.GetProfile(regionIdForPop, accessId, accessSecret);
            DefaultProfile.AddEndpoint(regionIdForPop, regionIdForPop, product, domain);
            IAcsClient acsClient = new DefaultAcsClient(profile);
            SendSmsRequest request = new SendSmsRequest();


            try
            {
                //request.SignName = "上云预发测试";//"管理控制台中配置的短信签名（状态必须是验证通过）"
                //request.TemplateCode = "SMS_71130001";//管理控制台中配置的审核通过的短信模板的模板CODE（状态必须是验证通过）"
                //request.RecNum = "13567939485";//"接收号码，多个号码可以逗号分隔"
                //request.ParamString = "{\"name\":\"123\"}";//短信模板中的变量；数字需要转换为字符串；个人用户每个变量长度必须小于15个字符。"
                //SingleSendSmsResponse httpResponse = client.GetAcsResponse(request);
                request.PhoneNumbers = mobile;// "1350000000";
                request.SignName = SignName;// "xxxxxx";
                request.TemplateCode = TemplateCode;// "SMS_xxxxxxx";
                request.TemplateParam = "{\"code\":\"" + code + "\"}";//数字验证码
                request.OutId = "";
                //请求失败这里会抛ClientException异常
                SendSmsResponse sendSmsResponse = acsClient.GetAcsResponse(request);
                if (sendSmsResponse.Code == "OK")
                {
                    return 1;
                }
                else
                {
                    logger.Debug("sendSmsResponse.Code_no_ok_msg：" + sendSmsResponse.Message);
                    return 0;

                }

            }
            catch (Exception ex)
            {
                logger.Error("ali_smsvcode_send_catch_error:" + ex.Message);
                return 0;
            }
            return 0;
        }
        #endregion

        #region 腾讯云——短信发送
        /// <summary>
        /// 腾讯云——短信发送
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="strcode"></param>
        /// <returns></returns>
        private int qcloud_smsvcode_send(string mobile, string strcode)
        {
            int appid = int.Parse(System.Configuration.ConfigurationManager.AppSettings["SMS_appid"].ToString()); //1400239746;
            // 短信应用 SDK AppKey
            string appkey = System.Configuration.ConfigurationManager.AppSettings["SMS_appkey"].ToString();// "05db653f1ac1c553025fab31c983ee4d";
            string templateid = System.Configuration.ConfigurationManager.AppSettings["SMS_templateid"].ToString();
            string smsSign = System.Configuration.ConfigurationManager.AppSettings["SMS_sign"].ToString();//SMS_sign

            SmsSingleSender ssender = new SmsSingleSender(appid, appkey);


            string[] msgcontent = { strcode, userConfig.regsmsexpired.ToString() };
            var sresult = ssender.sendWithParam("86", mobile, int.Parse(templateid), msgcontent
                , smsSign, "", "");

            if (sresult.result != 0)
            {
                logger.Debug("qcloud_smsvcode_send_ssender_error" + sresult.errMsg);
                return 0;
            }
            else
            {
                return 1;
            }

        }
        #endregion




        private void ResponseWriteJson(api.Models.JsonResult<userjson> res)
        {
            HttpContext context = HttpContext.Current;
            context.Response.ContentType = "application/json";
            //context.Response.Headers.Set("Access-Control-Allow-Origin", "http://localhost/");
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.ApplicationInstance.CompleteRequest();
            // context.Response.End();
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}