﻿using DTcms.Common;
using DTcms.Model;
using DTcms.Web.api.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace DTcms.Web.webapi
{
    /// <summary>
    /// newscate 的摘要说明
    /// </summary>
    public class newscate : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            //取得处事类型
            string action = DTRequest.GetQueryString("action");

            switch (action)
            {
                case "get": // 获取信息
                    get(context);
                    break;

                default:
                    break;
            }
        }

        private void get(HttpContext context)
        {
            JsonResult<article_category> res = new JsonResult<article_category>();
            try
            {

                int channel_id = DTRequest.GetFormInt("channel_id");
                int category_id = DTRequest.GetFormInt("category_id");
                int parent_id = DTRequest.GetFormInt("parent_id");

                BLL.article_category bll = new BLL.article_category();
                List<Model.article_category> list = new List<Model.article_category>();

                if (category_id == 0)
                {
                    DataTable dt = bll.GetList(parent_id, channel_id);

                    if (dt != null && dt.Rows.Count > 0)
                    {

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            Model.article_category model = new Model.article_category();
                            model.id = int.Parse(dt.Rows[i]["id"].ToString());
                            model.title = dt.Rows[i]["title"].ToString();
                            model.parent_id = int.Parse(dt.Rows[i]["parent_id"].ToString());
                            model.sort_id = int.Parse(dt.Rows[i]["sort_id"].ToString());
                            model.channel_id= int.Parse(dt.Rows[i]["channel_id"].ToString());
                            model.class_layer= int.Parse(dt.Rows[i]["class_layer"].ToString());
                            model.img_url = dt.Rows[i]["img_url"].ToString();
                            model.link_url= dt.Rows[i]["link_url"].ToString();

                            list.Add(model);
                        }
                    }
                }
                else
                {
                    var cate_model = bll.GetModel(category_id); 
                    if (cate_model != null)
                    {
                        list.Add(cate_model);
                    }
                }

                //

                res.ResultCount = list.Count;
                res.ResultCode = 1;
                res.ResultMessage = "成功";
                res.JsonEntity = list;

            }
            catch (Exception ex)
            {
                res.ResultCode = -2;
                res.ResultMessage = ex.Message;
            }
            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}