﻿using DTcms.Common;
using DTcms.Model;
using DTcms.Web.cmsapi.Models;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DTcms.Web.webapi
{
    /// <summary>
    /// memberpatient 的摘要说明
    /// </summary>
    public class memberpatient : userauthority,IHttpHandler
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public void ProcessRequest(HttpContext context)
        {
            //取得处事类型
            string action = DTRequest.GetQueryString("action");


            switch (action)
            {
                case "get": // 添加就诊人信息
                    get(context);
                    break;

                case "update"://更新就诊人信息
                    update(context);
                    break;// create
                case "create"://创建就诊人
                    create(context);
                    break;// create
            }
        }

        private void get(HttpContext context)
        {
            int userid = DTcms.Common.DTRequest.GetFormInt("userid");
            int patientid = DTcms.Common.DTRequest.GetFormInt("patientid");
            JsonResult<yjr_patient> res = new JsonResult<yjr_patient>();
            BLL.yjr_patient bll = new BLL.yjr_patient();
            if (patientid != 0)
            {
                var list = bll.GetModelList("id=" + patientid);
                res.JsonEntity = list;
                res.ResultCode = 1;
                res.ResultCount = 1;
                res.ResultMessage = "sucess";
            }
            else
            {
                var list = bll.GetModelList("userid="+ userid);
                res.JsonEntity = list;
                res.ResultCode = 1;
                res.ResultCount = list.Count;
                res.ResultMessage = "sucess";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();

        }
        private void update(HttpContext context)
        {
            int userid = DTcms.Common.DTRequest.GetFormInt("userid");
            int patientid = DTcms.Common.DTRequest.GetFormInt("patientid");
            string name = DTcms.Common.DTRequest.GetFormString("name");
            string gender = DTcms.Common.DTRequest.GetFormString("gender");
            string brithday = DTcms.Common.DTRequest.GetFormString("brithday");
           
            JsonResult<yjr_patient> res = new JsonResult<yjr_patient>();
            BLL.yjr_patient bll = new BLL.yjr_patient();
            if (patientid != 0)
            {
                var model = bll.GetModel(patientid);

                if (model != null)
                {
                    if(!string.IsNullOrEmpty(name))
                    {
                        model.name = name;
                    }
                    if (!string.IsNullOrEmpty(gender))
                    {
                        model.gender = gender;
                    }
                    if (!string.IsNullOrEmpty(brithday)&& Utils.IsDate(brithday))
                    {
                        model.brithday = brithday;
                    }
                    var upresult = bll.Update(model);
                    res.JsonEntity = new List<yjr_patient>() { model };
                    res.ResultCode = upresult == true ? 1 : 0; ;
                    res.ResultCount = res.ResultCode;
                    res.ResultMessage = upresult == true?"sucess":"fails";
                }
                else
                {
                    res.ResultCode = 0;
                    res.ResultCount = 0;
                    res.ResultMessage = "fails";
                }
             
            }
            else
            {
                res.ResultCode = 0;
                res.ResultCount = 0;
                res.ResultMessage = "patientid_error";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }

        private void create(HttpContext context)
        {
            int userid = DTcms.Common.DTRequest.GetFormInt("userid");
            //int patientid = DTcms.Common.DTRequest.GetFormInt("patientid");
            string name = DTcms.Common.DTRequest.GetFormString("name");
            string gender = DTcms.Common.DTRequest.GetFormString("gender");
            string brithday = DTcms.Common.DTRequest.GetFormString("brithday");

            JsonResult<yjr_patient> res = new JsonResult<yjr_patient>();
            BLL.yjr_patient bll = new BLL.yjr_patient();
            if (!bll.Exists(userid, name))
            {
                yjr_patient model = new yjr_patient();
                model.name = name;
                model.gender = gender;
                model.brithday = brithday;
                model.remark = "";
                model.status = 1;
                model.userid = userid;
                var result = bll.Add(model);
                res.JsonEntity = new List<yjr_patient>() { model };
                res.ResultCode = result;
                res.ResultCount = result;
                res.ResultMessage = "sucess";
            }
            else
            {
                res.ResultCode = 0;
                res.ResultCount = 0;
                res.ResultMessage = " already exists";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}