﻿using Aliyun.Acs.Core;
using Aliyun.Acs.Core.Profile;
using Aliyun.Acs.Dysmsapi.Model.V20170525;
using DTcms.Common;
using DTcms.Web.api.Models;

using Newtonsoft.Json;
using NLog;
using qcloudsms_csharp;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace DTcms.Web.webapi
{
    /// <summary>
    /// findpwd 的摘要说明
    /// </summary>
    public class findpwd : IHttpHandler,IRequiresSessionState
    {
        Model.userconfig userConfig = new BLL.userconfig().loadConfig();
        Model.sysconfig sysConfig = new BLL.sysconfig().loadConfig();
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public void ProcessRequest(HttpContext context)
        {
            //取得处事类型
            string action = DTRequest.GetQueryString("action");


            switch (action)
            {
                case "getcode": // 注册时获取手机验证码
                    user_fpwd_verify_smscode(context);
                    break;
                case "findpassword":
                    findpassword(context);
                    break;
                case "imgcode":
                    getcodeimg(context);
                    break;
            }
        }


        #region 找回密码

        private void findpassword(HttpContext context)
        {
            JsonResult<string> res = new JsonResult<string>();

            string code = DTRequest.GetFormString("code").Trim();//已发送的手机验证码
            string username = Utils.ToHtml(DTRequest.GetFormString("username").Trim());
            
            string password = DTRequest.GetFormString("Password");// Utils.Number(6); //DTRequest.GetFormString("txtPassword").Trim();
            string userip = DTRequest.GetIP();

            string error_msg = "";
            //检查用户名
            BLL.users bll = new BLL.users();
            if (!bll.Exists(username))
            {
                //context.Response.Write("{\"status\":0, \"msg\":\"对不起，该用户名已经存在！\"}");
                //return;
                error_msg = "对不起，该手机号码不存在！";
            }

            string result2 = verify_sms_code(context, code, username);//验证手机验证码

            logger.Info(result2);

            if (result2 != "success")
            {
                //context.Response.Write(result2);
                //return;
                error_msg = result2;
            }

            if (error_msg != "")
            {

                res.ResultCode = 0;
                res.ResultMessage = error_msg;

                context.Response.ContentType = "application/json";
                context.Response.Write(JsonConvert.SerializeObject(res));
                context.Response.End();
                return;
            }
            else
            {
                var userbll = new BLL.users();
                var usermodel = userbll.GetModel(username);

                usermodel.password = DESEncrypt.Encrypt(password.Trim(), usermodel.salt);
                int r = userbll.Update(usermodel) == true ? 1 : 0;
                res.ResultCode = r;
                res.ResultMessage = r == 1 ? "sucess" : "fails";
                context.Response.ContentType = "application/json";
                context.Response.Write(JsonConvert.SerializeObject(res));
                context.Response.End();
            }
        }

        #endregion

        #region 找回密码 校检手机验证码OK===============================
        private string verify_sms_code(HttpContext context, string strcode, string mobile)
        {
            if (string.IsNullOrEmpty(strcode))
            {
                return "对不起，请输入验证码！";// "{\"status\":0, \"msg\":\"对不起，请输入验证码！\"}";
            }
          
            if (DTcms.Common.CacheHelper.Get(DTKeys.FPWD_SESSION_key + mobile) == null)
            {
                return "对不起，验证码超时或已过期！";
            }
            if (strcode.ToLower() != (DTcms.Common.CacheHelper.Get(DTKeys.FPWD_SESSION_key + mobile).ToString()).ToLower())
            {
                return "您输入的验证码不正确！";
            }
            else
            {
                DTcms.Common.CacheHelper.Remove(DTKeys.FPWD_SESSION_key + mobile);
                return "success";
            }
        }
        #endregion


        #region 发送短信验证码
        private void user_fpwd_verify_smscode(HttpContext context)
        {
            JsonResult<string> res = new JsonResult<string>();
            //string error_msg = "";

            string mobile = Utils.ToHtml(DTRequest.GetString("mobile"));
            string chkcode = DTRequest.GetString("chkcode");
            if (string.IsNullOrEmpty(chkcode))
            {
                //context.Response.Write("{\"status\":0, \"msg\":\"验证码不能为空！\"}");
                res.ResultCode = 0;
                res.ResultMessage = "图形校验码不能为空！";
                context.Response.ContentType = "application/json";
                context.Response.Write(JsonConvert.SerializeObject(res));
                context.Response.End();
                return;
            }
            else
            {

                //验证图形验证码
                if (DTcms.Common.CacheHelper.Get("FPWD_V_CODE" + mobile) != null)
                {
                    if (chkcode.ToLower() != Convert.ToString(DTcms.Common.CacheHelper.Get("FPWD_V_CODE" + mobile)).ToLower())
                    {
                        //context.Response.Write("{\"status\":0, \"msg\":\"发送失败，验证码不正确！\"}");
                        res.ResultCode = 0;
                        res.ResultMessage = "图形校验码不正确！";
                        context.Response.ContentType = "application/json";
                        context.Response.Write(JsonConvert.SerializeObject(res));
                        context.Response.End();
                        return;
                    }
                    else
                    {

                    }
                }
                else
                {
                    //context.Response.Write("{\"status\":0, \"msg\":\"系统错误，请刷新后重试！\"}");
                    res.ResultCode = 0;
                    res.ResultMessage = "系统错误，请刷新后重试！";
                    context.Response.ContentType = "application/json";
                    context.Response.Write(JsonConvert.SerializeObject(res));
                    context.Response.End();
                    return;
                }

            }
            //检查手机
            if (string.IsNullOrEmpty(mobile) || mobile.Length != 11)
            {
                //context.Response.Write("{\"status\":0, \"msg\":\"发送失败，请填写手机号码！\"}");
                res.ResultCode = 0;
                res.ResultMessage = "系统错误，请填写正确的手机号码！";
                context.Response.ContentType = "application/json";
                context.Response.Write(JsonConvert.SerializeObject(res));
                context.Response.End();
                return;
            }
            else
            {
                BLL.users bll = new BLL.users();
                if (!bll.Exists(mobile))
                {//以手机号码为用户名 验证手机是否存在
                 //context.Response.Write("{\"status\":0, \"msg\":\"手机号码已经注册，请登录！\"}");
                    res.ResultCode = 0;
                    res.ResultMessage = "手机号码不存在，请注册";
                    context.Response.ContentType = "application/json";
                    context.Response.Write(JsonConvert.SerializeObject(res));
                    context.Response.End();
                    return;

                }

            }





            string strcode = Utils.Number(6); //随机验证码

            int ret = qcloud_smsvcode_send(mobile, strcode);// ali_smsvcode_send(mobile, strcode);

            if (ret == 0)
            {
                //return "{\"status\":0, \"msg\":\"发送失败! \"}";
                res.ResultCode = 0;
                res.ResultMessage = "发送失败，请稍后再试！";
                context.Response.ContentType = "application/json";
                context.Response.Write(JsonConvert.SerializeObject(res));
                context.Response.End();
            }
            else

            {


                //写入SESSION，保存验证码
                //context.Session[DTKeys.SESSION_SMS_CODE] = strcode;

                DTcms.Common.CacheHelper.Insert(DTKeys.FPWD_SESSION_key + mobile, strcode, 5);//短信验证码，5分钟有效

              

                res.ResultCode = 1;
                res.ResultMessage = "发送成功！";
                context.Response.ContentType = "application/json";
                context.Response.Write(JsonConvert.SerializeObject(res));
                context.Response.End();
            }
        }
        #endregion

        private void getcodeimg(HttpContext context)
        {
            string mobile = DTRequest.GetQueryString("mobile");
            if (mobile == "")
            {
                return;
            }
            int codeW = 85;
            int codeH = 24;
            int fontSize = 16;
            string chkCode = string.Empty;
            //颜色列表，用于验证码、噪线、噪点 
            Color[] color = { Color.Black, Color.Blue, Color.Green, Color.Orange, Color.Brown, Color.Brown, Color.DarkBlue };
            //字体列表，用于验证码 
            string[] font = { "Times New Roman", "Verdana", "Arial", "Gungsuh", "Impact" };
            //验证码的字符集，去掉了一些容易混淆的字符 
            // char[] character = { '2', '3', '4', '5', '6', '8', '9', 'a', 'b', 'd', 'e', 'f', 'h', 'k', 'm', 'n', 'r', 'x', 'y', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'R', 'S', 'T', 'W', 'X', 'Y' };
            char[] character = { '2', '3', '4', '5', '6', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'R', 'S', 'T', 'W', 'X', 'Y' };
            Random rnd = new Random();
            //生成验证码字符串 
            for (int i = 0; i < 4; i++)
            {
                chkCode += character[rnd.Next(character.Length)];
            }
            //写入缓存
            //context.Session["REG_V_CODE"+ mobile] = chkCode;
            DTcms.Common.CacheHelper.Insert("FPWD_V_CODE" + mobile, chkCode, 2);
            //创建画布
            Bitmap bmp = new Bitmap(codeW, codeH);
            Graphics g = Graphics.FromImage(bmp);
            g.Clear(Color.White);
            //画噪线 
            for (int i = 0; i < 1; i++)
            {
                int x1 = rnd.Next(codeW);
                int y1 = rnd.Next(codeH);
                int x2 = rnd.Next(codeW);
                int y2 = rnd.Next(codeH);
                Color clr = color[rnd.Next(color.Length)];
                g.DrawLine(new Pen(clr), x1, y1, x2, y2);
            }
            //画验证码字符串 
            for (int i = 0; i < chkCode.Length; i++)
            {
                string fnt = font[rnd.Next(font.Length)];
                Font ft = new Font(fnt, fontSize);
                Color clr = color[rnd.Next(color.Length)];
                g.DrawString(chkCode[i].ToString(), ft, new SolidBrush(clr), (float)i * 18 + 2, (float)0);
            }
            //画噪点 
            for (int i = 0; i < 100; i++)
            {
                int x = rnd.Next(bmp.Width);
                int y = rnd.Next(bmp.Height);
                Color clr = color[rnd.Next(color.Length)];
                bmp.SetPixel(x, y, clr);
            }
            //清除该页输出缓存，设置该页无缓存 
            context.Response.Buffer = true;
            context.Response.ExpiresAbsolute = System.DateTime.Now.AddMilliseconds(0);
            context.Response.Expires = 0;
            context.Response.CacheControl = "no-cache";
            context.Response.AppendHeader("Pragma", "No-Cache");
            //将验证码图片写入内存流，并将其以 "image/Png" 格式输出 
            MemoryStream ms = new MemoryStream();
            try
            {
                bmp.Save(ms, ImageFormat.Png);
                context.Response.ClearContent();
                context.Response.ContentType = "image/Png";
                context.Response.BinaryWrite(ms.ToArray());
            }
            finally
            {
                //显式释放资源 
                bmp.Dispose();
                g.Dispose();
            }
        }

        #region 阿里云-短信发送
        /// <summary>
        /// 阿里云-短信发送
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        private int ali_smsvcode_send(string mobile, string code)
        {
            string product = "Dysmsapi";//短信API产品名称
            string domain = "dysmsapi.aliyuncs.com";//短信API产品域名
            string accessId = System.Configuration.ConfigurationManager.AppSettings["alidayu_sms_accessId"].ToString(); ;
            string accessSecret = System.Configuration.ConfigurationManager.AppSettings["alidayu_sms_accessSecret"].ToString();

            string SignName = System.Configuration.ConfigurationManager.AppSettings["alidayu_sms_signname"].ToString();
            string TemplateCode = System.Configuration.ConfigurationManager.AppSettings["alidayu_sms_templatecode"].ToString();

            string regionIdForPop = "cn-hangzhou";

            IClientProfile profile = DefaultProfile.GetProfile(regionIdForPop, accessId, accessSecret);
            DefaultProfile.AddEndpoint(regionIdForPop, regionIdForPop, product, domain);
            IAcsClient acsClient = new DefaultAcsClient(profile);
            SendSmsRequest request = new SendSmsRequest();


            try
            {
                //request.SignName = "上云预发测试";//"管理控制台中配置的短信签名（状态必须是验证通过）"
                //request.TemplateCode = "SMS_71130001";//管理控制台中配置的审核通过的短信模板的模板CODE（状态必须是验证通过）"
                //request.RecNum = "13567939485";//"接收号码，多个号码可以逗号分隔"
                //request.ParamString = "{\"name\":\"123\"}";//短信模板中的变量；数字需要转换为字符串；个人用户每个变量长度必须小于15个字符。"
                //SingleSendSmsResponse httpResponse = client.GetAcsResponse(request);
                request.PhoneNumbers = mobile;// "1350000000";
                request.SignName = SignName;// "xxxxxx";
                request.TemplateCode = TemplateCode;// "SMS_xxxxxxx";
                request.TemplateParam = "{\"code\":\"" + code + "\"}";//数字验证码
                request.OutId = "";
                //请求失败这里会抛ClientException异常
                SendSmsResponse sendSmsResponse = acsClient.GetAcsResponse(request);
                if (sendSmsResponse.Code == "OK")
                {
                    return 1;
                }
                else
                {
                    logger.Debug("sendSmsResponse.Code_no_ok_msg：" + sendSmsResponse.Message);
                    return 0;

                }

            }
            catch (Exception ex)
            {
                logger.Error("ali_smsvcode_send_catch_error:" + ex.Message);
                return 0;
            }
            return 0;
        }
        #endregion

        #region 腾讯云——短信发送
        /// <summary>
        /// 腾讯云——短信发送
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="strcode"></param>
        /// <returns></returns>
        private int qcloud_smsvcode_send(string mobile, string strcode)
        {
            int appid = int.Parse(System.Configuration.ConfigurationManager.AppSettings["SMS_appid"].ToString()); //1400239746;
            // 短信应用 SDK AppKey
            string appkey = System.Configuration.ConfigurationManager.AppSettings["SMS_appkey"].ToString();// "05db653f1ac1c553025fab31c983ee4d";
            string templateid = System.Configuration.ConfigurationManager.AppSettings["SMS_templateid"].ToString();
            string smsSign = System.Configuration.ConfigurationManager.AppSettings["SMS_sign"].ToString();//SMS_sign

            SmsSingleSender ssender = new SmsSingleSender(appid, appkey);


            string[] msgcontent = { strcode, userConfig.regsmsexpired.ToString() };
            var sresult = ssender.sendWithParam("86", mobile, int.Parse(templateid), msgcontent
                , smsSign, "", "");

            if (sresult.result != 0)
            {
                logger.Debug("qcloud_smsvcode_send_ssender_error" + sresult.errMsg);
                return 0;
            }
            else
            {
                return 1;
            }

        }
        #endregion


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}