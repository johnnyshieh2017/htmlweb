﻿using DTcms.Common;
using DTcms.Model;
using DTcms.Web.cmsapi.Models;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace DTcms.Web.webapi
{
    /// <summary>
    /// doctorbusinessconfig 的摘要说明
    /// </summary>
    public class doctorbusinessconfig : IHttpHandler
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public void ProcessRequest(HttpContext context)
        {
            //取得处事类型
            string action = DTRequest.GetQueryString("action");


            switch (action)
            {
                case "get": // 获取医生业务详细配置
                    get(context);
                    break;
                case "create"://创建医生业务服务详细配置
                    create(context);
                    break;
                case "update"://更新医生业务服务详细配置
                    update(context);
                    break;
            }
        }

        private void create(HttpContext context)
        {

   


            JsonResult<string> res = new JsonResult<string>();
            int docid = DTRequest.GetFormInt("doctorid");
            int bid = DTRequest.GetFormInt("bid");//businessid
            int ctype= DTRequest.GetFormInt("ctype");//cost_type//服务周期类型
            int interval = DTRequest.GetFormInt("interval");//cost_interval
            int price = DTRequest.GetFormInt("price");

            #region 登录验证

            string token = DTRequest.GetFormString("usertoken");
            string userid = DTRequest.GetFormString("userid");

            //logger.Debug("userauthority_token:" + token);
            //logger.Debug("userauthority_userid:" + userid);
            //logger.Debug("userauthority_action:" + DTRequest.GetQueryString("action"));

            if (token != "" && userid != "")
            {
                BLL.yjr_user_uservstoken bll_uvt = new BLL.yjr_user_uservstoken();
                var model = bll_uvt.GetModel(int.Parse(userid));
                if (model != null)
                {
                    if (model.token == token && model.expires > System.DateTime.Now)
                    {

                    }
                    else
                    {
                        context.Response.ContentType = "application/json";
                       
                        res.ResultCode = 0;
                        res.ResultMessage = "登录超时或未登录";
                        context.Response.Write(JsonConvert.SerializeObject(res));
                        context.Response.End();
                    }
                }
            }
            else
            {
                context.Response.ContentType = "application/json";
                
                res.ResultCode = 0;
                res.ResultMessage = "未登录";
                context.Response.Write(JsonConvert.SerializeObject(res));
                context.Response.End();
            }

            #endregion


            BLL.yjr_doctor_vsbusinessconfig bll = new BLL.yjr_doctor_vsbusinessconfig();
            
            if (bll.GetModelList("doctorid=" + docid + " and businessid=" + bid + " and cost_type="+ ctype + " and cost_interval="+ interval + "").Count < 1)
            {
                yjr_doctor_vsbusinessconfig model = new yjr_doctor_vsbusinessconfig();
                
                //model.id = Guid.NewGuid().ToString("N");
                model.doctorid = docid;
                model.businessid = bid;
                model.cost_type = ctype;
                model.cost_interval = interval;
                model.price = price;
                model.add_time = System.DateTime.Now;
                if (bll.Addext(model)>0)
                {
                    res.ResultCode = 1;
                    res.ResultCount = 1;
                    res.ResultMessage = "sucess";
                }
                else
                {
                    res.ResultCode = 0;
                    res.ResultCount = 0;
                    res.ResultMessage = "fails";
                }
            }
            else
            {
                    res.ResultCode = 0;
                    res.ResultCount = 0;
                    res.ResultMessage = "fails";   
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }

        private void update(HttpContext context)
        {
            JsonResult<string> res = new JsonResult<string>();
            int configid = DTRequest.GetFormInt("configid");
            //int docid = DTRequest.GetFormInt("doctorid");
            //int bid = DTRequest.GetFormInt("bid");//businessid
            //int ctype = DTRequest.GetFormInt("ctype");//cost_type
            //int interval = DTRequest.GetFormInt("interval");//cost_interval
            int price = DTRequest.GetFormInt("price");

            #region 登录验证

            string token = DTRequest.GetFormString("usertoken");
            string userid = DTRequest.GetFormString("userid");

            //logger.Debug("userauthority_token:" + token);
            //logger.Debug("userauthority_userid:" + userid);
            //logger.Debug("userauthority_action:" + DTRequest.GetQueryString("action"));

            if (token != "" && userid != "")
            {
                BLL.yjr_user_uservstoken bll_uvt = new BLL.yjr_user_uservstoken();
                var model = bll_uvt.GetModel(int.Parse(userid));
                if (model != null)
                {
                    if (model.token == token && model.expires > System.DateTime.Now)
                    {

                    }
                    else
                    {
                        context.Response.ContentType = "application/json";

                        res.ResultCode = 0;
                        res.ResultMessage = "登录超时或未登录";
                        context.Response.Write(JsonConvert.SerializeObject(res));
                        context.Response.End();
                    }
                }
            }
            else
            {
                context.Response.ContentType = "application/json";

                res.ResultCode = 0;
                res.ResultMessage = "未登录";
                context.Response.Write(JsonConvert.SerializeObject(res));
                context.Response.End();
            }

            #endregion


            BLL.yjr_doctor_vsbusinessconfig bll = new BLL.yjr_doctor_vsbusinessconfig();

            if (bll.Exists(configid))
            {
                yjr_doctor_vsbusinessconfig model = bll.GetModel(configid);
                //model.doctorid = docid;
                //model.businessid = bid;
                //model.cost_type = ctype;
                //model.cost_interval = interval;
                model.price = price;
                model.add_time = System.DateTime.Now;
                if (bll.Updateext(model))
                {
                    res.ResultCode = 1;
                    res.ResultCount = 1;
                    res.ResultMessage = "sucess";
                }
                else
                {
                    res.ResultCode = 0;
                    res.ResultCount = 0;
                    res.ResultMessage = "fails";
                }
            }
            else
            {
                res.ResultCode = 0;
                res.ResultCount = 0;
                res.ResultMessage = "error:信息不存在";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }

        private void get(HttpContext context)
        {
            JsonResult<View_docvsbusiness> res = new JsonResult<View_docvsbusiness>();
            string configid = DTRequest.GetFormString("configid");
            int docid = DTRequest.GetFormInt("doctorid");
            int bid = DTRequest.GetFormInt("bid");//businessid
                                                  // BLL.yjr_doctor_vsbusinessconfig bll = new BLL.yjr_doctor_vsbusinessconfig();
            BLL.View_docvsbusiness bll = new BLL.View_docvsbusiness();
            string strwhere = " 1=1 ";
            if (configid != "")
            {
                strwhere += " and id='" + configid + "'";
            }
            else
            {
                strwhere += " and doctorid="+docid;
                if (bid != 0)
                {
                    strwhere += " and businessid="+bid;
                }
            }

            var list = bll.GetModelList(strwhere);
            res.JsonEntity = list;
            res.ResultCode = 1;
            res.ResultCount = list.Count;
            res.ResultMessage = "sucess";

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}