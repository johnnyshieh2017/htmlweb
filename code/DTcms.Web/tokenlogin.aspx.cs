﻿using DTcms.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DTcms.Web
{
    public partial class tokenlogin : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            var token = DTcms.Common.DTRequest.GetQueryString("usertoken");
            var return_url = DTcms.Common.DTRequest.GetQueryString("url");
            if (token != "")
            {
                BLL.yjr_user_uservstoken bll = new BLL.yjr_user_uservstoken();
                var model = bll.GetModelList("token='"+ token + "'").FirstOrDefault();
                if (model != null && model.expires > System.DateTime.Now)
                {
                    BLL.users blluser = new BLL.users();
                    var user = blluser.GetModel(model.userid);
                    if (user != null)
                    {
                        HttpContext.Current.Session[DTKeys.SESSION_USER_INFO] = user;
                    }
                }
            }

            if (!string.IsNullOrEmpty(return_url))
            {
                this.Response.Redirect(HttpUtility.UrlDecode(return_url));
            }
            else
            {
                this.Response.Redirect(hd_login_return_defaulturl.Value);
            }
        }
    }
}