﻿var $ymxul = $('.YMX-ul');
var $scanBox = $('.scan-box');
var $scanBoxCon = $scanBox.find('.scan-con');
var validtime = ''; // 过期时间
var inviteCardMods = [], ymxCardMods = [];
// 已选择提示容器
var $choosedBox = $('<div class="chooseed-box"><img src="/images/choosed-icon.png" /></div>');

var inviteCardObj = {
    isLoading: true,
    bgurl: bgurl,
    isYMX: false, // 
    // 获取数据邀请卡数据
    getInvitecardData: function (data) {
        this.liveTimeVal = data.time;
        this.liveFromVal = data.from;
        this.liveTitleVal = data.title;
        validtime = data.validtime;
        this.username = data.username
        this.remark = data.remark;
        this.isFreeVer = !data.liveroomtype; // 是否为免费版直播间
        // 邀请卡类型
        this.liveType = data.type;//topic
        // 设置分享语句
        this.shareTxt = '邀请您来一起来体验';
        // 封面 头像 二维码
        this.drawImgs = [data.poster || '', data.userhead || '', data.qrcode || ''];
        
        console.log("开始this.isYMX：" + this.isYMX);
     
            initCradModsFun();
          
            this.coverImg = data.poster;
           
            this.draw(inviteCardMods[9], 9);
            // this.draw(inviteCardMods[7], 7);
            $('.module .module-list .coustom').next().append($choosedBox);
            // 设置二维码
            $('.scan-box .scan-con-msg > img').attr('src', data.qrcode);
      

    },
    // 设置绘制文字样式
    setDrawtxtStyle: function (ctx, item, callback) {
        if (item.color) {
            ctx.fillStyle = item.color;
        }
        if (item.fontSize) {
            ctx.font = 'normal normal ' + (item.isBold ? 'bold ' : 'normal ') + item.fontSize + 'px Microsoft YaHei';
            typeof callback === 'function' && callback();
        }
    },
    // 文字换行
    textPrewrap: function (ctx, content, drawX, drawY, lineHeight, lineMaxWidth, lineNum) {
        var drawTxt = '';
        var drawLine = 1;
        var drawIndex = 0;
        lineNum = lineNum || 4;

        // 判断内容是否为多行
        if (ctx.measureText(content).width <= lineMaxWidth) {
            ctx.fillText(content.substring(drawIndex, i), drawX, drawY);
        } else {
            for (var i = 0; i < content.length; i++) {
                drawTxt += content[i];
                if (ctx.measureText(drawTxt).width >= lineMaxWidth) {
                    if (drawLine >= lineNum) {
                        ctx.fillText(content.substring(drawIndex, i) + '..', drawX, drawY);
                        break;
                    } else {
                        ctx.fillText(content.substring(drawIndex, i + 1), drawX, drawY);
                        drawIndex = i + 1;
                        drawLine += 1;
                        drawY += lineHeight;
                        drawTxt = '';
                    }
                } else {
                    // 内容绘制完毕，但是剩下的内容宽度不到lineMaxWidth
                    if (i === content.length - 1) {
                        ctx.fillText(content.substring(drawIndex), drawX, drawY);
                    }
                }
            }
        }
    },
    // 绘制画布
    draw: function (obj, type) {

        console.log("绘制画布 draw: function obj:");
        console.log(obj);

        var self = this;
        // 创建画布
        var canvas = document.createElement('canvas');
        var ctx = canvas.getContext('2d');
        var isYMX = self.isYMX;
        canvas.width = 750;
        canvas.height = 1344;

        // 绘制背景
        var cardBg = new Image();
        cardBg.src = self.bgurl;//'/img/card-bg1.jpg';//'/img/card-bg8.jpg';//self.bgurl;
        cardBg.onload = function () {
            ctx.drawImage(cardBg, 0, 0, canvas.width, canvas.height);

            // 绘制标题
            if ((type && self.liveTitleVal) || isYMX) {
                ctx.save();
                //if (!isYMX && (type === 2 || type === 3 || type === 8 || type === 9 || type === 10)) {
                //    ctx.textAlign = 'center';
                //}
                if (!isYMX && (type === 1 || type === 2 || type === 3 || type === 4 || type === 8 || type === 9 || type === 10 || type === 12)) {
                    ctx.textAlign = 'center';
                }
                obj.liveTitle.isBold = true;
                self.setDrawtxtStyle(ctx, obj.liveTitle);
                console.log("aaaaaaaaaaaaaaaaaaaaaaaaa:" + obj.liveTitle);
                // self.setDrawtxtStyle(ctx, "测试数据");
                console.log("bbbbbbbbbbbbbbbbbbbbbbb:" + self.liveTitleVal);// 输出标题
                self.textPrewrap(ctx, self.liveTitleVal, obj.liveTitle.x, obj.liveTitle.y, obj.liveTitle.distance, obj.liveTitle.maxWidth, isYMX ? 2 : obj.liveTitle.lineNum);
                ctx.restore();
            }

            // 主播名字背景
            if (type && self.username && type === 6) {
                ctx.save();
                ctx.font = obj.name.fontSize + 'px Microsoft YaHei';
                ctx.fillStyle = '#071474';
                var nameBgW = ctx.measureText(self.username.length > 10 ? self.username.substring(0, 8) + '...' : self.username).width;


                ctx.fillRect(obj.name.x - nameBgW - 4, obj.name.y - 30, nameBgW + 8, 36);
                ctx.restore();
            }

           

            // 封面图背景
            if (type && obj.coverBg) {
                ctx.save();
                self.setDrawtxtStyle(ctx, obj.coverBg);
                ctx.fillRect(obj.imgsArray[0].x - obj.coverBg.disX, obj.imgsArray[0].y - obj.coverBg.disY, obj.imgsArray[0].width + (obj.coverBg.disX * 2), obj.imgsArray[0].height + (obj.coverBg.disY * 2));
                ctx.restore();
            }

            

           

            // 绘制分享语句
            console.log("开始self.shareTxt：" + self.shareTxt);
            if (self.shareTxt) {
                ctx.save();
                if (isYMX) {
                    if (!type || type === 1) {
                        ctx.textAlign = 'right';
                    } else if (type === 2 || type === 3 || type === 4 || type === 9 || type === 10 || type === 12) {
                        ctx.textAlign = 'center';
                    }
                } else {
                    if (type === 2 || type === 5 || type === 6 || type === 9 || type === 11 || type === 12) {
                        ctx.textAlign = 'right'
                    } else if (type === 3 || type === 4 || type === 8 || type === 10) {
                        ctx.textAlign = 'center';
                    }
                }
                self.setDrawtxtStyle(ctx, obj.share)
                ctx.fillText(self.shareTxt, obj.share.x, obj.share.y);
                ctx.restore();
            }

            // 绘制主播名字
            ctx.save();

            console.log("开始isYMX：" + isYMX);
            if (isYMX) {
                if (!type || type === 1) {
                    ctx.textAlign = 'right';
                } else if (type === 2 || type === 3 || type === 4 || type === 9 || type === 10 || type === 12) {
                    ctx.textAlign = 'center';
                }
            } else {
                //isYMX=false
                if (type === 2 || type === 5 || type === 6 || type === 9 || type === 11 || type === 12) {
                    ctx.textAlign = 'right';
                } else if (type === 3 || type === 4 || type === 8 || type === 10) {
                    ctx.textAlign = 'center';
                }
            }
            self.setDrawtxtStyle(ctx, obj.name);
            ctx.fillText(self.username.length > 10 ? self.username.substring(0, 8) + '...' : self.username, obj.name.x, obj.name.y);
            ctx.restore();

            // 绘制头像背景
            if (isYMX || (!isYMX && (type === 2 || type === 3 || type === 6 || type === 9 || type === 10 || type === 12))) {
                ctx.save();
                ctx.strokeStyle = obj.headImgBg.color;
                ctx.arc(obj.imgsArray[isYMX ? 0 : 1].arcX, obj.imgsArray[isYMX ? 0 : 1].arcY, obj.imgsArray[isYMX ? 0 : 1].arcR + obj.headImgBg.dis, 0, 2 * Math.PI);
                ctx.fill();
                ctx.stroke();
            }

            // 绘制直播时间
            if (self.liveType === 'topic' && ((type && self.liveTimeVal) || isYMX)) {
                ctx.save();
                if (!isYMX && (type === 2 || type === 3 || type === 8 || type === 9 || type === 10 || type === 12)) {
                    ctx.textAlign = 'center';
                }
                self.setDrawtxtStyle(ctx, obj.liveTime);
                //绘制直播时间 中下部 小字部分
                ctx.fillText('活动截止时间：' + self.liveTimeVal, obj.liveTime.x, obj.liveTime.y);
                ctx.restore();
            }

            // 绘制直播间介绍
            if (isYMX) {
                ctx.save();
                self.setDrawtxtStyle(ctx, obj.desVal);
                self.textPrewrap(ctx, self.remark, obj.desVal.x, obj.desVal.y, obj.desVal.distance, obj.desVal.maxWidth);
                ctx.restore();
            }

            // 绘制直播方信息
            // From --惠医苑 名医直播间
            if (type && !isYMX && self.liveFromVal) {
                if (self.liveType === 'topic' || self.liveType === 'channel') {
                    var fromX = self.liveType === 'channel' ? obj.liveTime.x : obj.liveFrom.x;
                    var fromY = self.liveType === 'channel' ? obj.liveTime.y : obj.liveFrom.y;
                    ctx.save();
                    self.setDrawtxtStyle(ctx, obj.liveFrom);
                    if (type === 2 || type === 3 || type === 8 || type === 9 || type === 10 || type === 12) {
                        ctx.textAlign = 'center';
                    }
                    if (type === 8) {
                        ctx.fillText('<' + (self.liveFromVal.length > 15 ? self.liveFromVal.substring(0, 14) + '...' : self.liveFromVal) + '>', fromX, fromY);
                    } else {
                        ctx.fillText('From — ' + (self.liveFromVal.length > 15 ? self.liveFromVal.substring(0, 14) + '...' : self.liveFromVal), fromX, fromY);
                    }
                    ctx.restore();
                }
            }

            // 绘制封面图图片、用户头像图片、二维码图片
            var drawImgsArr = [];
            var loadNum = 0;
            self.drawImgs.forEach(function (item, index) {
                drawImgsArr[index] = new Image();
                drawImgsArr[index].src = item;
                drawImgsArr[index].onload = function () {
                    loadNum += 1;
                    if (loadNum === self.drawImgs.length) {
                        for (var imgInd = 0; imgInd < drawImgsArr.length; imgInd++) {
                            var imgItem = drawImgsArr[imgInd];
                            if (imgItem.src) {
                                ctx.save();
                                if (isYMX) {
                                    if (!imgInd) {
                                        ctx.arc(obj.imgsArray[imgInd].arcX, obj.imgsArray[imgInd].arcY, obj.imgsArray[imgInd].arcR, 0, 2 * Math.PI);
                                        ctx.clip();
                                    }
                                } else {
                                    if (imgInd === 1) {
                                        if (!type || type === 1 || type === 2 || type === 3 || type === 5 || type === 6 || type === 8 || type === 9 || type === 10 || type === 11 || type === 12) {
                                            ctx.arc(obj.imgsArray[1].arcX, obj.imgsArray[1].arcY, obj.imgsArray[1].arcR, 0, 2 * Math.PI);
                                            ctx.clip();
                                        }
                                    }
                                }
                                ctx.drawImage(imgItem, obj.imgsArray[imgInd].x, obj.imgsArray[imgInd].y, obj.imgsArray[imgInd].width, obj.imgsArray[imgInd].height);
                                ctx.restore();
                            }
                        }
                        $('.card-img').attr('src', canvas.toDataURL('image/jpeg'));
                        $('.module-loading').css('visibility', 'hidden');
                        self.isLoading = false;
                        if ($('.invite-loading').is(':visible')) {
                            $('.invite-loading').hide();
                            canvas = null;
                        }
                    }
                }
            })
        }
    }
};



// 非云美学邀请卡模板
function initCradModsFun() {
    inviteCardMods = [
        {
            share: {
                x: 90,
                y: 176,
                color: tplname === 'Wedding' ? '#fff' : '#747474',
                fontSize: 28
            },
            liveTitle: {
                value: ''
            },
            name: {
                x: 90,
                y: 134,
                fontSize: 34,
                color: '#353535'
            },
            liveTime: {
                value: ''
            },
            coverBg: {
                color: ''
            },
            liveFrom: {
                value: ''
            },
            imgsArray: [
                {
                    x: 0,
                    y: 0,
                    width: 0,
                    height: 0
                },
                {
                    x: 550,
                    y: 84,
                    width: 105,
                    height: 105,
                    arcX: 605,
                    arcY: 139,
                    arcR: 51
                },
                {
                    x: 80,
                    y: 1084,
                    width: 150,
                    height: 150
                }
            ]
        },
        {
            liveTitle: {
                x: 375,
                y: 652,
                maxWidth: 570,
                distance: 59,
                fontSize: 50,
                color: tplname === 'Wedding' ? '#fff' : '#0084ff'
            },
            name: {
                x: 90,
                y: 134,
                fontSize: 34,
                color: tplname === 'Wedding' ? '#fff' : '#353535'
            },
            liveTime: {
                x: 90,
                y: 894,
                fontSize: 28,
                color: tplname === 'Wedding' ? '#fff' : '#747474'
            },
            coverBg: {
                color: '#f0f0f0',
                disX: 1,
                disY: 1
            },
            share: {
                x: 90,
                y: 176,
                color: tplname === 'Wedding' ? '#fff' : '#747474',
                fontSize: 28
            },
            liveFrom: {
                x: 90,
                y: 940,
                fontSize: 28,
                color: tplname === 'Wedding' ? '#fff' : '#747474'
            },
            title: {
                titleArr: [
                    { x: 90, y: 898 },
                    { x: 90, y: 834 }
                ]
            },
            imgsArray: [
                {
                    x: inviteCardObj.liveType === 'liveroom' ? 220 : 97,
                    y: inviteCardObj.liveType === 'liveroom' ? 296 : 283,
                    width: inviteCardObj.liveType === 'liveroom' ? 305 : 552,
                    height: inviteCardObj.liveType === 'liveroom' ? 305 : 310
                },
                {
                    x: 550,
                    y: 84,
                    width: 105,
                    height: 105,
                    arcX: 605,
                    arcY: 139,
                    arcR: 51,
                },
                {
                    x: 80,
                    y: tplname === 'Wedding' ? 1092 : 1084,
                    width: tplname === 'Wedding' ? 135 : 150,
                    height: tplname === 'Wedding' ? 135 : 150
                }
            ]
        },
        {
            liveTitle: {
                x: 375,
                y: 662,
                maxWidth: 550,
                distance: 59,
                fontSize: 50,
                color: tplname === 'Wedding' ? '#C69A54' : '#0084ff'
            },
            name: {
                x: 710,
                y: 133,
                fontSize: 34,
                color: tplname === 'Wedding' ? '#C69A54' : '#fff'
            },
            headImgBg: {
                color: tplname === 'Wedding' ? '#C69A54' : '#91969e',
                dis: 3
            },
            liveTime: {
                x: 375,
                y: 922,
                fontSize: 28,
                color: tplname === 'Wedding' ? '#C69A54' : '#747474'
            },
            coverBg: {
                color: tplname === 'Wedding' ? '#C69A54' : '#f0f0f0',
                disX: 1,
                disY: 1
            },
            share: {
                x: 710,
                y: 176,
                color: tplname === 'Wedding' ? '#fff' : '#747474',
                fontSize: 28
            },
            liveFrom: {
                x: 375,
                y: 966,
                fontSize: 28,
                color: tplname === 'Wedding' ? '#C69A54' : '#747474'
            },
            title: {
                titleArr: [
                    { x: 101, y: 922 },
                    { x: 314, y: 962 }
                ]
            },
            imgsArray: [
                {
                    x: inviteCardObj.liveType === 'liveroom' ? 220 : 97,
                    y: inviteCardObj.liveType === 'liveroom' ? 300 : 265,
                    width: inviteCardObj.liveType === 'liveroom' ? 305 : 552,
                    height: inviteCardObj.liveType === 'liveroom' ? 305 : 310
                },
                {
                    x: 40,
                    y: 85,
                    width: 105,
                    height: 105,
                    arcX: 91,
                    arcY: 139,
                    arcR: 47,
                },
                {
                    x: 80,
                    y: tplname === 'Wedding' ? 1074 : inviteCardObj.liveType === 'liveroom' ? 1057 : 1060,
                    width: tplname === 'Wedding' ? 132 : 150,
                    height: tplname === 'Wedding' ? 132 : 150
                }
            ]
        },
        {
            liveTitle: {
                x: 375,
                y: 451,
                maxWidth: 540,
                distance: 48,
                fontSize: 45,
                color: tplname === 'Wedding' ? '#980000' : '#009e98'
            },
            name: {
                x: 375,
                y: 367,
                fontSize: 26,
                color: '#4B4B4B' //adsa
            },
            headImgBg: {
                color: '#FFF8E3',
                dis: 10
            },
            liveTime: {
                x: 375,
                y: 642,
                fontSize: 24,
                color: '#4B4B4B'
            },
            coverBg: {
                color: tplname === 'Wedding' ? '#4B4B4B' : '#f0f0f0',
                disX: 1,
                disY: 1
            },
            share: {
                x: 375,
                y: 400,
                color: '#4b4b4b',
                fontSize: 26
            },
            liveFrom: {
                x: 375,
                y: 677,
                fontSize: 24,
                color: '#4b4b4b'
            },
            title: {
                fontSize: 24,
                color: '#4b4b4b',
                titleArr: [
                    { x: 148, y: 642 },
                    { x: 327, y: 682 }
                ]
            },
            imgsArray: [
                {
                    x: inviteCardObj.liveType === 'liveroom' ? 222 : 99,
                    y: 696,
                    width: inviteCardObj.liveType === 'liveroom' ? 305 : 552,
                    height: inviteCardObj.liveType === 'liveroom' ? 305 : 312
                },
                {
                    x: 293,
                    y: 185,
                    width: 160,
                    height: 150,
                    arcX: 375,
                    arcY: 263,
                    arcR: 62,
                },
                {
                    x: 285,
                    y: 1055,
                    width: 181,
                    height: 177
                }
            ]
        },
        {
            liveTitle: {
                x: 375,
                y: 630,
                maxWidth: 574,
                distance: 52,
                fontSize: 50,
                color: tplname === 'Wedding' ? '#F0E6A8' : '#fff'
            },
            name: {
                x: 250,
                y: 1198,
                fontSize: 24,
                color: tplname === 'Wedding' ? '#F0E6A8' : '#fff'
            },
            liveTime: {
                x: 92,
                y: 836,
                fontSize: 28,
                color: tplname === 'Wedding' ? '#F0E6A8' : '#ffe600'
            },
            coverBg: {
                color: tplname === 'Wedding' ? '#F0E6A8' : '#f0f0f0',
                disX: 1,
                disY: 1
            },
            share: {
                x: 250,
                y: 1227,
                color: tplname === 'Wedding' ? '#F0E6A8' : '#fff',
                fontSize: 24
            },
            liveFrom: {
                x: 92,
                y: 879,
                fontSize: 28,
                color: tplname === 'Wedding' ? '#F0E6A8' : '#ffe600'
            },
            title: {
                color: '#ffe600',
                titleArr: [
                    { x: 94, y: 816 },
                    { x: 94, y: 854 }
                ]
            },
            imgsArray: [
                {
                    x: inviteCardObj.liveType === 'liveroom' ? 220 : 100,
                    y: inviteCardObj.liveType === 'liveroom' ? 265 : 257,
                    width: inviteCardObj.liveType === 'liveroom' ? 305 : 549,
                    height: inviteCardObj.liveType === 'liveroom' ? 305 : 308
                },
                {
                    x: 170,
                    y: 996,
                    width: 160,
                    height: 161,
                },
                {
                    x: 408,
                    y: 998,
                    width: 160,
                    height: 160
                }
            ]
        },
        {
            liveTitle: {
                x: 90,
                y: 477,
                maxWidth: 576,
                distance: 70,
                fontSize: 40,
                color: '#353535'
            },
            name: {
                x: 659,
                y: inviteCardObj.liveType === 'liveroom' ? 248 : 211,
                fontSize: 34,
                color: '#353535'
            },
            liveTime: {
                x: 90,
                y: 753,
                fontSize: 28,
                color: '#747474'
            },
            share: {
                x: 659,
                y: inviteCardObj.liveType === 'liveroom' ? 298 : 261,
                color: '#747474',
                fontSize: 28
            },
            liveFrom: {
                x: 90,
                y: 791,
                fontSize: 28,
                color: '#747474'
            },
            title: {
                titleArr: [
                    { x: 90, y: 760 },
                    { x: 90, y: 800 }
                ]
            },
            imgsArray: [
                {
                    x: 0,
                    y: 0,
                    width: 0,
                    height: 0
                },
                {
                    x: 90,
                    y: inviteCardObj.liveType === 'liveroom' ? 179 : 142,
                    width: 150,
                    height: 150,
                    arcX: 165,
                    arcY: inviteCardObj.liveType === 'liveroom' ? 255 : 217,
                    arcR: 75
                },
                {
                    x: 80,
                    y: inviteCardObj.liveType === 'liveroom' ? 957 : 955,
                    width: 150,
                    height: 150
                }
            ]
        },
        {
            liveTitle: {
                x: 74,
                y: 274,
                maxWidth: 574,
                distance: 70,
                fontSize: 40,
                color: '#071474'
            },
            name: {
                x: 641,
                y: inviteCardObj.liveType === 'liveroom' ? 1116 : 1153,
                fontSize: 34,
                color: '#fff'
            },
            liveTime: {
                x: 80,
                y: 586,
                fontSize: 28,
                color: '#d40019'
            },
            share: {
                x: 641,
                y: inviteCardObj.liveType === 'liveroom' ? 1163 : 1200,
                color: '#fff',
                fontSize: 28
            },
            headImgBg: {
                color: '#071474',
                dis: 10
            },
            liveFrom: {
                x: 80,
                y: 626,
                fontSize: 28,
                color: '#d40019'
            },
            title: {
                color: '#d40019',
                titleArr: [
                    { x: 77, y: 586 },
                    { x: 77, y: 619 }
                ]
            },
            imgsArray: [
                {
                    x: 0,
                    y: 0,
                    width: 0,
                    height: 0
                },
                {
                    x: 70,
                    y: inviteCardObj.liveType === 'liveroom' ? 1061 : 1095,
                    width: inviteCardObj.liveType === 'liveroom' ? 140 : 132,
                    height: inviteCardObj.liveType === 'liveroom' ? 140 : 132,
                    arcX: 136,
                    arcY: inviteCardObj.liveType === 'liveroom' ? 1125 : 1160,
                    arcR: 55
                },
                {
                    x: 70,
                    y: 785,
                    width: 137,
                    height: 134
                }
            ]
        },
        {
            liveTitle: {
                x: 106,
                y: inviteCardObj.liveType === 'liveroom' ? 578 : 548,
                maxWidth: 538,
                distance: 63,
                fontSize: 39,
                color: '#353535'
            },
            name: {
                x: 104,
                y: 389,
                fontSize: 35,
                color: '#353535'
            },
            liveTime: {
                x: 108,
                y: 829,
                fontSize: 28,
                color: '#747474'
            },
            share: {
                x: 104,
                y: 428,
                color: '#747474',
                fontSize: 26
            },
            liveFrom: {
                x: 108,
                y: 870,
                fontSize: 28,
                color: '#747474'
            },
            title: {
                titleArr: [
                    { x: 106, y: 821 },
                    { x: 106, y: 859 }
                ]
            },
            imgsArray: [
                {
                    x: 0,
                    y: 0,
                    width: 0,
                    height: 0
                },
                {
                    x: 498,
                    y: 320,
                    width: 155,
                    height: 155,
                    arcX: 576,
                    arcY: 398,
                    arcR: 75
                },
                {
                    x: 106,
                    y: inviteCardObj.liveType === 'liveroom' ? 1028 : 1008,
                    width: 159,
                    height: 159
                }
            ]
        },
        {
            liveTitle: {
                x: 375,
                y: 640,
                maxWidth: 540,
                distance: 60,
                fontSize: 49,
                color: '#101010'
            },
            name: {
                x: 375,
                y: 511,
                fontSize: 35,
                color: '#353535'
            },
            liveTime: {
                x: 375,
                y: 926,
                fontSize: 26,
                color: '#747474'
            },
            share: {
                x: 375,
                y: 565,
                color: '#747474',
                fontSize: 26
            },
            liveFrom: {
                x: 375,
                y: 968,
                fontSize: 26,
                color: '#747474'
            },
            title: {
                fontSize: 26,
                titleArr: [
                    { x: 375, y: 934 },
                    { x: 375, y: 975 }
                ]
            },
            imgsArray: [
                {
                    x: 0,
                    y: 0,
                    width: 0,
                    height: 0
                },
                {
                    x: 281,
                    y: 278,
                    width: 188,
                    height: 188,
                    arcX: 375,
                    arcY: 367,
                    arcR: 80
                },
                {
                    x: 195,
                    y: 1022,
                    width: 182,
                    height: 182
                }
            ]
        },
        {
            liveTitle: {
                x: 375,
                y: 642,
                maxWidth: 540,
                distance: 59,
                fontSize: 50,
                color: '#A32616'
            },
            name: {
                x: 710,
                y: 133,
                fontSize: 34,
                color: '#fff'
            },
            headImgBg: {
                color: '#91969e',
                dis: 3
            },
            liveTime: {
                x: 375,
                y: 922,
                fontSize: 28,
                color: '#747474'
            },
            coverBg: {
                color: '#f0f0f0',
                disX: 1,
                disY: 1
            },
            share: {
                x: 710,
                y: 176,
                color: '#fff',
                fontSize: 28
            },
            liveFrom: {
                x: 375,
                y: 966,
                fontSize: 28,
                color: '#747474'
            },
            title: {
                titleArr: [
                    { x: 101, y: 922 },
                    { x: 314, y: 962 }
                ]
            },
            imgsArray: [
                {
                    x: inviteCardObj.liveType === 'liveroom' ? 220 : 97,
                    y: 265,
                    width: inviteCardObj.liveType === 'liveroom' ? 305 : 552,
                    height: inviteCardObj.liveType === 'liveroom' ? 305 : 310
                },
                {
                    x: 40,
                    y: 85,
                    width: 105,
                    height: 105,
                    arcX: 91,
                    arcY: 139,
                    arcR: 47,
                },
                {
                    x: 80,
                    y: inviteCardObj.liveType === 'liveroom' ? 1057 : 1073,
                    width: 135,
                    height: 135
                }
            ]
        },
        {
            liveTitle: {
                x: 375,
                y: 451,
                maxWidth: 540,
                distance: 48,
                fontSize: 45,
                color: '#C22800'
            },
            name: {
                x: 375,
                y: 367,
                fontSize: 26,
                color: '#4B4B4B' //adsa
            },
            headImgBg: {
                color: '#FFF8E3',
                dis: 10
            },
            liveTime: {
                x: 375,
                y: 632,
                fontSize: 24,
                color: '#4B4B4B'
            },
            coverBg: {
                color: '#f0f0f0',
                disX: 1,
                disY: 1
            },
            share: {
                x: 375,
                y: 400,
                color: '#4b4b4b',
                fontSize: 26
            },
            liveFrom: {
                x: 375,
                y: 667,
                fontSize: 24,
                color: '#4b4b4b'
            },
            title: {
                color: '#4b4b4b',
                fontSize: 24,
                titleArr: [
                    { x: 148, y: 642 },
                    { x: 327, y: 682 }
                ]
            },
            imgsArray: [
                {
                    x: inviteCardObj.liveType === 'liveroom' ? 222 : 99,
                    y: 696,
                    width: inviteCardObj.liveType === 'liveroom' ? 305 : 552,
                    height: inviteCardObj.liveType === 'liveroom' ? 305 : 312
                },
                {
                    x: 293,
                    y: 185,
                    width: 160,
                    height: 150,
                    arcX: 375,
                    arcY: 263,
                    arcR: 62,
                },
                {
                    x: 285,
                    y: 1055,
                    width: 181,
                    height: 177
                }
            ]
        },
        {
            liveTitle: {
                x: 90,
                y: 477,
                maxWidth: 576,
                distance: 70,
                fontSize: 40,
                color: '#fff'
            },
            name: {
                x: 659,
                y: inviteCardObj.liveType === 'liveroom' ? 248 : 211,
                fontSize: 34,
                color: '#fff'
            },
            liveTime: {
                x: 90,
                y: 753,
                fontSize: 28,
                color: '#fff'
            },
            share: {
                x: 659,
                y: inviteCardObj.liveType === 'liveroom' ? 298 : 261,
                color: '#fff',
                fontSize: 28
            },
            liveFrom: {
                x: 90,
                y: 791,
                fontSize: 28,
                color: '#fff'
            },
            title: {
                color: '#fff',
                titleArr: [
                    { x: 90, y: 760 },
                    { x: 90, y: 800 }
                ]
            },
            imgsArray: [
                {
                    x: 0,
                    y: 0,
                    width: 0,
                    height: 0
                },
                {
                    x: 90,
                    y: inviteCardObj.liveType === 'liveroom' ? 179 : 142,
                    width: 150,
                    height: 150,
                    arcX: 165,
                    arcY: inviteCardObj.liveType === 'liveroom' ? 255 : 217,
                    arcR: 75
                },
                {
                    x: 80,
                    y: inviteCardObj.liveType === 'liveroom' ? 957 : 965,
                    width: 134,
                    height: 134
                }
            ]
        },
        {
            liveTitle: {
                x: 375,
                y: 642,
                maxWidth: 540,
                distance: 59,
                fontSize: 50,
                color: '#F7A106'
            },
            name: {
                x: 710,
                y: 133,
                fontSize: 34,
                color: '#fff'
            },
            headImgBg: {
                color: '#91969e',
                dis: 3
            },
            liveTime: {
                x: 375,
                y: 922,
                fontSize: 28,
                color: '#747474'
            },
            coverBg: {
                color: '#f0f0f0',
                disX: 1,
                disY: 1
            },
            share: {
                x: 710,
                y: 176,
                color: '#fff',
                fontSize: 28
            },
            liveFrom: {
                x: 375,
                y: 966,
                fontSize: 28,
                color: '#747474'
            },
            title: {
                titleArr: [
                    { x: 101, y: 922 },
                    { x: 314, y: 962 }
                ]
            },
            imgsArray: [
                {
                    x: inviteCardObj.liveType === 'liveroom' ? 220 : 97,
                    y: 265,
                    width: inviteCardObj.liveType === 'liveroom' ? 305 : 552,
                    height: inviteCardObj.liveType === 'liveroom' ? 305 : 310
                },
                {
                    x: 40,
                    y: 85,
                    width: 105,
                    height: 105,
                    arcX: 91,
                    arcY: 139,
                    arcR: 47,
                },
                {
                    x: 80,
                    y: inviteCardObj.liveType === 'liveroom' ? 1047 : 1063,
                    width: 135,
                    height: 135
                }
            ]
        }
    ];
}

// 获取数据
function createInviteCard(url, data) {
    console.log("cccccccccccccccccc:" + url);


    $.post(url, data, function (res) {
        var datas = res.dataObj;
        if (res.isok) {
            console.log("ddddddddddddddd:");
            console.log(datas);
            inviteCardObj.getInvitecardData(datas);
        } else {
            $(document).minTipsBox({
                tipsContent: res.Msg,
                tipsTime: 1
            });
        }
    })
}








$(document).ready(function () {
    if (datajson.isok) {
        console.log("inviteCardObj.getInvitecardData:")
        console.log(datajson.dataObj);
        inviteCardObj.getInvitecardData(datajson.dataObj);
    } else {
        $(document).minTipsBox({
            tipsContent: datajson.Msg,
            tipsTime: 1
        });
    }

      $('.invite-con img').click(function (event) {
        event.preventDefault();
    })
});
