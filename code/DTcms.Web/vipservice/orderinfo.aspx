﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="orderinfo.aspx.cs" Inherits="DTcms.Web.vipservice.orderinfo" %>

<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <link rel="stylesheet" href="css/weui.css"/>
    <link rel="stylesheet" href="css/weuix.css"/>

    <script src="js/zepto.min.js"></script>
    <script src="js/zepto.weui.js"></script>
    <script>
        $(function(){
               $(document).on("click","#btn",function(){
                   var user = $("#user").val();
                   if(user==""){
                       $.toptip("用户名不能为空");
                       return false;
                   }else{
                       $.toptip("提交成功",'success');
                   }
               })
			   //解决表单控件不能回弹 只有微信ios有这个问题
           $("input,select,textarea").blur(function(){
setTimeout(() => {
const scrollHeight = document.documentElement.scrollTop || document.body.scrollTop || 0;
window.scrollTo(0, Math.max(scrollHeight - 1, 0));
}, 100);
})    
  
        });
        
 

       

        function textarea(input) {
            var content = $(input);
              var max =  content.next().find('i') .text();
            var value = content.val();
            if (value.length>0) {

                value = value.replace(/\n|\r/gi,"");
                var len = value.length;
                content.next().find('span').text(len) ;
                 if(len>max){
                     content.next().addClass('f-red');
                 }else{
                     content.next().removeClass('f-red');
                 }
            }
        }

        function cleartxt(obj){
            $(obj).prev().find('.weui-input').val("");
            return false;
        }

        function cleararea(obj){
            $(obj).prev().find('.weui-textarea').val("").next().find("span").text(0);
            return false;
        }

    </script>
</head>

<body ontouchstart>
<div class="page-hd">
    <h1 class="page-hd-title">
        订单详情
    </h1>
    <p class="page-hd-desc"></p>
</div>

<div class="weui-cells__title"><label class="weui-label">贴心陪诊</label></div>

<div class="weui-cells weui-cells_form">
    <div class="weui-cell">
        <div class="weui-cell__hd"><label class="weui-label">客户姓名</label></div>
        <div class="weui-cell__bd">
            王冬梅
        </div>
    </div>
    <div class="weui-cell">
        <div class="weui-cell__hd">
            <label class="weui-label">手机号码</label>
        </div>
        <div class="weui-cell__bd">
           131861800
        </div>

    </div>
   
    <div class="weui-cell">
        <div class="weui-cell__hd"><label class="weui-label">预约日期</label></div>
        <div class="weui-cell__bd">
            2020年12月11日
        </div>
    </div>
    <div class="weui-cell">
        <div class="weui-cell__hd"><label class="weui-label">备注</label></div>
        <div class="weui-cell__bd">
            <input class="weui-input" value="" placeholder="" type="datetime-local">
        </div>
    </div>

    
</div>



   <div class="weui-cells">

 <div class="weui-cell">
        <div class="weui-cell__hd"><label class="weui-label">客户经理</label></div>
        <div class="weui-cell__bd">
            白小林
        </div>
    </div>
    <div class="weui-cell">
        <div class="weui-cell__hd">
            <label class="weui-label">联系电话</label>
        </div>
        <div class="weui-cell__bd">
           18978945612
        </div>

    </div>
   
    <div class="weui-cell">
        <div class="weui-cell__hd"><label class="weui-label">预约告知</label></div>
        <div class="weui-cell__bd">
            2020年12月11日 13：29：11
        </div>
    </div>
     <div class="weui-cell">
        <div class="weui-cell__hd"><label class="weui-label">服务告知</label></div>
        <div class="weui-cell__bd">
            2020年12月18日 18：29：11
        </div>
    </div>
  
       </div>

<div class="weui-cells">
    <div class="weui-cell">
        <div class="weui-cell__hd"><label class="weui-label">服务开始</label></div>
        <div class="weui-cell__bd">
            2020年12月11日 13：29：11
        </div>
    </div>
    <div class="weui-cell">
        <div class="weui-cell__hd">
            <label class="weui-label">服务结束</label>
        </div>
        <div class="weui-cell__bd">
           2020年12月18日 18：29：11
        </div>

    </div>
    <div class="weui-cell">
        <div class="weui-cell__hd">
            <label class="weui-label">客户评价</label>
        </div>
        <div class="weui-cell__bd">
           非常好
        </div>

    </div>
</div>

<%--<div class="weui-btn-area">
    <a class="weui-btn weui-btn_primary" href="javascript:" id="btn">确定</a>
</div>--%>


<br>
<br>
<br>
<br>
<div class="weui-footer weui-footer_fixed-bottom">
   <%-- <p class="weui-footer__links">
        <a href="../index.html" class="weui-footer__link">WeUI首页</a>
    </p>
    <p class="weui-footer__text">Copyright &copy; Yoby</p>--%>
    <a href="javascript:;" class="weui-btn weui-btn_mini weui-btn_primary">确认接单</a>
    <a href="javascript:;" class="weui-btn weui-btn_mini weui-btn_default">已联系客户</a>
    <a href="javascript:;" class="weui-btn weui-btn_mini weui-btn_primary">已告知经理</a>
    <a href="javascript:;" class="weui-btn weui-btn_mini weui-btn_warn">已告知经理</a>
</div>
</body>
</html>
