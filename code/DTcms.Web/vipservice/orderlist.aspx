﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="orderlist.aspx.cs" Inherits="DTcms.Web.vipservice.orderlist" %>

<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <link rel="stylesheet" href="css/weui.css"/>
    <link rel="stylesheet" href="css/weuix.css"/>

    <script src="js/zepto.min.js"></script>
    <script src="js/zepto.weui.js"></script>
    <script src="js/php.js"></script>
    <script>
        $(function(){
            $('#t1').tab({
                defaultIndex: 0,
                activeClass: 'tab-green',
                onToggle: function (index) {
                    $("#rank-list").html('');
                    ajaxpage(1,index+1);
                    var type = sessionStorage.setItem('type',index+1);
                }
            })

        });

    </script>
</head>

<body ontouchstart>
<div class="page-hd">
    <h1 class="page-hd-title">
      我的订单
    </h1>
  
</div>
<div class="weui-tab" id="t1" style="height:44px;">
    <div class="weui-navbar">
        <div class="weui-navbar__item">
            进行中
        </div>
        <div class="weui-navbar__item">
            已完成
        </div>
    </div>

</div>
<div class="page-bd-15">
    <div class="weui-cells" id="rank-list">
    </div>

    <div class="weui-loadmore" id="more">
        <i class="weui-loading"></i>
        <span class="weui-loadmore__tips">正在加载</span>
    </div>
</div>
    <div class="weui-panel">
           
            <div class="weui-panel__bd">
                <div class="weui-media-box weui-media-box_text">
                    <h4 class="weui-media-box__title">2020-12-21 西京免疫科 王伦天</h4>
                    <p class="weui-media-box__desc">由各种物质组成的巨型球状天体，叫做星球。星球有一定的形状，有自己的运行轨道。</p>
                    <ul class="weui-media-box__info">
                        <li class="weui-media-box__info__meta">文字来源</li>
                        <li class="weui-media-box__info__meta">时间</li>
                        <li class="weui-media-box__info__meta weui-media-box__info__meta_extra">其它信息</li>
                    </ul>
                </div>
            </div>
         <div class="weui-panel__bd">
                <div class="weui-media-box weui-media-box_text">
                    <h4 class="weui-media-box__title">标题一</h4>
                    <p class="weui-media-box__desc">由各种物质组成的巨型球状天体，叫做星球。星球有一定的形状，有自己的运行轨道。</p>
                    <ul class="weui-media-box__info">
                        <li class="weui-media-box__info__meta">文字来源</li>
                        <li class="weui-media-box__info__meta">时间</li>
                        <li class="weui-media-box__info__meta weui-media-box__info__meta_extra">其它信息</li>
                    </ul>
                </div>
            </div>
        </div>
    
     
  
<%--<script id="tpl" type="text/html">
    <% for(var i in list) {   %>
    <div class="weui-cell hand" data-id="<%=list[i].id%>">
        <div class="weui-cell__bd">
            <p><%=list[i].id%>姓名<%=list[i].title%>手机<%=list[i].phone%></p>
        </div>
        <div class="weui-cell__ft"><%=list[i].fen%></div>
    </div>
    <% } %>
</script>--%>
<script>
    var pagesize=15;//每页数据条数
    var page = 1;
    var maxpage;
    $('#more').hide();
    function ajaxpage(page,type){
        $.ajax({
            type : "POST",
            url : '../php/page.php',
            data: {"page":page,"pagesize":pagesize,ajax:2,type:type},
            dataType : "json",
            beforeSend:function(){
                $("#more").show();
            },
            success : function(rs) {
                $("#rank-list").append(tpl(document.getElementById('tpl').innerHTML,rs));
                var maxpage = Math.ceil(rs.total / pagesize);
                sessionStorage['maxpage'] = maxpage;
            },
            timeout : 15000
        });
    }

    $(window).scroll(
        function() {
            var scrollTop = $(this).scrollTop();
            var scrollHeight = $(document).height();
            var windowHeight = $(this).height();
            if (scrollTop + windowHeight == scrollHeight) {
                maxpage = sessionStorage['maxpage'];
                type = sessionStorage.getItem('type')
                if(page<maxpage) {
                    page++;
                    ajaxpage(page,type);
                }else{
                    $("#more").html("没有更多数据了");return false;
                }
            }

        });
    const goto=(id)=>{
        sessionStorage.setItem('index_list',$("#rank-list").html());//存储列表数据
        sessionStorage.setItem('index_page',page);//存储页码
        sessionStorage.setItem('index_scroll',$(window).scrollTop());//存储滚动条位置
        location.href="js91.html?id="+id;
    }
    $(document).on('tap click',"#rank-list .weui-cell",function(){
        id = $(this).data('id');
        goto(id);
    })
    $(function(){
        var l = sessionStorage.getItem('index_list');
        if(null !== l && '' !== l){
            $("#rank-list").html(l);
            $(window).scrollTop(sessionStorage.getItem('index_scroll'));
            page = sessionStorage.getItem('index_page');
            sessionStorage.removeItem('index_list');
            sessionStorage.removeItem('index_page');
            sessionStorage.removeItem('index_scroll');
        }else{
            ajaxpage(1,1);
        }
    })
</script>
    
<br>
<br>
<div class="weui-footer weui-footer_fixed-bottom">
    <p class="weui-footer__links">
        <a href="#" class="weui-footer__link"></a>
    </p>
    <p class="weui-footer__text">Copyright &copy; 惠医苑</p>
</div>
</body>
</html>
