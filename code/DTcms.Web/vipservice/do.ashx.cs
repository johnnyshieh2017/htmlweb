﻿using Aliyun.Acs.Core;
using Aliyun.Acs.Core.Profile;
using Aliyun.Acs.Dysmsapi.Model.V20170525;
using DTcms.Common;
using DTcms.Web.api.Models;
using Newtonsoft.Json;
using NLog;
using qcloudsms_csharp;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace DTcms.Web.vipservice
{
    /// <summary>
    /// Handler1 的摘要说明
    /// </summary>
    public class Handler1 : IHttpHandler, IRequiresSessionState
    {
        Model.userconfig userConfig = new BLL.userconfig().loadConfig();
        Model.sysconfig sysConfig = new BLL.sysconfig().loadConfig();
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public void ProcessRequest(HttpContext context)
        {
            //取得处事类型
            string action = DTRequest.GetQueryString("action");
            switch (action)
            {
                case "getlist_member": // 
                    getlist_member(context);
                    break;
                case "getlist": // 
                    getlist(context);
                    break;
                case "imgcode":
                    getcodeimg(context);
                    break;
                case "sendcode":
                    sendcode(context);
                    break;
                case "updateuser":
                    updateuser(context);
                    break;
                case "getuser":
                    getuser(context);
                    break;
                case "updateorder_status":
                    updateorder_status(context);
                    break;
                case "updateorder_notic":
                    updateorder_notic(context);
                    break;
                case "updateorder":
                    updateorder(context);
                    break;
                case "add_comment":
                    add_comment(context);
                    break;

                case "order_copy":
                    order_copy(context);
                    break;
                case "uploadimg":
                    uploadimg(context);
                    break;
                //updateorder_confirm
                case "updateorder_confirm":
                    updateorder_confirm(context);
                    break;

            }
        }

        private void order_copy(HttpContext context)
        {
            string useropenid = DTcms.Common.DTRequest.GetFormString("useropenid");
            int userid = DTcms.Common.DTRequest.GetFormInt("userid");

            int orderid = DTcms.Common.DTRequest.GetFormInt("orderid");
            string clinic_diagnosis = DTcms.Common.DTRequest.GetFormString("clinic_diagnosis");

            JsonResult<string> res = new JsonResult<string>();
            res.ResultCode = 0;
            res.ResultCount = 0;
            res.ResultMessage = "失败";

            if (orderid != 0)
            {
                BLL.vip_orders bll = new BLL.vip_orders();
                var order = bll.GetModel(orderid);
                if (order!=null && order.status == 10 && order.service_typeid == 2)
                {

                    order.clinic_diagnosis = clinic_diagnosis;
                    order.service_end = System.DateTime.Now;
                    order.status = 15;
                    bll.Update(order);

                    order.id = 0;
                    order.clinic_begin_notic = null;
                    order.clinic_diagnosis = "";
                    order.clinic_end_notic = null;
                    order.service_begin = null;
                    order.service_end = null;
                   

                    order.add_time = System.DateTime.Now;
                    order.parent_id = orderid;
                    order.status = 1;
                    order.pay_status = 0;
                    order.verifycode = Utils.Number(4);


                    if (bll.Add(order) > 0)
                    {
                        // 
                        var server = new BLL.vip_users().GetModel(order.servicer_id);
                        if (!string.IsNullOrEmpty(server.user_phone))
                        {
                            DTcms.BLL.vip_sms smsbll = new BLL.vip_sms();
                            Model.vip_sms sms = new Model.vip_sms();
                            sms.msg_ext = order.order_number;
                            sms.msg_type = 1;
                            sms.msg_content = server.user_truename + "|" + order.customer + "|" + order.clinic_date.ToString("MM月dd日 HH:mm") + "|" + order.clinic_department;
                            sms.msg_mobile = server.user_phone;
                            sms.templateid = "";
                            sms.msg_status = 0;
                            sms.send_date = Convert.ToDateTime(order.clinic_date.AddDays(-1).ToString("yyyy-MM-dd")).AddHours(21);
                            sms.add_time = System.DateTime.Now;
                            if (sms.send_date > System.DateTime.Now)
                            {
                                smsbll.Add(sms);
                            }
                        }

                        if (!string.IsNullOrEmpty(server.user_email))
                        {
                            DTcms.BLL.vip_email emailbll = new BLL.vip_email();
                            Model.vip_email mail = new Model.vip_email();
                            mail.mail_title =  "派单提醒";
                            //mail.mail_content = server.user_truename+"你好!<br/>";
                            mail.mail_content += "您有新的服务订单需要确认，<br/>";
                            mail.mail_content += "客户姓名：" + order.customer + "<br/>";
                            mail.mail_content += "客户电话：" + order.telephone + "<br/>";
                            mail.mail_content += "就诊日期：" + order.clinic_date.ToString("MM月dd日 HH:mm") + "<br/>";
                            mail.mail_content += "就诊科室：" + order.clinic_department + "<br/>";
                            mail.mail_content += "请及时处理，谢谢！";
                            mail.mail_to = server.user_email;
                            mail.mail_type = 1;
                            mail.send_date = System.DateTime.Now.AddMinutes(1);
                            mail.templateid = "";
                            mail.mail_status = 0;
                            mail.mail_ext = order.order_number;
                            mail.add_time = System.DateTime.Now;
                            emailbll.Add(mail);
                        }
                       
                        
                        res.ResultCode = 1;
                        res.ResultCount = 1;
                        res.ResultMessage = "保存成功";
                    }
                }
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }


        private void add_comment(HttpContext context)
        {
            string useropenid = DTcms.Common.DTRequest.GetFormString("useropenid");
            int userid = DTcms.Common.DTRequest.GetFormInt("userid");

            int orderid = DTcms.Common.DTRequest.GetFormInt("orderid");
            string level = DTcms.Common.DTRequest.GetFormString("level");
            string comment = DTcms.Common.DTRequest.GetFormString("comment",true);


            JsonResult<string> res = new JsonResult<string>();
            res.ResultCode = 0;
            res.ResultCount = 0;
            res.ResultMessage = "fails";
            if (useropenid != "" && userid != 0 && orderid != 0)
            {

                BLL.vip_orders bll = new BLL.vip_orders();
                var order = bll.GetModel(orderid);
                if (order != null)
                {
                    if (order.status ==15)
                    {

                        BLL.vip_comment comment_bll = new BLL.vip_comment();
                        if (comment_bll.GetRecordCount("order_id=" + order.id) == 0)
                        {
                            Model.vip_comment model = new Model.vip_comment();
                            model.order_id = order.id;
                            model.comment_level = level;
                            model.comment = comment;
                            model.userid = userid;
                            model.add_time = System.DateTime.Now;
                            comment_bll.Add(model);
                            res.ResultCode = 1;
                            res.ResultCount = 1;
                            res.ResultMessage = "保存成功";
                        }
                        else
                        {
                            res.ResultCode = 0;
                            res.ResultCount = 0;
                            res.ResultMessage = "已存在";
                        }
                       
                    }
                }


            }
            else
            {
                //res.JsonEntity = list;
                res.ResultCode = 0;
                res.ResultCount = 0;
                res.ResultMessage = "fails";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }

        #region 发送手机验证码=================================


        private void sendcode(HttpContext context)
        {
            JsonResult<string> res = new JsonResult<string>();
            res.ResultCode = 0;
            res.ResultCount = 0;
            res.ResultMessage = "fails";

            string useropenid = DTcms.Common.DTRequest.GetFormString("useropenid");
            int userid = DTcms.Common.DTRequest.GetFormInt("userid");

            string mobile = DTRequest.GetFormString("mobile");
            string chkcode = DTRequest.GetFormString("chkcode");

            if (useropenid != "" && userid != 0)
            {
                BLL.vip_users bll = new BLL.vip_users();
                var user = bll.GetModelList("user_wx_openid='" + useropenid + "' and id=" + userid).FirstOrDefault();
                if (user != null)
                {
                   

                    if (string.IsNullOrEmpty(chkcode))
                    {
                        //context.Response.Write("{\"status\":0, \"msg\":\"验证码不能为空！\"}");
                        res.ResultCode = 0;
                        res.ResultMessage = "图形校验码不能为空！";
                        context.Response.ContentType = "application/json";
                        context.Response.Write(JsonConvert.SerializeObject(res));
                        context.Response.End();
                        return;
                    }
                    else
                    {
                        //验证图形验证码
                        if (DTcms.Common.CacheHelper.Get("BPN_V_CODE" + mobile) != null)
                        {
                            if (chkcode.ToLower() != Convert.ToString(DTcms.Common.CacheHelper.Get("BPN_V_CODE" + mobile)).ToLower())
                            {
                                //context.Response.Write("{\"status\":0, \"msg\":\"发送失败，验证码不正确！\"}");
                                res.ResultCode = 0;
                                res.ResultMessage = "图形校验码不正确！";
                                context.Response.ContentType = "application/json";
                                context.Response.Write(JsonConvert.SerializeObject(res));
                                context.Response.End();
                                return;
                            }
                            else
                            {

                            }
                        }
                        else
                        {
                            //context.Response.Write("{\"status\":0, \"msg\":\"系统错误，请刷新后重试！\"}");
                            res.ResultCode = 0;
                            res.ResultMessage = "系统错误，请刷新后重试！";
                            context.Response.ContentType = "application/json";
                            context.Response.Write(JsonConvert.SerializeObject(res));
                            context.Response.End();
                            return;
                        }
                    }

                    //检查手机
                    if (string.IsNullOrEmpty(mobile) || mobile.Length != 11)
                    {
                        //context.Response.Write("{\"status\":0, \"msg\":\"发送失败，请填写手机号码！\"}");
                        res.ResultCode = 0;
                        res.ResultMessage = "系统错误，请填写正确的手机号码！";
                        context.Response.ContentType = "application/json";
                        context.Response.Write(JsonConvert.SerializeObject(res));
                        context.Response.End();
                        return;
                    }

                    BLL.vip_phonecode code_bll = new BLL.vip_phonecode();
                    var usercode = code_bll.GetModelList("table_name='vip_users' and table_pkvalue='" + mobile + "' ").FirstOrDefault();
                    if (usercode == null || usercode.add_time.AddSeconds(60) < System.DateTime.Now)
                    {
                        string strcode = Utils.Number(6); //随机验证码

                        int ret = qcloud_smsvcode_send(mobile, strcode);// ali_smsvcode_send(mobile, strcode);

                        if (ret == 0)
                        {
                            //return "{\"status\":0, \"msg\":\"发送失败! \"}";
                            res.ResultCode = 0;
                            res.ResultMessage = "发送失败，请稍后再试！";
                            context.Response.ContentType = "application/json";
                            context.Response.Write(JsonConvert.SerializeObject(res));
                            context.Response.End();
                        }
                        else

                        {

                            Model.vip_phonecode model = new Model.vip_phonecode();
                            model.table_name = "vip_users";
                            model.table_pkvalue = mobile;
                            model.code = strcode;
                            model.add_time = System.DateTime.Now;
                            code_bll.Add(model);
                            //写入SESSION，保存验证码
                            //context.Session[DTKeys.SESSION_SMS_CODE] = strcode;

                            DTcms.Common.CacheHelper.Insert(DTKeys.BPN_SESSION_key + mobile, strcode, 5);//短信验证码，5分钟有效



                            res.ResultCode = 1;
                            res.ResultMessage = "发送成功！";
                            context.Response.ContentType = "application/json";
                            context.Response.Write(JsonConvert.SerializeObject(res));
                            context.Response.End();
                        }
                    }
                    else
                    {
                        res.ResultCode = 0;
                        res.ResultMessage = "不能重复发送，请稍后再试！";
                        context.Response.ContentType = "application/json";
                        context.Response.Write(JsonConvert.SerializeObject(res));
                        context.Response.End();
                    }

                }
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
            return;
        }
        private void getcodeimg(HttpContext context)
        {
            string mobile = DTRequest.GetQueryString("mobile");
            if (mobile == "")
            {
                return;
            }
            int codeW = 85;
            int codeH = 24;
            int fontSize = 16;
            string chkCode = string.Empty;
            //颜色列表，用于验证码、噪线、噪点 
            Color[] color = { Color.Black, Color.Blue, Color.Green, Color.Orange, Color.Brown, Color.Brown, Color.DarkBlue };
            //字体列表，用于验证码 
            string[] font = { "Times New Roman", "Verdana", "Arial", "Gungsuh", "Impact" };
            //验证码的字符集，去掉了一些容易混淆的字符 
            // char[] character = { '2', '3', '4', '5', '6', '8', '9', 'a', 'b', 'd', 'e', 'f', 'h', 'k', 'm', 'n', 'r', 'x', 'y', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'R', 'S', 'T', 'W', 'X', 'Y' };
            char[] character = { '2', '3', '4', '5', '6', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'R', 'S', 'T', 'W', 'X', 'Y' };
            Random rnd = new Random();
            //生成验证码字符串 
            for (int i = 0; i < 4; i++)
            {
                chkCode += character[rnd.Next(character.Length)];
            }
            //写入缓存
            //context.Session["REG_V_CODE"+ mobile] = chkCode;
            DTcms.Common.CacheHelper.Insert("BPN_V_CODE" + mobile, chkCode, 2);
            //创建画布
            Bitmap bmp = new Bitmap(codeW, codeH);
            Graphics g = Graphics.FromImage(bmp);
            g.Clear(Color.White);
            //画噪线 
            for (int i = 0; i < 1; i++)
            {
                int x1 = rnd.Next(codeW);
                int y1 = rnd.Next(codeH);
                int x2 = rnd.Next(codeW);
                int y2 = rnd.Next(codeH);
                Color clr = color[rnd.Next(color.Length)];
                g.DrawLine(new Pen(clr), x1, y1, x2, y2);
            }
            //画验证码字符串 
            for (int i = 0; i < chkCode.Length; i++)
            {
                string fnt = font[rnd.Next(font.Length)];
                Font ft = new Font(fnt, fontSize);
                Color clr = color[rnd.Next(color.Length)];
                g.DrawString(chkCode[i].ToString(), ft, new SolidBrush(clr), (float)i * 18 + 2, (float)0);
            }
            //画噪点 
            for (int i = 0; i < 100; i++)
            {
                int x = rnd.Next(bmp.Width);
                int y = rnd.Next(bmp.Height);
                Color clr = color[rnd.Next(color.Length)];
                bmp.SetPixel(x, y, clr);
            }
            //清除该页输出缓存，设置该页无缓存 
            context.Response.Buffer = true;
            context.Response.ExpiresAbsolute = System.DateTime.Now.AddMilliseconds(0);
            context.Response.Expires = 0;
            context.Response.CacheControl = "no-cache";
            context.Response.AppendHeader("Pragma", "No-Cache");
            //将验证码图片写入内存流，并将其以 "image/Png" 格式输出 
            MemoryStream ms = new MemoryStream();
            try
            {
                bmp.Save(ms, ImageFormat.Png);
                context.Response.ClearContent();
                context.Response.ContentType = "image/Png";
                context.Response.BinaryWrite(ms.ToArray());
            }
            finally
            {
                //显式释放资源 
                bmp.Dispose();
                g.Dispose();
            }
        }
        #endregion

        #region  校检手机验证码OK===============================
        private string verify_sms_code(HttpContext context, string strcode, string mobile)
        {
            if (string.IsNullOrEmpty(strcode))
            {
                return "对不起，请输入验证码！";// "{\"status\":0, \"msg\":\"对不起，请输入验证码！\"}";
            }

            if (DTcms.Common.CacheHelper.Get(DTKeys.BPN_SESSION_key + mobile) == null)
            {
                return "对不起，验证码超时或已过期！";
            }
            if (strcode.ToLower() != (DTcms.Common.CacheHelper.Get(DTKeys.BPN_SESSION_key + mobile).ToString()).ToLower())
            {
                return "您输入的验证码不正确！";
            }
            else
            {
                DTcms.Common.CacheHelper.Remove(DTKeys.BPN_SESSION_key + mobile);
                return "success";
            }
        }
        #endregion

        #region  更新个人信息===================================
        private void updateuser(HttpContext context)
        {
            JsonResult<string> res = new JsonResult<string>();
            res.ResultCode = 0;
            res.ResultCount = 0;
            res.ResultMessage = "fails";

            string useropenid = DTcms.Common.DTRequest.GetFormString("useropenid");
            int userid = DTcms.Common.DTRequest.GetFormInt("userid");

            if (useropenid != "" && userid != 0)
            {
                BLL.vip_users bll = new BLL.vip_users();
                var user = bll.GetModelList("user_wx_openid='" + useropenid + "' and id=" + userid).FirstOrDefault();
                if (user!=null)
                {
                    if (DTcms.Common.DTRequest.GetFormString("truename") != "")
                    {
                        user.user_truename = DTRequest.GetFormString("truename");
                    }
                    if (DTcms.Common.DTRequest.GetFormString("phone") != "")
                    {
                        BLL.vip_phonecode codebll = new BLL.vip_phonecode();
                        var code = codebll.GetModelList("table_name='vip_users' and table_pkvalue='"+ DTRequest.GetFormString("phone") + "'  ").FirstOrDefault();
                        if (code != null)
                        {
                            if (code.code == DTRequest.GetFormString("code"))
                            {
                                code.code = "";
                                codebll.Update(code);
                                user.user_phone = DTRequest.GetFormString("phone");
                                user.status = 1;
                            }
                            else
                            {
                                res.ResultCode = 0;
                                res.ResultCount = 0;
                                res.ResultMessage = "验证码错误";
                                context.Response.ContentType = "application/json";
                                context.Response.Write(JsonConvert.SerializeObject(res));
                                context.Response.End();
                            }
                        }
                        else
                        {
                            res.ResultCode = 0;
                            res.ResultCount = 0;
                            res.ResultMessage = "验证码错误";
                            context.Response.ContentType = "application/json";
                            context.Response.Write(JsonConvert.SerializeObject(res));
                            context.Response.End();
                        }

                    }
                    if (DTcms.Common.DTRequest.GetFormString("email") != "")
                    {
                        user.user_email = DTRequest.GetFormString("email");
                    }
                    var s = bll.Update(user);
                    res.ResultCode = s == true ? 1 : 0; ;
                    res.ResultCount = 1;
                    res.ResultMessage = s == true ? "sucess" : "fails";
                }
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();

        }
        #endregion

        #region  获得个人信息===================================
        private void getuser(HttpContext context)
        {
            JsonResult<Model.vip_users> res = new JsonResult<Model.vip_users>();
            res.ResultCode = 0;
            res.ResultCount = 0;
            res.ResultMessage = "fails";

            string useropenid = DTcms.Common.DTRequest.GetFormString("useropenid");
            int userid = DTcms.Common.DTRequest.GetFormInt("userid");

            if (useropenid != "" && userid != 0)
            {
                BLL.vip_users bll = new BLL.vip_users();
                var user = bll.GetModelList("user_wx_openid='" + useropenid + "' and id=" + userid).FirstOrDefault();
                if (user != null)
                {
                    List<Model.vip_users> list = new List<Model.vip_users>();
                    list.Add(user);
                    res.JsonEntity = list;
                    res.ResultCode = 1;
                    res.ResultCount = list.Count;
                    res.ResultMessage = "sucess";
                }
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();

        }
        #endregion

        #region 更新订单信息===================================

        
        private void updateorder_status(HttpContext context)
        {
            string useropenid = DTcms.Common.DTRequest.GetFormString("useropenid");
            int userid = DTcms.Common.DTRequest.GetFormInt("userid");

            int orderid = DTcms.Common.DTRequest.GetFormInt("orderid");
            int status = DTcms.Common.DTRequest.GetFormIntValue("status", -1);
            string code = DTcms.Common.DTRequest.GetFormString("code");
            string clinic_diagnosis= DTcms.Common.DTRequest.GetFormString("clinic_diagnosis");

            JsonResult<string> res = new JsonResult<string>();
            res.ResultCode = 0;
            res.ResultCount = 0;
            res.ResultMessage = "fails";
            if (useropenid != "" && userid != 0 &&orderid!=0)
            {
                
                BLL.vip_orders bll = new BLL.vip_orders();
                var order = bll.GetModel(orderid);
                if (order != null)
                {
                    if(status!=-1)
                    {

                        order.status = status;
                        //服务开始
                        if (status == 10)
                        {
                            if (order.service_typeid > 2)
                            {
                                if (string.IsNullOrEmpty(order.clinic_confirm))
                                {
                                    res.ResultMessage = "对不起，请上传院前检查";
                                    context.Response.ContentType = "application/json";
                                    context.Response.Write(JsonConvert.SerializeObject(res));
                                    context.Response.End();
                                }
                            }

                            if (order.verifycode != code)
                            {
                                res.ResultMessage = "验证码错误";
                                context.Response.ContentType = "application/json";
                                context.Response.Write(JsonConvert.SerializeObject(res));
                                context.Response.End();
                            }
                           
                            order.service_begin = System.DateTime.Now;
                            
                        }
                        else if (status == 15) //服务结束
                        {
                            order.service_end = System.DateTime.Now;
                            if (clinic_diagnosis != "")
                            {
                                order.clinic_diagnosis = clinic_diagnosis;
                            }

                        }

                        
                        bll.Update(order);

                    
                        res.ResultCode = 1;
                        res.ResultCount = 1;
                        res.ResultMessage = "保存成功";
                    }
                }
               

            }
            else
            {
                //res.JsonEntity = list;
                res.ResultCode = 0;
                res.ResultCount = 0;
                res.ResultMessage = "fails";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }
        private void updateorder_notic(HttpContext context)
        {
            string useropenid = DTcms.Common.DTRequest.GetFormString("useropenid");
            int userid = DTcms.Common.DTRequest.GetFormInt("userid");

            int orderid = DTcms.Common.DTRequest.GetFormInt("orderid");
            string key = DTcms.Common.DTRequest.GetFormString("key");


            JsonResult<string> res = new JsonResult<string>();
            res.ResultCode = 0;
            res.ResultCount = 0;
            res.ResultMessage = "fails";
            if (useropenid != "" && userid != 0 && orderid != 0)
            {

                BLL.vip_orders bll = new BLL.vip_orders();
                var order = bll.GetModel(orderid);
                if (order != null)
                {
                    if (key == "begin")
                    {

                        order.clinic_begin_notic = System.DateTime.Now; ;

                    }
                    else if (key == "end")
                    {
                        order.clinic_end_notic = System.DateTime.Now; ;

                    }

                    bll.Update(order);


                    res.ResultCode = 1;
                    res.ResultCount = 1;
                    res.ResultMessage = "sucess";
                }


            }
            else
            {
                //res.JsonEntity = list;
                res.ResultCode = 0;
                res.ResultCount = 0;
                res.ResultMessage = "fails";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }
        private void updateorder(HttpContext context)
        {
            string useropenid = DTcms.Common.DTRequest.GetFormString("useropenid");
            int userid = DTcms.Common.DTRequest.GetFormInt("userid");

            int orderid = DTcms.Common.DTRequest.GetFormInt("orderid");

            int status = DTcms.Common.DTRequest.GetFormIntValue("status", -1);
            string clinic_department = DTcms.Common.DTRequest.GetFormString("clinic_department");
            string clinic_doctor = DTcms.Common.DTRequest.GetFormString("clinic_doctor");
            string clinic_date = DTcms.Common.DTRequest.GetFormString("clinic_date");//yyyy-MM-dd
            

            JsonResult<string> res = new JsonResult<string>();
            res.ResultCode = 0;
            res.ResultCount = 0;
            res.ResultMessage = "fails";
            if (useropenid != "" && userid != 0 && orderid != 0)
            {

                BLL.vip_orders bll = new BLL.vip_orders();
                var order = bll.GetModel(orderid);
                if (order != null)
                {
                    if (status != -1)
                    {

                        order.status = status;
                       
                    }
                    if (clinic_department != "")
                    {
                        order.clinic_department = clinic_department;
                    }
                    if (clinic_doctor != "")
                    {
                        order.clinic_doctor = clinic_doctor;
                    }
                    if (clinic_date != "")
                    {
                        order.clinic_date =DateTime.Parse(clinic_date);
                    }
                    
                    bll.Update(order);


                    res.ResultCode = 1;
                    res.ResultCount = 1;
                    res.ResultMessage = "保存成功";
                }


            }
            else
            {
                //res.JsonEntity = list;
                res.ResultCode = 0;
                res.ResultCount = 0;
                res.ResultMessage = "fails";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }
        private void updateorder_confirm(HttpContext context)
        {
            string useropenid = DTcms.Common.DTRequest.GetFormString("useropenid");
            int userid = DTcms.Common.DTRequest.GetFormInt("userid");

            int orderid = DTcms.Common.DTRequest.GetFormInt("orderid");

            int status = DTcms.Common.DTRequest.GetFormIntValue("status", -1);
            string clinic_department = DTcms.Common.DTRequest.GetFormString("clinic_department");
            string clinic_doctor = DTcms.Common.DTRequest.GetFormString("clinic_doctor");
            string clinic_date = DTcms.Common.DTRequest.GetFormString("clinic_date");//yyyy-MM-dd
            string clinic_confirm = DTcms.Common.DTRequest.GetFormString("clinic_confirm"); 

            JsonResult<string> res = new JsonResult<string>();
            res.ResultCode = 0;
            res.ResultCount = 0;
            res.ResultMessage = "fails";
            if (useropenid != "" && userid != 0 && orderid != 0)
            {

                BLL.vip_orders bll = new BLL.vip_orders();
                var order = bll.GetModel(orderid);
                if (order != null)
                {
                    //if (status != -1)
                    //{

                    //    order.status = status;

                    //}
                    if (clinic_department != "")
                    {
                        order.clinic_department = clinic_department;
                    }
                    if (clinic_doctor != "")
                    {
                        order.clinic_doctor = clinic_doctor;
                    }
                    if (clinic_date != "")
                    {
                        order.clinic_date = DateTime.Parse(clinic_date);
                    }
                    if (clinic_confirm != "")
                    {
                        order.clinic_confirm = clinic_confirm;
                    }
                    //order.status = status;

                    bll.Update(order);


                    res.ResultCode = 1;
                    res.ResultCount = 1;
                    res.ResultMessage = "保存成功";
                }


            }
            else
            {
                //res.JsonEntity = list;
                res.ResultCode = 0;
                res.ResultCount = 0;
                res.ResultMessage = "fails";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }
        #endregion

        #region 我的订单列表或详情--护士========================
        private void getlist(HttpContext context)
        {
            string useropenid = DTcms.Common.DTRequest.GetFormString("useropenid");
            int userid = DTcms.Common.DTRequest.GetFormInt("userid");

            int orderid = DTcms.Common.DTRequest.GetFormInt("orderid");
            int status = DTcms.Common.DTRequest.GetFormInt("status",-1);
            int pagesize = DTcms.Common.DTRequest.GetFormIntValue("pagesize", 10);
            int page = DTRequest.GetFormIntValue("page", 1);

            JsonResult<Model.View_vip_orders> res = new JsonResult<Model.View_vip_orders>();
            if (useropenid != ""&&userid!=0)
            {
                string strwhere = " servicer_id=" + userid + " ";
                if (orderid != 0)
                {
                    strwhere += " and id=" + orderid;
                }
                
                if (status != -1)
                {
                    if (status != 2)
                    {
                        strwhere += " and status=" + status;
                    }
                    else
                    {
                        strwhere += " and (status=" + status+" or status=10) ";
                    }
                }

                logger.Info("getlist,where:" + strwhere);
                BLL.View_vip_orders bll = new BLL.View_vip_orders();
                int recordcount = 0;
                var list = bll.GetModelList(pagesize, page, strwhere, "add_time desc", out recordcount);

                res.JsonEntity = list;
                res.ResultCode = 1;
                res.ResultCount = list.Count;
                res.ResultMessage = "sucess";

                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add("recordcount", recordcount.ToString());
                res.ResultExt = dic;

            }
            else
            {
                //res.JsonEntity = list;
                res.ResultCode = 0;
                res.ResultMessage = "fails";
                res.ResultCount = 0;
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }
        #endregion

        #region 我的订单列表或详情--客户========================
        private void getlist_member(HttpContext context)
        {
            string useropenid = DTcms.Common.DTRequest.GetFormString("useropenid");
            int userid = DTcms.Common.DTRequest.GetFormInt("userid");

            int orderid = DTcms.Common.DTRequest.GetFormInt("orderid");
            int status = DTcms.Common.DTRequest.GetFormInt("status", -1);
            int pagesize = DTcms.Common.DTRequest.GetFormIntValue("pagesize", 10);
            int page = DTRequest.GetFormIntValue("page", 1);

            JsonResult<Model.View_vip_orders> res = new JsonResult<Model.View_vip_orders>();
            if (useropenid != ""&& userid!=0)
            {

                string strwhere = " 1=1 ";
                BLL.vip_users userbll = new BLL.vip_users();
                var user = userbll.GetModelList("id="+userid+ " and user_wx_openid='"+ useropenid + "'").FirstOrDefault();
                if (user != null&&user.user_phone!="")
                {

                    strwhere += " and telephone='"+ user.user_phone + "'";
                }
                else
                {
                    strwhere += " and telephone='0' ";
                }
               
                if (orderid != 0)
                {
                    strwhere += " and id=" + orderid;
                }
                if (status != -1)
                {
                    strwhere += " and status="+status;
                }
                //BLL.vip_orders bll = new BLL.vip_orders();
                BLL.View_vip_orders bll = new BLL.View_vip_orders();
                int recordcount = 0;
                var list = bll.GetModelList(pagesize, page, strwhere, "add_time desc", out recordcount);

                res.JsonEntity = list;
                res.ResultCode = 1;
                res.ResultCount = list.Count;
                res.ResultMessage = "sucess";

                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add("recordcount", recordcount.ToString());
                res.ResultExt = dic;

            }
            else
            {
                //res.JsonEntity = list;
                res.ResultCode = 0;
                res.ResultCount = 0;
                res.ResultMessage = "fails";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }
        #endregion

        #region 阿里云-短信发送
        /// <summary>
        /// 阿里云-短信发送
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        private int ali_smsvcode_send(string mobile, string code)
        {
            string product = "Dysmsapi";//短信API产品名称
            string domain = "dysmsapi.aliyuncs.com";//短信API产品域名
            string accessId = System.Configuration.ConfigurationManager.AppSettings["alidayu_sms_accessId"].ToString(); ;
            string accessSecret = System.Configuration.ConfigurationManager.AppSettings["alidayu_sms_accessSecret"].ToString();

            string SignName = System.Configuration.ConfigurationManager.AppSettings["alidayu_sms_signname"].ToString();
            string TemplateCode = System.Configuration.ConfigurationManager.AppSettings["alidayu_sms_templatecode"].ToString();

            string regionIdForPop = "cn-hangzhou";

            IClientProfile profile = DefaultProfile.GetProfile(regionIdForPop, accessId, accessSecret);
            DefaultProfile.AddEndpoint(regionIdForPop, regionIdForPop, product, domain);
            IAcsClient acsClient = new DefaultAcsClient(profile);
            SendSmsRequest request = new SendSmsRequest();


            try
            {
                //request.SignName = "上云预发测试";//"管理控制台中配置的短信签名（状态必须是验证通过）"
                //request.TemplateCode = "SMS_71130001";//管理控制台中配置的审核通过的短信模板的模板CODE（状态必须是验证通过）"
                //request.RecNum = "13567939485";//"接收号码，多个号码可以逗号分隔"
                //request.ParamString = "{\"name\":\"123\"}";//短信模板中的变量；数字需要转换为字符串；个人用户每个变量长度必须小于15个字符。"
                //SingleSendSmsResponse httpResponse = client.GetAcsResponse(request);
                request.PhoneNumbers = mobile;// "1350000000";
                request.SignName = SignName;// "xxxxxx";
                request.TemplateCode = TemplateCode;// "SMS_xxxxxxx";
                request.TemplateParam = "{\"code\":\"" + code + "\"}";//数字验证码
                request.OutId = "";
                //请求失败这里会抛ClientException异常
                SendSmsResponse sendSmsResponse = acsClient.GetAcsResponse(request);
                if (sendSmsResponse.Code == "OK")
                {
                    return 1;
                }
                else
                {
                    logger.Debug("sendSmsResponse.Code_no_ok_msg：" + sendSmsResponse.Message);
                    return 0;

                }

            }
            catch (Exception ex)
            {
                logger.Error("ali_smsvcode_send_catch_error:" + ex.Message);
                return 0;
            }
            return 0;
        }
        #endregion

        #region 腾讯云——短信发送
        /// <summary>
        /// 腾讯云——短信发送
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="strcode"></param>
        /// <returns></returns>
        private int qcloud_smsvcode_send(string mobile, string strcode)
        {
            int appid = int.Parse(System.Configuration.ConfigurationManager.AppSettings["SMS_appid"].ToString()); //1400239746;
            // 短信应用 SDK AppKey
            string appkey = System.Configuration.ConfigurationManager.AppSettings["SMS_appkey"].ToString();// "05db653f1ac1c553025fab31c983ee4d";
            string templateid = System.Configuration.ConfigurationManager.AppSettings["SMS_templateid"].ToString();
            string smsSign = System.Configuration.ConfigurationManager.AppSettings["SMS_sign"].ToString();//SMS_sign

            SmsSingleSender ssender = new SmsSingleSender(appid, appkey);


            string[] msgcontent = { strcode, userConfig.regsmsexpired.ToString() };
            var sresult = ssender.sendWithParam("86", mobile, int.Parse(templateid), msgcontent
                , smsSign, "", "");

            if (sresult.result != 0)
            {
                logger.Debug("qcloud_smsvcode_send_ssender_error" + sresult.errMsg);
                return 0;
            }
            else
            {
                return 1;
            }

        }
        #endregion

        #region 上传图片
        private void uploadimg(HttpContext context)
        {
            JsonResult<SaveImageResult> res = new JsonResult<SaveImageResult>();
            string userid = DTRequest.GetFormString("userid");
            try
            {

                HttpFileCollection hfc = context.Request.Files;
                string saveName = System.DateTime.Now.ToString("yyyyMMddHHmmssffff");
                string extention = ".png";

                HttpPostedFile hpf = hfc[0];

                //文件保存目录路径
                String savepath = context.Server.MapPath("/upload/user" + userid + "/");
                if (!Directory.Exists(savepath))
                    Directory.CreateDirectory(savepath);

                String filePath = savepath + saveName + extention;

                List<SaveImageResult> imglist = new List<SaveImageResult>();

                hpf.SaveAs(filePath);
                res.ResultCode = 1;
                SaveImageResult info = new SaveImageResult();
                info.ReturnPath = "/upload/user" + userid + "/" + saveName + extention;
                imglist.Add(info);
                res.JsonEntity = imglist;



            }
            catch (Exception ex)
            {

                res.ResultCode = -2;
                res.ResultMessage = ex.Message;
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }

        private void uploadimgbase64(HttpContext context)
        {
            JsonResult<SaveImageResult> res = new JsonResult<SaveImageResult>();
            string userid = DTRequest.GetFormString("userid");
            string base64str = DTRequest.GetFormString("b64str");
            string savepath = "/upload/user" + userid + "/";

            if (!Directory.Exists(savepath))
                Directory.CreateDirectory(savepath);

            string saveName = System.DateTime.Now.ToString("yyyyMMddHHmmssffff");
            string extention = ".png";
            SaveImageResult result = api.UtilsTool.H5ImgUpload.SaveBase64StrToImage(base64str, "", savepath, saveName, extention, 635810);

            List<SaveImageResult> imglist = new List<SaveImageResult>();
            imglist.Add(result);

            res.ResultCode = result.ErrorCode == 0 ? 1 : 0;
            res.ResultMessage = result.ErrorMessage;

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }
        #endregion
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}