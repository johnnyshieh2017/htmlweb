﻿using DTcms.API.OAuth;
using DTcms.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DTcms.Web.vipservice
{
    public partial class wxlogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (DTcms.Common.DTRequest.GetQueryString("code") == "")
            {
                string url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=" + System.Configuration.ConfigurationManager.AppSettings["wxmp_appid"];//"wx657c74ba8a62696f";//
                url += "&redirect_uri=" + hdhost.Value + "/vipservice/wxlogin.aspx";
                url += "&response_type=code";
                url += "&scope=snsapi_base";
                url += "&state=1&connect_redirect=1#wechat_redirect";
                this.Response.Redirect(url);
            }
            else
            {
                string appid = System.Configuration.ConfigurationManager.AppSettings["wxmp_appid"];// 测试公众号 "wx657c74ba8a62696f";
                string appkey = System.Configuration.ConfigurationManager.AppSettings["wxmp_appsecret"]; // 测试key "8837a57e029cdec23363eb20285e1b79";
                string url = "";

                string access_token = string.Empty;
                string expires_in = string.Empty;
                string openid = string.Empty;
                //取得返回参数
                string state = DTRequest.GetQueryString("state");
                string code = DTRequest.GetQueryString("code");
                int userid = DTRequest.GetQueryInt("userid");
                //第一步：获取Access Token
                Dictionary<string, object> dic = weixin_helper.get_access_token(appid, appkey, url, code);
                if (dic == null || !dic.ContainsKey("access_token"))
                {
                    Response.Write("出错了，无法获取Access Token，请检查App Key是否正确！");
                    return;
                }

                
                access_token = dic["access_token"].ToString();

                expires_in = dic["expires_in"].ToString();

                openid = dic["openid"].ToString();
                //
                // 查找、写入openid
                //
                BLL.vip_users bll = new BLL.vip_users();
                var user = bll.GetModelList("user_wx_openid='" + openid + "' and user_type=1").FirstOrDefault();
                if (user != null)
                {
                    Common.Utils.WriteCookie("user_wx_openid", openid);
                    Common.Utils.WriteCookie("user_type", user.user_type.ToString());
                    Common.Utils.WriteCookie("user_status", user.status.ToString());
                    Common.Utils.WriteCookie("user_id", user.id.ToString());
                    this.Response.Redirect(hdredirect.Value);
                }
                else
                {
                    Model.vip_users model = new Model.vip_users();

                    model.user_wx_openid = openid;
                    model.status = 0;
                    model.user_type = 1;
                    model.add_time = System.DateTime.Now;
                    var id = bll.Add(model);

                    Common.Utils.WriteCookie("user_wx_openid", openid);
                    Common.Utils.WriteCookie("user_type", model.user_type.ToString());
                    Common.Utils.WriteCookie("user_status", model.status.ToString());
                    Common.Utils.WriteCookie("user_id", id.ToString());
                    this.Response.Redirect(hdredirect_userinfo.Value);
                }
            }
        }
    }
}