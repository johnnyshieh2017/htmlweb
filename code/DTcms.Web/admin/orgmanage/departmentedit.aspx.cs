﻿using DTcms.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DTcms.Web.admin.orgmanage
{
    public partial class departmentedit : Web.UI.ManagePage
    {
        private string action = DTEnums.ActionEnum.Add.ToString(); //操作类型
        private int id = 0;
        BLL.yjr_doctor_group bll = new BLL.yjr_doctor_group();
        protected void Page_Load(object sender, EventArgs e)
        {
            string _action = DTRequest.GetQueryString("action");
            if (!string.IsNullOrEmpty(_action) && _action == DTEnums.ActionEnum.Edit.ToString())
            {
                this.action = DTEnums.ActionEnum.Edit.ToString();//修改类型
                if (!int.TryParse(Request.QueryString["id"] as string, out this.id))
                {
                    JscriptMsg("传输参数不正确！", "back");
                    return;
                }
                if (!bll.Exists(this.id))
                {
                    JscriptMsg("记录不存在或已被删除！", "back");
                    return;
                }
            }

            if (!IsPostBack)
            {
                ChkAdminLevel("department_edit", DTEnums.ActionEnum.View.ToString()); //检查权限
                Model.manager model = GetAdminInfo(); //取得管理员信息

                InitData();

                if (action == DTEnums.ActionEnum.Edit.ToString()) //修改
                {
                    ShowInfo(this.id);
                }
            }
        }

        #region 基础初始化=================================
        void InitData()
        {
            var hospatil = new BLL.yjr_doc_hospital().GetModelList("");
            foreach (var item in hospatil)
            {
                ddlhospital.Items.Add(new ListItem(item.hospitalname, item.id.ToString()));
            }

           
        }
        #endregion

        #region 赋值操作=================================
        private void ShowInfo(int _id)
        {
            var model = bll.GetModel(_id);
            txtRealName.Text = model.groupname;

            txtLogo.Text = model.logo_url;
            txtintro.Value = model.description;
            txtLeaderIntro.Value = model.leader_desc;
            ddlhospital.SelectedValue = model.hospatilid.ToString();
            txtSort.Text = model.sort.ToString();

        }
        #endregion

        #region 增加操作=================================
        private bool DoAdd()
        {

            DTcms.Model.yjr_doctor_group model = new DTcms.Model.yjr_doctor_group();
            model.groupname = txtRealName.Text.Trim();
            model.hospatilid =int.Parse(ddlhospital.SelectedValue);
            model.leader_desc = txtLeaderIntro.Value;
            model.description = txtintro.Value;
            model.logo_url = txtLogo.Text.Trim();
            model.sort = int.Parse(txtSort.Text.Trim());

            model.status = 1;
            model.siteid = 1;
            
            if (bll.Add(model) > 0)
            { 
                return true;
            }
            return false;
        }
        #endregion

        #region 修改操作=================================
        private bool DoEdit(int _id)
        {
            bool result = false;

            DTcms.Model.yjr_doctor_group model = bll.GetModel(_id);
            model.groupname = txtRealName.Text.Trim();
            model.hospatilid =int.Parse(ddlhospital.SelectedValue);
            model.leader_desc = txtLeaderIntro.Value;
            model.logo_url = txtLogo.Text;
            model.sort = int.Parse(txtSort.Text.Trim());
            model.description = txtintro.Value;

            if (bll.Update(model))
            {
                //AddAdminLog(DTEnums.ActionEnum.Add.ToString(), "修改doctor:" + model.hospitalname); //记录日志
                result = true;
            }

            return result;
        }
        #endregion

        //保存
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (action == DTEnums.ActionEnum.Edit.ToString()) //修改
            {
                ChkAdminLevel("department_edit", DTEnums.ActionEnum.Edit.ToString()); //检查权限
                if (!DoEdit(this.id))
                {
                    JscriptMsg("保存过程中发生错误！", "");
                    return;
                }
                JscriptMsg("修改信息成功！", "departmentmanage.aspx");
            }
            else //添加
            {
                ChkAdminLevel("department_edit", DTEnums.ActionEnum.Add.ToString()); //检查权限
                if (!DoAdd())
                {
                    JscriptMsg("保存过程中发生错误！", "");
                    return;
                }
                JscriptMsg("添加信息成功！", "departmentmanage.aspx");
            }
        }
    }
}