﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="item_list.aspx.cs" Inherits="DTcms.Web.admin.vote.item_list" %>

<%@ Import Namespace="DTcms.Common" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,initial-scale=1.0,user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <title>选项设置</title>
    <link rel="stylesheet" type="text/css" href="../../scripts/artdialog/ui-dialog.css" />
    <link rel="stylesheet" type="text/css" href="../../css/pagination.css" />
    <link rel="stylesheet" type="text/css" href="../skin/icon/iconfont.css" />
    <link rel="stylesheet" type="text/css" href="../skin/default/style.css" />
    <script type="text/javascript" src="../../scripts/jquery/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="../../scripts/jquery/jquery.lazyload.min.js"></script>
    <script type="text/javascript" src="../../scripts/artdialog/dialog-plus-min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/laymain.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/common.js"></script>
    <script type="text/javascript">
        $(function () {
            //图片延迟加载
            $(".pic img").lazyload({ effect: "fadeIn" });
            //点击图片链接
            $(".pic img").click(function () {
                var linkUrl = $(this).parent().parent().find(".foot a").attr("href");
                if (linkUrl != "") {
                    location.href = linkUrl; //跳转到修改页面
                }
            });
        });
    </script>

    <script type="text/javascript">

        function delitem(id,sid) {
            var msg = "删除记录后不可恢复，您确定吗？";
           
            parent.dialog({
                title: '提示',
                content: msg,
                okValue: '确定',
                ok: function () {
                    location.href = 'vote/item_list.aspx?action=Delete&id='+id+"&sid="+sid;
                },
                cancelValue: '取消',
                cancel: function () { }
            }).showModal();

            return false;
        }

    </script>
</head>

<body class="mainbody">
    <form id="form1" runat="server">
        <!--导航栏-->
        <div class="location">
            <a href="javascript:history.back(-1);" class="back"><i class="iconfont icon-up"></i><span>返回上一页</span></a>
            <a href="../center.aspx" class="home"><i class="iconfont icon-home"></i><span>首页</span></a>
            <i class="arrow iconfont icon-arrow-right"></i>
            <span>问卷管理</span>
            <i class="arrow iconfont icon-arrow-right"></i>
            <span>选项设置</span>
        </div>
        <!--/导航栏-->

        <!--工具栏-->
        <div id="floatHead" class="toolbar-wrap">
            
            <div class="toolbar">
                <div class="box-wrap">
                    <a class="menu-btn"><i class="iconfont icon-more"></i></a>
                    <div class="l-list">
                        <ul class="icon-list">
                            <li><a href="#"><span>添加选项</span></a></li>

                        </ul>
                        <div class="menu-list">

                            <div class="rule-single-select">
                                <asp:DropDownList ID="ddlvotelist" runat="server">
                                 
                                </asp:DropDownList>
                            </div>
                            <label class="layui-form-label">选项名称</label>
                            <asp:TextBox ID="txtTitle" runat="server" CssClass="input normal" datatype="*2-80" sucmsg=" "></asp:TextBox>

                           
                             <label class="layui-form-label">序号</label>
                            <asp:TextBox ID="txtSort" runat="server" CssClass="input small" datatype="n" sucmsg=" "></asp:TextBox>

                             <asp:Button ID="btnAdd" Text="保存" CssClass="btn" runat="server" OnClick="btnAdd_Click" />
                        </div>
                    </div>

                   
                </div>
            </div>
        </div>
        <!--/工具栏-->

        <!--列表-->
        <div class="table-container">
            <asp:Repeater ID="rptList" runat="server" OnItemCommand="rptList_ItemCommand">
                <HeaderTemplate>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="ltable">
                        <tr>
                            <th width="8%">选择</th>
                            <th align="left" width="16%">问题</th>
                            <th align="left" width="16%">选项</th>
                           
                           
                            <th align="left" width="10%">序号</th>
                          
                            <th width="10%">操作</th>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td align="center">
                            <asp:CheckBox ID="chkId" CssClass="checkall" runat="server" Style="vertical-align: middle;" />
                            <asp:HiddenField ID="hidId" Value='<%#Eval("id")%>' runat="server" />
                        </td>
                        <td><%#GetsubjectName(Eval("vote_subject_id").ToString())%></a></td>
                        <td><%#Eval("title")%></td>
                      
                        
                        <td>
                            <%#Eval("sort_id")%></td>
                       
                        <td align="center"><a href="item_list.aspx?action=<%#DTEnums.ActionEnum.Edit %>&id=<%#Eval("id")%>&sid=<%#Eval("vote_subject_id")%>">修改</a>
                            <a href="#" onclick="delitem('<%#Eval("id")%>','<%#Eval("vote_subject_id")%>')">删除</a>
                           
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    <%#rptList.Items.Count == 0 ? "<tr><td align=\"center\" colspan=\"8\">暂无记录</td></tr>" : ""%>
</table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
        <!--/列表-->

        <!--内容底部-->
        <div class="line20"></div>
        <div class="pagelist">
            <div class="l-btns">
                <span>显示</span><asp:TextBox ID="txtPageNum" runat="server" CssClass="pagenum" onkeydown="return checkNumber(event);"
                    OnTextChanged="txtPageNum_TextChanged" AutoPostBack="True"></asp:TextBox><span>条/页</span>
            </div>
            <div id="PageContent" runat="server" class="default"></div>
        </div>
        <!--/内容底部-->
        <div id="floatfoot" class="toolbar-wrap">
            
        </div>
        <asp:HiddenField ID="hdItemId"  runat="server" Value="" />
    </form>
</body>
</html>
