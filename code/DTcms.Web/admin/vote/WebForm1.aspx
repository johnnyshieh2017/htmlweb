﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="DTcms.Web.admin.vote.WebForm1" %>

<%@ Import namespace="DTcms.Common" %>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,initial-scale=1.0,user-scalable=no" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<link rel="stylesheet" type="text/css" href="../../scripts/artdialog/ui-dialog.css" />
<link rel="stylesheet" type="text/css" href="../skin/icon/iconfont.css" />
<link rel="stylesheet" type="text/css" href="../skin/default/style.css" />
<script type="text/javascript" charset="utf-8" src="../../scripts/jquery/jquery-1.11.2.min.js"></script>
<script type="text/javascript" charset="utf-8" src="../../scripts/jquery/Validform_v5.3.2_min.js"></script>
<script type="text/javascript" charset="utf-8" src="../../scripts/artdialog/dialog-plus-min.js"></script>
<script type="text/javascript" charset="utf-8" src="../../scripts/webuploader/webuploader.min.js"></script>
<script type="text/javascript" charset="utf-8" src="../js/uploader.js"></script>
<%--<script type="text/javascript" charset="utf-8" src="../js/common.js"></script>
<script type="text/javascript" charset="utf-8" src="../js/laymain.js"></script>--%>
<script type="text/javascript">
    $(function () {
        //初始化表单验证
        $("#form1").initValidform();
        //初始化上传控件
        $(".upload-img").InitUploader({ sendurl: "../../tools/upload_ajax.ashx", swf: "../../scripts/webuploader/uploader.swf" });
    });
</script>
</head>

<body class="mainbody">
<form id="form1" runat="server">
<!--导航栏-->
<div class="location">
 
  <span>处理数据</span>
</div>
<div class="line10"></div>
<!--/导航栏-->

<!--内容-->
<div id="floatHead" class="content-tab-wrap">
  <div class="content-tab">
    <div class="content-tab-ul-wrap">
      <ul>
        <li><a class="selected" href="javascript:;">处理数据</a></li>
       
      </ul>
    </div>
  </div>
</div>

<div class="tab-content">
  
   
  <dl>
    <dt>职工数据</dt>
    <dd>
      <asp:TextBox ID="txtTitle" runat="server" CssClass="input normal"  datatype="*2-100" sucmsg=" "></asp:TextBox>
      <asp:Button ID="btnaddZG" runat="server" Text="导入" /><span class="Validform_checktip"></span>
    </dd>
  </dl>

 <dl>
    <dt>观众数据</dt>
    <dd>
      <asp:TextBox ID="TextBox1" runat="server" CssClass="input normal"  datatype="*2-100" sucmsg=" "></asp:TextBox>
      <asp:Button ID="btnaddGZ" runat="server" Text="导入" /><span class="Validform_checktip"></span>
    </dd>
  </dl>
<!--工具栏-->
<div class="page-footer">
  <div class="btn-wrap">
    <asp:Button ID="btnSubmit" runat="server" Text="下载数据" CssClass="btn" onclick="btnSubmit_Click" />
   
  </div>
</div>
<!--/工具栏-->
</div>
</form>
</body>
</html>

