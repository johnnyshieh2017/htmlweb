﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DTcms.Common;

namespace DTcms.Web.admin.vote
{
    public partial class vote_edit : Web.UI.ManagePage
    {
        private string action = DTEnums.ActionEnum.Add.ToString();//操作类型
        private int id = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            string _action = DTRequest.GetQueryString("action");

            if (!string.IsNullOrEmpty(_action) && _action == DTEnums.ActionEnum.Edit.ToString())
            {
                this.action = DTEnums.ActionEnum.Edit.ToString();//修改类型
                this.id = DTRequest.GetQueryInt("id");
                if (this.id == 0)
                {
                    JscriptMsg("传输参数不正确！", "back");
                    return;
                }
                if (!new BLL.vote().Exists(this.id))
                {
                    JscriptMsg("记录不存在或已被删除！", "back");
                    return;
                }
            }
            if (!Page.IsPostBack)
            {
                //ChkAdminLevel("sys_site_manage", DTEnums.ActionEnum.View.ToString()); //检查权限
                //ShowInfo(this.id); //绑定数据
                if (action == DTEnums.ActionEnum.Edit.ToString()) //修改
                {
                    ShowInfo(this.id);
                }
                else
                {
                    //txtBuildPath.Attributes.Add("ajaxurl", "../../tools/admin_ajax.ashx?action=site_path_validate");
                }
            }
        }


        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            bool str = !(this.action == DTEnums.ActionEnum.Edit.ToString());
            if (str)
            {
                str = this.DoAdd();
                if (str)
                {
                    base.JscriptMsg("添加成功啦！", "index.aspx", "Success");
                }
                else
                {
                    base.JscriptMsg("保存过程中发生错误啦！", "", "Error");
                }
            }
            else
            {
                str = this.DoEdit(this.id);
                if (str)
                {
                    //base.JscriptMsg("修改成功啦！", "index.aspx", "Success");
                    JscriptMsg("修改成功啦！", "index.aspx");
                    return;
                }
                else
                {
                    base.JscriptMsg("保存过程中发生错误啦！", "", "Error");
                }
            }
            
        }
        private void ShowInfo(int _id)
        {
            BLL.vote bll = new BLL.vote();
            Model.vote model = bll.GetModel(_id);
            this.txtTitle.Text = model.title;
            //this.rblType.SelectedValue = model.type.ToString();
            this.txtContent.Text = model.content;
            this.txtsort_id.Text = model.sort_id.ToString();

            this.txtStartTime.Text = model.start_time.ToString("yyyy-MM-dd");
            this.txtEndTime.Text = model.end_time.ToString("yyyy-MM-dd");

            this.rblis_user.SelectedValue = model.is_user.ToString();
        }
        private bool DoAdd()
        {
            bool flag = true;
            Model.vote model = new Model.vote();
            BLL.vote bll = new BLL.vote();
            model.title = this.txtTitle.Text.Trim();
            model.type = 1;// int.Parse(this.rblType.SelectedValue);
            model.sort_id = int.Parse(this.txtsort_id.Text.Trim());
            model.start_time = DateTime.Parse(this.txtStartTime.Text.Trim());
            model.end_time = DateTime.Parse(this.txtEndTime.Text.Trim());
            model.content = this.txtContent.Text.Trim();

            model.is_user = int.Parse(rblis_user.SelectedValue);
            model.status = 0;

            bool flag1 = bll.Add(model) >= 1;
            if (!flag1)
            {
                flag = false;
            }
            bool flag2 = flag;
            return flag2;
        }

        private bool DoEdit(int _id)
        {
            bool flag = true;
            BLL.vote bll = new BLL.vote();
            Model.vote model = bll.GetModel(_id);
            model.title = this.txtTitle.Text.Trim();
            model.type = 1;// int.Parse(this.rblType.SelectedValue);

            model.content = this.txtContent.Text.Trim();

            model.start_time = DateTime.Parse(this.txtStartTime.Text.Trim());
            model.end_time = DateTime.Parse(this.txtEndTime.Text.Trim());
            model.sort_id = int.Parse(this.txtsort_id.Text.Trim());
            model.is_user = int.Parse(rblis_user.SelectedValue);
            model.status = 0;

            bool flag1 = bll.Update(model);
            if (!flag1)
            {
                flag = false;
            }
            bool flag2 = flag;
            return flag2;
        }
    }
}