﻿using System;
using System.Collections;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DTcms.Common;
using DTcms.Model;

namespace DTcms.Web.admin.vote
{
    public partial class vote_report_anwser : System.Web.UI.Page
    {
        protected int channel_id;
        protected int totalCount;
        protected int page;
        protected int pageSize;

        protected int category_id;
        protected string channel_name = string.Empty;
        protected string property = string.Empty;
        protected string keywords = string.Empty;
        protected string prolistview = string.Empty;

        protected string action = "";
        protected string vote_subject_id = "";
        private BLL.vote_item vote_item_bll = new BLL.vote_item();
        protected void Page_Load(object sender, EventArgs e)
        {
            this.channel_id = DTRequest.GetQueryInt("channel_id");
            this.category_id = DTRequest.GetQueryInt("category_id");
            this.keywords = DTRequest.GetQueryString("keywords");
            this.property = DTRequest.GetQueryString("property");
            action = DTRequest.GetQueryString("action");
            vote_subject_id = DTRequest.GetQueryString("vsid");


            this.pageSize = GetPageSize(10); //每页数量
            this.prolistview = Utils.GetCookie("vote_list_view"); //显示方式
            if (!Page.IsPostBack)
            {
               

                RptBind(string.Concat("id>0 and vote_subject_id='" + vote_subject_id + "'", this.CombSqlTxt(this.keywords, this.property)), "add_time asc");

               
            }
        }

        void RptBind(string _strWhere, string _orderby)
        {
            this.page = DTRequest.GetQueryInt("page", 1);
            //this.txtKeywords.Text = this.keywords;

            BLL.vote_item_log bll = new BLL.vote_item_log();
            this.rptList.DataSource = bll.GetList(this.pageSize, this.page, _strWhere, _orderby, out this.totalCount);
            this.rptList.DataBind();
            this.txtPageNum.Text = this.pageSize.ToString();
            string[] strArrays = new string[4];
            strArrays[0] = this.keywords;
            strArrays[1] = this.property;
            strArrays[2] = "__id__";
            strArrays[3] = DTRequest.GetQueryString("vid");

            string str = Utils.CombUrlTxt("subject_list.aspx", "keywords={0}&property={1}&page={2}&vid={3}", strArrays);
            this.PageContent.InnerHtml = Utils.OutPageList(this.pageSize, this.page, this.totalCount, str, 8);
        }

        #region 返回图文每页数量=========================
        private int GetPageSize(int _default_size)
        {
            int _pagesize;
            if (int.TryParse(Utils.GetCookie("vote_page_size", "DTcmsPage"), out _pagesize))
            {
                if (_pagesize > 0)
                {
                    return _pagesize;
                }
            }
            return _default_size;
        }
        #endregion

        protected string CombSqlTxt(string _keywords, string _property)
        {
            StringBuilder stringBuilder = new StringBuilder();
            _keywords = _keywords.Replace("'", "");
            bool flag = string.IsNullOrEmpty(_keywords);
            if (!flag)
            {
                stringBuilder.Append(string.Concat(" and title like '%", _keywords, "%'"));
            }
            flag = string.IsNullOrEmpty(_property);
            if (!flag)
            {
                stringBuilder.Append(string.Concat(" and type=", _property));
            }
            string str = stringBuilder.ToString();
            return str;
        }


        protected void lbtnSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect(Utils.CombUrlTxt("subject_list.aspx", "keywords={0}&vid={1}", this.keywords, DTRequest.GetQueryString("vid")));//txtKeywords.Text
        }

        //设置分页数量
        protected void txtPageNum_TextChanged(object sender, EventArgs e)
        {
            int _pagesize;
            if (int.TryParse(txtPageNum.Text.Trim(), out _pagesize))
            {
                if (_pagesize > 0)
                {
                    Utils.WriteCookie("vote_page_size", "DTcmsPage", _pagesize.ToString(), 14400);
                }
            }
            Response.Redirect(Utils.CombUrlTxt("subject_list.aspx", "keywords={0}&vid={1}", this.keywords, DTRequest.GetQueryString("vid")));
        }

        protected void rptList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

        }
    }
}