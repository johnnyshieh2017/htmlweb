﻿using System;
using System.Collections;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DTcms.Common;

namespace DTcms.Web.admin.vote
{
    public partial class index : Web.UI.ManagePage
    {
        protected int channel_id;
        protected int totalCount;
        protected int page;
        protected int pageSize;

        protected int category_id;
        protected string channel_name = string.Empty;
        protected string property = string.Empty;
        protected string keywords = string.Empty;
        protected string prolistview = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            this.channel_id = DTRequest.GetQueryInt("channel_id");
            this.category_id = DTRequest.GetQueryInt("category_id");
            this.keywords = DTRequest.GetQueryString("keywords");
            this.property = DTRequest.GetQueryString("property");

            //if (channel_id == 0)
            //{
            //    JscriptMsg("频道参数不正确！", "back");
            //    return;
            //}
            this.channel_name = new BLL.site_channel().GetChannelName(this.channel_id); //取得频道名称
            this.pageSize = GetPageSize(10); //每页数量
            this.prolistview = Utils.GetCookie("vote_list_view"); //显示方式
            if (!Page.IsPostBack)
            {
                //ChkAdminLevel("channel_" + this.channel_name + "_list", DTEnums.ActionEnum.View.ToString()); //检查权限
                //TreeBind(this.channel_id); //绑定类别
                //WeiXinBind(); //绑定微信公众号
                //RptBind(this.channel_id, this.category_id, "id>0" + CombSqlTxt(this.keywords, this.property), "sort_id asc,add_time desc,id desc");

                RptBind(string.Concat("id>0", this.CombSqlTxt(this.keywords, this.property)), "add_time desc");
            }
        }

        void RptBind(string _strWhere, string _orderby)
        {
            this.page = DTRequest.GetQueryInt("page", 1);
            this.txtKeywords.Text = this.keywords;

            BLL.vote bll = new BLL.vote();
            this.rptList.DataSource = bll.GetList(this.pageSize, this.page, _strWhere, _orderby, out this.totalCount);
            this.rptList.DataBind();
            this.txtPageNum.Text = this.pageSize.ToString();
            string[] strArrays = new string[3];
            strArrays[0] = this.keywords;
            strArrays[1] = this.property;
            strArrays[2] = "__id__";
            string str = Utils.CombUrlTxt("index.aspx", "keywords={0}&property={1}&page={2}", strArrays);
            this.PageContent.InnerHtml = Utils.OutPageList(this.pageSize, this.page, this.totalCount, str, 8);
        }

        #region 返回图文每页数量=========================
        private int GetPageSize(int _default_size)
        {
            int _pagesize;
            if (int.TryParse(Utils.GetCookie("vote_page_size", "DTcmsPage"), out _pagesize))
            {
                if (_pagesize > 0)
                {
                    return _pagesize;
                }
            }
            return _default_size;
        }
        #endregion

        protected string CombSqlTxt(string _keywords, string _property)
        {
            StringBuilder stringBuilder = new StringBuilder();
            _keywords = _keywords.Replace("'", "");
            bool flag = string.IsNullOrEmpty(_keywords);
            if (!flag)
            {
                stringBuilder.Append(string.Concat(" and title like '%", _keywords, "%'"));
            }
            flag = string.IsNullOrEmpty(_property);
            if (!flag)
            {
                stringBuilder.Append(string.Concat(" and type=", _property));
            }
            string str = stringBuilder.ToString();
            return str;
        }


        protected void lbtnSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect(Utils.CombUrlTxt("index.aspx", "keywords={0}", txtKeywords.Text));
        }

        //设置分页数量
        protected void txtPageNum_TextChanged(object sender, EventArgs e)
        {
            int _pagesize;
            if (int.TryParse(txtPageNum.Text.Trim(), out _pagesize))
            {
                if (_pagesize > 0)
                {
                    Utils.WriteCookie("vote_page_size", "DTcmsPage", _pagesize.ToString(), 14400);
                }
            }
            Response.Redirect(Utils.CombUrlTxt("index.aspx", "keywords={0}", this.keywords));
        }

        protected void rptList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

        }

        protected void btnClose_Click(object sender, EventArgs e)
        {//
            int sucCount = 0;
            int errorCount = 0;

            DTcms.BLL.vote bll = new BLL.vote();
            for (int i = 0; i < rptList.Items.Count; i++)
            {
                int id = Convert.ToInt32(((HiddenField)rptList.Items[i].FindControl("hidId")).Value);
                CheckBox cb = (CheckBox)rptList.Items[i].FindControl("chkId");
                if (cb.Checked)
                {
                    Model.vote model = bll.GetModel(id);
                    model.status = -1;
                    bll.Update(model);
                }
            }
            AddAdminLog(DTEnums.ActionEnum.Edit.ToString(), "关闭问卷" + sucCount + "条"); //记录日志

            RptBind(string.Concat("id>0", this.CombSqlTxt(this.keywords, this.property)), "add_time desc");
        }

        protected void btnPublish_Click(object sender, EventArgs e)
        {//
            int sucCount = 0;
            int errorCount = 0;
            DTcms.BLL.vote bll = new BLL.vote();
            for (int i = 0; i < rptList.Items.Count; i++)
            {
                int id = Convert.ToInt32(((HiddenField)rptList.Items[i].FindControl("hidId")).Value);
                CheckBox cb = (CheckBox)rptList.Items[i].FindControl("chkId");
                if (cb.Checked)
                {
                    Model.vote model = bll.GetModel(id);
                    model.status = 1;
                    bll.Update(model);
                }
            }
            AddAdminLog(DTEnums.ActionEnum.Delete.ToString(), "发布问卷" + sucCount + "条"); //记录日志

            RptBind(string.Concat("id>0", this.CombSqlTxt(this.keywords, this.property)), "add_time desc");
        }

        protected string GetStatus(string status)
        {
            if (status == "0")
            {
                return "编辑中";
            }
            else if (status == "1")
            {
                return "已发布";
            }
            else if (status == "-1")
            {
                return "已关闭";
            }
            else
            {
                return "-";
            }
        }
    }
}