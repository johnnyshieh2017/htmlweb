﻿using System;
using System.Collections;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DTcms.Common;
using DTcms.Model;

namespace DTcms.Web.admin.vote
{
    public partial class subject_list : Web.UI.ManagePage
    {
        protected int channel_id;
        protected int totalCount;
        protected int page;
        protected int pageSize;

        protected int category_id;
        protected string channel_name = string.Empty;
        protected string property = string.Empty;
        protected string keywords = string.Empty;
        protected string prolistview = string.Empty;

        protected string action = "";
        protected string vote_id = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            this.channel_id = DTRequest.GetQueryInt("channel_id");
            this.category_id = DTRequest.GetQueryInt("category_id");
            this.keywords = DTRequest.GetQueryString("keywords");
            this.property = DTRequest.GetQueryString("property");
            action= DTRequest.GetQueryString("action");
            vote_id = DTRequest.GetQueryString("vid");
            

            this.pageSize = GetPageSize(10); //每页数量
            this.prolistview = Utils.GetCookie("vote_list_view"); //显示方式
            if (!Page.IsPostBack)
            {
                //ChkAdminLevel("channel_" + this.channel_name + "_list", DTEnums.ActionEnum.View.ToString()); //检查权限
                //TreeBind(this.channel_id); //绑定类别
                //WeiXinBind(); //绑定微信公众号
                //RptBind(this.channel_id, this.category_id, "id>0" + CombSqlTxt(this.keywords, this.property), "sort_id asc,add_time desc,id desc");

                if (action == "Edit")
                {
                    BindData(DTRequest.GetQueryString("id"));
                }
                else if (action == "Delete")
                {
                    DeleteData(DTRequest.GetQueryString("id"));
                }

                RptBind(string.Concat("id>0 and vote_id='"+ vote_id + "'", this.CombSqlTxt(this.keywords, this.property)), "sort_id desc");

                BindVote();
            }
        }


        void DeleteData(string id)
        {
            DTcms.BLL.vote_item vote_item_bll = new BLL.vote_item();
            var list_count = vote_item_bll.GetRecordCount(" vote_subject_id="+ id);
            if (list_count > 0)
            {
                JscriptMsg("请删除选项后再删除问题！", string.Empty);
            }
            else
            {
                DTcms.BLL.vote_subject bll = new BLL.vote_subject();
                bll.Delete(int.Parse(id));
            }
        }

        void BindData(string id)
        {
            hdItemId.Value = id;
            DTcms.Model.vote_subject model = new Model.vote_subject();
            DTcms.BLL.vote_subject bll = new BLL.vote_subject();
            model = bll.GetModel(int.Parse(id));

            if (model != null)
            {
                txtTitle.Text = model.title;
                txtSort.Text = model.sort_id.ToString();

                ddltype.SelectedValue = model.type.ToString();
            }
        }

        void BindVote()
        {
            BLL.vote bll = new BLL.vote();
            var list = bll.GetList(this.pageSize, this.page, " id='"+ vote_id + "'", " add_time desc", out this.totalCount);
            //ddlvotelist.DataSource = list;
            for (int i = 0; i < list.Tables[0].Rows.Count; i++)
            {
                ddlvotelist.Items.Add(new ListItem { Text = list.Tables[0].Rows[i]["title"].ToString(), Value = list.Tables[0].Rows[i]["id"].ToString() });
            }
        }

        void RptBind(string _strWhere, string _orderby)
        {
            this.page = DTRequest.GetQueryInt("page", 1);
            //this.txtKeywords.Text = this.keywords;

            BLL.vote_subject bll = new BLL.vote_subject();
            this.rptList.DataSource = bll.GetList(this.pageSize, this.page, _strWhere, _orderby, out this.totalCount);
            this.rptList.DataBind();
            this.txtPageNum.Text = this.pageSize.ToString();
            string[] strArrays = new string[4];
            strArrays[0] = this.keywords;
            strArrays[1] = this.property;
            strArrays[2] = "__id__";
            strArrays[3] = DTRequest.GetQueryString("vid");

            string str = Utils.CombUrlTxt("subject_list.aspx", "keywords={0}&property={1}&page={2}&vid={3}", strArrays);
            this.PageContent.InnerHtml = Utils.OutPageList(this.pageSize, this.page, this.totalCount, str, 8);
        }

        #region 返回图文每页数量=========================
        private int GetPageSize(int _default_size)
        {
            int _pagesize;
            if (int.TryParse(Utils.GetCookie("vote_page_size", "DTcmsPage"), out _pagesize))
            {
                if (_pagesize > 0)
                {
                    return _pagesize;
                }
            }
            return _default_size;
        }
        #endregion

        protected string CombSqlTxt(string _keywords, string _property)
        {
            StringBuilder stringBuilder = new StringBuilder();
            _keywords = _keywords.Replace("'", "");
            bool flag = string.IsNullOrEmpty(_keywords);
            if (!flag)
            {
                stringBuilder.Append(string.Concat(" and title like '%", _keywords, "%'"));
            }
            flag = string.IsNullOrEmpty(_property);
            if (!flag)
            {
                stringBuilder.Append(string.Concat(" and type=", _property));
            }
            string str = stringBuilder.ToString();
            return str;
        }


        protected void lbtnSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect(Utils.CombUrlTxt("subject_list.aspx", "keywords={0}&vid={1}", this.keywords, DTRequest.GetQueryString("vid")));//txtKeywords.Text
        }

        //设置分页数量
        protected void txtPageNum_TextChanged(object sender, EventArgs e)
        {
            int _pagesize;
            if (int.TryParse(txtPageNum.Text.Trim(), out _pagesize))
            {
                if (_pagesize > 0)
                {
                    Utils.WriteCookie("vote_page_size", "DTcmsPage", _pagesize.ToString(), 14400);
                }
            }
            Response.Redirect(Utils.CombUrlTxt("subject_list.aspx", "keywords={0}&vid={1}", this.keywords, DTRequest.GetQueryString("vid")));
        }

        protected void rptList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            //添加问题
            string vid = ddlvotelist.SelectedValue;
            string title = txtTitle.Text.Trim();
            string type = ddltype.SelectedValue;
            string sort = txtSort.Text.Trim();
            if (vid == "0")
            {
                ClientScript.RegisterClientScriptBlock(Page.GetType(), "JsPrint", "<script>alert('请选择问卷')</script>", false);

                return;
            }

           DTcms.Model.vote_subject model = new DTcms.Model.vote_subject();
            DTcms.BLL.vote_subject sub_bll = new BLL.vote_subject();

            model.vote_id = int.Parse(vid);
            model.type = int.Parse(type);
            model.title = txtTitle.Text.Trim();
            model.is_img = 0;
            model.is_user = 0;
            model.sort_id = int.Parse(txtSort.Text.Trim());
            model.add_time = System.DateTime.Now;
            if (hdItemId.Value != "")
            {
                model.id = int.Parse(hdItemId.Value);
                sub_bll.Update(model);
            }
            else
            {
                sub_bll.Add(model);
            }
            JscriptMsg("保存成功！", string.Empty);

            // RptBind(string.Concat("id>0", this.CombSqlTxt(this.keywords, this.property)), "add_time desc");
            RptBind(string.Concat("id>0 and vote_id='" + vote_id + "'", this.CombSqlTxt(this.keywords, this.property)), "sort_id desc");
        }

        protected string GetvoteName(string vote_id)
        {
            DTcms.BLL.vote bll = new BLL.vote();
            var model = bll.GetModel(int.Parse(vote_id));
            if (model != null)
            {
                return model.title;
            }
            else
            {
                return "";
            }
            
        }
        protected string GetsubjectType(string type_id)
        {

            if (type_id == "1")
            {
                return "单选";
            }
            else if (type_id == "2")
            {
                return "多选";
            }
            else if (type_id == "3")
            {
                return "问答";
            }
            else
            {
                return "-";
            }

        }
    }
}