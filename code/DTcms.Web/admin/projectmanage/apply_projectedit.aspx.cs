﻿using System;
using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DTcms.Common;

namespace DTcms.Web.admin.projectmanage
{
    public partial class apply_projectedit : Web.UI.ManagePage
    {
        private string action = DTEnums.ActionEnum.Add.ToString(); //操作类型
        private int id = 0;

        BLL.ky_applyproject Ky_Projects_bll = new BLL.ky_applyproject();

        BLL.ky_attachment attachment_bll = new BLL.ky_attachment();
        protected void Page_Load(object sender, EventArgs e)
        {
            string _action = DTRequest.GetQueryString("action");
            if (!string.IsNullOrEmpty(_action) && _action == DTEnums.ActionEnum.Edit.ToString())
            {
                this.action = DTEnums.ActionEnum.Edit.ToString();//修改类型
                if (!int.TryParse(Request.QueryString["id"] as string, out this.id))
                {
                    JscriptMsg("传输参数不正确！", "back");
                    return;
                }
                if (!Ky_Projects_bll.Exists(this.id))
                {
                    JscriptMsg("记录不存在或已被删除！", "back");
                    return;
                }
            }

            if (!IsPostBack)
            {
                ChkAdminLevel("apply_projectlist", DTEnums.ActionEnum.View.ToString()); //检查权限
               // Model.manager model = GetAdminInfo(); //取得管理员信息
                InitData();
                if (action == DTEnums.ActionEnum.Edit.ToString()) //修改
                {
                    ShowInfo(this.id);
                }
            }
        }

        #region 初始化===================================

        void InitData()
        {
            var hospatil = new BLL.yjr_doc_hospital().GetModelList("");
            foreach (var item in hospatil)
            {
                ddlhospital.Items.Add(new ListItem(item.hospitalname, item.id.ToString()));
            }

            var project_type = new BLL.ky_dictionary().GetModelListByCache("project_category");
            foreach (var item in project_type)
            {
                ddl_projecttype.Items.Add(new ListItem(item.dic_text, item.dic_value.ToString()));
            }

            var project_yanjiu_xingshi = new BLL.ky_dictionary().GetModelListByCache("yanjiu_xingshi");
            foreach (var item in project_yanjiu_xingshi)
            {
                ddl_base_yanjiu_xingshi.Items.Add(new ListItem(item.dic_text, item.dic_value.ToString()));
            }
            txt_owner.Text = GetAdminInfo().real_name;
        }
        #endregion

        #region 赋值操作=================================
        private void ShowInfo(int _id)
        {
            var model = Ky_Projects_bll.GetModel(_id);
            txt_title.Text = model.pa_title;
            txt_number.Text = model.pa_code;
            ddl_projecttype.SelectedValue = model.pa_type;
            ddlhospital.SelectedValue = model.pa_dptid;
            txt_owner.Text = model.pa_user;


            txt_dian_zi_you_xiang.Text = model.dian_zi_you_xiang;
            txt_shou_ji_hao_ma.Text = model.shou_ji_hao_ma;
            txt_shen_qing_ri_qi.Text = model.shen_qing_ri_qi;

            txt_base_didian.Text = model.base_didian;
            ddl_base_yanjiu_xingshi.SelectedValue = model.base_yanjiu_xingshi;

            txt_base_qizhishijian.Text = model.base_qizhishijian.Split('|')[0];
            txt_base_qizhishijian_end.Text = model.base_qizhishijian.Split('|').Length > 1 ? model.base_qizhishijian.Split('|')[1] : "";


            txt_base_hezuo_danwei.Text = model.base_hezuo_danwei;
            txt_base_hezuo_dizhi.Text = model.base_hezuo_dizhi;
            txt_base_hezuo_lianxiren.Text = model.base_hezuo_lianxiren;
            txt_base_hezuo_lianxidianhua.Text = model.base_hezuo_lianxidianhua;

            txt_base_xmgk_zhaiyao.Text = model.base_xmgk_zhaiyao;
            txt_base_xmgk_neirongmubiao.Text = model.base_xmgk_neirongmubiao;
            txt_base_xmgk_jishuluxian.Text = model.base_xmgk_jishuluxian;
            txt_base_xmgk_chuangxindian.Text = model.base_xmgk_chuangxindian;
            txt_base_xmgk_jichutiaojian.Text = model.base_xmgk_jichutiaojian;
            txt_base_xmgk_xiaoyi.Text = model.base_xmgk_xiaoyi;
            txt_base_xmgk_zhibiao.Text = model.base_xmgk_zhibiao;	

            ////绑定内容附件
            //rptAttachList.DataSource = attachment_bll.GetModelList("apply_id='applyproject_" + this.id + "' "); ;//
            //rptAttachList.DataBind();

        }
        #endregion

        #region 增加操作=================================
        private bool DoAdd()
        {
            DTcms.Model.ky_applyproject model_add = new Model.ky_applyproject();
            model_add.pa_title = txt_title.Text;
            model_add.pa_code = txt_number.Text;
            model_add.pa_type = ddl_projecttype.SelectedValue;
            model_add.pa_dptid = ddlhospital.SelectedValue;
            model_add.pa_user = txt_owner.Text;
            model_add.dian_zi_you_xiang = txt_dian_zi_you_xiang.Text;
            model_add.shou_ji_hao_ma = txt_shou_ji_hao_ma.Text;
            model_add.shen_qing_ri_qi = txt_shen_qing_ri_qi.Text;

            model_add.base_didian = txt_base_didian.Text;
            model_add.base_yanjiu_xingshi = ddl_base_yanjiu_xingshi.SelectedValue;
            model_add.base_qizhishijian = txt_base_qizhishijian.Text + "|" + txt_base_qizhishijian_end.Text;
            model_add.base_hezuo_danwei = txt_base_hezuo_danwei.Text;
            model_add.base_hezuo_dizhi = txt_base_hezuo_dizhi.Text;
            model_add.base_hezuo_lianxiren = txt_base_hezuo_lianxiren.Text;
            model_add.base_hezuo_lianxidianhua = txt_base_hezuo_lianxidianhua.Text;

            model_add.base_xmgk_zhaiyao = txt_base_xmgk_zhaiyao.Text;
            model_add.base_xmgk_neirongmubiao = txt_base_xmgk_neirongmubiao.Text;
            model_add.base_xmgk_jishuluxian = txt_base_xmgk_jishuluxian.Text;
            model_add.base_xmgk_chuangxindian = txt_base_xmgk_chuangxindian.Text;
            model_add.base_xmgk_jichutiaojian = txt_base_xmgk_jichutiaojian.Text;
            model_add.base_xmgk_xiaoyi = txt_base_xmgk_xiaoyi.Text;
            model_add.base_xmgk_zhibiao = txt_base_xmgk_zhibiao.Text;

                
            model_add.add_user = GetAdminInfo().user_name.ToString();
            model_add.add_time = System.DateTime.Now;
           

            var id = Ky_Projects_bll.Add(model_add);
            if (id > 0)
            {

                //
                #region 保存附件====================
                //保存附件
                string[] attachFileNameArr = Request.Form.GetValues("hid_attach_filename");
                string[] attachFilePathArr = Request.Form.GetValues("hid_attach_filepath");
                string[] attachFileSizeArr = Request.Form.GetValues("hid_attach_filesize");
                string[] attachPointArr = Request.Form.GetValues("txt_attach_point");
                if (attachFileNameArr != null && attachFilePathArr != null && attachFileSizeArr != null
                    && attachFileNameArr.Length > 0 && attachFilePathArr.Length > 0 && attachFileSizeArr.Length > 0)
                {
                    List<Model.article_attach> ls = new List<Model.article_attach>();
                    for (int i = 0; i < attachFileNameArr.Length; i++)
                    {
                        int fileSize = Utils.StrToInt(attachFileSizeArr[i], 0);
                        string fileExt = FileHelper.GetFileExt(attachFilePathArr[i]);
                        //int _point = Utils.StrToInt(attachPointArr[i], 0);
                        //ls.Add(new Model.article_attach { channel_id = this.channel_id, file_name = attachFileNameArr[i], file_path = attachFilePathArr[i], file_size = fileSize, file_ext = fileExt, point = _point });

                        //保存附件数据
                        DTcms.Model.ky_attachment _Attachment = new Model.ky_attachment();
                        _Attachment.file_name = attachFileNameArr[i];
                        _Attachment.file_path = attachFilePathArr[i];
                        _Attachment.file_ext = fileExt;
                        _Attachment.file_size = fileSize;
                        _Attachment.p_id = id;
                        _Attachment.add_time = System.DateTime.Now;
                        _Attachment.apply_id = "applyproject_"+ id;

                        attachment_bll.Add(_Attachment);
                    }

                }
                #endregion

                //
                AddAdminLog(DTEnums.ActionEnum.Add.ToString(), "添加apply_project:" + model_add.pa_title); //记录日志
                return true;
            }
            return false;
        }
        #endregion

        #region 修改操作=================================
        private bool DoEdit(int _id)
        {
            bool result = false;

            DTcms.Model.ky_applyproject model_edit = Ky_Projects_bll.GetModel(_id);
            model_edit.pa_title = txt_title.Text;
            model_edit.pa_code = txt_number.Text;
            model_edit.pa_type = ddl_projecttype.SelectedValue;
            model_edit.pa_dptid = ddlhospital.SelectedValue;
            model_edit.pa_user = txt_owner.Text;


            model_edit.dian_zi_you_xiang = txt_dian_zi_you_xiang.Text;
            model_edit.shou_ji_hao_ma = txt_shou_ji_hao_ma.Text;
            model_edit.shen_qing_ri_qi = txt_shen_qing_ri_qi.Text;

            model_edit.base_didian = txt_base_didian.Text;
            model_edit.base_yanjiu_xingshi = ddl_base_yanjiu_xingshi.SelectedValue;
            model_edit.base_qizhishijian = txt_base_qizhishijian.Text+"|"+txt_base_qizhishijian_end.Text;
            model_edit.base_hezuo_danwei = txt_base_hezuo_danwei.Text;
            model_edit.base_hezuo_dizhi = txt_base_hezuo_dizhi.Text;
            model_edit.base_hezuo_lianxiren = txt_base_hezuo_lianxiren.Text;
            model_edit.base_hezuo_lianxidianhua = txt_base_hezuo_lianxidianhua.Text;

            model_edit.base_xmgk_zhaiyao = txt_base_xmgk_zhaiyao.Text;
            model_edit.base_xmgk_neirongmubiao = txt_base_xmgk_neirongmubiao.Text;
            model_edit.base_xmgk_jishuluxian = txt_base_xmgk_jishuluxian.Text;
            model_edit.base_xmgk_chuangxindian = txt_base_xmgk_chuangxindian.Text;
            model_edit.base_xmgk_jichutiaojian = txt_base_xmgk_jichutiaojian.Text;
            model_edit.base_xmgk_xiaoyi = txt_base_xmgk_xiaoyi.Text;
            model_edit.base_xmgk_zhibiao = txt_base_xmgk_zhibiao.Text;


            if (Ky_Projects_bll.Update(model_edit))
            {

                #region 修改并保存附件====================
                
                //apply_project 目前无附件字段 2021-07-28

                //修改并保存附件
                //清除原来的附件列表 再保存
            
                //attachment_bll.Deletebyapply_id("applyproject_" + _id.ToString());

                //string[] attachFileNameArr = Request.Form.GetValues("hid_attach_filename");
                //string[] attachFilePathArr = Request.Form.GetValues("hid_attach_filepath");
                //string[] attachFileSizeArr = Request.Form.GetValues("hid_attach_filesize");
                //string[] attachPointArr = Request.Form.GetValues("txt_attach_point");
                //if (attachFileNameArr != null && attachFilePathArr != null && attachFileSizeArr != null
                //    && attachFileNameArr.Length > 0 && attachFilePathArr.Length > 0 && attachFileSizeArr.Length > 0)
                //{
                //    List<Model.article_attach> ls = new List<Model.article_attach>();
                //    for (int i = 0; i < attachFileNameArr.Length; i++)
                //    {
                //        int fileSize = Utils.StrToInt(attachFileSizeArr[i], 0);
                //        string fileExt = FileHelper.GetFileExt(attachFilePathArr[i]);
                //        //int _point = Utils.StrToInt(attachPointArr[i], 0);
                //        //ls.Add(new Model.article_attach { channel_id = this.channel_id, file_name = attachFileNameArr[i], file_path = attachFilePathArr[i], file_size = fileSize, file_ext = fileExt, point = _point });

                //        //保存附件数据
                //        DTcms.Model.ky_attachment _Attachment = new Model.ky_attachment();
                //        _Attachment.file_name = attachFileNameArr[i];
                //        _Attachment.file_path = attachFilePathArr[i];
                //        _Attachment.file_ext = fileExt;
                //        _Attachment.file_size = fileSize;
                //        _Attachment.p_id = id;
                //        _Attachment.add_time = System.DateTime.Now;
                //        _Attachment.apply_id = "applyproject_" + _id.ToString();

                //        attachment_bll.Add(_Attachment);
                //    }

                //}
                #endregion

                AddAdminLog(DTEnums.ActionEnum.Add.ToString(), "修改apply_project:" + model_edit.pa_title); //记录日志
                result = true;
            }

            return result;
        }
        #endregion

        //保存
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (action == DTEnums.ActionEnum.Edit.ToString()) //修改
            {
                ChkAdminLevel("apply_projectlist", DTEnums.ActionEnum.Edit.ToString()); //检查权限
                if (!DoEdit(this.id))
                {
                    JscriptMsg("保存过程中发生错误！", "");
                    return;
                }
                JscriptMsg("修改信息成功！", "apply_projectlist.aspx");
            }
            else //添加
            {
                ChkAdminLevel("apply_projectlist", DTEnums.ActionEnum.Add.ToString()); //检查权限
                if (!DoAdd())
                {
                    JscriptMsg("保存过程中发生错误！", "");
                    return;
                }
                JscriptMsg("添加信息成功！", "apply_projectlist.aspx");
            }
        }



        protected void ddlhospital_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (ddlhospital.SelectedValue != "0")
            //{
            //    BLL.yjr_doctor_group bll = new BLL.yjr_doctor_group();

            //    var group = bll.GetModelList(" hospatilid=" + ddlhospital.SelectedValue);
            //    foreach (var item in group)
            //    {
            //        ddsection.Items.Add(new ListItem(item.groupname, item.id.ToString()));
            //    }
            //}
            //else
            //{
            //    ddsection.Items.Clear();
            //}
        }
    }
}