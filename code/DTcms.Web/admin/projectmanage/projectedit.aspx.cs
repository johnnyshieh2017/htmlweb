﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DTcms.Common;

namespace DTcms.Web.admin.projectmanage
{
    public partial class projectedit : Web.UI.ManagePage
    {
        private string action = DTEnums.ActionEnum.Add.ToString(); //操作类型
        private int id = 0;
      
        BLL.ky_projects Ky_Projects_bll = new BLL.ky_projects();

        BLL.ky_attachment attachment_bll = new BLL.ky_attachment();
        protected void Page_Load(object sender, EventArgs e)
        {
            string _action = DTRequest.GetQueryString("action");
            if (!string.IsNullOrEmpty(_action) && _action == DTEnums.ActionEnum.Edit.ToString())
            {
                this.action = DTEnums.ActionEnum.Edit.ToString();//修改类型
                if (!int.TryParse(Request.QueryString["id"] as string, out this.id))
                {
                    JscriptMsg("传输参数不正确！", "back");
                    return;
                }
                if (!new BLL.ky_projects().Exists(this.id))
                {
                    JscriptMsg("记录不存在或已被删除！", "back");
                    return;
                }
            }

            if (!IsPostBack)
            {
                ChkAdminLevel("projectlist", DTEnums.ActionEnum.View.ToString()); //检查权限
                Model.manager model = GetAdminInfo(); //取得管理员信息
                InitData();
                if (action == DTEnums.ActionEnum.Edit.ToString()) //修改
                {
                    ShowInfo(this.id);
                }
            }
        }

        #region 初始化===================================

        void InitData()
        {

            var hospatil = new BLL.yjr_doc_hospital().GetModelList("");
            foreach (var item in hospatil)
            {
                ddlhospital.Items.Add(new ListItem(item.hospitalname, item.id.ToString()));
            }

            var project_type = new BLL.ky_dictionary().GetModelListByCache("project_type");
            foreach (var item in project_type)
            {
                ddl_projecttype.Items.Add(new ListItem(item.dic_text, item.dic_value.ToString()));
            }


            var apply_project = new BLL.ky_applyproject().GetModelList("status=1");
            foreach (var item in apply_project)
            {
                ddlapply_project.Items.Add(new ListItem(item.pa_title, item.pa_id.ToString()));
            }
        }
        #endregion

        #region 赋值操作=================================
        private void ShowInfo(int _id)
        {
            var model = Ky_Projects_bll.GetModel(_id);

            ddlapply_project.SelectedValue = model.pa_id;
            txt_number.Text = model.project_number;
            txt_title.Text = model.project_title;
            ddlhospital.SelectedValue = model.dptid.ToString();
            
            txt_owner.Text = model.owner;
            txt_tel.Text = model.tel;
            txt_start_time.Text = model.start_time;
            txt_end_time.Text = model.end_time;
            ddl_projecttype.SelectedValue = model.project_type.ToString();
            txt_fee_xiada.Text = model.fee_xiada;
            txt_fee_peitao.Text = model.fee_peitao;
            txt_fee_zongji.Text = model.fee_zongji;
            txt_member.Text = model.member;
            txt_intro.Value = model.remark;

            //绑定内容附件
            rptAttachList.DataSource = attachment_bll.GetModelList("p_id=" + _id + " and apply_id='' "); ;//  model.attach; //获取附件
            rptAttachList.DataBind();

        }
        #endregion

        #region 增加操作=================================
        private bool DoAdd()
        {
            DTcms.Model.ky_projects model_add = new Model.ky_projects();
            model_add.pa_id = ddlapply_project.SelectedValue;
            model_add.project_number = txt_number.Text;
            model_add.project_title = txt_title.Text;
            model_add.dptid = int.Parse(ddlhospital.SelectedValue);
            model_add.owner = txt_owner.Text;
            model_add.tel = txt_tel.Text;
            model_add.start_time = txt_start_time.Text;
            model_add.end_time = txt_end_time.Text;
            model_add.project_type = int.Parse(ddl_projecttype.SelectedValue);
            model_add.fee_xiada = txt_fee_xiada.Text;
            model_add.fee_peitao = txt_fee_peitao.Text;
            model_add.fee_zongji = txt_fee_zongji.Text;
            model_add.member = txt_member.Text;
            model_add.remark = txt_intro.Value;
            model_add.add_time = System.DateTime.Now;
            var id = Ky_Projects_bll.Add(model_add);
            if ( id> 0)
            {

                //
                #region 保存附件====================
                //保存附件
                string[] attachFileNameArr = Request.Form.GetValues("hid_attach_filename");
                string[] attachFilePathArr = Request.Form.GetValues("hid_attach_filepath");
                string[] attachFileSizeArr = Request.Form.GetValues("hid_attach_filesize");
                string[] attachPointArr = Request.Form.GetValues("txt_attach_point");
                if (attachFileNameArr != null && attachFilePathArr != null && attachFileSizeArr != null 
                    && attachFileNameArr.Length > 0 && attachFilePathArr.Length > 0 && attachFileSizeArr.Length > 0 )
                {
                    List<Model.article_attach> ls = new List<Model.article_attach>();
                    for (int i = 0; i < attachFileNameArr.Length; i++)
                    {
                        int fileSize = Utils.StrToInt(attachFileSizeArr[i], 0);
                        string fileExt = FileHelper.GetFileExt(attachFilePathArr[i]);
                        //int _point = Utils.StrToInt(attachPointArr[i], 0);
                        //ls.Add(new Model.article_attach { channel_id = this.channel_id, file_name = attachFileNameArr[i], file_path = attachFilePathArr[i], file_size = fileSize, file_ext = fileExt, point = _point });

                        //保存附件数据
                        DTcms.Model.ky_attachment _Attachment = new Model.ky_attachment();
                        _Attachment.file_name = attachFileNameArr[i];
                        _Attachment.file_path = attachFilePathArr[i];
                        _Attachment.file_ext = fileExt;
                        _Attachment.file_size = fileSize;
                        _Attachment.p_id = id;
                        _Attachment.add_time = System.DateTime.Now;
                        _Attachment.apply_id = "";

                        attachment_bll.Add(_Attachment);
                    }
                   
                }
                #endregion

                //
                AddAdminLog(DTEnums.ActionEnum.Add.ToString(), "添加doctor:" + model_add.project_title); //记录日志
                return true;
            }
            return false;
        }
        #endregion

        #region 修改操作=================================
        private bool DoEdit(int _id)
        {
            bool result = false;

            DTcms.Model.ky_projects model_edit = Ky_Projects_bll.GetModel(_id);

            model_edit.pa_id = ddlapply_project.SelectedValue;
            model_edit.project_number = txt_number.Text;
            model_edit.project_title = txt_title.Text;
            model_edit.dptid = int.Parse(ddlhospital.SelectedValue);
            model_edit.owner = txt_owner.Text;
            model_edit.tel = txt_tel.Text;
            model_edit.start_time = txt_start_time.Text;
            model_edit.end_time = txt_end_time.Text;
            model_edit.project_type = int.Parse(ddl_projecttype.SelectedValue);
            model_edit.fee_xiada = txt_fee_xiada.Text;
            model_edit.fee_peitao = txt_fee_peitao.Text;
            model_edit.fee_zongji = txt_fee_zongji.Text;
            model_edit.member = txt_member.Text;
            model_edit.remark = txt_intro.Value;


            if (Ky_Projects_bll.Update(model_edit))
            {

                #region 修改并保存附件====================
                //修改并保存附件
                //清除原来的附件列表 再保存
                attachment_bll.DeletebyP_id(_id.ToString());

                string[] attachFileNameArr = Request.Form.GetValues("hid_attach_filename");
                string[] attachFilePathArr = Request.Form.GetValues("hid_attach_filepath");
                string[] attachFileSizeArr = Request.Form.GetValues("hid_attach_filesize");
                string[] attachPointArr = Request.Form.GetValues("txt_attach_point");
                if (attachFileNameArr != null && attachFilePathArr != null && attachFileSizeArr != null 
                    && attachFileNameArr.Length > 0 && attachFilePathArr.Length > 0 && attachFileSizeArr.Length > 0 )
                {
                    List<Model.article_attach> ls = new List<Model.article_attach>();
                    for (int i = 0; i < attachFileNameArr.Length; i++)
                    {
                        int fileSize = Utils.StrToInt(attachFileSizeArr[i], 0);
                        string fileExt = FileHelper.GetFileExt(attachFilePathArr[i]);
                        //int _point = Utils.StrToInt(attachPointArr[i], 0);
                        //ls.Add(new Model.article_attach { channel_id = this.channel_id, file_name = attachFileNameArr[i], file_path = attachFilePathArr[i], file_size = fileSize, file_ext = fileExt, point = _point });

                        //保存附件数据
                        DTcms.Model.ky_attachment _Attachment = new Model.ky_attachment();
                        _Attachment.file_name = attachFileNameArr[i];
                        _Attachment.file_path = attachFilePathArr[i];
                        _Attachment.file_ext = fileExt;
                        _Attachment.file_size = fileSize;
                        _Attachment.p_id = id;
                        _Attachment.add_time = System.DateTime.Now;
                        _Attachment.apply_id = "";

                        attachment_bll.Add(_Attachment);
                    }

                }
                #endregion

                AddAdminLog(DTEnums.ActionEnum.Add.ToString(), "修改project:" + model_edit.project_number); //记录日志
                result = true;
            }

            return result;
        }
        #endregion

        //保存
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (action == DTEnums.ActionEnum.Edit.ToString()) //修改
            {
                ChkAdminLevel("projectlist", DTEnums.ActionEnum.Edit.ToString()); //检查权限
                if (!DoEdit(this.id))
                {
                    JscriptMsg("保存过程中发生错误！", "");
                    return;
                }
                JscriptMsg("修改信息成功！", "projectlist.aspx");
            }
            else //添加
            {
                ChkAdminLevel("projectlist", DTEnums.ActionEnum.Add.ToString()); //检查权限
                if (!DoAdd())
                {
                    JscriptMsg("保存过程中发生错误！", "");
                    return;
                }
                JscriptMsg("添加信息成功！", "projectlist.aspx");
            }
        }



        protected void ddlhospital_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (ddlhospital.SelectedValue != "0")
            //{
            //    BLL.yjr_doctor_group bll = new BLL.yjr_doctor_group();

            //    var group = bll.GetModelList(" hospatilid=" + ddlhospital.SelectedValue);
            //    foreach (var item in group)
            //    {
            //        ddsection.Items.Add(new ListItem(item.groupname, item.id.ToString()));
            //    }
            //}
            //else
            //{
            //    ddsection.Items.Clear();
            //}
        }

        protected void ddlapply_project_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlapply_project.SelectedValue != "")
            {
                var model = new BLL.ky_applyproject().GetModel(int.Parse(ddlapply_project.SelectedValue));
                if (model != null)
                {
                    txt_number.Text = model.pa_code;
                    txt_title.Text = model.pa_title;
                    ddlhospital.SelectedValue = model.pa_dptid;
                    txt_owner.Text = model.pa_user;
                    txt_tel.Text = model.shou_ji_hao_ma;
                }
            }
        }
    }
}