﻿using Aspose.Words;
using Aspose.Words.Tables;
using DTcms.Common;
using DTcms.Web.api.Models;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;

namespace DTcms.Web.admin.projectmanage
{
    /// <summary>
    /// ajax 的摘要说明
    /// </summary>
    public class ajax : IHttpHandler
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public void ProcessRequest(HttpContext context)
        {
            //取得处事类型
            string action = DTRequest.GetQueryString("action");


            switch (action)
            {
                case "getjieyu": // 
                    get(context);
                    break;

                case "add_team_person": // 
                    add_projectteam(context);
                    break;

                case "edit_team_person":
                    edit_rojectteam(context);
                    break;

                case "del_team_person":
                    del_rojectteam(context);
                    break;

                case "add_equipment":
                    add_equipment(context);
                    break;
                case "edit_equipment":
                    edit_equipment(context);
                    break;
                case "del_equipment":
                    del_equipment(context);
                    break;
                case "create_projectpdf":
                    create_projectpdf(context);
                    break;
            }
           
        }


        private static List<DTcms.Model.ky_applyproject_shebei> MakeListData(int pa_id)
        {
            //List<DTcms.Model.ky_applyproject_shebei> list = new List<DTcms.Model.ky_applyproject_shebei>();
            //DTcms.Model.ky_applyproject_shebei model1 = new DTcms.Model.ky_applyproject_shebei { id=1, shebei="设备1", xinghao="001", jiage="12", tianjia_liyou="ly1"};
            //DTcms.Model.ky_applyproject_shebei model2 = new DTcms.Model.ky_applyproject_shebei { id = 2, shebei = "设备2", xinghao = "002", jiage = "22",tianjia_liyou = "ly2" };
            //list.Add(model1);
            //list.Add(model2);
            var list = new BLL.ky_applyproject_shebei().GetModelList("pa_id="+pa_id);

            return list;
        }

        private static List<DTcms.Model.ky_applyproject_team> MakeTeamListData(int pa_id)
        {
            List<DTcms.Model.ky_applyproject_team> list = new BLL.ky_applyproject_team().GetModelList("pa_id="+ pa_id);
            

            return list;
        }

        Aspose.Words.Tables.Cell CreateCell(string value, Document doc)
        {
            Aspose.Words.Tables.Cell c1 = new Aspose.Words.Tables.Cell(doc);
            Aspose.Words.Paragraph p = new Paragraph(doc);
            p.AppendChild(new Run(doc, value));
            c1.AppendChild(p);
            return c1;
        }


        /// <summary>
        /// 项目团队表格 创建行
        /// </summary>
        /// <param name="table"></param>
        /// <param name="columnValues"></param>
        /// <param name="doc"></param>
        /// <returns></returns>
        Aspose.Words.Tables.Row CreateRow(Aspose.Words.Tables.Table table, string[] columnValues, Document doc)
        {
            //Aspose.Words.Tables.Row r2 = new Aspose.Words.Tables.Row(doc);
            Aspose.Words.Tables.Row clonedRow = (Row)table.LastRow.Clone(true);
           
            for (int i = 0; i < clonedRow.Cells.Count; i++)
            {
                //Cell lshCell = clonedRow.Cells[i];
                //clonedRow.Cells[i].FirstParagraph.Remove();
                clonedRow.Cells[i].Paragraphs.Clear();
                //新建一个段落
                Paragraph p = new Paragraph(doc);

                p.AppendChild(new Run(doc, columnValues[i]));
                clonedRow.Cells[i].AppendChild(p);
            }

            return clonedRow;

        }
        Aspose.Words.Tables.Row CreateRow(Aspose.Words.Tables.Table table, Model.ky_applyproject_team model, Document doc)
        {
            //Aspose.Words.Tables.Row r2 = new Aspose.Words.Tables.Row(doc);
            Aspose.Words.Tables.Row clonedRow = (Row)table.LastRow.Clone(true);

            for (int i = 0; i < clonedRow.Cells.Count; i++)
            {
                //Cell lshCell = clonedRow.Cells[i];
                //clonedRow.Cells[i].FirstParagraph.Remove();
                clonedRow.Cells[i].Paragraphs.Clear();
                //新建一个段落
                Paragraph p = new Paragraph(doc);

                p.AppendChild(new Run(doc, GetTeamModelValue(i, model)));
                clonedRow.Cells[i].AppendChild(p);
            }

            return clonedRow;

        }

        string GetTeamModelValue(int index, Model.ky_applyproject_team model)
        {
            var s = "";
            switch (index)
            {
                
                case 0 :
                    s = model.name;
                    break;
                case 1:
                    s = model.chusheng;
                    break;
                case 2:
                    s = model.sex;
                    break;
                case 3:
                    s = model.zhuanye;
                    break;
                case 4:
                    s = model.zhicheng;
                    break;
                case 5:
                    s = model.xueli;
                    break;
                case 6:
                    s = model.danwei;
                    break;
                case 7:
                    s = model.fengong;
                    break;
                case 8:
                    s = "";
                    break;
                default:
                    break;
            }

            return s;
        }
        string GetQishiRiqi(string s)
        {
            if (s.Contains("|"))
            {
                return s.Replace("|", "至");
            }
            else
            {
                return s;
            }
        }
        private void create_projectpdf(HttpContext context)
        {
            int pa_id = DTRequest.GetFormIntValue("pa_id", 0);
            JsonResult<string> res = new JsonResult<string>();
            res.ResultCode = 0;
            res.ResultMessage = "error";

            BLL.ky_applyproject bll = new BLL.ky_applyproject();
            if (bll.Exists(pa_id))
            {
                string tempFile = context.Server.MapPath("xadsyy_project_apply.doc"); //Path.GetFullPath(context.Server.MapPath("xadsyy_project_apply.doc")).ToString();      //获取模板路径，这个根据个人模板路径而定。
                Document doc = new Document(tempFile);
                DocumentBuilder builder = new DocumentBuilder(doc);   //操作word



                #region 处理一般 word doc 中 bookmark 的标记值 

                Dictionary<string, string> dic = new Dictionary<string, string>(); //创建键值对 第一个string 为书签名称 第二个string为要填充的数据
                var base_info = bll.GetModel(pa_id);

                //dic.Add("project_title", "基于xxxx技术的2021xxxx研究"); //项目名称
                //dic.Add("danwei_name", "陕西省科技局"); //单位
                dic.Add("project_title", base_info.pa_title); //项目名称
                dic.Add("project_title_first", base_info.pa_title);
                dic.Add("project_dpt", base_info.pa_dptname);
                dic.Add("project_owner", base_info.pa_user);
                dic.Add("project_email", base_info.dian_zi_you_xiang);
                dic.Add("project_telephone", base_info.shou_ji_hao_ma);
                dic.Add("project_apply_date", base_info.shen_qing_ri_qi);

                dic.Add("project_category", data_tool.Getproject_category(base_info.pa_type));
                dic.Add("base_didian", base_info.base_didian);
                dic.Add("base_qizhishijian", GetQishiRiqi(base_info.base_qizhishijian));
                dic.Add("base_hezuo_danwei", base_info.base_hezuo_danwei);
                dic.Add("base_hezuo_dizhi", base_info.base_hezuo_dizhi);
                dic.Add("base_hezuo_lianxiren", base_info.base_hezuo_lianxiren);
                dic.Add("base_hezuo_lianxidianhua", base_info.base_hezuo_lianxidianhua);

                dic.Add("base_xmgk_zhaiyao", base_info.base_xmgk_zhaiyao);
                dic.Add("base_xmgk_neirongmubiao", base_info.base_xmgk_neirongmubiao);
                dic.Add("base_xmgk_jishuluxian", base_info.base_xmgk_jishuluxian);
                dic.Add("base_xmgk_chuangxindian", base_info.base_xmgk_chuangxindian);
                dic.Add("base_xmgk_jichutiaojian", base_info.base_xmgk_jichutiaojian);
                dic.Add("base_xmgk_xiaoyi", base_info.base_xmgk_xiaoyi);
                dic.Add("base_xmgk_zhibiao", base_info.base_xmgk_zhibiao);

                dic.Add("team_owner_name", base_info.team_owner_name);
                dic.Add("team_owner_sex", base_info.team_owner_sex);
                dic.Add("team_owner_chusheng", base_info.team_owner_chusheng);
                dic.Add("team_owner_xueli", base_info.team_owner_xueli);
                dic.Add("team_owner_zhiwu", base_info.team_owner_zhiwu);
                dic.Add("team_owner_zhicheng", base_info.team_owner_zhicheng);
                dic.Add("team_owner_zhuanye", base_info.team_owner_zhuanye);
                dic.Add("team_owner_shouji", base_info.team_owner_shouji);
                dic.Add("team_owner_jianjie", base_info.team_owner_jianjie);

                //项目经费情况
                var fee = new BLL.ky_applyproject_jingfei().GetModelList("pa_id=" + pa_id).FirstOrDefault();
                dic.Add("laiyuan_zhuanxiang", fee.laiyuan_zhuanxiang);
                dic.Add("laiyuan_qita", fee.laiyuan_qita);
                dic.Add("zhijie", fee.zhijie);
                dic.Add("zhijie1", fee.zhijie1);
                dic.Add("zhijie2", fee.zhijie2);
                dic.Add("zhijie_shebei", fee.zhijie_shebei);
                dic.Add("zhijie_shebei1", fee.zhijie_shebei1);
                dic.Add("zhijie_shebei2", fee.zhijie_shebei2);
                dic.Add("zhijie_sb_gouzhi", fee.zhijie_sb_gouzhi);
                dic.Add("zhijie_sb_gouzhi1", fee.zhijie_sb_gouzhi1);
                dic.Add("zhijie_sb_gouzhi2", fee.zhijie_sb_gouzhi2);

                dic.Add("zhijie_sb_shizhi", fee.zhijie_sb_shizhi);
                dic.Add("zhijie_sb_shizhi1", fee.zhijie_sb_shizhi1);
                dic.Add("zhijie_sb_shizhi2", fee.zhijie_sb_shizhi2);

                dic.Add("zhijie_sb_gaizao", fee.zhijie_sb_gaizao);
                dic.Add("zhijie_sb_gaizao1", fee.zhijie_sb_gaizao1);
                dic.Add("zhijie_sb_gaizao2", fee.zhijie_sb_gaizao2);

                dic.Add("zhijie_cailiao", fee.zhijie_cailiao);
                dic.Add("zhijie_cailiao1", fee.zhijie_cailiao1);
                dic.Add("zhijie_cailiao2", fee.zhijie_cailiao2);

                dic.Add("zhijie_ceshi", fee.zhijie_ceshi);
                dic.Add("zhijie_ceshi1", fee.zhijie_ceshi1);
                dic.Add("zhijie_ceshi2", fee.zhijie_ceshi2);

                dic.Add("zhijie_ranliao", fee.zhijie_ranliao);
                dic.Add("zhijie_ranliao1", fee.zhijie_ranliao1);
                dic.Add("zhijie_ranliao2", fee.zhijie_ranliao2);

                dic.Add("zhijie_chailv", fee.zhijie_chailv);
                dic.Add("zhijie_chailv1", fee.zhijie_chailv1);
                dic.Add("zhijie_chailv2", fee.zhijie_chailv2);

                dic.Add("zhijie_huiyi", fee.zhijie_huiyi);
                dic.Add("zhijie_huiyi1", fee.zhijie_huiyi1);
                dic.Add("zhijie_huiyi2", fee.zhijie_huiyi2);

                dic.Add("zhijie_hezuojiaoliu", fee.zhijie_hezuojiaoliu);
                dic.Add("zhijie_hezuojiaoliu1", fee.zhijie_hezuojiaoliu1);
                dic.Add("zhijie_hezuojiaoliu2", fee.zhijie_hezuojiaoliu2);

                dic.Add("zhijie_xinxi", fee.zhijie_xinxi);
                dic.Add("zhijie_xinxi1", fee.zhijie_xinxi1);
                dic.Add("zhijie_xinxi2", fee.zhijie_xinxi2);

                dic.Add("zhijie_zixun", fee.zhijie_zixun);
                dic.Add("zhijie_zixun1", fee.zhijie_zixun1);
                dic.Add("zhijie_zixun2", fee.zhijie_zixun2);

                dic.Add("zhijie_laowu", fee.zhijie_laowu);
                dic.Add("zhijie_laowu1", fee.zhijie_laowu1);
                dic.Add("zhijie_laowu2", fee.zhijie_laowu2);

                dic.Add("zhijie_qita", fee.zhijie_qita);
                dic.Add("zhijie_qita1", fee.zhijie_qita1);
                dic.Add("zhijie_qita2", fee.zhijie_qita2);

                dic.Add("jianjie", fee.jianjie);
                dic.Add("jianjie1", fee.jianjie1);
                dic.Add("jianjie2", fee.jianjie2);

                dic.Add("jianjie_guanli", fee.jianjie_guanli);
                dic.Add("jianjie_guanli1", fee.jianjie_guanli1);
                dic.Add("jianjie_guanli2", fee.jianjie_guanli2);

                dic.Add("jianjie_jixiao", fee.jianjie_jixiao);
                dic.Add("jianjie_jixiao1", fee.jianjie_jixiao1);
                dic.Add("jianjie_jixiao2", fee.jianjie_jixiao2);

                dic.Add("heji", fee.heji);
                dic.Add("heji1", fee.heji1);
                dic.Add("heji2", fee.heji2);

                //进度计划
                dic.Add("jindu_jihua", base_info.jindu_jihua);

                //项目绩效目标
                var jixiao =new BLL.ky_applyproject_jixiao().GetModelList("pa_id=" + pa_id).FirstOrDefault();
                dic.Add("zhuanli_shuliang", jixiao.zhuanli_shuliang);
                dic.Add("zhuanli_shuliang_faming", jixiao.zhuanli_shuliang_faming);

                dic.Add("zhuanli_shuliang_shiyongxinxing", jixiao.zhuanli_shuliang_shiyongxinxing);
                dic.Add("zhuanli_shuliang_waiguan", jixiao.zhuanli_shuliang_waiguan);
                dic.Add("zhuanli_shouquan_shuliang", jixiao.zhuanli_shouquan_shuliang);
                dic.Add("zhuanli_shouquan_faming", jixiao.zhuanli_shouquan_faming);
                dic.Add("zhuanli_shouquan_shiyongxinxing", jixiao.zhuanli_shouquan_shiyongxinxing);
                dic.Add("zhuanli_shouquan_waiguan", jixiao.zhuanli_shouquan_waiguan);

                dic.Add("ruanzhu_shuliang", jixiao.ruanzhu_shuliang);
                dic.Add("lunwen_shuliang", jixiao.lunwen_shuliang);
                dic.Add("lunwen_shuliang_sci", jixiao.lunwen_shuliang_sci);
                dic.Add("lunwen_shuliang_ei", jixiao.lunwen_shuliang_ei);

                dic.Add("zhuzuo_shuliang", jixiao.zhuzuo_shuliang);
                dic.Add("biaozhun_shuliang", jixiao.biaozhun_shuliang);
                dic.Add("biaozhun_shuliang_guoji", jixiao.biaozhun_shuliang_guoji);
                dic.Add("biaozhun_shuliang_guojia", jixiao.biaozhun_shuliang_guojia);
                dic.Add("biaozhun_shuliang_hangye", jixiao.biaozhun_shuliang_hangye);
                dic.Add("biaozhun_shuliang_difang", jixiao.biaozhun_shuliang_difang);
                dic.Add("biaozhun_shuliang_qiye", jixiao.biaozhun_shuliang_qiye);

                dic.Add("qita_tianbujishukongbai_shuliang", jixiao.qita_tianbujishukongbai_shuliang);
                dic.Add("qita_tianbujishukongbai_shuliang_guoji", jixiao.qita_tianbujishukongbai_shuliang_guoji);
                dic.Add("qita_tianbujishukongbai_shuliang_shengji", jixiao.qita_tianbujishukongbai_shuliang_shengji);

                dic.Add("qita_huojiang_shuliang", jixiao.qita_huojiang_shuliang);
                dic.Add("qita_huojiang_shuliang_guojia", jixiao.qita_huojiang_shuliang_guojia);
                dic.Add("qita_huojiang_shuliang_sheng", jixiao.qita_huojiang_shuliang_sheng);
                dic.Add("qita_huojiang_shuliang_difang", jixiao.qita_huojiang_shuliang_difang);

                dic.Add("qita_qita_kejichengguo", jixiao.qita_qita_kejichengguo);
                dic.Add("qita_qita_xingongyi", jixiao.qita_qita_xingongyi);
                dic.Add("qita_qita_xinchanping", jixiao.qita_qita_xinchanping);
                dic.Add("qita_qita_xincailiao", jixiao.qita_qita_xincailiao);
                dic.Add("qita_qita_xinzhuangbei", jixiao.qita_qita_xinzhuangbei);
                dic.Add("qita_qita_pingtai", jixiao.qita_qita_pingtai);

                dic.Add("rencai_yj_shuliang", jixiao.rencai_yj_shuliang);
                dic.Add("rencai_yj_shuliang_boshi", jixiao.rencai_yj_shuliang_boshi);
                dic.Add("rencai_yj_shuliang_shuoshi", jixiao.rencai_yj_shuliang_shuoshi);
                dic.Add("rencai_py_shuliang", jixiao.rencai_py_shuliang);
                dic.Add("rencai_py_shuliang_boshi", jixiao.rencai_py_shuliang_boshi);
                dic.Add("rencai_py_shuliang_shuoshi", jixiao.rencai_py_shuliang_shuoshi);

                dic.Add("jingjixiaoyi", jixiao.jingjixiaoyi);
                dic.Add("shehuixiaoyi", jixiao.shehuixiaoyi);
                dic.Add("qitashuoming", jixiao.qitashuoming);

                foreach (var key in dic.Keys)   //循环键值对
                {
                    builder.MoveToBookmark(key); //将光标移入书签的位置
                    builder.Write(dic[key]);   //填充值
                }
                #endregion




                #region  团队成员
                NodeCollection allTables = doc.GetChildNodes(NodeType.Table, true); //拿到所有表格
                Aspose.Words.Tables.Table table = allTables[1] as Aspose.Words.Tables.Table; //拿到第二个表格 第二表格为团队成员表

                var teamlist = new BLL.ky_applyproject_team().GetModelList("pa_id=" + pa_id);

                foreach (var item in teamlist)
                {
                    var row = CreateRow(table, item, doc);
                    table.Rows.Add(row);
                }

                //var row = CreateRow(table, (new string[] { "马龙", "1982-11", "男","生物","高级","硕士","西安一所","工程师",""}), doc); //创建一行

                //var row2 = CreateRow(table, (new string[] { "21", "22", "23", "24", "25", "26", "27", "28", "29" }), doc);
                
                //table.Rows.Add(row);
                //table.Rows.Add(row2);

                #endregion  

                #region 团队成员表格---用书签标记 生成表格代码 废弃
                //builder.MoveToBookmark("teamlist");
                //#region team表头
                //string[] team_titles = new string[] { "姓名", "出生年月", "性别", "从事专业", "职称", "学历", "所在单位", "项目分工", "签名" };
                //int[] team_lens = new int[] { 15, 9, 8, 14, 10, 10, 13, 13, 10 };
                //for (int i = 0; i < team_titles.Length; i++)
                //{
                //    builder.Bold = false;
                //    builder.Font.Size = 10.5;
                //    builder.InsertCell();//插入单元格
                //    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(team_lens[i]);//列宽-百分比
                //    builder.CellFormat.Shading.BackgroundPatternColor = Color.White;//背景色-灰色
                //    builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;//对齐-居中
                //    builder.Write(team_titles[i]);//写入内容

                //}
                //builder.EndRow();//结束行
                //#endregion

                //#region team内容
                //var team_list = MakeTeamListData(pa_id);

                //for (int i = 0; i < team_list.Count; i++)
                //{
                //    DTcms.Model.ky_applyproject_team model = team_list[i];

                //    builder.Bold = false;
                //    builder.Font.Size = 10.5;
                //    builder.InsertCell();//插入单元格
                //    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(team_lens[0]);//列宽
                //    builder.CellFormat.Shading.BackgroundPatternColor = Color.White;//背景色-白色
                //    builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;//对齐-居中
                //    builder.Write(model.name);//写入内容

                //    builder.InsertCell();//插入单元格
                //    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(team_lens[1]);//列宽
                //    builder.CellFormat.Shading.BackgroundPatternColor = Color.White;//背景色-白色
                //    builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;//对齐-居中
                //    builder.Write(model.chusheng);//写入内容

                //    builder.InsertCell();//插入单元格
                //    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(team_lens[2]);//列宽
                //    builder.CellFormat.Shading.BackgroundPatternColor = Color.White;//背景色-白色
                //    builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;//对齐-居中
                //    builder.Write(model.sex);//写入内容

                //    builder.InsertCell();//插入单元格
                //    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(team_lens[3]);//列宽
                //    builder.CellFormat.Shading.BackgroundPatternColor = Color.White;//背景色-白色
                //    builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;//对齐-居中
                //    builder.Write(model.zhuanye);//写入内容

                //    builder.InsertCell();//插入单元格
                //    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(team_lens[4]);//列宽
                //    builder.CellFormat.Shading.BackgroundPatternColor = Color.White;//背景色-白色
                //    builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;//对齐-居中
                //    builder.Write(model.zhicheng);//写入内容

                //    builder.InsertCell();//插入单元格
                //    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(team_lens[5]);//列宽
                //    builder.CellFormat.Shading.BackgroundPatternColor = Color.White;//背景色-白色
                //    builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;//对齐-居中
                //    builder.Write(model.xueli);//写入内容

                //    builder.InsertCell();//插入单元格
                //    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(team_lens[6]);//列宽
                //    builder.CellFormat.Shading.BackgroundPatternColor = Color.White;//背景色-白色
                //    builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;//对齐-居中
                //    builder.Write(model.danwei);//写入内容

                //    builder.InsertCell();//插入单元格
                //    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(team_lens[7]);//列宽
                //    builder.CellFormat.Shading.BackgroundPatternColor = Color.White;//背景色-白色
                //    builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;//对齐-居中
                //    builder.Write(model.fengong);//写入内容

                //    builder.InsertCell();//插入单元格
                //    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(team_lens[8]);//列宽
                //    builder.CellFormat.Shading.BackgroundPatternColor = Color.White;//背景色-白色
                //    builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;//对齐-居中
                //    builder.Write("");//写入内容

                //    builder.EndRow();//结束行
                //}


                //#endregion


                //builder.EndTable();
                ////doc.Range.Bookmarks["table"].Text = "";
                #endregion



                #region 设备列表
                builder.MoveToBookmark("table_shebei");

                #region 表头
                string[] titles = new string[] { "序号", "设备名称", "型号", "价格（万元）", "增  添  理  由" };
                int[] lens = new int[] { 7, 27, 20, 20, 26 };
                for (int i = 0; i < 5; i++)
                {
                    builder.Bold = false;
                    builder.Font.Size = 10.5;
                    builder.InsertCell();//插入单元格
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(lens[i]);//列宽-百分比
                    builder.CellFormat.Shading.BackgroundPatternColor = Color.White;//背景色-灰色
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;//对齐-居中
                    builder.Write(titles[i]);//写入内容

                }
                builder.EndRow();//结束行
                #endregion

                #region 内容
                var list = MakeListData(pa_id);

                for (int i = 0; i < list.Count; i++)
                {
                    DTcms.Model.ky_applyproject_shebei model = list[i];

                    builder.Bold = false;
                    builder.Font.Size = 10.5;
                    builder.InsertCell();//插入单元格
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(lens[0]);//列宽
                    builder.CellFormat.Shading.BackgroundPatternColor = Color.White;//背景色-白色
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;//对齐-居中
                    builder.Write((i + 1).ToString());//写入内容

                    builder.InsertCell();//插入单元格
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(lens[1]);//列宽
                    builder.CellFormat.Shading.BackgroundPatternColor = Color.White;//背景色-白色
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;//对齐-靠左
                    builder.Write(model.shebei);//写入内容

                    builder.InsertCell();//插入单元格
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(lens[2]);//列宽
                    builder.CellFormat.Shading.BackgroundPatternColor = Color.White;//背景色-白色
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;//对齐-居中
                    builder.Write(model.xinghao);//写入内容

                    builder.InsertCell();//插入单元格
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(lens[3]);//列宽
                    builder.CellFormat.Shading.BackgroundPatternColor = Color.White;//背景色-白色
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;//对齐-靠右
                    builder.Write(model.jiage);//写入内容

                    builder.InsertCell();//插入单元格
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(lens[4]);//列宽
                    builder.CellFormat.Shading.BackgroundPatternColor = Color.White;//背景色-白色
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;//对齐-靠右
                    builder.Write(model.tianjia_liyou);//写入内容

                    builder.EndRow();//结束行
                }


                #endregion

                #endregion
                //


                var projecct_name = "/projectfile/" + "p_file_word_" + pa_id + ".doc";
                doc.Save(context.Server.MapPath(projecct_name)); //保存word

                doc.Save(context.Server.MapPath(projecct_name.Replace(".doc",".pdf")), Aspose.Words.SaveFormat.Pdf);

                res.ResultCode = 1;
                res.ResultMessage = projecct_name.Replace(".doc", ".pdf");
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }
        private void get(HttpContext context)
        {
            int pid = DTRequest.GetFormIntValue("pid", 0);


            JsonResult<string> res = new JsonResult<string>();

            BLL.ky_projects bll = new BLL.ky_projects();

            var model = bll.GetModel(pid);

            if (model != null)
            {
                var list = new BLL.ky_applyfee().GetModelList("p_id=" + pid + " and status=1 ");
                float result = 0;
                foreach (var item in list)
                {
                    var fee = float.Parse(item.fee);
                    result += fee;
                }



                res.ResultCode = 1;
                res.ResultMessage = (float.Parse(model.fee_zongji)- result).ToString();
            }
            else
            {
                res.ResultCode = 0;
                res.ResultMessage = "";
            }


          
            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }

        private void add_equipment(HttpContext context)
        {
            JsonResult<string> res = new JsonResult<string>();
            res.ResultCode = 0;
            res.ResultMessage = "error";

            int pa_id = DTRequest.GetFormIntValue("pa_id", 0);
            var shebei = DTRequest.GetFormString("shebei");
            var xinghao = DTRequest.GetFormString("xinghao");
            var jiage = DTRequest.GetFormString("jiage");
            var tianjia_liyou = DTRequest.GetFormString("tianjia_liyou");
            var add_user = DTRequest.GetFormString("add_user");

            Model.ky_applyproject_shebei model = new Model.ky_applyproject_shebei();
            model.shebei = shebei;
            model.xinghao = xinghao;
            model.jiage = jiage;
            model.tianjia_liyou = tianjia_liyou;

            model.pa_id = pa_id.ToString();
            model.add_time = System.DateTime.Now;
            model.add_user = add_user;

            var new_id = new BLL.ky_applyproject_shebei().Add(model);
            if (new_id > 0)
            {
                res.ResultCode = 1;
                res.ResultMessage = new_id.ToString();
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }

        private void edit_equipment(HttpContext context)
        {
            BLL.ky_applyproject_shebei bll = new BLL.ky_applyproject_shebei();
            JsonResult<string> res = new JsonResult<string>();
            res.ResultCode = 0;
            res.ResultMessage = "error";

            int id = DTRequest.GetFormIntValue("id", 0);
            var shebei = DTRequest.GetFormString("shebei");
            var xinghao = DTRequest.GetFormString("xinghao");
            var jiage = DTRequest.GetFormString("jiage");
            var tianjia_liyou = DTRequest.GetFormString("tianjia_liyou");
            var add_user = DTRequest.GetFormString("add_user");

            Model.ky_applyproject_shebei model = bll.GetModel(id);
            model.shebei = shebei;
            model.xinghao = xinghao;
            model.jiage = jiage;
            model.tianjia_liyou = tianjia_liyou;



            
            if (bll.Update(model))
            {
                res.ResultCode = 1;
                res.ResultMessage = "sucess";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }
        private void del_equipment(HttpContext context)
        {
            BLL.ky_applyproject_shebei bll = new BLL.ky_applyproject_shebei();
            JsonResult<string> res = new JsonResult<string>();
            res.ResultCode = 0;
            res.ResultMessage = "error";

            int id = DTRequest.GetFormIntValue("id", 0);
            var result = bll.Delete(id);
            if (result)
            {
                res.ResultCode = 1;
                res.ResultMessage = "sucess";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }







        private void add_projectteam(HttpContext context)
        {
            JsonResult<string> res = new JsonResult<string>();
            res.ResultCode = 0;
            res.ResultMessage = "error";

            int pa_id = DTRequest.GetFormIntValue("pa_id", 0);
            var name = DTRequest.GetFormString("name");
            var sex = DTRequest.GetFormString("sex");
            var chusheng = DTRequest.GetFormString("chusheng");
            var zhuanye = DTRequest.GetFormString("zhuanye");
            var zhicheng = DTRequest.GetFormString("zhicheng");
            var xueli = DTRequest.GetFormString("xueli");
            var danwei = DTRequest.GetFormString("danwei");
            var fengong = DTRequest.GetFormString("fengong");
            var add_user = DTRequest.GetFormString("add_user");

            Model.ky_applyproject_team model = new Model.ky_applyproject_team();
            model.name = name;
            model.sex = sex;
            model.chusheng = chusheng;
            model.zhuanye = zhuanye;
            model.zhicheng = zhicheng;
            model.xueli = xueli;
            model.danwei = danwei;
            model.fengong = fengong;

            model.pa_id = pa_id.ToString();
            model.add_time = System.DateTime.Now;
            model.add_user = add_user;

            var new_id = new BLL.ky_applyproject_team().Add(model);
            if (new_id > 0)
            {
                res.ResultCode = 1;
                res.ResultMessage = new_id.ToString();
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();

        }

        private void edit_rojectteam(HttpContext context)
        {

            BLL.ky_applyproject_team bll = new BLL.ky_applyproject_team();
            JsonResult<string> res = new JsonResult<string>();
            res.ResultCode = 0;
            res.ResultMessage = "error";

            int id = DTRequest.GetFormIntValue("id", 0);
            var name = DTRequest.GetFormString("name");
            var sex = DTRequest.GetFormString("sex");
            var chusheng = DTRequest.GetFormString("chusheng");
            var zhuanye = DTRequest.GetFormString("zhuanye");
            var zhicheng = DTRequest.GetFormString("zhicheng");
            var xueli = DTRequest.GetFormString("xueli");
            var danwei = DTRequest.GetFormString("danwei");
            var fengong = DTRequest.GetFormString("fengong");
            var add_user = DTRequest.GetFormString("add_user");

            Model.ky_applyproject_team model = bll.GetModel(id);// new Model.ky_applyproject_team();
            model.name = name;
            model.sex = sex;
            model.chusheng = chusheng;
            model.zhuanye = zhuanye;
            model.zhicheng = zhicheng;
            model.xueli = xueli;
            model.danwei = danwei;
            model.fengong = fengong;

           // model.pa_id = pa_id.ToString();
           // model.add_time = System.DateTime.Now;
           // model.add_user = add_user;

            var result = bll.Update(model);
            if (result)
            {
                res.ResultCode = 1;
                res.ResultMessage = "sucess";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }

        private void del_rojectteam(HttpContext context)
        {
            BLL.ky_applyproject_team bll = new BLL.ky_applyproject_team();
            JsonResult<string> res = new JsonResult<string>();
            res.ResultCode = 0;
            res.ResultMessage = "error";

            int id = DTRequest.GetFormIntValue("id", 0);
            var result = bll.Delete(id);
            if (result)
            {
                res.ResultCode = 1;
                res.ResultMessage = "sucess";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}