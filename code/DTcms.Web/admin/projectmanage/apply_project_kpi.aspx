﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="apply_project_kpi.aspx.cs" Inherits="DTcms.Web.admin.projectmanage.apply_project_kpi" %>

<%@ Import Namespace="DTcms.Common" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>项目绩效目标</title>
    <meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,initial-scale=1.0,user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <link rel="stylesheet" type="text/css" href="../../scripts/artdialog/ui-dialog.css" />
    <link rel="stylesheet" type="text/css" href="../skin/icon/iconfont.css" />
    <link rel="stylesheet" type="text/css" href="../skin/default/style.css" />
    <style>
        .tdcenter {
            text-align: center;
        }
        .wordblod {
        font-weight: bold;
        }
    </style>
    <script type="text/javascript" charset="utf-8" src="../../scripts/jquery/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../scripts/jquery/Validform_v5.3.2_min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../scripts/datepicker/WdatePicker.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../scripts/artdialog/dialog-plus-min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../scripts/webuploader/webuploader.min.js"></script>

    <script type="text/javascript" charset="utf-8" src="../../editor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../editor/ueditor.all.min.js"> </script>
    <script type="text/javascript" charset="utf-8" src="../../editor/lang/zh-cn/zh-cn.js"></script>

    <script type="text/javascript" charset="utf-8" src="../js/uploader.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/common.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/laymain.js"></script>
    <script type="text/javascript">
        $(function () {
            //初始化表单验证
            $("#form1").initValidform();
            //初始化上传控件
            $(".upload-img").InitUploader({
                filesize: "<%=sysConfig.attachsize %>", sendurl: "../../tools/upload_ajax.ashx", swf: "../../scripts/webuploader/uploader.swf", filetypes: "<%=sysConfig.fileextension %>"
            });

            //初始化editor
            $(".editor-mini").each(function (i) {
                var objId = $(this).attr("id");
                if (objId != "undefined") {
                    var editorMini = UE.getEditor(objId, {
                        serverUrl: '../../../tools/upload_ajax.ashx',
                        initialFrameWidth: '100%',
                        initialFrameHeight: 220,
                        toolbars: [[
                            'fullscreen', 'source', '|', 'undo', 'redo', '|',
                            'bold', 'italic', 'underline', 'strikethrough', 'removeformat', 'pasteplain', '|', 'forecolor', 'insertorderedlist', 'insertunorderedlist', '|',
                            'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|',
                            'link', 'unlink', 'anchor', '|',
                            'simpleupload', 'insertimage', 'scrawl', 'insertvideo'
                        ]]
                    });
                }
            });

            //创建上传附件
            $(".attach-btn").click(function () {
                showAttachDialog();
            });

        });


        //初始化附件窗口
        function showAttachDialog(obj) {
            var objNum = arguments.length;
            var attachDialog = top.dialog({
                id: 'attachDialogId',
                title: "上传附件",
                url: 'projectmanage/dialog_attach.aspx',
                width: 500,
                height: 180,
                onclose: function () {
                    var liHtml = this.returnValue; //获取返回值
                    if (liHtml.length > 0) {
                        $("#showAttachList").children("ul").append(liHtml);
                    }
                }
            }).showModal();
            //如果是修改状态，将对象传进去
            if (objNum == 1) {
                attachDialog.data = obj;
            }
        }
        //删除附件节点
        function delAttachNode(obj) {
            $(obj).parent().remove();
        }

    </script>
</head>

<body class="mainbody">
    <form id="form1" runat="server">
        <!--导航栏-->
        <div class="location">
            <a href="manager_list.aspx" class="back"><i class="iconfont icon-up"></i><span>返回列表页</span></a>
            <a href="../center.aspx"><i class="iconfont icon-home"></i><span>首页</span></a>
            <i class="arrow iconfont icon-arrow-right"></i>
            <a href="doc_list.aspx"><span>申报管理</span></a>
            <i class="arrow iconfont icon-arrow-right"></i>
            <span>项目绩效目标</span>
        </div>
        <div class="line10"></div>
        <!--/导航栏-->

        <!--内容-->
        <div id="floatHead" class="content-tab-wrap">
            <div class="content-tab">
                <div class="content-tab-ul-wrap">
                    <ul>
                        <li><a class="selected" href="javascript:;">项目绩效目标</a></li>
                      
                    </ul>
                </div>
            </div>
        </div>

        <div class="tab-content">

            <table  width="100%" border="0" cellspacing="0" cellpadding="0" class="ltable">
                       
                <tr>
                    <td class="tdcenter wordblod" style="width:15%" >一级指标类别</td><td class="tdcenter wordblod" style="width:15%">二级指标类别 </td><td class="tdcenter wordblod" style="width:15%">明细指标</td><td class="tdcenter wordblod">预期绩效目标
</td>
                </tr>
                <tr>
                    <td class="tdcenter wordblod" rowspan="39">产出类指标</td><td class="tdcenter wordblod" rowspan="19">知识产权 </td><td class="wordblod">1、专利申请数（项）
</td><td class="tdcenter"><input id="txt_zhuanli_shuliang" runat="server" type="text" class="input normal"  />
</td>
                </tr>
                <tr>
                     <td class="wordblod">（1）申请发明专利</td>
                  
                    <td class="tdcenter"><input id="txt_zhuanli_shuliang_faming" runat="server" type="text" class="input normal"  /></td>
                </tr>
                 <tr>
                     <td class="wordblod">（2）实用新型</td>
                  
                    <td class="tdcenter"><input id="txt_zhuanli_shuliang_shiyongxinxing" runat="server" type="text" class="input normal"  /></td>
                </tr>
                <tr>
                     <td class="wordblod">（3）外观设计</td>
                  
                    <td class="tdcenter"><input id="txt_zhuanli_shuliang_waiguan" runat="server" type="text" class="input normal"  /></td>
                </tr>
                 <tr>
                     <td class="wordblod">2、专利授权数（项）</td>
                  
                    <td class="tdcenter"><input id="txt_zhuanli_shouquan_shuliang" runat="server" type="text" class="input normal"  /></td>
                </tr>
                <tr>
                     <td class="wordblod">（1）授权发明专利</td>
                  
                    <td class="tdcenter"><input id="txt_zhuanli_shouquan_faming" runat="server" type="text" class="input normal"  /></td>
                </tr>
                 <tr>
                     <td class="wordblod">（2）实用新型</td>
                  
                    <td class="tdcenter"><input id="txt_zhuanli_shouquan_shiyongxinxing" runat="server" type="text" class="input normal"  /></td>
                </tr>
                <tr>
                     <td class="wordblod">（3）外观设计</td>
                  
                    <td class="tdcenter"><input id="txt_zhuanli_shouquan_waiguan" runat="server" type="text" class="input normal"  /></td>
                </tr>
                 <tr>
                     <td class="wordblod">3、软件著作权授权数（项）</td>
                  
                    <td class="tdcenter"><input id="txt_ruanzhu_shuliang" runat="server" type="text" class="input normal"  /></td>
                </tr>
                 <tr>
                     <td class="wordblod">4、发表论文（篇）</td>
                  
                    <td class="tdcenter"><input id="txt_lunwen_shuliang" runat="server" type="text" class="input normal"  /></td>
                </tr>
                <tr>
                     <td class="wordblod">（1）其中 SCI 索引收录数</td>
                  
                    <td class="tdcenter"><input id="txt_lunwen_shuliang_sci" runat="server" type="text" class="input normal"  /></td>
                </tr>
                <tr>
                     <td class="wordblod">（2）其中 EI 索引收录数</td>
                  
                    <td class="tdcenter"><input id="txt_lunwen_shuliang_ei" runat="server" type="text" class="input normal"  /></td>
                </tr>
                <tr>
                     <td class="wordblod">5、著作（部）</td>
                  
                    <td class="tdcenter"><input id="txt_zhuzuo_shuliang" runat="server" type="text" class="input normal"  /></td>
                </tr>
                <tr>
                     <td class="wordblod">6、制订标准数（项）</td>
                  
                    <td class="tdcenter"><input id="txt_biaozhun_shuliang" runat="server" type="text" class="input normal"  /></td>
                </tr>
                <tr>
                     <td class="wordblod">（1）国际标准</td>
                  
                    <td class="tdcenter"><input id="txt_biaozhun_shuliang_guoji" runat="server" type="text" class="input normal"  /></td>
                </tr>
                 <tr>
                     <td class="wordblod">（2）国家标准</td>
                  
                    <td class="tdcenter"><input id="txt_biaozhun_shuliang_guojia" runat="server" type="text" class="input normal"  /></td>
                </tr>
                 <tr>
                     <td class="wordblod">（3）行业标准</td>
                  
                    <td class="tdcenter"><input id="txt_biaozhun_shuliang_hangye" runat="server" type="text" class="input normal"  /></td>
                </tr>
                 <tr>
                     <td class="wordblod">（4）地方标准</td>
                  
                    <td class="tdcenter"><input id="txt_biaozhun_shuliang_difang" runat="server" type="text" class="input normal"  /></td>
                </tr>
                <tr>
                     <td class="wordblod">（5）企业标准</td>
                  
                    <td class="tdcenter"><input id="txt_biaozhun_shuliang_qiye" runat="server" type="text" class="input normal"  /></td>
                </tr>
                <tr>
                    <td class="wordblod" rowspan="14">其他成果</td>
                     <td class="wordblod">1、填补技术空白数</td>
                  
                    <td class="tdcenter"><input id="txt_qita_tianbujishukongbai_shuliang" runat="server" type="text" class="input normal"  /></td>
                </tr>
                 <tr>
                     <td class="wordblod">（1）国际</td>
                  
                    <td class="tdcenter"><input id="txt_qita_tianbujishukongbai_shuliang_guoji" runat="server" type="text" class="input normal"  /></td>
                </tr>
                 <tr>
                     <td class="wordblod">（2）国家</td>
                  
                    <td class="tdcenter"><input id="txt_qita_tianbujishukongbai_shuliang_guojia" runat="server" type="text" class="input normal"  /></td>
                </tr>
                 <tr>
                     <td class="wordblod">（3）省级</td>
                  
                    <td class="tdcenter"><input id="txt_qita_tianbujishukongbai_shuliang_shengji" runat="server" type="text" class="input normal"  /></td>
                </tr>
                 <tr>
                     <td class="wordblod">4、获奖项数</td>
                  
                    <td class="tdcenter"><input id="txt_qita_huojiang_shuliang" runat="server" type="text" class="input normal"  /></td>
                </tr>
                 <tr>
                     <td class="wordblod">（1）国家奖项</td>
                  
                    <td class="tdcenter"><input id="txt_qita_huojiang_shuliang_guojia" runat="server" type="text" class="input normal"  /></td>
                </tr>
                 <tr>
                     <td class="wordblod">（2）部、省奖项</td>
                  
                    <td class="tdcenter"><input id="txt_qita_huojiang_shuliang_sheng" runat="server" type="text" class="input normal"  /></td>
                </tr>
                 <tr>
                     <td class="wordblod">（3）地市级奖项</td>
                  
                    <td class="tdcenter"><input id="txt_qita_huojiang_shuliang_difang" runat="server" type="text" class="input normal"  /></td>
                </tr>
                 <tr>
                     <td class="wordblod">5、其他科技成果产出</td>
                  
                    <td class="tdcenter"><input id="txt_qita_qita_kejichengguo" runat="server" type="text" class="input normal"  /></td>
                </tr>
                 <tr>
                     <td class="wordblod">（1）新工艺（或新方法模式）</td>
                  
                    <td class="tdcenter"><input id="txt_qita_qita_xingongyi" runat="server" type="text" class="input normal"  /></td>
                </tr>
                 <tr>
                     <td class="wordblod">（2）新产品(含农业新品种)</td>
                  
                    <td class="tdcenter"><input id="txt_qita_qita_xinchanping" runat="server" type="text" class="input normal"  /></td>
                </tr>
                 <tr>
                     <td class="wordblod">（3）新材料</td>
                  
                    <td class="tdcenter"><input id="txt_qita_qita_xincailiao" runat="server" type="text" class="input normal"  /></td>
                </tr>
                 <tr>
                     <td class="wordblod">（4）新装备（装置）</td>
                  
                    <td class="tdcenter"><input id="txt_qita_qita_xinzhuangbei" runat="server" type="text" class="input normal"  /></td>
                </tr>
                 <tr>
                     <td class="wordblod">（5）平台/基地/示范点</td>
                  
                    <td class="tdcenter"><input id="txt_qita_qita_pingtai" runat="server" type="text" class="input normal"  /></td>
                </tr>
                 <tr>
                      <td class="wordblod" rowspan="6">人才引育</td>
                     <td class="wordblod">1、引进高层次人才</td>
                  
                    <td class="tdcenter"><input id="txt_rencai_yj_shuliang" runat="server" type="text" class="input normal"  /></td>
                </tr>
                 <tr>
                     <td class="wordblod">（1）博士、博士后</td>
                  
                    <td class="tdcenter"><input id="txt_rencai_yj_shuliang_boshi" runat="server" type="text" class="input normal"  /></td>
                </tr>
                 <tr>
                     <td class="wordblod">（2）硕士</td>
                  
                    <td class="tdcenter"><input id="txt_rencai_yj_shuliang_shuoshi" runat="server" type="text" class="input normal"  /></td>
                </tr>
                 <tr>
                     <td class="wordblod">2、培养高层次人才</td>
                  
                    <td class="tdcenter"><input id="txt_rencai_py_shuliang" runat="server" type="text" class="input normal"  /></td>
                </tr>
                 <tr>
                     <td class="wordblod">（1）博士、博士后</td>
                  
                    <td class="tdcenter"><input id="txt_rencai_py_shuliang_boshi" runat="server" type="text" class="input normal"  /></td>
                </tr>
                 <tr>
                     <td class="wordblod">（2）硕士</td>
                  
                    <td class="tdcenter"><input id="txt_rencai_py_shuliang_shuoshi" runat="server" type="text" class="input normal"  /></td>
                </tr>
                 <tr>
                      <td class="wordblod" rowspan="2">效果类指标</td>
                      <td class="wordblod">经济效益</td>
                     <td class="wordblod">新增收入（万元）</td>
                  
                    <td class="tdcenter"><input id="txt_jingjixiaoyi" runat="server" type="text" class="input normal"  /></td>
                </tr>
                 <tr>
                     <td class="wordblod" >社会效益</td>
                  
                    <td class="tdcenter" colspan="2"><input id="txt_shehuixiaoyi" runat="server" type="text" class="input normal"  /></td>
                </tr>
                <tr>
                    <td class="wordblod" colspan="2">其他需要说明的情况</td>
                    <td class="tdcenter" colspan="2"><textarea id="txtarea_qitashuoming" runat="server" class="input"></textarea></td>
                </tr>
                </table>

            

        </div>
        

        <!--/内容-->

        <!--工具栏-->
        <div class="page-footer">
            <div class="btn-wrap">
                <asp:Button ID="btnSubmit" runat="server" Text="提交保存" CssClass="btn" OnClick="btnSubmit_Click" />
                <input name="btnReturn" type="button" value="返回上一页" class="btn yellow" onclick="javascript: history.back(-1);" />
            </div>
        </div>
        <!--/工具栏-->
        <asp:HiddenField ID="hd_pa_id" runat="server"  />
        <asp:HiddenField ID="hd_add_user" runat="server" />
    </form>
</body>
</html>

