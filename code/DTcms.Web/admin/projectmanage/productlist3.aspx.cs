﻿using DTcms.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DTcms.Web.admin.projectmanage
{
    public partial class productlist3 : Web.UI.ManagePage
    {
        protected int totalCount;
        protected int page;
        protected int pageSize;

        BLL.ky_products_zhuanli bll = new BLL.ky_products_zhuanli();
        protected string keywords = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {

            this.keywords = DTRequest.GetQueryString("keywords");

            this.pageSize = GetPageSize(10); //每页数量
            if (!Page.IsPostBack)
            {
                ChkAdminLevel("productlist3", DTEnums.ActionEnum.View.ToString()); //检查权限
                Model.manager model = GetAdminInfo(); //取得当前管理员信息
                InitData();                                    
                RptBind(" 1=1 " + CombSqlTxt(keywords), "add_time desc");
            }
        }

        #region 初始化===================================

        void InitData()
        {
            var hospatil = new BLL.yjr_doc_hospital().GetModelList("");
            foreach (var item in hospatil)
            {
                ddldepartment.Items.Add(new ListItem(item.hospitalname, item.id.ToString()));
            }

            var zhuanli_leibie = new BLL.ky_dictionary().GetModelListByCache("zhuanli_leibie");
            foreach (var item in zhuanli_leibie)
            {
                ddlzhuanli_leibie.Items.Add(new ListItem(item.dic_text, item.dic_value));
            }




        }
        #endregion

        #region 数据绑定=======================================
        private void RptBind(string _strWhere, string _orderby)
        {
            this.page = DTRequest.GetQueryInt("page", 1);
            txtKeywords.Text = this.keywords;

            this.rptList.DataSource = bll.GetList(this.pageSize, this.page, _strWhere, _orderby, out this.totalCount);
            this.rptList.DataBind();

            //绑定页码
            txtPageNum.Text = this.pageSize.ToString();
            string pageUrl = Utils.CombUrlTxt("productlist3.aspx", "keywords={0}&page={1}", this.keywords, "__id__");
            PageContent.InnerHtml = Utils.OutPageList(this.pageSize, this.page, this.totalCount, pageUrl, 8);
        }
        #endregion

        #region 组合SQL查询语句==========================
        protected string CombSqlTxt(string _keywords)
        {
            StringBuilder strTemp = new StringBuilder();
            _keywords = _keywords.Replace("'", "");
            if (!string.IsNullOrEmpty(_keywords))
            {
                strTemp.Append(" and (zhuanli_mingcheng like  '%" + _keywords + "%' or fa_ming_ren like  '%" + _keywords + "%' )");
            }

            //专利类别
            if (ddlzhuanli_leibie.SelectedValue != "0")
            {
                strTemp.Append(" and zhuanli_leibie='" + ddlzhuanli_leibie.SelectedValue + "'");
            }
            //科室
            if (ddldepartment.SelectedValue != "0")
            {
                strTemp.Append(" and dptid=" + ddldepartment.SelectedValue);
            }
            return strTemp.ToString();
        }
        #endregion

        #region 返回每页数量=============================
        private int GetPageSize(int _default_size)
        {
            int _pagesize;
            if (int.TryParse(Utils.GetCookie("productlist3_page_size", "DTcmsPage"), out _pagesize))
            {
                if (_pagesize > 0)
                {
                    return _pagesize;
                }
            }
            return _default_size;
        }
        #endregion

        //关健字查询
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect(Utils.CombUrlTxt("productlist3.aspx", "keywords={0}", txtKeywords.Text));
        }

        //设置分页数量
        protected void txtPageNum_TextChanged(object sender, EventArgs e)
        {
            int _pagesize;
            if (int.TryParse(txtPageNum.Text.Trim(), out _pagesize))
            {
                if (_pagesize > 0)
                {
                    Utils.WriteCookie("productlist3_page_size", "DTcmsPage", _pagesize.ToString(), 14400);
                }
            }
            Response.Redirect(Utils.CombUrlTxt("productlist3.aspx", "keywords={0}", this.keywords));
        }

        protected string GetStatus(string status)
        {
            if (status == "1")
            {
                return "通过";
            }
            else if (status == "-1")
            {
                return "未通过";
            }
            else
            {
                return "--";
            }
        }

        protected void ddlzhuanli_leibie_SelectedIndexChanged(object sender, EventArgs e)
        {
            //专类类别
            RptBind("1=1 " + CombSqlTxt(keywords), "add_time desc");
        }

        protected void ddldepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            // 按部门查询
            RptBind("1=1 " + CombSqlTxt(keywords), "add_time desc");
        }
    }
}