﻿using DTcms.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DTcms.Web.admin.projectmanage
{
    public partial class apply_projectlist : Web.UI.ManagePage
    {
        protected int totalCount;
        protected int page;
        protected int pageSize;
        BLL.ky_applyproject bll = new BLL.ky_applyproject();
        protected string keywords = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {

            this.keywords = DTRequest.GetQueryString("keywords");

            this.pageSize = GetPageSize(10); //每页数量
            if (!Page.IsPostBack)
            {
                ChkAdminLevel("apply_projectlist", DTEnums.ActionEnum.View.ToString()); //检查权限
                Model.manager model = GetAdminInfo(); //取得当前管理员信息
                //RptBind("role_type>=" + model.role_type + CombSqlTxt(keywords), "add_time asc,id desc");
                InitData();

                RptBind("1=1 " + CombSqlTxt(keywords), "add_time desc");
            }
        }

        #region 初始化===================================

        void InitData()
        {
            var hospatil = new BLL.yjr_doc_hospital().GetModelList("");
            foreach (var item in hospatil)
            {
                ddldepartment.Items.Add(new ListItem(item.hospitalname, item.id.ToString()));
            }

            //项目类型？？ 与立项项目类型？？或单独建立
            //var projecttype = new BLL.ky_dictionary().GetModelListByCache("project_type");
            //foreach (var item in projecttype)
            //{
            //    ddlprojecttype.Items.Add(new ListItem(item.dic_text, item.dic_value));
            //}

            var project_type = new BLL.ky_dictionary().GetModelListByCache("project_category");
            foreach (var item in project_type)
            {
                ddlprojectcategory.Items.Add(new ListItem(item.dic_text, item.dic_value.ToString()));
            }

        }
        #endregion

        #region 数据绑定=======================================
        private void RptBind(string _strWhere, string _orderby)
        {
            this.page = DTRequest.GetQueryInt("page", 1);
            txtKeywords.Text = this.keywords;

            this.rptList.DataSource = bll.GetList(this.pageSize, this.page, _strWhere, _orderby, out this.totalCount);
            this.rptList.DataBind();

            //绑定页码
            txtPageNum.Text = this.pageSize.ToString();
            string pageUrl = Utils.CombUrlTxt("apply_projectlist.aspx", "keywords={0}&page={1}", this.keywords, "__id__");
            PageContent.InnerHtml = Utils.OutPageList(this.pageSize, this.page, this.totalCount, pageUrl, 8);
        }
        #endregion

        #region 组合SQL查询语句==========================
        protected string CombSqlTxt(string _keywords)
        {
            StringBuilder strTemp = new StringBuilder();
            _keywords = _keywords.Replace("'", "");
            if (!string.IsNullOrEmpty(_keywords))
            {
                strTemp.Append(" and (pa_title like  '%" + _keywords + "%' or pa_user like '%" + _keywords + "%' )");
            }

            if (ddlProperty.SelectedValue != "0")
            {
                strTemp.Append(" and status=" + ddlProperty.SelectedValue);
            }

            if (ddldepartment.SelectedValue != "0")
            {
                strTemp.Append(" and pa_dptid=" + ddldepartment.SelectedValue);
            }

            if (ddlprojecttype.SelectedValue != "0")
            {
                strTemp.Append(" and pa_type=" + ddlprojecttype.SelectedValue);
            }

            if (ddlprojectcategory.SelectedValue != "0")
            {
                strTemp.Append(" and pa_type=" + ddlprojectcategory.SelectedValue);
            }

            return strTemp.ToString();
        }
        #endregion

        #region 返回每页数量=============================
        private int GetPageSize(int _default_size)
        {
            int _pagesize;
            if (int.TryParse(Utils.GetCookie("apply_projectlist_page_size", "DTcmsPage"), out _pagesize))
            {
                if (_pagesize > 0)
                {
                    return _pagesize;
                }
            }
            return _default_size;
        }
        #endregion

        //关健字查询
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect(Utils.CombUrlTxt("apply_projectlist.aspx", "keywords={0}", txtKeywords.Text));
        }

        //设置分页数量
        protected void txtPageNum_TextChanged(object sender, EventArgs e)
        {
            int _pagesize;
            if (int.TryParse(txtPageNum.Text.Trim(), out _pagesize))
            {
                if (_pagesize > 0)
                {
                    Utils.WriteCookie("apply_projectlist_page_size", "DTcmsPage", _pagesize.ToString(), 14400);
                }
            }
            Response.Redirect(Utils.CombUrlTxt("apply_projectlist.aspx", "keywords={0}", this.keywords));
        }

        protected void ddlProperty_SelectedIndexChanged(object sender, EventArgs e)
        {
            // 按状态查询
            RptBind("1=1 " + CombSqlTxt(keywords), "add_time desc");
        }

        protected void ddldepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            // 按部门查询
            RptBind("1=1 " + CombSqlTxt(keywords), "add_time desc");
        }

        protected void ddlprojecttype_SelectedIndexChanged(object sender, EventArgs e)
        {
            //按项目类型查询
            RptBind("1=1 " + CombSqlTxt(keywords), "add_time desc");
        }

        protected void ddlprojectcategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            //按项目类型查询
            RptBind("1=1 " + CombSqlTxt(keywords), "add_time desc");
        }


    }
}