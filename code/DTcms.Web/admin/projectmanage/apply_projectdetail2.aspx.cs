﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DTcms.Common;

namespace DTcms.Web.admin.projectmanage
{
    public partial class apply_projectdetail2 : Web.UI.ManagePage
    {
        private string action = DTEnums.ActionEnum.Add.ToString(); //操作类型
        private int id = 0;

        BLL.ky_applyproject Ky_Projects_bll = new BLL.ky_applyproject();

        BLL.ky_attachment attachment_bll = new BLL.ky_attachment();
        protected void Page_Load(object sender, EventArgs e)
        {
            string _action = DTRequest.GetQueryString("action");
           
                this.action = DTEnums.ActionEnum.View.ToString();//修改类型
                if (!int.TryParse(Request.QueryString["id"] as string, out this.id))
                {
                    JscriptMsg("传输参数不正确！", "back");
                    return;
                }
                if (!Ky_Projects_bll.Exists(this.id))
                {
                    JscriptMsg("记录不存在或已被删除！", "back");
                    return;
                }
            

            if (!IsPostBack)
            {
                ChkAdminLevel("apply_projectlist", DTEnums.ActionEnum.View.ToString()); //检查权限
                                                                                        // Model.manager model = GetAdminInfo(); //取得管理员信息
                InitData();
              
                ShowInfo(this.id);
               
            }
        }

        #region 初始化===================================

        void InitData()
        {
            var hospatil = new BLL.yjr_doc_hospital().GetModelList("");
            foreach (var item in hospatil)
            {
                ddlhospital.Items.Add(new ListItem(item.hospitalname, item.id.ToString()));
            }

            //var project_type = new BLL.ky_dictionary().GetModelListByCache("project_type");
            //foreach (var item in project_type)
            //{
            //    ddl_projecttype.Items.Add(new ListItem(item.dic_text, item.dic_value.ToString()));
            //}

            var project_type = new BLL.ky_dictionary().GetModelListByCache("project_category");
            foreach (var item in project_type)
            {
                ddl_projecttype.Items.Add(new ListItem(item.dic_text, item.dic_value.ToString()));
            }

            var project_yanjiu_xingshi = new BLL.ky_dictionary().GetModelListByCache("yanjiu_xingshi");
            foreach (var item in project_yanjiu_xingshi)
            {
                ddl_base_yanjiu_xingshi.Items.Add(new ListItem(item.dic_text, item.dic_value.ToString()));
            }

            txt_owner.Text = GetAdminInfo().real_name;
        }
        #endregion

        #region 赋值操作=================================
        private void ShowInfo(int _id)
        {
            var model = Ky_Projects_bll.GetModel(_id);
            txt_title.Text = model.pa_title;
            txt_number.Text = model.pa_code;
            ddl_projecttype.SelectedValue = model.pa_type;
            ddlhospital.SelectedValue = model.pa_dptid;
            txt_owner.Text = model.pa_user;


            txt_dian_zi_you_xiang.Text = model.dian_zi_you_xiang;
            txt_shou_ji_hao_ma.Text = model.shou_ji_hao_ma;
            txt_shen_qing_ri_qi.Text = model.shen_qing_ri_qi;

            txt_base_didian.Text = model.base_didian;
            ddl_base_yanjiu_xingshi.SelectedValue = model.base_yanjiu_xingshi;

            txt_base_qizhishijian.Text = model.base_qizhishijian.Split('|')[0];
            txt_base_qizhishijian_end.Text = model.base_qizhishijian.Split('|').Length > 1 ? model.base_qizhishijian.Split('|')[1] : "";


            txt_base_hezuo_danwei.Text = model.base_hezuo_danwei;
            txt_base_hezuo_dizhi.Text = model.base_hezuo_dizhi;
            txt_base_hezuo_lianxiren.Text = model.base_hezuo_lianxiren;
            txt_base_hezuo_lianxidianhua.Text = model.base_hezuo_lianxidianhua;

            txt_base_xmgk_zhaiyao.Text = model.base_xmgk_zhaiyao;
            txt_base_xmgk_neirongmubiao.Text = model.base_xmgk_neirongmubiao;
            txt_base_xmgk_jishuluxian.Text = model.base_xmgk_jishuluxian;
            txt_base_xmgk_chuangxindian.Text = model.base_xmgk_chuangxindian;
            txt_base_xmgk_jichutiaojian.Text = model.base_xmgk_jichutiaojian;
            txt_base_xmgk_xiaoyi.Text = model.base_xmgk_xiaoyi;
            txt_base_xmgk_zhibiao.Text = model.base_xmgk_zhibiao;

            ////绑定内容附件
            //rptAttachList.DataSource = attachment_bll.GetModelList("apply_id='applyproject_" + this.id + "' "); ;//
            //rptAttachList.DataBind();

        }
        #endregion

       

        protected void ddlhospital_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }
    }
}