﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DTcms.Common;

namespace DTcms.Web.admin.projectmanage
{
    public partial class applyfeeaudit : Web.UI.ManagePage
    {
        private string action = DTEnums.ActionEnum.Add.ToString(); //操作类型
        private int id = 0;
        private int p_id = 0;

        BLL.ky_projects Ky_Projects_bll = new BLL.ky_projects();
        BLL.ky_applyfee applyrecord_bll = new BLL.ky_applyfee();
        BLL.ky_attachment attachment_bll = new BLL.ky_attachment();
        protected void Page_Load(object sender, EventArgs e)
        {
            string _action = DTRequest.GetQueryString("action");


            if (!int.TryParse(Request.QueryString["pid"] as string, out this.p_id))
            {
                JscriptMsg("传输参数不正确！", "back");
                return;
            }
            if (!new BLL.ky_projects().Exists(this.p_id))
            {
                JscriptMsg("记录不存在或已被删除！", "back");
                return;
            }


            if (!int.TryParse(Request.QueryString["id"] as string, out this.id))
            {
                JscriptMsg("传输参数不正确！", "back");
                return;
            }
            if (!new BLL.ky_applyfee().Exists(this.id))
            {
                JscriptMsg("记录不存在或已被删除！", "back");
                return;
            }
            action = _action;

            if (!IsPostBack)
            {
                ChkAdminLevel("applylist", DTEnums.ActionEnum.View.ToString()); //检查权限
                Model.manager model = GetAdminInfo(); //取得管理员信息
                InitData();
                //if (action == DTEnums.ActionEnum.Edit.ToString()) //修改
                //{
                ShowInfo(this.p_id);
                //}

                if (action == DTEnums.ActionEnum.Audit.ToString()) //审批
                {
                    ShowApplyInfo(this.id);
                }
            }
        }

        #region 初始化===================================

        void InitData()
        {
            var hospatil = new BLL.yjr_doc_hospital().GetModelList("");
            foreach (var item in hospatil)
            {
                ddlhospital.Items.Add(new ListItem(item.hospitalname, item.id.ToString()));
            }

            var fee_type = new BLL.ky_dictionary().GetModelListByCache("fee_type");
            foreach (var item in fee_type)
            {
                ddlfee_type.Items.Add(new ListItem(item.dic_text, item.dic_value.ToString()));

            }
        }
        #endregion

        #region 赋值操作=================================
        private void ShowInfo(int _id)
        {
            var model = Ky_Projects_bll.GetModel(_id);
            txt_number.Text = model.project_number;
            txt_title.Text = model.project_title;
            ddlhospital.SelectedValue = model.dptid.ToString();
            txt_owner.Text = model.owner;
            txt_tel.Text = model.tel;
            txt_start_time.Text = model.start_time;
            txt_end_time.Text = model.end_time;
            ddl_projecttype.SelectedValue = model.project_type.ToString();
            txt_fee_xiada.Text = model.fee_xiada;
            txt_fee_peitao.Text = model.fee_peitao;
            txt_fee_zongji.Text = model.fee_zongji;
            //txt_member.Text = model.member;

           

        }

        private void ShowApplyInfo(int _id)
        {
            var applyfee = applyrecord_bll.GetModel(this.id);
            txt_intro.Text = applyfee.reason;
            txtapplyfee.Text = applyfee.fee;
            ddlfee_type.SelectedValue = applyfee.fee_type;
            //绑定内容附件
            rptAttachList.DataSource = attachment_bll.GetModelList("apply_id='" + "ky_applyfee" + this.id + "' "); ;//  model.attach; //获取附件
            rptAttachList.DataBind();
        }
        #endregion

        //#region 增加操作=================================
        //private bool DoAdd()
        //{
        //    DTcms.Model.ky_applyrecord model_add = new Model.ky_applyrecord();
        //    model_add.p_id = this.p_id;
        //    model_add.add_time = System.DateTime.Now;
        //    model_add.add_user = GetAdminInfo().user_name.ToString();// "";// 登录用户
        //    model_add.remark = txt_intro.Text;

        //    var id = applyrecord_bll.Add(model_add);
        //    if (id > 0)
        //    {

        //        //
        //        #region 保存附件====================
        //        //保存附件
        //        string[] attachFileNameArr = Request.Form.GetValues("hid_attach_filename");
        //        string[] attachFilePathArr = Request.Form.GetValues("hid_attach_filepath");
        //        string[] attachFileSizeArr = Request.Form.GetValues("hid_attach_filesize");
        //        string[] attachPointArr = Request.Form.GetValues("txt_attach_point");
        //        if (attachFileNameArr != null && attachFilePathArr != null && attachFileSizeArr != null
        //            && attachFileNameArr.Length > 0 && attachFilePathArr.Length > 0 && attachFileSizeArr.Length > 0)
        //        {
        //            List<Model.article_attach> ls = new List<Model.article_attach>();
        //            for (int i = 0; i < attachFileNameArr.Length; i++)
        //            {
        //                int fileSize = Utils.StrToInt(attachFileSizeArr[i], 0);
        //                string fileExt = FileHelper.GetFileExt(attachFilePathArr[i]);
        //                //int _point = Utils.StrToInt(attachPointArr[i], 0);
        //                //ls.Add(new Model.article_attach { channel_id = this.channel_id, file_name = attachFileNameArr[i], file_path = attachFilePathArr[i], file_size = fileSize, file_ext = fileExt, point = _point });

        //                //保存附件数据
        //                DTcms.Model.ky_attachment _Attachment = new Model.ky_attachment();
        //                _Attachment.file_name = attachFileNameArr[i];
        //                _Attachment.file_path = attachFilePathArr[i];
        //                _Attachment.file_ext = fileExt;
        //                _Attachment.file_size = fileSize;
        //                _Attachment.p_id = this.p_id;
        //                _Attachment.add_time = System.DateTime.Now;
        //                _Attachment.apply_id = id.ToString();

        //                attachment_bll.Add(_Attachment);
        //            }

        //        }
        //        #endregion

        //        //
        //        AddAdminLog(DTEnums.ActionEnum.Add.ToString(), "添加中检申请:" + id); //记录日志
        //        return true;
        //    }
        //    return false;
        //}
        //#endregion

        #region 修改操作=================================
        private bool DoEdit(int _id)
        {
            bool result = false;

            DTcms.Model.ky_applyfee model_edit = applyrecord_bll.GetModel(_id);

            model_edit.status = int.Parse(ddlresult.SelectedValue);
            model_edit.audit_1_status = int.Parse(ddlresult.SelectedValue);
            model_edit.audit_1_time = System.DateTime.Now;
            model_edit.audit_1_user = GetAdminInfo().user_name;

            if (applyrecord_bll.Update(model_edit))
            {



                AddAdminLog(DTEnums.ActionEnum.Add.ToString(), "审批经费申请:" + model_edit.applyfee_id); //记录日志
                result = true;
            }

            return result;
        }
        #endregion

        //保存
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (action == DTEnums.ActionEnum.Audit.ToString()) //修改
            {
                ChkAdminLevel("applylist", DTEnums.ActionEnum.Audit.ToString()); //检查权限
                if (!DoEdit(this.id))
                {
                    JscriptMsg("保存过程中发生错误！", "");
                    return;
                }
                JscriptMsg("修改信息成功！", "applylist.aspx");
            }
            else //添加
            {

                JscriptMsg("保存过程中发生错误！", "");
                return;

            }
        }



        protected void ddlhospital_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}