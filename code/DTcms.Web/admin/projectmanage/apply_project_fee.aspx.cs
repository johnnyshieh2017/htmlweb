﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DTcms.Common;
using System.Linq;

namespace DTcms.Web.admin.projectmanage
{
    public partial class apply_project_fee : Web.UI.ManagePage
    {
        private string action = DTEnums.ActionEnum.Add.ToString(); //操作类型
        private int id = 0;

        BLL.ky_applyproject ky_applyproject_bll = new BLL.ky_applyproject();
        BLL.ky_applyproject_jingfei ky_Applyproject_jingfei_bll = new BLL.ky_applyproject_jingfei();
        BLL.ky_applyproject_shebei ky_Applyproject_Shebei_BLL = new BLL.ky_applyproject_shebei();

        protected int totalCount;
        protected int page;
        protected int pageSize;
        protected void Page_Load(object sender, EventArgs e)
        {
            this.pageSize = 50; //每页数量
            hd_add_user.Value = GetAdminInfo().id.ToString();


            if (!int.TryParse(Request.QueryString["id"] as string, out this.id))
            {
                JscriptMsg("传输参数不正确！", "back");
                return;
            }
            hd_pa_id.Value = this.id.ToString();
            if (!IsPostBack)
            {
                ShowInfo(this.id);
                RptBind("1=1 ", "add_time asc");
            }
        }

        #region 赋值操作=================================
        private void ShowInfo(int _id)
        {
           


            var  model = ky_Applyproject_jingfei_bll.GetModelList("pa_id="+_id).FirstOrDefault();
            if (model != null)
            {
                this.txt_laiyuan_zhuanxiang.Value = model.laiyuan_zhuanxiang;
                this.txt_laiyuan_qita.Value = model.laiyuan_qita;
                this.txt_zhijie_shebei.Value = model.zhijie_shebei;
                this.txt_zhijie_sb_gouzhi.Value = model.zhijie_sb_gouzhi;
                this.txt_zhijie_sb_shizhi.Value = model.zhijie_sb_shizhi;
                this.txt_zhijie_sb_gaizao.Value = model.zhijie_sb_gaizao;
                this.txt_zhijie_cailiao.Value = model.zhijie_cailiao;
                this.txt_zhijie_ceshi.Value = model.zhijie_ceshi;
                this.txt_zhijie_ranliao.Value = model.zhijie_ranliao;
                this.txt_zhijie_chailv.Value = model.zhijie_chailv;
                this.txt_zhijie_huiyi.Value = model.zhijie_huiyi;
                this.txt_zhijie_hezuojiaoliu.Value = model.zhijie_hezuojiaoliu;
                this.txt_zhijie_xinxi.Value = model.zhijie_xinxi;
                this.txt_zhijie_zixun.Value = model.zhijie_zixun;
                this.txt_zhijie_laowu.Value = model.zhijie_laowu;
                this.txt_zhijie_qita.Value = model.zhijie_qita;
                this.txt_jianjie_guanli.Value = model.jianjie_guanli;
                this.txt_jianjie_jixiao.Value = model.jianjie_jixiao;
                this.txt_zhijie_shebei1.Value = model.zhijie_shebei1;
                this.txt_zhijie_sb_gouzhi1.Value = model.zhijie_sb_gouzhi1;
                this.txt_zhijie_sb_shizhi1.Value = model.zhijie_sb_shizhi1;
                this.txt_zhijie_sb_gaizao1.Value = model.zhijie_sb_gaizao1;
                this.txt_zhijie_cailiao1.Value = model.zhijie_cailiao1;
                this.txt_zhijie_ceshi1.Value = model.zhijie_ceshi1;
                this.txt_zhijie_ranliao1.Value = model.zhijie_ranliao1;
                this.txt_zhijie_chailv1.Value = model.zhijie_chailv1;
                this.txt_zhijie_huiyi1.Value = model.zhijie_huiyi1;
                this.txt_zhijie_hezuojiaoliu1.Value = model.zhijie_hezuojiaoliu1;
                this.txt_zhijie_xinxi1.Value = model.zhijie_xinxi1;
                this.txt_zhijie_zixun1.Value = model.zhijie_zixun1;
                this.txt_zhijie_laowu1.Value = model.zhijie_laowu1;
                this.txt_zhijie_qita1.Value = model.zhijie_qita1;
                this.txt_jianjie_guanli1.Value = model.jianjie_guanli1;
                this.txt_jianjie_jixiao1.Value = model.jianjie_jixiao1;
                this.txt_zhijie_shebei2.Value = model.zhijie_shebei2;
                this.txt_zhijie_sb_gouzhi2.Value = model.zhijie_sb_gouzhi2;
                this.txt_zhijie_sb_shizhi2.Value = model.zhijie_sb_shizhi2;
                this.txt_zhijie_sb_gaizao2.Value = model.zhijie_sb_gaizao2;
                this.txt_zhijie_cailiao2.Value = model.zhijie_cailiao2;
                this.txt_zhijie_ceshi2.Value = model.zhijie_ceshi2;
                this.txt_zhijie_ranliao2.Value = model.zhijie_ranliao2;
                this.txt_zhijie_chailv2.Value = model.zhijie_chailv2;
                this.txt_zhijie_huiyi2.Value = model.zhijie_huiyi2;
                this.txt_zhijie_hezuojiaoliu2.Value = model.zhijie_hezuojiaoliu2;
                this.txt_zhijie_xinxi2.Value = model.zhijie_xinxi2;
                this.txt_zhijie_zixun2.Value = model.zhijie_zixun2;
                this.txt_zhijie_laowu2.Value = model.zhijie_laowu2;
                this.txt_zhijie_qita2.Value = model.zhijie_qita2;
                this.txt_jianjie_guanli2.Value = model.jianjie_guanli2;
                this.txt_jianjie_jixiao2.Value = model.jianjie_jixiao2;

                this.txt_zhijie1.Value = model.zhijie1;
                this.txt_zhijie2.Value = model.zhijie2;
                this.txt_zhijie.Value = model.zhijie;
                this.txt_jianjie.Value = model.jianjie;
                this.txt_jianjie1.Value = model.jianjie1;
                this.txt_jianjie2.Value = model.jianjie2;
                this.txt_heji.Value = model.heji;
                this.txt_heji1.Value = model.heji1;
                this.txt_heji2.Value = model.heji2;
            }
        }
        #endregion

        #region 数据绑定=======================================
        private void RptBind(string _strWhere, string _orderby)
        {
            this.page = 1;// DTRequest.GetQueryInt("page", 1);

            _strWhere += " and pa_id=" + this.id;
            this.rptList.DataSource = ky_Applyproject_Shebei_BLL.GetList(this.pageSize, this.page, _strWhere, _orderby, out this.totalCount);
            this.rptList.DataBind();

            //绑定页码
            //txtPageNum.Text = this.pageSize.ToString();
            //string pageUrl = Utils.CombUrlTxt("apply_projectlist.aspx", "keywords={0}&page={1}", this.keywords, "__id__");
            //PageContent.InnerHtml = Utils.OutPageList(this.pageSize, this.page, this.totalCount, pageUrl, 8);
        }
        #endregion


        #region 修改操作=================================
        private bool DoEdit(int _id)
        {
            bool result = false;

            string laiyuan_zhuanxiang = this.txt_laiyuan_zhuanxiang.Value;
            string laiyuan_qita = this.txt_laiyuan_qita.Value;
            string zhijie_shebei = this.txt_zhijie_shebei.Value;
            string zhijie_sb_gouzhi = this.txt_zhijie_sb_gouzhi.Value;
            string zhijie_sb_shizhi = this.txt_zhijie_sb_shizhi.Value;
            string zhijie_sb_gaizao = this.txt_zhijie_sb_gaizao.Value;
            string zhijie_cailiao = this.txt_zhijie_cailiao.Value;
            string zhijie_ceshi = this.txt_zhijie_ceshi.Value;
            string zhijie_ranliao = this.txt_zhijie_ranliao.Value;
            string zhijie_chailv = this.txt_zhijie_chailv.Value;
            string zhijie_huiyi = this.txt_zhijie_huiyi.Value;
            string zhijie_hezuojiaoliu = this.txt_zhijie_hezuojiaoliu.Value;
            string zhijie_xinxi = this.txt_zhijie_xinxi.Value;
            string zhijie_zixun = this.txt_zhijie_zixun.Value;
            string zhijie_laowu = this.txt_zhijie_laowu.Value;
            string zhijie_qita = this.txt_zhijie_qita.Value;
            string jianjie_guanli = this.txt_jianjie_guanli.Value;
            string jianjie_jixiao = this.txt_jianjie_jixiao.Value;
            string zhijie_shebei1 = this.txt_zhijie_shebei1.Value;
            string zhijie_sb_gouzhi1 = this.txt_zhijie_sb_gouzhi1.Value;
            string zhijie_sb_shizhi1 = this.txt_zhijie_sb_shizhi1.Value;
            string zhijie_sb_gaizao1 = this.txt_zhijie_sb_gaizao1.Value;
            string zhijie_cailiao1 = this.txt_zhijie_cailiao1.Value;
            string zhijie_ceshi1 = this.txt_zhijie_ceshi1.Value;
            string zhijie_ranliao1 = this.txt_zhijie_ranliao1.Value;
            string zhijie_chailv1 = this.txt_zhijie_chailv1.Value;
            string zhijie_huiyi1 = this.txt_zhijie_huiyi1.Value;
            string zhijie_hezuojiaoliu1 = this.txt_zhijie_hezuojiaoliu1.Value;
            string zhijie_xinxi1 = this.txt_zhijie_xinxi1.Value;
            string zhijie_zixun1 = this.txt_zhijie_zixun1.Value;
            string zhijie_laowu1 = this.txt_zhijie_laowu1.Value;
            string zhijie_qita1 = this.txt_zhijie_qita1.Value;
            string jianjie_guanli1 = this.txt_jianjie_guanli1.Value;
            string jianjie_jixiao1 = this.txt_jianjie_jixiao1.Value;
            string zhijie_shebei2 = this.txt_zhijie_shebei2.Value;
            string zhijie_sb_gouzhi2 = this.txt_zhijie_sb_gouzhi2.Value;
            string zhijie_sb_shizhi2 = this.txt_zhijie_sb_shizhi2.Value;
            string zhijie_sb_gaizao2 = this.txt_zhijie_sb_gaizao2.Value;
            string zhijie_cailiao2 = this.txt_zhijie_cailiao2.Value;
            string zhijie_ceshi2 = this.txt_zhijie_ceshi2.Value;
            string zhijie_ranliao2 = this.txt_zhijie_ranliao2.Value;
            string zhijie_chailv2 = this.txt_zhijie_chailv2.Value;
            string zhijie_huiyi2 = this.txt_zhijie_huiyi2.Value;
            string zhijie_hezuojiaoliu2 = this.txt_zhijie_hezuojiaoliu2.Value;
            string zhijie_xinxi2 = this.txt_zhijie_xinxi2.Value;
            string zhijie_zixun2 = this.txt_zhijie_zixun2.Value;
            string zhijie_laowu2 = this.txt_zhijie_laowu2.Value;
            string zhijie_qita2 = this.txt_zhijie_qita2.Value;
            string jianjie_guanli2 = this.txt_jianjie_guanli2.Value;
            string jianjie_jixiao2 = this.txt_jianjie_jixiao2.Value;

            //DateTime add_time = DateTime.Parse(this.txt_add_time.Value);
            //string add_user = this.txt_add_user.Value;
            //int status = int.Parse(this.txt_status.Value);
            string zhijie1 = this.txt_zhijie1.Value;
            string zhijie2 = this.txt_zhijie2.Value;
            string zhijie = this.txt_zhijie.Value;
            string jianjie = this.txt_jianjie.Value;
            string jianjie1 = this.txt_jianjie1.Value;
            string jianjie2 = this.txt_jianjie2.Value;
            string heji = this.txt_heji.Value;
            string heji1 = this.txt_heji1.Value;
            string heji2 = this.txt_heji2.Value;


            DTcms.Model.ky_applyproject_jingfei model = new DTcms.Model.ky_applyproject_jingfei();
           
           


            if (ky_Applyproject_jingfei_bll.Exists(this.id.ToString()))
            {
                model = ky_Applyproject_jingfei_bll.GetModelList("pa_id=" + _id).FirstOrDefault();
                model.laiyuan_zhuanxiang = laiyuan_zhuanxiang;
                model.laiyuan_qita = laiyuan_qita;
                model.zhijie_shebei = zhijie_shebei;
                model.zhijie_sb_gouzhi = zhijie_sb_gouzhi;
                model.zhijie_sb_shizhi = zhijie_sb_shizhi;
                model.zhijie_sb_gaizao = zhijie_sb_gaizao;
                model.zhijie_cailiao = zhijie_cailiao;
                model.zhijie_ceshi = zhijie_ceshi;
                model.zhijie_ranliao = zhijie_ranliao;
                model.zhijie_chailv = zhijie_chailv;
                model.zhijie_huiyi = zhijie_huiyi;
                model.zhijie_hezuojiaoliu = zhijie_hezuojiaoliu;
                model.zhijie_xinxi = zhijie_xinxi;
                model.zhijie_zixun = zhijie_zixun;
                model.zhijie_laowu = zhijie_laowu;
                model.zhijie_qita = zhijie_qita;
                model.jianjie_guanli = jianjie_guanli;
                model.jianjie_jixiao = jianjie_jixiao;
                model.zhijie_shebei1 = zhijie_shebei1;
                model.zhijie_sb_gouzhi1 = zhijie_sb_gouzhi1;
                model.zhijie_sb_shizhi1 = zhijie_sb_shizhi1;
                model.zhijie_sb_gaizao1 = zhijie_sb_gaizao1;
                model.zhijie_cailiao1 = zhijie_cailiao1;
                model.zhijie_ceshi1 = zhijie_ceshi1;
                model.zhijie_ranliao1 = zhijie_ranliao1;
                model.zhijie_chailv1 = zhijie_chailv1;
                model.zhijie_huiyi1 = zhijie_huiyi1;
                model.zhijie_hezuojiaoliu1 = zhijie_hezuojiaoliu1;
                model.zhijie_xinxi1 = zhijie_xinxi1;
                model.zhijie_zixun1 = zhijie_zixun1;
                model.zhijie_laowu1 = zhijie_laowu1;
                model.zhijie_qita1 = zhijie_qita1;
                model.jianjie_guanli1 = jianjie_guanli1;
                model.jianjie_jixiao1 = jianjie_jixiao1;
                model.zhijie_shebei2 = zhijie_shebei2;
                model.zhijie_sb_gouzhi2 = zhijie_sb_gouzhi2;
                model.zhijie_sb_shizhi2 = zhijie_sb_shizhi2;
                model.zhijie_sb_gaizao2 = zhijie_sb_gaizao2;
                model.zhijie_cailiao2 = zhijie_cailiao2;
                model.zhijie_ceshi2 = zhijie_ceshi2;
                model.zhijie_ranliao2 = zhijie_ranliao2;
                model.zhijie_chailv2 = zhijie_chailv2;
                model.zhijie_huiyi2 = zhijie_huiyi2;
                model.zhijie_hezuojiaoliu2 = zhijie_hezuojiaoliu2;
                model.zhijie_xinxi2 = zhijie_xinxi2;
                model.zhijie_zixun2 = zhijie_zixun2;
                model.zhijie_laowu2 = zhijie_laowu2;
                model.zhijie_qita2 = zhijie_qita2;
                model.jianjie_guanli2 = jianjie_guanli2;
                model.jianjie_jixiao2 = jianjie_jixiao2;

                model.zhijie1 = zhijie1;
                model.zhijie2 = zhijie2;
                model.zhijie = zhijie;
                model.jianjie = jianjie;
                model.jianjie1 = jianjie1;
                model.jianjie2 = jianjie2;
                model.heji = heji;
                model.heji1 = heji1;
                model.heji2 = heji2;
                if (ky_Applyproject_jingfei_bll.Update(model))
                {

                    result = true;
                }
            }
            else
            {
             
                model.pa_id = this.id.ToString();


                model.laiyuan_zhuanxiang = laiyuan_zhuanxiang;
                model.laiyuan_qita = laiyuan_qita;
                model.zhijie_shebei = zhijie_shebei;
                model.zhijie_sb_gouzhi = zhijie_sb_gouzhi;
                model.zhijie_sb_shizhi = zhijie_sb_shizhi;
                model.zhijie_sb_gaizao = zhijie_sb_gaizao;
                model.zhijie_cailiao = zhijie_cailiao;
                model.zhijie_ceshi = zhijie_ceshi;
                model.zhijie_ranliao = zhijie_ranliao;
                model.zhijie_chailv = zhijie_chailv;
                model.zhijie_huiyi = zhijie_huiyi;
                model.zhijie_hezuojiaoliu = zhijie_hezuojiaoliu;
                model.zhijie_xinxi = zhijie_xinxi;
                model.zhijie_zixun = zhijie_zixun;
                model.zhijie_laowu = zhijie_laowu;
                model.zhijie_qita = zhijie_qita;
                model.jianjie_guanli = jianjie_guanli;
                model.jianjie_jixiao = jianjie_jixiao;
                model.zhijie_shebei1 = zhijie_shebei1;
                model.zhijie_sb_gouzhi1 = zhijie_sb_gouzhi1;
                model.zhijie_sb_shizhi1 = zhijie_sb_shizhi1;
                model.zhijie_sb_gaizao1 = zhijie_sb_gaizao1;
                model.zhijie_cailiao1 = zhijie_cailiao1;
                model.zhijie_ceshi1 = zhijie_ceshi1;
                model.zhijie_ranliao1 = zhijie_ranliao1;
                model.zhijie_chailv1 = zhijie_chailv1;
                model.zhijie_huiyi1 = zhijie_huiyi1;
                model.zhijie_hezuojiaoliu1 = zhijie_hezuojiaoliu1;
                model.zhijie_xinxi1 = zhijie_xinxi1;
                model.zhijie_zixun1 = zhijie_zixun1;
                model.zhijie_laowu1 = zhijie_laowu1;
                model.zhijie_qita1 = zhijie_qita1;
                model.jianjie_guanli1 = jianjie_guanli1;
                model.jianjie_jixiao1 = jianjie_jixiao1;
                model.zhijie_shebei2 = zhijie_shebei2;
                model.zhijie_sb_gouzhi2 = zhijie_sb_gouzhi2;
                model.zhijie_sb_shizhi2 = zhijie_sb_shizhi2;
                model.zhijie_sb_gaizao2 = zhijie_sb_gaizao2;
                model.zhijie_cailiao2 = zhijie_cailiao2;
                model.zhijie_ceshi2 = zhijie_ceshi2;
                model.zhijie_ranliao2 = zhijie_ranliao2;
                model.zhijie_chailv2 = zhijie_chailv2;
                model.zhijie_huiyi2 = zhijie_huiyi2;
                model.zhijie_hezuojiaoliu2 = zhijie_hezuojiaoliu2;
                model.zhijie_xinxi2 = zhijie_xinxi2;
                model.zhijie_zixun2 = zhijie_zixun2;
                model.zhijie_laowu2 = zhijie_laowu2;
                model.zhijie_qita2 = zhijie_qita2;
                model.jianjie_guanli2 = jianjie_guanli2;
                model.jianjie_jixiao2 = jianjie_jixiao2;

                model.zhijie1 = zhijie1;
                model.zhijie2 = zhijie2;
                model.zhijie = zhijie;
                model.jianjie = jianjie;
                model.jianjie1 = jianjie1;
                model.jianjie2 = jianjie2;
                model.heji = heji;
                model.heji1 = heji1;
                model.heji2 = heji2;


                model.add_time = System.DateTime.Now; ;
                model.add_user = GetAdminInfo().id.ToString();
                model.status = 0;

                if (ky_Applyproject_jingfei_bll.Add(model) > 0)
                {
                    result = true;
                }

            }
           

            return result;
        }
        #endregion
        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            // 
            ChkAdminLevel("apply_projectlist", DTEnums.ActionEnum.Edit.ToString()); //检查权限
            if (!DoEdit(this.id))
            {
                JscriptMsg("保存过程中发生错误！", "");
                return;
            }
            JscriptMsg("保存信息成功！", "apply_projectlist.aspx");


            //if (action == DTEnums.ActionEnum.Edit.ToString()) //修改
            //{
            //    ChkAdminLevel("apply_projectlist", DTEnums.ActionEnum.Edit.ToString()); //检查权限
            //    //if (!DoEdit(this.id))
            //    //{
            //    //    JscriptMsg("保存过程中发生错误！", "");
            //    //    return;
            //    //}
            //    JscriptMsg("修改信息成功！", "apply_projectlist.aspx");
            //}
            //else //添加
            //{
            //    ChkAdminLevel("apply_projectlist", DTEnums.ActionEnum.Add.ToString()); //检查权限
            //    //if (!DoAdd())
            //    //{
            //    //    JscriptMsg("保存过程中发生错误！", "");
            //    //    return;
            //    //}
            //    JscriptMsg("添加信息成功！", "apply_projectlist.aspx");
            //}
        }

    }
}