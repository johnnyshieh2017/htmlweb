﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="apply_project_team.aspx.cs" Inherits="DTcms.Web.admin.projectmanage.apply_project_team" %>

<%@ Import Namespace="DTcms.Common" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>项目人员情况</title>
    <meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,initial-scale=1.0,user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <link rel="stylesheet" type="text/css" href="../../scripts/artdialog/ui-dialog.css" />
    <link rel="stylesheet" type="text/css" href="../skin/icon/iconfont.css" />
    <link rel="stylesheet" type="text/css" href="../skin/default/style.css" />
    <script type="text/javascript" charset="utf-8" src="../../scripts/jquery/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../scripts/jquery/Validform_v5.3.2_min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../scripts/datepicker/WdatePicker.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../scripts/artdialog/dialog-plus-min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../scripts/webuploader/webuploader.min.js"></script>

    <script type="text/javascript" charset="utf-8" src="../../editor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../editor/ueditor.all.min.js"> </script>
    <script type="text/javascript" charset="utf-8" src="../../editor/lang/zh-cn/zh-cn.js"></script>

    <script type="text/javascript" charset="utf-8" src="../js/uploader.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/common.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/laymain.js"></script>
    <script type="text/javascript">
        $(function () {
            //初始化表单验证
            $("#form1").initValidform();
            //初始化上传控件
            $(".upload-img").InitUploader({
                filesize: "<%=sysConfig.attachsize %>", sendurl: "../../tools/upload_ajax.ashx", swf: "../../scripts/webuploader/uploader.swf", filetypes: "<%=sysConfig.fileextension %>"
            });

            //初始化editor
            $(".editor-mini").each(function (i) {
                var objId = $(this).attr("id");
                if (objId != "undefined") {
                    var editorMini = UE.getEditor(objId, {
                        serverUrl: '../../../tools/upload_ajax.ashx',
                        initialFrameWidth: '100%',
                        initialFrameHeight: 220,
                        toolbars: [[
                            'fullscreen', 'source', '|', 'undo', 'redo', '|',
                            'bold', 'italic', 'underline', 'strikethrough', 'removeformat', 'pasteplain', '|', 'forecolor', 'insertorderedlist', 'insertunorderedlist', '|',
                            'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|',
                            'link', 'unlink', 'anchor', '|',
                            'simpleupload', 'insertimage', 'scrawl', 'insertvideo'
                        ]]
                    });
                }
            });

            //创建上传附件
            $(".attach-btn").click(function () {
                showAttachDialog();
            });

        });


        //初始化附件窗口
        function showAttachDialog(obj) {
            var objNum = arguments.length;
            var attachDialog = top.dialog({
                id: 'attachDialogId',
                title: "上传附件",
                url: 'projectmanage/dialog_attach.aspx',
                width: 500,
                height: 180,
                onclose: function () {
                    var liHtml = this.returnValue; //获取返回值
                    if (liHtml.length > 0) {
                        $("#showAttachList").children("ul").append(liHtml);
                    }
                }
            }).showModal();
            //如果是修改状态，将对象传进去
            if (objNum == 1) {
                attachDialog.data = obj;
            }
        }
        //删除附件节点
        function delAttachNode(obj) {
            $(obj).parent().remove();
        }

        function add_teampeople()
        {

            var name = $("#txtName").val();
            if (name == "") {
                alert('请输入姓名！');
                $("#txtName").focus();
                return;
            }

          

            var brithday = $("#txtbrithday").val();
            if (brithday == "") {
                alert('请输入出生年月！');
                $("#txtbrithday").focus();
                return;
            }

            var sex = $("#dropsex").val();

            var zhuanye = $("#txtzhuanye").val();
            if (zhuanye == "") {
                alert('请输入专业！');
                $("#txtzhuanye").focus();
                return;
            }

            var zhicheng = $("#txtzhicheng").val();
            if (zhicheng == "") {
                alert('请输入职称！');
                $("#txtzhicheng").focus();
                return;
            }

            var xueli = $("#txtxueli").val();
            if (xueli == "") {
                alert('请输入学历！');
                $("#txtxueli").focus();
                return;
            }

            var danwei = $("#txtdanwei").val();
            if (danwei == "") {
                alert('请输入单位！');
                $("#txtdanwei").focus();
                return;
            }

            var fengong = $("#txtfengong").val();
            if (fengong == "") {
                alert('请输入分工！');
                $("#txtfengong").focus();
                return;
            }


            if ($("#btnAdd").attr("title") != "") {
                //alert('编辑完成' + $("#btnAdd").attr("title"));



                $.ajax({
                    type: "post",
                    dataType: "json",

                    data: { name: name, sex: sex, chusheng: brithday, zhuanye: zhuanye, zhicheng: zhicheng, xueli: xueli, danwei: danwei, fengong: fengong, add_user: $("#hd_add_user").val(), id: $("#btnAdd").attr("title")},//注意：data参数可以是string个int类型

                    url: "ajax.ashx?action=edit_team_person",//模拟web服务，提交到方法
                    // 可选的 async:false,阻塞的异步就是同步
                    beforeSend: function () {
                        // do something.
                        // 一般是禁用按钮等防止用户重复提交
                        // $("#btnClick").attr({disabled:"disabled"});
                        // 或者是显示loading图片
                    },
                    success: function (data) {
                        //alert("success: " + data);//注意这里：必须通过data.d才能获取到服务器返回的值
                        if (data.ResultCode == "1") {
                            console.log(data.ResultMessage);

                            parent.jsprint("保存成功!请刷新。", "");
                            //
                            $("#txtName").val("");
                            $("#txtbrithday").val("");



                            $("#txtzhuanye").val("");
                            $("#txtzhicheng").val("");
                            $("#txtxueli").val("");
                            $("#txtdanwei").val("");
                            $("#txtfengong").val("");
                            $("#btnAdd").val("添加");
                            $("#btnAdd").attr("title", "");
                        }
                        else {
                            parent.jsprint("保存失败!请稍后再试。", "");
                        }


                    },
                    complete: function () {
                        //do something.
                        //$("#btnClick").removeAttr("disabled");
                        // 隐藏loading图片


                    },
                    error: function (data) {

                        //alert("error: " + data);
                        parent.jsprint("服务器异常!error:" + data, "");
                    },

                });
            }
            else {


                $.ajax({
                    type: "post",
                    dataType: "json",

                    data: { pa_id: $("#hd_pa_id").val(), name: name, sex: sex, chusheng: brithday, zhuanye: zhuanye, zhicheng: zhicheng, xueli: xueli, danwei: danwei, fengong: fengong, add_user: $("#hd_add_user").val(), },//注意：data参数可以是string个int类型

                    url: "ajax.ashx?action=add_team_person",//模拟web服务，提交到方法
                    // 可选的 async:false,阻塞的异步就是同步
                    beforeSend: function () {
                        // do something.
                        // 一般是禁用按钮等防止用户重复提交
                        // $("#btnClick").attr({disabled:"disabled"});
                        // 或者是显示loading图片
                    },
                    success: function (data) {
                        //alert("success: " + data);//注意这里：必须通过data.d才能获取到服务器返回的值
                        if (data.ResultCode == "1") {
                            console.log(data.ResultMessage);

                            parent.jsprint("保存成功!", "");
                            //
                            var trhtml = "<tr>" +
                                "                       " +
                                "                        <td align=\"center\">" + name + "</td>" +
                                "                        <td align=\"center\">" + brithday + "</td>" +
                                "                    " +
                                "                        <td align=\"center\">" + sex + "</td>" +
                                "                        <td align=\"center\">" + zhuanye + "</td>" +
                                "                        <td align=\"center\">" + zhicheng + "</td>" +
                                "                        <td align=\"center\">" + xueli + "</td>" +
                                "                       " +
                                "                        <td align=\"center\">" + danwei + "</td>" +
                                "                      " +
                                "                        <td align=\"center\">" +
                                "                            " +
                                "                           " + fengong + "" +
                                "                        </td>" +
                                "                        <td align=\"center\">" +
                                "                            <a href=\"#\" onclick='edit_teampeople(" + data.ResultMessage + ",this)'>编辑</a> <a href=\"#\" onclick='del_teampeople(" + data.ResultMessage + ",this)'>删除</a>" +
                                "                        </td>" +
                                "                    </tr>";

                            $("#tb_teamlist").append(trhtml);
                        }
                        else {
                            parent.jsprint("保存失败!请稍后再试。", "");
                        }


                    },
                    complete: function () {
                        //do something.
                        //$("#btnClick").removeAttr("disabled");
                        // 隐藏loading图片


                    },
                    error: function (data) {

                        //alert("error: " + data);
                        parent.jsprint("服务器异常!error:" + data, "");
                    },

                });

            }
           
        }

        function edit_teampeople(id, thisObj) {
            $("#btnAdd").val("编辑");
            $("#btnAdd").attr("title", id);

            $("#txtName").val($(thisObj).parent().parent().children("td").eq(0).text());
            $("#txtbrithday").val($(thisObj).parent().parent().children("td").eq(1).text());
            $("#dropsex").val($(thisObj).parent().parent().children("td").eq(2).text());
            $(".boxwrap").find("span").text($(thisObj).parent().parent().children("td").eq(2).text());

            $("#txtzhuanye").val($(thisObj).parent().parent().children("td").eq(3).text());
            $("#txtzhicheng").val($(thisObj).parent().parent().children("td").eq(4).text());
            $("#txtxueli").val($(thisObj).parent().parent().children("td").eq(5).text());
            $("#txtdanwei").val($(thisObj).parent().parent().children("td").eq(6).text());
            $("#txtfengong").val($(thisObj).parent().parent().children("td").eq(7).text());

        }

        function del_teampeople(id, thisObj) {
            if (confirm('确认删除吗？')) {


                $.ajax({
                    type: "post",
                    dataType: "json",

                    data: { id:id},//注意：data参数可以是string个int类型

                    url: "ajax.ashx?action=del_team_person",//模拟web服务，提交到方法
                    // 可选的 async:false,阻塞的异步就是同步
                    beforeSend: function () {
                        // do something.
                        // 一般是禁用按钮等防止用户重复提交
                        // $("#btnClick").attr({disabled:"disabled"});
                        // 或者是显示loading图片
                    },
                    success: function (data) {
                        //alert("success: " + data);//注意这里：必须通过data.d才能获取到服务器返回的值
                        if (data.ResultCode == "1") {
                            console.log(data.ResultMessage);

                            $(thisObj).parent().parent().remove();

                            parent.jsprint("删除成功!", "");
                            //
                           
                        }
                        else {
                            parent.jsprint("删除失败!请稍后再试。", "");
                        }


                    },
                    complete: function () {
                        //do something.
                        //$("#btnClick").removeAttr("disabled");
                        // 隐藏loading图片


                    },
                    error: function (data) {

                        //alert("error: " + data);
                        parent.jsprint("服务器异常!error:" + data, "");
                    },

                });
               
               
            }
        }
    </script>
</head>

<body class="mainbody">
    <form id="form1" runat="server">
        <!--导航栏-->
        <div class="location">
            <a href="manager_list.aspx" class="back"><i class="iconfont icon-up"></i><span>返回列表页</span></a>
            <a href="../center.aspx"><i class="iconfont icon-home"></i><span>首页</span></a>
            <i class="arrow iconfont icon-arrow-right"></i>
            <a href="doc_list.aspx"><span>申报管理</span></a>
            <i class="arrow iconfont icon-arrow-right"></i>
            <span>项目人员情况</span>
        </div>
        <div class="line10"></div>
        <!--/导航栏-->

        <!--内容-->
        <div id="floatHead" class="content-tab-wrap">
            <div class="content-tab">
                <div class="content-tab-ul-wrap">
                    <ul>
                        <li><a class="selected" href="javascript:;">项目负责人</a></li>
                        <li><a href="javascript:;">项目主要参与人</a></li>

                    </ul>
                </div>
            </div>
        </div>

        <div class="tab-content">



            <dl>
                <dt>姓名</dt>
                <dd>
                    <asp:TextBox ID="txt_team_owner_name" runat="server" CssClass="input normal" datatype="*2-50"></asp:TextBox>

                    <span class="Validform_checktip">*</span>
                </dd>
            </dl>
            <dl>
                <dt>性别</dt>
                <dd>
                    <div class="rule-multi-radio">
                        <asp:RadioButtonList ID="rbl_team_owner_sex" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                            <asp:ListItem Value="男" Selected="True">男</asp:ListItem>
                            <asp:ListItem Value="女">女</asp:ListItem>

                        </asp:RadioButtonList>
                    </div>

                    <span class="Validform_checktip">*</span>
                </dd>
            </dl>
            <dl>
                <dt>出生年月</dt>
                <dd>
                   
                        <asp:TextBox ID="txt_team_owner_chusheng" runat="server" CssClass="input normal" datatype="*2-50"></asp:TextBox>
                    
                </dd>
            </dl>
            <dl>
                <dt>学历</dt>
                <dd>
                    
                        <asp:TextBox ID="txt_team_owner_xueli" runat="server" CssClass="input normal" datatype="*2-50"></asp:TextBox>
                    
                </dd>
            </dl>
             <dl>
                <dt>职务</dt>
                <dd>
                    
                        <asp:TextBox ID="txt_team_owner_zhiwu" runat="server" CssClass="input normal" datatype="*2-50"></asp:TextBox>
                    
                </dd>
            </dl>
             <dl>
                <dt>职称 </dt>
                <dd>
                   
                        <asp:TextBox ID="txt_team_owner_zhicheng" runat="server" CssClass="input normal" datatype="*2-50"></asp:TextBox>
                    
                </dd>
            </dl>
            <dl>
                <dt>从事专业 </dt>
                <dd>
                   
                        <asp:TextBox ID="txt_team_owner_zhuanye" runat="server" CssClass="input normal" datatype="*2-50"></asp:TextBox>
                   
                </dd>
            </dl>
          
            
            <dl>
                <dt>手机号码</dt>
                <dd>
                    <asp:TextBox ID="txt_team_owner_shouji" runat="server" CssClass="input normal" datatype="*2-50"></asp:TextBox>

                    <span class="Validform_checktip">*</span>
                </dd>
            </dl>
           
           <dl>
                <dt>简介</dt>
                <dd>
                    <asp:TextBox ID="txt_team_owner_jianjie" runat="server" CssClass="input" datatype="*2-201" TextMode="MultiLine" Width="440" Height="180" />
                    <span class="Validform_checktip">限200字</span>
                </dd>
            </dl>

        </div>
        <div class="tab-content" style="display: none">
            
          
            <div class="toolbar">
                <div class="box-wrap">
                    <a class="menu-btn"><i class="iconfont icon-more"></i></a>
                    <div class="l-list">
                    
                          <div class="date-input">

                               <input id="txtName" runat="server" type="text" placeholder="请输入姓名" class="input" />
                          </div>

                        <div class="date-input">

                                 <input id="txtbrithday" runat="server" type="text" onfocus="WdatePicker({dateFmt:'yyyy-MM'})" placeholder="请输入出生年月" class="input" />
                          </div>
                        <div class="rule-single-select">
                                <select id="dropsex" >
                                    <option value="男">男</option>
                                     <option value="女">女</option>
                                </select>
                            </div>
                         <div class="date-input">

                                 <input id="txtzhuanye" runat="server" type="text" placeholder="请输入从事专业" class="input" />
                          </div>
                        <div class="date-input">

                                 <input id="txtzhicheng" runat="server" type="text" placeholder="请输入职称" class="input" />
                          </div>
                         <div class="date-input">

                                 <input id="txtxueli" runat="server" type="text" placeholder="请输入学历" class="input" />
                          </div>
                        <div class="date-input">

                                 <input id="txtdanwei" runat="server" type="text" placeholder="请输入所在单位" class="input" />
                          </div>
                         <div class="date-input">

                                 <input id="txtfengong" runat="server" type="text" placeholder="请输入项目分工" class="input" />
                          </div>
                         <div class="date-input">

                                <input id="btnAdd" title="" type="button" class="btn" value="添加" onclick="add_teampeople()" style="background: #bbc0c1;color: #2d2929;width: 100px;" />
                          </div>
                    </div>
                   <%-- <div class="r-list" style="position: unset;">
                        <asp:TextBox ID="txtKeywords" runat="server" CssClass="keyword" />
                        
                    </div>
                   --%>
                   
                       
                </div>
           
       
            <asp:Repeater ID="rptList" runat="server">
                <HeaderTemplate>
                    <table id="tb_teamlist" width="100%" border="0" cellspacing="0" cellpadding="0" class="ltable">
                        <tr>
                                                                   
                            <th align="center" width="8%">姓名</th>
                         
                             <th align="center" width="7%">出生年月</th>
                            <th align="center" width="7%">性别</th>

                            <th align="center" width="8%">从事专业</th>
                           
                            <th align="center" width="7%">职称</th>
                           <th align="center" width="7%">学历</th>
                           <th align="center" width="7%">所在单位</th>
                           <th align="center" width="7%">项目分工</th>
                            <th align="center" width="6%">操作</th>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                       
                        <td align="center"><%# Eval("name") %></td>
                        <td align="center"><%# Eval("chusheng") %></td>
                    
                        <td align="center"><%#Eval("sex")%></td>
                        <td align="center"><%# Eval("zhuanye") %></td>
                        <td align="center"><%# Eval("zhicheng") %></td>
                        <td align="center"><%# Eval("xueli") %></td>

                       
                        <td align="center"><%# Eval("danwei") %></td>
                      
                        <td align="center"><%# Eval("fengong") %></td>

                        <td align="center">
                            <a href="#" onclick="edit_teampeople(<%# Eval("id") %>,this)">编辑</a> <a href="#" onclick="del_teampeople(<%# Eval("id") %>,this)">删除</a>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    <%#rptList.Items.Count == 0 ? "<tr><td align=\"center\" colspan=\"8\">暂无记录</td></tr>" : ""%>
  </table>
                </FooterTemplate>
            </asp:Repeater>

        </div>

        </div>

        <!--/内容-->

        <!--工具栏-->
        <div class="page-footer">
            <div class="btn-wrap">
                <asp:Button ID="btnSubmit" runat="server" Text="提交保存" CssClass="btn" OnClick="btnSubmit_Click" />
                <input name="btnReturn" type="button" value="返回上一页" class="btn yellow" onclick="javascript: history.back(-1);" />
            </div>
        </div>
        <!--/工具栏-->
        <asp:HiddenField ID="hd_pa_id" runat="server"  />
        <asp:HiddenField ID="hd_add_user" runat="server" />
    </form>
</body>
</html>
