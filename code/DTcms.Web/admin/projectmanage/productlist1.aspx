﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="productlist1.aspx.cs" Inherits="DTcms.Web.admin.projectmanage.productlist1" %>

<%@ Import Namespace="DTcms.Common" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,initial-scale=1.0,user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <title>项目成果</title>
    <link rel="stylesheet" type="text/css" href="../../scripts/artdialog/ui-dialog.css" />
    <link rel="stylesheet" type="text/css" href="../../css/pagination.css" />
    <link rel="stylesheet" type="text/css" href="../skin/icon/iconfont.css" />
    <link rel="stylesheet" type="text/css" href="../skin/default/style.css" />
    <script type="text/javascript" src="../../scripts/jquery/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="../../scripts/artdialog/dialog-plus-min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/laymain.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/common.js"></script>

</head>

<body class="mainbody">
    <form id="form1" runat="server">
        <!--导航栏-->
        <div class="location">
            <a href="javascript:history.back(-1);" class="back"><i class="iconfont icon-up"></i><span>返回上一页</span></a>
            <a href="../center.aspx" class="home"><i class="iconfont icon-home"></i><span>首页</span></a>
            <i class="arrow iconfont icon-arrow-right"></i>
            <span>项目成果-中文</span>
        </div>
        <!--/导航栏-->

        <!--工具栏-->
        <div id="floatHead" class="toolbar-wrap">
            <div class="toolbar">
                <div class="box-wrap">
                    <a class="menu-btn"><i class="iconfont icon-more"></i></a>
                    <div class="l-list">
                        <ul class="icon-list">
                            <li><a href="productadd1.aspx?action=<%=DTEnums.ActionEnum.Add %>"><i class="iconfont icon-close"></i><span>新增</span></a></li>
                           <%-- <li><a href="javascript:;" onclick="checkAll(this);"><i class="iconfont icon-check"></i><span>全选</span></a></li>--%>

                        </ul>

                        <div class="menu-list">

                          

                            <div class="rule-single-select">
                                <asp:DropDownList ID="ddldepartment" runat="server" OnSelectedIndexChanged="ddldepartment_SelectedIndexChanged" AutoPostBack="true">
                                    <asp:ListItem Value="0" Selected="True">所有科室</asp:ListItem>

                                </asp:DropDownList>
                            </div>

                              <div class="rule-single-select">
                                <asp:DropDownList ID="ddlzuo_zhe_lei_xing" runat="server" OnSelectedIndexChanged="zuo_zhe_lei_xing_SelectedIndexChanged" AutoPostBack="true">
                                    <asp:ListItem Value="0" Selected="True">所有作者类型</asp:ListItem>

                                </asp:DropDownList>
                            </div>

                            <div class="rule-single-select">
                                <asp:DropDownList ID="ddlqi_kan_ji_bie" runat="server" OnSelectedIndexChanged="ddlqi_kan_ji_bie_SelectedIndexChanged" AutoPostBack="true">
                                    <asp:ListItem Value="0" Selected="True">所有期刊级别</asp:ListItem>

                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="r-list">
                        <asp:TextBox ID="txtKeywords" runat="server" CssClass="keyword" />
                        <asp:LinkButton ID="lbtnSearch" runat="server" CssClass="btn-search" OnClick="btnSearch_Click"><i class="iconfont icon-search"></i></asp:LinkButton>
                    </div>
                </div>


            </div>
        </div>
        <!--/工具栏-->

        <!--列表-->
        <div class="table-container">
            <asp:Repeater ID="rptList" runat="server">
                <HeaderTemplate>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="ltable">
                        <tr>
                            <th align="center" width="5%">编号</th>
                            <th align="center" width="12%">论文名称</th>
                            <th align="center" width="8%">刊物名称</th>

                            <th align="center" width="8%">子栏目</th>

                            <th align="center" width="8%">期刊级别</th>
                            <th align="center" width="8%">科室</th>
                            <th align="center" width="8%">作者</th>
                            <th align="center" width="8%">作者类型</th>
                            <th align="center" width="8%">第一完成单位是否为本院</th>
                            <th align="center" width="8%">出版日期</th>
                            <th align="center" width="2%">卷</th>
                            <th align="center" width="2%">期</th>
                            <th align="center" width="2%">页</th>
                            <th width="8%">备注</th>
                            <th width="12%">操作</th>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td align="center"><%# Eval("pd_id") %></td>
                        <td align="center"><%# Eval("lun_wen_ming_cheng") %></td>
                        <td align="center"><%# Eval("kan_wu_ming_cheng") %></td>
                        <td align="center"><%#Eval("zi_lan_mu")%></td>
                        <td align="center"><%# DTcms.Web.admin.projectmanage.data_tool.Getqikanjibie(Eval("qi_kan_ji_bie").ToString()) %></td>
                        <td align="center"><%# Eval("hospitalname") %></td>
                        <td align="center"><%# Eval("zuo_zhe") %></td>
                        <td align="center"><%# DTcms.Web.admin.projectmanage.data_tool.Getzuozheleixing(Eval("zuo_zhe_lei_xing").ToString()) %></td>
                        <td align="center"><%# Eval("shi_fou_ben_yuan").ToString()=="1"?"是":"否"%></td>
                        <td align="center"><%# Eval("chu_ban_ri_qi","{0:yyyy-MM-dd}") %></td>
                        <td align="center"><%# Eval("juan") %></td>
                        <td align="center"><%# Eval("qi") %></td>
                        <td align="center"><%# Eval("ye") %></td>
                        <td align="center"><%# Eval("beizhu") %></td>

                        <td align="center">

                            <a href="productadd1.aspx?action=<%#DTEnums.ActionEnum.Edit %>&id=<%#Eval("pd_id")%>">修改</a>

                            <a href="#">删除</a>

                        </td>
                    </tr>

                </ItemTemplate>
                <FooterTemplate>
                    <%#rptList.Items.Count == 0 ? "<tr><td align=\"center\" colspan=\"8\">暂无记录</td></tr>" : ""%>
  </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
        <!--/列表-->

        <!--内容底部-->
        <div class="line20"></div>
        <div class="pagelist">
            <div class="l-btns">
                <span>显示</span><asp:TextBox ID="txtPageNum" runat="server" CssClass="pagenum" onkeydown="return checkNumber(event);"
                    OnTextChanged="txtPageNum_TextChanged" AutoPostBack="True"></asp:TextBox><span>条/页</span>
            </div>
            <div id="PageContent" runat="server" class="default"></div>
        </div>
        <!--/内容底部-->
    </form>
</body>
</html>

