﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DTcms.Common;
using System.Linq;

namespace DTcms.Web.admin.projectmanage
{
    public partial class apply_project_kpi : Web.UI.ManagePage
    {
        private string action = DTEnums.ActionEnum.Add.ToString(); //操作类型
        private int id = 0;

        BLL.ky_applyproject ky_applyproject_bll = new BLL.ky_applyproject();
        BLL.ky_applyproject_jixiao ky_Applyproject_Jixiao_bll = new BLL.ky_applyproject_jixiao();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!int.TryParse(Request.QueryString["id"] as string, out this.id))
            {
                JscriptMsg("传输参数不正确！", "back");
                return;
            }
            hd_pa_id.Value = this.id.ToString();
            if (!IsPostBack)
            {
                ShowInfo(this.id);
                //RptBind("1=1 ", "add_time asc");
            }
        }

        #region 赋值操作=================================
        private void ShowInfo(int _id)
        {



            var model = ky_Applyproject_Jixiao_bll.GetModelList("pa_id=" + _id).FirstOrDefault();
            if (model != null)
            {
                //this.txt_id.Value = model.id.ToString();
                //this.txt_pa_id.Value = model.pa_id;
                
                this.txt_zhuanli_shuliang.Value = model.zhuanli_shuliang;
                this.txt_zhuanli_shuliang_faming.Value = model.zhuanli_shuliang_faming;
                this.txt_zhuanli_shuliang_shiyongxinxing.Value = model.zhuanli_shuliang_shiyongxinxing;
                this.txt_zhuanli_shuliang_waiguan.Value = model.zhuanli_shuliang_waiguan;
                this.txt_zhuanli_shouquan_shuliang.Value = model.zhuanli_shouquan_shuliang;
                this.txt_zhuanli_shouquan_faming.Value = model.zhuanli_shouquan_faming;
                this.txt_zhuanli_shouquan_shiyongxinxing.Value = model.zhuanli_shouquan_shiyongxinxing;
                this.txt_zhuanli_shouquan_waiguan.Value = model.zhuanli_shouquan_waiguan;
                this.txt_ruanzhu_shuliang.Value = model.ruanzhu_shuliang;
                this.txt_lunwen_shuliang.Value = model.lunwen_shuliang;
                this.txt_lunwen_shuliang_sci.Value = model.lunwen_shuliang_sci;
                this.txt_lunwen_shuliang_ei.Value = model.lunwen_shuliang_ei;
                this.txt_zhuzuo_shuliang.Value = model.zhuzuo_shuliang;
                this.txt_biaozhun_shuliang.Value = model.biaozhun_shuliang;
                this.txt_biaozhun_shuliang_guoji.Value = model.biaozhun_shuliang_guoji;
                this.txt_biaozhun_shuliang_guojia.Value = model.biaozhun_shuliang_guojia;
                this.txt_biaozhun_shuliang_hangye.Value = model.biaozhun_shuliang_hangye;
                this.txt_biaozhun_shuliang_difang.Value = model.biaozhun_shuliang_difang;
                this.txt_biaozhun_shuliang_qiye.Value = model.biaozhun_shuliang_qiye;
                this.txt_qita_tianbujishukongbai_shuliang.Value = model.qita_tianbujishukongbai_shuliang;
                this.txt_qita_tianbujishukongbai_shuliang_guoji.Value = model.qita_tianbujishukongbai_shuliang_guoji;
                this.txt_qita_tianbujishukongbai_shuliang_guojia.Value = model.qita_tianbujishukongbai_shuliang_guojia;
                this.txt_qita_tianbujishukongbai_shuliang_shengji.Value = model.qita_tianbujishukongbai_shuliang_shengji;
                this.txt_qita_huojiang_shuliang.Value = model.qita_huojiang_shuliang;
                this.txt_qita_huojiang_shuliang_guojia.Value = model.qita_huojiang_shuliang_guojia;
                this.txt_qita_huojiang_shuliang_sheng.Value = model.qita_huojiang_shuliang_sheng;
                this.txt_qita_huojiang_shuliang_difang.Value = model.qita_huojiang_shuliang_difang;
                this.txt_qita_qita_xingongyi.Value = model.qita_qita_xingongyi;
                this.txt_qita_qita_xinchanping.Value = model.qita_qita_xinchanping;
                this.txt_qita_qita_xincailiao.Value = model.qita_qita_xincailiao;
                this.txt_qita_qita_xinzhuangbei.Value = model.qita_qita_xinzhuangbei;
                this.txt_qita_qita_pingtai.Value = model.qita_qita_pingtai;
                this.txt_qita_qita_kejichengguo.Value = model.qita_qita_kejichengguo;
                this.txt_rencai_yj_shuliang.Value = model.rencai_yj_shuliang;
                this.txt_rencai_yj_shuliang_boshi.Value = model.rencai_yj_shuliang_boshi;
                this.txt_rencai_yj_shuliang_shuoshi.Value = model.rencai_yj_shuliang_shuoshi;
                this.txt_jingjixiaoyi.Value = model.jingjixiaoyi;
                this.txt_shehuixiaoyi.Value = model.shehuixiaoyi;
                this.txtarea_qitashuoming.Value = model.qitashuoming;


                //this.txt_add_time.Value = model.add_time.ToString();
                //this.txt_add_user.Value = model.add_user;
                //this.txt_status.Value = model.status.ToString();

                this.txt_rencai_py_shuliang.Value = model.rencai_py_shuliang;
                this.txt_rencai_py_shuliang_boshi.Value = model.rencai_py_shuliang_boshi;
                this.txt_rencai_py_shuliang_shuoshi.Value = model.rencai_py_shuliang_shuoshi;

            }
        }
        #endregion

        #region 修改操作=================================
        private bool DoEdit(int _id)
        {
            bool result = false;


            string zhuanli_shuliang = this.txt_zhuanli_shuliang.Value;
            string zhuanli_shuliang_faming = this.txt_zhuanli_shuliang_faming.Value;
            string zhuanli_shuliang_shiyongxinxing = this.txt_zhuanli_shuliang_shiyongxinxing.Value;
            string zhuanli_shuliang_waiguan = this.txt_zhuanli_shuliang_waiguan.Value;
            string zhuanli_shouquan_shuliang = this.txt_zhuanli_shouquan_shuliang.Value;
            string zhuanli_shouquan_faming = this.txt_zhuanli_shouquan_faming.Value;
            string zhuanli_shouquan_shiyongxinxing = this.txt_zhuanli_shouquan_shiyongxinxing.Value;
            string zhuanli_shouquan_waiguan = this.txt_zhuanli_shouquan_waiguan.Value;
            string ruanzhu_shuliang = this.txt_ruanzhu_shuliang.Value;
            string lunwen_shuliang = this.txt_lunwen_shuliang.Value;
            string lunwen_shuliang_sci = this.txt_lunwen_shuliang_sci.Value;
            string lunwen_shuliang_ei = this.txt_lunwen_shuliang_ei.Value;
            string zhuzuo_shuliang = this.txt_zhuzuo_shuliang.Value;
            string biaozhun_shuliang = this.txt_biaozhun_shuliang.Value;
            string biaozhun_shuliang_guoji = this.txt_biaozhun_shuliang_guoji.Value;
            string biaozhun_shuliang_guojia = this.txt_biaozhun_shuliang_guojia.Value;
            string biaozhun_shuliang_hangye = this.txt_biaozhun_shuliang_hangye.Value;
            string biaozhun_shuliang_difang = this.txt_biaozhun_shuliang_difang.Value;
            string biaozhun_shuliang_qiye = this.txt_biaozhun_shuliang_qiye.Value;
            string qita_tianbujishukongbai_shuliang = this.txt_qita_tianbujishukongbai_shuliang.Value;
            string qita_tianbujishukongbai_shuliang_guoji = this.txt_qita_tianbujishukongbai_shuliang_guoji.Value;
            string qita_tianbujishukongbai_shuliang_guojia = this.txt_qita_tianbujishukongbai_shuliang_guojia.Value;
            string qita_tianbujishukongbai_shuliang_shengji = this.txt_qita_tianbujishukongbai_shuliang_shengji.Value;
            string qita_huojiang_shuliang = this.txt_qita_huojiang_shuliang.Value;
            string qita_huojiang_shuliang_guojia = this.txt_qita_huojiang_shuliang_guojia.Value;
            string qita_huojiang_shuliang_sheng = this.txt_qita_huojiang_shuliang_sheng.Value;
            string qita_huojiang_shuliang_difang = this.txt_qita_huojiang_shuliang_difang.Value;
            string qita_qita_xingongyi = this.txt_qita_qita_xingongyi.Value;
            string qita_qita_xinchanping = this.txt_qita_qita_xinchanping.Value;
            string qita_qita_xincailiao = this.txt_qita_qita_xincailiao.Value;
            string qita_qita_xinzhuangbei = this.txt_qita_qita_xinzhuangbei.Value;
            string qita_qita_pingtai = this.txt_qita_qita_pingtai.Value;
            string qita_qita_kejichengguo = this.txt_qita_qita_kejichengguo.Value;
            string rencai_yj_shuliang = this.txt_rencai_yj_shuliang.Value;
            string rencai_yj_shuliang_boshi = this.txt_rencai_yj_shuliang_boshi.Value;
            string rencai_yj_shuliang_shuoshi = this.txt_rencai_yj_shuliang_shuoshi.Value;
            string jingjixiaoyi = this.txt_jingjixiaoyi.Value;
            string shehuixiaoyi = this.txt_shehuixiaoyi.Value;
            string qitashuoming = this.txtarea_qitashuoming.Value;

            DateTime add_time = System.DateTime.Now;
            string add_user = GetAdminInfo().id.ToString();
            int status = 0;

            string rencai_py_shuliang = this.txt_rencai_py_shuliang.Value;
            string rencai_py_shuliang_boshi = this.txt_rencai_py_shuliang_boshi.Value;
            string rencai_py_shuliang_shuoshi = this.txt_rencai_py_shuliang_shuoshi.Value;

            DTcms.Model.ky_applyproject_jixiao model = new DTcms.Model.ky_applyproject_jixiao();

            if (ky_Applyproject_Jixiao_bll.Exists(_id.ToString()))
            {
                //编辑
                model = ky_Applyproject_Jixiao_bll.GetModelList("pa_id=" + _id).FirstOrDefault();

                model.zhuanli_shuliang = zhuanli_shuliang;
                model.zhuanli_shuliang_faming = zhuanli_shuliang_faming;
                model.zhuanli_shuliang_shiyongxinxing = zhuanli_shuliang_shiyongxinxing;
                model.zhuanli_shuliang_waiguan = zhuanli_shuliang_waiguan;
                model.zhuanli_shouquan_shuliang = zhuanli_shouquan_shuliang;
                model.zhuanli_shouquan_faming = zhuanli_shouquan_faming;
                model.zhuanli_shouquan_shiyongxinxing = zhuanli_shouquan_shiyongxinxing;
                model.zhuanli_shouquan_waiguan = zhuanli_shouquan_waiguan;
                model.ruanzhu_shuliang = ruanzhu_shuliang;
                model.lunwen_shuliang = lunwen_shuliang;
                model.lunwen_shuliang_sci = lunwen_shuliang_sci;
                model.lunwen_shuliang_ei = lunwen_shuliang_ei;
                model.zhuzuo_shuliang = zhuzuo_shuliang;
                model.biaozhun_shuliang = biaozhun_shuliang;
                model.biaozhun_shuliang_guoji = biaozhun_shuliang_guoji;
                model.biaozhun_shuliang_guojia = biaozhun_shuliang_guojia;
                model.biaozhun_shuliang_hangye = biaozhun_shuliang_hangye;
                model.biaozhun_shuliang_difang = biaozhun_shuliang_difang;
                model.biaozhun_shuliang_qiye = biaozhun_shuliang_qiye;
                model.qita_tianbujishukongbai_shuliang = qita_tianbujishukongbai_shuliang;
                model.qita_tianbujishukongbai_shuliang_guoji = qita_tianbujishukongbai_shuliang_guoji;
                model.qita_tianbujishukongbai_shuliang_guojia = qita_tianbujishukongbai_shuliang_guojia;
                model.qita_tianbujishukongbai_shuliang_shengji = qita_tianbujishukongbai_shuliang_shengji;
                model.qita_huojiang_shuliang = qita_huojiang_shuliang;
                model.qita_huojiang_shuliang_guojia = qita_huojiang_shuliang_guojia;
                model.qita_huojiang_shuliang_sheng = qita_huojiang_shuliang_sheng;
                model.qita_huojiang_shuliang_difang = qita_huojiang_shuliang_difang;
                model.qita_qita_xingongyi = qita_qita_xingongyi;
                model.qita_qita_xinchanping = qita_qita_xinchanping;
                model.qita_qita_xincailiao = qita_qita_xincailiao;
                model.qita_qita_xinzhuangbei = qita_qita_xinzhuangbei;
                model.qita_qita_pingtai = qita_qita_pingtai;
                model.qita_qita_kejichengguo = qita_qita_kejichengguo;
                model.rencai_yj_shuliang = rencai_yj_shuliang;
                model.rencai_yj_shuliang_boshi = rencai_yj_shuliang_boshi;
                model.rencai_yj_shuliang_shuoshi = rencai_yj_shuliang_shuoshi;
                model.jingjixiaoyi = jingjixiaoyi;
                model.shehuixiaoyi = shehuixiaoyi;
                model.qitashuoming = qitashuoming;
             

                model.rencai_py_shuliang = rencai_py_shuliang;
                model.rencai_py_shuliang_boshi = rencai_py_shuliang_boshi;
                model.rencai_py_shuliang_shuoshi = rencai_py_shuliang_shuoshi;

                if (ky_Applyproject_Jixiao_bll.Update(model))
                {

                    result = true;
                }
            }
            else
            {
                model.zhuanli_shuliang = zhuanli_shuliang;
                model.zhuanli_shuliang_faming = zhuanli_shuliang_faming;
                model.zhuanli_shuliang_shiyongxinxing = zhuanli_shuliang_shiyongxinxing;
                model.zhuanli_shuliang_waiguan = zhuanli_shuliang_waiguan;
                model.zhuanli_shouquan_shuliang = zhuanli_shouquan_shuliang;
                model.zhuanli_shouquan_faming = zhuanli_shouquan_faming;
                model.zhuanli_shouquan_shiyongxinxing = zhuanli_shouquan_shiyongxinxing;
                model.zhuanli_shouquan_waiguan = zhuanli_shouquan_waiguan;
                model.ruanzhu_shuliang = ruanzhu_shuliang;
                model.lunwen_shuliang = lunwen_shuliang;
                model.lunwen_shuliang_sci = lunwen_shuliang_sci;
                model.lunwen_shuliang_ei = lunwen_shuliang_ei;
                model.zhuzuo_shuliang = zhuzuo_shuliang;
                model.biaozhun_shuliang = biaozhun_shuliang;
                model.biaozhun_shuliang_guoji = biaozhun_shuliang_guoji;
                model.biaozhun_shuliang_guojia = biaozhun_shuliang_guojia;
                model.biaozhun_shuliang_hangye = biaozhun_shuliang_hangye;
                model.biaozhun_shuliang_difang = biaozhun_shuliang_difang;
                model.biaozhun_shuliang_qiye = biaozhun_shuliang_qiye;
                model.qita_tianbujishukongbai_shuliang = qita_tianbujishukongbai_shuliang;
                model.qita_tianbujishukongbai_shuliang_guoji = qita_tianbujishukongbai_shuliang_guoji;
                model.qita_tianbujishukongbai_shuliang_guojia = qita_tianbujishukongbai_shuliang_guojia;
                model.qita_tianbujishukongbai_shuliang_shengji = qita_tianbujishukongbai_shuliang_shengji;
                model.qita_huojiang_shuliang = qita_huojiang_shuliang;
                model.qita_huojiang_shuliang_guojia = qita_huojiang_shuliang_guojia;
                model.qita_huojiang_shuliang_sheng = qita_huojiang_shuliang_sheng;
                model.qita_huojiang_shuliang_difang = qita_huojiang_shuliang_difang;
                model.qita_qita_xingongyi = qita_qita_xingongyi;
                model.qita_qita_xinchanping = qita_qita_xinchanping;
                model.qita_qita_xincailiao = qita_qita_xincailiao;
                model.qita_qita_xinzhuangbei = qita_qita_xinzhuangbei;
                model.qita_qita_pingtai = qita_qita_pingtai;
                model.qita_qita_kejichengguo = qita_qita_kejichengguo;
                model.rencai_yj_shuliang = rencai_yj_shuliang;
                model.rencai_yj_shuliang_boshi = rencai_yj_shuliang_boshi;
                model.rencai_yj_shuliang_shuoshi = rencai_yj_shuliang_shuoshi;
                model.jingjixiaoyi = jingjixiaoyi;
                model.shehuixiaoyi = shehuixiaoyi;
                model.qitashuoming = qitashuoming;

                model.rencai_py_shuliang = rencai_py_shuliang;
                model.rencai_py_shuliang_boshi = rencai_py_shuliang_boshi;
                model.rencai_py_shuliang_shuoshi = rencai_py_shuliang_shuoshi;
                model.add_time = add_time;
                model.add_user = add_user;
                model.status = status;
                model.pa_id = _id.ToString();
                if (ky_Applyproject_Jixiao_bll.Add(model) > 0)
                {
                    result = true;
                }
            }


            return result;
        }
        #endregion

        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            // 
            ChkAdminLevel("apply_projectlist", DTEnums.ActionEnum.Edit.ToString()); //检查权限
            if (!DoEdit(this.id))
            {
                JscriptMsg("保存过程中发生错误！", "");
                return;
            }
            JscriptMsg("保存信息成功！", "apply_projectlist.aspx");


           
        }
    }
}