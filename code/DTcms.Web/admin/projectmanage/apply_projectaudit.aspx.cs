﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DTcms.Common;

namespace DTcms.Web.admin.projectmanage
{
    public partial class apply_projectaudit : Web.UI.ManagePage
    {
        private string action = DTEnums.ActionEnum.Add.ToString(); //操作类型
        private int id = 0;

        BLL.ky_applyproject Ky_Projects_bll = new BLL.ky_applyproject();

        BLL.ky_attachment attachment_bll = new BLL.ky_attachment();
        protected void Page_Load(object sender, EventArgs e)
        {
            string _action = DTRequest.GetQueryString("action");
            if (!string.IsNullOrEmpty(_action) && _action == DTEnums.ActionEnum.Audit.ToString())
            {
                this.action = DTEnums.ActionEnum.Audit.ToString();//审核类型
                if (!int.TryParse(Request.QueryString["id"] as string, out this.id))
                {
                    JscriptMsg("传输参数不正确！", "back");
                    return;
                }
                if (!Ky_Projects_bll.Exists(this.id))
                {
                    JscriptMsg("记录不存在或已被删除！", "back");
                    return;
                }
            }

            if (!IsPostBack)
            {
                ChkAdminLevel("apply_projectlist", DTEnums.ActionEnum.Audit.ToString()); //检查权限
                                                                                        // Model.manager model = GetAdminInfo(); //取得管理员信息
                InitData();
                if (action == DTEnums.ActionEnum.Audit.ToString()) //修改
                {
                    ShowInfo(this.id);
                }
            }
        }

        #region 初始化===================================

        void InitData()
        {
            var hospatil = new BLL.yjr_doc_hospital().GetModelList("");
            foreach (var item in hospatil)
            {
                ddlhospital.Items.Add(new ListItem(item.hospitalname, item.id.ToString()));
            }

            var project_type = new BLL.ky_dictionary().GetModelListByCache("project_type");
            foreach (var item in project_type)
            {
                ddl_projecttype.Items.Add(new ListItem(item.dic_text, item.dic_value.ToString()));
            }

            txt_owner.Text = GetAdminInfo().real_name;
        }
        #endregion

        #region 赋值操作=================================
        private void ShowInfo(int _id)
        {
            var model = Ky_Projects_bll.GetModel(_id);
            txt_title.Text = model.pa_title;
            txt_number.Text = model.pa_code;
            ddl_projecttype.SelectedValue = model.pa_type;
            ddlhospital.SelectedValue = model.pa_dptid;
            txt_owner.Text = model.pa_user;
            txtOrgbaseinfo.Text = model.dan_wei_ji_ben_xin_xi;
            hf_txtOrgbaseinfo.HRef = model.dan_wei_ji_ben_xin_xi;

            txtBaseinfo.Text = model.xiang_mu_ji_ben_xin_xi;
            hf_txtBaseinfo.HRef= model.xiang_mu_ji_ben_xin_xi;

            txtTeamInfo.Text = model.xiang_mu_tuan_dui;
            hf_txtTeamInfo.HRef= model.xiang_mu_tuan_dui;

            txtGaikuang.Text = model.xiang_mu_gai_kuang;
            txtShishi.Text = model.xiang_mu_shi_shi;
            txtTouzi.Text = model.xiang_mu_tou_zi;
            txtJixiao.Text = model.xiang_mu_ji_xiao;

            txt_intro.Text = model.beizhu;

            //绑定内容附件
            rptAttachList.DataSource = attachment_bll.GetModelList("apply_id='applyproject_" + this.id + "' "); ;//
            rptAttachList.DataBind();

        }
        #endregion

        #region 增加操作=================================
        private bool DoAdd()
        {
            DTcms.Model.ky_applyproject model_add = new Model.ky_applyproject();
            model_add.pa_title = txt_number.Text;
            model_add.pa_code = txt_title.Text;
            model_add.pa_type = ddl_projecttype.SelectedValue;
            model_add.pa_dptid = ddlhospital.SelectedValue;
            model_add.pa_user = txt_owner.Text;
            model_add.add_user = GetAdminInfo().user_name.ToString();
            model_add.add_time = System.DateTime.Now;
            model_add.beizhu = txt_intro.Text;

            model_add.dan_wei_ji_ben_xin_xi = txtOrgbaseinfo.Text;
            model_add.xiang_mu_ji_ben_xin_xi = txtBaseinfo.Text;
            model_add.xiang_mu_tuan_dui = txtTeamInfo.Text;
            model_add.xiang_mu_gai_kuang = txtGaikuang.Text;
            model_add.xiang_mu_shi_shi = txtShishi.Text;
            model_add.xiang_mu_tou_zi = txtTouzi.Text;
            model_add.xiang_mu_ji_xiao = txtJixiao.Text;

            var id = Ky_Projects_bll.Add(model_add);
            if (id > 0)
            {

                //
                #region 保存附件====================
                //保存附件
                string[] attachFileNameArr = Request.Form.GetValues("hid_attach_filename");
                string[] attachFilePathArr = Request.Form.GetValues("hid_attach_filepath");
                string[] attachFileSizeArr = Request.Form.GetValues("hid_attach_filesize");
                string[] attachPointArr = Request.Form.GetValues("txt_attach_point");
                if (attachFileNameArr != null && attachFilePathArr != null && attachFileSizeArr != null
                    && attachFileNameArr.Length > 0 && attachFilePathArr.Length > 0 && attachFileSizeArr.Length > 0)
                {
                    List<Model.article_attach> ls = new List<Model.article_attach>();
                    for (int i = 0; i < attachFileNameArr.Length; i++)
                    {
                        int fileSize = Utils.StrToInt(attachFileSizeArr[i], 0);
                        string fileExt = FileHelper.GetFileExt(attachFilePathArr[i]);
                        //int _point = Utils.StrToInt(attachPointArr[i], 0);
                        //ls.Add(new Model.article_attach { channel_id = this.channel_id, file_name = attachFileNameArr[i], file_path = attachFilePathArr[i], file_size = fileSize, file_ext = fileExt, point = _point });

                        //保存附件数据
                        DTcms.Model.ky_attachment _Attachment = new Model.ky_attachment();
                        _Attachment.file_name = attachFileNameArr[i];
                        _Attachment.file_path = attachFilePathArr[i];
                        _Attachment.file_ext = fileExt;
                        _Attachment.file_size = fileSize;
                        _Attachment.p_id = id;
                        _Attachment.add_time = System.DateTime.Now;
                        _Attachment.apply_id = "applyproject_" + id;

                        attachment_bll.Add(_Attachment);
                    }

                }
                #endregion

                //
                AddAdminLog(DTEnums.ActionEnum.Add.ToString(), "添加apply_project:" + model_add.pa_title); //记录日志
                return true;
            }
            return false;
        }
        #endregion

        #region 修改操作=================================
        private bool DoEdit(int _id)
        {
            bool result = false;

            DTcms.Model.ky_applyproject model_edit = Ky_Projects_bll.GetModel(_id);

            model_edit.status = 1;// 审批通过 ？？？  确定审批  流程  改造审批状态
           


            if (Ky_Projects_bll.Update(model_edit))
            {

                

                AddAdminLog(DTEnums.ActionEnum.Add.ToString(), "修改apply_project:" + model_edit.pa_title); //记录日志
                result = true;
            }

            return result;
        }
        #endregion

        //保存
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (action == DTEnums.ActionEnum.Audit.ToString()) //审核
            {
                ChkAdminLevel("apply_projectlist", DTEnums.ActionEnum.Edit.ToString()); //检查权限
                if (!DoEdit(this.id))
                {
                    JscriptMsg("保存过程中发生错误！", "");
                    return;
                }
                JscriptMsg("保存成功！", "apply_projectlist.aspx");
            }
            else //添加
            {
                JscriptMsg("保存过程中发生错误！", "");
                return;
            }
        }

       
    }
}