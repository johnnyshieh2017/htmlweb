﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DTcms.Common;


namespace DTcms.Web.admin.projectmanage
{
    public partial class apply_project_team : Web.UI.ManagePage
    {
        private string action = DTEnums.ActionEnum.Add.ToString(); //操作类型
        private int id = 0;

        BLL.ky_applyproject ky_applyproject_bll = new BLL.ky_applyproject();
        BLL.ky_applyproject_team ky_Applyproject_Team_bll = new BLL.ky_applyproject_team();

        protected int totalCount;
        protected int page;
        protected int pageSize;
        protected void Page_Load(object sender, EventArgs e)
        {
            this.pageSize = 50; //每页数量
            hd_add_user.Value = GetAdminInfo().id.ToString();


            if (!int.TryParse(Request.QueryString["id"] as string, out this.id))
            {
                JscriptMsg("传输参数不正确！", "back");
                return;
            }
            hd_pa_id.Value = this.id.ToString();
            if (!IsPostBack)
            {
                ShowInfo(this.id);
                RptBind("1=1 ", "add_time asc");
            }
        }

        #region 赋值操作=================================
        private void ShowInfo(int _id)
        {



            var model = ky_applyproject_bll.GetModel(_id);
            if (model != null)
            {
                this.txt_team_owner_name.Text = model.team_owner_name;
                rbl_team_owner_sex.SelectedValue = model.team_owner_sex;
                txt_team_owner_chusheng.Text = model.team_owner_chusheng;
                txt_team_owner_xueli.Text = model.team_owner_xueli;
                txt_team_owner_zhiwu.Text = model.team_owner_zhiwu;
                txt_team_owner_zhicheng.Text = model.team_owner_zhicheng;
                txt_team_owner_zhuanye.Text = model.team_owner_zhuanye;
                txt_team_owner_shouji.Text = model.team_owner_shouji;
                txt_team_owner_jianjie.Text = model.team_owner_jianjie;
            }
        }
        #endregion

        #region 数据绑定=======================================
        private void RptBind(string _strWhere, string _orderby)
        {
            this.page = 1;// DTRequest.GetQueryInt("page", 1);

            _strWhere += " and pa_id="+this.id;
            this.rptList.DataSource = ky_Applyproject_Team_bll.GetList(this.pageSize, this.page, _strWhere, _orderby, out this.totalCount);
            this.rptList.DataBind();

            //绑定页码
            //txtPageNum.Text = this.pageSize.ToString();
            //string pageUrl = Utils.CombUrlTxt("apply_projectlist.aspx", "keywords={0}&page={1}", this.keywords, "__id__");
            //PageContent.InnerHtml = Utils.OutPageList(this.pageSize, this.page, this.totalCount, pageUrl, 8);
        }
        #endregion


        #region 修改操作=================================
        private bool DoEdit(int _id)
        {
            bool result = false;

           
            DTcms.Model.ky_applyproject model = new DTcms.Model.ky_applyproject();




           
                model = ky_applyproject_bll.GetModel(_id);

                model.team_owner_name = txt_team_owner_name.Text;
                model.team_owner_sex = rbl_team_owner_sex.SelectedValue;
                model.team_owner_chusheng = txt_team_owner_chusheng.Text;
                model.team_owner_xueli = txt_team_owner_xueli.Text;
                model.team_owner_zhiwu = txt_team_owner_zhiwu.Text;
                model.team_owner_zhicheng = txt_team_owner_zhicheng.Text;
                model.team_owner_zhuanye = txt_team_owner_zhuanye.Text;
                model.team_owner_shouji = txt_team_owner_shouji.Text;
                model.team_owner_jianjie = txt_team_owner_jianjie.Text;


                if (ky_applyproject_bll.Update(model))
                {

                    result = true;
                }
            

            


            return result;
        }
        #endregion

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            ChkAdminLevel("apply_projectlist", DTEnums.ActionEnum.Edit.ToString()); //检查权限
            if (!DoEdit(this.id))
            {
                JscriptMsg("保存过程中发生错误！", "");
                return;
            }
            else
            {
                JscriptMsg("修改信息成功！", "apply_projectlist.aspx");
            }
           
        }

    }
}