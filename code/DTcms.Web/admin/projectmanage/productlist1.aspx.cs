﻿using DTcms.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DTcms.Web.admin.projectmanage
{
    public partial class productlist1 : Web.UI.ManagePage
    {
        protected int totalCount;
        protected int page;
        protected int pageSize;

        BLL.ky_products bll = new BLL.ky_products();
        protected string keywords = string.Empty;
      
        protected void Page_Load(object sender, EventArgs e)
        {

            this.keywords = DTRequest.GetQueryString("keywords");
          
            this.pageSize = GetPageSize(10); //每页数量
            if (!Page.IsPostBack)
            {
                ChkAdminLevel("productlist1", DTEnums.ActionEnum.View.ToString()); //检查权限
                Model.manager model = GetAdminInfo(); //取得当前管理员信息
                InitData();                                    
                RptBind(" 1=1 " + CombSqlTxt(keywords), "add_time desc");
            }
        }

        #region 初始化===================================

        void InitData()
        {
            var hospatil = new BLL.yjr_doc_hospital().GetModelList("");
            foreach (var item in hospatil)
            {
                ddldepartment.Items.Add(new ListItem(item.hospitalname, item.id.ToString()));
            }

            var author_type = new BLL.ky_dictionary().GetModelListByCache("author_type");
            foreach (var item in author_type)
            {
                ddlzuo_zhe_lei_xing.Items.Add(new ListItem(item.dic_text, item.dic_value));
            }

            var qikan_jibie = new BLL.ky_dictionary().GetModelListByCache("qikan_jibie");
            foreach (var item in qikan_jibie)
            {
                ddlqi_kan_ji_bie.Items.Add(new ListItem(item.dic_text, item.dic_value));
            }


        }
        #endregion

        #region 数据绑定=======================================
        private void RptBind(string _strWhere, string _orderby)
        {
            this.page = DTRequest.GetQueryInt("page", 1);
            txtKeywords.Text = this.keywords;
            
            this.rptList.DataSource = bll.GetList(this.pageSize, this.page, _strWhere, _orderby, out this.totalCount);
            this.rptList.DataBind();

            //绑定页码
            txtPageNum.Text = this.pageSize.ToString();
            string pageUrl = Utils.CombUrlTxt("productlist1.aspx", "keywords={0}&page={1}", this.keywords, "__id__");
            PageContent.InnerHtml = Utils.OutPageList(this.pageSize, this.page, this.totalCount, pageUrl, 8);
        }
        #endregion

        #region 组合SQL查询语句==========================
        protected string CombSqlTxt(string _keywords)
        {
            StringBuilder strTemp = new StringBuilder();
            _keywords = _keywords.Replace("'", "");
            if (!string.IsNullOrEmpty(_keywords))
            {
                strTemp.Append(" and (lun_wen_ming_cheng like  '%" + _keywords + "%' or kan_wu_ming_cheng like  '%" + _keywords + "%' or zuo_zhe like  '%" + _keywords + "%')");
            }


            //期刊级别
            if (ddlqi_kan_ji_bie.SelectedValue != "0")
            {
                strTemp.Append(" and qi_kan_ji_bie='"+ddlqi_kan_ji_bie.SelectedValue+"'");
            }


            //作者类型
            if (ddlzuo_zhe_lei_xing.SelectedValue != "0")
            {
                strTemp.Append(" and zuo_zhe_lei_xing='" + ddlzuo_zhe_lei_xing.SelectedValue + "'");
            }

            //科室
            if (ddldepartment.SelectedValue != "0")
            {
                strTemp.Append(" and dptid=" + ddldepartment.SelectedValue);
            }

            return strTemp.ToString();
        }
        #endregion

        #region 返回每页数量=============================
        private int GetPageSize(int _default_size)
        {
            int _pagesize;
            if (int.TryParse(Utils.GetCookie("productlist1_page_size", "DTcmsPage"), out _pagesize))
            {
                if (_pagesize > 0)
                {
                    return _pagesize;
                }
            }
            return _default_size;
        }
        #endregion

        //关健字查询
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect(Utils.CombUrlTxt("productlist1.aspx", "keywords={0}", txtKeywords.Text));
        }

        //设置分页数量
        protected void txtPageNum_TextChanged(object sender, EventArgs e)
        {
            int _pagesize;
            if (int.TryParse(txtPageNum.Text.Trim(), out _pagesize))
            {
                if (_pagesize > 0)
                {
                    Utils.WriteCookie("productlist1_page_size", "DTcmsPage", _pagesize.ToString(), 14400);
                }
            }
            Response.Redirect(Utils.CombUrlTxt("productlist1.aspx", "keywords={0}", this.keywords));
        }

        protected string GetStatus(string status)
        {
            if (status == "1")
            {
                return "通过";
            }
            else if (status == "-1")
            {
                return "未通过";
            }
            else
            {
                return "--";
            }
        }

        protected void ddlqi_kan_ji_bie_SelectedIndexChanged(object sender, EventArgs e)
        {
            //期刊级别
            RptBind("1=1 " + CombSqlTxt(keywords), "add_time desc");
        }

        protected void zuo_zhe_lei_xing_SelectedIndexChanged(object sender, EventArgs e)
        {
            //作者类型
            RptBind("1=1 " + CombSqlTxt(keywords), "add_time desc");
        }

        protected void ddldepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            // 按部门查询
            RptBind("1=1 " + CombSqlTxt(keywords), "add_time desc");
        }
    }
}