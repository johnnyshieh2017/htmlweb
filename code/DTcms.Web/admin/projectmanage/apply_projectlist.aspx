﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="apply_projectlist.aspx.cs" Inherits="DTcms.Web.admin.projectmanage.apply_projectlist" %>

<%@ Import Namespace="DTcms.Common" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,initial-scale=1.0,user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <title>申报管理</title>
    <link rel="stylesheet" type="text/css" href="../../scripts/artdialog/ui-dialog.css" />
    <link rel="stylesheet" type="text/css" href="../../css/pagination.css" />
    <link rel="stylesheet" type="text/css" href="../skin/icon/iconfont.css" />
    <link rel="stylesheet" type="text/css" href="../skin/default/style.css" />
    <script type="text/javascript" src="../../scripts/jquery/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="../../scripts/artdialog/dialog-plus-min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/laymain.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/common.js"></script>

    <script type="text/javascript">


        $(document).ready(function () {
                
        });

        function create_file(paid, thisObj) {
            $.ajax({
                type: "post",
                dataType: "json",

                data: { pa_id: paid},//注意：data参数可以是string个int类型

                url: "ajax.ashx?action=create_projectpdf",//模拟web服务，提交到方法
                // 可选的 async:false,阻塞的异步就是同步
                beforeSend: function () {
                    // do something.
                    // 一般是禁用按钮等防止用户重复提交
                    // $("#btnClick").attr({disabled:"disabled"});
                    // 或者是显示loading图片
                },
                success: function (data) {
                    //alert("success: " + data);//注意这里：必须通过data.d才能获取到服务器返回的值
                    if (data.ResultCode == "1") {
                        console.log(data.ResultMessage);

                        parent.jsprint("生成成功!", "");
                        //window.open(data.ResultMessage);
                        $(thisObj).attr("href", data.ResultMessage);
                        $(thisObj).attr("target","_blank");
                        $(thisObj).attr("click", "");
                        $(thisObj).text("下载");
                    }
                    else {
                        parent.jsprint("生成失败!请稍后再试。", "");
                    }


                },
                complete: function () {
                    //do something.
                    //$("#btnClick").removeAttr("disabled");
                    // 隐藏loading图片


                },
                error: function (data) {

                    //alert("error: " + data);
                    parent.jsprint("服务器异常!error:" + data, "");
                },

            });
        }
    </script>
</head>

<body class="mainbody">
    <form id="form1" runat="server">
        <!--导航栏-->
        <div class="location">
            <a href="javascript:history.back(-1);" class="back"><i class="iconfont icon-up"></i><span>返回上一页</span></a>
            <a href="../center.aspx" class="home" ><i class="iconfont icon-home"></i><span>首页</span></a>
            <i class="arrow iconfont icon-arrow-right"></i>
            <span>申报管理</span>
        </div>
        <!--/导航栏-->

        <!--工具栏-->
        <div id="floatHead" class="toolbar-wrap">
            <div class="toolbar">
                <div class="box-wrap">
                    <a class="menu-btn"><i class="iconfont icon-more"></i></a>
                    <div class="l-list">
                        <ul class="icon-list">
                            <li><a href="apply_projectedit.aspx?action=<%=DTEnums.ActionEnum.Add %>"><i class="iconfont icon-close"></i><span>新增</span></a></li>
                            <li style="display: none"><a href="javascript:;" onclick="checkAll(this);"><i class="iconfont icon-check"></i><span>全选</span></a></li>

                        </ul>
                        <div class="menu-list">

                            <div class="rule-single-select">
                                <asp:DropDownList ID="ddlProperty" runat="server" OnSelectedIndexChanged="ddlProperty_SelectedIndexChanged" AutoPostBack="true">
                                    <asp:ListItem Value="0" Selected="True">所有状态</asp:ListItem>
                                    <asp:ListItem Value="2">已审核</asp:ListItem>
                                    <asp:ListItem Value="1">未审核</asp:ListItem>

                                </asp:DropDownList>
                            </div>

                            <div class="rule-single-select">
                                <asp:DropDownList ID="ddldepartment" runat="server" OnSelectedIndexChanged="ddldepartment_SelectedIndexChanged" AutoPostBack="true">
                                    <asp:ListItem Value="0" Selected="True">所有科室</asp:ListItem>

                                </asp:DropDownList>
                            </div>

                            <div class="rule-single-select">
                                <asp:DropDownList ID="ddlprojecttype" runat="server" OnSelectedIndexChanged="ddlprojecttype_SelectedIndexChanged" AutoPostBack="true" Visible="false">
                                    <asp:ListItem Value="0" Selected="True">所有项目类别</asp:ListItem>

                                </asp:DropDownList>
                            </div>

                            <div class="rule-single-select">
                                <asp:DropDownList ID="ddlprojectcategory" runat="server" OnSelectedIndexChanged="ddlprojectcategory_SelectedIndexChanged" AutoPostBack="true" >
                                    <asp:ListItem Value="0" Selected="True">所有项目类别</asp:ListItem>

                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="r-list">
                        <asp:TextBox ID="txtKeywords" runat="server" CssClass="keyword" />
                        <asp:LinkButton ID="lbtnSearch" runat="server" CssClass="btn-search" OnClick="btnSearch_Click"><i class="iconfont icon-search"></i></asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
        <!--/工具栏-->

        <!--列表-->
        <div class="table-container">
            <asp:Repeater ID="rptList" runat="server">
                <HeaderTemplate>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="ltable">
                        <tr>
                            <th width="8%" style="display: none">选择</th>



                              <th align="center" width="3%">编号</th>
                            <th align="center" width="8%">申报项目名称</th>
                         
                             <th align="center" width="7%">科研类别</th>
                            <th align="center" width="7%">所属科室</th>

                            <th align="center" width="7%">申报人</th>
                           
                            <th align="center" width="7%">申报时间</th>
                            <th align="center" width="7%">项目基本情况</th>
                             <th align="center" width="7%">项目人员情况</th>
                              <th align="center" width="7%">项目经费情况</th>
                             <th align="center" width="7%">项目进度计划</th>
                             <th align="center" width="7%">项目绩效目标</th>
                              <th align="center" width="7%">项目可行性研究报告</th>
                            <th align="center" width="7%">状态</th>
                            <th width="7%">操作</th>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td align="center" style="display: none">
                            <asp:CheckBox ID="chkId" CssClass="checkall" runat="server" Style="vertical-align: middle;" />
                            <asp:HiddenField ID="hidId" Value='<%#Eval("pa_id")%>' runat="server" />
                        </td>
                        <td align="center"><%# Eval("pa_id") %></td>
                        <td align="center"><%# Eval("pa_title") %></td>
                    
                        <td align="center"><%#DTcms.Web.admin.projectmanage.data_tool.Getproject_category(Eval("pa_type").ToString())%></td>
                        <td align="center"><%# Eval("hospitalname") %></td>
                        <td align="center"><%# Eval("pa_user") %></td>
                        <td align="center"><%# Eval("shen_qing_ri_qi") %></td>

                         <td align="center"><a href="apply_projectdetail2.aspx?action=<%#DTEnums.ActionEnum.View %>&id=<%#Eval("pa_id")%>">查看</a></td>
                         <td align="center"><a href="apply_project_team.aspx?action=<%#DTEnums.ActionEnum.Edit %>&id=<%#Eval("pa_id")%>">编辑</a></td>
                         <td align="center"><a href="apply_project_fee.aspx?action=<%#DTEnums.ActionEnum.Edit %>&id=<%#Eval("pa_id")%>">编辑</a></td>
                         <td align="center"><a href="apply_project_plan.aspx?action=<%#DTEnums.ActionEnum.Edit %>&id=<%#Eval("pa_id")%>">编辑</a></td>
                         <td align="center"><a href="apply_project_kpi.aspx?action=<%#DTEnums.ActionEnum.Edit %>&id=<%#Eval("pa_id")%>">编辑</a></td>
                         <td align="center"><a href="apply_project_kyreport.aspx?action=<%#DTEnums.ActionEnum.Edit %>&id=<%#Eval("pa_id")%>">编辑</a></td>

                        <td align="center"><%# Eval("status").ToString()=="1"?"已审核":"未审核" %></td>
                      
                        <td align="center">
                            <a href="apply_projectedit.aspx?action=<%#DTEnums.ActionEnum.Edit %>&id=<%#Eval("pa_id")%>">修改</a>
                            <a href="apply_projectaudit.aspx?action=<%#DTEnums.ActionEnum.Audit %>&id=<%#Eval("pa_id")%>">审核</a>
                            <br></br>
                            <a href="apply_projectdetail2.aspx?action=<%#DTEnums.ActionEnum.View %>&id=<%#Eval("pa_id")%>">基本情况</a>
                           
                            <a href="#" id='apfile_<%#Eval("pa_id")%>' onclick="create_file(<%#Eval("pa_id")%>,this)">导出PDF</a>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    <%#rptList.Items.Count == 0 ? "<tr><td align=\"center\" colspan=\"8\">暂无记录</td></tr>" : ""%>
  </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
        <!--/列表-->

        <!--内容底部-->
        <div class="line20"></div>
        <div class="pagelist">
            <div class="l-btns">
                <span>显示</span><asp:TextBox ID="txtPageNum" runat="server" CssClass="pagenum" onkeydown="return checkNumber(event);"
                    OnTextChanged="txtPageNum_TextChanged" AutoPostBack="True"></asp:TextBox><span>条/页</span>
            </div>
            <div id="PageContent" runat="server" class="default"></div>
        </div>
        <!--/内容底部-->
    </form>
</body>
</html>