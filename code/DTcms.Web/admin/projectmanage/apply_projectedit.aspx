﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="apply_projectedit.aspx.cs" Inherits="DTcms.Web.admin.projectmanage.apply_projectedit" %>

<%@ Import Namespace="DTcms.Common" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>编辑</title>
    <meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,initial-scale=1.0,user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <link rel="stylesheet" type="text/css" href="../../scripts/artdialog/ui-dialog.css" />
    <link rel="stylesheet" type="text/css" href="../skin/icon/iconfont.css" />
    <link rel="stylesheet" type="text/css" href="../skin/default/style.css" />
    <script type="text/javascript" charset="utf-8" src="../../scripts/jquery/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../scripts/jquery/Validform_v5.3.2_min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../scripts/datepicker/WdatePicker.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../scripts/artdialog/dialog-plus-min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../scripts/webuploader/webuploader.min.js"></script>

    <script type="text/javascript" charset="utf-8" src="../../editor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../editor/ueditor.all.min.js"> </script>
    <script type="text/javascript" charset="utf-8" src="../../editor/lang/zh-cn/zh-cn.js"></script>

    <script type="text/javascript" charset="utf-8" src="../js/uploader.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/common.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/laymain.js"></script>
    <script type="text/javascript">
        $(function () {
            //初始化表单验证
            $("#form1").initValidform();
            //初始化上传控件
            $(".upload-img").InitUploader({
                filesize: "<%=sysConfig.attachsize %>", sendurl: "../../tools/upload_ajax.ashx", swf: "../../scripts/webuploader/uploader.swf", filetypes: "<%=sysConfig.fileextension %>"
            });

            //初始化editor
            $(".editor-mini").each(function (i) {
                var objId = $(this).attr("id");
                if (objId != "undefined") {
                    var editorMini = UE.getEditor(objId, {
                        serverUrl: '../../../tools/upload_ajax.ashx',
                        initialFrameWidth: '100%',
                        initialFrameHeight: 220,
                        toolbars: [[
                            'fullscreen', 'source', '|', 'undo', 'redo', '|',
                            'bold', 'italic', 'underline', 'strikethrough', 'removeformat', 'pasteplain', '|', 'forecolor', 'insertorderedlist', 'insertunorderedlist', '|',
                            'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|',
                            'link', 'unlink', 'anchor', '|',
                            'simpleupload', 'insertimage', 'scrawl', 'insertvideo'
                        ]]
                    });
                }
            });

            //创建上传附件
            $(".attach-btn").click(function () {
                showAttachDialog();
            });

        });


        //初始化附件窗口
        function showAttachDialog(obj) {
            var objNum = arguments.length;
            var attachDialog = top.dialog({
                id: 'attachDialogId',
                title: "上传附件",
                url: 'projectmanage/dialog_attach.aspx',
                width: 500,
                height: 180,
                onclose: function () {
                    var liHtml = this.returnValue; //获取返回值
                    if (liHtml.length > 0) {
                        $("#showAttachList").children("ul").append(liHtml);
                    }
                }
            }).showModal();
            //如果是修改状态，将对象传进去
            if (objNum == 1) {
                attachDialog.data = obj;
            }
        }
        //删除附件节点
        function delAttachNode(obj) {
            $(obj).parent().remove();
        }
    </script>
</head>

<body class="mainbody">
    <form id="form1" runat="server">
        <!--导航栏-->
        <div class="location">
            <a href="manager_list.aspx" class="back"><i class="iconfont icon-up"></i><span>返回列表页</span></a>
            <a href="../center.aspx"><i class="iconfont icon-home"></i><span>首页</span></a>
            <i class="arrow iconfont icon-arrow-right"></i>
            <a href="doc_list.aspx"><span>申报管理</span></a>
            <i class="arrow iconfont icon-arrow-right"></i>
            <span>申报项目</span>
        </div>
        <div class="line10"></div>
        <!--/导航栏-->

        <!--内容-->
        <div id="floatHead" class="content-tab-wrap">
            <div class="content-tab">
                <div class="content-tab-ul-wrap">
                    <ul>
                        <li><a class="selected" href="javascript:;">申报信息</a></li>
                        <li><a href="javascript:;">基本情况</a></li>
                        <li><a href="javascript:;">项目概况</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="tab-content">



            <dl>
                <dt>申报项目名称</dt>
                <dd>
                    <asp:TextBox ID="txt_title" runat="server" CssClass="input normal" datatype="*2-50"></asp:TextBox>

                    <span class="Validform_checktip">*</span>
                </dd>
            </dl>
            <dl>
                <dt>项目申报编号</dt>
                <dd>
                    <asp:TextBox ID="txt_number" runat="server" CssClass="input normal" datatype="*2-50"></asp:TextBox>

                    <span class="Validform_checktip">*</span>
                </dd>
            </dl>
            <dl>
                <dt>科研类型</dt>
                <dd>
                    <div class="rule-single-select">
                        <asp:DropDownList ID="ddl_projecttype" runat="server" datatype="n" errormsg="请选择类型" sucmsg=" "  >
                            <asp:ListItem Text="请选择" Value="0"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </dd>
            </dl>
            <dl>
                <dt>所属科室</dt>
                <dd>
                    <div class="rule-single-select">
                        <asp:DropDownList ID="ddlhospital" runat="server" datatype="n" errormsg="请选择科室" sucmsg=" "  OnSelectedIndexChanged="ddlhospital_SelectedIndexChanged">
                            <asp:ListItem Text="请选择" Value="0"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </dd>
            </dl>


            <dl>
                <dt>项目负责人</dt>
                <dd>
                    <asp:TextBox ID="txt_owner" runat="server" CssClass="input normal" datatype="*2-50"></asp:TextBox>

                    <span class="Validform_checktip">*</span>
                </dd>
            </dl>
            <dl>
                <dt>手机号码</dt>
                <dd>
                    <asp:TextBox ID="txt_shou_ji_hao_ma" runat="server" CssClass="input normal" datatype="*2-50"></asp:TextBox>

                    <span class="Validform_checktip">*</span>
                </dd>
            </dl>
             <dl>
                <dt>电子邮箱</dt>
                <dd>
                    <asp:TextBox ID="txt_dian_zi_you_xiang" runat="server" CssClass="input normal" datatype="*2-50"></asp:TextBox>

                    <span class="Validform_checktip">*</span>
                </dd>
            </dl>
            <dl>
                <dt>申请日期</dt>
                <dd>
                    <asp:TextBox ID="txt_shen_qing_ri_qi" runat="server" CssClass="input normal" datatype="*2-50"></asp:TextBox>

                    <span class="Validform_checktip">*</span>
                </dd>
            </dl>

        </div>
        <div class="tab-content" style="display: none">
            <dl>
                <dt>项目实时地点</dt>
                <dd>
                    <asp:TextBox ID="txt_base_didian" runat="server" CssClass="input normal" datatype="*2-50"></asp:TextBox>

                    <span class="Validform_checktip">*</span>
                </dd>
            </dl>
            <dl>
                <dt>研究形式</dt>
                <dd>
                    <div class="rule-single-select">
                        <asp:DropDownList ID="ddl_base_yanjiu_xingshi" runat="server" datatype="n" errormsg="请选择形式" sucmsg=" ">
                            <asp:ListItem Text="请选择" Value=""></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </dd>
            </dl>
            <dl>
                <dt>项目起止日期</dt>
                <dd>
                    <asp:TextBox ID="txt_base_qizhishijian" runat="server" CssClass="input small" datatype="*2-50"></asp:TextBox>
                    <asp:TextBox ID="txt_base_qizhishijian_end" runat="server" CssClass="input small" datatype="*2-50"></asp:TextBox>

                    <span class="Validform_checktip">*</span>
                </dd>
            </dl>
            <dl>
                <dt>单位名称</dt>
                <dd>
                    <asp:TextBox ID="txt_base_hezuo_danwei" runat="server" CssClass="input normal" datatype="*2-50"></asp:TextBox>

                    <span class="Validform_checktip">*</span>
                </dd>
            </dl>
            <dl>
                <dt>单位地址</dt>
                <dd>
                    <asp:TextBox ID="txt_base_hezuo_dizhi" runat="server" CssClass="input normal" datatype="*2-50"></asp:TextBox>

                    <span class="Validform_checktip">*</span>
                </dd>
            </dl>
            <dl>
                <dt>联系人</dt>
                <dd>
                    <asp:TextBox ID="txt_base_hezuo_lianxiren" runat="server" CssClass="input normal" datatype="*2-50"></asp:TextBox>

                    <span class="Validform_checktip">*</span>
                </dd>
            </dl>
            <dl>
                <dt>联系电话</dt>
                <dd>
                    <asp:TextBox ID="txt_base_hezuo_lianxidianhua" runat="server" CssClass="input normal" datatype="*2-50"></asp:TextBox>

                    <span class="Validform_checktip">*</span>
                </dd>
            </dl>
        </div>


        <div class="tab-content" style="display: none">
            <dl>
                <dt>项目摘要</dt>
                <dd>
                    <asp:TextBox ID="txt_base_xmgk_zhaiyao" runat="server" CssClass="input" TextMode="MultiLine" Width="540" Height="280" />
                    <span class="Validform_checktip">限500字</span>
                </dd>
            </dl>
            <dl>
                <dt>主要研究内容与目标</dt>
                <dd>
                    <asp:TextBox ID="txt_base_xmgk_neirongmubiao" runat="server" CssClass="input" TextMode="MultiLine" Width="540" Height="280" />
                    <span class="Validform_checktip">限500字</span>
                </dd>
            </dl>
             <dl>
                <dt>技术路线</dt>
                <dd>
                    <asp:TextBox ID="txt_base_xmgk_jishuluxian" runat="server" CssClass="input" TextMode="MultiLine" Width="540" Height="280" />
                    <span class="Validform_checktip">限500字</span>
                </dd>
            </dl>
             <dl>
                <dt>创新点与关键技术</dt>
                <dd>
                    <asp:TextBox ID="txt_base_xmgk_chuangxindian" runat="server" CssClass="input" TextMode="MultiLine" Width="540" Height="280" />
                    <span class="Validform_checktip">限500字</span>
                </dd>
            </dl>
            <dl>
                <dt>现有基础与实施条件</dt>
                <dd>
                    <asp:TextBox ID="txt_base_xmgk_jichutiaojian" runat="server" CssClass="input" TextMode="MultiLine" Width="540" Height="280" />
                    <span class="Validform_checktip">限500字</span>
                </dd>
            </dl>
             <dl>
                <dt>预期成果与经济、社会效益</dt>
                <dd>
                    <asp:TextBox ID="txt_base_xmgk_xiaoyi" runat="server" CssClass="input" TextMode="MultiLine" Width="540" Height="280" />
                    <span class="Validform_checktip">限500字</span>
                </dd>
            </dl>
             <dl>
                <dt>考核指标</dt>
                <dd>
                    <asp:TextBox ID="txt_base_xmgk_zhibiao" runat="server" CssClass="input" TextMode="MultiLine" Width="540" Height="280" />
                    <span class="Validform_checktip">限500字</span>
                </dd>
            </dl>
        </div>
        <!--/内容-->

        <!--工具栏-->
        <div class="page-footer">
            <div class="btn-wrap">
                <asp:Button ID="btnSubmit" runat="server" Text="提交保存" CssClass="btn" OnClick="btnSubmit_Click" />
                <input name="btnReturn" type="button" value="返回上一页" class="btn yellow" onclick="javascript: history.back(-1);" />
            </div>
        </div>
        <!--/工具栏-->

    </form>
</body>
</html>
