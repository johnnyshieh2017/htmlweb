﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DTcms.Common;

namespace DTcms.Web.admin.projectmanage
{
    public partial class productadd4 : Web.UI.ManagePage
    {
        private string action = DTEnums.ActionEnum.Add.ToString(); //操作类型
        private int id = 0;


        BLL.ky_products_lunzhu ky_Products_lunzhu_bll = new BLL.ky_products_lunzhu();

        BLL.ky_attachment attachment_bll = new BLL.ky_attachment();
        protected void Page_Load(object sender, EventArgs e)
        {
            string _action = DTRequest.GetQueryString("action");
            if (!string.IsNullOrEmpty(_action) && _action == DTEnums.ActionEnum.Edit.ToString())
            {
                this.action = DTEnums.ActionEnum.Edit.ToString();//修改类型
                if (!int.TryParse(Request.QueryString["id"] as string, out this.id))
                {
                    JscriptMsg("传输参数不正确！", "back");
                    return;
                }
                if (!ky_Products_lunzhu_bll.Exists(this.id))
                {
                    JscriptMsg("记录不存在或已被删除！", "back");
                    return;
                }
            }

            if (!IsPostBack)
            {
                ChkAdminLevel("productadd4", DTEnums.ActionEnum.View.ToString()); //检查权限
                Model.manager model = GetAdminInfo(); //取得管理员信息
                InitData();
                if (action == DTEnums.ActionEnum.Edit.ToString()) //修改
                {
                    ShowInfo(this.id);
                    ShowApplyInfo(this.id);
                }
            }
        }

        #region 初始化===================================

        void InitData()
        {
            var hospatil = new BLL.yjr_doc_hospital().GetModelList("");
            foreach (var item in hospatil)
            {
                ddl_dpt.Items.Add(new ListItem(item.hospitalname, item.id.ToString()));
            }

           

            var zuozheleixing = new BLL.ky_dictionary().GetModelListByCache("author_type");
            foreach (var item in zuozheleixing)
            {
                ddl_zuo_zhe_lei_xing.Items.Add(new ListItem(item.dic_text, item.dic_value.ToString()));

            }

        }
        #endregion

        #region 赋值操作=================================
        private void ShowInfo(int _id)
        {
            var model = ky_Products_lunzhu_bll.GetModel(_id);

            txt_shu_ji_ming_cheng.Text = model.shu_ji_ming_cheng;
            txt_chu_ban_she.Text = model.chu_ban_she;
            txt_chu_ban_ri_qi.Text = model.chu_ban_ri_qi;
            txt_zuo_zhe.Text = model.zuo_zhe;
            ddl_zuo_zhe_lei_xing.SelectedValue = model.zuo_zhe_lei_xing;
            txt_bian_xie_zhang_jie.Text = model.bian_xie_zhang_jie;
            ddl_dpt.SelectedValue = model.dptid.ToString();
           
            txt_beizhu.Text = model.beizhu;



        }

        private void ShowApplyInfo(int _id)
        {
            //绑定内容附件
            rptAttachList.DataSource = attachment_bll.GetModelList("apply_id='product_lunzhu_" + this.id + "' "); ;//  model.attach; //获取附件
            rptAttachList.DataBind();
        }
        #endregion

        #region 增加操作=================================
        private bool DoAdd()
        {
            DTcms.Model.ky_products_lunzhu model = new Model.ky_products_lunzhu();


            model.shu_ji_ming_cheng = txt_shu_ji_ming_cheng.Text.Trim();
            model.zuo_zhe_lei_xing = ddl_zuo_zhe_lei_xing.SelectedValue;
            model.dptid = int.Parse(ddl_dpt.SelectedValue);
            model.chu_ban_she = txt_chu_ban_she.Text.Trim();
            model.chu_ban_ri_qi = txt_chu_ban_ri_qi.Text.Trim();

            model.zuo_zhe = txt_zuo_zhe.Text.Trim();

            model.bian_xie_zhang_jie = txt_bian_xie_zhang_jie.Text.Trim();

            model.beizhu = txt_beizhu.Text;

            model.add_time = System.DateTime.Now;
            model.add_user = GetAdminInfo().user_name.ToString();

            var id = ky_Products_lunzhu_bll.Add(model);
            if (id > 0)
            {

                //
                #region 保存附件====================
                //保存附件
                string[] attachFileNameArr = Request.Form.GetValues("hid_attach_filename");
                string[] attachFilePathArr = Request.Form.GetValues("hid_attach_filepath");
                string[] attachFileSizeArr = Request.Form.GetValues("hid_attach_filesize");
                string[] attachPointArr = Request.Form.GetValues("txt_attach_point");
                if (attachFileNameArr != null && attachFilePathArr != null && attachFileSizeArr != null
                    && attachFileNameArr.Length > 0 && attachFilePathArr.Length > 0 && attachFileSizeArr.Length > 0)
                {
                    List<Model.article_attach> ls = new List<Model.article_attach>();
                    for (int i = 0; i < attachFileNameArr.Length; i++)
                    {
                        int fileSize = Utils.StrToInt(attachFileSizeArr[i], 0);
                        string fileExt = FileHelper.GetFileExt(attachFilePathArr[i]);
                        //int _point = Utils.StrToInt(attachPointArr[i], 0);
                        //ls.Add(new Model.article_attach { channel_id = this.channel_id, file_name = attachFileNameArr[i], file_path = attachFilePathArr[i], file_size = fileSize, file_ext = fileExt, point = _point });

                        //保存附件数据
                        DTcms.Model.ky_attachment _Attachment = new Model.ky_attachment();
                        _Attachment.file_name = attachFileNameArr[i];
                        _Attachment.file_path = attachFilePathArr[i];
                        _Attachment.file_ext = fileExt;
                        _Attachment.file_size = fileSize;
                        _Attachment.p_id = 0;
                        _Attachment.add_time = System.DateTime.Now;
                        _Attachment.apply_id = "product_lunzhu_" + id.ToString();

                        attachment_bll.Add(_Attachment);
                    }

                }
                #endregion

                //
                AddAdminLog(DTEnums.ActionEnum.Add.ToString(), "添加成果:" + id); //记录日志
                return true;
            }
            return false;
        }
        #endregion

        #region 修改操作=================================
        private bool DoEdit(int _id)
        {
            bool result = false;

            DTcms.Model.ky_products_lunzhu model_edit = ky_Products_lunzhu_bll.GetModel(_id);

            model_edit.shu_ji_ming_cheng = txt_shu_ji_ming_cheng.Text.Trim();
            model_edit.zuo_zhe_lei_xing = ddl_zuo_zhe_lei_xing.SelectedValue;
            model_edit.dptid = int.Parse(ddl_dpt.SelectedValue);
            model_edit.chu_ban_she = txt_chu_ban_she.Text.Trim();
            model_edit.chu_ban_ri_qi = txt_chu_ban_ri_qi.Text.Trim();

            model_edit.zuo_zhe = txt_zuo_zhe.Text.Trim();

            model_edit.bian_xie_zhang_jie = txt_bian_xie_zhang_jie.Text.Trim();

            model_edit.beizhu = txt_beizhu.Text;


            if (ky_Products_lunzhu_bll.Update(model_edit))
            {

                #region 修改并保存附件====================
                //修改并保存附件
                //清除原来的附件列表 再保存
                attachment_bll.Deletebyapply_id("product_lunzhu_" + _id.ToString());

                string[] attachFileNameArr = Request.Form.GetValues("hid_attach_filename");
                string[] attachFilePathArr = Request.Form.GetValues("hid_attach_filepath");
                string[] attachFileSizeArr = Request.Form.GetValues("hid_attach_filesize");
                string[] attachPointArr = Request.Form.GetValues("txt_attach_point");
                if (attachFileNameArr != null && attachFilePathArr != null && attachFileSizeArr != null
                    && attachFileNameArr.Length > 0 && attachFilePathArr.Length > 0 && attachFileSizeArr.Length > 0)
                {
                    List<Model.article_attach> ls = new List<Model.article_attach>();
                    for (int i = 0; i < attachFileNameArr.Length; i++)
                    {
                        int fileSize = Utils.StrToInt(attachFileSizeArr[i], 0);
                        string fileExt = FileHelper.GetFileExt(attachFilePathArr[i]);
                        //int _point = Utils.StrToInt(attachPointArr[i], 0);
                        //ls.Add(new Model.article_attach { channel_id = this.channel_id, file_name = attachFileNameArr[i], file_path = attachFilePathArr[i], file_size = fileSize, file_ext = fileExt, point = _point });

                        //保存附件数据
                        DTcms.Model.ky_attachment _Attachment = new Model.ky_attachment();
                        _Attachment.file_name = attachFileNameArr[i];
                        _Attachment.file_path = attachFilePathArr[i];
                        _Attachment.file_ext = fileExt;
                        _Attachment.file_size = fileSize;
                        _Attachment.p_id = 0;
                        _Attachment.add_time = System.DateTime.Now;
                        _Attachment.apply_id = "product_lunzhu_" + _id.ToString();

                        attachment_bll.Add(_Attachment);
                    }

                }
                #endregion

                AddAdminLog(DTEnums.ActionEnum.Add.ToString(), "修改成果:" + model_edit.pd_id); //记录日志
                result = true;
            }

            return result;
        }
        #endregion

        //保存
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (action == DTEnums.ActionEnum.Edit.ToString()) //修改
            {
                ChkAdminLevel("productlist4", DTEnums.ActionEnum.Edit.ToString()); //检查权限
                if (!DoEdit(this.id))
                {
                    JscriptMsg("保存过程中发生错误！", "");
                    return;
                }
                JscriptMsg("修改信息成功！", "productlist4.aspx");
            }
            else //添加
            {
                ChkAdminLevel("productlist4", DTEnums.ActionEnum.Add.ToString()); //检查权限
                if (!DoAdd())
                {
                    JscriptMsg("保存过程中发生错误！", "");
                    return;
                }
                JscriptMsg("添加信息成功！", "productlist4.aspx");
            }
        }



        protected void ddlhospital_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}