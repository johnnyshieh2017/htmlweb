﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DTcms.Common;


namespace DTcms.Web.admin.projectmanage
{
    public partial class apply_project_plan : Web.UI.ManagePage
    {
        private string action = DTEnums.ActionEnum.Edit.ToString(); //操作类型
        private int id = 0;

        BLL.ky_applyproject ky_applyproject_bll = new BLL.ky_applyproject();
        protected void Page_Load(object sender, EventArgs e)
        {
            string _action = DTRequest.GetQueryString("action");
            if (!string.IsNullOrEmpty(_action) && _action == DTEnums.ActionEnum.Edit.ToString())
            {
                this.action = DTEnums.ActionEnum.Edit.ToString();//修改类型
                if (!int.TryParse(Request.QueryString["id"] as string, out this.id))
                {
                    JscriptMsg("传输参数不正确！", "back");
                    return;
                }
                if (!ky_applyproject_bll.Exists(this.id))
                {
                    JscriptMsg("记录不存在或已被删除！", "back");
                    return;
                }
            }

            if (!IsPostBack)
            {
                ChkAdminLevel("apply_projectlist", DTEnums.ActionEnum.View.ToString()); //检查权限
                                                                                        // Model.manager model = GetAdminInfo(); //取得管理员信息
                
                if (action == DTEnums.ActionEnum.Edit.ToString()) //修改
                {
                    ShowInfo(this.id);
                }
            }
        }

        #region 赋值操作=================================
        private void ShowInfo(int _id)
        {
            var model = ky_applyproject_bll.GetModel(_id);
           
            txt_jindu_jihua.Text = model.jindu_jihua;

        }
        #endregion

        #region 修改操作=================================
        private bool DoEdit(int _id)
        {
            bool result = false;

            DTcms.Model.ky_applyproject model_edit = ky_applyproject_bll.GetModel(_id);
            model_edit.jindu_jihua = txt_jindu_jihua.Text;


            if (ky_applyproject_bll.Update(model_edit))
            {
                AddAdminLog(DTEnums.ActionEnum.Add.ToString(), "修改apply_project进度计划:" + model_edit.pa_title); //记录日志
                result = true;
            }

            return result;
        }
        #endregion


        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            // 
            ChkAdminLevel("apply_projectlist", DTEnums.ActionEnum.Edit.ToString()); //检查权限
            if (!DoEdit(this.id))
            {
                JscriptMsg("保存过程中发生错误！", "");
                return;
            }
            JscriptMsg("保存信息成功！", "apply_projectlist.aspx");
 
        }
    }
}