﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="apply_projectdetail.aspx.cs" Inherits="DTcms.Web.admin.projectmanage.apply_projectdetail" %>

<%@ Import Namespace="DTcms.Common" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>查看</title>
    <meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,initial-scale=1.0,user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <link rel="stylesheet" type="text/css" href="../../scripts/artdialog/ui-dialog.css" />
    <link rel="stylesheet" type="text/css" href="../skin/icon/iconfont.css" />
    <link rel="stylesheet" type="text/css" href="../skin/default/style.css" />
    <script type="text/javascript" charset="utf-8" src="../../scripts/jquery/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../scripts/jquery/Validform_v5.3.2_min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../scripts/datepicker/WdatePicker.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../scripts/artdialog/dialog-plus-min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../scripts/webuploader/webuploader.min.js"></script>

    <script type="text/javascript" charset="utf-8" src="../../editor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../editor/ueditor.all.min.js"> </script>
    <script type="text/javascript" charset="utf-8" src="../../editor/lang/zh-cn/zh-cn.js"></script>

    <script type="text/javascript" charset="utf-8" src="../js/uploader.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/common.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/laymain.js"></script>
    <script type="text/javascript">
        $(function () {
            //初始化表单验证
            $("#form1").initValidform();
            //初始化上传控件
            $(".upload-img").InitUploader({
                filesize: "<%=sysConfig.attachsize %>", sendurl: "../../tools/upload_ajax.ashx", swf: "../../scripts/webuploader/uploader.swf", filetypes: "<%=sysConfig.fileextension %>"});

            //初始化editor
            $(".editor-mini").each(function (i) {
                var objId = $(this).attr("id");
                if (objId != "undefined") {
                    var editorMini = UE.getEditor(objId, {
                        serverUrl: '../../../tools/upload_ajax.ashx',
                        initialFrameWidth: '100%',
                        initialFrameHeight: 220,
                        toolbars: [[
                            'fullscreen', 'source', '|', 'undo', 'redo', '|',
                            'bold', 'italic', 'underline', 'strikethrough', 'removeformat', 'pasteplain', '|', 'forecolor', 'insertorderedlist', 'insertunorderedlist', '|',
                            'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|',
                            'link', 'unlink', 'anchor', '|',
                            'simpleupload', 'insertimage', 'scrawl', 'insertvideo'
                        ]]
                    });
                }
            });

            //创建上传附件
            $(".attach-btn").click(function () {
                showAttachDialog();
            });

        });


        //初始化附件窗口
        function showAttachDialog(obj) {
            var objNum = arguments.length;
            var attachDialog = top.dialog({
                id: 'attachDialogId',
                title: "上传附件",
                url: 'projectmanage/dialog_attach.aspx',
                width: 500,
                height: 180,
                onclose: function () {
                    var liHtml = this.returnValue; //获取返回值
                    if (liHtml.length > 0) {
                        $("#showAttachList").children("ul").append(liHtml);
                    }
                }
            }).showModal();
            //如果是修改状态，将对象传进去
            if (objNum == 1) {
                attachDialog.data = obj;
            }
        }
        //删除附件节点
        function delAttachNode(obj) {
            $(obj).parent().remove();
        }
    </script>
</head>

<body class="mainbody">
    <form id="form1" runat="server">
        <!--导航栏-->
        <div class="location">
            <a href="manager_list.aspx" class="back"><i class="iconfont icon-up"></i><span>返回列表页</span></a>
            <a href="../center.aspx"><i class="iconfont icon-home"></i><span>首页</span></a>
            <i class="arrow iconfont icon-arrow-right"></i>
            <a href="doc_list.aspx"><span>申报管理</span></a>
            <i class="arrow iconfont icon-arrow-right"></i>
            <span>申报项目</span>
        </div>
        <div class="line10"></div>
        <!--/导航栏-->

        <!--内容-->
        <div id="floatHead" class="content-tab-wrap">
            <div class="content-tab">
                <div class="content-tab-ul-wrap">
                    <ul>
                        <li><a class="selected" href="javascript:;">申报信息</a></li>
                        <li><a href="javascript:;" >项目资料</a></li>
                        <li><a href="javascript:;">附件</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="tab-content">



            <dl>
                <dt>申报项目名称</dt>
                <dd>
                    <asp:TextBox ID="txt_title" runat="server" CssClass="input normal" datatype="*2-50" ReadOnly="true"></asp:TextBox>

                    <span class="Validform_checktip">*</span>
                </dd>
            </dl>
            <dl>
                <dt>项目申报编号</dt>
                <dd>
                    <asp:TextBox ID="txt_number" runat="server" CssClass="input normal" datatype="*2-50" ReadOnly="true"></asp:TextBox>

                    <span class="Validform_checktip">*</span>
                </dd>
            </dl>
            <dl>
                <dt>科研类型</dt>
                <dd>
                    <div class="rule-single-select">
                        <asp:DropDownList ID="ddl_projecttype" runat="server" datatype="n" errormsg="请选择科室" sucmsg=" " >
                            <asp:ListItem Text="请选择" Value="0"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </dd>
            </dl>
            <dl>
                <dt>所属科室</dt>
                <dd>
                    <div class="rule-single-select">
                        <asp:DropDownList ID="ddlhospital" runat="server" datatype="n" errormsg="请选择科室" sucmsg=" " >
                            <asp:ListItem Text="请选择" Value="0"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </dd>
            </dl>


            <dl>
                <dt>申报人</dt>
                <dd>
                    <asp:TextBox ID="txt_owner" runat="server" CssClass="input normal" datatype="*2-50" ReadOnly="true"></asp:TextBox>

                    <span class="Validform_checktip">*</span>
                </dd>
            </dl>

        </div>
        <div class="tab-content" style="display: none">
            <dl>
                <dt>单位基本信息表</dt>
                <dd>
                    <asp:TextBox ID="txtOrgbaseinfo" runat="server" CssClass="input normal upload-path" Visible="false"  />

                    <a id="hf_txtOrgbaseinfo" target="_blank" runat="server">查看</a>
                </dd>
            </dl>

             <dl>
                <dt>项目基本信息表</dt>
                <dd>
                    <asp:TextBox ID="txtBaseinfo" runat="server" CssClass="input normal upload-path" Visible="false" />
                   <a id="hf_txtBaseinfo" target="_blank" runat="server">查看</a>
                </dd>
            </dl>
             <dl>
                <dt>项目团队信息表</dt>
                <dd>
                    <asp:TextBox ID="txtTeamInfo" runat="server" CssClass="input normal upload-path" Visible="false"  />
                   <a id="hf_txtTeamInfo" target="_blank" runat="server">查看</a>
                </dd>
            </dl>
            <dl>
                <dt>项目概况及相符性说明</dt>
                <dd>
                    <asp:TextBox ID="txtGaikuang" runat="server" CssClass="input" TextMode="MultiLine" ReadOnly="true" />

                </dd>
            </dl>
              <dl>
                <dt>实施方案及进度计划</dt>
                <dd>
                    <asp:TextBox ID="txtShishi" runat="server" CssClass="input" TextMode="MultiLine" ReadOnly="true" />
                </dd>
            </dl>
             <dl>
                <dt>总投资及经费使用计划</dt>
                <dd>
                    <asp:TextBox ID="txtTouzi" runat="server" CssClass="input" TextMode="MultiLine" ReadOnly="true" />
                </dd>
            </dl>
             <dl>
                <dt>预期绩效目标</dt>
                <dd>
                    <asp:TextBox ID="txtJixiao" runat="server" CssClass="input" TextMode="MultiLine" ReadOnly="true" />
                </dd>
            </dl>
        </div>
        <div class="tab-content" style="display: none">
            <dl id="div_attach_container" runat="server" visible="true">
                <dt>上传附件</dt>
                <dd>
                    <a class="icon-btn attach-btn"><i class="iconfont icon-add"></i><span>添加附件</span></a>
                    <div id="showAttachList" class="attach-list">
                        <ul>
                            <asp:Repeater ID="rptAttachList" runat="server">
                                <ItemTemplate>
                                    <li>
                                        <input name="hid_attach_id" type="hidden" value="<%#Eval("am_id")%>" />
                                        <input name="hid_attach_filename" type="hidden" value="<%#Eval("file_name")%>" />
                                        <input name="hid_attach_filepath" type="hidden" value="<%#Eval("file_path")%>" />
                                        <input name="hid_attach_filesize" type="hidden" value="<%#Eval("file_size")%>" />
                                        <i class="iconfont icon-attachment"></i>
                                        <a href="javascript:;" onclick="delAttachNode(this);" class="del" style="display:none" title="删除附件"><i class="iconfont icon-remove"></i></a>
                                        <a href="javascript:;" onclick="showAttachDialog(this);" class="edit" style="display:none"  title="更新附件"><i class="iconfont icon-pencil"></i></a>
                                        <div class="title"><a target="_blank" href="<%#Eval("file_path")%>"><%#Eval("file_name")%></a></div>
                                        <div class="info">
                                        <div class="info">
                                            类型：<span class="ext"><%#Eval("file_ext")%></span> 大小：<span class="size"><%#Convert.ToInt32(Eval("file_size")) > 1024 ? Convert.ToDouble((Convert.ToDouble(Eval("file_size")) / 1024f)).ToString("0.0") + "MB" : Eval("file_size") + "KB"%></span>
                                            <%--下载：<span class="down"><%#Eval("down_num")%></span>次</div>
                                        <div class="btns">下载积分：<input type="text" name="txt_attach_point" onkeydown="return checkNumber(event);" value="<%#Eval("point")%>" />--%>
                                        </div>
                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ul>
                    </div>
                </dd>
            </dl>

            <dl>
                <dt>备注</dt>
                <dd>
                  
                     <asp:TextBox ID="txt_intro" runat="server" CssClass="input" TextMode="MultiLine" ReadOnly="true" />
                </dd>
            </dl>
        </div>
        <!--/内容-->

        <!--工具栏-->
        <div class="page-footer">
            <div class="btn-wrap">
                <asp:Button ID="btnSubmit" runat="server" Text="审批通过" CssClass="btn" OnClick="btnSubmit_Click" Visible="false" />
                <input name="btnReturn" type="button" value="返回列表" class="btn yellow" onclick="javascript: history.back(-1);" />
            </div>
        </div>
        <!--/工具栏-->

    </form>
</body>
</html>

