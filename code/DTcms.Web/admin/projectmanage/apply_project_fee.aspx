﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="apply_project_fee.aspx.cs" Inherits="DTcms.Web.admin.projectmanage.apply_project_fee" %>

<%@ Import Namespace="DTcms.Common" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>项目经费情况</title>
    <meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,initial-scale=1.0,user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <link rel="stylesheet" type="text/css" href="../../scripts/artdialog/ui-dialog.css" />
    <link rel="stylesheet" type="text/css" href="../skin/icon/iconfont.css" />
    <link rel="stylesheet" type="text/css" href="../skin/default/style.css" />
    <style>
        .tdcenter {
            text-align: center;
        }
        .wordblod {
        font-weight: bold;
        }
    </style>
    <script type="text/javascript" charset="utf-8" src="../../scripts/jquery/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../scripts/jquery/Validform_v5.3.2_min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../scripts/datepicker/WdatePicker.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../scripts/artdialog/dialog-plus-min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../scripts/webuploader/webuploader.min.js"></script>

    <script type="text/javascript" charset="utf-8" src="../../editor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../editor/ueditor.all.min.js"> </script>
    <script type="text/javascript" charset="utf-8" src="../../editor/lang/zh-cn/zh-cn.js"></script>

    <script type="text/javascript" charset="utf-8" src="../js/uploader.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/common.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/laymain.js"></script>
    <script type="text/javascript">
        $(function () {
            //初始化表单验证
            $("#form1").initValidform();
            //初始化上传控件
            $(".upload-img").InitUploader({
                filesize: "<%=sysConfig.attachsize %>", sendurl: "../../tools/upload_ajax.ashx", swf: "../../scripts/webuploader/uploader.swf", filetypes: "<%=sysConfig.fileextension %>"
            });

            //初始化editor
            $(".editor-mini").each(function (i) {
                var objId = $(this).attr("id");
                if (objId != "undefined") {
                    var editorMini = UE.getEditor(objId, {
                        serverUrl: '../../../tools/upload_ajax.ashx',
                        initialFrameWidth: '100%',
                        initialFrameHeight: 220,
                        toolbars: [[
                            'fullscreen', 'source', '|', 'undo', 'redo', '|',
                            'bold', 'italic', 'underline', 'strikethrough', 'removeformat', 'pasteplain', '|', 'forecolor', 'insertorderedlist', 'insertunorderedlist', '|',
                            'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|',
                            'link', 'unlink', 'anchor', '|',
                            'simpleupload', 'insertimage', 'scrawl', 'insertvideo'
                        ]]
                    });
                }
            });

            //创建上传附件
            $(".attach-btn").click(function () {
                showAttachDialog();
            });

        });


        //初始化附件窗口
        function showAttachDialog(obj) {
            var objNum = arguments.length;
            var attachDialog = top.dialog({
                id: 'attachDialogId',
                title: "上传附件",
                url: 'projectmanage/dialog_attach.aspx',
                width: 500,
                height: 180,
                onclose: function () {
                    var liHtml = this.returnValue; //获取返回值
                    if (liHtml.length > 0) {
                        $("#showAttachList").children("ul").append(liHtml);
                    }
                }
            }).showModal();
            //如果是修改状态，将对象传进去
            if (objNum == 1) {
                attachDialog.data = obj;
            }
        }
        //删除附件节点
        function delAttachNode(obj) {
            $(obj).parent().remove();
        }

        function add_equipment()
        {

            var shebei = $("#txt_shebei").val();
            if (shebei == "") {
                alert('请输入设备名称！');
                $("#txt_shebei").focus();
                return;
            }

          

            var xinghao = $("#txt_xinghao").val();
            if (xinghao == "") {
                alert('请输入设备型号！');
                $("#txt_xinghao").focus();
                return;
            }

         

            var jiage = $("#txt_jiage").val();
            if (jiage == "") {
                alert('请输入设备价格！');
                $("#txt_jiage").focus();
                return;
            }

            var tianjia_liyou = $("#txt_tianjia_liyou").val();
            if (tianjia_liyou == "") {
                alert('请输入增加理由！');
                $("#txt_tianjia_liyou").focus();
                return;
            }

           

            if ($("#btnAdd").attr("title") != "") {
                //alert('编辑完成' + $("#btnAdd").attr("title"));



                $.ajax({
                    type: "post",
                    dataType: "json",

                    data: { shebei: shebei, xinghao: xinghao, jiage: jiage, tianjia_liyou: tianjia_liyou, add_user: $("#hd_add_user").val(), id: $("#btnAdd").attr("title")},//注意：data参数可以是string个int类型

                    url: "ajax.ashx?action=edit_equipment",//模拟web服务，提交到方法
                    // 可选的 async:false,阻塞的异步就是同步
                    beforeSend: function () {
                        // do something.
                        // 一般是禁用按钮等防止用户重复提交
                        // $("#btnClick").attr({disabled:"disabled"});
                        // 或者是显示loading图片
                    },
                    success: function (data) {
                        //alert("success: " + data);//注意这里：必须通过data.d才能获取到服务器返回的值
                        if (data.ResultCode == "1") {
                            console.log(data.ResultMessage);

                            parent.jsprint("保存成功!请刷新。", "");
                            //
                            $("#txt_shebei").val("");
                            $("#txt_xinghao").val("");



                            $("#txt_jiage").val("");
                            $("#txt_tianjia_liyou").val("");
                           
                            $("#btnAdd").val("添加");
                            $("#btnAdd").attr("title", "");
                        }
                        else {
                            parent.jsprint("保存失败!请稍后再试。", "");
                        }


                    },
                    complete: function () {
                        //do something.
                        //$("#btnClick").removeAttr("disabled");
                        // 隐藏loading图片


                    },
                    error: function (data) {

                        //alert("error: " + data);
                        parent.jsprint("服务器异常!error:" + data, "");
                    },

                });
            }
            else {


                $.ajax({
                    type: "post",
                    dataType: "json",

                    data: { pa_id: $("#hd_pa_id").val(), shebei: shebei, xinghao: xinghao, jiage: jiage, tianjia_liyou: tianjia_liyou, add_user: $("#hd_add_user").val() },//注意：data参数可以是string个int类型

                    url: "ajax.ashx?action=add_equipment",//模拟web服务，提交到方法
                    // 可选的 async:false,阻塞的异步就是同步
                    beforeSend: function () {
                        // do something.
                        // 一般是禁用按钮等防止用户重复提交
                        // $("#btnClick").attr({disabled:"disabled"});
                        // 或者是显示loading图片
                    },
                    success: function (data) {
                        //alert("success: " + data);//注意这里：必须通过data.d才能获取到服务器返回的值
                        if (data.ResultCode == "1") {
                            console.log(data.ResultMessage);

                            parent.jsprint("保存成功!", "");
                            //
                      




                            var trhtml = "<tr>" +
                                "                       " +
                                "                        <td align=\"center\">" + data.ResultMessage + "</td>" +
                                "                        <td align=\"center\">" + shebei + "</td>" +
                                "                        <td align=\"center\">" + xinghao + "</td>" +
                                "                    " +
                                "                        <td align=\"center\">" + jiage + "</td>" +
                                "                        <td align=\"center\">" + tianjia_liyou + "</td>" +
                                "                        <td align=\"center\">" +
                                "                            <a href=\"#\" onclick='edit_equipment(" + data.ResultMessage + ",this)'>编辑</a> <a href=\"#\" onclick='del_equipment(" + data.ResultMessage + ",this)'>删除</a>" +
                                "                        </td>" +
                                "                    </tr>";


                            var s = $("#tb_teamlist").append(trhtml);

                            console.log(s);
                        }
                        else {
                            parent.jsprint("保存失败!请稍后再试。", "");
                        }


                    },
                    complete: function () {
                        //do something.
                        //$("#btnClick").removeAttr("disabled");
                        // 隐藏loading图片


                    },
                    error: function (data) {

                        //alert("error: " + data);
                        parent.jsprint("服务器异常!error:" + data, "");
                    },

                });

            }
           
        }

        function edit_equipment(id, thisObj) {
            $("#btnAdd").val("编辑");
            $("#btnAdd").attr("title", id);

          
            $("#txt_shebei").val($(thisObj).parent().parent().children("td").eq(1).text());
          
            $("#txt_xinghao").val($(thisObj).parent().parent().children("td").eq(2).text());
            $("#txt_jiage").val($(thisObj).parent().parent().children("td").eq(3).text());
            $("#txt_tianjia_liyou").val($(thisObj).parent().parent().children("td").eq(4).text());
           

        }

        function del_equipment(id, thisObj) {
            if (confirm('确认删除吗？')) {


                $.ajax({
                    type: "post",
                    dataType: "json",

                    data: { id:id},//注意：data参数可以是string个int类型

                    url: "ajax.ashx?action=del_equipment",//模拟web服务，提交到方法
                    // 可选的 async:false,阻塞的异步就是同步
                    beforeSend: function () {
                        // do something.
                        // 一般是禁用按钮等防止用户重复提交
                        // $("#btnClick").attr({disabled:"disabled"});
                        // 或者是显示loading图片
                    },
                    success: function (data) {
                        //alert("success: " + data);//注意这里：必须通过data.d才能获取到服务器返回的值
                        if (data.ResultCode == "1") {
                            console.log(data.ResultMessage);

                            $(thisObj).parent().parent().remove();

                            parent.jsprint("删除成功!", "");
                            //
                           
                        }
                        else {
                            parent.jsprint("删除失败!请稍后再试。", "");
                        }


                    },
                    complete: function () {
                        //do something.
                        //$("#btnClick").removeAttr("disabled");
                        // 隐藏loading图片


                    },
                    error: function (data) {

                        //alert("error: " + data);
                        parent.jsprint("服务器异常!error:" + data, "");
                    },

                });
               
               
            }
        }
    </script>
</head>

<body class="mainbody">
    <form id="form1" runat="server">
        <!--导航栏-->
        <div class="location">
            <a href="manager_list.aspx" class="back"><i class="iconfont icon-up"></i><span>返回列表页</span></a>
            <a href="../center.aspx"><i class="iconfont icon-home"></i><span>首页</span></a>
            <i class="arrow iconfont icon-arrow-right"></i>
            <a href="doc_list.aspx"><span>申报管理</span></a>
            <i class="arrow iconfont icon-arrow-right"></i>
            <span>项目经费情况</span>
        </div>
        <div class="line10"></div>
        <!--/导航栏-->

        <!--内容-->
        <div id="floatHead" class="content-tab-wrap">
            <div class="content-tab">
                <div class="content-tab-ul-wrap">
                    <ul>
                        <li><a class="selected" href="javascript:;">经费预算</a></li>
                        <li><a href="javascript:;">需增添的主要仪器设备</a></li>

                    </ul>
                </div>
            </div>
        </div>

        <div class="tab-content">

            <table  width="100%" border="0" cellspacing="0" cellpadding="0" class="ltable">
                        <tr>
                            <td colspan="4"> 经费来源</td>
                        </tr>
                <tr>
                    <td class="tdcenter">申请专项经费（万元）</td><td><input id="txt_laiyuan_zhuanxiang" runat="server" type="text" class="input"  /></td><td class="tdcenter">其他来源（万元）</td><td><input id="txt_laiyuan_qita" runat="server" type="text" class="input"  /></td>
                </tr>
                <tr>
                    <td colspan="4"> 经费支出情况</td>
                </tr>
                <tr>
                    <td class="tdcenter wordblod">支出科目</td><td class="tdcenter wordblod">专项经费 </td><td class="tdcenter wordblod">其他来源</td><td class="tdcenter wordblod">说明</td>
                </tr>
                <tr>
                     <td class="wordblod">一、直接费用</td>
                    <td class="tdcenter"><input id="txt_zhijie" runat="server" type="text" class="input"  /></td>
                    <td class="tdcenter"><input id="txt_zhijie1" runat="server" type="text" class="input"  /></td>
                    <td class="tdcenter"><input id="txt_zhijie2" runat="server" type="text" class="input"  /></td>
                </tr>
                 <tr>
                     <td>1、设备费</td>
                     <td class="tdcenter"><input id="txt_zhijie_shebei" runat="server" type="text" class="input"  /></td>
                     <td class="tdcenter"><input id="txt_zhijie_shebei1" runat="server" type="text" class="input"  /></td>
                     <td class="tdcenter"><input id="txt_zhijie_shebei2" runat="server" type="text" class="input"  /></td>
                </tr>
                <tr>
                     <td>（1）购置设备费</td>
                    <td class="tdcenter"><input id="txt_zhijie_sb_gouzhi" runat="server" type="text" class="input"  /></td>
                    <td class="tdcenter"><input id="txt_zhijie_sb_gouzhi1" runat="server" type="text" class="input"  /></td>
                    <td class="tdcenter"><input id="txt_zhijie_sb_gouzhi2" runat="server" type="text" class="input"  /></td>
                </tr>
                 <tr>
                     <td>（2）试制设备费</td>
                     <td class="tdcenter"><input id="txt_zhijie_sb_shizhi" runat="server" type="text" class="input"  /></td>
                     <td class="tdcenter"><input id="txt_zhijie_sb_shizhi1" runat="server" type="text" class="input"  /></td>
                     <td class="tdcenter"><input id="txt_zhijie_sb_shizhi2" runat="server" type="text" class="input"  /></td>
                </tr>
                 <tr>
                     <td>（3）设备改造与租赁</td>
                     <td class="tdcenter"><input id="txt_zhijie_sb_gaizao" runat="server" type="text" class="input"  /></td>
                     <td class="tdcenter"><input id="txt_zhijie_sb_gaizao1" runat="server" type="text" class="input"  /></td>
                     <td class="tdcenter"><input id="txt_zhijie_sb_gaizao2" runat="server" type="text" class="input"  /></td>
                </tr>
                 <tr>
                     <td>2、材料费</td>
                     <td class="tdcenter"><input id="txt_zhijie_cailiao" runat="server" type="text" class="input"  /></td>
                     <td class="tdcenter"><input id="txt_zhijie_cailiao1" runat="server" type="text" class="input"  /></td>
                     <td class="tdcenter"><input id="txt_zhijie_cailiao2" runat="server" type="text" class="input"  /></td>
                </tr>
                 <tr>
                     <td>3、测试化验加工费</td>
                     <td class="tdcenter"><input id="txt_zhijie_ceshi" runat="server" type="text" class="input"  /></td>
                     <td class="tdcenter"><input id="txt_zhijie_ceshi1" runat="server" type="text" class="input"  /></td>
                     <td class="tdcenter"><input id="txt_zhijie_ceshi2" runat="server" type="text" class="input"  /></td>
                </tr>
                 <tr>
                     <td>4、燃料动力费</td>
                     <td class="tdcenter"><input id="txt_zhijie_ranliao" runat="server" type="text" class="input"  /></td>
                     <td class="tdcenter"><input id="txt_zhijie_ranliao1" runat="server" type="text" class="input"  /></td>
                     <td class="tdcenter"><input id="txt_zhijie_ranliao2" runat="server" type="text" class="input"  /></td>
                </tr>
                <tr>
                     <td>5、差旅费</td>
                    <td class="tdcenter"><input id="txt_zhijie_chailv" runat="server" type="text" class="input"  /></td>
                    <td class="tdcenter"><input id="txt_zhijie_chailv1" runat="server" type="text" class="input"  /></td>
                    <td class="tdcenter"><input id="txt_zhijie_chailv2" runat="server" type="text" class="input"  /></td>
                </tr>
                <tr>
                     <td>6、会议费</td>
                    <td class="tdcenter"><input id="txt_zhijie_huiyi" runat="server" type="text" class="input"  /></td>
                    <td class="tdcenter"><input id="txt_zhijie_huiyi1" runat="server" type="text" class="input"  /></td>
                    <td class="tdcenter"><input id="txt_zhijie_huiyi2" runat="server" type="text" class="input"  /></td>
                </tr>
                <tr>
                     <td>7、国际合作与交流费</td>
                    <td class="tdcenter"><input id="txt_zhijie_hezuojiaoliu" runat="server" type="text" class="input"  /></td>
                    <td class="tdcenter"><input id="txt_zhijie_hezuojiaoliu1" runat="server" type="text" class="input"  /></td>
                    <td class="tdcenter"><input id="txt_zhijie_hezuojiaoliu2" runat="server" type="text" class="input"  /></td>
                </tr>
                <tr>
                     <td>8、信息费(出版/文献/信息传播/知识产权事物费等)</td>
                    <td class="tdcenter"><input id="txt_zhijie_xinxi" runat="server" type="text" class="input"  /></td>
                    <td class="tdcenter"><input id="txt_zhijie_xinxi1" runat="server" type="text" class="input"  /></td>
                    <td class="tdcenter"><input id="txt_zhijie_xinxi2" runat="server" type="text" class="input"  /></td>
                </tr>
                <tr>
                     <td>9、专家咨询费</td>
                    <td class="tdcenter"><input id="txt_zhijie_zixun" runat="server" type="text" class="input"  /></td>
                    <td class="tdcenter"><input id="txt_zhijie_zixun1" runat="server" type="text" class="input"  /></td>
                    <td class="tdcenter"><input id="txt_zhijie_zixun2" runat="server" type="text" class="input"  /></td>
                </tr>
                <tr>
                     <td>10、劳务费</td>
                    <td class="tdcenter"><input id="txt_zhijie_laowu" runat="server" type="text" class="input"  /></td>
                    <td class="tdcenter"><input id="txt_zhijie_laowu1" runat="server" type="text" class="input"  /></td>
                    <td class="tdcenter"><input id="txt_zhijie_laowu2" runat="server" type="text" class="input"  /></td>
                </tr>
                <tr>
                     <td>11、其他支出</td>
                    <td class="tdcenter"><input id="txt_zhijie_qita" runat="server" type="text" class="input"  /></td>
                    <td class="tdcenter"><input id="txt_zhijie_qita1" runat="server" type="text" class="input"  /></td>
                    <td class="tdcenter"><input id="txt_zhijie_qita2" runat="server" type="text" class="input"  /></td>
                </tr>
                 <tr>
                     <td class="wordblod">二、间接费用</td>
                     <td class="tdcenter"><input id="txt_jianjie" runat="server" type="text" class="input"  /></td>
                     <td class="tdcenter"><input id="txt_jianjie1" runat="server" type="text" class="input"  /></td>
                     <td class="tdcenter"><input id="txt_jianjie2" runat="server" type="text" class="input"  /></td>
                </tr>
                 <tr>
                     <td>1、管理费</td>
                     <td class="tdcenter"><input id="txt_jianjie_guanli" runat="server" type="text" class="input"  /></td>
                     <td class="tdcenter"><input id="txt_jianjie_guanli1" runat="server" type="text" class="input"  /></td>
                     <td class="tdcenter"><input id="txt_jianjie_guanli2" runat="server" type="text" class="input"  /></td>
                </tr>
                <tr>
                     <td>2、绩效支出</td>
                    <td class="tdcenter"><input id="txt_jianjie_jixiao" runat="server" type="text" class="input"  /></td>
                    <td class="tdcenter"><input id="txt_jianjie_jixiao1" runat="server" type="text" class="input"  /></td>
                    <td class="tdcenter"><input id="txt_jianjie_jixiao2" runat="server" type="text" class="input"  /></td>
                </tr>
                 <tr>
                     <td class="tdcenter wordblod">合计</td>
                     <td class="tdcenter"><input id="txt_heji" runat="server" type="text" class="input"  /></td>
                     <td class="tdcenter"><input id="txt_heji1" runat="server" type="text" class="input"  /></td>
                     <td class="tdcenter"><input id="txt_heji2" runat="server" type="text" class="input"  /></td>
                </tr>
                </table>

            

        </div>
        <div class="tab-content" style="display: none">
            
          
            <div class="toolbar">
                <div class="box-wrap">
                    <a class="menu-btn"><i class="iconfont icon-more"></i></a>
                    <div class="l-list">
                    
                          <div class="date-input">

                               <input id="txt_shebei" runat="server" type="text" placeholder="请输入设备名称" class="input" />
                          </div>

                      
                         <div class="date-input">

                                 <input id="txt_xinghao" runat="server" type="text" placeholder="请输入型号" class="input" />
                          </div>
                        <div class="date-input">

                                 <input id="txt_jiage" runat="server" type="text" placeholder="请输入价格（万元）" class="input" />
                          </div>
                         <div class="date-input">

                                 <input id="txt_tianjia_liyou" runat="server" type="text" placeholder="请输入增添理由" class="input" />
                          </div>
                       
                         <div class="date-input">

                                <input id="btnAdd" title="" type="button" class="btn" value="添加" onclick="add_equipment()" style="background: #bbc0c1;color: #2d2929;width: 100px;" />
                          </div>
                    </div>
                   <%-- <div class="r-list" style="position: unset;">
                        <asp:TextBox ID="txtKeywords" runat="server" CssClass="keyword" />
                        
                    </div>
                   --%>
                   
                       
                </div>
           
       
            <asp:Repeater ID="rptList" runat="server">
                <HeaderTemplate>
                    <table id="tb_teamlist" width="100%" border="0" cellspacing="0" cellpadding="0" class="ltable">
                        <tr>
                                                                   
                            <th align="center" width="8%">序号</th>
                         
                             <th align="center" width="7%">设备名称</th>
                            <th align="center" width="7%">型号</th>

                            <th align="center" width="8%">价格（万元）</th>
                           
                            <th align="center" width="7%">增添理由</th>
                           
                            <th align="center" width="6%">操作</th>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                       
                        <td align="center"><%# Eval("id") %></td>
                        <td align="center"><%# Eval("shebei") %></td>
                    
                        <td align="center"><%#Eval("xinghao")%></td>
                        <td align="center"><%# Eval("jiage") %></td>
                        <td align="center"><%# Eval("tianjia_liyou") %></td>
                       

                        <td align="center">
                            <a href="#" onclick="edit_equipment(<%# Eval("id") %>,this)">编辑</a> <a href="#" onclick="del_equipment(<%# Eval("id") %>,this)">删除</a>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    <%#rptList.Items.Count == 0 ? "<tr><td align=\"center\" colspan=\"8\">暂无记录</td></tr>" : ""%>
  </table>
                </FooterTemplate>
            </asp:Repeater>

        </div>

        </div>

        <!--/内容-->

        <!--工具栏-->
        <div class="page-footer">
            <div class="btn-wrap">
                <asp:Button ID="btnSubmit" runat="server" Text="提交保存" CssClass="btn" OnClick="btnSubmit_Click" />
                <input name="btnReturn" type="button" value="返回上一页" class="btn yellow" onclick="javascript: history.back(-1);" />
            </div>
        </div>
        <!--/工具栏-->
        <asp:HiddenField ID="hd_pa_id" runat="server"  />
        <asp:HiddenField ID="hd_add_user" runat="server" />
    </form>
</body>
</html>

