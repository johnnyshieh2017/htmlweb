﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="orgedit.aspx.cs" ValidateRequest="false" Inherits="DTcms.Web.admin.projectmanage.orgedit" %>

<%@ Import Namespace="DTcms.Common" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>编辑</title>
    <meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,initial-scale=1.0,user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <link rel="stylesheet" type="text/css" href="../../scripts/artdialog/ui-dialog.css" />
    <link rel="stylesheet" type="text/css" href="../skin/icon/iconfont.css" />
    <link rel="stylesheet" type="text/css" href="../skin/default/style.css" />
    <script type="text/javascript" charset="utf-8" src="../../scripts/jquery/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../scripts/jquery/Validform_v5.3.2_min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../scripts/artdialog/dialog-plus-min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../scripts/webuploader/webuploader.min.js"></script>

    <script type="text/javascript" charset="utf-8" src="../../editor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../editor/ueditor.all.min.js"> </script>
    <script type="text/javascript" charset="utf-8" src="../../editor/lang/zh-cn/zh-cn.js"></script>

    <script type="text/javascript" charset="utf-8" src="../js/uploader.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/common.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/laymain.js"></script>
    <script type="text/javascript">
        $(function () {
            //初始化表单验证
            $("#form1").initValidform();
            //初始化上传控件
            $(".upload-img").InitUploader({ sendurl: "../../tools/upload_ajax.ashx", swf: "../../scripts/webuploader/uploader.swf" });

            //初始化editor
            $(".editor-mini").each(function (i) {
                var objId = $(this).attr("id");
                if (objId != "undefined") {
                    var editorMini = UE.getEditor(objId, {
                        serverUrl: '../../../tools/upload_ajax.ashx',
                        initialFrameWidth: '100%',
                        initialFrameHeight: 220,
                        toolbars: [[
                            'fullscreen', 'source', '|', 'undo', 'redo', '|',
                            'bold', 'italic', 'underline', 'strikethrough', 'removeformat', 'pasteplain', '|', 'forecolor', 'insertorderedlist', 'insertunorderedlist', '|',
                         'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|',
                         'link', 'unlink', 'anchor', '|',
                         'simpleupload', 'insertimage', 'scrawl', 'insertvideo'
                        ]]
                    });
                }
            });

        });
    </script>
</head>

<body class="mainbody">
    <form id="form1" runat="server">
        <!--导航栏-->
        <div class="location">
            <a href="manager_list.aspx" class="back"><i class="iconfont icon-up"></i><span>返回列表页</span></a>
            <a href="../center.aspx"><i class="iconfont icon-home"></i><span>首页</span></a>
            <i class="arrow iconfont icon-arrow-right"></i>
            <a href="hospitalmanage.aspx"><span>科室管理</span></a>
            <i class="arrow iconfont icon-arrow-right"></i>
            <span>编辑科室</span>
        </div>
        <div class="line10"></div>
        <!--/导航栏-->

        <!--内容-->
        <div id="floatHead" class="content-tab-wrap">
            <div class="content-tab">
                <div class="content-tab-ul-wrap">
                    <ul>
                        <li><a class="selected" href="javascript:;">医院信息</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="tab-content">


            <dl>
                <dt>科室名称</dt>
                <dd>
                    <asp:TextBox ID="txtRealName" runat="server" CssClass="input normal" datatype="*2-100"></asp:TextBox></dd>
            </dl>
            <dl style="display:none">
                <dt>等级</dt>
                <dd>
                    <div class="rule-single-select">
                        <asp:DropDownList ID="ddlhospitallevel" runat="server" datatype="*" errormsg="请选择" sucmsg=" ">
                            <asp:ListItem Text="三甲" Value="三甲"></asp:ListItem>
                             <asp:ListItem Text="三乙" Value="三乙"></asp:ListItem>
                             <asp:ListItem Text="三丙" Value="三丙"></asp:ListItem>

                            <asp:ListItem Text="二甲" Value="二甲"></asp:ListItem>
                             <asp:ListItem Text="二乙" Value="二乙"></asp:ListItem>
                             <asp:ListItem Text="二丙" Value="二丙"></asp:ListItem>

                            <asp:ListItem Text="一甲" Value="一甲"></asp:ListItem>
                             <asp:ListItem Text="一乙" Value="一乙"></asp:ListItem>
                             <asp:ListItem Text="一丙" Value="一丙"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </dd>
            </dl>
             <dl style="display:none">
                <dt>地址</dt>
                <dd>
                    <asp:TextBox ID="txtaddress" runat="server" CssClass="input normal" datatype="*2-100"></asp:TextBox>

                </dd>
            </dl>
            
            <dl>
                <dt>联系电话</dt>
                <dd>
                    <asp:TextBox ID="txttel" runat="server" CssClass="input normal" datatype="*2-100"></asp:TextBox>
                </dd>
            </dl>
            <dl style="display:none">
                <dt>交通</dt>
                <dd>
                    <asp:TextBox ID="txtmap" runat="server" CssClass="input normal" datatype="*2-400"></asp:TextBox>
                </dd>
            </dl>
          <dl style="display:none">
                <dt>logo</dt>
                <dd>
                     <asp:TextBox ID="txtLogo" runat="server" CssClass="input normal upload-path" />
                    <div class="upload-box upload-img"></div>

                </dd>
            </dl>
            <dl style="display:none">
                <dt>备注</dt>
                <dd>
                    <textarea id="txtintro" class="editor-mini" runat="server"></textarea></dd>
            </dl>

          
        </div>
        <!--/内容-->

        <!--工具栏-->
        <div class="page-footer">
            <div class="btn-wrap">
                <asp:Button ID="btnSubmit" runat="server" Text="提交保存" CssClass="btn" OnClick="btnSubmit_Click" />
                <input name="btnReturn" type="button" value="返回上一页" class="btn yellow" onclick="javascript: history.back(-1);" />
            </div>
        </div>
        <!--/工具栏-->

    </form>
</body>
</html>