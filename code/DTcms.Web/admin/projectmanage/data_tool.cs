﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DTcms.Web.admin.projectmanage
{
    public static class data_tool
    {
        static BLL.ky_dictionary dictionary_bll = new BLL.ky_dictionary();

        /// <summary>
        /// 获得项目级别
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string GetOrg(string id)
        {

            var  project_level = dictionary_bll.GetModelListByCache("project_type");

            var model = project_level.Find(p => p.dic_value == id);
            if (model != null)
            {
                return model.dic_text;
            }
            else
            {
                return "";
            }

           
        }

        public static string Getzuozheleixing(string id)
        {

            var zuozheleixing = dictionary_bll.GetModelListByCache("author_type");

            var model = zuozheleixing.Find(p => p.dic_value == id);
            if (model != null)
            {
                return model.dic_text;
            }
            else
            {
                return "";
            }
        }

        public static string Getqikanjibie(string id)
        {

            var qikanjibie = dictionary_bll.GetModelListByCache("qikan_jibie");

            var model = qikanjibie.Find(p => p.dic_value == id);
            if (model != null)
            {
                return model.dic_text;
            }
            else
            {
                return "";
            }
        }

        public static string Getzhuanlileibie(string id)
        {

            var qikanjibie = dictionary_bll.GetModelListByCache("zhuanli_leibie");

            var model = qikanjibie.Find(p => p.dic_value == id);
            if (model != null)
            {
                return model.dic_text;
            }
            else
            {
                return "";
            }
        }


        public static string Getproject_category(string id)
        {

            var project_level = dictionary_bll.GetModelListByCache("project_category");

            var model = project_level.Find(p => p.dic_value == id);
            if (model != null)
            {
                return model.dic_text;
            }
            else
            {
                return "";
            }


        }


    }
}