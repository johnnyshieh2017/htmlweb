﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="docservicemanage.aspx.cs" Inherits="DTcms.Web.admin.docmanage.docservicemanage" %>

<%@ Import Namespace="DTcms.Common" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,initial-scale=1.0,user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <title>医生服务列表</title>
    <link rel="stylesheet" type="text/css" href="../../scripts/artdialog/ui-dialog.css" />
    <link rel="stylesheet" type="text/css" href="../../css/pagination.css" />
    <link rel="stylesheet" type="text/css" href="../skin/icon/iconfont.css" />
    <link rel="stylesheet" type="text/css" href="../skin/default/style.css" />
    <script type="text/javascript" src="../../scripts/jquery/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="../../scripts/artdialog/dialog-plus-min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/laymain.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/common.js"></script>
    <script type="text/javascript">
        function create_account(id) {
            var text = "创建成功！";
            var type = "auto";


            $.post("doctormanage.ashx", { action: "create", id: id }, function (result) {

                console.log(result);
                if (result.ResultCode == "1") {
                    jsdialog("提示", text, "doclist.aspx", "");
                }
                else {
                    jsdialog("提示", result.ResultMessage, "doclist.aspx", "");
                }

            });
        }

        function show(id) {
            //dialog({ title: '.', content: "url:doc_qualification.aspx?id="+this.title }).showModal()
            ShowMaxDialog("详情", "doc_qualification.aspx?id=" + id);
        }

        function verify(id) {
            dialog({
                title: '提示',
                content: "确定审核此信息吗？",
                okValue: '确定',
                ok: function () {
                    $.post("doctormanage.ashx", { action: "verify", id: id }, function (result) {

                        console.log(result);
                        if (result.ResultCode == "1") {
                            jsdialog("提示", result.ResultMessage, "docservicemanage.aspx", "");
                        }
                        else {
                            jsdialog("提示", result.ResultMessage, "docservicemanage.aspx", "");
                        }

                    });
                },
                cancelValue: '取消',
                cancel: function () { }
            }).showModal();
        }
    </script>
</head>

<body class="mainbody">
    <form id="form1" runat="server">
        <!--导航栏-->
        <div class="location">
            <a href="javascript:history.back(-1);" class="back"><i class="iconfont icon-up"></i><span>返回上一页</span></a>
            <a href="../center.aspx" class="home"><i class="iconfont icon-home"></i><span>首页</span></a>
            <i class="arrow iconfont icon-arrow-right"></i>
            <span>管理员列表</span>
        </div>
        <!--/导航栏-->

        <!--工具栏-->
        <div id="floatHead" class="toolbar-wrap">
            <div class="toolbar">
                <div class="box-wrap">
                    <a class="menu-btn"><i class="iconfont icon-more"></i></a>
                    <div class="l-list">
                        <ul class="icon-list">

                            <li><a href="javascript:;" onclick="checkAll(this);"><i class="iconfont icon-check"></i><span>全选</span></a></li>
                            <li><asp:LinkButton ID="btnAudit" runat="server" OnClientClick="return ExePostBack('btnAudit','确定审核此项信息吗？');" onclick="btnAudit_Click"><i class="iconfont icon-survey"></i><span>审核</span></asp:LinkButton></li>
                            <li>
                                <asp:LinkButton ID="btnDelete" runat="server" OnClientClick="return ExePostBack('btnDelete','确认取消审核此项信息吗？');" OnClick="btnDelete_Click"><i class="iconfont icon-delete"></i><span>取消审核</span></asp:LinkButton></li>
                        </ul>
                    </div>
                    <div class="r-list">
                        <div class="rule-single-select">
                            <asp:DropDownList ID="ddlsearchtype" runat="server" >
                                <asp:ListItem Text="真实姓名" Value="1"></asp:ListItem>
                                <asp:ListItem Text="电话号码" Value="2"></asp:ListItem>

                                <asp:ListItem Text="身份证号" Value="3"></asp:ListItem>
                                <asp:ListItem Text="所在医院" Value="4"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <asp:TextBox ID="txtKeywords" runat="server" CssClass="keyword" />
                        <asp:LinkButton ID="lbtnSearch" runat="server" CssClass="btn-search" OnClick="btnSearch_Click"><i class="iconfont icon-search"></i></asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
        <!--/工具栏-->

        <!--列表-->
        <div class="table-container">
            <asp:Repeater ID="rptList" runat="server">
                <HeaderTemplate>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="ltable">
                        <tr>
                            <th width="8%">选择</th>
                            <th align="left">姓名</th>
                            <th align="left">医院</th>

                            <th align="left" width="12%">电话</th>
                            <th align="left" width="12%">身份证</th>
                            <th align="left">服务名称</th>
                            <th align="left">计费方式</th>
                            <th align="left">价格(元)</th>
                            <th width="8%">状态</th>
                            <th width="8%">操作</th>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td align="center">
                            <asp:CheckBox ID="chkId" CssClass="checkall" runat="server" Style="vertical-align: middle;" />
                            <asp:HiddenField ID="hidId" Value='<%#Eval("id")%>' runat="server" />
                            <asp:HiddenField ID="hiddid" Value='<%#Eval("doctorid")%>' runat="server"/>
                            <asp:HiddenField ID="hidbid" Value='<%#Eval("businessid")%>' runat="server"/>
                        </td>
                        <td><%# Eval("name") %></td>
                        <td><%# Eval("hospitalname") %></td>

                        <td><%# Eval("mobile") %></td>
                        <td><%# Eval("idcard") %></td>
                        <td><%#Eval("business_name")%></td>
                        <td><%#Eval("cost_type")%></td>
                        <td><%#GetPrice(Convert.ToDecimal(Eval("price")))%></td>
                        <td align="center"><%#GetStatus(Convert.ToInt32(Eval("status")))%></td>

                        <td align="center">
                           <%-- <a href="docedit.aspx?action=<%#DTEnums.ActionEnum.Edit %>&id=<%#Eval("id")%>">修改</a>--%>
                            <a href="#" title="<%#Eval("doctorid")%>" onclick="verify(this.title)">审核</a>

                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    <%#rptList.Items.Count == 0 ? "<tr><td align=\"center\" colspan=\"8\">暂无记录</td></tr>" : ""%>
  </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
        <!--/列表-->

        <!--内容底部-->
        <div class="line20"></div>
        <div class="pagelist">
            <div class="l-btns">
                <span>显示</span><asp:TextBox ID="txtPageNum" runat="server" CssClass="pagenum" onkeydown="return checkNumber(event);"
                    OnTextChanged="txtPageNum_TextChanged" AutoPostBack="True"></asp:TextBox><span>条/页</span>
            </div>
            <div id="PageContent" runat="server" class="default"></div>
        </div>
        <!--/内容底部-->
    </form>
</body>
</html>
