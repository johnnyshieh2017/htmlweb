﻿using DTcms.Common;
using DTcms.Model;
using DTcms.Web.cmsapi.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DTcms.Web.admin.docmanage
{
    /// <summary>
    /// doctormanage 的摘要说明
    /// </summary>
    public class doctormanage : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
           

            //取得处事类型
            string action = DTRequest.GetFormString("action");
            switch (action)
            {
                case "create": // 
                    create_account(context);
                    break;
                case "verify": // 
                    verify_doctor(context);
                    break;
            }
        }

        private void verify_doctor(HttpContext context)
        {
            JsonResult<DTcms.Model.yjr_doctor> res = new JsonResult<yjr_doctor>();
            string id = DTRequest.GetFormString("id");

            BLL.yjr_doctor docbll = new BLL.yjr_doctor();

            var doc = docbll.GetModel(int.Parse(id));
            if (doc != null)
            {
                doc.status = 1;

                int result = docbll.Update(doc) == true ? 1 : 0;

                res.ResultCode = result;
                res.ResultMessage = result == 1 ? "执行成功。" : "执行失败！";
            }
            else
            {
                res.ResultCode = 0;
                res.ResultMessage = "医生信息不存在！";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }


        private void create_account(HttpContext context)
        {
            JsonResult<DTcms.Model.yjr_doctor> res = new JsonResult<yjr_doctor>();
            string id = DTRequest.GetFormString("id");
            BLL.yjr_doctor_vsuser bll = new BLL.yjr_doctor_vsuser();
            BLL.yjr_doctor docbll = new BLL.yjr_doctor();
            if (bll.GetRecordCount(" docid=" + id) > 0)
            {
                res.ResultCode = 0;
                res.ResultMessage = "对不起，已存在账户信息";
            }
            else
            {
                var doc = docbll.GetModel(int.Parse(id));
                if (doc != null)
                {
                    Model.users model = new Model.users();
                    BLL.users userbll = new BLL.users();
                    model.site_id = 2;// int.Parse(ddlSiteId.SelectedValue);
                    model.group_id = 1;// int.Parse(ddlGroupId.SelectedValue);
                    model.status = 0;// int.Parse(rblStatus.SelectedValue);
                    //检测用户名是否重复
                    if (userbll.Exists(doc.mobile))
                    {
                        res.ResultCode = 0;
                        res.ResultMessage = "对不起，账户信息已存在";
                    }
                    else
                    {
                        model.user_name = doc.mobile;// Utils.DropHTML(txtUserName.Text.Trim());
                                                     //获得6位的salt加密字符串
                        model.salt = Utils.GetCheckCode(6);
                        //以随机生成的6位字符串做为密钥加密
                        model.password = DESEncrypt.Encrypt("888888", model.salt);
                        model.email = "";// Utils.DropHTML(txtEmail.Text);
                        model.nick_name = doc.name;// Utils.DropHTML(txtNickName.Text);
                        model.avatar = ""; //默认头像地址？？  // Utils.DropHTML(txtAvatar.Text);
                        model.sex = doc.gender;// rblSex.SelectedValue;

                        model.telphone = doc.mobile;// Utils.DropHTML(txtTelphone.Text.Trim());
                        model.mobile = doc.mobile;//  Utils.DropHTML(txtMobile.Text.Trim());
                        model.qq = "";// Utils.DropHTML(txtQQ.Text);
                        model.msn = "";// Utils.DropHTML(txtMsn.Text);
                        model.address = "";// Utils.DropHTML(txtAddress.Text.Trim());
                        model.amount = 0;// decimal.Parse(txtAmount.Text.Trim());
                        model.point = 10;// int.Parse(txtPoint.Text.Trim());
                        model.exp = 10;// int.Parse(txtExp.Text.Trim());
                        model.reg_time = DateTime.Now;
                        model.reg_ip = "127.0.0.1";// DTRequest.GetIP();
                        model.hospital = "2";//后台创建医生
                        var add_res = userbll.Add(model);
                        if (add_res > 0)
                        {
                            Model.yjr_doctor_vsuser docvsuser = new yjr_doctor_vsuser();
                            docvsuser.docid = id;
                            docvsuser.userid = add_res.ToString();
                            docvsuser.add_time = System.DateTime.Now;
                            bll.Add(docvsuser);

                            res.ResultCode = 1;
                            res.ResultMessage = "创建成功！";
                        }
                        else
                        {
                            res.ResultCode = 0;
                            res.ResultMessage = "对不起，账户信息创建失败";
                        }
                    }
                   
                }
                else
                {
                    res.ResultCode = -1;
                    res.ResultMessage = "对不起，医生信息不存在";
                }
            }
            
            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}