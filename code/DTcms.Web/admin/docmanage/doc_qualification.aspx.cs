﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DTcms.Web.admin.docmanage
{
    public partial class doc_qualification : System.Web.UI.Page
    {
        protected string idcard_front = "";
        protected string idcard_back = "";
        protected string license = "";
        protected string qualification = "";
        protected string titlepic = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var id = DTcms.Common.DTRequest.GetQueryInt("id");

                var _doc_model = new BLL.yjr_doctor().GetModel(id);
                if (_doc_model != null)
                {
                    idcard_front = _doc_model.idcardpic_front;
                    idcard_back = _doc_model.idcardpic_back;
                    license = _doc_model.doc_license;
                    qualification = _doc_model.doc_qualification;
                    titlepic = _doc_model.doc_titlepic;
                }
            }
        }
    }
}