﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="doc_qualification.aspx.cs" Inherits="DTcms.Web.admin.docmanage.doc_qualification" %>

<%@ Import Namespace="DTcms.Common" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>编辑</title>
    <meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,initial-scale=1.0,user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <link rel="stylesheet" type="text/css" href="../../scripts/artdialog/ui-dialog.css" />
    <link rel="stylesheet" type="text/css" href="../skin/icon/iconfont.css" />
    <link rel="stylesheet" type="text/css" href="../skin/default/style.css" />
<%--   
    <script type="text/javascript" src="../../scripts/jquery/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="../../scripts/artdialog/dialog-plus-min.js"></script>
  <script type="text/javascript" charset="utf-8" src="../js/common.js"></script>--%>

    <style type="text/css">
        div img {
            max-width:600px;
            max-height:350px;
        }
    </style>
 
   
</head>

<body class="mainbody">
    <form id="form1" runat="server">
      
        <!--内容-->
        <div id="floatHead" class="content-tab-wrap">
            <div class="content-tab">
                <div class="content-tab-ul-wrap">
                    <ul>
                        <li><a class="selected" href="javascript:;">资质信息</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="tab-content">
            <dl>
                <dt>身份证正面</dt>
                <dd>
                   <img src="<%=idcard_front %>" />
                </dd>
            </dl>
            <dl>
                <dt>身份证反面</dt>
                <dd>
                   <img src="<%=idcard_back %>" />
                </dd>
            </dl>
            
            <dl>
                <dt>医师职业证</dt>
                <dd>
                   <img src="<%=license %>" />
                  
                </dd>
            </dl>
            <dl>
                <dt>医师资格证</dt>
                <dd>
                  
                      <img src="<%=qualification %>" />
                  
                </dd>
            </dl>
            <dl>
                <dt>职称证</dt>
                <dd>
                  
                        <img src="<%=titlepic %>" />
                  
                </dd>
            </dl>
          

           
        </div>

       
        <!--/内容-->

       

    </form>
</body>
</html>