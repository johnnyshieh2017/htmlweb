﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DTcms.Common;

namespace DTcms.Web.admin.docmanage
{
    public partial class docedit : Web.UI.ManagePage
    {
        private string action = DTEnums.ActionEnum.Add.ToString(); //操作类型
        private int id = 0;
        BLL.yjr_doctor docbll = new BLL.yjr_doctor();
        protected void Page_Load(object sender, EventArgs e)
        {
            string _action = DTRequest.GetQueryString("action");
            if (!string.IsNullOrEmpty(_action) && _action == DTEnums.ActionEnum.Edit.ToString())
            {
                this.action = DTEnums.ActionEnum.Edit.ToString();//修改类型
                if (!int.TryParse(Request.QueryString["id"] as string, out this.id))
                {
                    JscriptMsg("传输参数不正确！", "back");
                    return;
                }
                if (!new BLL.yjr_doctor().Exists(this.id))
                {
                    JscriptMsg("记录不存在或已被删除！", "back");
                    return;
                }
            }

            if (!IsPostBack)
            {
                ChkAdminLevel("doclist", DTEnums.ActionEnum.View.ToString()); //检查权限
                Model.manager model = GetAdminInfo(); //取得管理员信息
                InitData();
                if (action == DTEnums.ActionEnum.Edit.ToString()) //修改
                {
                    ShowInfo(this.id);
                }
            }
        }

        #region 初始化===================================
        
        void InitData()
        {
            var hospatil = new BLL.yjr_doc_hospital().GetModelList("");
            foreach (var item in hospatil)
            {
                ddlhospital.Items.Add(new ListItem(item.hospitalname, item.id.ToString()));
            }

            var group = new BLL.yjr_doctor_group().GetModelList(" siteid =1 and status=1 and hospatilid=-1");
            foreach (var item in group)
            {
                ddldepartment.Items.Add(new ListItem(item.groupname, item.id.ToString()));
            }

            var title = new BLL.yjr_doctor_title().GetModelList(" siteid =1 and status=1");
            foreach (var item in title)
            {
                ddltitle.Items.Add(new ListItem(item.titlename, item.id.ToString()));
            }

            var position = new BLL.yjr_doctor_position().GetModelList("");
            foreach (var item in position)
            {
                ddlposition.Items.Add(new ListItem(item.position_name, item.id.ToString()));
            }
        }
        #endregion

        #region 赋值操作=================================
        private void ShowInfo(int _id)
        {
            var model = docbll.GetModel(_id);
            txtRealName.Text = model.name;
            //txtdepartmentName.Text = model.department_name;
            txtTelephone.Text = model.mobile;
            txtSort.Text = model.sort.ToString();
            txtintro.Value = model.intro;
            txtAvatar.Text = model.logoimg;
            txtspeciality.Text = model.speciality;
            ddlgender.Items.FindByText(model.gender).Selected = true;
            ddldepartment.SelectedValue = model.departmentid.ToString();
            ddlhospital.SelectedValue = model.hospitalid.ToString();
            if (ddlhospital.SelectedValue != "0")
            {
                BLL.yjr_doctor_group bll = new BLL.yjr_doctor_group();

                var group = bll.GetModelList(" hospatilid=" + ddlhospital.SelectedValue);
                foreach (var item in group)
                {
                    ddsection.Items.Add(new ListItem(item.groupname, item.id.ToString()));
                }
                ddsection.SelectedValue = model.departmentid.ToString();
            }
            ddltitle.SelectedValue = model.title.ToString();

            ddlposition.SelectedValue = model.position_id.ToString();
            txtIDCard.Text = model.idcard;
            txtidcard_front.Text = model.idcardpic_front;
            txtidcard_back.Text = model.idcardpic_back;
            txtlicense.Text = model.doc_license;
            txtqualification.Text = model.doc_qualification;
            txttitlepic.Text = model.doc_titlepic;


        }
        #endregion

        #region 增加操作=================================
        private bool DoAdd()
        {
            DTcms.Model.yjr_doctor model_add = new Model.yjr_doctor();
            model_add.name = txtRealName.Text.Trim();
            model_add.title = int.Parse(ddltitle.SelectedValue);
            model_add.gender = ddlgender.SelectedItem.Text;
            model_add.groupid =int.Parse(ddldepartment.SelectedValue);
            model_add.intro = txtintro.Value;
            model_add.speciality = txtspeciality.Text.Trim().Replace("，", ",");
            model_add.logoimg = txtAvatar.Text.Trim();
            model_add.mobile = txtTelephone.Text.Trim();
            model_add.siteid = 1;
            model_add.sort = int.Parse(txtSort.Text.Trim());
            model_add.hospitalid = int.Parse(ddlhospital.SelectedValue);
            model_add.department_name = "";// txtdepartmentName.Text.Trim();
            model_add.status = 1;
            model_add.isdelete = 0;
            model_add.departmentid = int.Parse(ddsection.SelectedValue);// int.Parse(ddldepartment.SelectedValue);
            model_add.createtime = System.DateTime.Now;

            model_add.position_id = int.Parse(ddlposition.SelectedValue);
            model_add.idcard = txtIDCard.Text.Trim();
            model_add.idcardpic_front = txtidcard_front.Text;
            model_add.idcardpic_back = txtidcard_back.Text;
            model_add.doc_license = txtlicense.Text;
            model_add.doc_qualification = txtqualification.Text;
            model_add.doc_titlepic = txttitlepic.Text;

            if (docbll.Add(model_add) > 0)
            {
                //
                AddAdminLog(DTEnums.ActionEnum.Add.ToString(), "添加doctor:" + model_add.name); //记录日志
                return true;
            }
            return false;
        }
        #endregion

        #region 修改操作=================================
        private bool DoEdit(int _id)
        {
            bool result = false;

            DTcms.Model.yjr_doctor model_edit = docbll.GetModel(_id);
            model_edit.name = txtRealName.Text.Trim();
            model_edit.title = int.Parse(ddltitle.SelectedValue);
            model_edit.gender = ddlgender.SelectedItem.Text;
            model_edit.groupid = int.Parse(ddldepartment.SelectedValue);
            model_edit.intro = txtintro.Value;
            model_edit.speciality = txtspeciality.Text.Trim().Replace("，", ",");
            model_edit.logoimg = txtAvatar.Text.Trim();
            model_edit.mobile = txtTelephone.Text.Trim();
            model_edit.siteid = 1;
            model_edit.sort = int.Parse(txtSort.Text.Trim());
            model_edit.department_name = "";// txtdepartmentName.Text.Trim();
            model_edit.hospitalid = int.Parse(ddlhospital.SelectedValue);
            model_edit.departmentid = int.Parse(ddsection.SelectedValue);

            model_edit.position_id = int.Parse(ddlposition.SelectedValue);
            model_edit.idcard = txtIDCard.Text.Trim();
            model_edit.idcardpic_front = txtidcard_front.Text;
            model_edit.idcardpic_back = txtidcard_back.Text;
            model_edit.doc_license = txtlicense.Text;
            model_edit.doc_qualification = txtqualification.Text;
            model_edit.doc_titlepic = txttitlepic.Text;


            model_edit.status = 1;
            model_edit.isdelete = 0;
        
            
            if (docbll.Update(model_edit) )
            {
                AddAdminLog(DTEnums.ActionEnum.Add.ToString(), "修改doctor:" + model_edit.name); //记录日志
                result =  true;
            }

            return result;
        }
        #endregion

        //保存
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (action == DTEnums.ActionEnum.Edit.ToString()) //修改
            {
                ChkAdminLevel("doclist", DTEnums.ActionEnum.Edit.ToString()); //检查权限
                if (!DoEdit(this.id))
                {
                    JscriptMsg("保存过程中发生错误！", "");
                    return;
                }
                JscriptMsg("修改信息成功！", "doclist.aspx");
            }
            else //添加
            {
                ChkAdminLevel("doclist", DTEnums.ActionEnum.Add.ToString()); //检查权限
                if (!DoAdd())
                {
                    JscriptMsg("保存过程中发生错误！", "");
                    return;
                }
                JscriptMsg("添加信息成功！", "doclist.aspx");
            }
        }

     

        protected void ddlhospital_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlhospital.SelectedValue != "0")
            {
                BLL.yjr_doctor_group bll = new BLL.yjr_doctor_group();

                var group = bll.GetModelList(" hospatilid=" + ddlhospital.SelectedValue);
                foreach (var item in group)
                {
                    ddsection.Items.Add(new ListItem(item.groupname, item.id.ToString()));
                }
            }
            else
            {
                ddsection.Items.Clear();
            }
        }
    }
}