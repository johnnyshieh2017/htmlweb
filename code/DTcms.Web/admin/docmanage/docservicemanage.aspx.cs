﻿using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DTcms.Common;

namespace DTcms.Web.admin.docmanage
{
    public partial class docservicemanage : Web.UI.ManagePage
    {
        protected int totalCount;
        protected int page;
        protected int pageSize;
        //BLL.yjr_doctor_vsbusinessconfig docvsuser_bll = new BLL.yjr_doctor_vsbusinessconfig();
        protected string keywords = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            this.keywords = DTRequest.GetQueryString("keywords");

            this.pageSize = GetPageSize(10); //每页数量
            if (!Page.IsPostBack)
            {
                ChkAdminLevel("docservicemanage", DTEnums.ActionEnum.View.ToString()); //检查权限
                Model.manager model = GetAdminInfo(); //取得当前管理员信息
                //RptBind("role_type>=" + model.role_type + CombSqlTxt(keywords), "add_time asc,id desc");
                RptBind("1=1 " + CombSqlTxt(keywords), "add_time desc");
            }
        }

        #region 数据绑定=================================
        private void RptBind(string _strWhere, string _orderby)
        {
            this.page = DTRequest.GetQueryInt("page", 1);
            txtKeywords.Text = this.keywords;
            BLL.yjr_doctor_vsbusinessconfig bll = new BLL.yjr_doctor_vsbusinessconfig();
            //BLL.manager bll = new BLL.manager();
            this.rptList.DataSource = bll.GetList(this.pageSize, this.page, _strWhere, _orderby, out this.totalCount);
            this.rptList.DataBind();

            //绑定页码
            txtPageNum.Text = this.pageSize.ToString();
            string pageUrl = Utils.CombUrlTxt("docservicemanage.aspx", "keywords={0}&page={1}", this.keywords, "__id__");
            PageContent.InnerHtml = Utils.OutPageList(this.pageSize, this.page, this.totalCount, pageUrl, 8);
        }
        #endregion

        #region 组合SQL查询语句==========================
        protected string CombSqlTxt(string _keywords)
        {
            StringBuilder strTemp = new StringBuilder();
            _keywords = _keywords.Replace("'", "");
            if (!string.IsNullOrEmpty(_keywords))
            {
                strTemp.Append(" and (name like  '%" + _keywords + "%' )");
            }

            return strTemp.ToString();
        }
        #endregion

        #region 返回每页数量=============================
        private int GetPageSize(int _default_size)
        {
            int _pagesize;
            if (int.TryParse(Utils.GetCookie("docservicemanage_page_size", "DTcmsPage"), out _pagesize))
            {
                if (_pagesize > 0)
                {
                    return _pagesize;
                }
            }
            return _default_size;
        }
        #endregion

        //关健字查询
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect(Utils.CombUrlTxt("docservicemanage.aspx", "keywords={0}", txtKeywords.Text));
        }

        //设置分页数量
        protected void txtPageNum_TextChanged(object sender, EventArgs e)
        {
            int _pagesize;
            if (int.TryParse(txtPageNum.Text.Trim(), out _pagesize))
            {
                if (_pagesize > 0)
                {
                    Utils.WriteCookie("docservicemanage_page_size", "DTcmsPage", _pagesize.ToString(), 14400);
                }
            }
            Response.Redirect(Utils.CombUrlTxt("docservicemanage.aspx", "keywords={0}", this.keywords));
        }


        //批量审核
        protected void btnAudit_Click(object sender, EventArgs e)
        {
            ChkAdminLevel("docservicemanage", DTEnums.ActionEnum.Audit.ToString()); //检查权限
            int sucCount = 0;
            int errorCount = 0;
            BLL.yjr_doctor_vsbusiness bll = new BLL.yjr_doctor_vsbusiness();
           
            for (int i = 0; i < rptList.Items.Count; i++)
            {
                int id = Convert.ToInt32(((HiddenField)rptList.Items[i].FindControl("hidId")).Value);
                int did = Convert.ToInt32(((HiddenField)rptList.Items[i].FindControl("hiddid")).Value);
                int bid = Convert.ToInt32(((HiddenField)rptList.Items[i].FindControl("hidbid")).Value);
                CheckBox cb = (CheckBox)rptList.Items[i].FindControl("chkId");
                if (cb.Checked)
                {
                    var model = bll.GetModel(did,bid);
                    model.status = 1;
                    if (bll.Update(model))
                    {
                        sucCount += 1;
                    }
                    else
                    {
                        errorCount += 1;
                    }
                }
            }
            AddAdminLog(DTEnums.ActionEnum.Delete.ToString(), "批量审核人员服务" + sucCount + "条，失败" + errorCount + "条"); //记录日志
            JscriptMsg("保存成功" + sucCount + "条，失败" + errorCount + "条！", Utils.CombUrlTxt("docservicemanage.aspx", "keywords={0}", this.keywords));
        }
        //批量删除
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            ChkAdminLevel("docservicemanage", DTEnums.ActionEnum.Audit.ToString()); //检查权限
            int sucCount = 0;
            int errorCount = 0;
            BLL.yjr_doctor_vsbusiness bll = new BLL.yjr_doctor_vsbusiness();

            for (int i = 0; i < rptList.Items.Count; i++)
            {
                int id = Convert.ToInt32(((HiddenField)rptList.Items[i].FindControl("hidId")).Value);
                int did = Convert.ToInt32(((HiddenField)rptList.Items[i].FindControl("hiddid")).Value);
                int bid = Convert.ToInt32(((HiddenField)rptList.Items[i].FindControl("hidbid")).Value);
                CheckBox cb = (CheckBox)rptList.Items[i].FindControl("chkId");
                if (cb.Checked)
                {
                    var model = bll.GetModel(did, bid);
                    model.status = 0;//取消审核
                    if (bll.Update(model))
                    {
                        sucCount += 1;
                    }
                    else
                    {
                        errorCount += 1;
                    }
                }
            }
            AddAdminLog(DTEnums.ActionEnum.Delete.ToString(), "批量取消审核人员服务" + sucCount + "条，失败" + errorCount + "条"); //记录日志
            JscriptMsg("保存成功" + sucCount + "条，失败" + errorCount + "条！", Utils.CombUrlTxt("docservicemanage.aspx", "keywords={0}", this.keywords));
        }

        protected string GetPrice(decimal price)
        {
            if (price > 0)
            {
                return (price / 100).ToString();
            }
            else
            {
                return "-";
            }
        }

        protected string GetStatus(int status)
        {
            string result = "";
            switch (status)
            {
                case 0:
                    result = "已申请";
                    break;
                case 1:
                    result = "已审核";
                    break;
                default:
                    result = "已申请";
                    break;
            }

            return result;
        }
        
    }
}