﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="doclist.aspx.cs" Inherits="DTcms.Web.admin.docmanage.doclist" %>

<%@ Import Namespace="DTcms.Common" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,initial-scale=1.0,user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <title>医生列表</title>
    <link rel="stylesheet" type="text/css" href="../../scripts/artdialog/ui-dialog.css" />
    <link rel="stylesheet" type="text/css" href="../../css/pagination.css" />
    <link rel="stylesheet" type="text/css" href="../skin/icon/iconfont.css" />
    <link rel="stylesheet" type="text/css" href="../skin/default/style.css" />
    <script type="text/javascript" src="../../scripts/jquery/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="../../scripts/artdialog/dialog-plus-min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/laymain.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/common.js"></script>
    <script type="text/javascript">
        function create_account(id) {
            var text = "创建成功！";
            var type = "auto";
           

            $.post("doctormanage.ashx", { action: "create", id: id }, function (result) {

                console.log(result);
                if (result.ResultCode == "1") {
                    jsdialog("提示", text, "doclist.aspx", "");
                }
                else {
                    jsdialog("提示", result.ResultMessage, "doclist.aspx", "");
                }

            });
        }

        function show(id)
        {
            //dialog({ title: '.', content: "url:doc_qualification.aspx?id="+this.title }).showModal();

            ShowMaxDialog("详情", "doc_qualification.aspx?id=" + id);
        }

        function verify(id)
        {
            dialog({
                title: '提示',
                content: "确定审核此信息吗？",
                okValue: '确定',
                ok: function () {
                    $.post("doctormanage.ashx", { action: "verify", id: id }, function (result) {

                        console.log(result);
                        if (result.ResultCode == "1") {
                            jsdialog("提示", result.ResultMessage, "doclist.aspx", "");
                        }
                        else {
                            jsdialog("提示", result.ResultMessage, "doclist.aspx", "");
                        }

                    });
                },
                cancelValue: '取消',
                cancel: function () { }
            }).showModal();
        }
    </script>
</head>

<body class="mainbody">
    <form id="form1" runat="server">
        <!--导航栏-->
        <div class="location">
            <a href="javascript:history.back(-1);" class="back"><i class="iconfont icon-up"></i><span>返回上一页</span></a>
            <a href="../center.aspx" class="home"><i class="iconfont icon-home"></i><span>首页</span></a>
            <i class="arrow iconfont icon-arrow-right"></i>
            <span>管理员列表</span>
        </div>
        <!--/导航栏-->

        <!--工具栏-->
        <div id="floatHead" class="toolbar-wrap">
            <div class="toolbar">
                <div class="box-wrap">
                    <a class="menu-btn"><i class="iconfont icon-more"></i></a>
                    <div class="l-list">
                        <ul class="icon-list">
                            <li><a href="docedit.aspx?action=<%=DTEnums.ActionEnum.Add %>"><i class="iconfont icon-close"></i><span>新增</span></a></li>
                            <li><a href="javascript:;" onclick="checkAll(this);"><i class="iconfont icon-check"></i><span>全选</span></a></li>
                            <li>
                                <asp:LinkButton ID="btnDelete" runat="server" OnClientClick="return ExePostBack('btnDelete');" OnClick="btnDelete_Click"><i class="iconfont icon-delete"></i><span>删除</span></asp:LinkButton></li>
                        </ul>
                    </div>
                    <div class="r-list">
                        <asp:TextBox ID="txtKeywords" runat="server" CssClass="keyword" />
                        <asp:LinkButton ID="lbtnSearch" runat="server" CssClass="btn-search" OnClick="btnSearch_Click"><i class="iconfont icon-search"></i></asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
        <!--/工具栏-->

        <!--列表-->
        <div class="table-container">
            <asp:Repeater ID="rptList" runat="server">
                <HeaderTemplate>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="ltable">
                        <tr>
                            <th width="8%">选择</th>
                            <th align="left">姓名</th>
                            <th align="left">医院</th>
                            <th align="left">科室</th>
                            <th align="left" width="8%">职称</th>


                            <th align="left" width="12%">电话</th>
                            <th align="left" width="12%">身份证</th>
                            <th align="left" width="5%">性别</th>

                            <th align="left" width="8%">资质</th>
                            <th width="8%">状态</th>
                            <th width="8%">操作</th>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td align="center">
                            <asp:CheckBox ID="chkId" CssClass="checkall" runat="server" Style="vertical-align: middle;" />
                            <asp:HiddenField ID="hidId" Value='<%#Eval("id")%>' runat="server" />
                        </td>
                        <td><a href="docedit.aspx?action=<%#DTEnums.ActionEnum.Edit %>&id=<%#Eval("id")%>"><%# Eval("name") %></a></td>
                        <td><%# Eval("hospitalname") %></td>
                        <td><%#Eval("department_name")%></td>

                        <td><%#Eval("titlename")%></td>


                        <td><%# Eval("mobile") %></td>
                        <td><%# Eval("idcard") %></td>
                        <td><%# Eval("gender") %></td>

                        <td><a href="doc_qualification.aspx?id=<%#Eval("id")%>" target="_blank" title="<%#Eval("id")%>">查看</a></td>
                        <td align="center"><%#Eval("status").ToString().Trim() == "0" ? "未审核" : "已审核"%></td>
                        <td align="center"><a href="docedit.aspx?action=<%#DTEnums.ActionEnum.Edit %>&id=<%#Eval("id")%>">修改</a>
                            <a href="#" title="<%#Eval("id")%>" onclick="verify(this.title)">审核</a>
                            <a href="#" onclick="create_account(<%#Eval("id")%>)" style="<%#ShowAccount(Eval("id").ToString())%>">创建账号</a>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    <%#rptList.Items.Count == 0 ? "<tr><td align=\"center\" colspan=\"8\">暂无记录</td></tr>" : ""%>
  </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
        <!--/列表-->

        <!--内容底部-->
        <div class="line20"></div>
        <div class="pagelist">
            <div class="l-btns">
                <span>显示</span><asp:TextBox ID="txtPageNum" runat="server" CssClass="pagenum" onkeydown="return checkNumber(event);"
                    OnTextChanged="txtPageNum_TextChanged" AutoPostBack="True"></asp:TextBox><span>条/页</span>
            </div>
            <div id="PageContent" runat="server" class="default"></div>
        </div>
        <!--/内容底部-->
    </form>
</body>
</html>
