﻿using DTcms.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DTcms.Web.admin.consulting
{
    public partial class consulation_show : Page
    {
        private int id = 0;
        protected Model.yjr_zx_order model = new Model.yjr_zx_order();
        protected void Page_Load(object sender, EventArgs e)
        {
            this.id = DTRequest.GetQueryInt("id");
            if (this.id == 0)
            {
                //JscriptMsg("传输参数不正确！", "back");
                return;
            }
            if (!new BLL.yjr_zx_order().Exists(this.id))
            {
                //JscriptMsg("记录不存在或已被删除！", "back");
                return;
            }
            if (!Page.IsPostBack)
            {
                //ChkAdminLevel("order_list", DTEnums.ActionEnum.View.ToString()); //检查权限
                ShowInfo(this.id);
            }
        }

        private void ShowInfo(int _id)
        {
            BLL.yjr_zx_order bll = new BLL.yjr_zx_order();

            model = bll.GetModel(_id);

            var order_list = bll.GetModelList("id=" + this.id);

            rptList.DataSource = order_list;
            rptList.DataBind();

        }
    }
}