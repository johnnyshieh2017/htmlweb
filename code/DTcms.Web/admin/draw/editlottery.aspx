﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="editlottery.aspx.cs" Inherits="DTcms.Web.admin.draw.editlottery" %>


<%@ Import namespace="DTcms.Common" %>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>编辑活动</title>
<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,initial-scale=1.0,user-scalable=no" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<link rel="stylesheet" type="text/css" href="../../scripts/artdialog/ui-dialog.css" />
<link rel="stylesheet" type="text/css" href="../skin/icon/iconfont.css" />
<link rel="stylesheet" type="text/css" href="../skin/default/style.css" />
<script type="text/javascript" charset="utf-8" src="../../scripts/jquery/jquery-1.11.2.min.js"></script>
<script type="text/javascript" charset="utf-8" src="../../scripts/jquery/Validform_v5.3.2_min.js"></script>
<script type="text/javascript" charset="utf-8" src="../../scripts/artdialog/dialog-plus-min.js"></script>
<script type="text/javascript" charset="utf-8" src="../../scripts/webuploader/webuploader.min.js"></script>
<script type="text/javascript" charset="utf-8" src="../js/uploader.js"></script>
<script type="text/javascript" charset="utf-8" src="../js/common.js"></script>
<script type="text/javascript" charset="utf-8" src="../js/laymain.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../scripts/datepicker/WdatePicker.js"></script>
<script type="text/javascript">
    $(function () {
        //初始化表单验证
        $("#form1").initValidform();
        //初始化上传控件
        $(".upload-img").InitUploader({ sendurl: "../../tools/upload_ajax.ashx", swf: "../../scripts/webuploader/uploader.swf" });
    });
</script>
</head>

<body class="mainbody">
<form id="form1" runat="server">
<!--导航栏-->
<div class="location">
  <a href="index.aspx" class="back"><i class="iconfont icon-up"></i><span>返回列表页</span></a>
  <a href="../center.aspx"><i class="iconfont icon-home"></i><span>首页</span></a>
  <i class="arrow iconfont icon-arrow-right"></i>
  <a href="site_list.aspx"><span>抽奖活动管理</span></a>
  <i class="arrow iconfont icon-arrow-right"></i>
  <span>编辑活动</span>
</div>
<div class="line10"></div>
<!--/导航栏-->

<!--内容-->
<div id="floatHead" class="content-tab-wrap">
  <div class="content-tab">
    <div class="content-tab-ul-wrap">
      <ul>
        <li><a class="selected" href="javascript:;">编辑活动</a></li>
       
      </ul>
    </div>
  </div>
</div>

<div class="tab-content">
  
   
  <dl>
    <dt>活动标题</dt>
    <dd>
      <asp:TextBox ID="txtTitle" runat="server" CssClass="input normal"  datatype="*2-100" sucmsg=" "></asp:TextBox>
      <span class="Validform_checktip">*标题，允许中文。</span>
    </dd>
  </dl>
  <dl>
    <dt>说明</dt>
    <dd>
      <asp:TextBox ID="txtContent" runat="server" CssClass="input" TextMode="MultiLine"></asp:TextBox>
      
    </dd>
  </dl>
  <dl>
    <dt>开始时间</dt>
    <dd>
      <asp:TextBox ID="txtStartTime" runat="server" CssClass="input rule-date-input" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm'})" datatype="/^\s*$|^\d{4}\-\d{1,2}\-\d{1,2}\s{1}(\d{1,2}:){1}\d{1,2}$/" errormsg="请选择正确的日期" sucmsg=" " ></asp:TextBox>
     
    </dd>
  </dl>
     <dl>
    <dt>结束时间</dt>
    <dd>
      <asp:TextBox ID="txtEndTime" runat="server" CssClass="input rule-date-input" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm'})" datatype="/^\s*$|^\d{4}\-\d{1,2}\-\d{1,2}\s{1}(\d{1,2}:){1}\d{1,2}$/" errormsg="请选择正确的日期" sucmsg=" " ></asp:TextBox>
     
    </dd>
  </dl>
  <dl>
    <dt>状态</dt>
    <dd>
       <asp:RadioButtonList ID="rblis_status" runat="server" RepeatDirection="Horizontal"
                                RepeatLayout="Flow">
                                <asp:ListItem Selected="True" Value="0">未发布</asp:ListItem>
                                <asp:ListItem Value="1">已发布</asp:ListItem>
           <asp:ListItem Value="2">已暂停</asp:ListItem>
                            </asp:RadioButtonList>
      <span class="Validform_checktip">*</span>
    </dd>
  </dl>
  <dl>
    <dt>默认未中奖提示</dt>
    <dd>
      <asp:TextBox ID="txtDefaultMsg" runat="server" CssClass="input normal"  datatype="*2-100" sucmsg=" "></asp:TextBox>
      <span class="Validform_checktip">*默认未中奖提示，允许中文。</span>
    </dd>
  </dl>

</div>


<!--/内容-->

<!--工具栏-->
<div class="page-footer">
  <div class="btn-wrap">
    <asp:Button ID="btnSubmit" runat="server" Text="提交保存" CssClass="btn" onclick="btnSubmit_Click" />
    <input name="btnReturn" type="button" value="返回上一页" class="btn yellow" onclick="javascript:history.back(-1);" />
  </div>
</div>
<!--/工具栏-->

</form>
</body>
</html>

