﻿using DTcms.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DTcms.Web.admin.draw
{
    public partial class PrizeList : Web.UI.ManagePage
    {
        protected int channel_id;
        protected int totalCount;
        protected int page;
        protected int pageSize;

        protected int category_id;
        protected string channel_name = string.Empty;
        protected string property = string.Empty;
        protected string keywords = string.Empty;
        protected string prolistview = string.Empty;
        protected string action = "";
        protected string lotteryid = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            this.channel_id = DTRequest.GetQueryInt("channel_id");
            this.category_id = DTRequest.GetQueryInt("category_id");
            this.keywords = DTRequest.GetQueryString("keywords");
            this.property = DTRequest.GetQueryString("property");
            action = DTRequest.GetQueryString("action");
            lotteryid = DTRequest.GetQueryString("lid");
            this.channel_name = new BLL.site_channel().GetChannelName(this.channel_id); //取得频道名称
            this.pageSize = GetPageSize(10); //每页数量
            this.prolistview = Utils.GetCookie("PrizeList_view"); //显示方式

            if (!Page.IsPostBack)
            {


                BindSubList();
                if (action == "Edit")
                {
                    BindData(DTRequest.GetQueryString("id"));
                }
                else if (action == "Delete")
                {
                    DeleteData(DTRequest.GetQueryString("id"));
                }


                RptBind(string.Concat("id>0 and lotteryid='" + lotteryid + "'", this.CombSqlTxt(this.keywords, this.property)), "sort asc");


            }
        }

        void DeleteData(string id)
        {
            DTcms.BLL.draw_Prize bll = new BLL.draw_Prize();
            bll.Delete(int.Parse(id));
        }

        void BindData(string id)
        {
            hdItemId.Value = id;
            DTcms.Model.draw_Prize model = new Model.draw_Prize();
            DTcms.BLL.draw_Prize bll = new BLL.draw_Prize();
            model = bll.GetModel(int.Parse(id));

            if (model != null)
            {
                txtTitle.Text = model.Prize_title;
                txtName.Text = model.Prize_name;

                txtSort.Text = model.sort.ToString();
                txtcount.Text = model.Prize_count.ToString();
            }
        }

        void BindSubList()
        {
            BLL.draw_lotteryList bll = new BLL.draw_lotteryList();
            var list = bll.GetList(this.pageSize, this.page, " status=0 and id=" + DTRequest.GetQueryInt("lid"), " createtime desc", out this.totalCount);
            //ddlvotelist.DataSource = list;
            for (int i = 0; i < list.Tables[0].Rows.Count; i++)
            {
                ddlvotelist.Items.Add(new ListItem { Text = list.Tables[0].Rows[i]["name"].ToString(), Value = list.Tables[0].Rows[i]["id"].ToString() });
            }
        }

        void RptBind(string _strWhere, string _orderby)
        {
            this.page = DTRequest.GetQueryInt("page", 1);
            //this.txtKeywords.Text = this.keywords;

            BLL.draw_Prize bll = new BLL.draw_Prize();
            this.rptList.DataSource = bll.GetList(this.pageSize, this.page, _strWhere, _orderby, out this.totalCount);
            this.rptList.DataBind();
            this.txtPageNum.Text = this.pageSize.ToString();
            string[] strArrays = new string[4];
            strArrays[0] = this.keywords;
            strArrays[1] = this.property;
            strArrays[2] = "__id__";
            strArrays[3] = DTRequest.GetQueryInt("lid").ToString();
            string str = Utils.CombUrlTxt("PrizeList.aspx", "keywords={0}&property={1}&page={2}&lid={3}", strArrays);
            this.PageContent.InnerHtml = Utils.OutPageList(this.pageSize, this.page, this.totalCount, str, 8);
        }

        #region 返回图文每页数量=========================
        private int GetPageSize(int _default_size)
        {
            int _pagesize;
            if (int.TryParse(Utils.GetCookie("PrizeList_page_size", "DTcmsPage"), out _pagesize))
            {
                if (_pagesize > 0)
                {
                    return _pagesize;
                }
            }
            return _default_size;
        }
        #endregion

        protected string CombSqlTxt(string _keywords, string _property)
        {
            StringBuilder stringBuilder = new StringBuilder();
            _keywords = _keywords.Replace("'", "");
            bool flag = string.IsNullOrEmpty(_keywords);
            if (!flag)
            {
                stringBuilder.Append(string.Concat(" and Prize_name like '%", _keywords, "%'"));
            }
            //flag = string.IsNullOrEmpty(_property);
            //if (!flag)
            //{
            //    stringBuilder.Append(string.Concat(" and type=", _property));
            //}
            string str = stringBuilder.ToString();
            return str;
        }


        protected void lbtnSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect(Utils.CombUrlTxt("PrizeList.aspx", "keywords={0}", ""));//txtKeywords.Text
        }

        //设置分页数量
        protected void txtPageNum_TextChanged(object sender, EventArgs e)
        {
            int _pagesize;
            if (int.TryParse(txtPageNum.Text.Trim(), out _pagesize))
            {
                if (_pagesize > 0)
                {
                    Utils.WriteCookie("PrizeList_page_size", "DTcmsPage", _pagesize.ToString(), 14400);
                }
            }
            Response.Redirect(Utils.CombUrlTxt("PrizeList.aspx", "keywords={0}&lid={1}", this.keywords, DTRequest.GetQueryInt("lid").ToString()));
        }

        protected void rptList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            //添加问题 选项
            string lotteryid = ddlvotelist.SelectedValue;
            string title = txtTitle.Text.Trim();
            string name = txtName.Text.Trim();// ddltype.SelectedValue;
            string sort = txtSort.Text.Trim();

            if (lotteryid == "0")
            {
                ClientScript.RegisterClientScriptBlock(Page.GetType(), "JsPrint", "<script>alert('请选择问题')</script>", false);

                return;
            }
            if (title == "" || sort == "")
            {
                ClientScript.RegisterClientScriptBlock(Page.GetType(), "JsPrint", "<script>alert('请填写名称和序号')</script>", false);

                return;
            }
            DTcms.Model.draw_Prize model = new DTcms.Model.draw_Prize();
            DTcms.BLL.draw_Prize sub_bll = new BLL.draw_Prize();

            model.lotteryid = int.Parse(lotteryid);
            //model.type = int.Parse(type);
            model.Prize_title = txtTitle.Text.Trim();
            model.Prize_name = txtName.Text.Trim();
            model.sort = int.Parse(txtSort.Text.Trim());
            model.Prize_count = int.Parse(txtcount.Text.Trim());

            //if (fileImg.FileContent.Length > 0)
            //{// 有上传图片

            //}

            

            if (hdItemId.Value != "")
            {
                model.id = int.Parse(hdItemId.Value);
                if (fileImg.FileContent.Length > 0)
                {
                    model.imgurl = upImg();
                }
                else
                {
                    model.imgurl = sub_bll.GetModel(int.Parse(hdItemId.Value)).imgurl;
                }
                
                sub_bll.Update(model);

            }
            else
            {
                model.imgurl = upImg();
                sub_bll.Add(model);
            }
            txtTitle.Text = "";
            txtSort.Text = "";
            txtName.Text = "";
            hdItemId.Value = "";
            JscriptMsg("保存成功！", string.Empty);

            RptBind(string.Concat("id>0 and lotteryid='" + lotteryid + "'", this.CombSqlTxt(this.keywords, this.property)), "sort asc");
        }


        private string upImg()
        {
            string saveName ="prize"+ System.DateTime.Now.ToString("yyyyMMddHHmmssffff");
            string extention = ".png";
            if (fileImg.FileContent.Length > 0)
            {
                String savePath = Server.MapPath("/upload/");
                fileImg.SaveAs(savePath + saveName + extention);
                return "/upload/" + saveName + extention;
            }
            else
            {
                return "";
            }
        }


        protected string GetvoteName(string vote_id)
        {
            DTcms.BLL.vote bll = new BLL.vote();
            var model = bll.GetModel(int.Parse(vote_id));
            if (model != null)
            {
                return model.title;
            }
            else
            {
                return "";
            }

        }
        protected string GetsubjectName(string vote_id)
        {
            DTcms.BLL.vote_subject bll = new BLL.vote_subject();
            var model = bll.GetModel(int.Parse(vote_id));
            if (model != null)
            {
                return model.title;
            }
            else
            {
                return "";
            }

        }

    }
}