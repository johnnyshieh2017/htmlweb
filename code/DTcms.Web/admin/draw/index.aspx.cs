﻿using System;
using System.Linq;
using System.Collections;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DTcms.Common;
using NLog;

namespace DTcms.Web.admin.draw
{
    public partial class index : Web.UI.ManagePage
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        protected int channel_id;
        protected int totalCount;
        protected int page;
        protected int pageSize;

        protected int category_id;
        protected string channel_name = string.Empty;
        protected string property = string.Empty;
        protected string keywords = string.Empty;
        protected string prolistview = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            this.keywords = DTRequest.GetQueryString("keywords");
            this.property = DTRequest.GetQueryString("property");

            //if (channel_id == 0)
            //{
            //    JscriptMsg("频道参数不正确！", "back");
            //    return;
            //}
            this.channel_name = new BLL.site_channel().GetChannelName(this.channel_id); //取得频道名称
            this.pageSize = GetPageSize(10); //每页数量
            this.prolistview = Utils.GetCookie("lottery_list_view"); //显示方式
            if (!Page.IsPostBack)
            {
             

                RptBind(string.Concat("id>0", this.CombSqlTxt(this.keywords, this.property)), "createtime desc");
            }
        }

        void RptBind(string _strWhere, string _orderby)
        {
            this.page = DTRequest.GetQueryInt("page", 1);
            this.txtKeywords.Text = this.keywords;

            BLL.draw_lotteryList bll = new BLL.draw_lotteryList();
            this.rptList.DataSource = bll.GetList(this.pageSize, this.page, _strWhere, _orderby, out this.totalCount);
            this.rptList.DataBind();
            this.txtPageNum.Text = this.pageSize.ToString();
            string[] strArrays = new string[3];
            strArrays[0] = this.keywords;
            strArrays[1] = this.property;
            strArrays[2] = "__id__";
            string str = Utils.CombUrlTxt("index.aspx", "keywords={0}&property={1}&page={2}", strArrays);
            this.PageContent.InnerHtml = Utils.OutPageList(this.pageSize, this.page, this.totalCount, str, 8);
        }

        #region 返回图文每页数量=========================
        private int GetPageSize(int _default_size)
        {
            int _pagesize;
            if (int.TryParse(Utils.GetCookie("lottery_page_size", "DTcmsPage"), out _pagesize))
            {
                if (_pagesize > 0)
                {
                    return _pagesize;
                }
            }
            return _default_size;
        }
        #endregion

        protected string CombSqlTxt(string _keywords, string _property)
        {
            StringBuilder stringBuilder = new StringBuilder();
            _keywords = _keywords.Replace("'", "");
            bool flag = string.IsNullOrEmpty(_keywords);
            if (!flag)
            {
                stringBuilder.Append(string.Concat(" and name like '%", _keywords, "%'"));
            }
           
           
            string str = stringBuilder.ToString();
            return str;
        }
        protected void lbtnSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect(Utils.CombUrlTxt("index.aspx", "keywords={0}", txtKeywords.Text));
        }

        //设置分页数量
        protected void txtPageNum_TextChanged(object sender, EventArgs e)
        {
            int _pagesize;
            if (int.TryParse(txtPageNum.Text.Trim(), out _pagesize))
            {
                if (_pagesize > 0)
                {
                    Utils.WriteCookie("lottery_page_size", "DTcmsPage", _pagesize.ToString(), 14400);
                }
            }
            Response.Redirect(Utils.CombUrlTxt("index.aspx", "keywords={0}", this.keywords));
        }

        protected void rptList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

        }

        protected void btnClose_Click(object sender, EventArgs e)
        {//
            int sucCount = 0;
            int errorCount = 0;

            DTcms.BLL.draw_lotteryList bll = new BLL.draw_lotteryList();
            for (int i = 0; i < rptList.Items.Count; i++)
            {
                int id = Convert.ToInt32(((HiddenField)rptList.Items[i].FindControl("hidId")).Value);
                CheckBox cb = (CheckBox)rptList.Items[i].FindControl("chkId");
                if (cb.Checked)
                {
                    Model.draw_lotteryList model = bll.GetModel(id);
                    model.status = -1;
                    bll.Update(model);
                }
            }
            AddAdminLog(DTEnums.ActionEnum.Edit.ToString(), "暂停活动" + sucCount + "条"); //记录日志

            RptBind(string.Concat("id>0", this.CombSqlTxt(this.keywords, this.property)), "createtime desc");
        }

        protected void btnPublish_Click(object sender, EventArgs e)
        {//
            int sucCount = 0;
            int errorCount = 0;
            DTcms.BLL.draw_lotteryList bll = new BLL.draw_lotteryList();
            for (int i = 0; i < rptList.Items.Count; i++)
            {
                int id = Convert.ToInt32(((HiddenField)rptList.Items[i].FindControl("hidId")).Value);
                CheckBox cb = (CheckBox)rptList.Items[i].FindControl("chkId");
                if (cb.Checked)
                {
                    Model.draw_lotteryList model = bll.GetModel(id);
                    if (model.status == 0)
                    {//只有在未发布==》发布
                        //抽奖算法
                        Draw(id);
                    }
                    model.status = 1;
                    bll.Update(model);
                }
            }
            AddAdminLog(DTEnums.ActionEnum.Delete.ToString(), "发布活动" + sucCount + "条"); //记录日志

            RptBind(string.Concat("id>0", this.CombSqlTxt(this.keywords, this.property)), "createtime desc");
        }

        private void Draw(int lotteryid)
        {
            //验证奖品
            //验证抽奖资格人
            BLL.draw_Prize prizebll = new BLL.draw_Prize();
            BLL.draw_pond pondbll = new BLL.draw_pond();
            var prizecount = prizebll.GetRecordCount(" lotteryid=" + lotteryid);

            var pondcount = pondbll.GetRecordCount(" lotteryid="+ lotteryid);
            var pondopen = pondbll.GetRecordCount(" lotteryid=" + lotteryid+ " and Prizeid!=0 ");//不为零，则已经开奖

            if (prizecount == 0)
            {
                AddAdminLog(DTEnums.ActionEnum.Build.ToString(), "启动开奖，但未设置奖品，lotteryid="+ lotteryid); //记录日志
                return;
            }

            if (pondcount < 1)
            {
                AddAdminLog(DTEnums.ActionEnum.Build.ToString(), "启动开奖，但未导入抽奖资格人，lotteryid=" + lotteryid); //记录日志
                return;
            }
            if (pondopen >0)
            {
                AddAdminLog(DTEnums.ActionEnum.Build.ToString(), "启动开奖，但已存在中奖人员，lotteryid=" + lotteryid); //记录日志
                return;
            }

            //kaijiang

            try
            {
                var prizelist = prizebll.GetModelList(" lotteryid=" + lotteryid);//奖品数据
                List<Model.draw_Prize> prizelisttemp = new List<Model.draw_Prize>();//奖品集合
                for (int i = 0; i < prizelist.Count; i++)
                {
                    //s += "person:"+list[i].id+"|奖："+ prizelist[i].Prize_title;
                    for (int k = 0; k < prizelist[i].Prize_count; k++)
                    {
                        prizelisttemp.Add(prizelist[i]);
                    }

                }

                prizelisttemp = prizelisttemp.OrderBy(c => Guid.NewGuid()).ToList();//奖品随机乱序


                var pondlist = pondbll.GetModelList(" lotteryid=" + lotteryid);
                pondlist = pondlist.OrderBy(c => Guid.NewGuid()).ToList();//参与抽奖随机乱序

                if (pondlist.Count >= prizelisttemp.Count)
                {//奖品数小于等于抽奖人数
                    for (int i = 0; i < prizelisttemp.Count; i++)
                    {
                        var pond = pondbll.GetModel(pondlist[i].id);
                        pond.Prizeid = prizelisttemp[i].id;
                        pond.status = 1;
                        pondbll.Update(pond);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("抽奖kaijiang， private void Draw(int lotteryid) eroor：" + ex.Message);
            }
        }

        protected string GetPondCount(string lid)
        {
            BLL.draw_pond pondbll = new BLL.draw_pond();
            return pondbll.GetRecordCount(" lotteryid="+lid).ToString();
        }

        protected string GetPrizeOpenPondCount(string lid)
        {
            string result = "";
            BLL.draw_pond pondbll = new BLL.draw_pond();
            BLL.draw_Prize prizebll = new BLL.draw_Prize();

            int pcount = 0;

            var prizelist = prizebll.GetModelList(" lotteryid=" + lid);
            foreach (var item in prizelist)
            {
                pcount += item.Prize_count;
            }

            var pondopenCount = pondbll.GetRecordCount(" lotteryid=" + lid + " and status=2 and Prizeid>0 ");
            result = pcount + "/" + pondopenCount;

            return result;
        }

        protected string GetStatus(string status)
        {
            string s = "";
            switch (status)
            {
                case "0":
                    s = "未发布";
                    break;
                case "1":
                    s = "已发布";
                    break;
                case "2":
                    s = "已暂停";
                    break;
                default:
                    break;
            }

            return s;
        }
    }
}