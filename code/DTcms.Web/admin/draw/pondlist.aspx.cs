﻿using DTcms.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DTcms.Web.admin.draw
{
    public partial class pondlist : Web.UI.ManagePage
    {
        protected int channel_id;
        protected int totalCount;
        protected int page;
        protected int pageSize;

        protected int category_id;
        protected string channel_name = string.Empty;
        protected string property = string.Empty;
        protected string keywords = string.Empty;
        protected string prolistview = string.Empty;
        protected string action = "";
        protected string lotteryid = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            this.channel_id = DTRequest.GetQueryInt("channel_id");
            this.category_id = DTRequest.GetQueryInt("category_id");
            this.keywords = DTRequest.GetQueryString("keywords");
            this.property = DTRequest.GetQueryString("property");
            action = DTRequest.GetQueryString("action");
            lotteryid = DTRequest.GetQueryString("lid");
            this.channel_name = new BLL.site_channel().GetChannelName(this.channel_id); //取得频道名称
            this.pageSize = GetPageSize(10); //每页数量
            this.prolistview = Utils.GetCookie("pondlist_view"); //显示方式

            if (!Page.IsPostBack)
            {


               // BindSubList();
                //if (action == "Edit")
                //{
                //    BindData(DTRequest.GetQueryString("id"));
                //}
                //else if (action == "Delete")
                //{
                //    DeleteData(DTRequest.GetQueryString("id"));
                //}


                RptBind(string.Concat("id>0 and status>1 and Prizeid>0 and lotteryid='" + lotteryid + "'", this.CombSqlTxt(this.keywords, this.property)), "Prizeid asc");


            }
        }

        void RptBind(string _strWhere, string _orderby)
        {
            this.page = DTRequest.GetQueryInt("page", 1);
            //this.txtKeywords.Text = this.keywords;

            BLL.draw_pond bll = new BLL.draw_pond();
            this.rptList.DataSource = bll.GetList(this.pageSize, this.page, _strWhere, _orderby, out this.totalCount);
            this.rptList.DataBind();
            this.txtPageNum.Text = this.pageSize.ToString();
            string[] strArrays = new string[4];
            strArrays[0] = this.keywords;
            strArrays[1] = this.property;
            strArrays[2] = "__id__";
            strArrays[3] = DTRequest.GetQueryInt("lid").ToString();
            string str = Utils.CombUrlTxt("pondlist.aspx", "keywords={0}&property={1}&page={2}&lid={3}", strArrays);
            this.PageContent.InnerHtml = Utils.OutPageList(this.pageSize, this.page, this.totalCount, str, 8);
        }

        #region 返回图文每页数量=========================
        private int GetPageSize(int _default_size)
        {
            int _pagesize;
            if (int.TryParse(Utils.GetCookie("pondlist_page_size", "DTcmsPage"), out _pagesize))
            {
                if (_pagesize > 0)
                {
                    return _pagesize;
                }
            }
            return _default_size;
        }
        #endregion

        protected string CombSqlTxt(string _keywords, string _property)
        {
            StringBuilder stringBuilder = new StringBuilder();
            _keywords = _keywords.Replace("'", "");
            bool flag = string.IsNullOrEmpty(_keywords);
            if (!flag)
            {
                stringBuilder.Append(string.Concat(" and telephone like '%", _keywords, "%'"));
            }
            //flag = string.IsNullOrEmpty(_property);
            //if (!flag)
            //{
            //    stringBuilder.Append(string.Concat(" and type=", _property));
            //}
            string str = stringBuilder.ToString();
            return str;
        }


        protected void lbtnSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect(Utils.CombUrlTxt("pondlist.aspx", "keywords={0}", ""));//txtKeywords.Text
        }

        //设置分页数量
        protected void txtPageNum_TextChanged(object sender, EventArgs e)
        {
            int _pagesize;
            if (int.TryParse(txtPageNum.Text.Trim(), out _pagesize))
            {
                if (_pagesize > 0)
                {
                    Utils.WriteCookie("pondlist_page_size", "DTcmsPage", _pagesize.ToString(), 14400);
                }
            }
            Response.Redirect(Utils.CombUrlTxt("pondlist.aspx", "keywords={0}&lid={1}", this.keywords, DTRequest.GetQueryInt("lid").ToString()));
        }

        protected void rptList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

        }

        protected string GetPrize(string id)
        {
            string s = "";
            BLL.draw_Prize bll = new BLL.draw_Prize();
            var prize = bll.GetModel(int.Parse(id));
            if(prize!=null)
            {
                s = prize.Prize_title + "--" + prize.Prize_name;
            }
            return s;

        }
    }
}