﻿using System;
using System.Collections;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DTcms.Common;
using Aspose.Cells;

namespace DTcms.Web.admin.draw
{
    public partial class gz_data_tool : Web.UI.ManagePage
    {
        protected string prolistview = string.Empty;
        protected string property = string.Empty;
        protected string keywords = string.Empty;

        protected string lotteryid = "";
        protected int channel_id;
        protected int totalCount;
        protected int page;
        protected int pageSize;
        protected void Page_Load(object sender, EventArgs e)
        {
            this.pageSize = GetPageSize(10); //每页数量
            this.prolistview = Utils.GetCookie("gz_data_tool_list_view"); //显示方式

            lotteryid = DTRequest.GetQueryString("lid");
            if (!Page.IsPostBack)
            {


                RptBind(string.Concat("id>0 and xingming='' and lotteryid=" + lotteryid, this.CombSqlTxt(this.keywords, this.property)), "id desc");
            }
        }

        void RptBind(string _strWhere, string _orderby)
        {
            this.page = DTRequest.GetQueryInt("page", 1);


            BLL.draw_gz_data bll = new BLL.draw_gz_data();
            this.rptList.DataSource = bll.GetList(this.pageSize, this.page, _strWhere, _orderby, out this.totalCount);
            this.rptList.DataBind();
            this.txtPageNum.Text = this.pageSize.ToString();
            string[] strArrays = new string[3];
            strArrays[0] = "";
            strArrays[1] = this.lotteryid;
            strArrays[2] = "__id__";
            string str = Utils.CombUrlTxt("gz_data_tool.aspx", "keywords={0}&lid={1}&page={2}", strArrays);
            this.PageContent.InnerHtml = Utils.OutPageList(this.pageSize, this.page, this.totalCount, str, 8);
        }

        #region 返回图文每页数量=========================
        private int GetPageSize(int _default_size)
        {
            int _pagesize;
            if (int.TryParse(Utils.GetCookie("gz_data_tool_page_size", "DTcmsPage"), out _pagesize))
            {
                if (_pagesize > 0)
                {
                    return _pagesize;
                }
            }
            return _default_size;
        }
        #endregion

        protected string CombSqlTxt(string _keywords, string _property)
        {
            StringBuilder stringBuilder = new StringBuilder();
            _keywords = _keywords.Replace("'", "");
            bool flag = string.IsNullOrEmpty(_keywords);
            if (!flag)
            {
                stringBuilder.Append(string.Concat(" and mobile like '%", _keywords, "%'"));
            }


            string str = stringBuilder.ToString();
            return str;
        }
        protected void lbtnSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect(Utils.CombUrlTxt("gz_data_tool.aspx", "keywords={0}", ""));
        }

        //设置分页数量
        protected void txtPageNum_TextChanged(object sender, EventArgs e)
        {
            int _pagesize;
            if (int.TryParse(txtPageNum.Text.Trim(), out _pagesize))
            {
                if (_pagesize > 0)
                {
                    Utils.WriteCookie("lottery_page_size", "DTcmsPage", _pagesize.ToString(), 14400);
                }
            }
            Response.Redirect(Utils.CombUrlTxt("gz_data_tool.aspx", "keywords={0}&lid={1}", this.keywords,this.lotteryid));
        }

        protected void rptList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            //
            HttpPostedFile postedFile = this.fu_gzdata.PostedFile; //获取到要上传的文件

            String fileName = DateTime.Now.ToString("yyyyMMddHHmmssff") + ".xls";//文件名

            String filePath = "/upload/";//文件保存路径

            //fileName = System.IO.Path.GetFileName(postedFile.FileName);//获取文件名称

            if (System.IO.Directory.Exists(Server.MapPath(filePath)) == false)//判断文件夹是否存在

            {

                System.IO.Directory.CreateDirectory(Server.MapPath(filePath));//如果不存在就创建file文件夹

            }

            if (System.IO.File.Exists(Server.MapPath(filePath + fileName)) == true)//判断同名文件是否存在

            {

                Page.ClientScript.RegisterStartupScript(this.GetType(), "message", "alert('同名文件已存在')", true);//弹窗提示文件已存在

            }

            else//文件不存在则保存文件

            {

                if (fileName != "")//判断前端是否有文件传过来

                {

                    String fileSuffix = System.IO.Path.GetExtension(fileName); //获取上传文件的扩展名

                    postedFile.SaveAs(System.Web.HttpContext.Current.Request.MapPath(filePath) + fileName);//保存文件至根目录下的files文件夹里

                    //读取excel
                    Workbook workbook = new Workbook(Server.MapPath(filePath + fileName));
                    Cells cells = workbook.Worksheets[0].Cells;
                    System.Data.DataTable dataTable1 = cells.ExportDataTable(1, 0, cells.MaxDataRow, cells.MaxColumn + 1);//没有标题

                    //  var dt =  new Maticsoft.Common.ExcelOperation().ReadExcel(Server.MapPath(filePath + fileName),1,0,0);// ReadExcel()
                    DTcms.BLL.draw_gz_data bll = new DTcms.BLL.draw_gz_data();

                    bll.DeleteListwhere(" lotteryid="+ lotteryid);
                    for (int i = 0; i < dataTable1.Rows.Count; i++)
                    {
                        DTcms.Model.draw_gz_data model = new DTcms.Model.draw_gz_data();
                        model.MemID = dataTable1.Rows[i][0].ToString();
                        model.MemName = dataTable1.Rows[i][1].ToString(); ;
                        model.ThirdID = dataTable1.Rows[i][2].ToString(); ;
                        model.Memlink = dataTable1.Rows[i][3].ToString(); ;
                        model.guankanshichang = DateTime.Parse(dataTable1.Rows[i][4].ToString());
                        model.mobile = dataTable1.Rows[i][5].ToString();
                        model.fangwenlaiyuan = dataTable1.Rows[i][6].ToString(); ;
                        model.fangwencishu = decimal.Parse(dataTable1.Rows[i][7].ToString()); ;
                        model.fenxiangcishu = decimal.Parse(dataTable1.Rows[i][8].ToString()); ;
                        model.liaotiancishu = decimal.Parse(dataTable1.Rows[i][9].ToString()); ;
                        model.zaixianshichang = DateTime.Parse(dataTable1.Rows[i][10].ToString()); ;
                        model.firsttime = DateTime.Parse(dataTable1.Rows[i][11].ToString()); ;
                        model.lasttime = DateTime.Parse(dataTable1.Rows[i][12].ToString()); ;
                        model.shebei = dataTable1.Rows[i][13].ToString(); ;
                        model.guankanfangshi = dataTable1.Rows[i][14].ToString(); ;
                        model.dengluIP = dataTable1.Rows[i][15].ToString(); ;
                        model.shifouzhigong = "";
                        model.sheng = "";
                        model.shi = "";
                        model.gongsi = "";
                        model.xingming = "";
                        model.lotteryid = int.Parse(lotteryid);

                        bll.Add(model);

                    }

                    //Page.ClientScript.RegisterStartupScript(this.GetType(), "message", "alert('已经导入成功')", true);//弹窗提示保存成功
                    base.JscriptMsg("已经导入成功！", "gz_data_tool.aspx?lid="+ lotteryid, "Success");
                }


                else

                {

                    //Page.ClientScript.RegisterStartupScript(this.GetType(), "message", "alert('请选择文件')", true);//弹窗提示未选择文件
                    base.JscriptMsg("请选择文件！", "", "Error");
                }
            }
        }

        protected void btnSelect_Click(object sender, EventArgs e)
        {
            //筛选

            //按条件 标记 筛选数据
            BLL.draw_zg_data zgbll = new BLL.draw_zg_data();
            BLL.draw_gz_data gzbll = new BLL.draw_gz_data();
            if (cbxPass.Checked)
            {
                var list = zgbll.GetModelList("");//所有职工
                foreach (var item in list)
                {
                    var gz = gzbll.GetRecordCount(" lotteryid=" + lotteryid + " and mobile='" + item.shouji + "'");//此次活动人员
                    if (gz > 0)
                    {
                        //var gzmodel = gzbll.GetModelList(" lotteryid="+ lotteryid + " and mobile='" + item.shouji + "'");
                        //if (gzmodel != null && gzmodel.Count > 0)
                        //{
                        //    //gzmodel[0].xingming = "1";
                        //    //gzbll.Update(gzmodel[0]);

                        //}
                        gzbll.updatebywhere(" lotteryid=" + lotteryid + " and mobile='" + item.shouji + "'", "1");
                    }
                }

            }
            //按观看时长 标记

            int time = 0;
            int.TryParse(txtTime.Text.Trim(), out time);
            if (time != 0)
            {
                gzbll.updatebywhere(" lotteryid=" + lotteryid + " and DateDiff (N,CONVERT(datetime,'1899-12-31',101), guankanshichang) <" + time);
            }


            var list2 = gzbll.GetModelList(" xingming='' and lotteryid="+ lotteryid);
            BLL.draw_pond pondbll = new BLL.draw_pond();
            pondbll.Deletewhere(" status=0 and lotteryid="+lotteryid);
            foreach (var item in list2)
            {
                var pond = new Model.draw_pond();
                pond.lotteryid = Convert.ToInt32( item.lotteryid);
                pond.Idnumber = item.id.ToString();
                pond.telephone = item.mobile;
                pond.createdate = System.DateTime.Now;
                pond.Prizeid = 0;//
                pond.status = 0;
                pondbll.Add(pond);
            }

            RptBind(string.Concat("id>0 and xingming='' and lotteryid=" + lotteryid, this.CombSqlTxt(this.keywords, this.property)), "id desc");
        }
    }
}