﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DTcms.Common;

namespace DTcms.Web.admin.draw
{
    public partial class editlottery : Web.UI.ManagePage
    {
        private string action = DTEnums.ActionEnum.Add.ToString();//操作类型
        private int id = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            string _action = DTRequest.GetQueryString("action");

            if (!string.IsNullOrEmpty(_action) && _action == DTEnums.ActionEnum.Edit.ToString())
            {
                this.action = DTEnums.ActionEnum.Edit.ToString();//修改类型
                this.id = DTRequest.GetQueryInt("id");
                if (this.id == 0)
                {
                    JscriptMsg("传输参数不正确！", "back");
                    return;
                }
                if (!new BLL.draw_lotteryList().Exists(this.id))
                {
                    JscriptMsg("记录不存在或已被删除！", "back");
                    return;
                }
            }
            if (!Page.IsPostBack)
            {
                //ChkAdminLevel("sys_site_manage", DTEnums.ActionEnum.View.ToString()); //检查权限
                //ShowInfo(this.id); //绑定数据
                if (action == DTEnums.ActionEnum.Edit.ToString()) //修改
                {
                    ShowInfo(this.id);
                }
                else
                {
                    //txtBuildPath.Attributes.Add("ajaxurl", "../../tools/admin_ajax.ashx?action=site_path_validate");
                }
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            bool str = !(this.action == DTEnums.ActionEnum.Edit.ToString());
            if (str)
            {
                str = this.DoAdd();
                if (str)
                {
                    base.JscriptMsg("添加成功啦！", "index.aspx");
                }
                else
                {
                    base.JscriptMsg("保存过程中发生错误啦！", "", "Error");
                }
            }
            else
            {
                str = this.DoEdit(this.id);
                if (str)
                {
                    //base.JscriptMsg("修改成功啦！", "index.aspx", "Success");
                    JscriptMsg("修改成功啦！", "index.aspx");
                    return;
                }
                else
                {
                    base.JscriptMsg("保存过程中发生错误啦！", "", "Error");
                }
            }

        }
        private void ShowInfo(int _id)
        {
            DTcms.BLL.draw_lotteryList bll = new BLL.draw_lotteryList();
            Model.draw_lotteryList model = bll.GetModel(_id);
            this.txtTitle.Text = model.name;
            //this.rblType.SelectedValue = model.type.ToString();
            this.txtContent.Text = model.description;
            txtDefaultMsg.Text = model.default_msg;

            this.txtStartTime.Text = model.begin_date.ToString("yyyy-MM-dd HH:mm");
            this.txtEndTime.Text = model.end_date.ToString("yyyy-MM-dd HH:mm");

            this.rblis_status.SelectedValue = model.status.ToString();
        }
        private bool DoAdd()
        {
            //bool flag = true;
            Model.draw_lotteryList model = new Model.draw_lotteryList();
            BLL.draw_lotteryList bll = new BLL.draw_lotteryList();
            model.name = this.txtTitle.Text.Trim();
           
            model.begin_date = DateTime.Parse(this.txtStartTime.Text.Trim());
            model.end_date = DateTime.Parse(this.txtEndTime.Text.Trim());
            model.description = this.txtContent.Text.Trim();
            model.default_msg = txtDefaultMsg.Text.Trim();
            model.status = int.Parse(rblis_status.SelectedValue);
            model.createtime = System.DateTime.Now;

            bool flag1 = bll.Add(model) >= 1;
            
            return flag1;
        }

        private bool DoEdit(int _id)
        {
            bool flag = true;
            BLL.draw_lotteryList bll = new BLL.draw_lotteryList();
            Model.draw_lotteryList model = bll.GetModel(_id);
            model.name = this.txtTitle.Text.Trim();

            model.begin_date = DateTime.Parse(this.txtStartTime.Text.Trim());
            model.end_date = DateTime.Parse(this.txtEndTime.Text.Trim());
            model.description = this.txtContent.Text.Trim();
            model.default_msg = txtDefaultMsg.Text.Trim();
            model.status = int.Parse(rblis_status.SelectedValue);

            bool flag1 = bll.Update(model);
            if (!flag1)
            {
                flag = false;
            }
            bool flag2 = flag;
            return flag2;
        }
    }
}