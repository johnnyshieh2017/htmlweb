﻿using DTcms.Common;
using DTcms.Web.api.UtilsTool;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DTcms.Web.admin.vipserviceorder
{
    public partial class view : Web.UI.ManagePage
    {
        private string action = DTEnums.ActionEnum.Add.ToString(); //操作类型
        private int id = 0;
        BLL.vip_orders bll = new BLL.vip_orders();
        protected void Page_Load(object sender, EventArgs e)
        {
            string _action = DTRequest.GetQueryString("action");
            if (!string.IsNullOrEmpty(_action) && _action == DTEnums.ActionEnum.View.ToString())
            {
                this.action = DTEnums.ActionEnum.Edit.ToString();//修改类型
                if (!int.TryParse(Request.QueryString["id"] as string, out this.id))
                {
                    JscriptMsg("传输参数不正确！", "back");
                    return;
                }

                if (!bll.Exists(this.id))
                {
                    JscriptMsg("记录不存在或已被删除！", "back");
                    return;
                }
            }
            if (!IsPostBack)
            {
                InitData();

                if (action == DTEnums.ActionEnum.Edit.ToString()) //修改
                {
                    ShowInfo(this.id);
                }
            }
        }
        #region 初始化基础数据


        void InitData()
        {
            string source_file = HttpContext.Current.Server.MapPath("/xmlconfig/custom_source.json");
            System.IO.StreamReader file = System.IO.File.OpenText(source_file);
            string jsonWordTemplate = file.ReadToEnd();
            var list = JsonConvert.DeserializeObject<List<source>>(jsonWordTemplate);

            this.ddlsource.DataSource = list;
            this.ddlsource.DataTextField = "source_name";
            this.ddlsource.DataValueField = "id";
            this.ddlsource.DataBind();

            BLL.vip_users bll = new BLL.vip_users();
            var users = bll.GetModelList("user_type=2 and status=1");
            this.ddlserver.DataSource = users;
            ddlserver.DataTextField = "user_truename";
            ddlserver.DataValueField = "id";
            ddlserver.DataBind();

        }
        #endregion
        #region 赋值操作=================================
        private void ShowInfo(int _id)
        {
            var model = bll.GetModel(_id);

            rblservicetype.Text = model.service_typeid.ToString();
            ddlsource.Text = model.source;
            txt_custom_name.Text = model.customer;
            rblsex.Text = model.gender;
            txt_custom_telephone.Text = model.telephone;

            txt_contact.Text = model.contact_name;
            txt_contact_telephone.Text = model.contact_telephone;

            txt_custom_manager.Text = model.customer_manager;
            txt_custommanager_telephone.Text = model.customer_telephone;

            txt_remark.Value = model.remark;
            ddlserver.Text = model.servicer_id.ToString();
            txt_clinic_date.Text = model.clinic_date.ToString("yyyy-MM-dd HH:mm");
            txt_clinic_department.Text = model.clinic_department;
            txt_doctorname.Text = model.clinic_doctor;
            txt_revisit_date.Text = model.return_visit_date != null ? model.return_visit_date.ToString() : "";
            txtverifycode.Text = model.verifycode;

            if (!string.IsNullOrEmpty(model.clinic_confirm))
            {
                lbl_clinic_confirm.Text = "<a target='_blank' href='"+ model.clinic_confirm + "'>查看</a>";
            }
            lbl_service_begin.Text = model.service_begin.ToString();
            lbl_service_end.Text = model.service_end.ToString();

            BLL.vip_comment commentbll = new BLL.vip_comment();
            var comment = commentbll.GetModelList("order_id="+model.id).FirstOrDefault();
            if (comment != null)
            {
                lbl_comment_level.Text = comment.comment_level + "星";
                lbl_comment.Text = comment.comment;
            }
        }
        #endregion

        #region 增加操作=================================
        private bool DoAdd()
        {
            DTcms.Model.vip_orders model = new Model.vip_orders();

            model.source = ddlsource.Text;
            model.service_typeid = int.Parse(rblservicetype.Text);
            model.customer = txt_custom_name.Text.Trim();
            model.telephone = txt_custom_telephone.Text.Trim();
            model.gender = rblsex.SelectedItem.Text;

            model.contact_name = txt_contact.Text.Trim();
            model.contact_telephone = txt_contact_telephone.Text.Trim();

            model.customer_manager = txt_custom_manager.Text.Trim();
            model.customer_telephone = txt_custommanager_telephone.Text.Trim();
            model.remark = txt_remark.Value;

            model.servicer_id = int.Parse(ddlserver.Text);

            // model.book_date = ;
            model.clinic_department = txt_clinic_department.Text.Trim();
            model.clinic_doctor = txt_doctorname.Text.Trim();
            model.clinic_date = DateTime.Parse(txt_clinic_date.Text);


            //model.service_begin = service_begin;
            //model.service_end = service_end;

            model.status = 1;// 后台添加 status 由 1 开始
            model.pay_status = 0;

            model.add_time = System.DateTime.Now;
            //model.return_visit_date = return_visit_date;
            model.parent_id = 0;
            model.order_number = "vo_" + StringTool.GetUniqueStr();
            model.user_id = 0;
            model.verifycode = Utils.Number(4);

            if (bll.Add(model) > 0)
            {
                // 
                var server = new BLL.vip_users().GetModel(model.servicer_id);
                if (!string.IsNullOrEmpty(server.user_phone))
                {
                    DTcms.BLL.vip_sms smsbll = new BLL.vip_sms();
                    Model.vip_sms sms = new Model.vip_sms();
                    sms.msg_ext = model.order_number;
                    sms.msg_type = 1;
                    sms.msg_content = server.user_truename + "|" + model.customer + "|" + model.clinic_date.ToString("MM月dd日 HH:mm") + "|" + model.clinic_department;
                    sms.msg_mobile = server.user_phone;
                    sms.templateid = "";
                    sms.msg_status = 0;
                    sms.send_date = Convert.ToDateTime(model.clinic_date.AddDays(-1).ToString("yyyy-MM-dd")).AddHours(21);
                    sms.add_time = System.DateTime.Now;
                    if (sms.send_date > System.DateTime.Now)
                    {
                        smsbll.Add(sms);
                    }
                }

                if (!string.IsNullOrEmpty(server.user_email))
                {
                    DTcms.BLL.vip_email emailbll = new BLL.vip_email();
                    Model.vip_email mail = new Model.vip_email();
                    mail.mail_title = "[" + rblservicetype.SelectedItem.Text + "]派单提醒";
                    //mail.mail_content = server.user_truename+"你好!<br/>";
                    mail.mail_content += "您有新的服务订单需要确认，<br/>";
                    mail.mail_content += "客户姓名：" + model.customer + "<br/>";
                    mail.mail_content += "客户电话：" + model.customer_telephone + "<br/>";
                    mail.mail_content += "就诊日期：" + model.clinic_date.ToString("MM月dd日 HH:mm") + "<br/>";
                    mail.mail_content += "就诊科室：" + model.clinic_department + "<br/>";
                    mail.mail_content += "请及时处理，谢谢！";
                    mail.mail_to = server.user_email;
                    mail.mail_type = 1;
                    mail.send_date = System.DateTime.Now.AddMinutes(1);
                    mail.templateid = "";
                    mail.mail_status = 0;
                    mail.mail_ext = model.order_number;
                    mail.add_time = System.DateTime.Now;
                    emailbll.Add(mail);
                }
                //
                AddAdminLog(DTEnums.ActionEnum.Add.ToString(), "添加vip_service_订单:" + model.customer_telephone); //记录日志
                return true;
            }
            return false;
        }
        #endregion

        #region 修改操作=================================
        private bool DoEdit(int _id)
        {
            bool result = false;

            DTcms.Model.vip_orders model_edit = bll.GetModel(_id);

            model_edit.source = ddlsource.Text;
            model_edit.service_typeid = int.Parse(rblservicetype.Text);
            model_edit.customer = txt_custom_name.Text.Trim();
            model_edit.telephone = txt_custom_telephone.Text.Trim();
            model_edit.gender = rblsex.SelectedItem.Text;

            model_edit.contact_name = txt_contact.Text.Trim();
            model_edit.contact_telephone = txt_contact_telephone.Text.Trim();

            model_edit.customer_manager = txt_custom_manager.Text.Trim();
            model_edit.customer_telephone = txt_custommanager_telephone.Text.Trim();
            model_edit.remark = txt_remark.Value;

            model_edit.servicer_id = int.Parse(ddlserver.Text);

            // model.book_date = ;
            model_edit.clinic_department = txt_clinic_department.Text.Trim();
            model_edit.clinic_doctor = txt_doctorname.Text.Trim();
            model_edit.clinic_date = DateTime.Parse(txt_clinic_date.Text);

            if (bll.Update(model_edit))
            {

                var server = new BLL.vip_users().GetModel(model_edit.servicer_id);
                if (!string.IsNullOrEmpty(server.user_phone))
                {

                    DTcms.BLL.vip_sms smsbll = new BLL.vip_sms();
                    var sms = smsbll.GetModelList("msg_status=0 and msg_ext='" + model_edit.order_number + "'").FirstOrDefault();

                    if (sms != null)
                    {
                        sms.msg_ext = model_edit.order_number;
                        sms.msg_type = 1;
                        sms.msg_content = server.user_truename + "|" + model_edit.customer + "|" + model_edit.clinic_date.ToString("MM月dd日 HH:mm") + "|" + model_edit.clinic_department;
                        sms.msg_mobile = server.user_phone;
                        sms.templateid = "";
                        sms.msg_status = 0;
                        sms.send_date = Convert.ToDateTime(model_edit.clinic_date.AddDays(-1).ToString("yyyy-MM-dd")).AddHours(21);
                        sms.add_time = System.DateTime.Now;
                        if (sms.send_date > System.DateTime.Now)
                        {
                            smsbll.Update(sms);
                        }
                    }
                    else
                    {
                        sms = new Model.vip_sms();
                        sms.msg_ext = model_edit.order_number;
                        sms.msg_type = 1;
                        sms.msg_content = server.user_truename + "|" + model_edit.customer + "|" + model_edit.clinic_date.ToString("MM月dd日 HH:mm") + "|" + model_edit.clinic_department;
                        sms.msg_mobile = server.user_phone;
                        sms.templateid = "";
                        sms.msg_status = 0;
                        sms.send_date = Convert.ToDateTime(model_edit.clinic_date.AddDays(-1).ToString("yyyy-MM-dd")).AddHours(21);
                        sms.add_time = System.DateTime.Now;
                        if (sms.send_date > System.DateTime.Now)
                        {
                            smsbll.Add(sms);
                        }
                    }
                }

                if (!string.IsNullOrEmpty(server.user_email))
                {
                    DTcms.BLL.vip_email emailbll = new BLL.vip_email();

                    var mail = emailbll.GetModelList("mail_status=0 and mail_ext='" + model_edit.order_number + "'").FirstOrDefault();
                    if (mail != null)
                    {
                        mail.mail_title = "[" + rblservicetype.SelectedItem.Text + "]就诊服务派单提醒";
                        // mail.mail_content = server.user_truename + "你好!<br/>";
                        mail.mail_content += "您有新的服务订单需要确认，<br/>";
                        mail.mail_content += "客户姓名：" + model_edit.customer + "<br/>";
                        mail.mail_content += "客户电话：" + model_edit.customer_telephone + "<br/>";
                        mail.mail_content += "就诊日期：" + model_edit.clinic_date.ToString("MM月dd日 HH:mm") + "<br/>";
                        mail.mail_content += "就诊科室：" + model_edit.clinic_department + "<br/>";
                        mail.mail_content += "请及时处理，谢谢！";
                        mail.mail_to = server.user_email;
                        mail.mail_type = 1;
                        mail.send_date = System.DateTime.Now.AddMinutes(1);
                        mail.templateid = "";
                        mail.mail_status = 0;
                        mail.mail_ext = model_edit.order_number;
                        emailbll.Update(mail);
                    }
                    else
                    {
                        mail = new Model.vip_email();
                        mail.mail_title = "[" + rblservicetype.SelectedItem.Text + "]就诊服务派单提醒";
                        mail.mail_content = server.user_truename + "你好!<br/>";
                        mail.mail_content += "你有新的服务订单需要确认，<br/>";
                        mail.mail_content += "客户姓名：" + model_edit.customer + "<br/>";
                        mail.mail_content += "客户电话：" + model_edit.customer_telephone + "<br/>";
                        mail.mail_content += "就诊日期：" + model_edit.clinic_date.ToString("MM月dd日 HH:mm") + "<br/>";
                        mail.mail_content += "就诊科室：" + model_edit.clinic_department + "<br/>";
                        mail.mail_content += "请及时处理，谢谢！";
                        mail.mail_to = server.user_email;
                        mail.mail_type = 1;
                        mail.send_date = System.DateTime.Now.AddMinutes(1);
                        mail.templateid = "";
                        mail.mail_status = 0;
                        mail.mail_ext = model_edit.order_number;
                        mail.add_time = System.DateTime.Now;
                        emailbll.Add(mail);
                    }
                }

                AddAdminLog(DTEnums.ActionEnum.Add.ToString(), "修改vip_service_订单:" + model_edit.id); //记录日志
                result = true;
            }

            return result;
        }
        #endregion

        //保存
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (action == DTEnums.ActionEnum.Edit.ToString()) //修改
            {
                //ChkAdminLevel("doclist", DTEnums.ActionEnum.Edit.ToString()); //检查权限
                if (!DoEdit(this.id))
                {
                    JscriptMsg("保存过程中发生错误！", "");
                    return;
                }
                JscriptMsg("修改信息成功！", "list.aspx");
            }
            else //添加
            {
                // ChkAdminLevel("doclist", DTEnums.ActionEnum.Add.ToString()); //检查权限
                if (!DoAdd())
                {
                    JscriptMsg("保存过程中发生错误！", "");
                    return;
                }
                JscriptMsg("添加信息成功！", "list.aspx");
            }
        }
    }
}