﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="edit.aspx.cs" ValidateRequest="false"  Inherits="DTcms.Web.admin.vipserviceorder.edit" %>

<%@ Import Namespace="DTcms.Common" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>编辑</title>
    <meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,initial-scale=1.0,user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <link rel="stylesheet" type="text/css" href="../../scripts/artdialog/ui-dialog.css" />
    <link rel="stylesheet" type="text/css" href="../skin/icon/iconfont.css" />
    <link rel="stylesheet" type="text/css" href="../skin/default/style.css" />
    <script type="text/javascript" charset="utf-8" src="../../scripts/jquery/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../scripts/jquery/Validform_v5.3.2_min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../scripts/artdialog/dialog-plus-min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../scripts/webuploader/webuploader.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../scripts/datepicker/WdatePicker.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../editor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../editor/ueditor.all.min.js"> </script>
    <script type="text/javascript" charset="utf-8" src="../../editor/lang/zh-cn/zh-cn.js"></script>

    <script type="text/javascript" charset="utf-8" src="../js/uploader.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/common.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/laymain.js"></script>
    <script type="text/javascript">
        $(function () {
            //初始化表单验证
            $("#form1").initValidform();
            //初始化上传控件
            $(".upload-img").InitUploader({ sendurl: "../../tools/upload_ajax.ashx", swf: "../../scripts/webuploader/uploader.swf" });

            //初始化editor
            $(".editor-mini").each(function (i) {
                var objId = $(this).attr("id");
                if (objId != "undefined") {
                    var editorMini = UE.getEditor(objId, {
                        serverUrl: '../../../tools/upload_ajax.ashx',
                        initialFrameWidth: '100%',
                        initialFrameHeight: 220,
                        toolbars: [[
                            'fullscreen', 'source', '|', 'undo', 'redo', '|',
                            'bold', 'italic', 'underline', 'strikethrough', 'removeformat', 'pasteplain', '|', 'forecolor', 'insertorderedlist', 'insertunorderedlist', '|',
                         'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|',
                         'link', 'unlink', 'anchor', '|',
                         'simpleupload', 'insertimage', 'scrawl', 'insertvideo'
                        ]]
                    });
                }
            });

        });
    </script>
</head>

<body class="mainbody">
    <form id="form1" runat="server">
        <!--导航栏-->
        <div class="location">
            <a href="manager_list.aspx" class="back"><i class="iconfont icon-up"></i><span>返回列表页</span></a>
            <a href="../center.aspx"><i class="iconfont icon-home"></i><span>首页</span></a>
            <i class="arrow iconfont icon-arrow-right"></i>
            <a href="departmentmanage.aspx"><span>订单管理</span></a>
            <i class="arrow iconfont icon-arrow-right"></i>
            <span>编辑订单</span>
        </div>
        <div class="line10"></div>
        <!--/导航栏-->

        <!--内容-->
        <div id="floatHead" class="content-tab-wrap">
            <div class="content-tab">
                <div class="content-tab-ul-wrap">
                    <ul>
                        <li><a class="selected" href="javascript:;">订单信息</a></li>
                        <li><a href="javascript:;">陪诊信息</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="tab-content">

            <dl>
                <dt>服务类型</dt>
                <dd>
                    <div class="rule-multi-radio">
                        <asp:RadioButtonList ID="rblservicetype" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                            <asp:ListItem Text="贴心陪诊" Value="1" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="贵宾门诊" Value="2"></asp:ListItem>
                            <asp:ListItem Text="贵宾手术" Value="3"></asp:ListItem>
                            <asp:ListItem Text="贵宾住院" Value="4"></asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </dd>
            </dl>
            <dl>
                <dt>客户来源</dt>
                <dd>
                    <div class="rule-single-select">
                        <asp:DropDownList ID="ddlsource" runat="server" datatype="n" errormsg="客户来源" sucmsg=" ">
                           
                        </asp:DropDownList>
                    </div>
                </dd>
            </dl>
            <dl>
                <dt>就诊人</dt>
                <dd>
                    <asp:TextBox ID="txt_custom_name" runat="server" CssClass="input normal" datatype="*2-100"></asp:TextBox></dd>
            </dl>
             <dl>
                <dt>性 别</dt>
                <dd>
                    <div class="rule-multi-radio">
                        <asp:RadioButtonList ID="rblsex" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                            <asp:ListItem Text="男" Value="男" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="女" Value="女"></asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </dd>
            </dl>
            <dl>
                <dt>电话</dt>
                <dd>
                    <asp:TextBox ID="txt_custom_telephone" runat="server" CssClass="input normal" datatype="*2-100"></asp:TextBox></dd>
            </dl>
            <dl>
                <dt>联系人</dt>
                <dd>
                    <asp:TextBox ID="txt_contact" runat="server" CssClass="input normal" datatype="*2-100"></asp:TextBox></dd>
            </dl>
             <dl>
                <dt>联系人电话</dt>
                <dd>
                    <asp:TextBox ID="txt_contact_telephone" runat="server" CssClass="input normal" datatype="*2-100"></asp:TextBox></dd>
            </dl>
            <dl>
                <dt>客户经理</dt>
                <dd>
                    <asp:TextBox ID="txt_custom_manager" runat="server" CssClass="input normal" datatype="*0-100"></asp:TextBox></dd>
            </dl>

            <dl>
                <dt>客户经理电话</dt>
                <dd>
                    <asp:TextBox ID="txt_custommanager_telephone" runat="server" CssClass="input normal" datatype="*0-20"></asp:TextBox></dd>
            </dl>

           
            <dl>
                <dt>备注</dt>
                <dd>
                    <textarea id="txt_remark" class="input" rows="2" runat="server"></textarea></dd>
            </dl>

        </div>

        <div class="tab-content" style="display: none">
            <dl>
                <dt>陪诊人员</dt>
                <dd>
                    <div class="rule-single-select">
                        <asp:DropDownList ID="ddlserver" runat="server" datatype="*" sucmsg=" ">
                           
                        </asp:DropDownList>
                    </div>
                </dd>
            </dl>
             <dl>
                <dt>预约时间</dt>
                <dd>
                    <asp:TextBox ID="txt_clinic_date" runat="server" CssClass="input rule-date-input" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm'})" datatype="/^\d{4}\-\d{1,2}\-\d{1,2}\s{1}\d{1,2}:\d{1,2}$/" ></asp:TextBox></dd>
            </dl>

            <dl>
                <dt>就诊医院科室</dt>
                <dd>
                    <asp:TextBox ID="txt_clinic_department" runat="server" CssClass="input normal" datatype="*2-100"></asp:TextBox></dd>
            </dl>
            <dl>
                <dt>就诊医生</dt>
                <dd>
                    <asp:TextBox ID="txt_doctorname" runat="server" CssClass="input normal" datatype="*2-100"></asp:TextBox></dd>
            </dl>
            <dl>
                <dt>复诊日期</dt>
                <dd>
                    <asp:TextBox ID="txt_revisit_date" runat="server" CssClass="input rule-date-input" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'})" datatype="/^\s*$|^\d{4}\-\d{1,2}\-\d{1,2}$/" errormsg="请选择正确的日期" sucmsg=" " />
                    <span class="Validform_checktip">以“,”逗号区分开，255个字符以内</span>
                </dd>
            </dl>

        </div>
        <!--/内容-->

        <!--工具栏-->
        <div class="page-footer">
            <div class="btn-wrap">
                <asp:Button ID="btnSubmit" runat="server" Text="提交保存" CssClass="btn" OnClick="btnSubmit_Click" />
                <input name="btnReturn" type="button" value="返回上一页" class="btn yellow" onclick="javascript: history.back(-1);" />
            </div>
        </div>
        <!--/工具栏-->

    </form>
</body>
</html>
