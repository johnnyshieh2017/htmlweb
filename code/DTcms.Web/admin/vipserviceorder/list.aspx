﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="list.aspx.cs" Inherits="DTcms.Web.admin.vipserviceorder.list" %>

<%@ Import Namespace="DTcms.Common" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,initial-scale=1.0,user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <title>订单管理</title>
    <link rel="stylesheet" type="text/css" href="../../scripts/artdialog/ui-dialog.css" />
    <link rel="stylesheet" type="text/css" href="../../css/pagination.css" />
    <link rel="stylesheet" type="text/css" href="../skin/icon/iconfont.css" />
    <link rel="stylesheet" type="text/css" href="../skin/default/style.css" />
    <script type="text/javascript" src="../../scripts/jquery/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="../../scripts/artdialog/dialog-plus-min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/laymain.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/common.js"></script>

</head>

<body class="mainbody">
    <form id="form1" runat="server">
        <!--导航栏-->
        <div class="location">
            <a href="javascript:history.back(-1);" class="back"><i class="iconfont icon-up"></i><span>返回上一页</span></a>
            <a href="../center.aspx" class="home"><i class="iconfont icon-home"></i><span>首页</span></a>
            <i class="arrow iconfont icon-arrow-right"></i>
            <span>订单管理</span>
        </div>
        <!--/导航栏-->

        <!--工具栏-->
        <div id="floatHead" class="toolbar-wrap">
            <div class="toolbar">
                <div class="box-wrap">
                    <a class="menu-btn"><i class="iconfont icon-more"></i></a>

                    <div class="l-list">
                        <ul class="icon-list">
                            <li><a href="edit.aspx?action=<%=DTEnums.ActionEnum.Add %>"><i class="iconfont icon-close"></i><span>新增</span></a></li>
                            <li><a href="javascript:;" onclick="checkAll(this);"><i class="iconfont icon-check"></i><span>全选</span></a></li>

                        </ul>
                        <div class="menu-list">
                            <div class="rule-single-select">
                                <asp:DropDownList ID="ddlservicetype" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlservicetype_SelectedIndexChanged">
                                    <asp:ListItem Text="所有类型" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="贴心陪诊" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="贵宾门诊" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="贵宾手术" Value="3"></asp:ListItem>
                                    <asp:ListItem Text="贵宾住院" Value="4"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="menu-list">
                            <div class="rule-single-select">
                                <asp:DropDownList ID="ddlsource" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlsource_SelectedIndexChanged">
                                    <asp:ListItem Text="所有来源" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="menu-list">
                            <div class="rule-single-select">
                                <asp:DropDownList ID="ddlserver" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlserver_SelectedIndexChanged">
                                    <asp:ListItem Text="所有人员" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>

                    <div class="r-list">

                        <asp:TextBox ID="txtKeywords" runat="server" CssClass="keyword" />
                        <asp:LinkButton ID="lbtnSearch" runat="server" CssClass="btn-search" OnClick="btnSearch_Click"><i class="iconfont icon-search"></i></asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
        <!--/工具栏-->

        <!--列表-->
        <div class="table-container">
            <asp:Repeater ID="rptList" runat="server">
                <HeaderTemplate>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="ltable">
                        <tr>
                            <th width="5%">选择</th>
                            <th align="center" width="7%">客户姓名</th>
                            <th align="center" width="8%">电话</th>

                            <th align="center" width="7%">来源</th>
                             <th align="center" width="8%">类型</th>

                            <th align="center" width="12%">客户经理</th>
                            <th align="center" width="11%">预约信息</th>
                            <th align="center" width="11%">服务信息</th>
                            <th align="center" width="8%">状态</th>
                            <th align="center" width="8%">操作</th>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td align="center">
                            <asp:CheckBox ID="chkId" CssClass="checkall" runat="server" Style="vertical-align: middle;" />
                            <asp:HiddenField ID="hidId" Value='<%#Eval("id")%>' runat="server" />
                        </td>
                        <td align="center"><%# Eval("customer") %></td>
                        <td align="center"><%# Eval("telephone") %></td>
                        <td align="center"><%#GetSource(Eval("source").ToString())%></td>
                        <td align="center"><%#GetServicename(Eval("service_typeid").ToString())%></td>
                        <td align="center"><%# Eval("customer_manager") %>(<%# Eval("customer_telephone") %>)</td>
                        <td align="left">
                            <p>就诊科室：<%# Eval("clinic_department") %></p>
                            <p>就诊医师：<%# Eval("clinic_doctor") %></p>
                             <p>预约日期：<%# Eval("clinic_date","{0:yyyy-MM-dd}") %></p>
                        </td>
                        <td align="left">
                            <p>服务人员：<%# Eval("user_truename") %></p>
                            <p>开始时间：<%# Eval("service_begin") %></p>
                            <p>结束时间：<%# Eval("service_end") %></p>

                        </td>
                        <td align="center"><%# GetStatus(Eval("status").ToString()) %></td>
                        <td align="center"><a href="edit.aspx?action=<%#DTEnums.ActionEnum.Edit %>&id=<%#Eval("id")%>">修改</a>
                            <a href="view.aspx?action=<%#DTEnums.ActionEnum.View %>&id=<%#Eval("id")%>" target="_blank">查看</a>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    <%#rptList.Items.Count == 0 ? "<tr><td align=\"center\" colspan=\"8\">暂无记录</td></tr>" : ""%>
  </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
        <!--/列表-->

        <!--内容底部-->
        <div class="line20"></div>
        <div class="pagelist">
            <div class="l-btns">
                <span>显示</span><asp:TextBox ID="txtPageNum" runat="server" CssClass="pagenum" onkeydown="return checkNumber(event);"
                    OnTextChanged="txtPageNum_TextChanged" AutoPostBack="True"></asp:TextBox><span>条/页</span>
            </div>
            <div id="PageContent" runat="server" class="default"></div>
        </div>
        <!--/内容底部-->
    </form>
</body>
</html>
