﻿using DTcms.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DTcms.Web.admin.vipserviceorder
{
    public partial class list : Web.UI.ManagePage
    {
        protected int totalCount;
        protected int page;
        protected int pageSize;
        BLL.vip_orders bll = new BLL.vip_orders();
        protected string keywords = string.Empty;
        protected int servicetype ;
        protected int source ;
        protected int serverid;
        protected void Page_Load(object sender, EventArgs e)
        {
            this.keywords = DTRequest.GetQueryString("keywords");
            this.servicetype = DTRequest.GetQueryInt("servicetype");
            this.serverid = DTRequest.GetQueryInt("serverid");
            this.source = DTRequest.GetQueryInt("source");

            this.pageSize = GetPageSize(10); //每页数量
            if (!Page.IsPostBack)
            {
                ChkAdminLevel("vipserviceorderlist", DTEnums.ActionEnum.View.ToString()); //检查权限
                Model.manager model = GetAdminInfo(); //取得当前管理员信息

                InitData();
                //RptBind("role_type>=" + model.role_type + CombSqlTxt(keywords), "add_time asc,id desc");
                RptBind("1=1 " + CombSqlTxt(keywords), "id desc");
            }
        }

        #region 初始化基础数据


        void InitData()
        {
            string source_file = HttpContext.Current.Server.MapPath("/xmlconfig/custom_source.json");
            System.IO.StreamReader file = System.IO.File.OpenText(source_file);
            string jsonWordTemplate = file.ReadToEnd();
            var list = JsonConvert.DeserializeObject<List<source>>(jsonWordTemplate);
            foreach (var item in list)
            {
                ddlsource.Items.Add(new ListItem(item.source_name,item.id.ToString()));
            }
            //this.ddlsource.DataSource = list;
            //this.ddlsource.DataTextField = "source_name";
            //this.ddlsource.DataValueField = "id";
            //this.ddlsource.DataBind();

            BLL.vip_users bll = new BLL.vip_users();
            var users = bll.GetModelList("user_type=2 and status=1");
            foreach (var item in users)
            {
                ddlserver.Items.Add(new ListItem(item.user_truename, item.id.ToString()));
            }
            //this.ddlserver.DataSource = users;
            //ddlserver.DataTextField = "user_truename";
            //ddlserver.DataValueField = "id";
            //ddlserver.DataBind();

        }
        #endregion

        #region 数据绑定=================================
        private void RptBind(string _strWhere, string _orderby)
        {
            this.page = DTRequest.GetQueryInt("page", 1);
            txtKeywords.Text = this.keywords;
            ddlserver.Text = this.serverid.ToString();
            ddlservicetype.Text = this.servicetype.ToString();
            ddlsource.Text = this.source.ToString();
           
            this.rptList.DataSource = bll.GetList(this.pageSize, this.page, _strWhere, _orderby, out this.totalCount);
            this.rptList.DataBind();

            //绑定页码
            txtPageNum.Text = this.pageSize.ToString();
            string pageUrl = Utils.CombUrlTxt("list.aspx", "keywords={0}&servicetype={1}&serverid={2}&source={3}&page={4}", this.keywords,this.servicetype.ToString(),this.serverid.ToString(),this.source.ToString(), "__id__");
            PageContent.InnerHtml = Utils.OutPageList(this.pageSize, this.page, this.totalCount, pageUrl, 8);
        }
        #endregion

        #region 组合SQL查询语句==========================
        protected string CombSqlTxt(string _keywords)
        {
            StringBuilder strTemp = new StringBuilder();
            _keywords = _keywords.Replace("'", "");

            if (this.source != 0)
            {
                strTemp.Append(" and source='"+this.source+"'");
            }
            if (this.serverid != 0)
            {
                strTemp.Append(" and servicer_id=" + this.serverid + "");
            }
            if (this.servicetype != 0)
            {
                strTemp.Append(" and service_typeid=" + this.servicetype + "");
            }
            if (!string.IsNullOrEmpty(_keywords))
            {
                strTemp.Append(" and (customer like  '%" + _keywords + "%' or telephone like '%"+_keywords+"%')");
            }
            
            return strTemp.ToString();
        }
        #endregion

        #region 返回每页数量=============================
        private int GetPageSize(int _default_size)
        {
            int _pagesize;
            if (int.TryParse(Utils.GetCookie("vipserviceorderlist_page_size", "DTcmsPage"), out _pagesize))
            {
                if (_pagesize > 0)
                {
                    return _pagesize;
                }
            }
            return _default_size;
        }
        #endregion

        #region 转换来源、类型、状态
        protected string GetSource(string id)
        {
            string source_file = HttpContext.Current.Server.MapPath("/xmlconfig/custom_source.json");
            System.IO.StreamReader file = System.IO.File.OpenText(source_file);
            string jsonWordTemplate = file.ReadToEnd();
            var list = JsonConvert.DeserializeObject<List<source>>(jsonWordTemplate);
            var source = list.Find(p => p.id == int.Parse(id));
            if (source != null)
            {
                return source.source_name;
            }
            return "";
        }

        protected string GetServicename(string id)
        {
            string result = "";
            switch (id)
            {
                case "1":
                    result= "贴心陪诊";
                    break;
                case "2":
                    result = "贵宾门诊";
                    break;
                case "3":
                    result = "贵宾手术";
                    break;
                case "4":
                    result = "贵宾住院";
                    break;
                default:
                    break;
            }
            return result;
        }

        protected string GetStatus(string id)
        {
            string result = "";
            switch (id)
            {
                case "0":
                    result = "未确认";
                    break;
                case "1":
                    result = "已派单";
                    break;
                case "2":
                    result = "进行中";
                    break;
                case "10":
                    result = "进行中";
                    break;
                case "15":
                    result = "已完成";
                    break;
                default:
                    break;
            }
            return result;
        }
        #endregion
        //关健字查询
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect(Utils.CombUrlTxt("list.aspx", "keywords={0}&servicetype={1}&serverid={2}&source={3}", txtKeywords.Text,this.servicetype.ToString(),this.serverid.ToString(),this.source.ToString()));
        }

        //设置分页数量
        protected void txtPageNum_TextChanged(object sender, EventArgs e)
        {
            int _pagesize;
            if (int.TryParse(txtPageNum.Text.Trim(), out _pagesize))
            {
                if (_pagesize > 0)
                {
                    Utils.WriteCookie("vipserviceorderlist_page_size", "DTcmsPage", _pagesize.ToString(), 14400);
                }
            }
            Response.Redirect(Utils.CombUrlTxt("list.aspx", "keywords={0}", this.keywords));
        }

        protected void ddlservicetype_SelectedIndexChanged(object sender, EventArgs e)
        {
            Response.Redirect(Utils.CombUrlTxt("list.aspx", "keywords={0}&servicetype={1}&serverid={2}&source={3}", txtKeywords.Text, this.ddlservicetype.Text, this.serverid.ToString(), this.source.ToString()));
        }

        protected void ddlsource_SelectedIndexChanged(object sender, EventArgs e)
        {
            Response.Redirect(Utils.CombUrlTxt("list.aspx", "keywords={0}&servicetype={1}&serverid={2}&source={3}", txtKeywords.Text, this.servicetype.ToString(), this.serverid.ToString(),ddlsource.Text));
        }

        protected void ddlserver_SelectedIndexChanged(object sender, EventArgs e)
        {
            Response.Redirect(Utils.CombUrlTxt("list.aspx", "keywords={0}&servicetype={1}&serverid={2}&source={3}", txtKeywords.Text, this.servicetype.ToString(), this.ddlserver.Text, this.source.ToString()));
        }
    }
}