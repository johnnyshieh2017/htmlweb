﻿using DTcms.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DTcms.Web.admin.businessmanage
{
    public partial class businessedit : Web.UI.ManagePage
    {
        private string action = DTEnums.ActionEnum.Add.ToString(); //操作类型
        private int id = 0;
        BLL.yjr_zx_business bll = new BLL.yjr_zx_business();
        protected void Page_Load(object sender, EventArgs e)
        {
            string _action = DTRequest.GetQueryString("action");
            if (!string.IsNullOrEmpty(_action) && _action == DTEnums.ActionEnum.Edit.ToString())
            {
                this.action = DTEnums.ActionEnum.Edit.ToString();//修改类型
                if (!int.TryParse(Request.QueryString["id"] as string, out this.id))
                {
                    JscriptMsg("传输参数不正确！", "back");
                    return;
                }
                if (!bll.Exists(this.id))
                {
                    JscriptMsg("记录不存在或已被删除！", "back");
                    return;
                }
            }

            if (!IsPostBack)
            {
                ChkAdminLevel("businessedit", DTEnums.ActionEnum.View.ToString()); //检查权限
                Model.manager model = GetAdminInfo(); //取得管理员信息
                //InitData();
                if (action == DTEnums.ActionEnum.Edit.ToString()) //修改
                {
                    ShowInfo(this.id);
                }
            }
        }

        #region 赋值操作=================================
        private void ShowInfo(int _id)
        {
            var model = bll.GetModel(_id);
            txtRealName.Text    = model.business_name;
            txtintro.Value      = model.business_intro;
            txtSort.Text        = model.sort.ToString();

        }
        #endregion

        #region 增加操作=================================
        private bool DoAdd()
        {

            DTcms.Model.yjr_zx_business model = new DTcms.Model.yjr_zx_business();
            model.business_name = txtRealName.Text;
            model.business_intro = txtintro.Value;
            model.sort = int.Parse(txtSort.Text.Trim());

            if (bll.Add(model) > 0)
           {
                //AddAdminLog(DTEnums.ActionEnum.Add.ToString(), "添加医院:" + model.hospitalname); //记录日志
                return true;
            }
            return false;
        }
        #endregion

        #region 修改操作=================================
        private bool DoEdit(int _id)
        {
            bool result = false;

            DTcms.Model.yjr_zx_business model = bll.GetModel(_id);
            model.business_name = txtRealName.Text;
            model.business_intro = txtintro.Value;
            model.sort = int.Parse(txtSort.Text.Trim());
            if (bll.Update(model))
            {
                //AddAdminLog(DTEnums.ActionEnum.Add.ToString(), "修改doctor:" + model.hospitalname); //记录日志
                result = true;
            }

            return result;
        }
        #endregion

        //保存
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (action == DTEnums.ActionEnum.Edit.ToString()) //修改
            {
                ChkAdminLevel("hospitaladd", DTEnums.ActionEnum.Edit.ToString()); //检查权限
                if (!DoEdit(this.id))
                {
                    JscriptMsg("保存过程中发生错误！", "");
                    return;
                }
                JscriptMsg("修改信息成功！", "businessmanage.aspx");
            }
            else //添加
            {
                ChkAdminLevel("hospitaladd", DTEnums.ActionEnum.Add.ToString()); //检查权限
                if (!DoAdd())
                {
                    JscriptMsg("保存过程中发生错误！", "");
                    return;
                }
                JscriptMsg("添加信息成功！", "businessmanage.aspx");
            }
        }
    }
}