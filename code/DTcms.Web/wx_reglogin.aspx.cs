﻿using DTcms.API.OAuth;
using DTcms.Common;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DTcms.Web
{
    public partial class wx_reglogin : System.Web.UI.Page
    {
        Model.userconfig userConfig = new BLL.userconfig().loadConfig();
        Model.sysconfig sysConfig = new BLL.sysconfig().loadConfig();
        private static Logger logger = LogManager.GetCurrentClassLogger();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (DTcms.Common.DTRequest.GetQueryString("code") == "")
            {
                string url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=" + System.Configuration.ConfigurationManager.AppSettings["wxmp_appid"];//"wx657c74ba8a62696f";//
                url += "&redirect_uri=" + hdhost.Value + "/wx_reglogin.aspx";
                url += "&response_type=code";
                url += "&scope=snsapi_base";
                url += "&state=1&connect_redirect=1#wechat_redirect";
                this.Response.Redirect(url);
            }
            else
            {
                string appid = System.Configuration.ConfigurationManager.AppSettings["wxmp_appid"];// 测试公众号 "wx657c74ba8a62696f";
                string appkey = System.Configuration.ConfigurationManager.AppSettings["wxmp_appsecret"]; // 测试key "8837a57e029cdec23363eb20285e1b79";
                string url = "";

                string access_token = string.Empty;
                string expires_in = string.Empty;
                string openid = string.Empty;
                //取得返回参数
                string state = DTRequest.GetQueryString("state");
                string code = DTRequest.GetQueryString("code");
                //int userid = DTRequest.GetQueryInt("userid");
                //第一步：获取Access Token
                Dictionary<string, object> dic = weixin_helper.get_access_token(appid, appkey, url, code);
                if (dic == null || !dic.ContainsKey("access_token"))
                {
                    Response.Write("出错了，无法获取Access Token，请检查App Key是否正确！");
                    return;
                }


                access_token = dic["access_token"].ToString();

                expires_in = dic["expires_in"].ToString();

                openid = dic["openid"].ToString();

                Common.Utils.WriteCookie("user_wx_openid", openid);
                if (openid != "")
                {
                    Dictionary<string, object> dic_info = weixin_helper.get_info(access_token, openid);
                    if (dic_info == null)
                    {
                        Response.Write("出错了，无法获取user_info，请检查App Key是否正确！");
                        return;
                    }
                    else
                    {
                        //Common.Utils.WriteCookie("nickname", dic_info["nickname"].ToString());
                        //Common.Utils.WriteCookie("headimgurl", dic_info["headimgurl"].ToString());

                        BLL.yjr_user_uservsopenid yjr_uservsopenid = new BLL.yjr_user_uservsopenid();
                        var uservsopenid = yjr_uservsopenid.GetModelList("openid='" + openid + "'").FirstOrDefault();
                        if (uservsopenid != null)
                        {
                            // 登录
                            BLL.users bll = new BLL.users();
                            var user = bll.GetModel(uservsopenid.userid);

                            Session[DTKeys.SESSION_USER_INFO] = user;
                            Session.Timeout = 45;

                            Response.Redirect("/");
                        }
                        else
                        {
                            string error_msg="";
                            reguser(openid, "1", dic_info["nickname"].ToString(), dic_info["headimgurl"].ToString(), out error_msg);

                            Response.Redirect("/");
                        }
                    }
                }
                //
                // 查找openid  注册 用户基本信息 产生userid
                // 
                //BLL.vip_users bll = new BLL.vip_users();
                //var user = bll.GetModelList("user_wx_openid='" + openid + "' and user_type=1").FirstOrDefault();
                //if (user != null)
                //{
                //    Common.Utils.WriteCookie("user_wx_openid", openid);
                //    Common.Utils.WriteCookie("user_type", user.user_type.ToString());
                //    Common.Utils.WriteCookie("user_status", user.status.ToString());
                //    Common.Utils.WriteCookie("user_id", user.id.ToString());
                //    this.Response.Redirect(hdredirect.Value);
                //}
                //else
                //{
                //    Model.vip_users model = new Model.vip_users();

                //    model.user_wx_openid = openid;
                //    model.status = 0;
                //    model.user_type = 1;
                //    model.add_time = System.DateTime.Now;
                //    var id = bll.Add(model);

                //    Common.Utils.WriteCookie("user_wx_openid", openid);
                //    Common.Utils.WriteCookie("user_type", model.user_type.ToString());
                //    Common.Utils.WriteCookie("user_status", model.status.ToString());
                //    Common.Utils.WriteCookie("user_id", id.ToString());
                //    this.Response.Redirect(hdredirect_userinfo.Value);
                //}
            }
        }

        int reguser(string openid, string usertype,string nick_name,string avatar,out string error_msg)
        {
            error_msg = "";
            Model.user_groups modelGroup = new BLL.user_groups().GetDefault();
            if (modelGroup == null)
            {
                //context.Response.Write("{\"status\":0, \"msg\":\"用户尚未分组，请联系网站管理员！\"}");
                //return;
                error_msg = "用户尚未分组，请联系网站管理员!";

                return -1;
            }

            #region 保存用户注册信息
            BLL.users bll = new BLL.users();
            string mobile = "";
            if (!bll.ExistsMobile("99900000001"))
            {
                mobile = "99900000001";
            }
            else
            {
                var topmobile = bll.GetList(1, "", "mobile desc").Tables[0].Rows[0]["mobile"].ToString();
                mobile = Convert.ToInt64(topmobile) + 1.ToString();
            }

            Model.users model = new Model.users();
            model.site_id = 1;// site_id;
            model.group_id = modelGroup.id;
            model.user_name = mobile;
            model.salt = Utils.GetCheckCode(6); //获得6位的salt加密字符串
            model.password = DESEncrypt.Encrypt(Utils.Number(6), model.salt);
            model.email = "";// email;
            model.mobile = mobile;// mobile;
            model.reg_ip = "";// userip;
            model.reg_time = DateTime.Now;
            model.hospital = usertype;//区分用户类型
            model.department = "";// department;
            model.truename = "";// truename;
            model.nick_name = nick_name;// "会员1";
            model.avatar = avatar;// "/upload/default-head.png";//默认头像地址
            //设置用户状态
            if (userConfig.regstatus == 3)
            {
                model.status = 1; //待验证
            }
            else if (userConfig.regverify == 1)
            {
                model.status = 2; //待审核
            }
            else
            {
                model.status = 0; //正常
            }
            #endregion

            var userid =  bll.Add(model);

            if (userid > 0)
            {
                BLL.yjr_user_uservsopenid yjr_uservsopenid = new BLL.yjr_user_uservsopenid();
                if (!yjr_uservsopenid.Exists(userid))
                {
                    Model.yjr_user_uservsopenid uservsopenid = new Model.yjr_user_uservsopenid();
                    uservsopenid.userid = userid;
                    uservsopenid.openid = openid;
                    uservsopenid.add_time = System.DateTime.Now;
                    yjr_uservsopenid.Add(uservsopenid);
                }
            }

            return userid;
        }
    }
}