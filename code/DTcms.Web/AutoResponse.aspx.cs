﻿using DTcms.API.OAuth;
using DTcms.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DTcms.Web
{
    public partial class AutoResponse : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           

            string appid = "wx657c74ba8a62696f";
            string appkey = "8837a57e029cdec23363eb20285e1b79";
            string url = "";

            string access_token = string.Empty;
            string expires_in = string.Empty;
            string openid = string.Empty;
            //取得返回参数
            string state = DTRequest.GetQueryString("state");
            string code = DTRequest.GetQueryString("code");

            //第一步：获取Access Token
            Dictionary<string, object> dic = weixin_helper.get_access_token(appid, appkey, url, code);
            if (dic == null || !dic.ContainsKey("access_token"))
            {
                Response.Write("出错了，无法获取Access Token，请检查App Key是否正确！");
                return;
            }

            access_token = dic["access_token"].ToString();
            expires_in = dic["expires_in"].ToString();
            openid = dic["openid"].ToString();
            //储存获取数据用到的信息
            //Session["site_oauth_id"] = config.oauth_id;
            //Session["oauth_name"] = config.oauth_name;
            Session["oauth_access_token"] = access_token;
            Session["oauth_openid"] = openid;

            //第二步：跳转到指定页面


            Response.Redirect("form1.aspx");//

        }
    }
}