﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DTcms.Web
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            
            if (!IsPostBack)
            {

                string a = HttpContext.Current.Server.MapPath("/custom_source.txt");
                var list = new List<source>();
                using (System.IO.StreamReader file = System.IO.File.OpenText(a))
                {
                    string jsonWordTemplate = file.ReadToEnd();
                     list = JsonConvert.DeserializeObject<List<source>>(jsonWordTemplate);  
                }
                
                int c = list.Count;
                string n = list.FirstOrDefault().source_name;
                string n2 = list.FirstOrDefault().id.ToString();
                //Response.Write(jsonWordTemplate);
                //List<dpinfo> list = new List<dpinfo>();
                //dpinfo dp = new dpinfo();
                //dp.dpid = 1;
                //dp.dpname = "免疫科";
                //dp.dpicon = "";
                //list.Add(dp);

                //dp = new dpinfo();
                //dp.dpid = 2;
                //dp.dpname = "儿科";
                //dp.dpicon = "";
                //list.Add(dp);

                //dp = new dpinfo();
                //dp.dpid = 3;
                //dp.dpname = "外科";
                //dp.dpicon = "";
                //list.Add(dp);

                //dp = new dpinfo();
                //dp.dpid = 4;
                //dp.dpname = "消化科";
                //dp.dpicon = "";
                //list.Add(dp);

                //dp = new dpinfo();
                //dp.dpid = 5;
                //dp.dpname = "妇产科";
                //dp.dpicon = "";
                //list.Add(dp);

                //dp = new dpinfo();
                //dp.dpid = 6;
                //dp.dpname = "肾内科";
                //dp.dpicon = "";
                //list.Add(dp);

                //dp = new dpinfo();
                //dp.dpid = 7;
                //dp.dpname = "全科医学";
                //dp.dpicon = "";
                //list.Add(dp);

                //dp = new dpinfo();
                //dp.dpid = 1;
                //dp.dpname = "免疫科";
                //dp.dpicon = "";
                //list.Add(dp);

                //dp = new dpinfo();
                //dp.dpid = 8;
                //dp.dpname = "口腔科";
                //dp.dpicon = "";
                //list.Add(dp);

                //dp = new dpinfo();
                //dp.dpid = 9;
                //dp.dpname = "骨科";
                //dp.dpicon = "";
                //list.Add(dp);


                //dp = new dpinfo();
                //dp.dpid = 10;
                //dp.dpname = "肿瘤科";
                //dp.dpicon = "";
                //list.Add(dp);

                //List<source> list = new List<source>();
                //source s = new source();
                //s.id = 10;
                //s.source_name = "建行";
                //list.Add(s);

                //s = new source();
                //s.id = 11;
                //s.source_name = "万科";
                //list.Add(s);


                //s = new source();
                //s.id = 12;
                //s.source_name = "其他";
                //list.Add(s);


                //Response.Write(JsonConvert.SerializeObject(list));
            }
        }

        void BindData()
        {
            
            //string wordTemplateName = this.Page.Server.MapPath("~/xmlconfig/");
            //StreamReader sr = File.OpenText(wordTemplateName+"custom_source.txt");
            //string jsonWordTemplate = sr.ReadToEnd();
            //var list = JsonConvert.DeserializeObject<List<source>>(jsonWordTemplate);
               
        }


        /// <summary>
        /// 解压RAR和ZIP文件(需存在Winrar.exe(只要自己电脑上可以解压或压缩文件就存在Winrar.exe))
        /// </summary>
        /// <param name="UnPath">解压后文件保存目录</param>
        /// <param name="rarPathName">待解压文件存放绝对路径（包括文件名称）</param>
        /// <param name="IsCover">所解压的文件是否会覆盖已存在的文件(如果不覆盖,所解压出的文件和已存在的相同名称文件不会共同存在,只保留原已存在文件)</param>
        /// <param name="PassWord">解压密码(如果不需要密码则为空)</param>
        /// <returns>true(解压成功);false(解压失败)</returns>
        public static bool UnRarOrZip(string UnPath, string rarPathName, bool IsCover, string PassWord)
        {
            if (!Directory.Exists(UnPath))
                Directory.CreateDirectory(UnPath);
            Process Process1 = new Process();
            Process1.StartInfo.FileName = "Winrar.exe";
            Process1.StartInfo.CreateNoWindow = true;
            string cmd = "";
            if (!string.IsNullOrEmpty(PassWord) && IsCover)
                //解压加密文件且覆盖已存在文件( -p密码 )
                cmd = string.Format(" x -p{0} -o+ {1} {2} -y", PassWord, rarPathName, UnPath);
            else if (!string.IsNullOrEmpty(PassWord) && !IsCover)
                //解压加密文件且不覆盖已存在文件( -p密码 )
                cmd = string.Format(" x -p{0} -o- {1} {2} -y", PassWord, rarPathName, UnPath);
            else if (IsCover)
                //覆盖命令( x -o+ 代表覆盖已存在的文件)
                cmd = string.Format(" x -o+ {0} {1} -y", rarPathName, UnPath);
            else
                //不覆盖命令( x -o- 代表不覆盖已存在的文件)
                cmd = string.Format(" x -o- {0} {1} -y", rarPathName, UnPath);
            //命令
            Process1.StartInfo.Arguments = cmd;
            Process1.Start();
            Process1.WaitForExit();//无限期等待进程 winrar.exe 退出
                                   //Process1.ExitCode==0指正常执行，Process1.ExitCode==1则指不正常执行
            if (Process1.ExitCode == 0)
            {
                Process1.Close();
                return true;
            }
            else
            {
                Process1.Close();
                return false;
            }

        }

    }

    public class source
    {
        public int id { get; set; }
        public string source_name { get; set; }

    }

    public class dpinfo
    {
        public int dpid { get; set; }
        public string dpname { get; set; }
        public string dpicon { get; set; }
    }
}