﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DTcms.Web
{
    public partial class mudu_oauth : System.Web.UI.Page
    {
        protected string viewurl = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var visitorId = DTcms.Common.DTRequest.GetQueryString("visitorId");
                var notify_url = DTcms.Common.DTRequest.GetQueryString("notify_url");
                var actId = DTcms.Common.DTRequest.GetQueryString("actId");

                hd_visitorid.Text = visitorId;
                hd_notify_url.Text = notify_url;
                hd_actId.Text = actId;


                string url = "http://mudu.tv/activity.php?a=userAssign";

                url += "&id=" + hd_actId.Text;
                url += "&userid=889900";
                url += "&name=johnny";
                url += "&key=" + DTcms.Common.DM5.md5("889900" + txtkey.Text.Trim(), 32);

                viewurl = url;
            }
        }

        protected void btnlogin_Click(object sender, EventArgs e)
        {
            string url = "http://mudu.tv/activity.php?a=userAssign";

            url += "&id="+hd_actId.Text;
            url += "&userid="+txtuname.Text;
            url += "&name="+txtuname.Text;
            url += "&key="+DTcms.Common.DM5.md5(txtuname.Text + txtkey.Text.Trim(),32);

            if (txtpwd.Text == "123456")
            {
                Response.Redirect(url);
            }
            else
            {
                this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", "<script>alert('密码错误')</script>");
            }
        }
    }
}