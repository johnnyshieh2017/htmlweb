﻿using DTcms.API.OAuth;
using DTcms.Common;
using System;
using System.Collections.Generic;

namespace DTcms.Web
{
    public partial class getcode : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (DTcms.Common.DTRequest.GetQueryString("code") == "")
            {
                int userid = DTRequest.GetQueryInt("userid");
                if (userid != 0)
                {
                    //string url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx657c74ba8a62696f&redirect_uri=http://johnnyshieh.51vip.biz/AutoResponse.aspx&response_type=code&scope=snsapi_userinfo&state=1&connect_redirect=1#wechat_redirect";
                    //snsapi_base
                    string url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid="+ System.Configuration.ConfigurationManager.AppSettings["wxmp_appid"];
                    url += "&redirect_uri="+ hdhost.Value + "/getcode.aspx?userid="+ userid;
                    url += "&response_type=code";
                    url += "&scope=snsapi_base";
                    url += "&state=1&connect_redirect=1#wechat_redirect";
                    this.Response.Redirect(url);
                }
            }
            else
            {
                string appid = System.Configuration.ConfigurationManager.AppSettings["wxmp_appid"];// 测试公众号 "wx657c74ba8a62696f";
                string appkey = System.Configuration.ConfigurationManager.AppSettings["wxmp_appsecret"]; // 测试key "8837a57e029cdec23363eb20285e1b79";
                string url = "";

                string access_token = string.Empty;
                string expires_in = string.Empty;
                string openid = string.Empty;
                //取得返回参数
                string state = DTRequest.GetQueryString("state");
                string code = DTRequest.GetQueryString("code");
                int userid = DTRequest.GetQueryInt("userid");
                //第一步：获取Access Token
                Dictionary<string, object> dic = weixin_helper.get_access_token(appid, appkey, url, code);
                if (dic == null || !dic.ContainsKey("access_token"))
                {
                    Response.Write("出错了，无法获取Access Token，请检查App Key是否正确！");
                    return;
                }

                
                BLL.yjr_user_uservsopenid bll = new BLL.yjr_user_uservsopenid();

                if (!bll.Exists(userid))
                {
                    Model.yjr_user_uservsopenid model = new Model.yjr_user_uservsopenid();
                    model.openid = dic["openid"].ToString();
                    model.userid = userid;
                    model.add_time = System.DateTime.Now;
                    bll.Add(model);
                }
                else
                {
                    var model = bll.GetModel(userid);
                    model.openid = dic["openid"].ToString();
                    model.add_time = System.DateTime.Now;
                    bll.Update(model);
                }
                this.Response.Redirect(hdredirect.Value);
            }
        }
    }
}