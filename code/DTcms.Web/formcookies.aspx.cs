﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DTcms.Web
{
    public partial class formcookies : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CheckCookiesID();
            }
        }

        void CheckCookiesID()
        {
            //检测当前浏览cookiesid读取数据 
            var cookie_id = DTcms.Common.Utils.GetCookie("meetsign","id");
            if (!string.IsNullOrEmpty(cookie_id))
            {
                txtid.Value = cookie_id;
                txtname.Value = DTcms.Common.Utils.GetCookie("meetsign", "name");
            }
        }

        protected void btnsave_ServerClick(object sender, EventArgs e)
        {
            var cookie_id = DTcms.Common.Utils.GetCookie("meetsign", "id");
            if (string.IsNullOrEmpty(cookie_id))
            {
                string useridkey = Guid.NewGuid().ToString("N") + System.DateTime.Now.ToString("yyyyMMddHHmmss");
                DTcms.Common.Utils.WriteCookie("meetsign", "id", useridkey, 43200);
                DTcms.Common.Utils.WriteCookie("meetsign", "name", txtname.Value, 43200);
            }
            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "alertinfo", "<script>alert('保存成功');</script>");
        }
    }
}