﻿using DTcms.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DTcms.Web.live
{
    /// <summary>
    /// getlive 的摘要说明
    /// </summary>
    public class getlive : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            JsonResult<channel_article_live> res = new JsonResult<channel_article_live>();
            try
            {
               

                int channel_id  = DTRequest.GetQueryInt("channel_id"); 
                int category_id = DTRequest.GetQueryInt("category_id");
                int pagesize = DTRequest.GetQueryInt("pagesize");
                int page = DTRequest.GetQueryInt("page");
                int ordertype=DTRequest.GetQueryInt("ordertype");
                int record_count = 0;

                string keywords = DTRequest.GetQueryString("keywords");
                int flag = DTRequest.GetQueryInt("flag");

                BLL.article articlebll = new BLL.article();
                BLL.article_category BLL_category = new BLL.article_category();
                string strorder = " add_time desc ";
                if (ordertype == 1)
                {
                    strorder = " sort_id desc ";
                }
                else if(ordertype==2)
                {
                    strorder = " sort_id asc ";
                }
                string strwhere = " status!=2 ";
                if(keywords.Trim()!=""&& keywords .Trim().Length<10)
                {
                    strwhere += " and title like '%"+keywords.Trim()+"%'";
                }
                if (flag == 0)
                { }
                else if (flag == 1)
                {
                    strwhere += " and is_top=1 ";
                }
                else if (flag == 2)
                {
                    strwhere += " and is_top=0 ";
                }
                var listdataset = articlebll.GetList(channel_id, category_id, pagesize, page, strwhere, strorder,out record_count);
                List<channel_article_live> modellist = new List<channel_article_live>();
                if (listdataset != null && listdataset.Tables.Count>0)
                {
                    var list = listdataset.Tables[0];
                    for (int i = 0; i < list.Rows.Count; i++)
                    {
                        channel_article_live model = new channel_article_live();
                        model.id = int.Parse(list.Rows[i]["id"].ToString());
                        model.channel_id = int.Parse(list.Rows[i]["channel_id"].ToString());
                        model.category_id = int.Parse(list.Rows[i]["category_id"].ToString());
                        model.title = list.Rows[i]["title"].ToString();
                        model.link_url = list.Rows[i]["link_url"].ToString();
                        model.img_url = list.Rows[i]["img_url"].ToString();
                        model.content = list.Rows[i]["content"].ToString();
                        model.sort_id = int.Parse(list.Rows[i]["sort_id"].ToString());
                        model.status = int.Parse(list.Rows[i]["status"].ToString());
                        model.add_time = DateTime.Parse(list.Rows[i]["add_time"].ToString());

                        model.tags = list.Rows[i]["tags"].ToString();// BLL_category.GetModel(int.Parse(list.Rows[i]["category_id"].ToString())).title;

                        modellist.Add(model);
                    }
                }

                res.ResultCount = modellist.Count;
                res.ResultCode = 1;
                res.ResultMessage = "成功";
                res.JsonEntity = modellist;

            }
            catch (Exception ex)
            {
                res.ResultCode = -2;
                res.ResultMessage = ex.Message;
            }
            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(res));
            context.Response.End();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }

    public class channel_article_live {
        public channel_article_live() {

        }
        private int _id;
        //private int _site_id = 0;
        private int _channel_id = 0;
        private int _category_id = 0;
        private string _title;
        private string _link_url;
        private string _img_url;
        //private string _seo_title;
        //private string _seo_keywords;
        //private string _seo_description;
        private string _tags;
        //private string _zhaiyao;
        private string _content;
        private int _sort_id = 99;
      //  private int _click = 0;
        private int _status = 0;
        private DateTime _add_time = DateTime.Now;
      //  private DateTime? _update_time;
        /// 
		/// </summary>
		public int id
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        //public int site_id
        //{
        //    set { _site_id = value; }
        //    get { return _site_id; }
        //}
        /// <summary>
        /// 
        /// </summary>
        public int channel_id
        {
            set { _channel_id = value; }
            get { return _channel_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int category_id
        {
            set { _category_id = value; }
            get { return _category_id; }
        }
        /// <summary>
		/// 
		/// </summary>
		public string title
        {
            set { _title = value; }
            get { return _title; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string link_url
        {
            set { _link_url = value; }
            get { return _link_url; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string img_url
        {
            set { _img_url = value; }
            get { return _img_url; }
        }
        ///// <summary>
        ///// 
        ///// </summary>
        //public string seo_title
        //{
        //    set { _seo_title = value; }
        //    get { return _seo_title; }
        //}
        ///// <summary>
        ///// 
        ///// </summary>
        //public string seo_keywords
        //{
        //    set { _seo_keywords = value; }
        //    get { return _seo_keywords; }
        //}
        /// <summary>
        /// 
        /// </summary>
        //      public string seo_description
        //      {
        //          set { _seo_description = value; }
        //          get { return _seo_description; }
        //      }
        /// <summary>
        /// 
        /// </summary>
        public string tags
        {
            set { _tags = value; }
            get { return _tags; }
        }
        //      /// <summary>
        ///// 
        ///// </summary>
        //public string zhaiyao
        //      {
        //          set { _zhaiyao = value; }
        //          get { return _zhaiyao; }
        //      }
        /// <summary>
        /// 
        /// </summary>
        public string content
        {
            set { _content = value; }
            get { return _content; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int sort_id
        {
            set { _sort_id = value; }
            get { return _sort_id; }
        }
  //      /// <summary>
		///// 
		///// </summary>
		//public int click
  //      {
  //          set { _click = value; }
  //          get { return _click; }
  //      }
        /// <summary>
        /// 
        /// </summary>
        public int status
        {
            set { _status = value; }
            get { return _status; }
        }
        /// <summary>
		/// 
		/// </summary>
		public DateTime add_time
        {
            set { _add_time = value; }
            get { return _add_time; }
        }
        /// <summary>
        /// 
        /// </summary>
        //public DateTime? update_time
        //{
        //    set { _update_time = value; }
        //    get { return _update_time; }
        //}
    }

    public class JsonResult<T>
    {

        public List<T> JsonEntity { get; set; }

        /// <summary>
        /// 无错误 1 面向用户的错误信息 0
        /// </summary>
        public int ResultCode { get; set; }

        public int ResultCount { get; set; }
        /// <summary>
        /// 错误信息
        /// </summary>
        public string ResultMessage { get; set; }

    }
}