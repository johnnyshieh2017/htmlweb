﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DTcms.Web
{
    public class Prize
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { set; get; }
        /// <summary>
        /// 中奖概率
        /// </summary>
        public decimal Probability { set; get; }
    }

    public class EveryRandomAlgo
    {
        public List<Prize> Prizes
        {
            private set;
            get;
        }
        public EveryRandomAlgo(List<Prize> prizes)
        {
            this.Prizes = prizes;
        }


        /// <summary>
        /// 随机下一次奖品
        /// </summary>
        /// <returns></returns>
        public string Next()
        {
            long tick = DateTime.Now.Ticks;
            Random ran = new Random((int)(tick & 0xffffffffL) | (int)(tick >> 32));  //产生不重复随机
            var rnum = ran.Next(0, 9999);//将0~100映射到0~10000，提高精度到小数点后2位；生成随机数rnum；
            //Console.WriteLine(rnum);
            var randomProbability = (decimal)rnum / 100;//再换算为0~100范围
            var target = 0;//命中index
            var end = Prizes[0].Probability;
            for (int k = -1; k < Prizes.Count - 1; k++)
            {
                var min = k < 0 ? 0 : Prizes[k].Probability;//最小中奖概率
                if (randomProbability >= min && randomProbability <= end)//随机出来的中奖概率位于中奖概率区间内
                {
                    target = k + 1;
                    break;
                }
                end += Prizes[k + 1].Probability;//最大中奖概率累计值
            }
            return Prizes[target].Name;
        }
    }
}