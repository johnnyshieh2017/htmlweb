﻿using System; 
using System.Text;
using System.Data.SqlClient;
using System.Collections.Generic; 
using System.Data;
using DTcms.DBUtility;
using DTcms.Common;
namespace DTcms.DAL  
{
	 	//View_doctor_group
		public partial class View_doctor_group
	{
   		     
		public bool Exists()
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from View_doctor_group");
			strSql.Append(" where ");
						SqlParameter[] parameters = {
			};

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}
		
				
		
		/// <summary>
		/// 增加一条数据
		/// </summary>
		public void Add(DTcms.Model.View_doctor_group model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into View_doctor_group(");			
            strSql.Append("id,groupname,sort,status,siteid,hospatilid,description,leader_desc,logo_url,hospitalname");
			strSql.Append(") values (");
            strSql.Append("@id,@groupname,@sort,@status,@siteid,@hospatilid,@description,@leader_desc,@logo_url,@hospitalname");            
            strSql.Append(") ");            
            		
			SqlParameter[] parameters = {
			            new SqlParameter("@id", SqlDbType.Int,4) ,            
                        new SqlParameter("@groupname", SqlDbType.VarChar,100) ,            
                        new SqlParameter("@sort", SqlDbType.Int,4) ,            
                        new SqlParameter("@status", SqlDbType.Int,4) ,            
                        new SqlParameter("@siteid", SqlDbType.Int,4) ,            
                        new SqlParameter("@hospatilid", SqlDbType.Int,4) ,            
                        new SqlParameter("@description", SqlDbType.Text) ,            
                        new SqlParameter("@leader_desc", SqlDbType.Text) ,            
                        new SqlParameter("@logo_url", SqlDbType.VarChar,120) ,            
                        new SqlParameter("@hospitalname", SqlDbType.NVarChar,50)             
              
            };
			            
            parameters[0].Value = model.id;                        
            parameters[1].Value = model.groupname;                        
            parameters[2].Value = model.sort;                        
            parameters[3].Value = model.status;                        
            parameters[4].Value = model.siteid;                        
            parameters[5].Value = model.hospatilid;                        
            parameters[6].Value = model.description;                        
            parameters[7].Value = model.leader_desc;                        
            parameters[8].Value = model.logo_url;                        
            parameters[9].Value = model.hospitalname;                        
			            DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
            			
		}
		
		
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTcms.Model.View_doctor_group model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update View_doctor_group set ");
			                        
            strSql.Append(" id = @id , ");                                    
            strSql.Append(" groupname = @groupname , ");                                    
            strSql.Append(" sort = @sort , ");                                    
            strSql.Append(" status = @status , ");                                    
            strSql.Append(" siteid = @siteid , ");                                    
            strSql.Append(" hospatilid = @hospatilid , ");                                    
            strSql.Append(" description = @description , ");                                    
            strSql.Append(" leader_desc = @leader_desc , ");                                    
            strSql.Append(" logo_url = @logo_url , ");                                    
            strSql.Append(" hospitalname = @hospitalname  ");            			
			strSql.Append(" where  ");
						
SqlParameter[] parameters = {
			            new SqlParameter("@id", SqlDbType.Int,4) ,            
                        new SqlParameter("@groupname", SqlDbType.VarChar,100) ,            
                        new SqlParameter("@sort", SqlDbType.Int,4) ,            
                        new SqlParameter("@status", SqlDbType.Int,4) ,            
                        new SqlParameter("@siteid", SqlDbType.Int,4) ,            
                        new SqlParameter("@hospatilid", SqlDbType.Int,4) ,            
                        new SqlParameter("@description", SqlDbType.Text) ,            
                        new SqlParameter("@leader_desc", SqlDbType.Text) ,            
                        new SqlParameter("@logo_url", SqlDbType.VarChar,120) ,            
                        new SqlParameter("@hospitalname", SqlDbType.NVarChar,50)             
              
            };
						            
            parameters[0].Value = model.id;                        
            parameters[1].Value = model.groupname;                        
            parameters[2].Value = model.sort;                        
            parameters[3].Value = model.status;                        
            parameters[4].Value = model.siteid;                        
            parameters[5].Value = model.hospatilid;                        
            parameters[6].Value = model.description;                        
            parameters[7].Value = model.leader_desc;                        
            parameters[8].Value = model.logo_url;                        
            parameters[9].Value = model.hospitalname;                        
            int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete()
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from View_doctor_group ");
			strSql.Append(" where ");
						SqlParameter[] parameters = {
			};


			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
				
		
		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.View_doctor_group GetModel()
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select id, groupname, sort, status, siteid, hospatilid, description, leader_desc, logo_url, hospitalname  ");			
			strSql.Append("  from View_doctor_group ");
			strSql.Append(" where ");
						SqlParameter[] parameters = {
			};

			
			DTcms.Model.View_doctor_group model=new DTcms.Model.View_doctor_group();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			
			if(ds.Tables[0].Rows.Count>0)
			{
												if(ds.Tables[0].Rows[0]["id"].ToString()!="")
				{
					model.id=int.Parse(ds.Tables[0].Rows[0]["id"].ToString());
				}
																																				model.groupname= ds.Tables[0].Rows[0]["groupname"].ToString();
																												if(ds.Tables[0].Rows[0]["sort"].ToString()!="")
				{
					model.sort=int.Parse(ds.Tables[0].Rows[0]["sort"].ToString());
				}
																																if(ds.Tables[0].Rows[0]["status"].ToString()!="")
				{
					model.status=int.Parse(ds.Tables[0].Rows[0]["status"].ToString());
				}
																																if(ds.Tables[0].Rows[0]["siteid"].ToString()!="")
				{
					model.siteid=int.Parse(ds.Tables[0].Rows[0]["siteid"].ToString());
				}
																																if(ds.Tables[0].Rows[0]["hospatilid"].ToString()!="")
				{
					model.hospatilid=int.Parse(ds.Tables[0].Rows[0]["hospatilid"].ToString());
				}
																																				model.description= ds.Tables[0].Rows[0]["description"].ToString();
																																model.leader_desc= ds.Tables[0].Rows[0]["leader_desc"].ToString();
																																model.logo_url= ds.Tables[0].Rows[0]["logo_url"].ToString();
																																model.hospitalname= ds.Tables[0].Rows[0]["hospitalname"].ToString();
																										
				return model;
			}
			else
			{
				return null;
			}
		}
		
		
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select * ");
			strSql.Append(" FROM View_doctor_group ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}
		
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" * ");
			strSql.Append(" FROM View_doctor_group ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得查询分页数据
		/// </summary>
		public DataSet GetList(int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
		 {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * FROM View_doctor_group ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            recordCount = Convert.ToInt32(DbHelperSQL.GetSingle(PagingHelper.CreateCountingSql(strSql.ToString())));
            return DbHelperSQL.Query(PagingHelper.CreatePagingSql(recordCount, pageSize, pageIndex, strSql.ToString(), filedOrder));
		}

   
	}
}

