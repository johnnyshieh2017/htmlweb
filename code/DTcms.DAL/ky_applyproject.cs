﻿/**  版本信息模板在安装目录下，可自行修改。
* ky_applyproject.cs
*
* 功 能： N/A
* 类 名： ky_applyproject
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2021/7/27 10:34:22   N/A    初版
*

*/
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DTcms.DBUtility;//Please add references

using DTcms.Common;

namespace DTcms.DAL
{
	/// <summary>
	/// 数据访问类:ky_applyproject
	/// </summary>
	public partial class ky_applyproject
	{
		public ky_applyproject()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("pa_id", "ky_applyproject"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int pa_id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from ky_applyproject");
			strSql.Append(" where pa_id=@pa_id");
			SqlParameter[] parameters = {
					new SqlParameter("@pa_id", SqlDbType.Int,4)
			};
			parameters[0].Value = pa_id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(DTcms.Model.ky_applyproject model)
		{
			StringBuilder strSql = new StringBuilder();
			strSql.Append("insert into ky_applyproject(");
			strSql.Append("pa_title,pa_code,pa_type,pa_dptid,pa_dptname,pa_user,dian_zi_you_xiang,shou_ji_hao_ma,shen_qing_ri_qi,add_user,add_time,status,beizhu,dan_wei_ji_ben_xin_xi,xiang_mu_ji_ben_xin_xi,xiang_mu_tuan_dui,xiang_mu_gai_kuang,xiang_mu_shi_shi,xiang_mu_tou_zi,xiang_mu_ji_xiao,base_didian,base_yanjiu_xingshi,base_qizhishijian,base_hezuo_danwei,base_hezuo_dizhi,base_hezuo_lianxiren,base_hezuo_lianxidianhua,base_xmgk_zhaiyao,base_xmgk_neirongmubiao,base_xmgk_jishuluxian,base_xmgk_chuangxindian,base_xmgk_jichutiaojian,base_xmgk_xiaoyi,base_xmgk_zhibiao,team_owner_name,team_owner_sex,team_owner_chusheng,team_owner_xueli,team_owner_zhiwu,team_owner_zhicheng,team_owner_shouji,team_owner_zhuanye,team_owner_jianjie,jindu_jihua,keyan_baogao)");
			strSql.Append(" values (");
			strSql.Append("@pa_title,@pa_code,@pa_type,@pa_dptid,@pa_dptname,@pa_user,@dian_zi_you_xiang,@shou_ji_hao_ma,@shen_qing_ri_qi,@add_user,@add_time,@status,@beizhu,@dan_wei_ji_ben_xin_xi,@xiang_mu_ji_ben_xin_xi,@xiang_mu_tuan_dui,@xiang_mu_gai_kuang,@xiang_mu_shi_shi,@xiang_mu_tou_zi,@xiang_mu_ji_xiao,@base_didian,@base_yanjiu_xingshi,@base_qizhishijian,@base_hezuo_danwei,@base_hezuo_dizhi,@base_hezuo_lianxiren,@base_hezuo_lianxidianhua,@base_xmgk_zhaiyao,@base_xmgk_neirongmubiao,@base_xmgk_jishuluxian,@base_xmgk_chuangxindian,@base_xmgk_jichutiaojian,@base_xmgk_xiaoyi,@base_xmgk_zhibiao,@team_owner_name,@team_owner_sex,@team_owner_chusheng,@team_owner_xueli,@team_owner_zhiwu,@team_owner_zhicheng,@team_owner_shouji,@team_owner_zhuanye,@team_owner_jianjie,@jindu_jihua,@keyan_baogao)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@pa_title", SqlDbType.VarChar,50),
					new SqlParameter("@pa_code", SqlDbType.VarChar,50),
					new SqlParameter("@pa_type", SqlDbType.VarChar,50),
					new SqlParameter("@pa_dptid", SqlDbType.VarChar,50),
					new SqlParameter("@pa_dptname", SqlDbType.VarChar,50),
					new SqlParameter("@pa_user", SqlDbType.VarChar,50),
					new SqlParameter("@dian_zi_you_xiang", SqlDbType.VarChar,50),
					new SqlParameter("@shou_ji_hao_ma", SqlDbType.VarChar,50),
					new SqlParameter("@shen_qing_ri_qi", SqlDbType.VarChar,50),
					new SqlParameter("@add_user", SqlDbType.VarChar,50),
					new SqlParameter("@add_time", SqlDbType.DateTime),
					new SqlParameter("@status", SqlDbType.Int,4),
					new SqlParameter("@beizhu", SqlDbType.VarChar,500),
					new SqlParameter("@dan_wei_ji_ben_xin_xi", SqlDbType.VarChar,200),
					new SqlParameter("@xiang_mu_ji_ben_xin_xi", SqlDbType.VarChar,200),
					new SqlParameter("@xiang_mu_tuan_dui", SqlDbType.VarChar,200),
					new SqlParameter("@xiang_mu_gai_kuang", SqlDbType.VarChar,2000),
					new SqlParameter("@xiang_mu_shi_shi", SqlDbType.VarChar,2000),
					new SqlParameter("@xiang_mu_tou_zi", SqlDbType.VarChar,2000),
					new SqlParameter("@xiang_mu_ji_xiao", SqlDbType.VarChar,2000),
					new SqlParameter("@base_didian", SqlDbType.VarChar,50),
					new SqlParameter("@base_yanjiu_xingshi", SqlDbType.VarChar,50),
					new SqlParameter("@base_qizhishijian", SqlDbType.VarChar,50),
					new SqlParameter("@base_hezuo_danwei", SqlDbType.VarChar,50),
					new SqlParameter("@base_hezuo_dizhi", SqlDbType.VarChar,50),
					new SqlParameter("@base_hezuo_lianxiren", SqlDbType.VarChar,50),
					new SqlParameter("@base_hezuo_lianxidianhua", SqlDbType.VarChar,50),
					new SqlParameter("@base_xmgk_zhaiyao", SqlDbType.VarChar,1000),
					new SqlParameter("@base_xmgk_neirongmubiao", SqlDbType.VarChar,1000),
					new SqlParameter("@base_xmgk_jishuluxian", SqlDbType.VarChar,1000),
					new SqlParameter("@base_xmgk_chuangxindian", SqlDbType.VarChar,1000),
					new SqlParameter("@base_xmgk_jichutiaojian", SqlDbType.VarChar,1000),
					new SqlParameter("@base_xmgk_xiaoyi", SqlDbType.VarChar,500),
					new SqlParameter("@base_xmgk_zhibiao", SqlDbType.VarChar,800),
					new SqlParameter("@team_owner_name", SqlDbType.VarChar,50),
					new SqlParameter("@team_owner_sex", SqlDbType.VarChar,50),
					new SqlParameter("@team_owner_chusheng", SqlDbType.VarChar,50),
					new SqlParameter("@team_owner_xueli", SqlDbType.VarChar,50),
					new SqlParameter("@team_owner_zhiwu", SqlDbType.VarChar,50),
					new SqlParameter("@team_owner_zhicheng", SqlDbType.VarChar,50),
					new SqlParameter("@team_owner_shouji", SqlDbType.VarChar,50),
					new SqlParameter("@team_owner_zhuanye", SqlDbType.VarChar,50),
					new SqlParameter("@team_owner_jianjie", SqlDbType.VarChar,500),
					new SqlParameter("@jindu_jihua", SqlDbType.VarChar,3000),
					new SqlParameter("@keyan_baogao", SqlDbType.VarChar,200)};
			parameters[0].Value = model.pa_title;
			parameters[1].Value = model.pa_code;
			parameters[2].Value = model.pa_type;
			parameters[3].Value = model.pa_dptid;
			parameters[4].Value = model.pa_dptname;
			parameters[5].Value = model.pa_user;
			parameters[6].Value = model.dian_zi_you_xiang;
			parameters[7].Value = model.shou_ji_hao_ma;
			parameters[8].Value = model.shen_qing_ri_qi;
			parameters[9].Value = model.add_user;
			parameters[10].Value = model.add_time;
			parameters[11].Value = model.status;
			parameters[12].Value = model.beizhu;
			parameters[13].Value = model.dan_wei_ji_ben_xin_xi;
			parameters[14].Value = model.xiang_mu_ji_ben_xin_xi;
			parameters[15].Value = model.xiang_mu_tuan_dui;
			parameters[16].Value = model.xiang_mu_gai_kuang;
			parameters[17].Value = model.xiang_mu_shi_shi;
			parameters[18].Value = model.xiang_mu_tou_zi;
			parameters[19].Value = model.xiang_mu_ji_xiao;
			parameters[20].Value = model.base_didian;
			parameters[21].Value = model.base_yanjiu_xingshi;
			parameters[22].Value = model.base_qizhishijian;
			parameters[23].Value = model.base_hezuo_danwei;
			parameters[24].Value = model.base_hezuo_dizhi;
			parameters[25].Value = model.base_hezuo_lianxiren;
			parameters[26].Value = model.base_hezuo_lianxidianhua;
			parameters[27].Value = model.base_xmgk_zhaiyao;
			parameters[28].Value = model.base_xmgk_neirongmubiao;
			parameters[29].Value = model.base_xmgk_jishuluxian;
			parameters[30].Value = model.base_xmgk_chuangxindian;
			parameters[31].Value = model.base_xmgk_jichutiaojian;
			parameters[32].Value = model.base_xmgk_xiaoyi;
			parameters[33].Value = model.base_xmgk_zhibiao;
			parameters[34].Value = model.team_owner_name;
			parameters[35].Value = model.team_owner_sex;
			parameters[36].Value = model.team_owner_chusheng;
			parameters[37].Value = model.team_owner_xueli;
			parameters[38].Value = model.team_owner_zhiwu;
			parameters[39].Value = model.team_owner_zhicheng;
			parameters[40].Value = model.team_owner_shouji;
			parameters[41].Value = model.team_owner_zhuanye;
			parameters[42].Value = model.team_owner_jianjie;
			parameters[43].Value = model.jindu_jihua;
			parameters[44].Value = model.keyan_baogao;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTcms.Model.ky_applyproject model)
		{
			StringBuilder strSql = new StringBuilder();
			strSql.Append("update ky_applyproject set ");
			strSql.Append("pa_title=@pa_title,");
			strSql.Append("pa_code=@pa_code,");
			strSql.Append("pa_type=@pa_type,");
			strSql.Append("pa_dptid=@pa_dptid,");
			strSql.Append("pa_dptname=@pa_dptname,");
			strSql.Append("pa_user=@pa_user,");
			strSql.Append("dian_zi_you_xiang=@dian_zi_you_xiang,");
			strSql.Append("shou_ji_hao_ma=@shou_ji_hao_ma,");
			strSql.Append("shen_qing_ri_qi=@shen_qing_ri_qi,");
			strSql.Append("add_user=@add_user,");
			strSql.Append("add_time=@add_time,");
			strSql.Append("status=@status,");
			strSql.Append("beizhu=@beizhu,");
			strSql.Append("dan_wei_ji_ben_xin_xi=@dan_wei_ji_ben_xin_xi,");
			strSql.Append("xiang_mu_ji_ben_xin_xi=@xiang_mu_ji_ben_xin_xi,");
			strSql.Append("xiang_mu_tuan_dui=@xiang_mu_tuan_dui,");
			strSql.Append("xiang_mu_gai_kuang=@xiang_mu_gai_kuang,");
			strSql.Append("xiang_mu_shi_shi=@xiang_mu_shi_shi,");
			strSql.Append("xiang_mu_tou_zi=@xiang_mu_tou_zi,");
			strSql.Append("xiang_mu_ji_xiao=@xiang_mu_ji_xiao,");
			strSql.Append("base_didian=@base_didian,");
			strSql.Append("base_yanjiu_xingshi=@base_yanjiu_xingshi,");
			strSql.Append("base_qizhishijian=@base_qizhishijian,");
			strSql.Append("base_hezuo_danwei=@base_hezuo_danwei,");
			strSql.Append("base_hezuo_dizhi=@base_hezuo_dizhi,");
			strSql.Append("base_hezuo_lianxiren=@base_hezuo_lianxiren,");
			strSql.Append("base_hezuo_lianxidianhua=@base_hezuo_lianxidianhua,");
			strSql.Append("base_xmgk_zhaiyao=@base_xmgk_zhaiyao,");
			strSql.Append("base_xmgk_neirongmubiao=@base_xmgk_neirongmubiao,");
			strSql.Append("base_xmgk_jishuluxian=@base_xmgk_jishuluxian,");
			strSql.Append("base_xmgk_chuangxindian=@base_xmgk_chuangxindian,");
			strSql.Append("base_xmgk_jichutiaojian=@base_xmgk_jichutiaojian,");
			strSql.Append("base_xmgk_xiaoyi=@base_xmgk_xiaoyi,");
			strSql.Append("base_xmgk_zhibiao=@base_xmgk_zhibiao,");
			strSql.Append("team_owner_name=@team_owner_name,");
			strSql.Append("team_owner_sex=@team_owner_sex,");
			strSql.Append("team_owner_chusheng=@team_owner_chusheng,");
			strSql.Append("team_owner_xueli=@team_owner_xueli,");
			strSql.Append("team_owner_zhiwu=@team_owner_zhiwu,");
			strSql.Append("team_owner_zhicheng=@team_owner_zhicheng,");
			strSql.Append("team_owner_shouji=@team_owner_shouji,");
			strSql.Append("team_owner_zhuanye=@team_owner_zhuanye,");
			strSql.Append("team_owner_jianjie=@team_owner_jianjie,");
			strSql.Append("jindu_jihua=@jindu_jihua,");
			strSql.Append("keyan_baogao=@keyan_baogao");
			strSql.Append(" where pa_id=@pa_id");
			SqlParameter[] parameters = {
					new SqlParameter("@pa_title", SqlDbType.VarChar,50),
					new SqlParameter("@pa_code", SqlDbType.VarChar,50),
					new SqlParameter("@pa_type", SqlDbType.VarChar,50),
					new SqlParameter("@pa_dptid", SqlDbType.VarChar,50),
					new SqlParameter("@pa_dptname", SqlDbType.VarChar,50),
					new SqlParameter("@pa_user", SqlDbType.VarChar,50),
					new SqlParameter("@dian_zi_you_xiang", SqlDbType.VarChar,50),
					new SqlParameter("@shou_ji_hao_ma", SqlDbType.VarChar,50),
					new SqlParameter("@shen_qing_ri_qi", SqlDbType.VarChar,50),
					new SqlParameter("@add_user", SqlDbType.VarChar,50),
					new SqlParameter("@add_time", SqlDbType.DateTime),
					new SqlParameter("@status", SqlDbType.Int,4),
					new SqlParameter("@beizhu", SqlDbType.VarChar,500),
					new SqlParameter("@dan_wei_ji_ben_xin_xi", SqlDbType.VarChar,200),
					new SqlParameter("@xiang_mu_ji_ben_xin_xi", SqlDbType.VarChar,200),
					new SqlParameter("@xiang_mu_tuan_dui", SqlDbType.VarChar,200),
					new SqlParameter("@xiang_mu_gai_kuang", SqlDbType.VarChar,2000),
					new SqlParameter("@xiang_mu_shi_shi", SqlDbType.VarChar,2000),
					new SqlParameter("@xiang_mu_tou_zi", SqlDbType.VarChar,2000),
					new SqlParameter("@xiang_mu_ji_xiao", SqlDbType.VarChar,2000),
					new SqlParameter("@base_didian", SqlDbType.VarChar,50),
					new SqlParameter("@base_yanjiu_xingshi", SqlDbType.VarChar,50),
					new SqlParameter("@base_qizhishijian", SqlDbType.VarChar,50),
					new SqlParameter("@base_hezuo_danwei", SqlDbType.VarChar,50),
					new SqlParameter("@base_hezuo_dizhi", SqlDbType.VarChar,50),
					new SqlParameter("@base_hezuo_lianxiren", SqlDbType.VarChar,50),
					new SqlParameter("@base_hezuo_lianxidianhua", SqlDbType.VarChar,50),
					new SqlParameter("@base_xmgk_zhaiyao", SqlDbType.VarChar,1000),
					new SqlParameter("@base_xmgk_neirongmubiao", SqlDbType.VarChar,1000),
					new SqlParameter("@base_xmgk_jishuluxian", SqlDbType.VarChar,1000),
					new SqlParameter("@base_xmgk_chuangxindian", SqlDbType.VarChar,1000),
					new SqlParameter("@base_xmgk_jichutiaojian", SqlDbType.VarChar,1000),
					new SqlParameter("@base_xmgk_xiaoyi", SqlDbType.VarChar,500),
					new SqlParameter("@base_xmgk_zhibiao", SqlDbType.VarChar,800),
					new SqlParameter("@team_owner_name", SqlDbType.VarChar,50),
					new SqlParameter("@team_owner_sex", SqlDbType.VarChar,50),
					new SqlParameter("@team_owner_chusheng", SqlDbType.VarChar,50),
					new SqlParameter("@team_owner_xueli", SqlDbType.VarChar,50),
					new SqlParameter("@team_owner_zhiwu", SqlDbType.VarChar,50),
					new SqlParameter("@team_owner_zhicheng", SqlDbType.VarChar,50),
					new SqlParameter("@team_owner_shouji", SqlDbType.VarChar,50),
					new SqlParameter("@team_owner_zhuanye", SqlDbType.VarChar,50),
					new SqlParameter("@team_owner_jianjie", SqlDbType.VarChar,500),
					new SqlParameter("@jindu_jihua", SqlDbType.VarChar,3000),
					new SqlParameter("@keyan_baogao", SqlDbType.VarChar,200),
					new SqlParameter("@pa_id", SqlDbType.Int,4)};
			parameters[0].Value = model.pa_title;
			parameters[1].Value = model.pa_code;
			parameters[2].Value = model.pa_type;
			parameters[3].Value = model.pa_dptid;
			parameters[4].Value = model.pa_dptname;
			parameters[5].Value = model.pa_user;
			parameters[6].Value = model.dian_zi_you_xiang;
			parameters[7].Value = model.shou_ji_hao_ma;
			parameters[8].Value = model.shen_qing_ri_qi;
			parameters[9].Value = model.add_user;
			parameters[10].Value = model.add_time;
			parameters[11].Value = model.status;
			parameters[12].Value = model.beizhu;
			parameters[13].Value = model.dan_wei_ji_ben_xin_xi;
			parameters[14].Value = model.xiang_mu_ji_ben_xin_xi;
			parameters[15].Value = model.xiang_mu_tuan_dui;
			parameters[16].Value = model.xiang_mu_gai_kuang;
			parameters[17].Value = model.xiang_mu_shi_shi;
			parameters[18].Value = model.xiang_mu_tou_zi;
			parameters[19].Value = model.xiang_mu_ji_xiao;
			parameters[20].Value = model.base_didian;
			parameters[21].Value = model.base_yanjiu_xingshi;
			parameters[22].Value = model.base_qizhishijian;
			parameters[23].Value = model.base_hezuo_danwei;
			parameters[24].Value = model.base_hezuo_dizhi;
			parameters[25].Value = model.base_hezuo_lianxiren;
			parameters[26].Value = model.base_hezuo_lianxidianhua;
			parameters[27].Value = model.base_xmgk_zhaiyao;
			parameters[28].Value = model.base_xmgk_neirongmubiao;
			parameters[29].Value = model.base_xmgk_jishuluxian;
			parameters[30].Value = model.base_xmgk_chuangxindian;
			parameters[31].Value = model.base_xmgk_jichutiaojian;
			parameters[32].Value = model.base_xmgk_xiaoyi;
			parameters[33].Value = model.base_xmgk_zhibiao;
			parameters[34].Value = model.team_owner_name;
			parameters[35].Value = model.team_owner_sex;
			parameters[36].Value = model.team_owner_chusheng;
			parameters[37].Value = model.team_owner_xueli;
			parameters[38].Value = model.team_owner_zhiwu;
			parameters[39].Value = model.team_owner_zhicheng;
			parameters[40].Value = model.team_owner_shouji;
			parameters[41].Value = model.team_owner_zhuanye;
			parameters[42].Value = model.team_owner_jianjie;
			parameters[43].Value = model.jindu_jihua;
			parameters[44].Value = model.keyan_baogao;
			parameters[45].Value = model.pa_id;

			int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int pa_id)
		{

			StringBuilder strSql = new StringBuilder();
			strSql.Append("delete from ky_applyproject ");
			strSql.Append(" where pa_id=@pa_id");
			SqlParameter[] parameters = {
					new SqlParameter("@pa_id", SqlDbType.Int,4)
			};
			parameters[0].Value = pa_id;

			int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string pa_idlist)
		{
			StringBuilder strSql = new StringBuilder();
			strSql.Append("delete from ky_applyproject ");
			strSql.Append(" where pa_id in (" + pa_idlist + ")  ");
			int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.ky_applyproject GetModel(int pa_id)
		{

			StringBuilder strSql = new StringBuilder();
			//strSql.Append("select  top 1 pa_id,pa_title,pa_code,pa_type,pa_dptid,pa_dptname,pa_user,dian_zi_you_xiang,shou_ji_hao_ma,shen_qing_ri_qi,add_user,add_time,status,beizhu,dan_wei_ji_ben_xin_xi,xiang_mu_ji_ben_xin_xi,xiang_mu_tuan_dui,xiang_mu_gai_kuang,xiang_mu_shi_shi,xiang_mu_tou_zi,xiang_mu_ji_xiao,base_didian,base_yanjiu_xingshi,base_qizhishijian,base_hezuo_danwei,base_hezuo_dizhi,base_hezuo_lianxiren,base_hezuo_lianxidianhua,base_xmgk_zhaiyao,base_xmgk_neirongmubiao,base_xmgk_jishuluxian,base_xmgk_chuangxindian,base_xmgk_jichutiaojian,base_xmgk_xiaoyi,base_xmgk_zhibiao,team_owner_name,team_owner_sex,team_owner_chusheng,team_owner_xueli,team_owner_zhiwu,team_owner_zhicheng,team_owner_shouji,team_owner_zhuanye,team_owner_jianjie,jindu_jihua,keyan_baogao from ky_applyproject ");

			strSql.Append("select  top 1 pa_id,pa_title,pa_code,pa_type,pa_dptid,(select top 1 hospitalname from yjr_doc_hospital where yjr_doc_hospital.id =ky_applyproject.pa_dptid ) as pa_dptname,pa_user,dian_zi_you_xiang,shou_ji_hao_ma,shen_qing_ri_qi,add_user,add_time,status,beizhu,dan_wei_ji_ben_xin_xi,xiang_mu_ji_ben_xin_xi,xiang_mu_tuan_dui,xiang_mu_gai_kuang,xiang_mu_shi_shi,xiang_mu_tou_zi,xiang_mu_ji_xiao,base_didian,base_yanjiu_xingshi,base_qizhishijian,base_hezuo_danwei,base_hezuo_dizhi,base_hezuo_lianxiren,base_hezuo_lianxidianhua,base_xmgk_zhaiyao,base_xmgk_neirongmubiao,base_xmgk_jishuluxian,base_xmgk_chuangxindian,base_xmgk_jichutiaojian,base_xmgk_xiaoyi,base_xmgk_zhibiao,team_owner_name,team_owner_sex,team_owner_chusheng,team_owner_xueli,team_owner_zhiwu,team_owner_zhicheng,team_owner_shouji,team_owner_zhuanye,team_owner_jianjie,jindu_jihua,keyan_baogao from ky_applyproject ");
			strSql.Append(" where pa_id=@pa_id");
			//strSql.Append(" where pa_id=@pa_id");
			SqlParameter[] parameters = {
					new SqlParameter("@pa_id", SqlDbType.Int,4)
			};
			parameters[0].Value = pa_id;

			DTcms.Model.ky_applyproject model = new DTcms.Model.ky_applyproject();
			DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
			if (ds.Tables[0].Rows.Count > 0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.ky_applyproject DataRowToModel(DataRow row)
		{
			DTcms.Model.ky_applyproject model = new DTcms.Model.ky_applyproject();
			if (row != null)
			{
				if (row["pa_id"] != null && row["pa_id"].ToString() != "")
				{
					model.pa_id = int.Parse(row["pa_id"].ToString());
				}
				if (row["pa_title"] != null)
				{
					model.pa_title = row["pa_title"].ToString();
				}
				if (row["pa_code"] != null)
				{
					model.pa_code = row["pa_code"].ToString();
				}
				if (row["pa_type"] != null)
				{
					model.pa_type = row["pa_type"].ToString();
				}
				if (row["pa_dptid"] != null)
				{
					model.pa_dptid = row["pa_dptid"].ToString();
				}
				if (row["pa_dptname"] != null)
				{
					model.pa_dptname = row["pa_dptname"].ToString();
				}
				if (row["pa_user"] != null)
				{
					model.pa_user = row["pa_user"].ToString();
				}
				if (row["dian_zi_you_xiang"] != null)
				{
					model.dian_zi_you_xiang = row["dian_zi_you_xiang"].ToString();
				}
				if (row["shou_ji_hao_ma"] != null)
				{
					model.shou_ji_hao_ma = row["shou_ji_hao_ma"].ToString();
				}
				if (row["shen_qing_ri_qi"] != null)
				{
					model.shen_qing_ri_qi = row["shen_qing_ri_qi"].ToString();
				}
				if (row["add_user"] != null)
				{
					model.add_user = row["add_user"].ToString();
				}
				if (row["add_time"] != null && row["add_time"].ToString() != "")
				{
					model.add_time = DateTime.Parse(row["add_time"].ToString());
				}
				if (row["status"] != null && row["status"].ToString() != "")
				{
					model.status = int.Parse(row["status"].ToString());
				}
				if (row["beizhu"] != null)
				{
					model.beizhu = row["beizhu"].ToString();
				}
				if (row["dan_wei_ji_ben_xin_xi"] != null)
				{
					model.dan_wei_ji_ben_xin_xi = row["dan_wei_ji_ben_xin_xi"].ToString();
				}
				if (row["xiang_mu_ji_ben_xin_xi"] != null)
				{
					model.xiang_mu_ji_ben_xin_xi = row["xiang_mu_ji_ben_xin_xi"].ToString();
				}
				if (row["xiang_mu_tuan_dui"] != null)
				{
					model.xiang_mu_tuan_dui = row["xiang_mu_tuan_dui"].ToString();
				}
				if (row["xiang_mu_gai_kuang"] != null)
				{
					model.xiang_mu_gai_kuang = row["xiang_mu_gai_kuang"].ToString();
				}
				if (row["xiang_mu_shi_shi"] != null)
				{
					model.xiang_mu_shi_shi = row["xiang_mu_shi_shi"].ToString();
				}
				if (row["xiang_mu_tou_zi"] != null)
				{
					model.xiang_mu_tou_zi = row["xiang_mu_tou_zi"].ToString();
				}
				if (row["xiang_mu_ji_xiao"] != null)
				{
					model.xiang_mu_ji_xiao = row["xiang_mu_ji_xiao"].ToString();
				}
				if (row["base_didian"] != null)
				{
					model.base_didian = row["base_didian"].ToString();
				}
				if (row["base_yanjiu_xingshi"] != null)
				{
					model.base_yanjiu_xingshi = row["base_yanjiu_xingshi"].ToString();
				}
				if (row["base_qizhishijian"] != null)
				{
					model.base_qizhishijian = row["base_qizhishijian"].ToString();
				}
				if (row["base_hezuo_danwei"] != null)
				{
					model.base_hezuo_danwei = row["base_hezuo_danwei"].ToString();
				}
				if (row["base_hezuo_dizhi"] != null)
				{
					model.base_hezuo_dizhi = row["base_hezuo_dizhi"].ToString();
				}
				if (row["base_hezuo_lianxiren"] != null)
				{
					model.base_hezuo_lianxiren = row["base_hezuo_lianxiren"].ToString();
				}
				if (row["base_hezuo_lianxidianhua"] != null)
				{
					model.base_hezuo_lianxidianhua = row["base_hezuo_lianxidianhua"].ToString();
				}
				if (row["base_xmgk_zhaiyao"] != null)
				{
					model.base_xmgk_zhaiyao = row["base_xmgk_zhaiyao"].ToString();
				}
				if (row["base_xmgk_neirongmubiao"] != null)
				{
					model.base_xmgk_neirongmubiao = row["base_xmgk_neirongmubiao"].ToString();
				}
				if (row["base_xmgk_jishuluxian"] != null)
				{
					model.base_xmgk_jishuluxian = row["base_xmgk_jishuluxian"].ToString();
				}
				if (row["base_xmgk_chuangxindian"] != null)
				{
					model.base_xmgk_chuangxindian = row["base_xmgk_chuangxindian"].ToString();
				}
				if (row["base_xmgk_jichutiaojian"] != null)
				{
					model.base_xmgk_jichutiaojian = row["base_xmgk_jichutiaojian"].ToString();
				}
				if (row["base_xmgk_xiaoyi"] != null)
				{
					model.base_xmgk_xiaoyi = row["base_xmgk_xiaoyi"].ToString();
				}
				if (row["base_xmgk_zhibiao"] != null)
				{
					model.base_xmgk_zhibiao = row["base_xmgk_zhibiao"].ToString();
				}
				if (row["team_owner_name"] != null)
				{
					model.team_owner_name = row["team_owner_name"].ToString();
				}
				if (row["team_owner_sex"] != null)
				{
					model.team_owner_sex = row["team_owner_sex"].ToString();
				}
				if (row["team_owner_chusheng"] != null)
				{
					model.team_owner_chusheng = row["team_owner_chusheng"].ToString();
				}
				if (row["team_owner_xueli"] != null)
				{
					model.team_owner_xueli = row["team_owner_xueli"].ToString();
				}
				if (row["team_owner_zhiwu"] != null)
				{
					model.team_owner_zhiwu = row["team_owner_zhiwu"].ToString();
				}
				if (row["team_owner_zhicheng"] != null)
				{
					model.team_owner_zhicheng = row["team_owner_zhicheng"].ToString();
				}
				if (row["team_owner_shouji"] != null)
				{
					model.team_owner_shouji = row["team_owner_shouji"].ToString();
				}
				if (row["team_owner_zhuanye"] != null)
				{
					model.team_owner_zhuanye = row["team_owner_zhuanye"].ToString();
				}
				if (row["team_owner_jianjie"] != null)
				{
					model.team_owner_jianjie = row["team_owner_jianjie"].ToString();
				}
				if (row["jindu_jihua"] != null)
				{
					model.jindu_jihua = row["jindu_jihua"].ToString();
				}
				if (row["keyan_baogao"] != null)
				{
					model.keyan_baogao = row["keyan_baogao"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql = new StringBuilder();
			strSql.Append("select pa_id,pa_title,pa_code,pa_type,pa_dptid,pa_dptname,pa_user,dian_zi_you_xiang,shou_ji_hao_ma,shen_qing_ri_qi,add_user,add_time,status,beizhu,dan_wei_ji_ben_xin_xi,xiang_mu_ji_ben_xin_xi,xiang_mu_tuan_dui,xiang_mu_gai_kuang,xiang_mu_shi_shi,xiang_mu_tou_zi,xiang_mu_ji_xiao,base_didian,base_yanjiu_xingshi,base_qizhishijian,base_hezuo_danwei,base_hezuo_dizhi,base_hezuo_lianxiren,base_hezuo_lianxidianhua,base_xmgk_zhaiyao,base_xmgk_neirongmubiao,base_xmgk_jishuluxian,base_xmgk_chuangxindian,base_xmgk_jichutiaojian,base_xmgk_xiaoyi,base_xmgk_zhibiao,team_owner_name,team_owner_sex,team_owner_chusheng,team_owner_xueli,team_owner_zhiwu,team_owner_zhicheng,team_owner_shouji,team_owner_zhuanye,team_owner_jianjie,jindu_jihua,keyan_baogao ");
			strSql.Append(" FROM ky_applyproject ");
			if (strWhere.Trim() != "")
			{
				strSql.Append(" where " + strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top, string strWhere, string filedOrder)
		{
			StringBuilder strSql = new StringBuilder();
			strSql.Append("select ");
			if (Top > 0)
			{
				strSql.Append(" top " + Top.ToString());
			}
			strSql.Append(" pa_id,pa_title,pa_code,pa_type,pa_dptid,pa_dptname,pa_user,dian_zi_you_xiang,shou_ji_hao_ma,shen_qing_ri_qi,add_user,add_time,status,beizhu,dan_wei_ji_ben_xin_xi,xiang_mu_ji_ben_xin_xi,xiang_mu_tuan_dui,xiang_mu_gai_kuang,xiang_mu_shi_shi,xiang_mu_tou_zi,xiang_mu_ji_xiao,base_didian,base_yanjiu_xingshi,base_qizhishijian,base_hezuo_danwei,base_hezuo_dizhi,base_hezuo_lianxiren,base_hezuo_lianxidianhua,base_xmgk_zhaiyao,base_xmgk_neirongmubiao,base_xmgk_jishuluxian,base_xmgk_chuangxindian,base_xmgk_jichutiaojian,base_xmgk_xiaoyi,base_xmgk_zhibiao,team_owner_name,team_owner_sex,team_owner_chusheng,team_owner_xueli,team_owner_zhiwu,team_owner_zhicheng,team_owner_shouji,team_owner_zhuanye,team_owner_jianjie,jindu_jihua,keyan_baogao ");
			strSql.Append(" FROM ky_applyproject ");
			if (strWhere.Trim() != "")
			{
				strSql.Append(" where " + strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}


		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM ky_applyproject ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.pa_id desc");
			}
			strSql.Append(")AS Row, T.*  from ky_applyproject T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "ky_applyproject";
			parameters[1].Value = "pa_id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod
		public DataSet GetList(int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
		{
			StringBuilder strSql = new StringBuilder();
			strSql.Append("select * FROM View_ky_applyproject ");
			if (strWhere.Trim() != "")
			{
				strSql.Append(" where " + strWhere);
			}
			recordCount = Convert.ToInt32(DbHelperSQL.GetSingle(PagingHelper.CreateCountingSql(strSql.ToString())));
			return DbHelperSQL.Query(PagingHelper.CreatePagingSql(recordCount, pageSize, pageIndex, strSql.ToString(), filedOrder));
		}
		#endregion  ExtensionMethod
	}
}

