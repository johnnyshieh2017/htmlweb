﻿using System; 
using System.Text;
using System.Data.SqlClient;
using System.Collections.Generic; 
using System.Data;
using DTcms.DBUtility;
using DTcms.Common;
namespace DTcms.DAL  
{
	 	//yjr_doctor_vsbusiness
		public partial class yjr_doctor_vsbusiness
	{
   		     
		public bool Exists(int doctorid,int businessid)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from yjr_doctor_vsbusiness");
			strSql.Append(" where ");
			                                       strSql.Append(" doctorid = @doctorid and  ");
                                                                   strSql.Append(" businessid = @businessid  ");
                            			SqlParameter[] parameters = {
					new SqlParameter("@doctorid", SqlDbType.Int,4),
					new SqlParameter("@businessid", SqlDbType.Int,4)			};
			parameters[0].Value = doctorid;
			parameters[1].Value = businessid;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}
		
				
		
		/// <summary>
		/// 增加一条数据
		/// </summary>
		public void Add(DTcms.Model.yjr_doctor_vsbusiness model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into yjr_doctor_vsbusiness(");			
            strSql.Append("doctorid,businessid,add_time,status");
			strSql.Append(") values (");
            strSql.Append("@doctorid,@businessid,@add_time,@status");            
            strSql.Append(") ");            
            		
			SqlParameter[] parameters = {
			            new SqlParameter("@doctorid", SqlDbType.Int,4) ,            
                        new SqlParameter("@businessid", SqlDbType.Int,4) ,            
                        new SqlParameter("@add_time", SqlDbType.DateTime) ,            
                        new SqlParameter("@status", SqlDbType.Int,4)             
              
            };
			            
            parameters[0].Value = model.doctorid;                        
            parameters[1].Value = model.businessid;                        
            parameters[2].Value = model.add_time;                        
            parameters[3].Value = model.status;                        
			            DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
            			
		}
		
		
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTcms.Model.yjr_doctor_vsbusiness model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update yjr_doctor_vsbusiness set ");
			                        
            strSql.Append(" doctorid = @doctorid , ");                                    
            strSql.Append(" businessid = @businessid , ");                                    
            strSql.Append(" add_time = @add_time , ");                                    
            strSql.Append(" status = @status  ");            			
			strSql.Append(" where doctorid=@doctorid and businessid=@businessid  ");
						
SqlParameter[] parameters = {
			            new SqlParameter("@doctorid", SqlDbType.Int,4) ,            
                        new SqlParameter("@businessid", SqlDbType.Int,4) ,            
                        new SqlParameter("@add_time", SqlDbType.DateTime) ,            
                        new SqlParameter("@status", SqlDbType.Int,4)             
              
            };
						            
            parameters[0].Value = model.doctorid;                        
            parameters[1].Value = model.businessid;                        
            parameters[2].Value = model.add_time;                        
            parameters[3].Value = model.status;                        
            int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int doctorid,int businessid)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from yjr_doctor_vsbusiness ");
			strSql.Append(" where doctorid=@doctorid and businessid=@businessid ");
						SqlParameter[] parameters = {
					new SqlParameter("@doctorid", SqlDbType.Int,4),
					new SqlParameter("@businessid", SqlDbType.Int,4)			};
			parameters[0].Value = doctorid;
			parameters[1].Value = businessid;


			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
				
		
		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.yjr_doctor_vsbusiness GetModel(int doctorid,int businessid)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select doctorid, businessid, add_time, status  ");			
			strSql.Append("  from yjr_doctor_vsbusiness ");
			strSql.Append(" where doctorid=@doctorid and businessid=@businessid ");
						SqlParameter[] parameters = {
					new SqlParameter("@doctorid", SqlDbType.Int,4),
					new SqlParameter("@businessid", SqlDbType.Int,4)			};
			parameters[0].Value = doctorid;
			parameters[1].Value = businessid;

			
			DTcms.Model.yjr_doctor_vsbusiness model=new DTcms.Model.yjr_doctor_vsbusiness();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			
			if(ds.Tables[0].Rows.Count>0)
			{
												if(ds.Tables[0].Rows[0]["doctorid"].ToString()!="")
				{
					model.doctorid=int.Parse(ds.Tables[0].Rows[0]["doctorid"].ToString());
				}
																																if(ds.Tables[0].Rows[0]["businessid"].ToString()!="")
				{
					model.businessid=int.Parse(ds.Tables[0].Rows[0]["businessid"].ToString());
				}
																																if(ds.Tables[0].Rows[0]["add_time"].ToString()!="")
				{
					model.add_time=DateTime.Parse(ds.Tables[0].Rows[0]["add_time"].ToString());
				}
																																if(ds.Tables[0].Rows[0]["status"].ToString()!="")
				{
					model.status=int.Parse(ds.Tables[0].Rows[0]["status"].ToString());
				}
																														
				return model;
			}
			else
			{
				return null;
			}
		}
		
		
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select * ");
			strSql.Append(" FROM yjr_doctor_vsbusiness ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}
		
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" * ");
			strSql.Append(" FROM yjr_doctor_vsbusiness ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得查询分页数据
		/// </summary>
		public DataSet GetList(int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
		 {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * FROM yjr_doctor_vsbusiness ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            recordCount = Convert.ToInt32(DbHelperSQL.GetSingle(PagingHelper.CreateCountingSql(strSql.ToString())));
            return DbHelperSQL.Query(PagingHelper.CreatePagingSql(recordCount, pageSize, pageIndex, strSql.ToString(), filedOrder));
		}

   
	}
}

