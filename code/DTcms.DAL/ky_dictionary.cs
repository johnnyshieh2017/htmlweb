﻿/**  版本信息模板在安装目录下，可自行修改。
* ky_dictionary.cs
*
* 功 能： N/A
* 类 名： ky_dictionary
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2021/7/15 10:35:44   N/A    初版
*

*/
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DTcms.DBUtility;//Please add references
namespace DTcms.DAL
{
	/// <summary>
	/// 数据访问类:ky_dictionary
	/// </summary>
	public partial class ky_dictionary
	{
		public ky_dictionary()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(DTcms.Model.ky_dictionary model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into ky_dictionary(");
			strSql.Append("dic_value,dic_text,dic_group,dic_sort,dic_status)");
			strSql.Append(" values (");
			strSql.Append("@dic_value,@dic_text,@dic_group,@dic_sort,@dic_status)");
			SqlParameter[] parameters = {
					new SqlParameter("@dic_value", SqlDbType.VarChar,50),
					new SqlParameter("@dic_text", SqlDbType.VarChar,50),
					new SqlParameter("@dic_group", SqlDbType.VarChar,50),
					new SqlParameter("@dic_sort", SqlDbType.Int,4),
					new SqlParameter("@dic_status", SqlDbType.Int,4)};
			parameters[0].Value = model.dic_value;
			parameters[1].Value = model.dic_text;
			parameters[2].Value = model.dic_group;
			parameters[3].Value = model.dic_sort;
			parameters[4].Value = model.dic_status;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTcms.Model.ky_dictionary model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update ky_dictionary set ");
			strSql.Append("dic_value=@dic_value,");
			strSql.Append("dic_text=@dic_text,");
			strSql.Append("dic_group=@dic_group,");
			strSql.Append("dic_sort=@dic_sort,");
			strSql.Append("dic_status=@dic_status");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
					new SqlParameter("@dic_value", SqlDbType.VarChar,50),
					new SqlParameter("@dic_text", SqlDbType.VarChar,50),
					new SqlParameter("@dic_group", SqlDbType.VarChar,50),
					new SqlParameter("@dic_sort", SqlDbType.Int,4),
					new SqlParameter("@dic_status", SqlDbType.Int,4)};
			parameters[0].Value = model.dic_value;
			parameters[1].Value = model.dic_text;
			parameters[2].Value = model.dic_group;
			parameters[3].Value = model.dic_sort;
			parameters[4].Value = model.dic_status;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete()
		{
			////该表无主键信息，请自定义主键/条件字段
			//StringBuilder strSql=new StringBuilder();
			//strSql.Append("delete from ky_dictionary ");
			//strSql.Append(" where ");
			//SqlParameter[] parameters = {
			//};

			//int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			//if (rows > 0)
			//{
			//	return true;
			//}
			//else
			//{
			//	return false;
			//}

			return false;
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.ky_dictionary GetModel()
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 dic_value,dic_text,dic_group,dic_sort,dic_status from ky_dictionary ");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
			};

			DTcms.Model.ky_dictionary model=new DTcms.Model.ky_dictionary();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.ky_dictionary DataRowToModel(DataRow row)
		{
			DTcms.Model.ky_dictionary model=new DTcms.Model.ky_dictionary();
			if (row != null)
			{
				if(row["dic_value"]!=null)
				{
					model.dic_value=row["dic_value"].ToString();
				}
				if(row["dic_text"]!=null)
				{
					model.dic_text=row["dic_text"].ToString();
				}
				if(row["dic_group"]!=null)
				{
					model.dic_group=row["dic_group"].ToString();
				}
				if(row["dic_sort"]!=null && row["dic_sort"].ToString()!="")
				{
					model.dic_sort=int.Parse(row["dic_sort"].ToString());
				}
				if(row["dic_status"]!=null && row["dic_status"].ToString()!="")
				{
					model.dic_status=int.Parse(row["dic_status"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select dic_value,dic_text,dic_group,dic_sort,dic_status ");
			strSql.Append(" FROM ky_dictionary ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where dic_status=1 "+strWhere);
			}
			strSql.Append(" order by dic_sort ");
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" dic_value,dic_text,dic_group,dic_sort,dic_status ");
			strSql.Append(" FROM ky_dictionary ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM ky_dictionary ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T. desc");
			}
			strSql.Append(")AS Row, T.*  from ky_dictionary T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "ky_dictionary";
			parameters[1].Value = "";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

