﻿using System; 
using System.Text;
using System.Data.SqlClient;
using System.Collections.Generic; 
using System.Data;
using DTcms.DBUtility;
using DTcms.Common;
namespace DTcms.DAL  
{
	 	//View_zx_order
		public partial class View_zx_order
	{
   		     
		public bool Exists()
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from View_zx_order");
			strSql.Append(" where ");
						SqlParameter[] parameters = {
			};

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}



        /// <summary>
        /// 增加一条数据
        /// </summary>
        public void Add(DTcms.Model.View_zx_order model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into View_zx_order(");
            strSql.Append("id,userid,service_type,businessconfigid,businessappointmentid,question_desc,doctor_id,createtime,service_fee,pay_status,status,pay_time,ms_sessionid,business_name,name,logoimg,titlename,avatar,nick_name,user_name,patientid,patient_name,patient_gender,patient_age,hospitalname,hoslevel,groupname,diagnosis_result,visited_department,visited_hospital,visited_status,sessionid,reportimg");
            strSql.Append(") values (");
            strSql.Append("@id,@userid,@service_type,@businessconfigid,@businessappointmentid,@question_desc,@doctor_id,@createtime,@service_fee,@pay_status,@status,@pay_time,@ms_sessionid,@business_name,@name,@logoimg,@titlename,@avatar,@nick_name,@user_name,@patientid,@patient_name,@patient_gender,@patient_age,@hospitalname,@hoslevel,@groupname,@diagnosis_result,@visited_department,@visited_hospital,@visited_status,@sessionid,@reportimg");
            strSql.Append(") ");

            SqlParameter[] parameters = {
                        new SqlParameter("@id", SqlDbType.Int,4) ,
                        new SqlParameter("@userid", SqlDbType.Int,4) ,
                        new SqlParameter("@service_type", SqlDbType.Int,4) ,
                        new SqlParameter("@businessconfigid", SqlDbType.Int,4) ,
                        new SqlParameter("@businessappointmentid", SqlDbType.Int,4) ,
                        new SqlParameter("@question_desc", SqlDbType.NVarChar,-1) ,
                        new SqlParameter("@doctor_id", SqlDbType.Int,4) ,
                        new SqlParameter("@createtime", SqlDbType.DateTime) ,
                        new SqlParameter("@service_fee", SqlDbType.Int,4) ,
                        new SqlParameter("@pay_status", SqlDbType.Int,4) ,
                        new SqlParameter("@status", SqlDbType.Int,4) ,
                        new SqlParameter("@pay_time", SqlDbType.DateTime) ,
                        new SqlParameter("@ms_sessionid", SqlDbType.VarChar,100) ,
                        new SqlParameter("@business_name", SqlDbType.NVarChar,100) ,
                        new SqlParameter("@name", SqlDbType.VarChar,50) ,
                        new SqlParameter("@logoimg", SqlDbType.VarChar,100) ,
                        new SqlParameter("@titlename", SqlDbType.VarChar,50) ,
                        new SqlParameter("@avatar", SqlDbType.NVarChar,255) ,
                        new SqlParameter("@nick_name", SqlDbType.NVarChar,100) ,
                        new SqlParameter("@user_name", SqlDbType.NVarChar,100) ,
                        new SqlParameter("@patientid", SqlDbType.Int,4) ,
                        new SqlParameter("@patient_name", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@patient_gender", SqlDbType.VarChar,50) ,
                        new SqlParameter("@patient_age", SqlDbType.Int,4) ,
                        new SqlParameter("@hospitalname", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@hoslevel", SqlDbType.VarChar,50) ,
                        new SqlParameter("@groupname", SqlDbType.VarChar,100) ,
                        new SqlParameter("@diagnosis_result", SqlDbType.VarChar,100) ,
                        new SqlParameter("@visited_department", SqlDbType.VarChar,50) ,
                        new SqlParameter("@visited_hospital", SqlDbType.VarChar,50) ,
                        new SqlParameter("@visited_status", SqlDbType.Int,4) ,
                        new SqlParameter("@sessionid", SqlDbType.VarChar,100) ,
                        new SqlParameter("@reportimg", SqlDbType.VarChar,200)

            };

            parameters[0].Value = model.id;
            parameters[1].Value = model.userid;
            parameters[2].Value = model.service_type;
            parameters[3].Value = model.businessconfigid;
            parameters[4].Value = model.businessappointmentid;
            parameters[5].Value = model.question_desc;
            parameters[6].Value = model.doctor_id;
            parameters[7].Value = model.createtime;
            parameters[8].Value = model.service_fee;
            parameters[9].Value = model.pay_status;
            parameters[10].Value = model.status;
            parameters[11].Value = model.pay_time;
            parameters[12].Value = model.ms_sessionid;
            parameters[13].Value = model.business_name;
            parameters[14].Value = model.name;
            parameters[15].Value = model.logoimg;
            parameters[16].Value = model.titlename;
            parameters[17].Value = model.avatar;
            parameters[18].Value = model.nick_name;
            parameters[19].Value = model.user_name;
            parameters[20].Value = model.patientid;
            parameters[21].Value = model.patient_name;
            parameters[22].Value = model.patient_gender;
            parameters[23].Value = model.patient_age;
            parameters[24].Value = model.hospitalname;
            parameters[25].Value = model.hoslevel;
            parameters[26].Value = model.groupname;
            parameters[27].Value = model.diagnosis_result;
            parameters[28].Value = model.visited_department;
            parameters[29].Value = model.visited_hospital;
            parameters[30].Value = model.visited_status;
            parameters[31].Value = model.sessionid;
            parameters[32].Value = model.reportimg;
            DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);

        }


        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(DTcms.Model.View_zx_order model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update View_zx_order set ");

            strSql.Append(" id = @id , ");
            strSql.Append(" userid = @userid , ");
            strSql.Append(" service_type = @service_type , ");
            strSql.Append(" businessconfigid = @businessconfigid , ");
            strSql.Append(" businessappointmentid = @businessappointmentid , ");
            strSql.Append(" question_desc = @question_desc , ");
            strSql.Append(" doctor_id = @doctor_id , ");
            strSql.Append(" createtime = @createtime , ");
            strSql.Append(" service_fee = @service_fee , ");
            strSql.Append(" pay_status = @pay_status , ");
            strSql.Append(" status = @status , ");
            strSql.Append(" pay_time = @pay_time , ");
            strSql.Append(" ms_sessionid = @ms_sessionid , ");
            strSql.Append(" business_name = @business_name , ");
            strSql.Append(" name = @name , ");
            strSql.Append(" logoimg = @logoimg , ");
            strSql.Append(" titlename = @titlename , ");
            strSql.Append(" avatar = @avatar , ");
            strSql.Append(" nick_name = @nick_name , ");
            strSql.Append(" user_name = @user_name , ");
            strSql.Append(" patientid = @patientid , ");
            strSql.Append(" patient_name = @patient_name , ");
            strSql.Append(" patient_gender = @patient_gender , ");
            strSql.Append(" patient_age = @patient_age , ");
            strSql.Append(" hospitalname = @hospitalname , ");
            strSql.Append(" hoslevel = @hoslevel , ");
            strSql.Append(" groupname = @groupname , ");
            strSql.Append(" diagnosis_result = @diagnosis_result , ");
            strSql.Append(" visited_department = @visited_department , ");
            strSql.Append(" visited_hospital = @visited_hospital , ");
            strSql.Append(" visited_status = @visited_status , ");
            strSql.Append(" sessionid = @sessionid , ");
            strSql.Append(" reportimg = @reportimg  ");
            strSql.Append(" where  ");

            SqlParameter[] parameters = {
                        new SqlParameter("@id", SqlDbType.Int,4) ,
                        new SqlParameter("@userid", SqlDbType.Int,4) ,
                        new SqlParameter("@service_type", SqlDbType.Int,4) ,
                        new SqlParameter("@businessconfigid", SqlDbType.Int,4) ,
                        new SqlParameter("@businessappointmentid", SqlDbType.Int,4) ,
                        new SqlParameter("@question_desc", SqlDbType.NVarChar,-1) ,
                        new SqlParameter("@doctor_id", SqlDbType.Int,4) ,
                        new SqlParameter("@createtime", SqlDbType.DateTime) ,
                        new SqlParameter("@service_fee", SqlDbType.Int,4) ,
                        new SqlParameter("@pay_status", SqlDbType.Int,4) ,
                        new SqlParameter("@status", SqlDbType.Int,4) ,
                        new SqlParameter("@pay_time", SqlDbType.DateTime) ,
                        new SqlParameter("@ms_sessionid", SqlDbType.VarChar,100) ,
                        new SqlParameter("@business_name", SqlDbType.NVarChar,100) ,
                        new SqlParameter("@name", SqlDbType.VarChar,50) ,
                        new SqlParameter("@logoimg", SqlDbType.VarChar,100) ,
                        new SqlParameter("@titlename", SqlDbType.VarChar,50) ,
                        new SqlParameter("@avatar", SqlDbType.NVarChar,255) ,
                        new SqlParameter("@nick_name", SqlDbType.NVarChar,100) ,
                        new SqlParameter("@user_name", SqlDbType.NVarChar,100) ,
                        new SqlParameter("@patientid", SqlDbType.Int,4) ,
                        new SqlParameter("@patient_name", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@patient_gender", SqlDbType.VarChar,50) ,
                        new SqlParameter("@patient_age", SqlDbType.Int,4) ,
                        new SqlParameter("@hospitalname", SqlDbType.NVarChar,50) ,
                        new SqlParameter("@hoslevel", SqlDbType.VarChar,50) ,
                        new SqlParameter("@groupname", SqlDbType.VarChar,100) ,
                        new SqlParameter("@diagnosis_result", SqlDbType.VarChar,100) ,
                        new SqlParameter("@visited_department", SqlDbType.VarChar,50) ,
                        new SqlParameter("@visited_hospital", SqlDbType.VarChar,50) ,
                        new SqlParameter("@visited_status", SqlDbType.Int,4) ,
                        new SqlParameter("@sessionid", SqlDbType.VarChar,100) ,
                        new SqlParameter("@reportimg", SqlDbType.VarChar,200)

            };

            parameters[0].Value = model.id;
            parameters[1].Value = model.userid;
            parameters[2].Value = model.service_type;
            parameters[3].Value = model.businessconfigid;
            parameters[4].Value = model.businessappointmentid;
            parameters[5].Value = model.question_desc;
            parameters[6].Value = model.doctor_id;
            parameters[7].Value = model.createtime;
            parameters[8].Value = model.service_fee;
            parameters[9].Value = model.pay_status;
            parameters[10].Value = model.status;
            parameters[11].Value = model.pay_time;
            parameters[12].Value = model.ms_sessionid;
            parameters[13].Value = model.business_name;
            parameters[14].Value = model.name;
            parameters[15].Value = model.logoimg;
            parameters[16].Value = model.titlename;
            parameters[17].Value = model.avatar;
            parameters[18].Value = model.nick_name;
            parameters[19].Value = model.user_name;
            parameters[20].Value = model.patientid;
            parameters[21].Value = model.patient_name;
            parameters[22].Value = model.patient_gender;
            parameters[23].Value = model.patient_age;
            parameters[24].Value = model.hospitalname;
            parameters[25].Value = model.hoslevel;
            parameters[26].Value = model.groupname;
            parameters[27].Value = model.diagnosis_result;
            parameters[28].Value = model.visited_department;
            parameters[29].Value = model.visited_hospital;
            parameters[30].Value = model.visited_status;
            parameters[31].Value = model.sessionid;
            parameters[32].Value = model.reportimg;
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete()
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from View_zx_order ");
            strSql.Append(" where ");
            SqlParameter[] parameters = {
            };


            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }



        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public DTcms.Model.View_zx_order GetModel()
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select id, userid, service_type, businessconfigid, businessappointmentid, question_desc, doctor_id, createtime, service_fee, pay_status, status, pay_time, ms_sessionid, business_name, name, logoimg, titlename, avatar, nick_name, user_name, patientid, patient_name, patient_gender, patient_age, hospitalname, hoslevel, groupname, diagnosis_result, visited_department, visited_hospital, visited_status, sessionid, reportimg  ");
            strSql.Append("  from View_zx_order ");
            strSql.Append(" where ");
            SqlParameter[] parameters = {
            };


            DTcms.Model.View_zx_order model = new DTcms.Model.View_zx_order();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);

            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["id"].ToString() != "")
                {
                    model.id = int.Parse(ds.Tables[0].Rows[0]["id"].ToString());
                }
                if (ds.Tables[0].Rows[0]["userid"].ToString() != "")
                {
                    model.userid = int.Parse(ds.Tables[0].Rows[0]["userid"].ToString());
                }
                if (ds.Tables[0].Rows[0]["service_type"].ToString() != "")
                {
                    model.service_type = int.Parse(ds.Tables[0].Rows[0]["service_type"].ToString());
                }
                if (ds.Tables[0].Rows[0]["businessconfigid"].ToString() != "")
                {
                    model.businessconfigid = int.Parse(ds.Tables[0].Rows[0]["businessconfigid"].ToString());
                }
                if (ds.Tables[0].Rows[0]["businessappointmentid"].ToString() != "")
                {
                    model.businessappointmentid = int.Parse(ds.Tables[0].Rows[0]["businessappointmentid"].ToString());
                }
                model.question_desc = ds.Tables[0].Rows[0]["question_desc"].ToString();
                if (ds.Tables[0].Rows[0]["doctor_id"].ToString() != "")
                {
                    model.doctor_id = int.Parse(ds.Tables[0].Rows[0]["doctor_id"].ToString());
                }
                if (ds.Tables[0].Rows[0]["createtime"].ToString() != "")
                {
                    model.createtime = DateTime.Parse(ds.Tables[0].Rows[0]["createtime"].ToString());
                }
                if (ds.Tables[0].Rows[0]["service_fee"].ToString() != "")
                {
                    model.service_fee = int.Parse(ds.Tables[0].Rows[0]["service_fee"].ToString());
                }
                if (ds.Tables[0].Rows[0]["pay_status"].ToString() != "")
                {
                    model.pay_status = int.Parse(ds.Tables[0].Rows[0]["pay_status"].ToString());
                }
                if (ds.Tables[0].Rows[0]["status"].ToString() != "")
                {
                    model.status = int.Parse(ds.Tables[0].Rows[0]["status"].ToString());
                }
                if (ds.Tables[0].Rows[0]["pay_time"].ToString() != "")
                {
                    model.pay_time = DateTime.Parse(ds.Tables[0].Rows[0]["pay_time"].ToString());
                }
                model.ms_sessionid = ds.Tables[0].Rows[0]["ms_sessionid"].ToString();
                model.business_name = ds.Tables[0].Rows[0]["business_name"].ToString();
                model.name = ds.Tables[0].Rows[0]["name"].ToString();
                model.logoimg = ds.Tables[0].Rows[0]["logoimg"].ToString();
                model.titlename = ds.Tables[0].Rows[0]["titlename"].ToString();
                model.avatar = ds.Tables[0].Rows[0]["avatar"].ToString();
                model.nick_name = ds.Tables[0].Rows[0]["nick_name"].ToString();
                model.user_name = ds.Tables[0].Rows[0]["user_name"].ToString();
                if (ds.Tables[0].Rows[0]["patientid"].ToString() != "")
                {
                    model.patientid = int.Parse(ds.Tables[0].Rows[0]["patientid"].ToString());
                }
                model.patient_name = ds.Tables[0].Rows[0]["patient_name"].ToString();
                model.patient_gender = ds.Tables[0].Rows[0]["patient_gender"].ToString();
                if (ds.Tables[0].Rows[0]["patient_age"].ToString() != "")
                {
                    model.patient_age = int.Parse(ds.Tables[0].Rows[0]["patient_age"].ToString());
                }
                model.hospitalname = ds.Tables[0].Rows[0]["hospitalname"].ToString();
                model.hoslevel = ds.Tables[0].Rows[0]["hoslevel"].ToString();
                model.groupname = ds.Tables[0].Rows[0]["groupname"].ToString();
                model.diagnosis_result = ds.Tables[0].Rows[0]["diagnosis_result"].ToString();
                model.visited_department = ds.Tables[0].Rows[0]["visited_department"].ToString();
                model.visited_hospital = ds.Tables[0].Rows[0]["visited_hospital"].ToString();
                if (ds.Tables[0].Rows[0]["visited_status"].ToString() != "")
                {
                    model.visited_status = int.Parse(ds.Tables[0].Rows[0]["visited_status"].ToString());
                }

                model.sessionid = ds.Tables[0].Rows[0]["sessionid"].ToString();
                model.reportimg = ds.Tables[0].Rows[0]["reportimg"].ToString();

                return model;
            }
            else
            {
                return null;
            }
        }
		
		
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select * ");
			strSql.Append(" FROM View_zx_order ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}
		
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" * ");
			strSql.Append(" FROM View_zx_order ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得查询分页数据
		/// </summary>
		public DataSet GetList(int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
		 {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * FROM View_zx_order ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            recordCount = Convert.ToInt32(DbHelperSQL.GetSingle(PagingHelper.CreateCountingSql(strSql.ToString())));
            return DbHelperSQL.Query(PagingHelper.CreatePagingSql(recordCount, pageSize, pageIndex, strSql.ToString(), filedOrder));
		}

        public int Getrecordcount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM View_zx_order ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return Convert.ToInt32(DbHelperSQL.GetSingle(strSql.ToString()));
        }


    }
}

