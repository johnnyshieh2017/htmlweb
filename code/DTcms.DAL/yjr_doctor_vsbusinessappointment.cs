﻿using System; 
using System.Text;
using System.Data.SqlClient;
using System.Collections.Generic; 
using System.Data;
using DTcms.DBUtility;
using DTcms.Common;
namespace DTcms.DAL  
{
	 	//yjr_doctor_vsbusinessappointment
		public partial class yjr_doctor_vsbusinessappointment
	{
   		     
		public bool Exists(int id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from yjr_doctor_vsbusinessappointment");
			strSql.Append(" where ");
			                                       strSql.Append(" id = @id  ");
                            			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)			};
			parameters[0].Value = id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}
		
				
		
		/// <summary>
		/// 增加一条数据
		/// </summary>
		public void Add(DTcms.Model.yjr_doctor_vsbusinessappointment model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into yjr_doctor_vsbusinessappointment(");			
            strSql.Append("id,doctorid,busines_sconfig_id,clinic_date,clinic_time,add_time");
			strSql.Append(") values (");
            strSql.Append("@id,@doctorid,@busines_sconfig_id,@clinic_date,@clinic_time,@add_time");            
            strSql.Append(") ");            
            		
			SqlParameter[] parameters = {
			            new SqlParameter("@id", SqlDbType.Int,4) ,            
                        new SqlParameter("@doctorid", SqlDbType.Int,4) ,            
                        new SqlParameter("@busines_sconfig_id", SqlDbType.Int,4) ,            
                        new SqlParameter("@clinic_date", SqlDbType.VarChar,50) ,            
                        new SqlParameter("@clinic_time", SqlDbType.VarChar,50) ,            
                        new SqlParameter("@add_time", SqlDbType.DateTime)             
              
            };
			            
            parameters[0].Value = model.id;                        
            parameters[1].Value = model.doctorid;                        
            parameters[2].Value = model.busines_sconfig_id;                        
            parameters[3].Value = model.clinic_date;                        
            parameters[4].Value = model.clinic_time;                        
            parameters[5].Value = model.add_time;                        
			            DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
            			
		}
		
		
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTcms.Model.yjr_doctor_vsbusinessappointment model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update yjr_doctor_vsbusinessappointment set ");
			                        
            strSql.Append(" id = @id , ");                                    
            strSql.Append(" doctorid = @doctorid , ");                                    
            strSql.Append(" busines_sconfig_id = @busines_sconfig_id , ");                                    
            strSql.Append(" clinic_date = @clinic_date , ");                                    
            strSql.Append(" clinic_time = @clinic_time , ");                                    
            strSql.Append(" add_time = @add_time  ");            			
			strSql.Append(" where id=@id  ");
						
SqlParameter[] parameters = {
			            new SqlParameter("@id", SqlDbType.Int,4) ,            
                        new SqlParameter("@doctorid", SqlDbType.Int,4) ,            
                        new SqlParameter("@busines_sconfig_id", SqlDbType.Int,4) ,            
                        new SqlParameter("@clinic_date", SqlDbType.VarChar,50) ,            
                        new SqlParameter("@clinic_time", SqlDbType.VarChar,50) ,            
                        new SqlParameter("@add_time", SqlDbType.DateTime)             
              
            };
						            
            parameters[0].Value = model.id;                        
            parameters[1].Value = model.doctorid;                        
            parameters[2].Value = model.busines_sconfig_id;                        
            parameters[3].Value = model.clinic_date;                        
            parameters[4].Value = model.clinic_time;                        
            parameters[5].Value = model.add_time;                        
            int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from yjr_doctor_vsbusinessappointment ");
			strSql.Append(" where id=@id ");
						SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)			};
			parameters[0].Value = id;


			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
				
		
		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.yjr_doctor_vsbusinessappointment GetModel(int id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select id, doctorid, busines_sconfig_id, clinic_date, clinic_time, add_time  ");			
			strSql.Append("  from yjr_doctor_vsbusinessappointment ");
			strSql.Append(" where id=@id ");
						SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)			};
			parameters[0].Value = id;

			
			DTcms.Model.yjr_doctor_vsbusinessappointment model=new DTcms.Model.yjr_doctor_vsbusinessappointment();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			
			if(ds.Tables[0].Rows.Count>0)
			{
												if(ds.Tables[0].Rows[0]["id"].ToString()!="")
				{
					model.id=int.Parse(ds.Tables[0].Rows[0]["id"].ToString());
				}
																																if(ds.Tables[0].Rows[0]["doctorid"].ToString()!="")
				{
					model.doctorid=int.Parse(ds.Tables[0].Rows[0]["doctorid"].ToString());
				}
																																if(ds.Tables[0].Rows[0]["busines_sconfig_id"].ToString()!="")
				{
					model.busines_sconfig_id=int.Parse(ds.Tables[0].Rows[0]["busines_sconfig_id"].ToString());
				}
																																				model.clinic_date= ds.Tables[0].Rows[0]["clinic_date"].ToString();
																																model.clinic_time= ds.Tables[0].Rows[0]["clinic_time"].ToString();
																												if(ds.Tables[0].Rows[0]["add_time"].ToString()!="")
				{
					model.add_time=DateTime.Parse(ds.Tables[0].Rows[0]["add_time"].ToString());
				}
																														
				return model;
			}
			else
			{
				return null;
			}
		}
		
		
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select * ");
			strSql.Append(" FROM yjr_doctor_vsbusinessappointment ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}
		
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" * ");
			strSql.Append(" FROM yjr_doctor_vsbusinessappointment ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得查询分页数据
		/// </summary>
		public DataSet GetList(int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
		 {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * FROM yjr_doctor_vsbusinessappointment ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            recordCount = Convert.ToInt32(DbHelperSQL.GetSingle(PagingHelper.CreateCountingSql(strSql.ToString())));
            return DbHelperSQL.Query(PagingHelper.CreatePagingSql(recordCount, pageSize, pageIndex, strSql.ToString(), filedOrder));
		}

   
	}
}

