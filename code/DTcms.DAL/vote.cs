﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DTcms.DBUtility;
using DTcms.Common;//Please add references
namespace DTcms.DAL
{
    /// <summary>
    /// 数据访问类:vote
    /// </summary>
    public partial class vote
    {
        public vote()
        { }
        #region  BasicMethod

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("id", "dt_vote");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int id)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from dt_vote");
            strSql.Append(" where id=@id");
            SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
            parameters[0].Value = id;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(DTcms.Model.vote model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into dt_vote(");
            strSql.Append("title,content,type,start_time,end_time,sort_id,user_id,is_user,img_url,click,add_time,status)");
            strSql.Append(" values (");
            strSql.Append("@title,@content,@type,@start_time,@end_time,@sort_id,@user_id,@is_user,@img_url,@click,@add_time,@status)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
                    new SqlParameter("@title", SqlDbType.NVarChar,100),
                    new SqlParameter("@content", SqlDbType.Text),
                    new SqlParameter("@type", SqlDbType.Int,4),
                    new SqlParameter("@start_time", SqlDbType.DateTime),
                    new SqlParameter("@end_time", SqlDbType.DateTime),
                    new SqlParameter("@sort_id", SqlDbType.Int,4),
                    new SqlParameter("@user_id", SqlDbType.Int,4),
                    new SqlParameter("@is_user", SqlDbType.Int,4),
                    new SqlParameter("@img_url", SqlDbType.NVarChar,250),
                    new SqlParameter("@click", SqlDbType.Int,4),
                    new SqlParameter("@add_time", SqlDbType.DateTime),
                    new SqlParameter("@status", SqlDbType.Int,4)};
            parameters[0].Value = model.title;
            parameters[1].Value = model.content;
            parameters[2].Value = model.type;
            parameters[3].Value = model.start_time;
            parameters[4].Value = model.end_time;
            parameters[5].Value = model.sort_id;
            parameters[6].Value = model.user_id;
            parameters[7].Value = model.is_user;
            parameters[8].Value = model.img_url;
            parameters[9].Value = model.click;
            parameters[10].Value = model.add_time;
            parameters[11].Value = model.status;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(DTcms.Model.vote model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update dt_vote set ");
            strSql.Append("title=@title,");
            strSql.Append("content=@content,");
            strSql.Append("type=@type,");
            strSql.Append("start_time=@start_time,");
            strSql.Append("end_time=@end_time,");
            strSql.Append("sort_id=@sort_id,");
            strSql.Append("user_id=@user_id,");
            strSql.Append("is_user=@is_user,");
            strSql.Append("img_url=@img_url,");
            strSql.Append("click=@click,");
            strSql.Append("add_time=@add_time,");
            strSql.Append("status=@status");
            strSql.Append(" where id=@id");
            SqlParameter[] parameters = {
                    new SqlParameter("@title", SqlDbType.NVarChar,100),
                    new SqlParameter("@content", SqlDbType.Text),
                    new SqlParameter("@type", SqlDbType.Int,4),
                    new SqlParameter("@start_time", SqlDbType.DateTime),
                    new SqlParameter("@end_time", SqlDbType.DateTime),
                    new SqlParameter("@sort_id", SqlDbType.Int,4),
                    new SqlParameter("@user_id", SqlDbType.Int,4),
                    new SqlParameter("@is_user", SqlDbType.Int,4),
                    new SqlParameter("@img_url", SqlDbType.NVarChar,250),
                    new SqlParameter("@click", SqlDbType.Int,4),
                    new SqlParameter("@add_time", SqlDbType.DateTime),
                    new SqlParameter("@status", SqlDbType.Int,4),
                    new SqlParameter("@id", SqlDbType.Int,4)};
            parameters[0].Value = model.title;
            parameters[1].Value = model.content;
            parameters[2].Value = model.type;
            parameters[3].Value = model.start_time;
            parameters[4].Value = model.end_time;
            parameters[5].Value = model.sort_id;
            parameters[6].Value = model.user_id;
            parameters[7].Value = model.is_user;
            parameters[8].Value = model.img_url;
            parameters[9].Value = model.click;
            parameters[10].Value = model.add_time;
            parameters[11].Value = model.status;
            parameters[12].Value = model.id;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 使用这个方法删更干净，删除一条数据
        /// </summary>
        public bool Delete(int id)
        {

            //清除下级所有标题
            DataTable dt = new DAL.vote_subject().GetList(" vote_id=" + id + " ").Tables[0];

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                new DAL.vote_subject().Delete(Convert.ToInt32(dt.Rows[i]["id"])); 
                                                 
            }

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from dt_vote ");
            strSql.Append(" where id=@id");
            SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
            parameters[0].Value = id;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string idlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from dt_vote ");
            strSql.Append(" where id in (" + idlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public DTcms.Model.vote GetModel(int id)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 id,title,content,type,start_time,end_time,sort_id,user_id,is_user,img_url,click,add_time,status from dt_vote ");
            strSql.Append(" where id=@id");
            SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
            parameters[0].Value = id;

            DTcms.Model.vote model = new DTcms.Model.vote();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public DTcms.Model.vote GetModel(string strWhere)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 id,title,content,type,start_time,end_time,sort_id,user_id,is_user,img_url,click,add_time,status from dt_vote ");
            strSql.Append(" where " + strWhere);            

            DTcms.Model.vote model = new DTcms.Model.vote();
            DataSet ds = DbHelperSQL.Query(strSql.ToString());
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public DTcms.Model.vote DataRowToModel(DataRow row)
        {
            DTcms.Model.vote model = new DTcms.Model.vote();
            if (row != null)
            {
                if (row["id"] != null && row["id"].ToString() != "")
                {
                    model.id = int.Parse(row["id"].ToString());
                }
                if (row["title"] != null)
                {
                    model.title = row["title"].ToString();
                }
                if (row["content"] != null)
                {
                    model.content = row["content"].ToString();
                }
                if (row["type"] != null && row["type"].ToString() != "")
                {
                    model.type = int.Parse(row["type"].ToString());
                }
                if (row["start_time"] != null && row["start_time"].ToString() != "")
                {
                    model.start_time = DateTime.Parse(row["start_time"].ToString());
                }
                if (row["end_time"] != null && row["end_time"].ToString() != "")
                {
                    model.end_time = DateTime.Parse(row["end_time"].ToString());
                }
                if (row["sort_id"] != null && row["sort_id"].ToString() != "")
                {
                    model.sort_id = int.Parse(row["sort_id"].ToString());
                }
                if (row["user_id"] != null && row["user_id"].ToString() != "")
                {
                    model.user_id = int.Parse(row["user_id"].ToString());
                }
                if (row["is_user"] != null && row["is_user"].ToString() != "")
                {
                    model.is_user = int.Parse(row["is_user"].ToString());
                }
                if (row["img_url"] != null)
                {
                    model.img_url = row["img_url"].ToString();
                }
                if (row["click"] != null && row["click"].ToString() != "")
                {
                    model.click = int.Parse(row["click"].ToString());
                }
                if (row["add_time"] != null && row["add_time"].ToString() != "")
                {
                    model.add_time = DateTime.Parse(row["add_time"].ToString());
                }
                if (row["status"] != null && row["status"].ToString() != "")
                {
                    model.status = int.Parse(row["status"].ToString());
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select id,title,content,type,start_time,end_time,sort_id,user_id,is_user,img_url,click,add_time,status ");
            strSql.Append(" FROM dt_vote ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" id,title,content,type,start_time,end_time,sort_id,user_id,is_user,img_url,click,add_time,status ");
            strSql.Append(" FROM dt_vote ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM dt_vote ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.id desc");
            }
            strSql.Append(")AS Row, T.*  from dt_vote T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得查询分页数据
        /// </summary>
        public DataSet GetList(int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * FROM dt_vote");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            recordCount = Convert.ToInt32(DbHelperSQL.GetSingle(PagingHelper.CreateCountingSql(strSql.ToString())));
            return DbHelperSQL.Query(PagingHelper.CreatePagingSql(recordCount, pageSize, pageIndex, strSql.ToString(), filedOrder));
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "dt_vote";
            parameters[1].Value = "id";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

