﻿using System;
using System.Text;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Data;
using DTcms.DBUtility;
using DTcms.Common;
namespace DTcms.DAL
{
    //yjr_zx_messages
    public partial class yjr_zx_messages
    {

        public bool Exists(int id)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from yjr_zx_messages");
            strSql.Append(" where ");
            strSql.Append(" id = @id  ");
            SqlParameter[] parameters = {
                    new SqlParameter("@id", SqlDbType.Int,4)
            };
            parameters[0].Value = id;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }



        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(DTcms.Model.yjr_zx_messages model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into yjr_zx_messages(");
            strSql.Append("userid,sessionid,doctorid,zx_number,zx_content,createtime,status,visited_status,visited_hospital,visited_department,diagnosis_result,reportimg,patientid,owner,read_status");
            strSql.Append(") values (");
            strSql.Append("@userid,@sessionid,@doctorid,@zx_number,@zx_content,@createtime,@status,@visited_status,@visited_hospital,@visited_department,@diagnosis_result,@reportimg,@patientid,@owner,@read_status");
            strSql.Append(") ");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
                        new SqlParameter("@userid", SqlDbType.Int,4) ,
                        new SqlParameter("@sessionid", SqlDbType.VarChar,100) ,
                        new SqlParameter("@doctorid", SqlDbType.Int,4) ,
                        new SqlParameter("@zx_number", SqlDbType.Int,4) ,
                        new SqlParameter("@zx_content", SqlDbType.NVarChar,-1) ,
                        new SqlParameter("@createtime", SqlDbType.DateTime) ,
                        new SqlParameter("@status", SqlDbType.Int,4) ,
                        new SqlParameter("@visited_status", SqlDbType.Int,4) ,
                        new SqlParameter("@visited_hospital", SqlDbType.VarChar,50) ,
                        new SqlParameter("@visited_department", SqlDbType.VarChar,50) ,
                        new SqlParameter("@diagnosis_result", SqlDbType.VarChar,100) ,
                        new SqlParameter("@reportimg", SqlDbType.VarChar,200) ,
                        new SqlParameter("@patientid", SqlDbType.Int,4) ,
                        new SqlParameter("@owner", SqlDbType.Int,4) ,
                        new SqlParameter("@read_status", SqlDbType.Int,4)

            };

            parameters[0].Value = model.userid;
            parameters[1].Value = model.sessionid;
            parameters[2].Value = model.doctorid;
            parameters[3].Value = model.zx_number;
            parameters[4].Value = model.zx_content;
            parameters[5].Value = model.createtime;
            parameters[6].Value = model.status;
            parameters[7].Value = model.visited_status;
            parameters[8].Value = model.visited_hospital;
            parameters[9].Value = model.visited_department;
            parameters[10].Value = model.diagnosis_result;
            parameters[11].Value = model.reportimg;
            parameters[12].Value = model.patientid;
            parameters[13].Value = model.owner;
            parameters[14].Value = model.read_status;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {

                return Convert.ToInt32(obj);

            }

        }


        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(DTcms.Model.yjr_zx_messages model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update yjr_zx_messages set ");

            strSql.Append(" userid = @userid , ");
            strSql.Append(" sessionid = @sessionid , ");
            strSql.Append(" doctorid = @doctorid , ");
            strSql.Append(" zx_number = @zx_number , ");
            strSql.Append(" zx_content = @zx_content , ");
            strSql.Append(" createtime = @createtime , ");
            strSql.Append(" status = @status , ");
            strSql.Append(" visited_status = @visited_status , ");
            strSql.Append(" visited_hospital = @visited_hospital , ");
            strSql.Append(" visited_department = @visited_department , ");
            strSql.Append(" diagnosis_result = @diagnosis_result , ");
            strSql.Append(" reportimg = @reportimg , ");
            strSql.Append(" patientid = @patientid , ");
            strSql.Append(" owner = @owner , ");
            strSql.Append(" read_status = @read_status  ");
            strSql.Append(" where id=@id ");

            SqlParameter[] parameters = {
                        new SqlParameter("@id", SqlDbType.Int,4) ,
                        new SqlParameter("@userid", SqlDbType.Int,4) ,
                        new SqlParameter("@sessionid", SqlDbType.VarChar,100) ,
                        new SqlParameter("@doctorid", SqlDbType.Int,4) ,
                        new SqlParameter("@zx_number", SqlDbType.Int,4) ,
                        new SqlParameter("@zx_content", SqlDbType.NVarChar,-1) ,
                        new SqlParameter("@createtime", SqlDbType.DateTime) ,
                        new SqlParameter("@status", SqlDbType.Int,4) ,
                        new SqlParameter("@visited_status", SqlDbType.Int,4) ,
                        new SqlParameter("@visited_hospital", SqlDbType.VarChar,50) ,
                        new SqlParameter("@visited_department", SqlDbType.VarChar,50) ,
                        new SqlParameter("@diagnosis_result", SqlDbType.VarChar,100) ,
                        new SqlParameter("@reportimg", SqlDbType.VarChar,200) ,
                        new SqlParameter("@patientid", SqlDbType.Int,4) ,
                        new SqlParameter("@owner", SqlDbType.Int,4) ,
                        new SqlParameter("@read_status", SqlDbType.Int,4)

            };

            parameters[0].Value = model.id;
            parameters[1].Value = model.userid;
            parameters[2].Value = model.sessionid;
            parameters[3].Value = model.doctorid;
            parameters[4].Value = model.zx_number;
            parameters[5].Value = model.zx_content;
            parameters[6].Value = model.createtime;
            parameters[7].Value = model.status;
            parameters[8].Value = model.visited_status;
            parameters[9].Value = model.visited_hospital;
            parameters[10].Value = model.visited_department;
            parameters[11].Value = model.diagnosis_result;
            parameters[12].Value = model.reportimg;
            parameters[13].Value = model.patientid;
            parameters[14].Value = model.owner;
            parameters[15].Value = model.read_status;
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int id)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from yjr_zx_messages ");
            strSql.Append(" where id=@id");
            SqlParameter[] parameters = {
                    new SqlParameter("@id", SqlDbType.Int,4)
            };
            parameters[0].Value = id;


            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 批量删除一批数据
        /// </summary>
        public bool DeleteList(string idlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from yjr_zx_messages ");
            strSql.Append(" where ID in (" + idlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public DTcms.Model.yjr_zx_messages GetModel(int id)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select id, userid, sessionid, doctorid, zx_number, zx_content, createtime, status, visited_status, visited_hospital, visited_department, diagnosis_result, reportimg, patientid, owner, read_status  ");
            strSql.Append("  from yjr_zx_messages ");
            strSql.Append(" where id=@id");
            SqlParameter[] parameters = {
                    new SqlParameter("@id", SqlDbType.Int,4)
            };
            parameters[0].Value = id;


            DTcms.Model.yjr_zx_messages model = new DTcms.Model.yjr_zx_messages();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);

            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["id"].ToString() != "")
                {
                    model.id = int.Parse(ds.Tables[0].Rows[0]["id"].ToString());
                }
                if (ds.Tables[0].Rows[0]["userid"].ToString() != "")
                {
                    model.userid = int.Parse(ds.Tables[0].Rows[0]["userid"].ToString());
                }
                model.sessionid = ds.Tables[0].Rows[0]["sessionid"].ToString();
                if (ds.Tables[0].Rows[0]["doctorid"].ToString() != "")
                {
                    model.doctorid = int.Parse(ds.Tables[0].Rows[0]["doctorid"].ToString());
                }
                if (ds.Tables[0].Rows[0]["zx_number"].ToString() != "")
                {
                    model.zx_number = int.Parse(ds.Tables[0].Rows[0]["zx_number"].ToString());
                }
                model.zx_content = ds.Tables[0].Rows[0]["zx_content"].ToString();
                if (ds.Tables[0].Rows[0]["createtime"].ToString() != "")
                {
                    model.createtime = DateTime.Parse(ds.Tables[0].Rows[0]["createtime"].ToString());
                }
                if (ds.Tables[0].Rows[0]["status"].ToString() != "")
                {
                    model.status = int.Parse(ds.Tables[0].Rows[0]["status"].ToString());
                }
                if (ds.Tables[0].Rows[0]["visited_status"].ToString() != "")
                {
                    model.visited_status = int.Parse(ds.Tables[0].Rows[0]["visited_status"].ToString());
                }
                model.visited_hospital = ds.Tables[0].Rows[0]["visited_hospital"].ToString();
                model.visited_department = ds.Tables[0].Rows[0]["visited_department"].ToString();
                model.diagnosis_result = ds.Tables[0].Rows[0]["diagnosis_result"].ToString();
                model.reportimg = ds.Tables[0].Rows[0]["reportimg"].ToString();
                if (ds.Tables[0].Rows[0]["patientid"].ToString() != "")
                {
                    model.patientid = int.Parse(ds.Tables[0].Rows[0]["patientid"].ToString());
                }
                if (ds.Tables[0].Rows[0]["owner"].ToString() != "")
                {
                    model.owner = int.Parse(ds.Tables[0].Rows[0]["owner"].ToString());
                }
                if (ds.Tables[0].Rows[0]["read_status"].ToString() != "")
                {
                    model.read_status = int.Parse(ds.Tables[0].Rows[0]["read_status"].ToString());
                }

                return model;
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * ");
            strSql.Append(" FROM yjr_zx_messages ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" * ");
            strSql.Append(" FROM yjr_zx_messages ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得查询分页数据
        /// </summary>
        public DataSet GetList(int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * FROM yjr_zx_messages ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            recordCount = Convert.ToInt32(DbHelperSQL.GetSingle(PagingHelper.CreateCountingSql(strSql.ToString())));
            return DbHelperSQL.Query(PagingHelper.CreatePagingSql(recordCount, pageSize, pageIndex, strSql.ToString(), filedOrder));
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM yjr_zx_messages ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }


      





    }
}

