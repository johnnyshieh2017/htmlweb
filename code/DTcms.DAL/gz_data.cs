﻿/**  版本信息模板在安装目录下，可自行修改。
* gz_data.cs
*
* 功 能： N/A
* 类 名： gz_data
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2020/3/14 10:52:32   N/A    初版
*
* Copyright (c) 2019  Corporation. All rights reserved.
*/
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DTcms.DBUtility;
namespace DTcms.DAL
{
	/// <summary>
	/// 数据访问类:gz_data
	/// </summary>
	public partial class gz_data
	{
		public gz_data()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(DTcms.Model.gz_data model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into gz_data(");
			strSql.Append("观众ID,观众名,第三方ID,地址,观看总时长,手机号,访问来源,访问次数,分享次数,聊天次数,在线时长,首次进入时间,最后离开时间,最后观看设备,最后观看方式,最后登录ip,是否职工,省,市,公司)");
			strSql.Append(" values (");
			strSql.Append("@观众ID,@观众名,@第三方ID,@地址,@观看总时长,@手机号,@访问来源,@访问次数,@分享次数,@聊天次数,@在线时长,@首次进入时间,@最后离开时间,@最后观看设备,@最后观看方式,@最后登录ip,@是否职工,@省,@市,@公司)");
			SqlParameter[] parameters = {
					new SqlParameter("@观众ID", SqlDbType.NVarChar,255),
					new SqlParameter("@观众名", SqlDbType.NVarChar,255),
					new SqlParameter("@第三方ID", SqlDbType.NVarChar,255),
					new SqlParameter("@地址", SqlDbType.NVarChar,255),
					new SqlParameter("@观看总时长", SqlDbType.DateTime),
					new SqlParameter("@手机号", SqlDbType.Float,8),
					new SqlParameter("@访问来源", SqlDbType.NVarChar,255),
					new SqlParameter("@访问次数", SqlDbType.Float,8),
					new SqlParameter("@分享次数", SqlDbType.Float,8),
					new SqlParameter("@聊天次数", SqlDbType.Float,8),
					new SqlParameter("@在线时长", SqlDbType.DateTime),
					new SqlParameter("@首次进入时间", SqlDbType.DateTime),
					new SqlParameter("@最后离开时间", SqlDbType.DateTime),
					new SqlParameter("@最后观看设备", SqlDbType.NVarChar,255),
					new SqlParameter("@最后观看方式", SqlDbType.NVarChar,255),
					new SqlParameter("@最后登录ip", SqlDbType.NVarChar,255),
					new SqlParameter("@是否职工", SqlDbType.NVarChar,50),
					new SqlParameter("@省", SqlDbType.NVarChar,50),
					new SqlParameter("@市", SqlDbType.NVarChar,50),
					new SqlParameter("@公司", SqlDbType.NVarChar,250)};
			parameters[0].Value = model.观众ID;
			parameters[1].Value = model.观众名;
			parameters[2].Value = model.第三方ID;
			parameters[3].Value = model.地址;
			parameters[4].Value = model.观看总时长;
			parameters[5].Value = model.手机号;
			parameters[6].Value = model.访问来源;
			parameters[7].Value = model.访问次数;
			parameters[8].Value = model.分享次数;
			parameters[9].Value = model.聊天次数;
			parameters[10].Value = model.在线时长;
			parameters[11].Value = model.首次进入时间;
			parameters[12].Value = model.最后离开时间;
			parameters[13].Value = model.最后观看设备;
			parameters[14].Value = model.最后观看方式;
			parameters[15].Value = model.最后登录ip;
			parameters[16].Value = model.是否职工;
			parameters[17].Value = model.省;
			parameters[18].Value = model.市;
			parameters[19].Value = model.公司;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTcms.Model.gz_data model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update gz_data set ");
			strSql.Append("观众ID=@观众ID,");
			strSql.Append("观众名=@观众名,");
			strSql.Append("第三方ID=@第三方ID,");
			strSql.Append("地址=@地址,");
			strSql.Append("观看总时长=@观看总时长,");
			strSql.Append("手机号=@手机号,");
			strSql.Append("访问来源=@访问来源,");
			strSql.Append("访问次数=@访问次数,");
			strSql.Append("分享次数=@分享次数,");
			strSql.Append("聊天次数=@聊天次数,");
			strSql.Append("在线时长=@在线时长,");
			strSql.Append("首次进入时间=@首次进入时间,");
			strSql.Append("最后离开时间=@最后离开时间,");
			strSql.Append("最后观看设备=@最后观看设备,");
			strSql.Append("最后观看方式=@最后观看方式,");
			strSql.Append("最后登录ip=@最后登录ip,");
			strSql.Append("是否职工=@是否职工,");
			strSql.Append("省=@省,");
			strSql.Append("市=@市,");
			strSql.Append("公司=@公司");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
					new SqlParameter("@观众ID", SqlDbType.NVarChar,255),
					new SqlParameter("@观众名", SqlDbType.NVarChar,255),
					new SqlParameter("@第三方ID", SqlDbType.NVarChar,255),
					new SqlParameter("@地址", SqlDbType.NVarChar,255),
					new SqlParameter("@观看总时长", SqlDbType.DateTime),
					new SqlParameter("@手机号", SqlDbType.Float,8),
					new SqlParameter("@访问来源", SqlDbType.NVarChar,255),
					new SqlParameter("@访问次数", SqlDbType.Float,8),
					new SqlParameter("@分享次数", SqlDbType.Float,8),
					new SqlParameter("@聊天次数", SqlDbType.Float,8),
					new SqlParameter("@在线时长", SqlDbType.DateTime),
					new SqlParameter("@首次进入时间", SqlDbType.DateTime),
					new SqlParameter("@最后离开时间", SqlDbType.DateTime),
					new SqlParameter("@最后观看设备", SqlDbType.NVarChar,255),
					new SqlParameter("@最后观看方式", SqlDbType.NVarChar,255),
					new SqlParameter("@最后登录ip", SqlDbType.NVarChar,255),
					new SqlParameter("@是否职工", SqlDbType.NVarChar,50),
					new SqlParameter("@省", SqlDbType.NVarChar,50),
					new SqlParameter("@市", SqlDbType.NVarChar,50),
					new SqlParameter("@公司", SqlDbType.NVarChar,250)};
			parameters[0].Value = model.观众ID;
			parameters[1].Value = model.观众名;
			parameters[2].Value = model.第三方ID;
			parameters[3].Value = model.地址;
			parameters[4].Value = model.观看总时长;
			parameters[5].Value = model.手机号;
			parameters[6].Value = model.访问来源;
			parameters[7].Value = model.访问次数;
			parameters[8].Value = model.分享次数;
			parameters[9].Value = model.聊天次数;
			parameters[10].Value = model.在线时长;
			parameters[11].Value = model.首次进入时间;
			parameters[12].Value = model.最后离开时间;
			parameters[13].Value = model.最后观看设备;
			parameters[14].Value = model.最后观看方式;
			parameters[15].Value = model.最后登录ip;
			parameters[16].Value = model.是否职工;
			parameters[17].Value = model.省;
			parameters[18].Value = model.市;
			parameters[19].Value = model.公司;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

        public bool Updatewhere(string sheng,string shi,string gongsi,string xm,string where)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update gz_data set ");

            strSql.Append("是否职工='是',");
            strSql.Append("省='"+sheng+"',");
            strSql.Append("市='"+shi+"',");
            strSql.Append("公司='"+gongsi+"',");
            strSql.Append("姓名='" + xm + "'");
            strSql.Append(" where ");
            strSql.Append(where);

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete()
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from gz_data ");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
			};

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.gz_data GetModel()
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 观众ID,观众名,第三方ID,地址,观看总时长,手机号,访问来源,访问次数,分享次数,聊天次数,在线时长,首次进入时间,最后离开时间,最后观看设备,最后观看方式,最后登录ip,是否职工,省,市,公司 from gz_data ");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
			};

			DTcms.Model.gz_data model=new DTcms.Model.gz_data();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.gz_data DataRowToModel(DataRow row)
		{
			DTcms.Model.gz_data model=new DTcms.Model.gz_data();
			if (row != null)
			{
				if(row["观众ID"]!=null)
				{
					model.观众ID=row["观众ID"].ToString();
				}
				if(row["观众名"]!=null)
				{
					model.观众名=row["观众名"].ToString();
				}
				if(row["第三方ID"]!=null)
				{
					model.第三方ID=row["第三方ID"].ToString();
				}
				if(row["地址"]!=null)
				{
					model.地址=row["地址"].ToString();
				}
				if(row["观看总时长"]!=null && row["观看总时长"].ToString()!="")
				{
					model.观看总时长=DateTime.Parse(row["观看总时长"].ToString());
				}
				if(row["手机号"]!=null && row["手机号"].ToString()!="")
				{
					model.手机号=decimal.Parse(row["手机号"].ToString());
				}
				if(row["访问来源"]!=null)
				{
					model.访问来源=row["访问来源"].ToString();
				}
				if(row["访问次数"]!=null && row["访问次数"].ToString()!="")
				{
					model.访问次数=decimal.Parse(row["访问次数"].ToString());
				}
				if(row["分享次数"]!=null && row["分享次数"].ToString()!="")
				{
					model.分享次数=decimal.Parse(row["分享次数"].ToString());
				}
				if(row["聊天次数"]!=null && row["聊天次数"].ToString()!="")
				{
					model.聊天次数=decimal.Parse(row["聊天次数"].ToString());
				}
				if(row["在线时长"]!=null && row["在线时长"].ToString()!="")
				{
					model.在线时长=DateTime.Parse(row["在线时长"].ToString());
				}
				if(row["首次进入时间"]!=null && row["首次进入时间"].ToString()!="")
				{
					model.首次进入时间=DateTime.Parse(row["首次进入时间"].ToString());
				}
				if(row["最后离开时间"]!=null && row["最后离开时间"].ToString()!="")
				{
					model.最后离开时间=DateTime.Parse(row["最后离开时间"].ToString());
				}
				if(row["最后观看设备"]!=null)
				{
					model.最后观看设备=row["最后观看设备"].ToString();
				}
				if(row["最后观看方式"]!=null)
				{
					model.最后观看方式=row["最后观看方式"].ToString();
				}
				if(row["最后登录ip"]!=null)
				{
					model.最后登录ip=row["最后登录ip"].ToString();
				}
				if(row["是否职工"]!=null)
				{
					model.是否职工=row["是否职工"].ToString();
				}
				if(row["省"]!=null)
				{
					model.省=row["省"].ToString();
				}
				if(row["市"]!=null)
				{
					model.市=row["市"].ToString();
				}
				if(row["公司"]!=null)
				{
					model.公司=row["公司"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select 观众ID,观众名,第三方ID,地址,观看总时长,手机号,访问来源,访问次数,分享次数,聊天次数,在线时长,首次进入时间,最后离开时间,最后观看设备,最后观看方式,最后登录ip,是否职工,省,市,公司 ");
			strSql.Append(" FROM gz_data ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" 观众ID,观众名,第三方ID,地址,观看总时长,手机号,访问来源,访问次数,分享次数,聊天次数,在线时长,首次进入时间,最后离开时间,最后观看设备,最后观看方式,最后登录ip,是否职工,省,市,公司 ");
			strSql.Append(" FROM gz_data ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM gz_data ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T. desc");
			}
			strSql.Append(")AS Row, T.*  from gz_data T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "gz_data";
			parameters[1].Value = "";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

