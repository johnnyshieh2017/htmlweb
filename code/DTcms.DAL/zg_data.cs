﻿/**  版本信息模板在安装目录下，可自行修改。
* zg_data.cs
*
* 功 能： N/A
* 类 名： zg_data
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2020/3/14 10:52:32   N/A    初版
*
* Copyright (c) 2019  Corporation. All rights reserved.
*/
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DTcms.DBUtility;
namespace DTcms.DAL
{
	/// <summary>
	/// 数据访问类:zg_data
	/// </summary>
	public partial class zg_data
	{
		public zg_data()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(DTcms.Model.zg_data model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into zg_data(");
			strSql.Append("省区,城市,公司名称,姓名,职务,微信昵称,手机号,F8)");
			strSql.Append(" values (");
			strSql.Append("@省区,@城市,@公司名称,@姓名,@职务,@微信昵称,@手机号,@F8)");
			SqlParameter[] parameters = {
					new SqlParameter("@省区", SqlDbType.NVarChar,255),
					new SqlParameter("@城市", SqlDbType.NVarChar,255),
					new SqlParameter("@公司名称", SqlDbType.NVarChar,255),
					new SqlParameter("@姓名", SqlDbType.NVarChar,255),
					new SqlParameter("@职务", SqlDbType.NVarChar,255),
					new SqlParameter("@微信昵称", SqlDbType.NVarChar,255),
					new SqlParameter("@手机号", SqlDbType.NVarChar,255),
					new SqlParameter("@F8", SqlDbType.NVarChar,255)};
			parameters[0].Value = model.省区;
			parameters[1].Value = model.城市;
			parameters[2].Value = model.公司名称;
			parameters[3].Value = model.姓名;
			parameters[4].Value = model.职务;
			parameters[5].Value = model.微信昵称;
			parameters[6].Value = model.手机号;
			parameters[7].Value = model.F8;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTcms.Model.zg_data model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update zg_data set ");
			strSql.Append("省区=@省区,");
			strSql.Append("城市=@城市,");
			strSql.Append("公司名称=@公司名称,");
			strSql.Append("姓名=@姓名,");
			strSql.Append("职务=@职务,");
			strSql.Append("微信昵称=@微信昵称,");
			strSql.Append("手机号=@手机号,");
			strSql.Append("F8=@F8");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
					new SqlParameter("@省区", SqlDbType.NVarChar,255),
					new SqlParameter("@城市", SqlDbType.NVarChar,255),
					new SqlParameter("@公司名称", SqlDbType.NVarChar,255),
					new SqlParameter("@姓名", SqlDbType.NVarChar,255),
					new SqlParameter("@职务", SqlDbType.NVarChar,255),
					new SqlParameter("@微信昵称", SqlDbType.NVarChar,255),
					new SqlParameter("@手机号", SqlDbType.NVarChar,255),
					new SqlParameter("@F8", SqlDbType.NVarChar,255)};
			parameters[0].Value = model.省区;
			parameters[1].Value = model.城市;
			parameters[2].Value = model.公司名称;
			parameters[3].Value = model.姓名;
			parameters[4].Value = model.职务;
			parameters[5].Value = model.微信昵称;
			parameters[6].Value = model.手机号;
			parameters[7].Value = model.F8;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete()
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from zg_data ");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
			};

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.zg_data GetModel()
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 省区,城市,公司名称,姓名,职务,微信昵称,手机号,F8 from zg_data ");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
			};

			DTcms.Model.zg_data model=new DTcms.Model.zg_data();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.zg_data DataRowToModel(DataRow row)
		{
			DTcms.Model.zg_data model=new DTcms.Model.zg_data();
			if (row != null)
			{
				if(row["省区"]!=null)
				{
					model.省区=row["省区"].ToString();
				}
				if(row["城市"]!=null)
				{
					model.城市=row["城市"].ToString();
				}
				if(row["公司名称"]!=null)
				{
					model.公司名称=row["公司名称"].ToString();
				}
				if(row["姓名"]!=null)
				{
					model.姓名=row["姓名"].ToString();
				}
				if(row["职务"]!=null)
				{
					model.职务=row["职务"].ToString();
				}
				if(row["微信昵称"]!=null)
				{
					model.微信昵称=row["微信昵称"].ToString();
				}
				if(row["手机号"]!=null)
				{
					model.手机号=row["手机号"].ToString();
				}
				if(row["F8"]!=null)
				{
					model.F8=row["F8"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select 省区,城市,公司名称,姓名,职务,微信昵称,手机号,F8 ");
			strSql.Append(" FROM zg_data ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" 省区,城市,公司名称,姓名,职务,微信昵称,手机号,F8 ");
			strSql.Append(" FROM zg_data ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM zg_data ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T. desc");
			}
			strSql.Append(")AS Row, T.*  from zg_data T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "zg_data";
			parameters[1].Value = "";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

