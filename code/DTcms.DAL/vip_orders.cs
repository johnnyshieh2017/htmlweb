﻿/**  版本信息模板在安装目录下，可自行修改。
* vip_orders.cs
*
* 功 能： N/A
* 类 名： vip_orders
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2020/12/21 10:20:22   N/A    初版
*
* Copyright (c) 2019  Corporation. All rights reserved.
*/
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DTcms.DBUtility;//Please add references
using DTcms.Common;

namespace DTcms.DAL
{
	/// <summary>
	/// 数据访问类:vip_orders
	/// </summary>
	public partial class vip_orders
	{
		public vip_orders()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("id", "vip_orders"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from vip_orders");
			strSql.Append(" where id=@id");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
			parameters[0].Value = id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(DTcms.Model.vip_orders model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into vip_orders(");
            strSql.Append("source,service_typeid,customer,telephone,gender,contact_name,contact_telephone,customer_manager,customer_telephone,book_date,clinic_department,clinic_doctor,clinic_date,clinic_diagnosis,servicer_id,service_begin,service_end,status,pay_status,remark,add_time,return_visit_date,parent_id,order_number,user_id,clinic_begin_notic,clinic_end_notic,clinic_confirm,verifycode)");
            strSql.Append(" values (");
            strSql.Append("@source,@service_typeid,@customer,@telephone,@gender,@contact_name,@contact_telephone,@customer_manager,@customer_telephone,@book_date,@clinic_department,@clinic_doctor,@clinic_date,@clinic_diagnosis,@servicer_id,@service_begin,@service_end,@status,@pay_status,@remark,@add_time,@return_visit_date,@parent_id,@order_number,@user_id,@clinic_begin_notic,@clinic_end_notic,@clinic_confirm,@verifycode)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
                    new SqlParameter("@source", SqlDbType.VarChar,50),
                    new SqlParameter("@service_typeid", SqlDbType.Int,4),
                    new SqlParameter("@customer", SqlDbType.VarChar,50),
                    new SqlParameter("@telephone", SqlDbType.VarChar,20),
                    new SqlParameter("@gender", SqlDbType.VarChar,10),
                    new SqlParameter("@contact_name", SqlDbType.VarChar,50),
                    new SqlParameter("@contact_telephone", SqlDbType.VarChar,50),
                    new SqlParameter("@customer_manager", SqlDbType.VarChar,50),
                    new SqlParameter("@customer_telephone", SqlDbType.VarChar,20),
                    new SqlParameter("@book_date", SqlDbType.DateTime),
                    new SqlParameter("@clinic_department", SqlDbType.VarChar,50),
                    new SqlParameter("@clinic_doctor", SqlDbType.VarChar,50),
                    new SqlParameter("@clinic_date", SqlDbType.DateTime),
                    new SqlParameter("@clinic_diagnosis", SqlDbType.VarChar,500),
                    new SqlParameter("@servicer_id", SqlDbType.Int,4),
                    new SqlParameter("@service_begin", SqlDbType.DateTime),
                    new SqlParameter("@service_end", SqlDbType.DateTime),
                    new SqlParameter("@status", SqlDbType.Int,4),
                    new SqlParameter("@pay_status", SqlDbType.Int,4),
                    new SqlParameter("@remark", SqlDbType.VarChar,2000),
                    new SqlParameter("@add_time", SqlDbType.DateTime),
                    new SqlParameter("@return_visit_date", SqlDbType.DateTime),
                    new SqlParameter("@parent_id", SqlDbType.Int,4),
                    new SqlParameter("@order_number", SqlDbType.VarChar,50),
                    new SqlParameter("@user_id", SqlDbType.Int,4),
                    new SqlParameter("@clinic_begin_notic", SqlDbType.DateTime),
                    new SqlParameter("@clinic_end_notic", SqlDbType.DateTime),
                    new SqlParameter("@clinic_confirm", SqlDbType.VarChar,200),
                    new SqlParameter("@verifycode", SqlDbType.VarChar,50)};
            parameters[0].Value = model.source;
            parameters[1].Value = model.service_typeid;
            parameters[2].Value = model.customer;
            parameters[3].Value = model.telephone;
            parameters[4].Value = model.gender;
            parameters[5].Value = model.contact_name;
            parameters[6].Value = model.contact_telephone;
            parameters[7].Value = model.customer_manager;
            parameters[8].Value = model.customer_telephone;
            parameters[9].Value = model.book_date;
            parameters[10].Value = model.clinic_department;
            parameters[11].Value = model.clinic_doctor;
            parameters[12].Value = model.clinic_date;
            parameters[13].Value = model.clinic_diagnosis;
            parameters[14].Value = model.servicer_id;
            parameters[15].Value = model.service_begin;
            parameters[16].Value = model.service_end;
            parameters[17].Value = model.status;
            parameters[18].Value = model.pay_status;
            parameters[19].Value = model.remark;
            parameters[20].Value = model.add_time;
            parameters[21].Value = model.return_visit_date;
            parameters[22].Value = model.parent_id;
            parameters[23].Value = model.order_number;
            parameters[24].Value = model.user_id;
            parameters[25].Value = model.clinic_begin_notic;
            parameters[26].Value = model.clinic_end_notic;
            parameters[27].Value = model.clinic_confirm;
            parameters[28].Value = model.verifycode;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(DTcms.Model.vip_orders model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update vip_orders set ");
            strSql.Append("source=@source,");
            strSql.Append("service_typeid=@service_typeid,");
            strSql.Append("customer=@customer,");
            strSql.Append("telephone=@telephone,");
            strSql.Append("gender=@gender,");
            strSql.Append("contact_name=@contact_name,");
            strSql.Append("contact_telephone=@contact_telephone,");
            strSql.Append("customer_manager=@customer_manager,");
            strSql.Append("customer_telephone=@customer_telephone,");
            strSql.Append("book_date=@book_date,");
            strSql.Append("clinic_department=@clinic_department,");
            strSql.Append("clinic_doctor=@clinic_doctor,");
            strSql.Append("clinic_date=@clinic_date,");
            strSql.Append("clinic_diagnosis=@clinic_diagnosis,");
            strSql.Append("servicer_id=@servicer_id,");
            strSql.Append("service_begin=@service_begin,");
            strSql.Append("service_end=@service_end,");
            strSql.Append("status=@status,");
            strSql.Append("pay_status=@pay_status,");
            strSql.Append("remark=@remark,");
            strSql.Append("add_time=@add_time,");
            strSql.Append("return_visit_date=@return_visit_date,");
            strSql.Append("parent_id=@parent_id,");
            strSql.Append("order_number=@order_number,");
            strSql.Append("user_id=@user_id,");
            strSql.Append("clinic_begin_notic=@clinic_begin_notic,");
            strSql.Append("clinic_end_notic=@clinic_end_notic,");
            strSql.Append("clinic_confirm=@clinic_confirm,");
            strSql.Append("verifycode=@verifycode");
            strSql.Append(" where id=@id");
            SqlParameter[] parameters = {
                    new SqlParameter("@source", SqlDbType.VarChar,50),
                    new SqlParameter("@service_typeid", SqlDbType.Int,4),
                    new SqlParameter("@customer", SqlDbType.VarChar,50),
                    new SqlParameter("@telephone", SqlDbType.VarChar,20),
                    new SqlParameter("@gender", SqlDbType.VarChar,10),
                    new SqlParameter("@contact_name", SqlDbType.VarChar,50),
                    new SqlParameter("@contact_telephone", SqlDbType.VarChar,50),
                    new SqlParameter("@customer_manager", SqlDbType.VarChar,50),
                    new SqlParameter("@customer_telephone", SqlDbType.VarChar,20),
                    new SqlParameter("@book_date", SqlDbType.DateTime),
                    new SqlParameter("@clinic_department", SqlDbType.VarChar,50),
                    new SqlParameter("@clinic_doctor", SqlDbType.VarChar,50),
                    new SqlParameter("@clinic_date", SqlDbType.DateTime),
                    new SqlParameter("@clinic_diagnosis", SqlDbType.VarChar,500),
                    new SqlParameter("@servicer_id", SqlDbType.Int,4),
                    new SqlParameter("@service_begin", SqlDbType.DateTime),
                    new SqlParameter("@service_end", SqlDbType.DateTime),
                    new SqlParameter("@status", SqlDbType.Int,4),
                    new SqlParameter("@pay_status", SqlDbType.Int,4),
                    new SqlParameter("@remark", SqlDbType.VarChar,2000),
                    new SqlParameter("@add_time", SqlDbType.DateTime),
                    new SqlParameter("@return_visit_date", SqlDbType.DateTime),
                    new SqlParameter("@parent_id", SqlDbType.Int,4),
                    new SqlParameter("@order_number", SqlDbType.VarChar,50),
                    new SqlParameter("@user_id", SqlDbType.Int,4),
                    new SqlParameter("@clinic_begin_notic", SqlDbType.DateTime),
                    new SqlParameter("@clinic_end_notic", SqlDbType.DateTime),
                    new SqlParameter("@clinic_confirm", SqlDbType.VarChar,200),
                    new SqlParameter("@verifycode", SqlDbType.VarChar,50),
                    new SqlParameter("@id", SqlDbType.Int,4)};
            parameters[0].Value = model.source;
            parameters[1].Value = model.service_typeid;
            parameters[2].Value = model.customer;
            parameters[3].Value = model.telephone;
            parameters[4].Value = model.gender;
            parameters[5].Value = model.contact_name;
            parameters[6].Value = model.contact_telephone;
            parameters[7].Value = model.customer_manager;
            parameters[8].Value = model.customer_telephone;
            parameters[9].Value = model.book_date;
            parameters[10].Value = model.clinic_department;
            parameters[11].Value = model.clinic_doctor;
            parameters[12].Value = model.clinic_date;
            parameters[13].Value = model.clinic_diagnosis;
            parameters[14].Value = model.servicer_id;
            parameters[15].Value = model.service_begin;
            parameters[16].Value = model.service_end;
            parameters[17].Value = model.status;
            parameters[18].Value = model.pay_status;
            parameters[19].Value = model.remark;
            parameters[20].Value = model.add_time;
            parameters[21].Value = model.return_visit_date;
            parameters[22].Value = model.parent_id;
            parameters[23].Value = model.order_number;
            parameters[24].Value = model.user_id;
            parameters[25].Value = model.clinic_begin_notic;
            parameters[26].Value = model.clinic_end_notic;
            parameters[27].Value = model.clinic_confirm;
            parameters[28].Value = model.verifycode;
            parameters[29].Value = model.id;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int id)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from vip_orders ");
            strSql.Append(" where id=@id");
            SqlParameter[] parameters = {
                    new SqlParameter("@id", SqlDbType.Int,4)
            };
            parameters[0].Value = id;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string idlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from vip_orders ");
            strSql.Append(" where id in (" + idlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public DTcms.Model.vip_orders GetModel(int id)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            strSql.Append(" id,source,service_typeid,customer,telephone,gender,contact_name,contact_telephone,customer_manager,customer_telephone,book_date,clinic_department,clinic_doctor,clinic_date,clinic_diagnosis,servicer_id,service_begin,service_end,status,pay_status,remark,add_time,return_visit_date,parent_id,order_number,user_id,clinic_begin_notic,clinic_end_notic,clinic_confirm,verifycode ");
            strSql.Append(" FROM vip_orders ");
            strSql.Append(" where id=@id");
            SqlParameter[] parameters = {
                    new SqlParameter("@id", SqlDbType.Int,4)
            };
            parameters[0].Value = id;

            DTcms.Model.vip_orders model = new DTcms.Model.vip_orders();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public DTcms.Model.vip_orders DataRowToModel(DataRow row)
        {
            DTcms.Model.vip_orders model = new DTcms.Model.vip_orders();
            if (row != null)
            {
                if (row["id"] != null && row["id"].ToString() != "")
                {
                    model.id = int.Parse(row["id"].ToString());
                }
                if (row["source"] != null)
                {
                    model.source = row["source"].ToString();
                }
                if (row["service_typeid"] != null && row["service_typeid"].ToString() != "")
                {
                    model.service_typeid = int.Parse(row["service_typeid"].ToString());
                }
                if (row["customer"] != null)
                {
                    model.customer = row["customer"].ToString();
                }
                if (row["telephone"] != null)
                {
                    model.telephone = row["telephone"].ToString();
                }
                if (row["gender"] != null)
                {
                    model.gender = row["gender"].ToString();
                }
                if (row["contact_name"] != null)
                {
                    model.contact_name = row["contact_name"].ToString();
                }
                if (row["contact_telephone"] != null)
                {
                    model.contact_telephone = row["contact_telephone"].ToString();
                }
                if (row["customer_manager"] != null)
                {
                    model.customer_manager = row["customer_manager"].ToString();
                }
                if (row["customer_telephone"] != null)
                {
                    model.customer_telephone = row["customer_telephone"].ToString();
                }
                if (row["book_date"] != null && row["book_date"].ToString() != "")
                {
                    model.book_date = DateTime.Parse(row["book_date"].ToString());
                }
                if (row["clinic_department"] != null)
                {
                    model.clinic_department = row["clinic_department"].ToString();
                }
                if (row["clinic_doctor"] != null)
                {
                    model.clinic_doctor = row["clinic_doctor"].ToString();
                }
                if (row["clinic_date"] != null && row["clinic_date"].ToString() != "")
                {
                    model.clinic_date = DateTime.Parse(row["clinic_date"].ToString());
                }
                if (row["clinic_diagnosis"] != null)
                {
                    model.clinic_diagnosis = row["clinic_diagnosis"].ToString();
                }
                if (row["servicer_id"] != null && row["servicer_id"].ToString() != "")
                {
                    model.servicer_id = int.Parse(row["servicer_id"].ToString());
                }
                if (row["service_begin"] != null && row["service_begin"].ToString() != "")
                {
                    model.service_begin = DateTime.Parse(row["service_begin"].ToString());
                }
                if (row["service_end"] != null && row["service_end"].ToString() != "")
                {
                    model.service_end = DateTime.Parse(row["service_end"].ToString());
                }
                if (row["status"] != null && row["status"].ToString() != "")
                {
                    model.status = int.Parse(row["status"].ToString());
                }
                if (row["pay_status"] != null && row["pay_status"].ToString() != "")
                {
                    model.pay_status = int.Parse(row["pay_status"].ToString());
                }
                if (row["remark"] != null)
                {
                    model.remark = row["remark"].ToString();
                }
                if (row["add_time"] != null && row["add_time"].ToString() != "")
                {
                    model.add_time = DateTime.Parse(row["add_time"].ToString());
                }
                if (row["return_visit_date"] != null && row["return_visit_date"].ToString() != "")
                {
                    model.return_visit_date = DateTime.Parse(row["return_visit_date"].ToString());
                }
                if (row["parent_id"] != null && row["parent_id"].ToString() != "")
                {
                    model.parent_id = int.Parse(row["parent_id"].ToString());
                }
                if (row["order_number"] != null)
                {
                    model.order_number = row["order_number"].ToString();
                }
                if (row["user_id"] != null && row["user_id"].ToString() != "")
                {
                    model.user_id = int.Parse(row["user_id"].ToString());
                }
                if (row["clinic_begin_notic"] != null && row["clinic_begin_notic"].ToString() != "")
                {
                    model.clinic_begin_notic = DateTime.Parse(row["clinic_begin_notic"].ToString());
                }
                if (row["clinic_end_notic"] != null && row["clinic_end_notic"].ToString() != "")
                {
                    model.clinic_end_notic = DateTime.Parse(row["clinic_end_notic"].ToString());
                }

                if (row["clinic_confirm"] != null)
                {
                    model.clinic_confirm = row["clinic_confirm"].ToString();
                }
                if (row["verifycode"] != null)
                {
                    model.verifycode = row["verifycode"].ToString();
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            strSql.Append(" id,source,service_typeid,customer,telephone,gender,contact_name,contact_telephone,customer_manager,customer_telephone,book_date,clinic_department,clinic_doctor,clinic_date,clinic_diagnosis,servicer_id,service_begin,service_end,status,pay_status,remark,add_time,return_visit_date,parent_id,order_number,user_id,clinic_begin_notic,clinic_end_notic,clinic_confirm,verifycode ");
            strSql.Append(" FROM vip_orders ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" id,source,service_typeid,customer,telephone,gender,contact_name,contact_telephone,customer_manager,customer_telephone,book_date,clinic_department,clinic_doctor,clinic_date,clinic_diagnosis,servicer_id,service_begin,service_end,status,pay_status,remark,add_time,return_visit_date,parent_id,order_number,user_id,clinic_begin_notic,clinic_end_notic,clinic_confirm,verifycode ");
            strSql.Append(" FROM vip_orders ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM vip_orders ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.id desc");
            }
            strSql.Append(")AS Row, T.*  from vip_orders T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "vip_orders";
			parameters[1].Value = "id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

        #endregion  BasicMethod
        #region  ExtensionMethod
        /// <summary>
        /// 获得查询分页数据
        /// </summary>
        public DataSet GetList(int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * FROM View_vip_orders ");//vip_orders=> View_vip_orders
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            recordCount = Convert.ToInt32(DbHelperSQL.GetSingle(PagingHelper.CreateCountingSql(strSql.ToString())));
            return DbHelperSQL.Query(PagingHelper.CreatePagingSql(recordCount, pageSize, pageIndex, strSql.ToString(), filedOrder));
        }
        #endregion  ExtensionMethod
    }
}

