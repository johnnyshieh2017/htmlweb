﻿using System; 
using System.Text;
using System.Data.SqlClient;
using System.Collections.Generic; 
using System.Data;
using DTcms.DBUtility;
using DTcms.Common;
namespace DTcms.DAL  
{
	 	//yjr_zx_order
		public partial class yjr_zx_order
	{
   		     
		public bool Exists(int id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from yjr_zx_order");
			strSql.Append(" where ");
			                                       strSql.Append(" id = @id  ");
                            			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
			parameters[0].Value = id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}
		
				
		
		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(DTcms.Model.yjr_zx_order model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into yjr_zx_order(");			
            strSql.Append("userid,service_type,businessconfigid,businessappointmentid,question_desc,doctor_id,createtime,service_fee,pay_status,status,pay_time,ms_sessionid");
			strSql.Append(") values (");
            strSql.Append("@userid,@service_type,@businessconfigid,@businessappointmentid,@question_desc,@doctor_id,@createtime,@service_fee,@pay_status,@status,@pay_time,@ms_sessionid");            
            strSql.Append(") ");            
            strSql.Append(";select @@IDENTITY");		
			SqlParameter[] parameters = {
			            new SqlParameter("@userid", SqlDbType.Int,4) ,            
                        new SqlParameter("@service_type", SqlDbType.Int,4) ,            
                        new SqlParameter("@businessconfigid", SqlDbType.Int,4) ,            
                        new SqlParameter("@businessappointmentid", SqlDbType.Int,4) ,            
                        new SqlParameter("@question_desc", SqlDbType.NVarChar,-1) ,            
                        new SqlParameter("@doctor_id", SqlDbType.Int,4) ,            
                        new SqlParameter("@createtime", SqlDbType.DateTime) ,            
                        new SqlParameter("@service_fee", SqlDbType.Int,4) ,            
                        new SqlParameter("@pay_status", SqlDbType.Int,4) ,            
                        new SqlParameter("@status", SqlDbType.Int,4) ,            
                        new SqlParameter("@pay_time", SqlDbType.DateTime) ,            
                        new SqlParameter("@ms_sessionid", SqlDbType.VarChar,100)             
              
            };
			            
            parameters[0].Value = model.userid;                        
            parameters[1].Value = model.service_type;                        
            parameters[2].Value = model.businessconfigid;                        
            parameters[3].Value = model.businessappointmentid;                        
            parameters[4].Value = model.question_desc;                        
            parameters[5].Value = model.doctor_id;                        
            parameters[6].Value = model.createtime;                        
            parameters[7].Value = model.service_fee;                        
            parameters[8].Value = model.pay_status;                        
            parameters[9].Value = model.status;                        
            parameters[10].Value = model.pay_time;                        
            parameters[11].Value = model.ms_sessionid;                        
			   
			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);			
			if (obj == null)
			{
				return 0;
			}
			else
			{
				                    
            	return Convert.ToInt32(obj);
                                                                  
			}			   
            			
		}
		
		
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTcms.Model.yjr_zx_order model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update yjr_zx_order set ");
			                                                
            strSql.Append(" userid = @userid , ");                                    
            strSql.Append(" service_type = @service_type , ");                                    
            strSql.Append(" businessconfigid = @businessconfigid , ");                                    
            strSql.Append(" businessappointmentid = @businessappointmentid , ");                                    
            strSql.Append(" question_desc = @question_desc , ");                                    
            strSql.Append(" doctor_id = @doctor_id , ");                                    
            strSql.Append(" createtime = @createtime , ");                                    
            strSql.Append(" service_fee = @service_fee , ");                                    
            strSql.Append(" pay_status = @pay_status , ");                                    
            strSql.Append(" status = @status , ");                                    
            strSql.Append(" pay_time = @pay_time , ");                                    
            strSql.Append(" ms_sessionid = @ms_sessionid  ");            			
			strSql.Append(" where id=@id ");
						
SqlParameter[] parameters = {
			            new SqlParameter("@id", SqlDbType.Int,4) ,            
                        new SqlParameter("@userid", SqlDbType.Int,4) ,            
                        new SqlParameter("@service_type", SqlDbType.Int,4) ,            
                        new SqlParameter("@businessconfigid", SqlDbType.Int,4) ,            
                        new SqlParameter("@businessappointmentid", SqlDbType.Int,4) ,            
                        new SqlParameter("@question_desc", SqlDbType.NVarChar,-1) ,            
                        new SqlParameter("@doctor_id", SqlDbType.Int,4) ,            
                        new SqlParameter("@createtime", SqlDbType.DateTime) ,            
                        new SqlParameter("@service_fee", SqlDbType.Int,4) ,            
                        new SqlParameter("@pay_status", SqlDbType.Int,4) ,            
                        new SqlParameter("@status", SqlDbType.Int,4) ,            
                        new SqlParameter("@pay_time", SqlDbType.DateTime) ,            
                        new SqlParameter("@ms_sessionid", SqlDbType.VarChar,100)             
              
            };
						            
            parameters[0].Value = model.id;                        
            parameters[1].Value = model.userid;                        
            parameters[2].Value = model.service_type;                        
            parameters[3].Value = model.businessconfigid;                        
            parameters[4].Value = model.businessappointmentid;                        
            parameters[5].Value = model.question_desc;                        
            parameters[6].Value = model.doctor_id;                        
            parameters[7].Value = model.createtime;                        
            parameters[8].Value = model.service_fee;                        
            parameters[9].Value = model.pay_status;                        
            parameters[10].Value = model.status;                        
            parameters[11].Value = model.pay_time;                        
            parameters[12].Value = model.ms_sessionid;                        
            int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from yjr_zx_order ");
			strSql.Append(" where id=@id");
						SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
			parameters[0].Value = id;


			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
				/// <summary>
		/// 批量删除一批数据
		/// </summary>
		public bool DeleteList(string idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from yjr_zx_order ");
			strSql.Append(" where ID in ("+idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
				
		
		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.yjr_zx_order GetModel(int id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select id, userid, service_type, businessconfigid, businessappointmentid, question_desc, doctor_id, createtime, service_fee, pay_status, status, pay_time, ms_sessionid  ");			
			strSql.Append("  from yjr_zx_order ");
			strSql.Append(" where id=@id");
						SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
			parameters[0].Value = id;

			
			DTcms.Model.yjr_zx_order model=new DTcms.Model.yjr_zx_order();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			
			if(ds.Tables[0].Rows.Count>0)
			{
												if(ds.Tables[0].Rows[0]["id"].ToString()!="")
				{
					model.id=int.Parse(ds.Tables[0].Rows[0]["id"].ToString());
				}
																																if(ds.Tables[0].Rows[0]["userid"].ToString()!="")
				{
					model.userid=int.Parse(ds.Tables[0].Rows[0]["userid"].ToString());
				}
																																if(ds.Tables[0].Rows[0]["service_type"].ToString()!="")
				{
					model.service_type=int.Parse(ds.Tables[0].Rows[0]["service_type"].ToString());
				}
																																if(ds.Tables[0].Rows[0]["businessconfigid"].ToString()!="")
				{
					model.businessconfigid=int.Parse(ds.Tables[0].Rows[0]["businessconfigid"].ToString());
				}
																																if(ds.Tables[0].Rows[0]["businessappointmentid"].ToString()!="")
				{
					model.businessappointmentid=int.Parse(ds.Tables[0].Rows[0]["businessappointmentid"].ToString());
				}
																																				model.question_desc= ds.Tables[0].Rows[0]["question_desc"].ToString();
																												if(ds.Tables[0].Rows[0]["doctor_id"].ToString()!="")
				{
					model.doctor_id=int.Parse(ds.Tables[0].Rows[0]["doctor_id"].ToString());
				}
																																if(ds.Tables[0].Rows[0]["createtime"].ToString()!="")
				{
					model.createtime=DateTime.Parse(ds.Tables[0].Rows[0]["createtime"].ToString());
				}
																																if(ds.Tables[0].Rows[0]["service_fee"].ToString()!="")
				{
					model.service_fee=int.Parse(ds.Tables[0].Rows[0]["service_fee"].ToString());
				}
																																if(ds.Tables[0].Rows[0]["pay_status"].ToString()!="")
				{
					model.pay_status=int.Parse(ds.Tables[0].Rows[0]["pay_status"].ToString());
				}
																																if(ds.Tables[0].Rows[0]["status"].ToString()!="")
				{
					model.status=int.Parse(ds.Tables[0].Rows[0]["status"].ToString());
				}
																																if(ds.Tables[0].Rows[0]["pay_time"].ToString()!="")
				{
					model.pay_time=DateTime.Parse(ds.Tables[0].Rows[0]["pay_time"].ToString());
				}
																																				model.ms_sessionid= ds.Tables[0].Rows[0]["ms_sessionid"].ToString();
																										
				return model;
			}
			else
			{
				return null;
			}
		}
		
		
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select * ");
			strSql.Append(" FROM View_zx_order ");//yjr_zx_order
            if (strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}
		
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" * ");
			strSql.Append(" FROM yjr_zx_order ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得查询分页数据
		/// </summary>
		public DataSet GetList(int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
		 {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * FROM View_zx_order ");// yjr_zx_order=> View_zx_order
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            recordCount = Convert.ToInt32(DbHelperSQL.GetSingle(PagingHelper.CreateCountingSql(strSql.ToString())));
            return DbHelperSQL.Query(PagingHelper.CreatePagingSql(recordCount, pageSize, pageIndex, strSql.ToString(), filedOrder));
		}

   
	}
}

