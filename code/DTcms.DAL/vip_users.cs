﻿/**  版本信息模板在安装目录下，可自行修改。
* vip_users.cs
*
* 功 能： N/A
* 类 名： vip_users
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2020/12/21 10:20:23   N/A    初版
*
* Copyright (c) 2019  Corporation. All rights reserved.
*/
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DTcms.DBUtility;//Please add references
namespace DTcms.DAL
{
	/// <summary>
	/// 数据访问类:vip_users
	/// </summary>
	public partial class vip_users
	{
		public vip_users()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("id", "vip_users"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from vip_users");
			strSql.Append(" where id=@id");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
			parameters[0].Value = id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(DTcms.Model.vip_users model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into vip_users(");
			strSql.Append("user_truename,user_phone,user_wx_openid,add_time,status,user_type,user_email)");
			strSql.Append(" values (");
			strSql.Append("@user_truename,@user_phone,@user_wx_openid,@add_time,@status,@user_type,@user_email)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@user_truename", SqlDbType.VarChar,50),
					new SqlParameter("@user_phone", SqlDbType.VarChar,50),
					new SqlParameter("@user_wx_openid", SqlDbType.VarChar,50),
					new SqlParameter("@add_time", SqlDbType.DateTime),
					new SqlParameter("@status", SqlDbType.Int,4),
					new SqlParameter("@user_type", SqlDbType.Int,4),
					new SqlParameter("@user_email", SqlDbType.VarChar,50)};
			parameters[0].Value = model.user_truename;
			parameters[1].Value = model.user_phone;
			parameters[2].Value = model.user_wx_openid;
			parameters[3].Value = model.add_time;
			parameters[4].Value = model.status;
			parameters[5].Value = model.user_type;
			parameters[6].Value = model.user_email;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTcms.Model.vip_users model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update vip_users set ");
			strSql.Append("user_truename=@user_truename,");
			strSql.Append("user_phone=@user_phone,");
			strSql.Append("user_wx_openid=@user_wx_openid,");
			strSql.Append("add_time=@add_time,");
			strSql.Append("status=@status,");
			strSql.Append("user_type=@user_type,");
			strSql.Append("user_email=@user_email");
			strSql.Append(" where id=@id");
			SqlParameter[] parameters = {
					new SqlParameter("@user_truename", SqlDbType.VarChar,50),
					new SqlParameter("@user_phone", SqlDbType.VarChar,50),
					new SqlParameter("@user_wx_openid", SqlDbType.VarChar,50),
					new SqlParameter("@add_time", SqlDbType.DateTime),
					new SqlParameter("@status", SqlDbType.Int,4),
					new SqlParameter("@user_type", SqlDbType.Int,4),
					new SqlParameter("@user_email", SqlDbType.VarChar,50),
					new SqlParameter("@id", SqlDbType.Int,4)};
			parameters[0].Value = model.user_truename;
			parameters[1].Value = model.user_phone;
			parameters[2].Value = model.user_wx_openid;
			parameters[3].Value = model.add_time;
			parameters[4].Value = model.status;
			parameters[5].Value = model.user_type;
			parameters[6].Value = model.user_email;
			parameters[7].Value = model.id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from vip_users ");
			strSql.Append(" where id=@id");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
			parameters[0].Value = id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from vip_users ");
			strSql.Append(" where id in ("+idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.vip_users GetModel(int id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 id,user_truename,user_phone,user_wx_openid,add_time,status,user_type,user_email from vip_users ");
			strSql.Append(" where id=@id");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
			parameters[0].Value = id;

			DTcms.Model.vip_users model=new DTcms.Model.vip_users();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.vip_users DataRowToModel(DataRow row)
		{
			DTcms.Model.vip_users model=new DTcms.Model.vip_users();
			if (row != null)
			{
				if(row["id"]!=null && row["id"].ToString()!="")
				{
					model.id=int.Parse(row["id"].ToString());
				}
				if(row["user_truename"]!=null)
				{
					model.user_truename=row["user_truename"].ToString();
				}
				if(row["user_phone"]!=null)
				{
					model.user_phone=row["user_phone"].ToString();
				}
				if(row["user_wx_openid"]!=null)
				{
					model.user_wx_openid=row["user_wx_openid"].ToString();
				}
				if(row["add_time"]!=null && row["add_time"].ToString()!="")
				{
					model.add_time=DateTime.Parse(row["add_time"].ToString());
				}
				if(row["status"]!=null && row["status"].ToString()!="")
				{
					model.status=int.Parse(row["status"].ToString());
				}
				if(row["user_type"]!=null && row["user_type"].ToString()!="")
				{
					model.user_type=int.Parse(row["user_type"].ToString());
				}
				if(row["user_email"]!=null)
				{
					model.user_email=row["user_email"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select id,user_truename,user_phone,user_wx_openid,add_time,status,user_type,user_email ");
			strSql.Append(" FROM vip_users ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" id,user_truename,user_phone,user_wx_openid,add_time,status,user_type,user_email ");
			strSql.Append(" FROM vip_users ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM vip_users ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.id desc");
			}
			strSql.Append(")AS Row, T.*  from vip_users T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "vip_users";
			parameters[1].Value = "id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

