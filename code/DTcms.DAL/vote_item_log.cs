﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DTcms.DBUtility;//Please add references
using DTcms.Common;

namespace DTcms.DAL
{
	/// <summary>
	/// 数据访问类:vote_item_log
	/// </summary>
	public partial class vote_item_log
	{
		public vote_item_log()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("id", "dt_vote_item_log"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from dt_vote_item_log");
			strSql.Append(" where id=@id");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
			parameters[0].Value = id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from dt_vote_item_log");
            strSql.Append(" where " + strWhere);   
            return DbHelperSQL.Exists(strSql.ToString());
        }



        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(DTcms.Model.vote_item_log model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into dt_vote_item_log(");
            strSql.Append("vote_item_id,ip,user_id,add_time,openid,vote_subject_id,vote_item_value,vote_id)");
            strSql.Append(" values (");
            strSql.Append("@vote_item_id,@ip,@user_id,@add_time,@openid,@vote_subject_id,@vote_item_value,@vote_id)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
                    new SqlParameter("@vote_item_id", SqlDbType.Int,4),
                    new SqlParameter("@ip", SqlDbType.NVarChar,100),
                    new SqlParameter("@user_id", SqlDbType.Int,4),
                    new SqlParameter("@add_time", SqlDbType.DateTime),
                    new SqlParameter("@openid", SqlDbType.VarChar,128),
                    new SqlParameter("@vote_subject_id", SqlDbType.Int,4),
                    new SqlParameter("@vote_item_value", SqlDbType.NVarChar,300),
                    new SqlParameter("@vote_id", SqlDbType.Int,4)};
            parameters[0].Value = model.vote_item_id;
            parameters[1].Value = model.ip;
            parameters[2].Value = model.user_id;
            parameters[3].Value = model.add_time;
            parameters[4].Value = model.openid;
            parameters[5].Value = model.vote_subject_id;
            parameters[6].Value = model.vote_item_value;
            parameters[7].Value = model.vote_id;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(DTcms.Model.vote_item_log model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update dt_vote_item_log set ");
            strSql.Append("vote_item_id=@vote_item_id,");
            strSql.Append("ip=@ip,");
            strSql.Append("user_id=@user_id,");
            strSql.Append("add_time=@add_time,");
            strSql.Append("openid=@openid,");
            strSql.Append("vote_subject_id=@vote_subject_id,");
            strSql.Append("vote_item_value=@vote_item_value,");
            strSql.Append("vote_id=@vote_id");
            strSql.Append(" where id=@id");
            SqlParameter[] parameters = {
                    new SqlParameter("@vote_item_id", SqlDbType.Int,4),
                    new SqlParameter("@ip", SqlDbType.NVarChar,100),
                    new SqlParameter("@user_id", SqlDbType.Int,4),
                    new SqlParameter("@add_time", SqlDbType.DateTime),
                    new SqlParameter("@openid", SqlDbType.VarChar,128),
                    new SqlParameter("@vote_subject_id", SqlDbType.Int,4),
                    new SqlParameter("@vote_item_value", SqlDbType.NVarChar,300),
                    new SqlParameter("@vote_id", SqlDbType.Int,4),
                    new SqlParameter("@id", SqlDbType.Int,4)};
            parameters[0].Value = model.vote_item_id;
            parameters[1].Value = model.ip;
            parameters[2].Value = model.user_id;
            parameters[3].Value = model.add_time;
            parameters[4].Value = model.openid;
            parameters[5].Value = model.vote_subject_id;
            parameters[6].Value = model.vote_item_value;
            parameters[7].Value = model.vote_id;
            parameters[8].Value = model.id;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from dt_vote_item_log ");
			strSql.Append(" where id=@id");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
			parameters[0].Value = id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from dt_vote_item_log ");
			strSql.Append(" where id in ("+idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

        		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteWhere(string strWhere )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from dt_vote_item_log ");
			strSql.Append(" where "+strWhere);
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}     



		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Model.vote_item_log GetModel(int id)
		{
			
			StringBuilder strSql=new StringBuilder();
            strSql.Append("select  top 1 id,vote_item_id,ip,user_id,add_time,openid,vote_subject_id,vote_item_value,vote_id from dt_vote_item_log ");
            strSql.Append(" where id=@id");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
			parameters[0].Value = id;

			Model.vote_item_log model=new Model.vote_item_log();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Model.vote_item_log DataRowToModel(DataRow row)
		{
			Model.vote_item_log model=new Model.vote_item_log();
			if (row != null)
			{
				if(row["id"]!=null && row["id"].ToString()!="")
				{
					model.id=int.Parse(row["id"].ToString());
				}
				if(row["vote_item_id"]!=null && row["vote_item_id"].ToString()!="")
				{
					model.vote_item_id=int.Parse(row["vote_item_id"].ToString());
				}
				if(row["ip"]!=null)
				{
					model.ip=row["ip"].ToString();
				}
				if(row["user_id"]!=null && row["user_id"].ToString()!="")
				{
					model.user_id=int.Parse(row["user_id"].ToString());
				}
                if (row["add_time"] != null && row["add_time"].ToString() != "")
                {
                    model.add_time = DateTime.Parse(row["add_time"].ToString());
                }
                if (row["openid"] != null)
                {
                    model.openid = row["openid"].ToString();
                }
                if (row["vote_subject_id"] != null && row["vote_subject_id"].ToString() != "")
                {
                    model.vote_subject_id = int.Parse(row["vote_subject_id"].ToString());
                }
                if (row["vote_item_value"] != null)
                {
                    model.vote_item_value = row["vote_item_value"].ToString();
                }
                if (row["vote_id"] != null && row["vote_id"].ToString() != "")
                {
                    model.vote_id = int.Parse(row["vote_id"].ToString());
                }
            }
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("select id,vote_item_id,ip,user_id,add_time,openid,vote_subject_id,vote_item_value,vote_id ");
            strSql.Append(" FROM dt_vote_item_log ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
   

			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
            strSql.Append("  dbo.dt_vote_item_log.user_id, dbo.dt_vote_item_log.id, dbo.dt_vote_item_log.vote_item_id, dbo.dt_vote_item_log.ip, dbo.dt_vote_item_log.add_time, dbo.dt_users.nick_name, dbo.dt_users.avatar, dbo.dt_vote_item.[content] ");
            strSql.Append(" FROM dbo.dt_vote_item_log LEFT OUTER JOIN  dbo.dt_vote_item ON dbo.dt_vote_item_log.vote_item_id = dbo.dt_vote_item.id LEFT OUTER JOIN   dbo.dt_users ON dbo.dt_vote_item_log.user_id = dbo.dt_users.id ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM dt_vote_item_log ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.id desc");
			}
			strSql.Append(")AS Row, T.*  from dt_vote_item_log T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

        public DataSet GetList(int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * FROM dt_vote_item_log");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            recordCount = Convert.ToInt32(DbHelperSQL.GetSingle(PagingHelper.CreateCountingSql(strSql.ToString())));
            return DbHelperSQL.Query(PagingHelper.CreatePagingSql(recordCount, pageSize, pageIndex, strSql.ToString(), filedOrder));
        }
        /*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "dt_vote_item_log";
			parameters[1].Value = "id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

