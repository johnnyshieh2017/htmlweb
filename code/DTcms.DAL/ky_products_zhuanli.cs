﻿/**  版本信息模板在安装目录下，可自行修改。
* ky_products_zhuanli.cs
*
* 功 能： N/A
* 类 名： ky_products_zhuanli
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2021/7/14 16:21:22   N/A    初版
*
* Copyright (c) 2012 DTcms Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DTcms.DBUtility;//Please add references
using DTcms.Common;

namespace DTcms.DAL
{
	/// <summary>
	/// 数据访问类:ky_products_zhuanli
	/// </summary>
	public partial class ky_products_zhuanli
	{
		public ky_products_zhuanli()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("pd_id", "ky_products_zhuanli"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int pd_id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from ky_products_zhuanli");
			strSql.Append(" where pd_id=@pd_id");
			SqlParameter[] parameters = {
					new SqlParameter("@pd_id", SqlDbType.Int,4)
			};
			parameters[0].Value = pd_id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(DTcms.Model.ky_products_zhuanli model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into ky_products_zhuanli(");
			strSql.Append("add_time,add_user,p_id,zhuanli_mingcheng,zhuanli_leibie,dptid,fa_ming_ren,zhuan_li_quan_ren,shou_quan_gong_gao_ri,zhuan_li_hao,beizhu)");
			strSql.Append(" values (");
			strSql.Append("@add_time,@add_user,@p_id,@zhuanli_mingcheng,@zhuanli_leibie,@dptid,@fa_ming_ren,@zhuan_li_quan_ren,@shou_quan_gong_gao_ri,@zhuan_li_hao,@beizhu)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@add_time", SqlDbType.DateTime),
					new SqlParameter("@add_user", SqlDbType.VarChar,50),
					new SqlParameter("@p_id", SqlDbType.Int,4),
					new SqlParameter("@zhuanli_mingcheng", SqlDbType.VarChar,50),
					new SqlParameter("@zhuanli_leibie", SqlDbType.VarChar,50),
					new SqlParameter("@dptid", SqlDbType.Int,4),
					new SqlParameter("@fa_ming_ren", SqlDbType.VarChar,50),
					new SqlParameter("@zhuan_li_quan_ren", SqlDbType.VarChar,100),
					new SqlParameter("@shou_quan_gong_gao_ri", SqlDbType.VarChar,50),
					new SqlParameter("@zhuan_li_hao", SqlDbType.VarChar,50),
					new SqlParameter("@beizhu", SqlDbType.VarChar,200)};
			parameters[0].Value = model.add_time;
			parameters[1].Value = model.add_user;
			parameters[2].Value = model.p_id;
			parameters[3].Value = model.zhuanli_mingcheng;
			parameters[4].Value = model.zhuanli_leibie;
			parameters[5].Value = model.dptid;
			parameters[6].Value = model.fa_ming_ren;
			parameters[7].Value = model.zhuan_li_quan_ren;
			parameters[8].Value = model.shou_quan_gong_gao_ri;
			parameters[9].Value = model.zhuan_li_hao;
			parameters[10].Value = model.beizhu;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTcms.Model.ky_products_zhuanli model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update ky_products_zhuanli set ");
			strSql.Append("add_time=@add_time,");
			strSql.Append("add_user=@add_user,");
			strSql.Append("p_id=@p_id,");
			strSql.Append("zhuanli_mingcheng=@zhuanli_mingcheng,");
			strSql.Append("zhuanli_leibie=@zhuanli_leibie,");
			strSql.Append("dptid=@dptid,");
			strSql.Append("fa_ming_ren=@fa_ming_ren,");
			strSql.Append("zhuan_li_quan_ren=@zhuan_li_quan_ren,");
			strSql.Append("shou_quan_gong_gao_ri=@shou_quan_gong_gao_ri,");
			strSql.Append("zhuan_li_hao=@zhuan_li_hao,");
			strSql.Append("beizhu=@beizhu");
			strSql.Append(" where pd_id=@pd_id");
			SqlParameter[] parameters = {
					new SqlParameter("@add_time", SqlDbType.DateTime),
					new SqlParameter("@add_user", SqlDbType.VarChar,50),
					new SqlParameter("@p_id", SqlDbType.Int,4),
					new SqlParameter("@zhuanli_mingcheng", SqlDbType.VarChar,50),
					new SqlParameter("@zhuanli_leibie", SqlDbType.VarChar,50),
					new SqlParameter("@dptid", SqlDbType.Int,4),
					new SqlParameter("@fa_ming_ren", SqlDbType.VarChar,50),
					new SqlParameter("@zhuan_li_quan_ren", SqlDbType.VarChar,100),
					new SqlParameter("@shou_quan_gong_gao_ri", SqlDbType.VarChar,50),
					new SqlParameter("@zhuan_li_hao", SqlDbType.VarChar,50),
					new SqlParameter("@beizhu", SqlDbType.VarChar,200),
					new SqlParameter("@pd_id", SqlDbType.Int,4)};
			parameters[0].Value = model.add_time;
			parameters[1].Value = model.add_user;
			parameters[2].Value = model.p_id;
			parameters[3].Value = model.zhuanli_mingcheng;
			parameters[4].Value = model.zhuanli_leibie;
			parameters[5].Value = model.dptid;
			parameters[6].Value = model.fa_ming_ren;
			parameters[7].Value = model.zhuan_li_quan_ren;
			parameters[8].Value = model.shou_quan_gong_gao_ri;
			parameters[9].Value = model.zhuan_li_hao;
			parameters[10].Value = model.beizhu;
			parameters[11].Value = model.pd_id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int pd_id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from ky_products_zhuanli ");
			strSql.Append(" where pd_id=@pd_id");
			SqlParameter[] parameters = {
					new SqlParameter("@pd_id", SqlDbType.Int,4)
			};
			parameters[0].Value = pd_id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string pd_idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from ky_products_zhuanli ");
			strSql.Append(" where pd_id in ("+pd_idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.ky_products_zhuanli GetModel(int pd_id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 pd_id,add_time,add_user,p_id,zhuanli_mingcheng,zhuanli_leibie,dptid,fa_ming_ren,zhuan_li_quan_ren,shou_quan_gong_gao_ri,zhuan_li_hao,beizhu from ky_products_zhuanli ");
			strSql.Append(" where pd_id=@pd_id");
			SqlParameter[] parameters = {
					new SqlParameter("@pd_id", SqlDbType.Int,4)
			};
			parameters[0].Value = pd_id;

			DTcms.Model.ky_products_zhuanli model=new DTcms.Model.ky_products_zhuanli();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.ky_products_zhuanli DataRowToModel(DataRow row)
		{
			DTcms.Model.ky_products_zhuanli model=new DTcms.Model.ky_products_zhuanli();
			if (row != null)
			{
				if(row["pd_id"]!=null && row["pd_id"].ToString()!="")
				{
					model.pd_id=int.Parse(row["pd_id"].ToString());
				}
				if(row["add_time"]!=null && row["add_time"].ToString()!="")
				{
					model.add_time=DateTime.Parse(row["add_time"].ToString());
				}
				if(row["add_user"]!=null)
				{
					model.add_user=row["add_user"].ToString();
				}
				if(row["p_id"]!=null && row["p_id"].ToString()!="")
				{
					model.p_id=int.Parse(row["p_id"].ToString());
				}
				if(row["zhuanli_mingcheng"]!=null)
				{
					model.zhuanli_mingcheng=row["zhuanli_mingcheng"].ToString();
				}
				if(row["zhuanli_leibie"]!=null)
				{
					model.zhuanli_leibie=row["zhuanli_leibie"].ToString();
				}
				if(row["dptid"]!=null && row["dptid"].ToString()!="")
				{
					model.dptid=int.Parse(row["dptid"].ToString());
				}
				if(row["fa_ming_ren"]!=null)
				{
					model.fa_ming_ren=row["fa_ming_ren"].ToString();
				}
				if(row["zhuan_li_quan_ren"]!=null)
				{
					model.zhuan_li_quan_ren=row["zhuan_li_quan_ren"].ToString();
				}
				if(row["shou_quan_gong_gao_ri"]!=null)
				{
					model.shou_quan_gong_gao_ri=row["shou_quan_gong_gao_ri"].ToString();
				}
				if(row["zhuan_li_hao"]!=null)
				{
					model.zhuan_li_hao=row["zhuan_li_hao"].ToString();
				}
				if(row["beizhu"]!=null)
				{
					model.beizhu=row["beizhu"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select pd_id,add_time,add_user,p_id,zhuanli_mingcheng,zhuanli_leibie,dptid,fa_ming_ren,zhuan_li_quan_ren,shou_quan_gong_gao_ri,zhuan_li_hao,beizhu ");
			strSql.Append(" FROM ky_products_zhuanli ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" pd_id,add_time,add_user,p_id,zhuanli_mingcheng,zhuanli_leibie,dptid,fa_ming_ren,zhuan_li_quan_ren,shou_quan_gong_gao_ri,zhuan_li_hao,beizhu ");
			strSql.Append(" FROM ky_products_zhuanli ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM ky_products_zhuanli ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.pd_id desc");
			}
			strSql.Append(")AS Row, T.*  from ky_products_zhuanli T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "ky_products_zhuanli";
			parameters[1].Value = "pd_id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod
		public DataSet GetList(int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
		{
			StringBuilder strSql = new StringBuilder();
			strSql.Append("select * FROM View_ky_products_zhuanli ");
			if (strWhere.Trim() != "")
			{
				strSql.Append(" where " + strWhere);
			}
			recordCount = Convert.ToInt32(DbHelperSQL.GetSingle(PagingHelper.CreateCountingSql(strSql.ToString())));
			return DbHelperSQL.Query(PagingHelper.CreatePagingSql(recordCount, pageSize, pageIndex, strSql.ToString(), filedOrder));
		}
		#endregion  ExtensionMethod
	}
}

