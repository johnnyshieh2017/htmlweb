﻿using System; 
using System.Text;
using System.Data.SqlClient;
using System.Collections.Generic; 
using System.Data;
using DTcms.DBUtility;
using DTcms.Common;
namespace DTcms.DAL  
{
	 	//yjr_doc_hospital
		public partial class yjr_doc_hospital
	{
   		     
		public bool Exists(int id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from yjr_doc_hospital");
			strSql.Append(" where ");
			                                       strSql.Append(" id = @id  ");
                            			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
			parameters[0].Value = id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}
		
				
		
		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(DTcms.Model.yjr_doc_hospital model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into yjr_doc_hospital(");			
            strSql.Append("hospitalname,hoslevel,address,tel,map,description,logo_url");
			strSql.Append(") values (");
            strSql.Append("@hospitalname,@hoslevel,@address,@tel,@map,@description,@logo_url");            
            strSql.Append(") ");            
            strSql.Append(";select @@IDENTITY");		
			SqlParameter[] parameters = {
			            new SqlParameter("@hospitalname", SqlDbType.NVarChar,50) ,            
                        new SqlParameter("@hoslevel", SqlDbType.VarChar,50) ,            
                        new SqlParameter("@address", SqlDbType.VarChar,100) ,            
                        new SqlParameter("@tel", SqlDbType.VarChar,100) ,            
                        new SqlParameter("@map", SqlDbType.VarChar,500) ,            
                        new SqlParameter("@description", SqlDbType.Text) ,            
                        new SqlParameter("@logo_url", SqlDbType.VarChar,120)             
              
            };
			            
            parameters[0].Value = model.hospitalname;                        
            parameters[1].Value = model.hoslevel;                        
            parameters[2].Value = model.address;                        
            parameters[3].Value = model.tel;                        
            parameters[4].Value = model.map;                        
            parameters[5].Value = model.description;                        
            parameters[6].Value = model.logo_url;                        
			   
			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);			
			if (obj == null)
			{
				return 0;
			}
			else
			{
				                    
            	return Convert.ToInt32(obj);
                                                                  
			}			   
            			
		}
		
		
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTcms.Model.yjr_doc_hospital model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update yjr_doc_hospital set ");
			                                                
            strSql.Append(" hospitalname = @hospitalname , ");                                    
            strSql.Append(" hoslevel = @hoslevel , ");                                    
            strSql.Append(" address = @address , ");                                    
            strSql.Append(" tel = @tel , ");                                    
            strSql.Append(" map = @map , ");                                    
            strSql.Append(" description = @description , ");                                    
            strSql.Append(" logo_url = @logo_url  ");            			
			strSql.Append(" where id=@id ");
						
SqlParameter[] parameters = {
			            new SqlParameter("@id", SqlDbType.Int,4) ,            
                        new SqlParameter("@hospitalname", SqlDbType.NVarChar,50) ,            
                        new SqlParameter("@hoslevel", SqlDbType.VarChar,50) ,            
                        new SqlParameter("@address", SqlDbType.VarChar,100) ,            
                        new SqlParameter("@tel", SqlDbType.VarChar,100) ,            
                        new SqlParameter("@map", SqlDbType.VarChar,500) ,            
                        new SqlParameter("@description", SqlDbType.Text) ,            
                        new SqlParameter("@logo_url", SqlDbType.VarChar,120)             
              
            };
						            
            parameters[0].Value = model.id;                        
            parameters[1].Value = model.hospitalname;                        
            parameters[2].Value = model.hoslevel;                        
            parameters[3].Value = model.address;                        
            parameters[4].Value = model.tel;                        
            parameters[5].Value = model.map;                        
            parameters[6].Value = model.description;                        
            parameters[7].Value = model.logo_url;                        
            int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from yjr_doc_hospital ");
			strSql.Append(" where id=@id");
						SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
			parameters[0].Value = id;


			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
				/// <summary>
		/// 批量删除一批数据
		/// </summary>
		public bool DeleteList(string idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from yjr_doc_hospital ");
			strSql.Append(" where ID in ("+idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
				
		
		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.yjr_doc_hospital GetModel(int id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select id, hospitalname, hoslevel, address, tel, map, description, logo_url  ");			
			strSql.Append("  from yjr_doc_hospital ");
			strSql.Append(" where id=@id");
						SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
			parameters[0].Value = id;

			
			DTcms.Model.yjr_doc_hospital model=new DTcms.Model.yjr_doc_hospital();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			
			if(ds.Tables[0].Rows.Count>0)
			{
												if(ds.Tables[0].Rows[0]["id"].ToString()!="")
				{
					model.id=int.Parse(ds.Tables[0].Rows[0]["id"].ToString());
				}
																																				model.hospitalname= ds.Tables[0].Rows[0]["hospitalname"].ToString();
																																model.hoslevel= ds.Tables[0].Rows[0]["hoslevel"].ToString();
																																model.address= ds.Tables[0].Rows[0]["address"].ToString();
																																model.tel= ds.Tables[0].Rows[0]["tel"].ToString();
																																model.map= ds.Tables[0].Rows[0]["map"].ToString();
																																model.description= ds.Tables[0].Rows[0]["description"].ToString();
																																model.logo_url= ds.Tables[0].Rows[0]["logo_url"].ToString();
																										
				return model;
			}
			else
			{
				return null;
			}
		}
		
		
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select * ");
			strSql.Append(" FROM yjr_doc_hospital ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}
		
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" * ");
			strSql.Append(" FROM yjr_doc_hospital ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得查询分页数据
		/// </summary>
		public DataSet GetList(int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
		 {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * FROM yjr_doc_hospital ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            recordCount = Convert.ToInt32(DbHelperSQL.GetSingle(PagingHelper.CreateCountingSql(strSql.ToString())));
            return DbHelperSQL.Query(PagingHelper.CreatePagingSql(recordCount, pageSize, pageIndex, strSql.ToString(), filedOrder));
		}

   
	}
}

