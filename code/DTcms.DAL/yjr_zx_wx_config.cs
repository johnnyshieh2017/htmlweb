﻿/**  版本信息模板在安装目录下，可自行修改。
* yjr_zx_wx_config.cs
*
* 功 能： N/A
* 类 名： yjr_zx_wx_config
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2020/9/15 17:27:32   N/A    初版
*
* Copyright (c) 2019  Corporation. All rights reserved.
*/
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DTcms.DBUtility;//Please add references
namespace DTcms.DAL
{
	/// <summary>
	/// 数据访问类:yjr_zx_wx_config
	/// </summary>
	public partial class yjr_zx_wx_config
	{
		public yjr_zx_wx_config()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string appid)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from yjr_zx_wx_config");
			strSql.Append(" where appid=@appid ");
			SqlParameter[] parameters = {
					new SqlParameter("@appid", SqlDbType.VarChar,50)			};
			parameters[0].Value = appid;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(DTcms.Model.yjr_zx_wx_config model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into yjr_zx_wx_config(");
			strSql.Append("appid,secret,token,access_token,updatetime)");
			strSql.Append(" values (");
			strSql.Append("@appid,@secret,@token,@access_token,@updatetime)");
			SqlParameter[] parameters = {
					new SqlParameter("@appid", SqlDbType.VarChar,50),
					new SqlParameter("@secret", SqlDbType.VarChar,100),
					new SqlParameter("@token", SqlDbType.VarChar,100),
					new SqlParameter("@access_token", SqlDbType.VarChar,600),
					new SqlParameter("@updatetime", SqlDbType.DateTime)};
			parameters[0].Value = model.appid;
			parameters[1].Value = model.secret;
			parameters[2].Value = model.token;
			parameters[3].Value = model.access_token;
			parameters[4].Value = model.updatetime;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTcms.Model.yjr_zx_wx_config model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update yjr_zx_wx_config set ");
			strSql.Append("secret=@secret,");
			strSql.Append("token=@token,");
			strSql.Append("access_token=@access_token,");
			strSql.Append("updatetime=@updatetime");
			strSql.Append(" where appid=@appid ");
			SqlParameter[] parameters = {
					new SqlParameter("@secret", SqlDbType.VarChar,100),
					new SqlParameter("@token", SqlDbType.VarChar,100),
					new SqlParameter("@access_token", SqlDbType.VarChar,600),
					new SqlParameter("@updatetime", SqlDbType.DateTime),
					new SqlParameter("@appid", SqlDbType.VarChar,50)};
			parameters[0].Value = model.secret;
			parameters[1].Value = model.token;
			parameters[2].Value = model.access_token;
			parameters[3].Value = model.updatetime;
			parameters[4].Value = model.appid;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string appid)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from yjr_zx_wx_config ");
			strSql.Append(" where appid=@appid ");
			SqlParameter[] parameters = {
					new SqlParameter("@appid", SqlDbType.VarChar,50)			};
			parameters[0].Value = appid;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string appidlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from yjr_zx_wx_config ");
			strSql.Append(" where appid in ("+appidlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.yjr_zx_wx_config GetModel(string appid)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 appid,secret,token,access_token,updatetime from yjr_zx_wx_config ");
			strSql.Append(" where appid=@appid ");
			SqlParameter[] parameters = {
					new SqlParameter("@appid", SqlDbType.VarChar,50)			};
			parameters[0].Value = appid;

			DTcms.Model.yjr_zx_wx_config model=new DTcms.Model.yjr_zx_wx_config();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.yjr_zx_wx_config DataRowToModel(DataRow row)
		{
			DTcms.Model.yjr_zx_wx_config model=new DTcms.Model.yjr_zx_wx_config();
			if (row != null)
			{
				if(row["appid"]!=null)
				{
					model.appid=row["appid"].ToString();
				}
				if(row["secret"]!=null)
				{
					model.secret=row["secret"].ToString();
				}
				if(row["token"]!=null)
				{
					model.token=row["token"].ToString();
				}
				if(row["access_token"]!=null)
				{
					model.access_token=row["access_token"].ToString();
				}
				if(row["updatetime"]!=null && row["updatetime"].ToString()!="")
				{
					model.updatetime=DateTime.Parse(row["updatetime"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select appid,secret,token,access_token,updatetime ");
			strSql.Append(" FROM yjr_zx_wx_config ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" appid,secret,token,access_token,updatetime ");
			strSql.Append(" FROM yjr_zx_wx_config ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM yjr_zx_wx_config ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.appid desc");
			}
			strSql.Append(")AS Row, T.*  from yjr_zx_wx_config T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "yjr_zx_wx_config";
			parameters[1].Value = "appid";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

