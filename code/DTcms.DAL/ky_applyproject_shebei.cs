﻿/**  版本信息模板在安装目录下，可自行修改。
* ky_applyproject_shebei.cs
*
* 功 能： N/A
* 类 名： ky_applyproject_shebei
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2021/7/27 10:34:25   N/A    初版
*
　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DTcms.DBUtility;//Please add references
using DTcms.Common;

namespace DTcms.DAL
{
	/// <summary>
	/// 数据访问类:ky_applyproject_shebei
	/// </summary>
	public partial class ky_applyproject_shebei
	{
		public ky_applyproject_shebei()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("id", "ky_applyproject_shebei"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from ky_applyproject_shebei");
			strSql.Append(" where id=@id");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
			parameters[0].Value = id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(DTcms.Model.ky_applyproject_shebei model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into ky_applyproject_shebei(");
			strSql.Append("pa_id,shebei,xinghao,jiage,tianjia_liyou,add_time,add_user,status)");
			strSql.Append(" values (");
			strSql.Append("@pa_id,@shebei,@xinghao,@jiage,@tianjia_liyou,@add_time,@add_user,@status)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@pa_id", SqlDbType.VarChar,50),
					new SqlParameter("@shebei", SqlDbType.VarChar,50),
					new SqlParameter("@xinghao", SqlDbType.VarChar,50),
					new SqlParameter("@jiage", SqlDbType.VarChar,50),
					new SqlParameter("@tianjia_liyou", SqlDbType.VarChar,50),
					new SqlParameter("@add_time", SqlDbType.DateTime),
					new SqlParameter("@add_user", SqlDbType.VarChar,50),
					new SqlParameter("@status", SqlDbType.Int,4)};
			parameters[0].Value = model.pa_id;
			parameters[1].Value = model.shebei;
			parameters[2].Value = model.xinghao;
			parameters[3].Value = model.jiage;
			parameters[4].Value = model.tianjia_liyou;
			parameters[5].Value = model.add_time;
			parameters[6].Value = model.add_user;
			parameters[7].Value = model.status;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTcms.Model.ky_applyproject_shebei model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update ky_applyproject_shebei set ");
			strSql.Append("pa_id=@pa_id,");
			strSql.Append("shebei=@shebei,");
			strSql.Append("xinghao=@xinghao,");
			strSql.Append("jiage=@jiage,");
			strSql.Append("tianjia_liyou=@tianjia_liyou,");
			strSql.Append("add_time=@add_time,");
			strSql.Append("add_user=@add_user,");
			strSql.Append("status=@status");
			strSql.Append(" where id=@id");
			SqlParameter[] parameters = {
					new SqlParameter("@pa_id", SqlDbType.VarChar,50),
					new SqlParameter("@shebei", SqlDbType.VarChar,50),
					new SqlParameter("@xinghao", SqlDbType.VarChar,50),
					new SqlParameter("@jiage", SqlDbType.VarChar,50),
					new SqlParameter("@tianjia_liyou", SqlDbType.VarChar,50),
					new SqlParameter("@add_time", SqlDbType.DateTime),
					new SqlParameter("@add_user", SqlDbType.VarChar,50),
					new SqlParameter("@status", SqlDbType.Int,4),
					new SqlParameter("@id", SqlDbType.Int,4)};
			parameters[0].Value = model.pa_id;
			parameters[1].Value = model.shebei;
			parameters[2].Value = model.xinghao;
			parameters[3].Value = model.jiage;
			parameters[4].Value = model.tianjia_liyou;
			parameters[5].Value = model.add_time;
			parameters[6].Value = model.add_user;
			parameters[7].Value = model.status;
			parameters[8].Value = model.id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from ky_applyproject_shebei ");
			strSql.Append(" where id=@id");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
			parameters[0].Value = id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from ky_applyproject_shebei ");
			strSql.Append(" where id in ("+idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.ky_applyproject_shebei GetModel(int id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 id,pa_id,shebei,xinghao,jiage,tianjia_liyou,add_time,add_user,status from ky_applyproject_shebei ");
			strSql.Append(" where id=@id");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
			parameters[0].Value = id;

			DTcms.Model.ky_applyproject_shebei model=new DTcms.Model.ky_applyproject_shebei();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.ky_applyproject_shebei DataRowToModel(DataRow row)
		{
			DTcms.Model.ky_applyproject_shebei model=new DTcms.Model.ky_applyproject_shebei();
			if (row != null)
			{
				if(row["id"]!=null && row["id"].ToString()!="")
				{
					model.id=int.Parse(row["id"].ToString());
				}
				if(row["pa_id"]!=null)
				{
					model.pa_id=row["pa_id"].ToString();
				}
				if(row["shebei"]!=null)
				{
					model.shebei=row["shebei"].ToString();
				}
				if(row["xinghao"]!=null)
				{
					model.xinghao=row["xinghao"].ToString();
				}
				if(row["jiage"]!=null)
				{
					model.jiage=row["jiage"].ToString();
				}
				if(row["tianjia_liyou"]!=null)
				{
					model.tianjia_liyou=row["tianjia_liyou"].ToString();
				}
				if(row["add_time"]!=null && row["add_time"].ToString()!="")
				{
					model.add_time=DateTime.Parse(row["add_time"].ToString());
				}
				if(row["add_user"]!=null)
				{
					model.add_user=row["add_user"].ToString();
				}
				if(row["status"]!=null && row["status"].ToString()!="")
				{
					model.status=int.Parse(row["status"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select id,pa_id,shebei,xinghao,jiage,tianjia_liyou,add_time,add_user,status ");
			strSql.Append(" FROM ky_applyproject_shebei ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" id,pa_id,shebei,xinghao,jiage,tianjia_liyou,add_time,add_user,status ");
			strSql.Append(" FROM ky_applyproject_shebei ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM ky_applyproject_shebei ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.id desc");
			}
			strSql.Append(")AS Row, T.*  from ky_applyproject_shebei T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "ky_applyproject_shebei";
			parameters[1].Value = "id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod
		public DataSet GetList(int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
		{
			StringBuilder strSql = new StringBuilder();
			strSql.Append("select * FROM ky_applyproject_shebei ");
			if (strWhere.Trim() != "")
			{
				strSql.Append(" where " + strWhere);
			}
			recordCount = Convert.ToInt32(DbHelperSQL.GetSingle(PagingHelper.CreateCountingSql(strSql.ToString())));
			return DbHelperSQL.Query(PagingHelper.CreatePagingSql(recordCount, pageSize, pageIndex, strSql.ToString(), filedOrder));
		}
		#endregion  ExtensionMethod
	}
}

