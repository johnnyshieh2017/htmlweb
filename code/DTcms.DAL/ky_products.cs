﻿/**  版本信息模板在安装目录下，可自行修改。
* ky_products.cs
*
* 功 能： N/A
* 类 名： ky_products
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2021/7/6 17:14:13   N/A    初版
*

*/
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DTcms.DBUtility;//Please add references
using DTcms.Common;

namespace DTcms.DAL
{
	/// <summary>
	/// 数据访问类:ky_products
	/// </summary>
	public partial class ky_products
	{
		public ky_products()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("pd_id", "ky_products"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int pd_id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from ky_products");
			strSql.Append(" where pd_id=@pd_id ");
			SqlParameter[] parameters = {
					new SqlParameter("@pd_id", SqlDbType.Int,4)			};
			parameters[0].Value = pd_id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(DTcms.Model.ky_products model)
		{
			StringBuilder strSql = new StringBuilder();
			strSql.Append("insert into ky_products(");
			strSql.Append("add_time,add_user,p_id,lun_wen_ming_cheng,kan_wu_ming_cheng,zi_lan_mu,qi_kan_ji_bie,dptid,zuo_zhe,zuo_zhe_lei_xing,shi_fou_ben_yuan,chu_ban_ri_qi,juan,qi,ye,beizhu)");
			strSql.Append(" values (");
			strSql.Append("@add_time,@add_user,@p_id,@lun_wen_ming_cheng,@kan_wu_ming_cheng,@zi_lan_mu,@qi_kan_ji_bie,@dptid,@zuo_zhe,@zuo_zhe_lei_xing,@shi_fou_ben_yuan,@chu_ban_ri_qi,@juan,@qi,@ye,@beizhu)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@add_time", SqlDbType.DateTime),
					new SqlParameter("@add_user", SqlDbType.VarChar,50),
					new SqlParameter("@p_id", SqlDbType.Int,4),
					new SqlParameter("@lun_wen_ming_cheng", SqlDbType.VarChar,50),
					new SqlParameter("@kan_wu_ming_cheng", SqlDbType.VarChar,50),
					new SqlParameter("@zi_lan_mu", SqlDbType.VarChar,50),
					new SqlParameter("@qi_kan_ji_bie", SqlDbType.VarChar,50),
					new SqlParameter("@dptid", SqlDbType.Int,4),
					new SqlParameter("@zuo_zhe", SqlDbType.VarChar,50),
					new SqlParameter("@zuo_zhe_lei_xing", SqlDbType.VarChar,50),
					new SqlParameter("@shi_fou_ben_yuan", SqlDbType.VarChar,50),
					new SqlParameter("@chu_ban_ri_qi", SqlDbType.DateTime),
					new SqlParameter("@juan", SqlDbType.VarChar,50),
					new SqlParameter("@qi", SqlDbType.VarChar,50),
					new SqlParameter("@ye", SqlDbType.VarChar,50),
					new SqlParameter("@beizhu", SqlDbType.VarChar,200)};
			parameters[0].Value = model.add_time;
			parameters[1].Value = model.add_user;
			parameters[2].Value = model.p_id;
			parameters[3].Value = model.lun_wen_ming_cheng;
			parameters[4].Value = model.kan_wu_ming_cheng;
			parameters[5].Value = model.zi_lan_mu;
			parameters[6].Value = model.qi_kan_ji_bie;
			parameters[7].Value = model.dptid;
			parameters[8].Value = model.zuo_zhe;
			parameters[9].Value = model.zuo_zhe_lei_xing;
			parameters[10].Value = model.shi_fou_ben_yuan;
			parameters[11].Value = model.chu_ban_ri_qi;
			parameters[12].Value = model.juan;
			parameters[13].Value = model.qi;
			parameters[14].Value = model.ye;
			parameters[15].Value = model.beizhu;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTcms.Model.ky_products model)
		{
			StringBuilder strSql = new StringBuilder();
			strSql.Append("update ky_products set ");
			strSql.Append("add_time=@add_time,");
			strSql.Append("add_user=@add_user,");
			strSql.Append("p_id=@p_id,");
			strSql.Append("lun_wen_ming_cheng=@lun_wen_ming_cheng,");
			strSql.Append("kan_wu_ming_cheng=@kan_wu_ming_cheng,");
			strSql.Append("zi_lan_mu=@zi_lan_mu,");
			strSql.Append("qi_kan_ji_bie=@qi_kan_ji_bie,");
			strSql.Append("dptid=@dptid,");
			strSql.Append("zuo_zhe=@zuo_zhe,");
			strSql.Append("zuo_zhe_lei_xing=@zuo_zhe_lei_xing,");
			strSql.Append("shi_fou_ben_yuan=@shi_fou_ben_yuan,");
			strSql.Append("chu_ban_ri_qi=@chu_ban_ri_qi,");
			strSql.Append("juan=@juan,");
			strSql.Append("qi=@qi,");
			strSql.Append("ye=@ye,");
			strSql.Append("beizhu=@beizhu");
			strSql.Append(" where pd_id=@pd_id");
			SqlParameter[] parameters = {
					new SqlParameter("@add_time", SqlDbType.DateTime),
					new SqlParameter("@add_user", SqlDbType.VarChar,50),
					new SqlParameter("@p_id", SqlDbType.Int,4),
					new SqlParameter("@lun_wen_ming_cheng", SqlDbType.VarChar,50),
					new SqlParameter("@kan_wu_ming_cheng", SqlDbType.VarChar,50),
					new SqlParameter("@zi_lan_mu", SqlDbType.VarChar,50),
					new SqlParameter("@qi_kan_ji_bie", SqlDbType.VarChar,50),
					new SqlParameter("@dptid", SqlDbType.Int,4),
					new SqlParameter("@zuo_zhe", SqlDbType.VarChar,50),
					new SqlParameter("@zuo_zhe_lei_xing", SqlDbType.VarChar,50),
					new SqlParameter("@shi_fou_ben_yuan", SqlDbType.VarChar,50),
					new SqlParameter("@chu_ban_ri_qi", SqlDbType.DateTime),
					new SqlParameter("@juan", SqlDbType.VarChar,50),
					new SqlParameter("@qi", SqlDbType.VarChar,50),
					new SqlParameter("@ye", SqlDbType.VarChar,50),
					new SqlParameter("@beizhu", SqlDbType.VarChar,200),
					new SqlParameter("@pd_id", SqlDbType.Int,4)};
			parameters[0].Value = model.add_time;
			parameters[1].Value = model.add_user;
			parameters[2].Value = model.p_id;
			parameters[3].Value = model.lun_wen_ming_cheng;
			parameters[4].Value = model.kan_wu_ming_cheng;
			parameters[5].Value = model.zi_lan_mu;
			parameters[6].Value = model.qi_kan_ji_bie;
			parameters[7].Value = model.dptid;
			parameters[8].Value = model.zuo_zhe;
			parameters[9].Value = model.zuo_zhe_lei_xing;
			parameters[10].Value = model.shi_fou_ben_yuan;
			parameters[11].Value = model.chu_ban_ri_qi;
			parameters[12].Value = model.juan;
			parameters[13].Value = model.qi;
			parameters[14].Value = model.ye;
			parameters[15].Value = model.beizhu;
			parameters[16].Value = model.pd_id;

			int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int pd_id)
		{

			StringBuilder strSql = new StringBuilder();
			strSql.Append("delete from ky_products ");
			strSql.Append(" where pd_id=@pd_id");
			SqlParameter[] parameters = {
					new SqlParameter("@pd_id", SqlDbType.Int,4)
			};
			parameters[0].Value = pd_id;

			int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string pd_idlist)
		{
			StringBuilder strSql = new StringBuilder();
			strSql.Append("delete from ky_products ");
			strSql.Append(" where pd_id in (" + pd_idlist + ")  ");
			int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.ky_products GetModel(int pd_id)
		{

			StringBuilder strSql = new StringBuilder();
			strSql.Append("select  top 1 pd_id,add_time,add_user,p_id,lun_wen_ming_cheng,kan_wu_ming_cheng,zi_lan_mu,qi_kan_ji_bie,dptid,zuo_zhe,zuo_zhe_lei_xing,shi_fou_ben_yuan,chu_ban_ri_qi,juan,qi,ye,beizhu from ky_products ");
			strSql.Append(" where pd_id=@pd_id");
			SqlParameter[] parameters = {
					new SqlParameter("@pd_id", SqlDbType.Int,4)
			};
			parameters[0].Value = pd_id;

			DTcms.Model.ky_products model = new DTcms.Model.ky_products();
			DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
			if (ds.Tables[0].Rows.Count > 0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.ky_products DataRowToModel(DataRow row)
		{
			DTcms.Model.ky_products model = new DTcms.Model.ky_products();
			if (row != null)
			{
				if (row["pd_id"] != null && row["pd_id"].ToString() != "")
				{
					model.pd_id = int.Parse(row["pd_id"].ToString());
				}
				if (row["add_time"] != null && row["add_time"].ToString() != "")
				{
					model.add_time = DateTime.Parse(row["add_time"].ToString());
				}
				if (row["add_user"] != null)
				{
					model.add_user = row["add_user"].ToString();
				}
				if (row["p_id"] != null && row["p_id"].ToString() != "")
				{
					model.p_id = int.Parse(row["p_id"].ToString());
				}
				if (row["lun_wen_ming_cheng"] != null)
				{
					model.lun_wen_ming_cheng = row["lun_wen_ming_cheng"].ToString();
				}
				if (row["kan_wu_ming_cheng"] != null)
				{
					model.kan_wu_ming_cheng = row["kan_wu_ming_cheng"].ToString();
				}
				if (row["zi_lan_mu"] != null)
				{
					model.zi_lan_mu = row["zi_lan_mu"].ToString();
				}
				if (row["qi_kan_ji_bie"] != null)
				{
					model.qi_kan_ji_bie = row["qi_kan_ji_bie"].ToString();
				}
				if (row["dptid"] != null && row["dptid"].ToString() != "")
				{
					model.dptid = int.Parse(row["dptid"].ToString());
				}
				if (row["zuo_zhe"] != null)
				{
					model.zuo_zhe = row["zuo_zhe"].ToString();
				}
				if (row["zuo_zhe_lei_xing"] != null)
				{
					model.zuo_zhe_lei_xing = row["zuo_zhe_lei_xing"].ToString();
				}
				if (row["shi_fou_ben_yuan"] != null)
				{
					model.shi_fou_ben_yuan = row["shi_fou_ben_yuan"].ToString();
				}
				if (row["chu_ban_ri_qi"] != null && row["chu_ban_ri_qi"].ToString() != "")
				{
					model.chu_ban_ri_qi = DateTime.Parse(row["chu_ban_ri_qi"].ToString());
				}
				if (row["juan"] != null)
				{
					model.juan = row["juan"].ToString();
				}
				if (row["qi"] != null)
				{
					model.qi = row["qi"].ToString();
				}
				if (row["ye"] != null)
				{
					model.ye = row["ye"].ToString();
				}
				if (row["beizhu"] != null)
				{
					model.beizhu = row["beizhu"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql = new StringBuilder();
			strSql.Append("select pd_id,add_time,add_user,p_id,lun_wen_ming_cheng,kan_wu_ming_cheng,zi_lan_mu,qi_kan_ji_bie,dptid,zuo_zhe,zuo_zhe_lei_xing,shi_fou_ben_yuan,chu_ban_ri_qi,juan,qi,ye,beizhu ");
			strSql.Append(" FROM ky_products ");
			if (strWhere.Trim() != "")
			{
				strSql.Append(" where " + strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top, string strWhere, string filedOrder)
		{
			StringBuilder strSql = new StringBuilder();
			strSql.Append("select ");
			if (Top > 0)
			{
				strSql.Append(" top " + Top.ToString());
			}
			strSql.Append(" pd_id,add_time,add_user,p_id,lun_wen_ming_cheng,kan_wu_ming_cheng,zi_lan_mu,qi_kan_ji_bie,dptid,zuo_zhe,zuo_zhe_lei_xing,shi_fou_ben_yuan,chu_ban_ri_qi,juan,qi,ye,beizhu ");
			strSql.Append(" FROM ky_products ");
			if (strWhere.Trim() != "")
			{
				strSql.Append(" where " + strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql = new StringBuilder();
			strSql.Append("select count(1) FROM ky_products ");
			if (strWhere.Trim() != "")
			{
				strSql.Append(" where " + strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql = new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby);
			}
			else
			{
				strSql.Append("order by T.pd_id desc");
			}
			strSql.Append(")AS Row, T.*  from ky_products T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "ky_products";
			parameters[1].Value = "pd_id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod
		/// <summary>
		/// 获得查询分页数据
		/// </summary>
		public DataSet GetList(int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
		{
			StringBuilder strSql = new StringBuilder();
			strSql.Append("select * FROM View_ky_products ");
			if (strWhere.Trim() != "")
			{
				strSql.Append(" where " + strWhere);
			}
			recordCount = Convert.ToInt32(DbHelperSQL.GetSingle(PagingHelper.CreateCountingSql(strSql.ToString())));
			return DbHelperSQL.Query(PagingHelper.CreatePagingSql(recordCount, pageSize, pageIndex, strSql.ToString(), filedOrder));
		}
		#endregion  ExtensionMethod
	}
}

