﻿using System; 
using System.Text;
using System.Data.SqlClient;
using System.Collections.Generic; 
using System.Data;
using DTcms.DBUtility;
using DTcms.Common;
namespace DTcms.DAL  
{
	 	//yjr_doctor_vsbusinessconfig
		public partial class yjr_doctor_vsbusinessconfig
	{
   		     
		public bool Exists(int id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from yjr_doctor_vsbusinessconfig");
			strSql.Append(" where ");
			                                       strSql.Append(" id = @id  ");
                            			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
			parameters[0].Value = id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}
		
				
		
		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(DTcms.Model.yjr_doctor_vsbusinessconfig model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into yjr_doctor_vsbusinessconfig(");			
            strSql.Append("doctorid,businessid,add_time,cost_type,cost_interval,price");
			strSql.Append(") values (");
            strSql.Append("@doctorid,@businessid,@add_time,@cost_type,@cost_interval,@price");            
            strSql.Append(") ");            
            strSql.Append(";select @@IDENTITY");		
			SqlParameter[] parameters = {
			            new SqlParameter("@doctorid", SqlDbType.Int,4) ,            
                        new SqlParameter("@businessid", SqlDbType.Int,4) ,            
                        new SqlParameter("@add_time", SqlDbType.DateTime) ,            
                        new SqlParameter("@cost_type", SqlDbType.Int,4) ,            
                        new SqlParameter("@cost_interval", SqlDbType.Int,4) ,            
                        new SqlParameter("@price", SqlDbType.Int,4)             
              
            };
			            
            parameters[0].Value = model.doctorid;                        
            parameters[1].Value = model.businessid;                        
            parameters[2].Value = model.add_time;                        
            parameters[3].Value = model.cost_type;                        
            parameters[4].Value = model.cost_interval;                        
            parameters[5].Value = model.price;                        
			   
			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);			
			if (obj == null)
			{
				return 0;
			}
			else
			{
				                    
            	return Convert.ToInt32(obj);
                                                                  
			}			   
            			
		}
		
		
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTcms.Model.yjr_doctor_vsbusinessconfig model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update yjr_doctor_vsbusinessconfig set ");
			                                                
            strSql.Append(" doctorid = @doctorid , ");                                    
            strSql.Append(" businessid = @businessid , ");                                    
            strSql.Append(" add_time = @add_time , ");                                    
            strSql.Append(" cost_type = @cost_type , ");                                    
            strSql.Append(" cost_interval = @cost_interval , ");                                    
            strSql.Append(" price = @price  ");            			
			strSql.Append(" where id=@id ");
						
SqlParameter[] parameters = {
			            new SqlParameter("@id", SqlDbType.Int,4) ,            
                        new SqlParameter("@doctorid", SqlDbType.Int,4) ,            
                        new SqlParameter("@businessid", SqlDbType.Int,4) ,            
                        new SqlParameter("@add_time", SqlDbType.DateTime) ,            
                        new SqlParameter("@cost_type", SqlDbType.Int,4) ,            
                        new SqlParameter("@cost_interval", SqlDbType.Int,4) ,            
                        new SqlParameter("@price", SqlDbType.Int,4)             
              
            };
						            
            parameters[0].Value = model.id;                        
            parameters[1].Value = model.doctorid;                        
            parameters[2].Value = model.businessid;                        
            parameters[3].Value = model.add_time;                        
            parameters[4].Value = model.cost_type;                        
            parameters[5].Value = model.cost_interval;                        
            parameters[6].Value = model.price;                        
            int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from yjr_doctor_vsbusinessconfig ");
			strSql.Append(" where id=@id");
						SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
			parameters[0].Value = id;


			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
				/// <summary>
		/// 批量删除一批数据
		/// </summary>
		public bool DeleteList(string idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from yjr_doctor_vsbusinessconfig ");
			strSql.Append(" where ID in ("+idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
				
		
		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.yjr_doctor_vsbusinessconfig GetModel(int id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select id, doctorid, businessid, add_time, cost_type, cost_interval, price  ");			
			strSql.Append("  from yjr_doctor_vsbusinessconfig ");
			strSql.Append(" where id=@id");
						SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
			parameters[0].Value = id;

			
			DTcms.Model.yjr_doctor_vsbusinessconfig model=new DTcms.Model.yjr_doctor_vsbusinessconfig();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			
			if(ds.Tables[0].Rows.Count>0)
			{
												if(ds.Tables[0].Rows[0]["id"].ToString()!="")
				{
					model.id=int.Parse(ds.Tables[0].Rows[0]["id"].ToString());
				}
																																if(ds.Tables[0].Rows[0]["doctorid"].ToString()!="")
				{
					model.doctorid=int.Parse(ds.Tables[0].Rows[0]["doctorid"].ToString());
				}
																																if(ds.Tables[0].Rows[0]["businessid"].ToString()!="")
				{
					model.businessid=int.Parse(ds.Tables[0].Rows[0]["businessid"].ToString());
				}
																																if(ds.Tables[0].Rows[0]["add_time"].ToString()!="")
				{
					model.add_time=DateTime.Parse(ds.Tables[0].Rows[0]["add_time"].ToString());
				}
																																if(ds.Tables[0].Rows[0]["cost_type"].ToString()!="")
				{
					model.cost_type=int.Parse(ds.Tables[0].Rows[0]["cost_type"].ToString());
				}
																																if(ds.Tables[0].Rows[0]["cost_interval"].ToString()!="")
				{
					model.cost_interval=int.Parse(ds.Tables[0].Rows[0]["cost_interval"].ToString());
				}
																																if(ds.Tables[0].Rows[0]["price"].ToString()!="")
				{
					model.price=int.Parse(ds.Tables[0].Rows[0]["price"].ToString());
				}
																														
				return model;
			}
			else
			{
				return null;
			}
		}
		
		
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select * ");
			strSql.Append(" FROM View_docvsbusiness ");//View_docvsbusiness   yjr_doctor_vsbusinessconfig
            if (strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}
		
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" * ");
			strSql.Append(" FROM yjr_doctor_vsbusinessconfig ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得查询分页数据
		/// </summary>
		public DataSet GetList(int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
		 {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * FROM View_docvsbusiness ");//yjr_doctor_vsbusinessconfig
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            recordCount = Convert.ToInt32(DbHelperSQL.GetSingle(PagingHelper.CreateCountingSql(strSql.ToString())));
            return DbHelperSQL.Query(PagingHelper.CreatePagingSql(recordCount, pageSize, pageIndex, strSql.ToString(), filedOrder));
		}

   
	}
}

