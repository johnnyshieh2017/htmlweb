﻿/**  版本信息模板在安装目录下，可自行修改。
* draw_gz_data.cs
*
* 功 能： N/A
* 类 名： draw_gz_data
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2020/4/1 15:19:14   N/A    初版
*
* Copyright (c) 2019  Corporation. All rights reserved.
*/
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DTcms.DBUtility;
using DTcms.Common;

namespace DTcms.DAL
{
	/// <summary>
	/// 数据访问类:draw_gz_data
	/// </summary>
	public partial class draw_gz_data
	{
		public draw_gz_data()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("id", "draw_gz_data"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from draw_gz_data");
			strSql.Append(" where id=@id");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
			parameters[0].Value = id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}



        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(DTcms.Model.draw_gz_data model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into draw_gz_data(");
            strSql.Append("MemID,MemName,ThirdID,Memlink,guankanshichang,mobile,fangwenlaiyuan,fangwencishu,fenxiangcishu,liaotiancishu,zaixianshichang,firsttime,lasttime,shebei,guankanfangshi,dengluIP,shifouzhigong,sheng,shi,gongsi,xingming,lotteryid)");
            strSql.Append(" values (");
            strSql.Append("@MemID,@MemName,@ThirdID,@Memlink,@guankanshichang,@mobile,@fangwenlaiyuan,@fangwencishu,@fenxiangcishu,@liaotiancishu,@zaixianshichang,@firsttime,@lasttime,@shebei,@guankanfangshi,@dengluIP,@shifouzhigong,@sheng,@shi,@gongsi,@xingming,@lotteryid)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
                    new SqlParameter("@MemID", SqlDbType.NVarChar,255),
                    new SqlParameter("@MemName", SqlDbType.NVarChar,255),
                    new SqlParameter("@ThirdID", SqlDbType.NVarChar,255),
                    new SqlParameter("@Memlink", SqlDbType.NVarChar,255),
                    new SqlParameter("@guankanshichang", SqlDbType.DateTime),
                    new SqlParameter("@mobile", SqlDbType.VarChar,50),
                    new SqlParameter("@fangwenlaiyuan", SqlDbType.NVarChar,255),
                    new SqlParameter("@fangwencishu", SqlDbType.Float,8),
                    new SqlParameter("@fenxiangcishu", SqlDbType.Float,8),
                    new SqlParameter("@liaotiancishu", SqlDbType.Float,8),
                    new SqlParameter("@zaixianshichang", SqlDbType.DateTime),
                    new SqlParameter("@firsttime", SqlDbType.DateTime),
                    new SqlParameter("@lasttime", SqlDbType.DateTime),
                    new SqlParameter("@shebei", SqlDbType.NVarChar,255),
                    new SqlParameter("@guankanfangshi", SqlDbType.NVarChar,255),
                    new SqlParameter("@dengluIP", SqlDbType.NVarChar,255),
                    new SqlParameter("@shifouzhigong", SqlDbType.NVarChar,50),
                    new SqlParameter("@sheng", SqlDbType.NVarChar,50),
                    new SqlParameter("@shi", SqlDbType.NVarChar,50),
                    new SqlParameter("@gongsi", SqlDbType.NVarChar,250),
                    new SqlParameter("@xingming", SqlDbType.NVarChar,250),
                    new SqlParameter("@lotteryid", SqlDbType.Int,4)};
            parameters[0].Value = model.MemID;
            parameters[1].Value = model.MemName;
            parameters[2].Value = model.ThirdID;
            parameters[3].Value = model.Memlink;
            parameters[4].Value = model.guankanshichang;
            parameters[5].Value = model.mobile;
            parameters[6].Value = model.fangwenlaiyuan;
            parameters[7].Value = model.fangwencishu;
            parameters[8].Value = model.fenxiangcishu;
            parameters[9].Value = model.liaotiancishu;
            parameters[10].Value = model.zaixianshichang;
            parameters[11].Value = model.firsttime;
            parameters[12].Value = model.lasttime;
            parameters[13].Value = model.shebei;
            parameters[14].Value = model.guankanfangshi;
            parameters[15].Value = model.dengluIP;
            parameters[16].Value = model.shifouzhigong;
            parameters[17].Value = model.sheng;
            parameters[18].Value = model.shi;
            parameters[19].Value = model.gongsi;
            parameters[20].Value = model.xingming;
            parameters[21].Value = model.lotteryid;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(DTcms.Model.draw_gz_data model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update draw_gz_data set ");
            strSql.Append("MemID=@MemID,");
            strSql.Append("MemName=@MemName,");
            strSql.Append("ThirdID=@ThirdID,");
            strSql.Append("Memlink=@Memlink,");
            strSql.Append("guankanshichang=@guankanshichang,");
            strSql.Append("mobile=@mobile,");
            strSql.Append("fangwenlaiyuan=@fangwenlaiyuan,");
            strSql.Append("fangwencishu=@fangwencishu,");
            strSql.Append("fenxiangcishu=@fenxiangcishu,");
            strSql.Append("liaotiancishu=@liaotiancishu,");
            strSql.Append("zaixianshichang=@zaixianshichang,");
            strSql.Append("firsttime=@firsttime,");
            strSql.Append("lasttime=@lasttime,");
            strSql.Append("shebei=@shebei,");
            strSql.Append("guankanfangshi=@guankanfangshi,");
            strSql.Append("dengluIP=@dengluIP,");
            strSql.Append("shifouzhigong=@shifouzhigong,");
            strSql.Append("sheng=@sheng,");
            strSql.Append("shi=@shi,");
            strSql.Append("gongsi=@gongsi,");
            strSql.Append("xingming=@xingming,");
            strSql.Append("lotteryid=@lotteryid");
            strSql.Append(" where id=@id");
            SqlParameter[] parameters = {
                    new SqlParameter("@MemID", SqlDbType.NVarChar,255),
                    new SqlParameter("@MemName", SqlDbType.NVarChar,255),
                    new SqlParameter("@ThirdID", SqlDbType.NVarChar,255),
                    new SqlParameter("@Memlink", SqlDbType.NVarChar,255),
                    new SqlParameter("@guankanshichang", SqlDbType.DateTime),
                    new SqlParameter("@mobile", SqlDbType.VarChar,50),
                    new SqlParameter("@fangwenlaiyuan", SqlDbType.NVarChar,255),
                    new SqlParameter("@fangwencishu", SqlDbType.Float,8),
                    new SqlParameter("@fenxiangcishu", SqlDbType.Float,8),
                    new SqlParameter("@liaotiancishu", SqlDbType.Float,8),
                    new SqlParameter("@zaixianshichang", SqlDbType.DateTime),
                    new SqlParameter("@firsttime", SqlDbType.DateTime),
                    new SqlParameter("@lasttime", SqlDbType.DateTime),
                    new SqlParameter("@shebei", SqlDbType.NVarChar,255),
                    new SqlParameter("@guankanfangshi", SqlDbType.NVarChar,255),
                    new SqlParameter("@dengluIP", SqlDbType.NVarChar,255),
                    new SqlParameter("@shifouzhigong", SqlDbType.NVarChar,50),
                    new SqlParameter("@sheng", SqlDbType.NVarChar,50),
                    new SqlParameter("@shi", SqlDbType.NVarChar,50),
                    new SqlParameter("@gongsi", SqlDbType.NVarChar,250),
                    new SqlParameter("@xingming", SqlDbType.NVarChar,250),
                    new SqlParameter("@lotteryid", SqlDbType.Int,4),
                    new SqlParameter("@id", SqlDbType.Int,4)};
            parameters[0].Value = model.MemID;
            parameters[1].Value = model.MemName;
            parameters[2].Value = model.ThirdID;
            parameters[3].Value = model.Memlink;
            parameters[4].Value = model.guankanshichang;
            parameters[5].Value = model.mobile;
            parameters[6].Value = model.fangwenlaiyuan;
            parameters[7].Value = model.fangwencishu;
            parameters[8].Value = model.fenxiangcishu;
            parameters[9].Value = model.liaotiancishu;
            parameters[10].Value = model.zaixianshichang;
            parameters[11].Value = model.firsttime;
            parameters[12].Value = model.lasttime;
            parameters[13].Value = model.shebei;
            parameters[14].Value = model.guankanfangshi;
            parameters[15].Value = model.dengluIP;
            parameters[16].Value = model.shifouzhigong;
            parameters[17].Value = model.sheng;
            parameters[18].Value = model.shi;
            parameters[19].Value = model.gongsi;
            parameters[20].Value = model.xingming;
            parameters[21].Value = model.lotteryid;
            parameters[22].Value = model.id;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

     

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from draw_gz_data ");
			strSql.Append(" where id=@id");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
			parameters[0].Value = id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from draw_gz_data ");
			strSql.Append(" where id in ("+idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.draw_gz_data GetModel(int id)
		{

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 id,MemID,MemName,ThirdID,Memlink,guankanshichang,mobile,fangwenlaiyuan,fangwencishu,fenxiangcishu,liaotiancishu,zaixianshichang,firsttime,lasttime,shebei,guankanfangshi,dengluIP,shifouzhigong,sheng,shi,gongsi,xingming,lotteryid from draw_gz_data ");
            strSql.Append(" where id=@id");
            SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
			parameters[0].Value = id;

			DTcms.Model.draw_gz_data model=new DTcms.Model.draw_gz_data();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public DTcms.Model.draw_gz_data DataRowToModel(DataRow row)
        {
            DTcms.Model.draw_gz_data model = new DTcms.Model.draw_gz_data();
            if (row != null)
            {
                if (row["id"] != null && row["id"].ToString() != "")
                {
                    model.id = int.Parse(row["id"].ToString());
                }
                if (row["MemID"] != null)
                {
                    model.MemID = row["MemID"].ToString();
                }
                if (row["MemName"] != null)
                {
                    model.MemName = row["MemName"].ToString();
                }
                if (row["ThirdID"] != null)
                {
                    model.ThirdID = row["ThirdID"].ToString();
                }
                if (row["Memlink"] != null)
                {
                    model.Memlink = row["Memlink"].ToString();
                }
                if (row["guankanshichang"] != null && row["guankanshichang"].ToString() != "")
                {
                    model.guankanshichang = DateTime.Parse(row["guankanshichang"].ToString());
                }
                if (row["mobile"] != null)
                {
                    model.mobile = row["mobile"].ToString();
                }
                if (row["fangwenlaiyuan"] != null)
                {
                    model.fangwenlaiyuan = row["fangwenlaiyuan"].ToString();
                }
                if (row["fangwencishu"] != null && row["fangwencishu"].ToString() != "")
                {
                    model.fangwencishu = decimal.Parse(row["fangwencishu"].ToString());
                }
                if (row["fenxiangcishu"] != null && row["fenxiangcishu"].ToString() != "")
                {
                    model.fenxiangcishu = decimal.Parse(row["fenxiangcishu"].ToString());
                }
                if (row["liaotiancishu"] != null && row["liaotiancishu"].ToString() != "")
                {
                    model.liaotiancishu = decimal.Parse(row["liaotiancishu"].ToString());
                }
                if (row["zaixianshichang"] != null && row["zaixianshichang"].ToString() != "")
                {
                    model.zaixianshichang = DateTime.Parse(row["zaixianshichang"].ToString());
                }
                if (row["firsttime"] != null && row["firsttime"].ToString() != "")
                {
                    model.firsttime = DateTime.Parse(row["firsttime"].ToString());
                }
                if (row["lasttime"] != null && row["lasttime"].ToString() != "")
                {
                    model.lasttime = DateTime.Parse(row["lasttime"].ToString());
                }
                if (row["shebei"] != null)
                {
                    model.shebei = row["shebei"].ToString();
                }
                if (row["guankanfangshi"] != null)
                {
                    model.guankanfangshi = row["guankanfangshi"].ToString();
                }
                if (row["dengluIP"] != null)
                {
                    model.dengluIP = row["dengluIP"].ToString();
                }
                if (row["shifouzhigong"] != null)
                {
                    model.shifouzhigong = row["shifouzhigong"].ToString();
                }
                if (row["sheng"] != null)
                {
                    model.sheng = row["sheng"].ToString();
                }
                if (row["shi"] != null)
                {
                    model.shi = row["shi"].ToString();
                }
                if (row["gongsi"] != null)
                {
                    model.gongsi = row["gongsi"].ToString();
                }
                if (row["xingming"] != null)
                {
                    model.xingming = row["xingming"].ToString();
                }
                if (row["lotteryid"] != null && row["lotteryid"].ToString() != "")
                {
                    model.lotteryid = int.Parse(row["lotteryid"].ToString());
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select id,MemID,MemName,ThirdID,Memlink,guankanshichang,mobile,fangwenlaiyuan,fangwencishu,fenxiangcishu,liaotiancishu,zaixianshichang,firsttime,lasttime,shebei,guankanfangshi,dengluIP,shifouzhigong,sheng,shi,gongsi,xingming,lotteryid ");
            strSql.Append(" FROM draw_gz_data ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" id,MemID,MemName,ThirdID,Memlink,guankanshichang,mobile,fangwenlaiyuan,fangwencishu,fenxiangcishu,liaotiancishu,zaixianshichang,firsttime,lasttime,shebei,guankanfangshi,dengluIP,shifouzhigong,sheng,shi,gongsi,xingming,lotteryid ");
            strSql.Append(" FROM draw_gz_data ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }


        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM draw_gz_data ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.id desc");
			}
			strSql.Append(")AS Row, T.*  from draw_gz_data T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

        /*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "draw_gz_data";
			parameters[1].Value = "id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

        #endregion  BasicMethod
        #region  ExtensionMethod
        /// <summary>
        /// 获得查询分页数据
        /// </summary>
        public DataSet GetList(int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * FROM draw_gz_data ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            recordCount = Convert.ToInt32(DbHelperSQL.GetSingle(PagingHelper.CreateCountingSql(strSql.ToString())));
            return DbHelperSQL.Query(PagingHelper.CreatePagingSql(recordCount, pageSize, pageIndex, strSql.ToString(), filedOrder));
        }

        public bool DeleteListwhere(string where)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from draw_gz_data ");
            strSql.Append(" where "+ where);
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public int updatebywhere(string where)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update draw_gz_data set xingming='2' ");
            strSql.Append(" where " + where);
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());

            return rows;
        }
        public int updatebywhere(string where,string xingming)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update draw_gz_data set xingming='"+xingming+"' ");
            strSql.Append(" where " + where);
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());

            return rows;
        }
        #endregion  ExtensionMethod
    }
}

