﻿/**  版本信息模板在安装目录下，可自行修改。
* yjr_doctor.cs
*
* 功 能： N/A
* 类 名： yjr_doctor
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2020/8/6 9:55:21   N/A    初版
*
* Copyright (c) 2019  Corporation. All rights reserved.
*/
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DTcms.DBUtility;//Please add references
using DTcms.Common;

namespace DTcms.DAL
{
	/// <summary>
	/// 数据访问类:yjr_doctor
	/// </summary>
	public partial class yjr_doctor
	{
		public yjr_doctor()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("id", "yjr_doctor"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from yjr_doctor");
			strSql.Append(" where id=@id");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
			parameters[0].Value = id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(DTcms.Model.yjr_doctor model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into yjr_doctor(");
            strSql.Append("name,title,gender,groupid,intro,speciality,logoimg,hospitalid,departmentid,department_name,createtime,status,isdelete,sort,siteid,mobile,idcard,idcardpic_front,idcardpic_back,doc_license,doc_qualification,doc_titlepic,position_id");
            strSql.Append(") values (");
            strSql.Append("@name,@title,@gender,@groupid,@intro,@speciality,@logoimg,@hospitalid,@departmentid,@department_name,@createtime,@status,@isdelete,@sort,@siteid,@mobile,@idcard,@idcardpic_front,@idcardpic_back,@doc_license,@doc_qualification,@doc_titlepic,@position_id");
            strSql.Append(") ");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
                        new SqlParameter("@name", SqlDbType.VarChar,50) ,
                        new SqlParameter("@title", SqlDbType.Int,4) ,
                        new SqlParameter("@gender", SqlDbType.VarChar,50) ,
                        new SqlParameter("@groupid", SqlDbType.Int,4) ,
                        new SqlParameter("@intro", SqlDbType.Text) ,
                        new SqlParameter("@speciality", SqlDbType.NVarChar,-1) ,
                        new SqlParameter("@logoimg", SqlDbType.VarChar,100) ,
                        new SqlParameter("@hospitalid", SqlDbType.Int,4) ,
                        new SqlParameter("@departmentid", SqlDbType.Int,4) ,
                        new SqlParameter("@department_name", SqlDbType.VarChar,100) ,
                        new SqlParameter("@createtime", SqlDbType.DateTime) ,
                        new SqlParameter("@status", SqlDbType.Int,4) ,
                        new SqlParameter("@isdelete", SqlDbType.Int,4) ,
                        new SqlParameter("@sort", SqlDbType.Int,4) ,
                        new SqlParameter("@siteid", SqlDbType.Int,4) ,
                        new SqlParameter("@mobile", SqlDbType.VarChar,50) ,
                        new SqlParameter("@idcard", SqlDbType.VarChar,50) ,
                        new SqlParameter("@idcardpic_front", SqlDbType.VarChar,100) ,
                        new SqlParameter("@idcardpic_back", SqlDbType.VarChar,100) ,
                        new SqlParameter("@doc_license", SqlDbType.VarChar,100) ,
                        new SqlParameter("@doc_qualification", SqlDbType.VarChar,100) ,
                        new SqlParameter("@doc_titlepic", SqlDbType.VarChar,100) ,
                        new SqlParameter("@position_id", SqlDbType.Int,4)

            };

            parameters[0].Value = model.name;
            parameters[1].Value = model.title;
            parameters[2].Value = model.gender;
            parameters[3].Value = model.groupid;
            parameters[4].Value = model.intro;
            parameters[5].Value = model.speciality;
            parameters[6].Value = model.logoimg;
            parameters[7].Value = model.hospitalid;
            parameters[8].Value = model.departmentid;
            parameters[9].Value = model.department_name;
            parameters[10].Value = model.createtime;
            parameters[11].Value = model.status;
            parameters[12].Value = model.isdelete;
            parameters[13].Value = model.sort;
            parameters[14].Value = model.siteid;
            parameters[15].Value = model.mobile;
            parameters[16].Value = model.idcard;
            parameters[17].Value = model.idcardpic_front;
            parameters[18].Value = model.idcardpic_back;
            parameters[19].Value = model.doc_license;
            parameters[20].Value = model.doc_qualification;
            parameters[21].Value = model.doc_titlepic;
            parameters[22].Value = model.position_id;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {

                return Convert.ToInt32(obj);

            }

        }


        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(DTcms.Model.yjr_doctor model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update yjr_doctor set ");

            strSql.Append(" name = @name , ");
            strSql.Append(" title = @title , ");
            strSql.Append(" gender = @gender , ");
            strSql.Append(" groupid = @groupid , ");
            strSql.Append(" intro = @intro , ");
            strSql.Append(" speciality = @speciality , ");
            strSql.Append(" logoimg = @logoimg , ");
            strSql.Append(" hospitalid = @hospitalid , ");
            strSql.Append(" departmentid = @departmentid , ");
            strSql.Append(" department_name = @department_name , ");
            strSql.Append(" createtime = @createtime , ");
            strSql.Append(" status = @status , ");
            strSql.Append(" isdelete = @isdelete , ");
            strSql.Append(" sort = @sort , ");
            strSql.Append(" siteid = @siteid , ");
            strSql.Append(" mobile = @mobile , ");
            strSql.Append(" idcard = @idcard , ");
            strSql.Append(" idcardpic_front = @idcardpic_front , ");
            strSql.Append(" idcardpic_back = @idcardpic_back , ");
            strSql.Append(" doc_license = @doc_license , ");
            strSql.Append(" doc_qualification = @doc_qualification , ");
            strSql.Append(" doc_titlepic = @doc_titlepic , ");
            strSql.Append(" position_id = @position_id  ");
            strSql.Append(" where id=@id ");

            SqlParameter[] parameters = {
                        new SqlParameter("@id", SqlDbType.Int,4) ,
                        new SqlParameter("@name", SqlDbType.VarChar,50) ,
                        new SqlParameter("@title", SqlDbType.Int,4) ,
                        new SqlParameter("@gender", SqlDbType.VarChar,50) ,
                        new SqlParameter("@groupid", SqlDbType.Int,4) ,
                        new SqlParameter("@intro", SqlDbType.Text) ,
                        new SqlParameter("@speciality", SqlDbType.NVarChar,-1) ,
                        new SqlParameter("@logoimg", SqlDbType.VarChar,100) ,
                        new SqlParameter("@hospitalid", SqlDbType.Int,4) ,
                        new SqlParameter("@departmentid", SqlDbType.Int,4) ,
                        new SqlParameter("@department_name", SqlDbType.VarChar,100) ,
                        new SqlParameter("@createtime", SqlDbType.DateTime) ,
                        new SqlParameter("@status", SqlDbType.Int,4) ,
                        new SqlParameter("@isdelete", SqlDbType.Int,4) ,
                        new SqlParameter("@sort", SqlDbType.Int,4) ,
                        new SqlParameter("@siteid", SqlDbType.Int,4) ,
                        new SqlParameter("@mobile", SqlDbType.VarChar,50) ,
                        new SqlParameter("@idcard", SqlDbType.VarChar,50) ,
                        new SqlParameter("@idcardpic_front", SqlDbType.VarChar,100) ,
                        new SqlParameter("@idcardpic_back", SqlDbType.VarChar,100) ,
                        new SqlParameter("@doc_license", SqlDbType.VarChar,100) ,
                        new SqlParameter("@doc_qualification", SqlDbType.VarChar,100) ,
                        new SqlParameter("@doc_titlepic", SqlDbType.VarChar,100) ,
                        new SqlParameter("@position_id", SqlDbType.Int,4)

            };

            parameters[0].Value = model.id;
            parameters[1].Value = model.name;
            parameters[2].Value = model.title;
            parameters[3].Value = model.gender;
            parameters[4].Value = model.groupid;
            parameters[5].Value = model.intro;
            parameters[6].Value = model.speciality;
            parameters[7].Value = model.logoimg;
            parameters[8].Value = model.hospitalid;
            parameters[9].Value = model.departmentid;
            parameters[10].Value = model.department_name;
            parameters[11].Value = model.createtime;
            parameters[12].Value = model.status;
            parameters[13].Value = model.isdelete;
            parameters[14].Value = model.sort;
            parameters[15].Value = model.siteid;
            parameters[16].Value = model.mobile;
            parameters[17].Value = model.idcard;
            parameters[18].Value = model.idcardpic_front;
            parameters[19].Value = model.idcardpic_back;
            parameters[20].Value = model.doc_license;
            parameters[21].Value = model.doc_qualification;
            parameters[22].Value = model.doc_titlepic;
            parameters[23].Value = model.position_id;
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int id)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from yjr_doctor ");
            strSql.Append(" where id=@id");
            SqlParameter[] parameters = {
                    new SqlParameter("@id", SqlDbType.Int,4)
            };
            parameters[0].Value = id;


            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 批量删除一批数据
        /// </summary>
        public bool DeleteList(string idlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from yjr_doctor ");
            strSql.Append(" where ID in (" + idlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public DTcms.Model.yjr_doctor GetModel(int id)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select yjr_doctor.*  ");
            //增加字段

            strSql.Append(",yjr_doc_hospital.hospitalname,yjr_doc_hospital.hoslevel,yjr_doctor_group.groupname,yjr_doctor_title.titlename ");

            strSql.Append("  from yjr_doctor  LEFT OUTER  join yjr_doc_hospital on yjr_doctor.hospitalid=yjr_doc_hospital.id ");
            strSql.Append("   LEFT OUTER  join yjr_doctor_group on yjr_doctor_group.id=yjr_doctor.departmentid ");
            strSql.Append("   LEFT OUTER  join yjr_doctor_title on yjr_doctor_title.id=yjr_doctor.title ");
            strSql.Append(" where yjr_doctor.id=@id");
            SqlParameter[] parameters = {
                    new SqlParameter("@id", SqlDbType.Int,4)
            };
            parameters[0].Value = id;


            DTcms.Model.yjr_doctor model = new DTcms.Model.yjr_doctor();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);

            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["id"].ToString() != "")
                {
                    model.id = int.Parse(ds.Tables[0].Rows[0]["id"].ToString());
                }
                model.name = ds.Tables[0].Rows[0]["name"].ToString();
                if (ds.Tables[0].Rows[0]["title"].ToString() != "")
                {
                    model.title = int.Parse(ds.Tables[0].Rows[0]["title"].ToString());
                }
                model.gender = ds.Tables[0].Rows[0]["gender"].ToString();
                if (ds.Tables[0].Rows[0]["groupid"].ToString() != "")
                {
                    model.groupid = int.Parse(ds.Tables[0].Rows[0]["groupid"].ToString());
                }
                model.intro = ds.Tables[0].Rows[0]["intro"].ToString();
                model.speciality = ds.Tables[0].Rows[0]["speciality"].ToString();
                model.logoimg = ds.Tables[0].Rows[0]["logoimg"].ToString();
                if (ds.Tables[0].Rows[0]["hospitalid"].ToString() != "")
                {
                    model.hospitalid = int.Parse(ds.Tables[0].Rows[0]["hospitalid"].ToString());
                }
                if (ds.Tables[0].Rows[0]["departmentid"].ToString() != "")
                {
                    model.departmentid = int.Parse(ds.Tables[0].Rows[0]["departmentid"].ToString());
                }
                model.department_name = ds.Tables[0].Rows[0]["department_name"].ToString();
                if (ds.Tables[0].Rows[0]["createtime"].ToString() != "")
                {
                    model.createtime = DateTime.Parse(ds.Tables[0].Rows[0]["createtime"].ToString());
                }
                if (ds.Tables[0].Rows[0]["status"].ToString() != "")
                {
                    model.status = int.Parse(ds.Tables[0].Rows[0]["status"].ToString());
                }
                if (ds.Tables[0].Rows[0]["isdelete"].ToString() != "")
                {
                    model.isdelete = int.Parse(ds.Tables[0].Rows[0]["isdelete"].ToString());
                }
                if (ds.Tables[0].Rows[0]["sort"].ToString() != "")
                {
                    model.sort = int.Parse(ds.Tables[0].Rows[0]["sort"].ToString());
                }
                if (ds.Tables[0].Rows[0]["siteid"].ToString() != "")
                {
                    model.siteid = int.Parse(ds.Tables[0].Rows[0]["siteid"].ToString());
                }
                model.mobile = ds.Tables[0].Rows[0]["mobile"].ToString();
                model.idcard = ds.Tables[0].Rows[0]["idcard"].ToString();
                model.idcardpic_front = ds.Tables[0].Rows[0]["idcardpic_front"].ToString();
                model.idcardpic_back = ds.Tables[0].Rows[0]["idcardpic_back"].ToString();
                model.doc_license = ds.Tables[0].Rows[0]["doc_license"].ToString();
                model.doc_qualification = ds.Tables[0].Rows[0]["doc_qualification"].ToString();
                model.doc_titlepic = ds.Tables[0].Rows[0]["doc_titlepic"].ToString();
                if (ds.Tables[0].Rows[0]["position_id"].ToString() != "")
                {
                    model.position_id = int.Parse(ds.Tables[0].Rows[0]["position_id"].ToString());
                }

                model.hospitalname = ds.Tables[0].Rows[0]["hospitalname"].ToString(); ;
                model.hospitallevel = ds.Tables[0].Rows[0]["hoslevel"].ToString(); ;
                model.titlename = ds.Tables[0].Rows[0]["titlename"].ToString(); ;
                model.department_name = ds.Tables[0].Rows[0]["groupname"].ToString(); ;
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.yjr_doctor DataRowToModel(DataRow row)
        {
            DTcms.Model.yjr_doctor model = new DTcms.Model.yjr_doctor();
            if (row != null)
            {
                if (row["id"] != null && row["id"].ToString() != "")
                {
                    model.id = int.Parse(row["id"].ToString());
                }
                if (row["name"] != null)
                {
                    model.name = row["name"].ToString();
                }
                if (row["title"] != null && row["title"].ToString() != "")
                {
                    model.title = int.Parse(row["title"].ToString());
                }
                if (row["gender"] != null)
                {
                    model.gender = row["gender"].ToString();
                }
                if (row["groupid"] != null && row["groupid"].ToString() != "")
                {
                    model.groupid = int.Parse(row["groupid"].ToString());
                }
                if (row["intro"] != null)
                {
                    model.intro = row["intro"].ToString();
                }
                if (row["speciality"] != null)
                {
                    model.speciality = row["speciality"].ToString();
                }
                if (row["logoimg"] != null)
                {
                    model.logoimg = row["logoimg"].ToString();
                }
                if (row["hospitalid"] != null && row["hospitalid"].ToString() != "")
                {
                    model.hospitalid = int.Parse(row["hospitalid"].ToString());
                }
                if (row["departmentid"] != null && row["departmentid"].ToString() != "")
                {
                    model.departmentid = int.Parse(row["departmentid"].ToString());
                }
                if (row["department_name"] != null)
                {
                    model.department_name = row["department_name"].ToString();
                }
                if (row["createtime"] != null && row["createtime"].ToString() != "")
                {
                    model.createtime = DateTime.Parse(row["createtime"].ToString());
                }
                if (row["status"] != null && row["status"].ToString() != "")
                {
                    model.status = int.Parse(row["status"].ToString());
                }
                if (row["isdelete"] != null && row["isdelete"].ToString() != "")
                {
                    model.isdelete = int.Parse(row["isdelete"].ToString());
                }
                if (row["sort"] != null && row["sort"].ToString() != "")
                {
                    model.sort = int.Parse(row["sort"].ToString());
                }
                if (row["siteid"] != null && row["siteid"].ToString() != "")
                {
                    model.siteid = int.Parse(row["siteid"].ToString());
                }
                if (row["mobile"] != null)
                {
                    model.mobile = row["mobile"].ToString();
                }
                if (row["idcard"] != null)
                {
                    model.idcard = row["idcard"].ToString();
                }
                if (row["idcardpic_front"] != null)
                {
                    model.idcardpic_front = row["idcardpic_front"].ToString();
                }
                if (row["idcardpic_back"] != null)
                {
                    model.idcardpic_back = row["idcardpic_back"].ToString();
                }
                if (row["doc_license"] != null)
                {
                    model.doc_license = row["doc_license"].ToString();
                }
                if (row["doc_qualification"] != null)
                {
                    model.doc_qualification = row["doc_qualification"].ToString();
                }
                if (row["doc_titlepic"] != null)
                {
                    model.doc_titlepic = row["doc_titlepic"].ToString();
                }
                if (row["position_id"] != null && row["position_id"].ToString() != "")
                {
                    model.position_id = int.Parse(row["position_id"].ToString());
                }

                if (row["hospitalname"] != null && row["hospitalname"].ToString() != "")
                {
                    model.hospitalname = row["hospitalname"].ToString();
                }
                if (row["hoslevel"] != null && row["hoslevel"].ToString() != "")
                {
                    model.hospitallevel = row["hoslevel"].ToString();
                }
                if (row["titlename"] != null && row["titlename"].ToString() != "")
                {
                    model.titlename = row["titlename"].ToString();
                }
                // 接诊率 接单量 平均响应时间
                if (row["rate"] != null && row["rate"].ToString() != "")
                {
                    model.rate = row["rate"].ToString();
                }
                if (row["bookcount"] != null && row["bookcount"].ToString() != "")
                {
                    model.bookcount = row["bookcount"].ToString();
                }
                if (row["responsetime"] != null && row["responsetime"].ToString() != "")
                {
                    model.responsetime = row["responsetime"].ToString();
                }

            }
            return model;
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * ");
            strSql.Append(" FROM yjr_doctor ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" * ");
            strSql.Append(" FROM yjr_doctor ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        #endregion  BasicMethod
        #region  ExtensionMethod
        /// <summary>
        /// 获得查询分页数据
        /// </summary>
        public DataSet GetList(int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * FROM View_doclist");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            recordCount = Convert.ToInt32(DbHelperSQL.GetSingle(PagingHelper.CreateCountingSql(strSql.ToString())));
            return DbHelperSQL.Query(PagingHelper.CreatePagingSql(recordCount, pageSize, pageIndex, strSql.ToString(), filedOrder));
        }
        #endregion  ExtensionMethod
    }
}

