﻿using System;
using System.Text;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Data;
using DTcms.DBUtility;
using DTcms.Common;
namespace DTcms.DAL
{
    //yjr_doctor_group
    public partial class yjr_doctor_group
    {

        public bool Exists(int id)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from yjr_doctor_group");
            strSql.Append(" where ");
            strSql.Append(" id = @id  ");
            SqlParameter[] parameters = {
                    new SqlParameter("@id", SqlDbType.Int,4)
            };
            parameters[0].Value = id;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }



        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(DTcms.Model.yjr_doctor_group model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into yjr_doctor_group(");
            strSql.Append("groupname,sort,status,siteid,hospatilid,description,leader_desc,logo_url");
            strSql.Append(") values (");
            strSql.Append("@groupname,@sort,@status,@siteid,@hospatilid,@description,@leader_desc,@logo_url");
            strSql.Append(") ");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
                        new SqlParameter("@groupname", SqlDbType.VarChar,100) ,
                        new SqlParameter("@sort", SqlDbType.Int,4) ,
                        new SqlParameter("@status", SqlDbType.Int,4) ,
                        new SqlParameter("@siteid", SqlDbType.Int,4) ,
                        new SqlParameter("@hospatilid", SqlDbType.Int,4) ,
                        new SqlParameter("@description", SqlDbType.Text) ,
                        new SqlParameter("@leader_desc", SqlDbType.Text) ,
                        new SqlParameter("@logo_url", SqlDbType.VarChar,120)

            };

            parameters[0].Value = model.groupname;
            parameters[1].Value = model.sort;
            parameters[2].Value = model.status;
            parameters[3].Value = model.siteid;
            parameters[4].Value = model.hospatilid;
            parameters[5].Value = model.description;
            parameters[6].Value = model.leader_desc;
            parameters[7].Value = model.logo_url;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {

                return Convert.ToInt32(obj);

            }

        }


        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(DTcms.Model.yjr_doctor_group model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update yjr_doctor_group set ");

            strSql.Append(" groupname = @groupname , ");
            strSql.Append(" sort = @sort , ");
            strSql.Append(" status = @status , ");
            strSql.Append(" siteid = @siteid , ");
            strSql.Append(" hospatilid = @hospatilid , ");
            strSql.Append(" description = @description , ");
            strSql.Append(" leader_desc = @leader_desc , ");
            strSql.Append(" logo_url = @logo_url  ");
            strSql.Append(" where id=@id ");

            SqlParameter[] parameters = {
                        new SqlParameter("@id", SqlDbType.Int,4) ,
                        new SqlParameter("@groupname", SqlDbType.VarChar,100) ,
                        new SqlParameter("@sort", SqlDbType.Int,4) ,
                        new SqlParameter("@status", SqlDbType.Int,4) ,
                        new SqlParameter("@siteid", SqlDbType.Int,4) ,
                        new SqlParameter("@hospatilid", SqlDbType.Int,4) ,
                        new SqlParameter("@description", SqlDbType.Text) ,
                        new SqlParameter("@leader_desc", SqlDbType.Text) ,
                        new SqlParameter("@logo_url", SqlDbType.VarChar,120)

            };

            parameters[0].Value = model.id;
            parameters[1].Value = model.groupname;
            parameters[2].Value = model.sort;
            parameters[3].Value = model.status;
            parameters[4].Value = model.siteid;
            parameters[5].Value = model.hospatilid;
            parameters[6].Value = model.description;
            parameters[7].Value = model.leader_desc;
            parameters[8].Value = model.logo_url;
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int id)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from yjr_doctor_group ");
            strSql.Append(" where id=@id");
            SqlParameter[] parameters = {
                    new SqlParameter("@id", SqlDbType.Int,4)
            };
            parameters[0].Value = id;


            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 批量删除一批数据
        /// </summary>
        public bool DeleteList(string idlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from yjr_doctor_group ");
            strSql.Append(" where ID in (" + idlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public DTcms.Model.yjr_doctor_group GetModel(int id)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select id, groupname, sort, status, siteid, hospatilid, description, leader_desc, logo_url  ");
            strSql.Append("  from yjr_doctor_group ");
            strSql.Append(" where id=@id");
            SqlParameter[] parameters = {
                    new SqlParameter("@id", SqlDbType.Int,4)
            };
            parameters[0].Value = id;


            DTcms.Model.yjr_doctor_group model = new DTcms.Model.yjr_doctor_group();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);

            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["id"].ToString() != "")
                {
                    model.id = int.Parse(ds.Tables[0].Rows[0]["id"].ToString());
                }
                model.groupname = ds.Tables[0].Rows[0]["groupname"].ToString();
                if (ds.Tables[0].Rows[0]["sort"].ToString() != "")
                {
                    model.sort = int.Parse(ds.Tables[0].Rows[0]["sort"].ToString());
                }
                if (ds.Tables[0].Rows[0]["status"].ToString() != "")
                {
                    model.status = int.Parse(ds.Tables[0].Rows[0]["status"].ToString());
                }
                if (ds.Tables[0].Rows[0]["siteid"].ToString() != "")
                {
                    model.siteid = int.Parse(ds.Tables[0].Rows[0]["siteid"].ToString());
                }
                if (ds.Tables[0].Rows[0]["hospatilid"].ToString() != "")
                {
                    model.hospatilid = int.Parse(ds.Tables[0].Rows[0]["hospatilid"].ToString());
                }
                model.description = ds.Tables[0].Rows[0]["description"].ToString();
                model.leader_desc = ds.Tables[0].Rows[0]["leader_desc"].ToString();
                model.logo_url = ds.Tables[0].Rows[0]["logo_url"].ToString();

                return model;
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * ");
            strSql.Append(" FROM yjr_doctor_group ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" * ");
            strSql.Append(" FROM yjr_doctor_group ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得查询分页数据
        /// </summary>
        public DataSet GetList(int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * FROM View_doctor_group ");// 修改为视图
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            recordCount = Convert.ToInt32(DbHelperSQL.GetSingle(PagingHelper.CreateCountingSql(strSql.ToString())));
            return DbHelperSQL.Query(PagingHelper.CreatePagingSql(recordCount, pageSize, pageIndex, strSql.ToString(), filedOrder));
        }


    }
}

