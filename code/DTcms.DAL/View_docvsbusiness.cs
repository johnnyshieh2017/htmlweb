﻿using System; 
using System.Text;
using System.Data.SqlClient;
using System.Collections.Generic; 
using System.Data;
using DTcms.DBUtility;
using DTcms.Common;
namespace DTcms.DAL  
{
	 	//View_docvsbusiness
		public partial class View_docvsbusiness
	{
   		     
		public bool Exists()
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from View_docvsbusiness");
			strSql.Append(" where ");
						SqlParameter[] parameters = {
			};

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}
		
				
		
		/// <summary>
		/// 增加一条数据
		/// </summary>
		public void Add(DTcms.Model.View_docvsbusiness model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into View_docvsbusiness(");			
            strSql.Append("id,doctorid,businessid,add_time,cost_type,cost_interval,price,business_name,hospitalname,name,mobile,idcard,status,business_intro");
			strSql.Append(") values (");
            strSql.Append("@id,@doctorid,@businessid,@add_time,@cost_type,@cost_interval,@price,@business_name,@hospitalname,@name,@mobile,@idcard,@status,@business_intro");            
            strSql.Append(") ");            
            		
			SqlParameter[] parameters = {
			            new SqlParameter("@id", SqlDbType.Int,4) ,            
                        new SqlParameter("@doctorid", SqlDbType.Int,4) ,            
                        new SqlParameter("@businessid", SqlDbType.Int,4) ,            
                        new SqlParameter("@add_time", SqlDbType.DateTime) ,            
                        new SqlParameter("@cost_type", SqlDbType.Int,4) ,            
                        new SqlParameter("@cost_interval", SqlDbType.Int,4) ,            
                        new SqlParameter("@price", SqlDbType.Int,4) ,            
                        new SqlParameter("@business_name", SqlDbType.NVarChar,100) ,            
                        new SqlParameter("@hospitalname", SqlDbType.NVarChar,50) ,            
                        new SqlParameter("@name", SqlDbType.VarChar,50) ,            
                        new SqlParameter("@mobile", SqlDbType.VarChar,50) ,            
                        new SqlParameter("@idcard", SqlDbType.VarChar,50) ,            
                        new SqlParameter("@status", SqlDbType.Int,4) ,            
                        new SqlParameter("@business_intro", SqlDbType.NVarChar,-1)             
              
            };
			            
            parameters[0].Value = model.id;                        
            parameters[1].Value = model.doctorid;                        
            parameters[2].Value = model.businessid;                        
            parameters[3].Value = model.add_time;                        
            parameters[4].Value = model.cost_type;                        
            parameters[5].Value = model.cost_interval;                        
            parameters[6].Value = model.price;                        
            parameters[7].Value = model.business_name;                        
            parameters[8].Value = model.hospitalname;                        
            parameters[9].Value = model.name;                        
            parameters[10].Value = model.mobile;                        
            parameters[11].Value = model.idcard;                        
            parameters[12].Value = model.status;                        
            parameters[13].Value = model.business_intro;                        
			            DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
            			
		}
		
		
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTcms.Model.View_docvsbusiness model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update View_docvsbusiness set ");
			                        
            strSql.Append(" id = @id , ");                                    
            strSql.Append(" doctorid = @doctorid , ");                                    
            strSql.Append(" businessid = @businessid , ");                                    
            strSql.Append(" add_time = @add_time , ");                                    
            strSql.Append(" cost_type = @cost_type , ");                                    
            strSql.Append(" cost_interval = @cost_interval , ");                                    
            strSql.Append(" price = @price , ");                                    
            strSql.Append(" business_name = @business_name , ");                                    
            strSql.Append(" hospitalname = @hospitalname , ");                                    
            strSql.Append(" name = @name , ");                                    
            strSql.Append(" mobile = @mobile , ");                                    
            strSql.Append(" idcard = @idcard , ");                                    
            strSql.Append(" status = @status , ");                                    
            strSql.Append(" business_intro = @business_intro  ");            			
			strSql.Append(" where  ");
						
SqlParameter[] parameters = {
			            new SqlParameter("@id", SqlDbType.Int,4) ,            
                        new SqlParameter("@doctorid", SqlDbType.Int,4) ,            
                        new SqlParameter("@businessid", SqlDbType.Int,4) ,            
                        new SqlParameter("@add_time", SqlDbType.DateTime) ,            
                        new SqlParameter("@cost_type", SqlDbType.Int,4) ,            
                        new SqlParameter("@cost_interval", SqlDbType.Int,4) ,            
                        new SqlParameter("@price", SqlDbType.Int,4) ,            
                        new SqlParameter("@business_name", SqlDbType.NVarChar,100) ,            
                        new SqlParameter("@hospitalname", SqlDbType.NVarChar,50) ,            
                        new SqlParameter("@name", SqlDbType.VarChar,50) ,            
                        new SqlParameter("@mobile", SqlDbType.VarChar,50) ,            
                        new SqlParameter("@idcard", SqlDbType.VarChar,50) ,            
                        new SqlParameter("@status", SqlDbType.Int,4) ,            
                        new SqlParameter("@business_intro", SqlDbType.NVarChar,-1)             
              
            };
						            
            parameters[0].Value = model.id;                        
            parameters[1].Value = model.doctorid;                        
            parameters[2].Value = model.businessid;                        
            parameters[3].Value = model.add_time;                        
            parameters[4].Value = model.cost_type;                        
            parameters[5].Value = model.cost_interval;                        
            parameters[6].Value = model.price;                        
            parameters[7].Value = model.business_name;                        
            parameters[8].Value = model.hospitalname;                        
            parameters[9].Value = model.name;                        
            parameters[10].Value = model.mobile;                        
            parameters[11].Value = model.idcard;                        
            parameters[12].Value = model.status;                        
            parameters[13].Value = model.business_intro;                        
            int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete()
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from View_docvsbusiness ");
			strSql.Append(" where ");
						SqlParameter[] parameters = {
			};


			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
				
		
		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.View_docvsbusiness GetModel()
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select id, doctorid, businessid, add_time, cost_type, cost_interval, price, business_name, hospitalname, name, mobile, idcard, status, business_intro  ");			
			strSql.Append("  from View_docvsbusiness ");
			strSql.Append(" where ");
						SqlParameter[] parameters = {
			};

			
			DTcms.Model.View_docvsbusiness model=new DTcms.Model.View_docvsbusiness();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			
			if(ds.Tables[0].Rows.Count>0)
			{
												if(ds.Tables[0].Rows[0]["id"].ToString()!="")
				{
					model.id=int.Parse(ds.Tables[0].Rows[0]["id"].ToString());
				}
																																if(ds.Tables[0].Rows[0]["doctorid"].ToString()!="")
				{
					model.doctorid=int.Parse(ds.Tables[0].Rows[0]["doctorid"].ToString());
				}
																																if(ds.Tables[0].Rows[0]["businessid"].ToString()!="")
				{
					model.businessid=int.Parse(ds.Tables[0].Rows[0]["businessid"].ToString());
				}
																																if(ds.Tables[0].Rows[0]["add_time"].ToString()!="")
				{
					model.add_time=DateTime.Parse(ds.Tables[0].Rows[0]["add_time"].ToString());
				}
																																if(ds.Tables[0].Rows[0]["cost_type"].ToString()!="")
				{
					model.cost_type=int.Parse(ds.Tables[0].Rows[0]["cost_type"].ToString());
				}
																																if(ds.Tables[0].Rows[0]["cost_interval"].ToString()!="")
				{
					model.cost_interval=int.Parse(ds.Tables[0].Rows[0]["cost_interval"].ToString());
				}
																																if(ds.Tables[0].Rows[0]["price"].ToString()!="")
				{
					model.price=int.Parse(ds.Tables[0].Rows[0]["price"].ToString());
				}
																																				model.business_name= ds.Tables[0].Rows[0]["business_name"].ToString();
																																model.hospitalname= ds.Tables[0].Rows[0]["hospitalname"].ToString();
																																model.name= ds.Tables[0].Rows[0]["name"].ToString();
																																model.mobile= ds.Tables[0].Rows[0]["mobile"].ToString();
																																model.idcard= ds.Tables[0].Rows[0]["idcard"].ToString();
																												if(ds.Tables[0].Rows[0]["status"].ToString()!="")
				{
					model.status=int.Parse(ds.Tables[0].Rows[0]["status"].ToString());
				}
																																				model.business_intro= ds.Tables[0].Rows[0]["business_intro"].ToString();
																										
				return model;
			}
			else
			{
				return null;
			}
		}
		
		
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select * ");
			strSql.Append(" FROM View_docvsbusiness ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}
		
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" * ");
			strSql.Append(" FROM View_docvsbusiness ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得查询分页数据
		/// </summary>
		public DataSet GetList(int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
		 {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * FROM View_docvsbusiness ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            recordCount = Convert.ToInt32(DbHelperSQL.GetSingle(PagingHelper.CreateCountingSql(strSql.ToString())));
            return DbHelperSQL.Query(PagingHelper.CreatePagingSql(recordCount, pageSize, pageIndex, strSql.ToString(), filedOrder));
		}

   
	}
}

