﻿/**  版本信息模板在安装目录下，可自行修改。
* vip_email.cs
*
* 功 能： N/A
* 类 名： vip_email
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2020/12/29 10:57:33   N/A    初版
*
* Copyright (c) 2019  Corporation. All rights reserved.
*/
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DTcms.DBUtility;//Please add references
namespace DTcms.DAL
{
	/// <summary>
	/// 数据访问类:vip_email
	/// </summary>
	public partial class vip_email
	{
		public vip_email()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("id", "vip_email"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from vip_email");
			strSql.Append(" where id=@id");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
			parameters[0].Value = id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(DTcms.Model.vip_email model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into vip_email(");
			strSql.Append("mail_type,mail_status,add_time,mail_title,mail_content,templateid,mail_to,send_date,complate_date,mail_ext)");
			strSql.Append(" values (");
			strSql.Append("@mail_type,@mail_status,@add_time,@mail_title,@mail_content,@templateid,@mail_to,@send_date,@complate_date,@mail_ext)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@mail_type", SqlDbType.Int,4),
					new SqlParameter("@mail_status", SqlDbType.Int,4),
					new SqlParameter("@add_time", SqlDbType.DateTime),
					new SqlParameter("@mail_title", SqlDbType.VarChar,50),
					new SqlParameter("@mail_content", SqlDbType.VarChar,500),
					new SqlParameter("@templateid", SqlDbType.VarChar,50),
					new SqlParameter("@mail_to", SqlDbType.VarChar,100),
					new SqlParameter("@send_date", SqlDbType.DateTime),
					new SqlParameter("@complate_date", SqlDbType.DateTime),
					new SqlParameter("@mail_ext", SqlDbType.VarChar,50)};
			parameters[0].Value = model.mail_type;
			parameters[1].Value = model.mail_status;
			parameters[2].Value = model.add_time;
			parameters[3].Value = model.mail_title;
			parameters[4].Value = model.mail_content;
			parameters[5].Value = model.templateid;
			parameters[6].Value = model.mail_to;
			parameters[7].Value = model.send_date;
			parameters[8].Value = model.complate_date;
			parameters[9].Value = model.mail_ext;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTcms.Model.vip_email model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update vip_email set ");
			strSql.Append("mail_type=@mail_type,");
			strSql.Append("mail_status=@mail_status,");
			strSql.Append("add_time=@add_time,");
			strSql.Append("mail_title=@mail_title,");
			strSql.Append("mail_content=@mail_content,");
			strSql.Append("templateid=@templateid,");
			strSql.Append("mail_to=@mail_to,");
			strSql.Append("send_date=@send_date,");
			strSql.Append("complate_date=@complate_date,");
			strSql.Append("mail_ext=@mail_ext");
			strSql.Append(" where id=@id");
			SqlParameter[] parameters = {
					new SqlParameter("@mail_type", SqlDbType.Int,4),
					new SqlParameter("@mail_status", SqlDbType.Int,4),
					new SqlParameter("@add_time", SqlDbType.DateTime),
					new SqlParameter("@mail_title", SqlDbType.VarChar,50),
					new SqlParameter("@mail_content", SqlDbType.VarChar,500),
					new SqlParameter("@templateid", SqlDbType.VarChar,50),
					new SqlParameter("@mail_to", SqlDbType.VarChar,100),
					new SqlParameter("@send_date", SqlDbType.DateTime),
					new SqlParameter("@complate_date", SqlDbType.DateTime),
					new SqlParameter("@mail_ext", SqlDbType.VarChar,50),
					new SqlParameter("@id", SqlDbType.Int,4)};
			parameters[0].Value = model.mail_type;
			parameters[1].Value = model.mail_status;
			parameters[2].Value = model.add_time;
			parameters[3].Value = model.mail_title;
			parameters[4].Value = model.mail_content;
			parameters[5].Value = model.templateid;
			parameters[6].Value = model.mail_to;
			parameters[7].Value = model.send_date;
			parameters[8].Value = model.complate_date;
			parameters[9].Value = model.mail_ext;
			parameters[10].Value = model.id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from vip_email ");
			strSql.Append(" where id=@id");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
			parameters[0].Value = id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from vip_email ");
			strSql.Append(" where id in ("+idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.vip_email GetModel(int id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 id,mail_type,mail_status,add_time,mail_title,mail_content,templateid,mail_to,send_date,complate_date,mail_ext from vip_email ");
			strSql.Append(" where id=@id");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
			parameters[0].Value = id;

			DTcms.Model.vip_email model=new DTcms.Model.vip_email();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.vip_email DataRowToModel(DataRow row)
		{
			DTcms.Model.vip_email model=new DTcms.Model.vip_email();
			if (row != null)
			{
				if(row["id"]!=null && row["id"].ToString()!="")
				{
					model.id=int.Parse(row["id"].ToString());
				}
				if(row["mail_type"]!=null && row["mail_type"].ToString()!="")
				{
					model.mail_type=int.Parse(row["mail_type"].ToString());
				}
				if(row["mail_status"]!=null && row["mail_status"].ToString()!="")
				{
					model.mail_status=int.Parse(row["mail_status"].ToString());
				}
				if(row["add_time"]!=null && row["add_time"].ToString()!="")
				{
					model.add_time=DateTime.Parse(row["add_time"].ToString());
				}
				if(row["mail_title"]!=null)
				{
					model.mail_title=row["mail_title"].ToString();
				}
				if(row["mail_content"]!=null)
				{
					model.mail_content=row["mail_content"].ToString();
				}
				if(row["templateid"]!=null)
				{
					model.templateid=row["templateid"].ToString();
				}
				if(row["mail_to"]!=null)
				{
					model.mail_to=row["mail_to"].ToString();
				}
				if(row["send_date"]!=null && row["send_date"].ToString()!="")
				{
					model.send_date=DateTime.Parse(row["send_date"].ToString());
				}
				if(row["complate_date"]!=null && row["complate_date"].ToString()!="")
				{
					model.complate_date=DateTime.Parse(row["complate_date"].ToString());
				}
				if(row["mail_ext"]!=null)
				{
					model.mail_ext=row["mail_ext"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select id,mail_type,mail_status,add_time,mail_title,mail_content,templateid,mail_to,send_date,complate_date,mail_ext ");
			strSql.Append(" FROM vip_email ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" id,mail_type,mail_status,add_time,mail_title,mail_content,templateid,mail_to,send_date,complate_date,mail_ext ");
			strSql.Append(" FROM vip_email ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM vip_email ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.id desc");
			}
			strSql.Append(")AS Row, T.*  from vip_email T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "vip_email";
			parameters[1].Value = "id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

