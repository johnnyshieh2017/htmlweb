﻿/**  版本信息模板在安装目录下，可自行修改。
* ky_applyproject_jixiao.cs
*
* 功 能： N/A
* 类 名： ky_applyproject_jixiao
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2021/7/27 10:34:24   N/A    初版
*
　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DTcms.DBUtility;//Please add references
using DTcms.Common;

namespace DTcms.DAL
{
	/// <summary>
	/// 数据访问类:ky_applyproject_jixiao
	/// </summary>
	public partial class ky_applyproject_jixiao
	{
		public ky_applyproject_jixiao()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("id", "ky_applyproject_jixiao"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from ky_applyproject_jixiao");
			strSql.Append(" where id=@id");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
			parameters[0].Value = id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(DTcms.Model.ky_applyproject_jixiao model)
		{
			StringBuilder strSql = new StringBuilder();
			strSql.Append("insert into ky_applyproject_jixiao(");
			strSql.Append("pa_id,zhuanli_shuliang,zhuanli_shuliang_faming,zhuanli_shuliang_shiyongxinxing,zhuanli_shuliang_waiguan,zhuanli_shouquan_shuliang,zhuanli_shouquan_faming,zhuanli_shouquan_shiyongxinxing,zhuanli_shouquan_waiguan,ruanzhu_shuliang,lunwen_shuliang,lunwen_shuliang_sci,lunwen_shuliang_ei,zhuzuo_shuliang,biaozhun_shuliang,biaozhun_shuliang_guoji,biaozhun_shuliang_guojia,biaozhun_shuliang_hangye,biaozhun_shuliang_difang,biaozhun_shuliang_qiye,qita_tianbujishukongbai_shuliang,qita_tianbujishukongbai_shuliang_guoji,qita_tianbujishukongbai_shuliang_guojia,qita_tianbujishukongbai_shuliang_shengji,qita_huojiang_shuliang,qita_huojiang_shuliang_guojia,qita_huojiang_shuliang_sheng,qita_huojiang_shuliang_difang,qita_qita_xingongyi,qita_qita_xinchanping,qita_qita_xincailiao,qita_qita_xinzhuangbei,qita_qita_pingtai,qita_qita_kejichengguo,rencai_yj_shuliang,rencai_yj_shuliang_boshi,rencai_yj_shuliang_shuoshi,jingjixiaoyi,shehuixiaoyi,qitashuoming,add_time,add_user,status,rencai_py_shuliang,rencai_py_shuliang_boshi,rencai_py_shuliang_shuoshi)");
			strSql.Append(" values (");
			strSql.Append("@pa_id,@zhuanli_shuliang,@zhuanli_shuliang_faming,@zhuanli_shuliang_shiyongxinxing,@zhuanli_shuliang_waiguan,@zhuanli_shouquan_shuliang,@zhuanli_shouquan_faming,@zhuanli_shouquan_shiyongxinxing,@zhuanli_shouquan_waiguan,@ruanzhu_shuliang,@lunwen_shuliang,@lunwen_shuliang_sci,@lunwen_shuliang_ei,@zhuzuo_shuliang,@biaozhun_shuliang,@biaozhun_shuliang_guoji,@biaozhun_shuliang_guojia,@biaozhun_shuliang_hangye,@biaozhun_shuliang_difang,@biaozhun_shuliang_qiye,@qita_tianbujishukongbai_shuliang,@qita_tianbujishukongbai_shuliang_guoji,@qita_tianbujishukongbai_shuliang_guojia,@qita_tianbujishukongbai_shuliang_shengji,@qita_huojiang_shuliang,@qita_huojiang_shuliang_guojia,@qita_huojiang_shuliang_sheng,@qita_huojiang_shuliang_difang,@qita_qita_xingongyi,@qita_qita_xinchanping,@qita_qita_xincailiao,@qita_qita_xinzhuangbei,@qita_qita_pingtai,@qita_qita_kejichengguo,@rencai_yj_shuliang,@rencai_yj_shuliang_boshi,@rencai_yj_shuliang_shuoshi,@jingjixiaoyi,@shehuixiaoyi,@qitashuoming,@add_time,@add_user,@status,@rencai_py_shuliang,@rencai_py_shuliang_boshi,@rencai_py_shuliang_shuoshi)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@pa_id", SqlDbType.VarChar,50),
					new SqlParameter("@zhuanli_shuliang", SqlDbType.VarChar,50),
					new SqlParameter("@zhuanli_shuliang_faming", SqlDbType.VarChar,50),
					new SqlParameter("@zhuanli_shuliang_shiyongxinxing", SqlDbType.VarChar,50),
					new SqlParameter("@zhuanli_shuliang_waiguan", SqlDbType.VarChar,50),
					new SqlParameter("@zhuanli_shouquan_shuliang", SqlDbType.VarChar,50),
					new SqlParameter("@zhuanli_shouquan_faming", SqlDbType.VarChar,50),
					new SqlParameter("@zhuanli_shouquan_shiyongxinxing", SqlDbType.VarChar,50),
					new SqlParameter("@zhuanli_shouquan_waiguan", SqlDbType.VarChar,50),
					new SqlParameter("@ruanzhu_shuliang", SqlDbType.VarChar,50),
					new SqlParameter("@lunwen_shuliang", SqlDbType.VarChar,50),
					new SqlParameter("@lunwen_shuliang_sci", SqlDbType.VarChar,50),
					new SqlParameter("@lunwen_shuliang_ei", SqlDbType.VarChar,50),
					new SqlParameter("@zhuzuo_shuliang", SqlDbType.VarChar,50),
					new SqlParameter("@biaozhun_shuliang", SqlDbType.VarChar,50),
					new SqlParameter("@biaozhun_shuliang_guoji", SqlDbType.VarChar,50),
					new SqlParameter("@biaozhun_shuliang_guojia", SqlDbType.VarChar,50),
					new SqlParameter("@biaozhun_shuliang_hangye", SqlDbType.VarChar,50),
					new SqlParameter("@biaozhun_shuliang_difang", SqlDbType.VarChar,50),
					new SqlParameter("@biaozhun_shuliang_qiye", SqlDbType.VarChar,50),
					new SqlParameter("@qita_tianbujishukongbai_shuliang", SqlDbType.VarChar,50),
					new SqlParameter("@qita_tianbujishukongbai_shuliang_guoji", SqlDbType.VarChar,50),
					new SqlParameter("@qita_tianbujishukongbai_shuliang_guojia", SqlDbType.VarChar,50),
					new SqlParameter("@qita_tianbujishukongbai_shuliang_shengji", SqlDbType.VarChar,50),
					new SqlParameter("@qita_huojiang_shuliang", SqlDbType.VarChar,50),
					new SqlParameter("@qita_huojiang_shuliang_guojia", SqlDbType.VarChar,50),
					new SqlParameter("@qita_huojiang_shuliang_sheng", SqlDbType.VarChar,50),
					new SqlParameter("@qita_huojiang_shuliang_difang", SqlDbType.VarChar,50),
					new SqlParameter("@qita_qita_xingongyi", SqlDbType.VarChar,50),
					new SqlParameter("@qita_qita_xinchanping", SqlDbType.VarChar,50),
					new SqlParameter("@qita_qita_xincailiao", SqlDbType.VarChar,50),
					new SqlParameter("@qita_qita_xinzhuangbei", SqlDbType.VarChar,50),
					new SqlParameter("@qita_qita_pingtai", SqlDbType.VarChar,50),
					new SqlParameter("@qita_qita_kejichengguo", SqlDbType.VarChar,50),
					new SqlParameter("@rencai_yj_shuliang", SqlDbType.VarChar,50),
					new SqlParameter("@rencai_yj_shuliang_boshi", SqlDbType.VarChar,50),
					new SqlParameter("@rencai_yj_shuliang_shuoshi", SqlDbType.VarChar,50),
					new SqlParameter("@jingjixiaoyi", SqlDbType.VarChar,200),
					new SqlParameter("@shehuixiaoyi", SqlDbType.VarChar,200),
					new SqlParameter("@qitashuoming", SqlDbType.VarChar,500),
					new SqlParameter("@add_time", SqlDbType.DateTime),
					new SqlParameter("@add_user", SqlDbType.VarChar,50),
					new SqlParameter("@status", SqlDbType.Int,4),
					new SqlParameter("@rencai_py_shuliang", SqlDbType.VarChar,50),
					new SqlParameter("@rencai_py_shuliang_boshi", SqlDbType.VarChar,50),
					new SqlParameter("@rencai_py_shuliang_shuoshi", SqlDbType.VarChar,50)};
			parameters[0].Value = model.pa_id;
			parameters[1].Value = model.zhuanli_shuliang;
			parameters[2].Value = model.zhuanli_shuliang_faming;
			parameters[3].Value = model.zhuanli_shuliang_shiyongxinxing;
			parameters[4].Value = model.zhuanli_shuliang_waiguan;
			parameters[5].Value = model.zhuanli_shouquan_shuliang;
			parameters[6].Value = model.zhuanli_shouquan_faming;
			parameters[7].Value = model.zhuanli_shouquan_shiyongxinxing;
			parameters[8].Value = model.zhuanli_shouquan_waiguan;
			parameters[9].Value = model.ruanzhu_shuliang;
			parameters[10].Value = model.lunwen_shuliang;
			parameters[11].Value = model.lunwen_shuliang_sci;
			parameters[12].Value = model.lunwen_shuliang_ei;
			parameters[13].Value = model.zhuzuo_shuliang;
			parameters[14].Value = model.biaozhun_shuliang;
			parameters[15].Value = model.biaozhun_shuliang_guoji;
			parameters[16].Value = model.biaozhun_shuliang_guojia;
			parameters[17].Value = model.biaozhun_shuliang_hangye;
			parameters[18].Value = model.biaozhun_shuliang_difang;
			parameters[19].Value = model.biaozhun_shuliang_qiye;
			parameters[20].Value = model.qita_tianbujishukongbai_shuliang;
			parameters[21].Value = model.qita_tianbujishukongbai_shuliang_guoji;
			parameters[22].Value = model.qita_tianbujishukongbai_shuliang_guojia;
			parameters[23].Value = model.qita_tianbujishukongbai_shuliang_shengji;
			parameters[24].Value = model.qita_huojiang_shuliang;
			parameters[25].Value = model.qita_huojiang_shuliang_guojia;
			parameters[26].Value = model.qita_huojiang_shuliang_sheng;
			parameters[27].Value = model.qita_huojiang_shuliang_difang;
			parameters[28].Value = model.qita_qita_xingongyi;
			parameters[29].Value = model.qita_qita_xinchanping;
			parameters[30].Value = model.qita_qita_xincailiao;
			parameters[31].Value = model.qita_qita_xinzhuangbei;
			parameters[32].Value = model.qita_qita_pingtai;
			parameters[33].Value = model.qita_qita_kejichengguo;
			parameters[34].Value = model.rencai_yj_shuliang;
			parameters[35].Value = model.rencai_yj_shuliang_boshi;
			parameters[36].Value = model.rencai_yj_shuliang_shuoshi;
			parameters[37].Value = model.jingjixiaoyi;
			parameters[38].Value = model.shehuixiaoyi;
			parameters[39].Value = model.qitashuoming;
			parameters[40].Value = model.add_time;
			parameters[41].Value = model.add_user;
			parameters[42].Value = model.status;
			parameters[43].Value = model.rencai_py_shuliang;
			parameters[44].Value = model.rencai_py_shuliang_boshi;
			parameters[45].Value = model.rencai_py_shuliang_shuoshi;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTcms.Model.ky_applyproject_jixiao model)
		{
			StringBuilder strSql = new StringBuilder();
			strSql.Append("update ky_applyproject_jixiao set ");
			strSql.Append("pa_id=@pa_id,");
			strSql.Append("zhuanli_shuliang=@zhuanli_shuliang,");
			strSql.Append("zhuanli_shuliang_faming=@zhuanli_shuliang_faming,");
			strSql.Append("zhuanli_shuliang_shiyongxinxing=@zhuanli_shuliang_shiyongxinxing,");
			strSql.Append("zhuanli_shuliang_waiguan=@zhuanli_shuliang_waiguan,");
			strSql.Append("zhuanli_shouquan_shuliang=@zhuanli_shouquan_shuliang,");
			strSql.Append("zhuanli_shouquan_faming=@zhuanli_shouquan_faming,");
			strSql.Append("zhuanli_shouquan_shiyongxinxing=@zhuanli_shouquan_shiyongxinxing,");
			strSql.Append("zhuanli_shouquan_waiguan=@zhuanli_shouquan_waiguan,");
			strSql.Append("ruanzhu_shuliang=@ruanzhu_shuliang,");
			strSql.Append("lunwen_shuliang=@lunwen_shuliang,");
			strSql.Append("lunwen_shuliang_sci=@lunwen_shuliang_sci,");
			strSql.Append("lunwen_shuliang_ei=@lunwen_shuliang_ei,");
			strSql.Append("zhuzuo_shuliang=@zhuzuo_shuliang,");
			strSql.Append("biaozhun_shuliang=@biaozhun_shuliang,");
			strSql.Append("biaozhun_shuliang_guoji=@biaozhun_shuliang_guoji,");
			strSql.Append("biaozhun_shuliang_guojia=@biaozhun_shuliang_guojia,");
			strSql.Append("biaozhun_shuliang_hangye=@biaozhun_shuliang_hangye,");
			strSql.Append("biaozhun_shuliang_difang=@biaozhun_shuliang_difang,");
			strSql.Append("biaozhun_shuliang_qiye=@biaozhun_shuliang_qiye,");
			strSql.Append("qita_tianbujishukongbai_shuliang=@qita_tianbujishukongbai_shuliang,");
			strSql.Append("qita_tianbujishukongbai_shuliang_guoji=@qita_tianbujishukongbai_shuliang_guoji,");
			strSql.Append("qita_tianbujishukongbai_shuliang_guojia=@qita_tianbujishukongbai_shuliang_guojia,");
			strSql.Append("qita_tianbujishukongbai_shuliang_shengji=@qita_tianbujishukongbai_shuliang_shengji,");
			strSql.Append("qita_huojiang_shuliang=@qita_huojiang_shuliang,");
			strSql.Append("qita_huojiang_shuliang_guojia=@qita_huojiang_shuliang_guojia,");
			strSql.Append("qita_huojiang_shuliang_sheng=@qita_huojiang_shuliang_sheng,");
			strSql.Append("qita_huojiang_shuliang_difang=@qita_huojiang_shuliang_difang,");
			strSql.Append("qita_qita_xingongyi=@qita_qita_xingongyi,");
			strSql.Append("qita_qita_xinchanping=@qita_qita_xinchanping,");
			strSql.Append("qita_qita_xincailiao=@qita_qita_xincailiao,");
			strSql.Append("qita_qita_xinzhuangbei=@qita_qita_xinzhuangbei,");
			strSql.Append("qita_qita_pingtai=@qita_qita_pingtai,");
			strSql.Append("qita_qita_kejichengguo=@qita_qita_kejichengguo,");
			strSql.Append("rencai_yj_shuliang=@rencai_yj_shuliang,");
			strSql.Append("rencai_yj_shuliang_boshi=@rencai_yj_shuliang_boshi,");
			strSql.Append("rencai_yj_shuliang_shuoshi=@rencai_yj_shuliang_shuoshi,");
			strSql.Append("jingjixiaoyi=@jingjixiaoyi,");
			strSql.Append("shehuixiaoyi=@shehuixiaoyi,");
			strSql.Append("qitashuoming=@qitashuoming,");
			strSql.Append("add_time=@add_time,");
			strSql.Append("add_user=@add_user,");
			strSql.Append("status=@status,");
			strSql.Append("rencai_py_shuliang=@rencai_py_shuliang,");
			strSql.Append("rencai_py_shuliang_boshi=@rencai_py_shuliang_boshi,");
			strSql.Append("rencai_py_shuliang_shuoshi=@rencai_py_shuliang_shuoshi");
			strSql.Append(" where id=@id");
			SqlParameter[] parameters = {
					new SqlParameter("@pa_id", SqlDbType.VarChar,50),
					new SqlParameter("@zhuanli_shuliang", SqlDbType.VarChar,50),
					new SqlParameter("@zhuanli_shuliang_faming", SqlDbType.VarChar,50),
					new SqlParameter("@zhuanli_shuliang_shiyongxinxing", SqlDbType.VarChar,50),
					new SqlParameter("@zhuanli_shuliang_waiguan", SqlDbType.VarChar,50),
					new SqlParameter("@zhuanli_shouquan_shuliang", SqlDbType.VarChar,50),
					new SqlParameter("@zhuanli_shouquan_faming", SqlDbType.VarChar,50),
					new SqlParameter("@zhuanli_shouquan_shiyongxinxing", SqlDbType.VarChar,50),
					new SqlParameter("@zhuanli_shouquan_waiguan", SqlDbType.VarChar,50),
					new SqlParameter("@ruanzhu_shuliang", SqlDbType.VarChar,50),
					new SqlParameter("@lunwen_shuliang", SqlDbType.VarChar,50),
					new SqlParameter("@lunwen_shuliang_sci", SqlDbType.VarChar,50),
					new SqlParameter("@lunwen_shuliang_ei", SqlDbType.VarChar,50),
					new SqlParameter("@zhuzuo_shuliang", SqlDbType.VarChar,50),
					new SqlParameter("@biaozhun_shuliang", SqlDbType.VarChar,50),
					new SqlParameter("@biaozhun_shuliang_guoji", SqlDbType.VarChar,50),
					new SqlParameter("@biaozhun_shuliang_guojia", SqlDbType.VarChar,50),
					new SqlParameter("@biaozhun_shuliang_hangye", SqlDbType.VarChar,50),
					new SqlParameter("@biaozhun_shuliang_difang", SqlDbType.VarChar,50),
					new SqlParameter("@biaozhun_shuliang_qiye", SqlDbType.VarChar,50),
					new SqlParameter("@qita_tianbujishukongbai_shuliang", SqlDbType.VarChar,50),
					new SqlParameter("@qita_tianbujishukongbai_shuliang_guoji", SqlDbType.VarChar,50),
					new SqlParameter("@qita_tianbujishukongbai_shuliang_guojia", SqlDbType.VarChar,50),
					new SqlParameter("@qita_tianbujishukongbai_shuliang_shengji", SqlDbType.VarChar,50),
					new SqlParameter("@qita_huojiang_shuliang", SqlDbType.VarChar,50),
					new SqlParameter("@qita_huojiang_shuliang_guojia", SqlDbType.VarChar,50),
					new SqlParameter("@qita_huojiang_shuliang_sheng", SqlDbType.VarChar,50),
					new SqlParameter("@qita_huojiang_shuliang_difang", SqlDbType.VarChar,50),
					new SqlParameter("@qita_qita_xingongyi", SqlDbType.VarChar,50),
					new SqlParameter("@qita_qita_xinchanping", SqlDbType.VarChar,50),
					new SqlParameter("@qita_qita_xincailiao", SqlDbType.VarChar,50),
					new SqlParameter("@qita_qita_xinzhuangbei", SqlDbType.VarChar,50),
					new SqlParameter("@qita_qita_pingtai", SqlDbType.VarChar,50),
					new SqlParameter("@qita_qita_kejichengguo", SqlDbType.VarChar,50),
					new SqlParameter("@rencai_yj_shuliang", SqlDbType.VarChar,50),
					new SqlParameter("@rencai_yj_shuliang_boshi", SqlDbType.VarChar,50),
					new SqlParameter("@rencai_yj_shuliang_shuoshi", SqlDbType.VarChar,50),
					new SqlParameter("@jingjixiaoyi", SqlDbType.VarChar,200),
					new SqlParameter("@shehuixiaoyi", SqlDbType.VarChar,200),
					new SqlParameter("@qitashuoming", SqlDbType.VarChar,500),
					new SqlParameter("@add_time", SqlDbType.DateTime),
					new SqlParameter("@add_user", SqlDbType.VarChar,50),
					new SqlParameter("@status", SqlDbType.Int,4),
					new SqlParameter("@rencai_py_shuliang", SqlDbType.VarChar,50),
					new SqlParameter("@rencai_py_shuliang_boshi", SqlDbType.VarChar,50),
					new SqlParameter("@rencai_py_shuliang_shuoshi", SqlDbType.VarChar,50),
					new SqlParameter("@id", SqlDbType.Int,4)};
			parameters[0].Value = model.pa_id;
			parameters[1].Value = model.zhuanli_shuliang;
			parameters[2].Value = model.zhuanli_shuliang_faming;
			parameters[3].Value = model.zhuanli_shuliang_shiyongxinxing;
			parameters[4].Value = model.zhuanli_shuliang_waiguan;
			parameters[5].Value = model.zhuanli_shouquan_shuliang;
			parameters[6].Value = model.zhuanli_shouquan_faming;
			parameters[7].Value = model.zhuanli_shouquan_shiyongxinxing;
			parameters[8].Value = model.zhuanli_shouquan_waiguan;
			parameters[9].Value = model.ruanzhu_shuliang;
			parameters[10].Value = model.lunwen_shuliang;
			parameters[11].Value = model.lunwen_shuliang_sci;
			parameters[12].Value = model.lunwen_shuliang_ei;
			parameters[13].Value = model.zhuzuo_shuliang;
			parameters[14].Value = model.biaozhun_shuliang;
			parameters[15].Value = model.biaozhun_shuliang_guoji;
			parameters[16].Value = model.biaozhun_shuliang_guojia;
			parameters[17].Value = model.biaozhun_shuliang_hangye;
			parameters[18].Value = model.biaozhun_shuliang_difang;
			parameters[19].Value = model.biaozhun_shuliang_qiye;
			parameters[20].Value = model.qita_tianbujishukongbai_shuliang;
			parameters[21].Value = model.qita_tianbujishukongbai_shuliang_guoji;
			parameters[22].Value = model.qita_tianbujishukongbai_shuliang_guojia;
			parameters[23].Value = model.qita_tianbujishukongbai_shuliang_shengji;
			parameters[24].Value = model.qita_huojiang_shuliang;
			parameters[25].Value = model.qita_huojiang_shuliang_guojia;
			parameters[26].Value = model.qita_huojiang_shuliang_sheng;
			parameters[27].Value = model.qita_huojiang_shuliang_difang;
			parameters[28].Value = model.qita_qita_xingongyi;
			parameters[29].Value = model.qita_qita_xinchanping;
			parameters[30].Value = model.qita_qita_xincailiao;
			parameters[31].Value = model.qita_qita_xinzhuangbei;
			parameters[32].Value = model.qita_qita_pingtai;
			parameters[33].Value = model.qita_qita_kejichengguo;
			parameters[34].Value = model.rencai_yj_shuliang;
			parameters[35].Value = model.rencai_yj_shuliang_boshi;
			parameters[36].Value = model.rencai_yj_shuliang_shuoshi;
			parameters[37].Value = model.jingjixiaoyi;
			parameters[38].Value = model.shehuixiaoyi;
			parameters[39].Value = model.qitashuoming;
			parameters[40].Value = model.add_time;
			parameters[41].Value = model.add_user;
			parameters[42].Value = model.status;
			parameters[43].Value = model.rencai_py_shuliang;
			parameters[44].Value = model.rencai_py_shuliang_boshi;
			parameters[45].Value = model.rencai_py_shuliang_shuoshi;
			parameters[46].Value = model.id;

			int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int id)
		{

			StringBuilder strSql = new StringBuilder();
			strSql.Append("delete from ky_applyproject_jixiao ");
			strSql.Append(" where id=@id");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
			parameters[0].Value = id;

			int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string idlist)
		{
			StringBuilder strSql = new StringBuilder();
			strSql.Append("delete from ky_applyproject_jixiao ");
			strSql.Append(" where id in (" + idlist + ")  ");
			int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.ky_applyproject_jixiao GetModel(int id)
		{

			StringBuilder strSql = new StringBuilder();
			strSql.Append("select  top 1 id,pa_id,zhuanli_shuliang,zhuanli_shuliang_faming,zhuanli_shuliang_shiyongxinxing,zhuanli_shuliang_waiguan,zhuanli_shouquan_shuliang,zhuanli_shouquan_faming,zhuanli_shouquan_shiyongxinxing,zhuanli_shouquan_waiguan,ruanzhu_shuliang,lunwen_shuliang,lunwen_shuliang_sci,lunwen_shuliang_ei,zhuzuo_shuliang,biaozhun_shuliang,biaozhun_shuliang_guoji,biaozhun_shuliang_guojia,biaozhun_shuliang_hangye,biaozhun_shuliang_difang,biaozhun_shuliang_qiye,qita_tianbujishukongbai_shuliang,qita_tianbujishukongbai_shuliang_guoji,qita_tianbujishukongbai_shuliang_guojia,qita_tianbujishukongbai_shuliang_shengji,qita_huojiang_shuliang,qita_huojiang_shuliang_guojia,qita_huojiang_shuliang_sheng,qita_huojiang_shuliang_difang,qita_qita_xingongyi,qita_qita_xinchanping,qita_qita_xincailiao,qita_qita_xinzhuangbei,qita_qita_pingtai,qita_qita_kejichengguo,rencai_yj_shuliang,rencai_yj_shuliang_boshi,rencai_yj_shuliang_shuoshi,jingjixiaoyi,shehuixiaoyi,qitashuoming,add_time,add_user,status,rencai_py_shuliang,rencai_py_shuliang_boshi,rencai_py_shuliang_shuoshi from ky_applyproject_jixiao ");
			strSql.Append(" where id=@id");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
			parameters[0].Value = id;

			DTcms.Model.ky_applyproject_jixiao model = new DTcms.Model.ky_applyproject_jixiao();
			DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
			if (ds.Tables[0].Rows.Count > 0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.ky_applyproject_jixiao DataRowToModel(DataRow row)
		{
			DTcms.Model.ky_applyproject_jixiao model = new DTcms.Model.ky_applyproject_jixiao();
			if (row != null)
			{
				if (row["id"] != null && row["id"].ToString() != "")
				{
					model.id = int.Parse(row["id"].ToString());
				}
				if (row["pa_id"] != null)
				{
					model.pa_id = row["pa_id"].ToString();
				}
				if (row["zhuanli_shuliang"] != null)
				{
					model.zhuanli_shuliang = row["zhuanli_shuliang"].ToString();
				}
				if (row["zhuanli_shuliang_faming"] != null)
				{
					model.zhuanli_shuliang_faming = row["zhuanli_shuliang_faming"].ToString();
				}
				if (row["zhuanli_shuliang_shiyongxinxing"] != null)
				{
					model.zhuanli_shuliang_shiyongxinxing = row["zhuanli_shuliang_shiyongxinxing"].ToString();
				}
				if (row["zhuanli_shuliang_waiguan"] != null)
				{
					model.zhuanli_shuliang_waiguan = row["zhuanli_shuliang_waiguan"].ToString();
				}
				if (row["zhuanli_shouquan_shuliang"] != null)
				{
					model.zhuanli_shouquan_shuliang = row["zhuanli_shouquan_shuliang"].ToString();
				}
				if (row["zhuanli_shouquan_faming"] != null)
				{
					model.zhuanli_shouquan_faming = row["zhuanli_shouquan_faming"].ToString();
				}
				if (row["zhuanli_shouquan_shiyongxinxing"] != null)
				{
					model.zhuanli_shouquan_shiyongxinxing = row["zhuanli_shouquan_shiyongxinxing"].ToString();
				}
				if (row["zhuanli_shouquan_waiguan"] != null)
				{
					model.zhuanli_shouquan_waiguan = row["zhuanli_shouquan_waiguan"].ToString();
				}
				if (row["ruanzhu_shuliang"] != null)
				{
					model.ruanzhu_shuliang = row["ruanzhu_shuliang"].ToString();
				}
				if (row["lunwen_shuliang"] != null)
				{
					model.lunwen_shuliang = row["lunwen_shuliang"].ToString();
				}
				if (row["lunwen_shuliang_sci"] != null)
				{
					model.lunwen_shuliang_sci = row["lunwen_shuliang_sci"].ToString();
				}
				if (row["lunwen_shuliang_ei"] != null)
				{
					model.lunwen_shuliang_ei = row["lunwen_shuliang_ei"].ToString();
				}
				if (row["zhuzuo_shuliang"] != null)
				{
					model.zhuzuo_shuliang = row["zhuzuo_shuliang"].ToString();
				}
				if (row["biaozhun_shuliang"] != null)
				{
					model.biaozhun_shuliang = row["biaozhun_shuliang"].ToString();
				}
				if (row["biaozhun_shuliang_guoji"] != null)
				{
					model.biaozhun_shuliang_guoji = row["biaozhun_shuliang_guoji"].ToString();
				}
				if (row["biaozhun_shuliang_guojia"] != null)
				{
					model.biaozhun_shuliang_guojia = row["biaozhun_shuliang_guojia"].ToString();
				}
				if (row["biaozhun_shuliang_hangye"] != null)
				{
					model.biaozhun_shuliang_hangye = row["biaozhun_shuliang_hangye"].ToString();
				}
				if (row["biaozhun_shuliang_difang"] != null)
				{
					model.biaozhun_shuliang_difang = row["biaozhun_shuliang_difang"].ToString();
				}
				if (row["biaozhun_shuliang_qiye"] != null)
				{
					model.biaozhun_shuliang_qiye = row["biaozhun_shuliang_qiye"].ToString();
				}
				if (row["qita_tianbujishukongbai_shuliang"] != null)
				{
					model.qita_tianbujishukongbai_shuliang = row["qita_tianbujishukongbai_shuliang"].ToString();
				}
				if (row["qita_tianbujishukongbai_shuliang_guoji"] != null)
				{
					model.qita_tianbujishukongbai_shuliang_guoji = row["qita_tianbujishukongbai_shuliang_guoji"].ToString();
				}
				if (row["qita_tianbujishukongbai_shuliang_guojia"] != null)
				{
					model.qita_tianbujishukongbai_shuliang_guojia = row["qita_tianbujishukongbai_shuliang_guojia"].ToString();
				}
				if (row["qita_tianbujishukongbai_shuliang_shengji"] != null)
				{
					model.qita_tianbujishukongbai_shuliang_shengji = row["qita_tianbujishukongbai_shuliang_shengji"].ToString();
				}
				if (row["qita_huojiang_shuliang"] != null)
				{
					model.qita_huojiang_shuliang = row["qita_huojiang_shuliang"].ToString();
				}
				if (row["qita_huojiang_shuliang_guojia"] != null)
				{
					model.qita_huojiang_shuliang_guojia = row["qita_huojiang_shuliang_guojia"].ToString();
				}
				if (row["qita_huojiang_shuliang_sheng"] != null)
				{
					model.qita_huojiang_shuliang_sheng = row["qita_huojiang_shuliang_sheng"].ToString();
				}
				if (row["qita_huojiang_shuliang_difang"] != null)
				{
					model.qita_huojiang_shuliang_difang = row["qita_huojiang_shuliang_difang"].ToString();
				}
				if (row["qita_qita_xingongyi"] != null)
				{
					model.qita_qita_xingongyi = row["qita_qita_xingongyi"].ToString();
				}
				if (row["qita_qita_xinchanping"] != null)
				{
					model.qita_qita_xinchanping = row["qita_qita_xinchanping"].ToString();
				}
				if (row["qita_qita_xincailiao"] != null)
				{
					model.qita_qita_xincailiao = row["qita_qita_xincailiao"].ToString();
				}
				if (row["qita_qita_xinzhuangbei"] != null)
				{
					model.qita_qita_xinzhuangbei = row["qita_qita_xinzhuangbei"].ToString();
				}
				if (row["qita_qita_pingtai"] != null)
				{
					model.qita_qita_pingtai = row["qita_qita_pingtai"].ToString();
				}
				if (row["qita_qita_kejichengguo"] != null)
				{
					model.qita_qita_kejichengguo = row["qita_qita_kejichengguo"].ToString();
				}
				if (row["rencai_yj_shuliang"] != null)
				{
					model.rencai_yj_shuliang = row["rencai_yj_shuliang"].ToString();
				}
				if (row["rencai_yj_shuliang_boshi"] != null)
				{
					model.rencai_yj_shuliang_boshi = row["rencai_yj_shuliang_boshi"].ToString();
				}
				if (row["rencai_yj_shuliang_shuoshi"] != null)
				{
					model.rencai_yj_shuliang_shuoshi = row["rencai_yj_shuliang_shuoshi"].ToString();
				}
				if (row["jingjixiaoyi"] != null)
				{
					model.jingjixiaoyi = row["jingjixiaoyi"].ToString();
				}
				if (row["shehuixiaoyi"] != null)
				{
					model.shehuixiaoyi = row["shehuixiaoyi"].ToString();
				}
				if (row["qitashuoming"] != null)
				{
					model.qitashuoming = row["qitashuoming"].ToString();
				}
				if (row["add_time"] != null && row["add_time"].ToString() != "")
				{
					model.add_time = DateTime.Parse(row["add_time"].ToString());
				}
				if (row["add_user"] != null)
				{
					model.add_user = row["add_user"].ToString();
				}
				if (row["status"] != null && row["status"].ToString() != "")
				{
					model.status = int.Parse(row["status"].ToString());
				}
				if (row["rencai_py_shuliang"] != null)
				{
					model.rencai_py_shuliang = row["rencai_py_shuliang"].ToString();
				}
				if (row["rencai_py_shuliang_boshi"] != null)
				{
					model.rencai_py_shuliang_boshi = row["rencai_py_shuliang_boshi"].ToString();
				}
				if (row["rencai_py_shuliang_shuoshi"] != null)
				{
					model.rencai_py_shuliang_shuoshi = row["rencai_py_shuliang_shuoshi"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql = new StringBuilder();
			strSql.Append("select id,pa_id,zhuanli_shuliang,zhuanli_shuliang_faming,zhuanli_shuliang_shiyongxinxing,zhuanli_shuliang_waiguan,zhuanli_shouquan_shuliang,zhuanli_shouquan_faming,zhuanli_shouquan_shiyongxinxing,zhuanli_shouquan_waiguan,ruanzhu_shuliang,lunwen_shuliang,lunwen_shuliang_sci,lunwen_shuliang_ei,zhuzuo_shuliang,biaozhun_shuliang,biaozhun_shuliang_guoji,biaozhun_shuliang_guojia,biaozhun_shuliang_hangye,biaozhun_shuliang_difang,biaozhun_shuliang_qiye,qita_tianbujishukongbai_shuliang,qita_tianbujishukongbai_shuliang_guoji,qita_tianbujishukongbai_shuliang_guojia,qita_tianbujishukongbai_shuliang_shengji,qita_huojiang_shuliang,qita_huojiang_shuliang_guojia,qita_huojiang_shuliang_sheng,qita_huojiang_shuliang_difang,qita_qita_xingongyi,qita_qita_xinchanping,qita_qita_xincailiao,qita_qita_xinzhuangbei,qita_qita_pingtai,qita_qita_kejichengguo,rencai_yj_shuliang,rencai_yj_shuliang_boshi,rencai_yj_shuliang_shuoshi,jingjixiaoyi,shehuixiaoyi,qitashuoming,add_time,add_user,status,rencai_py_shuliang,rencai_py_shuliang_boshi,rencai_py_shuliang_shuoshi ");
			strSql.Append(" FROM ky_applyproject_jixiao ");
			if (strWhere.Trim() != "")
			{
				strSql.Append(" where " + strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top, string strWhere, string filedOrder)
		{
			StringBuilder strSql = new StringBuilder();
			strSql.Append("select ");
			if (Top > 0)
			{
				strSql.Append(" top " + Top.ToString());
			}
			strSql.Append(" id,pa_id,zhuanli_shuliang,zhuanli_shuliang_faming,zhuanli_shuliang_shiyongxinxing,zhuanli_shuliang_waiguan,zhuanli_shouquan_shuliang,zhuanli_shouquan_faming,zhuanli_shouquan_shiyongxinxing,zhuanli_shouquan_waiguan,ruanzhu_shuliang,lunwen_shuliang,lunwen_shuliang_sci,lunwen_shuliang_ei,zhuzuo_shuliang,biaozhun_shuliang,biaozhun_shuliang_guoji,biaozhun_shuliang_guojia,biaozhun_shuliang_hangye,biaozhun_shuliang_difang,biaozhun_shuliang_qiye,qita_tianbujishukongbai_shuliang,qita_tianbujishukongbai_shuliang_guoji,qita_tianbujishukongbai_shuliang_guojia,qita_tianbujishukongbai_shuliang_shengji,qita_huojiang_shuliang,qita_huojiang_shuliang_guojia,qita_huojiang_shuliang_sheng,qita_huojiang_shuliang_difang,qita_qita_xingongyi,qita_qita_xinchanping,qita_qita_xincailiao,qita_qita_xinzhuangbei,qita_qita_pingtai,qita_qita_kejichengguo,rencai_yj_shuliang,rencai_yj_shuliang_boshi,rencai_yj_shuliang_shuoshi,jingjixiaoyi,shehuixiaoyi,qitashuoming,add_time,add_user,status,rencai_py_shuliang,rencai_py_shuliang_boshi,rencai_py_shuliang_shuoshi ");
			strSql.Append(" FROM ky_applyproject_jixiao ");
			if (strWhere.Trim() != "")
			{
				strSql.Append(" where " + strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM ky_applyproject_jixiao ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.id desc");
			}
			strSql.Append(")AS Row, T.*  from ky_applyproject_jixiao T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "ky_applyproject_jixiao";
			parameters[1].Value = "id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod
		public DataSet GetList(int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
		{
			StringBuilder strSql = new StringBuilder();
			strSql.Append("select * FROM ky_applyproject_jixiao ");
			if (strWhere.Trim() != "")
			{
				strSql.Append(" where " + strWhere);
			}
			recordCount = Convert.ToInt32(DbHelperSQL.GetSingle(PagingHelper.CreateCountingSql(strSql.ToString())));
			return DbHelperSQL.Query(PagingHelper.CreatePagingSql(recordCount, pageSize, pageIndex, strSql.ToString(), filedOrder));
		}
		#endregion  ExtensionMethod
	}
}

