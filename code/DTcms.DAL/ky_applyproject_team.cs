﻿/**  版本信息模板在安装目录下，可自行修改。
* ky_applyproject_team.cs
*
* 功 能： N/A
* 类 名： ky_applyproject_team
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2021/7/27 10:34:25   N/A    初版

*/
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DTcms.DBUtility;//Please add references
using DTcms.Common;

namespace DTcms.DAL
{
	/// <summary>
	/// 数据访问类:ky_applyproject_team
	/// </summary>
	public partial class ky_applyproject_team
	{
		public ky_applyproject_team()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("id", "ky_applyproject_team"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from ky_applyproject_team");
			strSql.Append(" where id=@id");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
			parameters[0].Value = id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(DTcms.Model.ky_applyproject_team model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into ky_applyproject_team(");
			strSql.Append("pa_id,name,sex,chusheng,zhuanye,zhicheng,xueli,danwei,fengong,add_time,add_user,status)");
			strSql.Append(" values (");
			strSql.Append("@pa_id,@name,@sex,@chusheng,@zhuanye,@zhicheng,@xueli,@danwei,@fengong,@add_time,@add_user,@status)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@pa_id", SqlDbType.VarChar,50),
					new SqlParameter("@name", SqlDbType.VarChar,50),
					new SqlParameter("@sex", SqlDbType.VarChar,50),
					new SqlParameter("@chusheng", SqlDbType.VarChar,50),
					new SqlParameter("@zhuanye", SqlDbType.VarChar,50),
					new SqlParameter("@zhicheng", SqlDbType.VarChar,50),
					new SqlParameter("@xueli", SqlDbType.VarChar,50),
					new SqlParameter("@danwei", SqlDbType.VarChar,50),
					new SqlParameter("@fengong", SqlDbType.VarChar,50),
					new SqlParameter("@add_time", SqlDbType.DateTime),
					new SqlParameter("@add_user", SqlDbType.VarChar,50),
					new SqlParameter("@status", SqlDbType.Int,4)};
			parameters[0].Value = model.pa_id;
			parameters[1].Value = model.name;
			parameters[2].Value = model.sex;
			parameters[3].Value = model.chusheng;
			parameters[4].Value = model.zhuanye;
			parameters[5].Value = model.zhicheng;
			parameters[6].Value = model.xueli;
			parameters[7].Value = model.danwei;
			parameters[8].Value = model.fengong;
			parameters[9].Value = model.add_time;
			parameters[10].Value = model.add_user;
			parameters[11].Value = model.status;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTcms.Model.ky_applyproject_team model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update ky_applyproject_team set ");
			strSql.Append("pa_id=@pa_id,");
			strSql.Append("name=@name,");
			strSql.Append("sex=@sex,");
			strSql.Append("chusheng=@chusheng,");
			strSql.Append("zhuanye=@zhuanye,");
			strSql.Append("zhicheng=@zhicheng,");
			strSql.Append("xueli=@xueli,");
			strSql.Append("danwei=@danwei,");
			strSql.Append("fengong=@fengong,");
			strSql.Append("add_time=@add_time,");
			strSql.Append("add_user=@add_user,");
			strSql.Append("status=@status");
			strSql.Append(" where id=@id");
			SqlParameter[] parameters = {
					new SqlParameter("@pa_id", SqlDbType.VarChar,50),
					new SqlParameter("@name", SqlDbType.VarChar,50),
					new SqlParameter("@sex", SqlDbType.VarChar,50),
					new SqlParameter("@chusheng", SqlDbType.VarChar,50),
					new SqlParameter("@zhuanye", SqlDbType.VarChar,50),
					new SqlParameter("@zhicheng", SqlDbType.VarChar,50),
					new SqlParameter("@xueli", SqlDbType.VarChar,50),
					new SqlParameter("@danwei", SqlDbType.VarChar,50),
					new SqlParameter("@fengong", SqlDbType.VarChar,50),
					new SqlParameter("@add_time", SqlDbType.DateTime),
					new SqlParameter("@add_user", SqlDbType.VarChar,50),
					new SqlParameter("@status", SqlDbType.Int,4),
					new SqlParameter("@id", SqlDbType.Int,4)};
			parameters[0].Value = model.pa_id;
			parameters[1].Value = model.name;
			parameters[2].Value = model.sex;
			parameters[3].Value = model.chusheng;
			parameters[4].Value = model.zhuanye;
			parameters[5].Value = model.zhicheng;
			parameters[6].Value = model.xueli;
			parameters[7].Value = model.danwei;
			parameters[8].Value = model.fengong;
			parameters[9].Value = model.add_time;
			parameters[10].Value = model.add_user;
			parameters[11].Value = model.status;
			parameters[12].Value = model.id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from ky_applyproject_team ");
			strSql.Append(" where id=@id");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
			parameters[0].Value = id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from ky_applyproject_team ");
			strSql.Append(" where id in ("+idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.ky_applyproject_team GetModel(int id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 id,pa_id,name,sex,chusheng,zhuanye,zhicheng,xueli,danwei,fengong,add_time,add_user,status from ky_applyproject_team ");
			strSql.Append(" where id=@id");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
			parameters[0].Value = id;

			DTcms.Model.ky_applyproject_team model=new DTcms.Model.ky_applyproject_team();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.ky_applyproject_team DataRowToModel(DataRow row)
		{
			DTcms.Model.ky_applyproject_team model=new DTcms.Model.ky_applyproject_team();
			if (row != null)
			{
				if(row["id"]!=null && row["id"].ToString()!="")
				{
					model.id=int.Parse(row["id"].ToString());
				}
				if(row["pa_id"]!=null)
				{
					model.pa_id=row["pa_id"].ToString();
				}
				if(row["name"]!=null)
				{
					model.name=row["name"].ToString();
				}
				if(row["sex"]!=null)
				{
					model.sex=row["sex"].ToString();
				}
				if(row["chusheng"]!=null)
				{
					model.chusheng=row["chusheng"].ToString();
				}
				if(row["zhuanye"]!=null)
				{
					model.zhuanye=row["zhuanye"].ToString();
				}
				if(row["zhicheng"]!=null)
				{
					model.zhicheng=row["zhicheng"].ToString();
				}
				if(row["xueli"]!=null)
				{
					model.xueli=row["xueli"].ToString();
				}
				if(row["danwei"]!=null)
				{
					model.danwei=row["danwei"].ToString();
				}
				if(row["fengong"]!=null)
				{
					model.fengong=row["fengong"].ToString();
				}
				if(row["add_time"]!=null && row["add_time"].ToString()!="")
				{
					model.add_time=DateTime.Parse(row["add_time"].ToString());
				}
				if(row["add_user"]!=null)
				{
					model.add_user=row["add_user"].ToString();
				}
				if(row["status"]!=null && row["status"].ToString()!="")
				{
					model.status=int.Parse(row["status"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select id,pa_id,name,sex,chusheng,zhuanye,zhicheng,xueli,danwei,fengong,add_time,add_user,status ");
			strSql.Append(" FROM ky_applyproject_team ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" id,pa_id,name,sex,chusheng,zhuanye,zhicheng,xueli,danwei,fengong,add_time,add_user,status ");
			strSql.Append(" FROM ky_applyproject_team ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM ky_applyproject_team ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.id desc");
			}
			strSql.Append(")AS Row, T.*  from ky_applyproject_team T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "ky_applyproject_team";
			parameters[1].Value = "id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod
		public DataSet GetList(int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
		{
			StringBuilder strSql = new StringBuilder();
			strSql.Append("select * FROM ky_applyproject_team ");
			if (strWhere.Trim() != "")
			{
				strSql.Append(" where " + strWhere);
			}
			recordCount = Convert.ToInt32(DbHelperSQL.GetSingle(PagingHelper.CreateCountingSql(strSql.ToString())));
			return DbHelperSQL.Query(PagingHelper.CreatePagingSql(recordCount, pageSize, pageIndex, strSql.ToString(), filedOrder));
		}
		#endregion  ExtensionMethod
	}
}

