﻿using System; 
using System.Text;
using System.Data.SqlClient;
using System.Collections.Generic; 
using System.Data;
using DTcms.DBUtility;
namespace DTcms.DAL  
{
	 	//ky_attachment
		public partial class ky_attachment
	{
   		     
		public bool Exists(int am_id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from ky_attachment");
			strSql.Append(" where ");
			                                       strSql.Append(" am_id = @am_id  ");
                            			SqlParameter[] parameters = {
					new SqlParameter("@am_id", SqlDbType.Int,4)
			};
			parameters[0].Value = am_id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}
		
				
		
		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(DTcms.Model.ky_attachment model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into ky_attachment(");			
            strSql.Append("file_name,file_path,file_size,file_ext,p_id,sort,add_time,status,apply_id");
			strSql.Append(") values (");
            strSql.Append("@file_name,@file_path,@file_size,@file_ext,@p_id,@sort,@add_time,@status,@apply_id");            
            strSql.Append(") ");            
            strSql.Append(";select @@IDENTITY");		
			SqlParameter[] parameters = {
			            new SqlParameter("@file_name", SqlDbType.VarChar,100) ,            
                        new SqlParameter("@file_path", SqlDbType.VarChar,100) ,            
                        new SqlParameter("@file_size", SqlDbType.Int,4) ,            
                        new SqlParameter("@file_ext", SqlDbType.VarChar,50) ,            
                        new SqlParameter("@p_id", SqlDbType.Int,4) ,            
                        new SqlParameter("@sort", SqlDbType.Int,4) ,            
                        new SqlParameter("@add_time", SqlDbType.DateTime) ,            
                        new SqlParameter("@status", SqlDbType.Int,4) ,            
                        new SqlParameter("@apply_id", SqlDbType.VarChar,50)             
              
            };
			            
            parameters[0].Value = model.file_name;                        
            parameters[1].Value = model.file_path;                        
            parameters[2].Value = model.file_size;                        
            parameters[3].Value = model.file_ext;                        
            parameters[4].Value = model.p_id;                        
            parameters[5].Value = model.sort;                        
            parameters[6].Value = model.add_time;                        
            parameters[7].Value = model.status;                        
            parameters[8].Value = model.apply_id;                        
			   
			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);			
			if (obj == null)
			{
				return 0;
			}
			else
			{
				                    
            	return Convert.ToInt32(obj);
                                                                  
			}			   
            			
		}
		
		
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTcms.Model.ky_attachment model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update ky_attachment set ");
			                                                
            strSql.Append(" file_name = @file_name , ");                                    
            strSql.Append(" file_path = @file_path , ");                                    
            strSql.Append(" file_size = @file_size , ");                                    
            strSql.Append(" file_ext = @file_ext , ");                                    
            strSql.Append(" p_id = @p_id , ");                                    
            strSql.Append(" sort = @sort , ");                                    
            strSql.Append(" add_time = @add_time , ");                                    
            strSql.Append(" status = @status , ");                                    
            strSql.Append(" apply_id = @apply_id  ");            			
			strSql.Append(" where am_id=@am_id ");
						
SqlParameter[] parameters = {
			            new SqlParameter("@am_id", SqlDbType.Int,4) ,            
                        new SqlParameter("@file_name", SqlDbType.VarChar,100) ,            
                        new SqlParameter("@file_path", SqlDbType.VarChar,100) ,            
                        new SqlParameter("@file_size", SqlDbType.Int,4) ,            
                        new SqlParameter("@file_ext", SqlDbType.VarChar,50) ,            
                        new SqlParameter("@p_id", SqlDbType.Int,4) ,            
                        new SqlParameter("@sort", SqlDbType.Int,4) ,            
                        new SqlParameter("@add_time", SqlDbType.DateTime) ,            
                        new SqlParameter("@status", SqlDbType.Int,4) ,            
                        new SqlParameter("@apply_id", SqlDbType.VarChar,50)             
              
            };
						            
            parameters[0].Value = model.am_id;                        
            parameters[1].Value = model.file_name;                        
            parameters[2].Value = model.file_path;                        
            parameters[3].Value = model.file_size;                        
            parameters[4].Value = model.file_ext;                        
            parameters[5].Value = model.p_id;                        
            parameters[6].Value = model.sort;                        
            parameters[7].Value = model.add_time;                        
            parameters[8].Value = model.status;                        
            parameters[9].Value = model.apply_id;                        
            int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int am_id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from ky_attachment ");
			strSql.Append(" where am_id=@am_id");
						SqlParameter[] parameters = {
					new SqlParameter("@am_id", SqlDbType.Int,4)
			};
			parameters[0].Value = am_id;


			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
				/// <summary>
		/// 批量删除一批数据
		/// </summary>
		public bool DeleteList(string am_idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from ky_attachment ");
			strSql.Append(" where ID in ("+am_idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.ky_attachment GetModel(int am_id)
		{

			StringBuilder strSql = new StringBuilder();
			strSql.Append("select am_id, file_name, file_path, file_size, file_ext, p_id, sort, add_time, status, apply_id  ");
			strSql.Append("  from ky_attachment ");
			strSql.Append(" where am_id=@am_id");
			SqlParameter[] parameters = {
					new SqlParameter("@am_id", SqlDbType.Int,4)
			};
			parameters[0].Value = am_id;


			DTcms.Model.ky_attachment model = new DTcms.Model.ky_attachment();
			DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);

			if (ds.Tables[0].Rows.Count > 0)
			{
				if (ds.Tables[0].Rows[0]["am_id"].ToString() != "")
				{
					model.am_id = int.Parse(ds.Tables[0].Rows[0]["am_id"].ToString());
				}
				model.file_name = ds.Tables[0].Rows[0]["file_name"].ToString();
				model.file_path = ds.Tables[0].Rows[0]["file_path"].ToString();
				if (ds.Tables[0].Rows[0]["file_size"].ToString() != "")
				{
					model.file_size = int.Parse(ds.Tables[0].Rows[0]["file_size"].ToString());
				}
				model.file_ext = ds.Tables[0].Rows[0]["file_ext"].ToString();
				if (ds.Tables[0].Rows[0]["p_id"].ToString() != "")
				{
					model.p_id = int.Parse(ds.Tables[0].Rows[0]["p_id"].ToString());
				}
				if (ds.Tables[0].Rows[0]["sort"].ToString() != "")
				{
					model.sort = int.Parse(ds.Tables[0].Rows[0]["sort"].ToString());
				}
				if (ds.Tables[0].Rows[0]["add_time"].ToString() != "")
				{
					model.add_time = DateTime.Parse(ds.Tables[0].Rows[0]["add_time"].ToString());
				}
				if (ds.Tables[0].Rows[0]["status"].ToString() != "")
				{
					model.status = int.Parse(ds.Tables[0].Rows[0]["status"].ToString());
				}
				model.apply_id = ds.Tables[0].Rows[0]["apply_id"].ToString();

				return model;
			}
			else
			{
				return null;
			}
		}
		
		
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select * ");
			strSql.Append(" FROM ky_attachment ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}
		
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" * ");
			strSql.Append(" FROM ky_attachment ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}


		/// <summary>
		/// 批量删除一批数据
		/// </summary>
		public bool DeletebyP_id(string p_id)
		{
			StringBuilder strSql = new StringBuilder();
			strSql.Append("delete from ky_attachment ");
			strSql.Append(" where p_id = "+ p_id + " and apply_id='' ");
			int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 批量删除一批数据
		/// </summary>
		public bool Deletebyapply_id(string apply_id)
		{
			StringBuilder strSql = new StringBuilder();
			strSql.Append("delete from ky_attachment ");
			strSql.Append(" where   apply_id='"+ apply_id + "' ");
			int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


	}
}

