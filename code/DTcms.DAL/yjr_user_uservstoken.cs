﻿/**  版本信息模板在安装目录下，可自行修改。
* yjr_user_uservstoken.cs
*
* 功 能： N/A
* 类 名： yjr_user_uservstoken
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2020/8/27 9:21:27   N/A    初版
*
* Copyright (c) 2019  Corporation. All rights reserved.
*/
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DTcms.DBUtility;//Please add references
namespace DTcms.DAL
{
	/// <summary>
	/// 数据访问类:yjr_user_uservstoken
	/// </summary>
	public partial class yjr_user_uservstoken
	{
		public yjr_user_uservstoken()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("userid", "yjr_user_uservstoken"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int userid)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from yjr_user_uservstoken");
			strSql.Append(" where userid=@userid ");
			SqlParameter[] parameters = {
					new SqlParameter("@userid", SqlDbType.Int,4)			};
			parameters[0].Value = userid;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(DTcms.Model.yjr_user_uservstoken model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into yjr_user_uservstoken(");
			strSql.Append("userid,token,expires)");
			strSql.Append(" values (");
			strSql.Append("@userid,@token,@expires)");
			SqlParameter[] parameters = {
					new SqlParameter("@userid", SqlDbType.Int,4),
					new SqlParameter("@token", SqlDbType.VarChar,100),
					new SqlParameter("@expires", SqlDbType.DateTime)};
			parameters[0].Value = model.userid;
			parameters[1].Value = model.token;
			parameters[2].Value = model.expires;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTcms.Model.yjr_user_uservstoken model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update yjr_user_uservstoken set ");
			strSql.Append("token=@token,");
			strSql.Append("expires=@expires");
			strSql.Append(" where userid=@userid ");
			SqlParameter[] parameters = {
					new SqlParameter("@token", SqlDbType.VarChar,100),
					new SqlParameter("@expires", SqlDbType.DateTime),
					new SqlParameter("@userid", SqlDbType.Int,4)};
			parameters[0].Value = model.token;
			parameters[1].Value = model.expires;
			parameters[2].Value = model.userid;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int userid)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from yjr_user_uservstoken ");
			strSql.Append(" where userid=@userid ");
			SqlParameter[] parameters = {
					new SqlParameter("@userid", SqlDbType.Int,4)			};
			parameters[0].Value = userid;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string useridlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from yjr_user_uservstoken ");
			strSql.Append(" where userid in ("+useridlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.yjr_user_uservstoken GetModel(int userid)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 userid,token,expires from yjr_user_uservstoken ");
			strSql.Append(" where userid=@userid ");
			SqlParameter[] parameters = {
					new SqlParameter("@userid", SqlDbType.Int,4)			};
			parameters[0].Value = userid;

			DTcms.Model.yjr_user_uservstoken model=new DTcms.Model.yjr_user_uservstoken();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.yjr_user_uservstoken DataRowToModel(DataRow row)
		{
			DTcms.Model.yjr_user_uservstoken model=new DTcms.Model.yjr_user_uservstoken();
			if (row != null)
			{
				if(row["userid"]!=null && row["userid"].ToString()!="")
				{
					model.userid=int.Parse(row["userid"].ToString());
				}
				if(row["token"]!=null)
				{
					model.token=row["token"].ToString();
				}
				if(row["expires"]!=null && row["expires"].ToString()!="")
				{
					model.expires=DateTime.Parse(row["expires"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select userid,token,expires ");
			strSql.Append(" FROM yjr_user_uservstoken ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" userid,token,expires ");
			strSql.Append(" FROM yjr_user_uservstoken ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM yjr_user_uservstoken ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.userid desc");
			}
			strSql.Append(")AS Row, T.*  from yjr_user_uservstoken T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "yjr_user_uservstoken";
			parameters[1].Value = "userid";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

