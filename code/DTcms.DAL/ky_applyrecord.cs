﻿/**  版本信息模板在安装目录下，可自行修改。
* ky_applyrecord.cs
*
* 功 能： N/A
* 类 名： ky_applyrecord
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2021/7/5 13:57:53   N/A    初版
*

*/
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DTcms.DBUtility;//Please add references
using DTcms.Common;

namespace DTcms.DAL
{
	/// <summary>
	/// 数据访问类:ky_applyrecord
	/// </summary>
	public partial class ky_applyrecord
	{
		public ky_applyrecord()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("id", "ky_applyrecord"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from ky_applyrecord");
			strSql.Append(" where id=@id");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
			parameters[0].Value = id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(DTcms.Model.ky_applyrecord model)
		{
			StringBuilder strSql = new StringBuilder();
			strSql.Append("insert into ky_applyrecord(");
			strSql.Append("p_id,add_user,add_time,remark,audit_user,audit_status,audit_time,audit_type,audit_remark)");
			strSql.Append(" values (");
			strSql.Append("@p_id,@add_user,@add_time,@remark,@audit_user,@audit_status,@audit_time,@audit_type,@audit_remark)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@p_id", SqlDbType.Int,4),
					new SqlParameter("@add_user", SqlDbType.VarChar,50),
					new SqlParameter("@add_time", SqlDbType.DateTime),
					new SqlParameter("@remark", SqlDbType.VarChar,500),
					new SqlParameter("@audit_user", SqlDbType.VarChar,50),
					new SqlParameter("@audit_status", SqlDbType.VarChar,50),
					new SqlParameter("@audit_time", SqlDbType.DateTime),
					new SqlParameter("@audit_type", SqlDbType.VarChar,50),
					new SqlParameter("@audit_remark", SqlDbType.VarChar,500)};
			parameters[0].Value = model.p_id;
			parameters[1].Value = model.add_user;
			parameters[2].Value = model.add_time;
			parameters[3].Value = model.remark;
			parameters[4].Value = model.audit_user;
			parameters[5].Value = model.audit_status;
			parameters[6].Value = model.audit_time;
			parameters[7].Value = model.audit_type;
			parameters[8].Value = model.audit_remark;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTcms.Model.ky_applyrecord model)
		{
			StringBuilder strSql = new StringBuilder();
			strSql.Append("update ky_applyrecord set ");
			strSql.Append("p_id=@p_id,");
			strSql.Append("add_user=@add_user,");
			strSql.Append("add_time=@add_time,");
			strSql.Append("remark=@remark,");
			strSql.Append("audit_user=@audit_user,");
			strSql.Append("audit_status=@audit_status,");
			strSql.Append("audit_time=@audit_time,");
			strSql.Append("audit_type=@audit_type,");
			strSql.Append("audit_remark=@audit_remark");
			strSql.Append(" where id=@id");
			SqlParameter[] parameters = {
					new SqlParameter("@p_id", SqlDbType.Int,4),
					new SqlParameter("@add_user", SqlDbType.VarChar,50),
					new SqlParameter("@add_time", SqlDbType.DateTime),
					new SqlParameter("@remark", SqlDbType.VarChar,500),
					new SqlParameter("@audit_user", SqlDbType.VarChar,50),
					new SqlParameter("@audit_status", SqlDbType.VarChar,50),
					new SqlParameter("@audit_time", SqlDbType.DateTime),
					new SqlParameter("@audit_type", SqlDbType.VarChar,50),
					new SqlParameter("@audit_remark", SqlDbType.VarChar,500),
					new SqlParameter("@id", SqlDbType.Int,4)};
			parameters[0].Value = model.p_id;
			parameters[1].Value = model.add_user;
			parameters[2].Value = model.add_time;
			parameters[3].Value = model.remark;
			parameters[4].Value = model.audit_user;
			parameters[5].Value = model.audit_status;
			parameters[6].Value = model.audit_time;
			parameters[7].Value = model.audit_type;
			parameters[8].Value = model.audit_remark;
			parameters[9].Value = model.id;


			int rows =DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from ky_applyrecord ");
			strSql.Append(" where id=@id");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
			parameters[0].Value = id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from ky_applyrecord ");
			strSql.Append(" where id in ("+idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.ky_applyrecord GetModel(int id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 id,p_id,add_user,add_time,remark,audit_user,audit_status,audit_time,audit_type,audit_remark from ky_applyrecord ");
			strSql.Append(" where id=@id");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
			parameters[0].Value = id;

			DTcms.Model.ky_applyrecord model=new DTcms.Model.ky_applyrecord();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.ky_applyrecord DataRowToModel(DataRow row)
		{
			DTcms.Model.ky_applyrecord model=new DTcms.Model.ky_applyrecord();
			if (row != null)
			{
				if(row["id"]!=null && row["id"].ToString()!="")
				{
					model.id=int.Parse(row["id"].ToString());
				}
				if(row["p_id"]!=null && row["p_id"].ToString()!="")
				{
					model.p_id=int.Parse(row["p_id"].ToString());
				}
				if(row["add_user"]!=null)
				{
					model.add_user=row["add_user"].ToString();
				}
				if(row["add_time"]!=null && row["add_time"].ToString()!="")
				{
					model.add_time=DateTime.Parse(row["add_time"].ToString());
				}
				if(row["remark"]!=null)
				{
					model.remark=row["remark"].ToString();
				}
				if(row["audit_user"]!=null)
				{
					model.audit_user=row["audit_user"].ToString();
				}
				if(row["audit_status"]!=null)
				{
					model.audit_status=row["audit_status"].ToString();
				}
				if(row["audit_time"]!=null && row["audit_time"].ToString()!="")
				{
					model.audit_time=DateTime.Parse(row["audit_time"].ToString());
				}
				if(row["audit_type"]!=null)
				{
					model.audit_type=row["audit_type"].ToString();
				}
				if (row["audit_remark"] != null)
				{
					model.audit_remark = row["audit_remark"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select id,p_id,add_user,add_time,remark,audit_user,audit_status,audit_time,audit_type,audit_remark ");
			strSql.Append(" FROM ky_applyrecord ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" id,p_id,add_user,add_time,remark,audit_user,audit_status,audit_time,audit_type,audit_remark ");
			strSql.Append(" FROM ky_applyrecord ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM ky_applyrecord ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.id desc");
			}
			strSql.Append(")AS Row, T.*  from ky_applyrecord T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "ky_applyrecord";
			parameters[1].Value = "id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod
		public DataSet GetList(int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
		{
			StringBuilder strSql = new StringBuilder();
			strSql.Append("select * FROM View_ky_applylist ");
			if (strWhere.Trim() != "")
			{
				strSql.Append(" where " + strWhere);
			}
			recordCount = Convert.ToInt32(DbHelperSQL.GetSingle(PagingHelper.CreateCountingSql(strSql.ToString())));
			return DbHelperSQL.Query(PagingHelper.CreatePagingSql(recordCount, pageSize, pageIndex, strSql.ToString(), filedOrder));
		}
		#endregion  ExtensionMethod
	}
}

