﻿using System; 
using System.Text;
using System.Data.SqlClient;
using System.Collections.Generic; 
using System.Data;
using DTcms.DBUtility;
using DTcms.Common;
namespace DTcms.DAL  
{
	//ky_projects
	public partial class ky_projects
	{

		public bool Exists(int p_id)
		{
			StringBuilder strSql = new StringBuilder();
			strSql.Append("select count(1) from ky_projects");
			strSql.Append(" where ");
			strSql.Append(" p_id = @p_id  ");
			SqlParameter[] parameters = {
					new SqlParameter("@p_id", SqlDbType.Int,4)
			};
			parameters[0].Value = p_id;

			return DbHelperSQL.Exists(strSql.ToString(), parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(DTcms.Model.ky_projects model)
		{
			StringBuilder strSql = new StringBuilder();
			strSql.Append("insert into ky_projects(");
			strSql.Append("project_number,project_title,dptid,owner,member,tel,start_time,end_time,project_type,add_time,status,fee_xiada,fee_peitao,fee_zongji,fee_jieyu,remark,pa_id)");
			strSql.Append(" values (");
			strSql.Append("@project_number,@project_title,@dptid,@owner,@member,@tel,@start_time,@end_time,@project_type,@add_time,@status,@fee_xiada,@fee_peitao,@fee_zongji,@fee_jieyu,@remark,@pa_id)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@project_number", SqlDbType.VarChar,100),
					new SqlParameter("@project_title", SqlDbType.VarChar,300),
					new SqlParameter("@dptid", SqlDbType.Int,4),
					new SqlParameter("@owner", SqlDbType.VarChar,50),
					new SqlParameter("@member", SqlDbType.VarChar,200),
					new SqlParameter("@tel", SqlDbType.VarChar,50),
					new SqlParameter("@start_time", SqlDbType.VarChar,50),
					new SqlParameter("@end_time", SqlDbType.VarChar,50),
					new SqlParameter("@project_type", SqlDbType.Int,4),
					new SqlParameter("@add_time", SqlDbType.DateTime),
					new SqlParameter("@status", SqlDbType.Int,4),
					new SqlParameter("@fee_xiada", SqlDbType.VarChar,50),
					new SqlParameter("@fee_peitao", SqlDbType.VarChar,50),
					new SqlParameter("@fee_zongji", SqlDbType.VarChar,50),
					new SqlParameter("@fee_jieyu", SqlDbType.VarChar,50),
					new SqlParameter("@remark", SqlDbType.VarChar,500),
					new SqlParameter("@pa_id", SqlDbType.VarChar,50)};
			parameters[0].Value = model.project_number;
			parameters[1].Value = model.project_title;
			parameters[2].Value = model.dptid;
			parameters[3].Value = model.owner;
			parameters[4].Value = model.member;
			parameters[5].Value = model.tel;
			parameters[6].Value = model.start_time;
			parameters[7].Value = model.end_time;
			parameters[8].Value = model.project_type;
			parameters[9].Value = model.add_time;
			parameters[10].Value = model.status;
			parameters[11].Value = model.fee_xiada;
			parameters[12].Value = model.fee_peitao;
			parameters[13].Value = model.fee_zongji;
			parameters[14].Value = model.fee_jieyu;
			parameters[15].Value = model.remark;
			parameters[16].Value = model.pa_id;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTcms.Model.ky_projects model)
		{
			StringBuilder strSql = new StringBuilder();
			strSql.Append("update ky_projects set ");
			strSql.Append("project_number=@project_number,");
			strSql.Append("project_title=@project_title,");
			strSql.Append("dptid=@dptid,");
			strSql.Append("owner=@owner,");
			strSql.Append("member=@member,");
			strSql.Append("tel=@tel,");
			strSql.Append("start_time=@start_time,");
			strSql.Append("end_time=@end_time,");
			strSql.Append("project_type=@project_type,");
			strSql.Append("add_time=@add_time,");
			strSql.Append("status=@status,");
			strSql.Append("fee_xiada=@fee_xiada,");
			strSql.Append("fee_peitao=@fee_peitao,");
			strSql.Append("fee_zongji=@fee_zongji,");
			strSql.Append("fee_jieyu=@fee_jieyu,");
			strSql.Append("remark=@remark,");
			strSql.Append("pa_id=@pa_id");
			strSql.Append(" where p_id=@p_id");
			SqlParameter[] parameters = {
					new SqlParameter("@project_number", SqlDbType.VarChar,100),
					new SqlParameter("@project_title", SqlDbType.VarChar,300),
					new SqlParameter("@dptid", SqlDbType.Int,4),
					new SqlParameter("@owner", SqlDbType.VarChar,50),
					new SqlParameter("@member", SqlDbType.VarChar,200),
					new SqlParameter("@tel", SqlDbType.VarChar,50),
					new SqlParameter("@start_time", SqlDbType.VarChar,50),
					new SqlParameter("@end_time", SqlDbType.VarChar,50),
					new SqlParameter("@project_type", SqlDbType.Int,4),
					new SqlParameter("@add_time", SqlDbType.DateTime),
					new SqlParameter("@status", SqlDbType.Int,4),
					new SqlParameter("@fee_xiada", SqlDbType.VarChar,50),
					new SqlParameter("@fee_peitao", SqlDbType.VarChar,50),
					new SqlParameter("@fee_zongji", SqlDbType.VarChar,50),
					new SqlParameter("@fee_jieyu", SqlDbType.VarChar,50),
					new SqlParameter("@remark", SqlDbType.VarChar,500),
					new SqlParameter("@pa_id", SqlDbType.VarChar,50),
					new SqlParameter("@p_id", SqlDbType.Int,4)};
			parameters[0].Value = model.project_number;
			parameters[1].Value = model.project_title;
			parameters[2].Value = model.dptid;
			parameters[3].Value = model.owner;
			parameters[4].Value = model.member;
			parameters[5].Value = model.tel;
			parameters[6].Value = model.start_time;
			parameters[7].Value = model.end_time;
			parameters[8].Value = model.project_type;
			parameters[9].Value = model.add_time;
			parameters[10].Value = model.status;
			parameters[11].Value = model.fee_xiada;
			parameters[12].Value = model.fee_peitao;
			parameters[13].Value = model.fee_zongji;
			parameters[14].Value = model.fee_jieyu;
			parameters[15].Value = model.remark;
			parameters[16].Value = model.pa_id;
			parameters[17].Value = model.p_id;

			int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int p_id)
		{

			StringBuilder strSql = new StringBuilder();
			strSql.Append("delete from ky_projects ");
			strSql.Append(" where p_id=@p_id");
			SqlParameter[] parameters = {
					new SqlParameter("@p_id", SqlDbType.Int,4)
			};
			parameters[0].Value = p_id;

			int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string p_idlist)
		{
			StringBuilder strSql = new StringBuilder();
			strSql.Append("delete from ky_projects ");
			strSql.Append(" where p_id in (" + p_idlist + ")  ");
			int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.ky_projects GetModel(int p_id)
		{

			StringBuilder strSql = new StringBuilder();
			strSql.Append("select  top 1 p_id,project_number,project_title,dptid,owner,member,tel,start_time,end_time,project_type,add_time,status,fee_xiada,fee_peitao,fee_zongji,fee_jieyu,remark,pa_id from ky_projects ");
			strSql.Append(" where p_id=@p_id");
			SqlParameter[] parameters = {
					new SqlParameter("@p_id", SqlDbType.Int,4)
			};
			parameters[0].Value = p_id;

			DTcms.Model.ky_projects model = new DTcms.Model.ky_projects();
			DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
			if (ds.Tables[0].Rows.Count > 0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.ky_projects DataRowToModel(DataRow row)
		{
			DTcms.Model.ky_projects model = new DTcms.Model.ky_projects();
			if (row != null)
			{
				if (row["p_id"] != null && row["p_id"].ToString() != "")
				{
					model.p_id = int.Parse(row["p_id"].ToString());
				}
				if (row["project_number"] != null)
				{
					model.project_number = row["project_number"].ToString();
				}
				if (row["project_title"] != null)
				{
					model.project_title = row["project_title"].ToString();
				}
				if (row["dptid"] != null && row["dptid"].ToString() != "")
				{
					model.dptid = int.Parse(row["dptid"].ToString());
				}
				if (row["owner"] != null)
				{
					model.owner = row["owner"].ToString();
				}
				if (row["member"] != null)
				{
					model.member = row["member"].ToString();
				}
				if (row["tel"] != null)
				{
					model.tel = row["tel"].ToString();
				}
				if (row["start_time"] != null)
				{
					model.start_time = row["start_time"].ToString();
				}
				if (row["end_time"] != null)
				{
					model.end_time = row["end_time"].ToString();
				}
				if (row["project_type"] != null && row["project_type"].ToString() != "")
				{
					model.project_type = int.Parse(row["project_type"].ToString());
				}
				if (row["add_time"] != null && row["add_time"].ToString() != "")
				{
					model.add_time = DateTime.Parse(row["add_time"].ToString());
				}
				if (row["status"] != null && row["status"].ToString() != "")
				{
					model.status = int.Parse(row["status"].ToString());
				}
				if (row["fee_xiada"] != null)
				{
					model.fee_xiada = row["fee_xiada"].ToString();
				}
				if (row["fee_peitao"] != null)
				{
					model.fee_peitao = row["fee_peitao"].ToString();
				}
				if (row["fee_zongji"] != null)
				{
					model.fee_zongji = row["fee_zongji"].ToString();
				}
				if (row["fee_jieyu"] != null)
				{
					model.fee_jieyu = row["fee_jieyu"].ToString();
				}
				if (row["remark"] != null)
				{
					model.remark = row["remark"].ToString();
				}
				if (row["pa_id"] != null)
				{
					model.pa_id = row["pa_id"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql = new StringBuilder();
			strSql.Append("select p_id,project_number,project_title,dptid,owner,member,tel,start_time,end_time,project_type,add_time,status,fee_xiada,fee_peitao,fee_zongji,fee_jieyu,remark,pa_id ");
			strSql.Append(" FROM ky_projects ");
			if (strWhere.Trim() != "")
			{
				strSql.Append(" where " + strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top, string strWhere, string filedOrder)
		{
			StringBuilder strSql = new StringBuilder();
			strSql.Append("select ");
			if (Top > 0)
			{
				strSql.Append(" top " + Top.ToString());
			}
			strSql.Append(" p_id,project_number,project_title,dptid,owner,member,tel,start_time,end_time,project_type,add_time,status,fee_xiada,fee_peitao,fee_zongji,fee_jieyu,remark,pa_id ");
			strSql.Append(" FROM ky_projects ");
			if (strWhere.Trim() != "")
			{
				strSql.Append(" where " + strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}
		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql = new StringBuilder();
			strSql.Append("select count(1) FROM ky_projects ");
			if (strWhere.Trim() != "")
			{
				strSql.Append(" where " + strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 获得查询分页数据
		/// </summary>
		public DataSet GetList(int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
		{
			StringBuilder strSql = new StringBuilder();
			strSql.Append("select * FROM View_ky_projectlist ");
			if (strWhere.Trim() != "")
			{
				strSql.Append(" where " + strWhere);
			}
			recordCount = Convert.ToInt32(DbHelperSQL.GetSingle(PagingHelper.CreateCountingSql(strSql.ToString())));
			return DbHelperSQL.Query(PagingHelper.CreatePagingSql(recordCount, pageSize, pageIndex, strSql.ToString(), filedOrder));
		}


	}
}

