﻿/**  版本信息模板在安装目录下，可自行修改。
* ky_applyfee.cs
*
* 功 能： N/A
* 类 名： ky_applyfee
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2021-07-18 16:55:12   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.

*/
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DTcms.DBUtility;//Please add references
using DTcms.Common;

namespace DTcms.DAL
{
	/// <summary>
	/// 数据访问类:ky_applyfee
	/// </summary>
	public partial class ky_applyfee
	{
		public ky_applyfee()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("applyfee_id", "ky_applyfee"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int applyfee_id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from ky_applyfee");
			strSql.Append(" where applyfee_id=@applyfee_id");
			SqlParameter[] parameters = {
					new SqlParameter("@applyfee_id", SqlDbType.Int,4)
			};
			parameters[0].Value = applyfee_id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(DTcms.Model.ky_applyfee model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into ky_applyfee(");
			strSql.Append("p_id,fee,reason,add_time,add_user,status,audit_1_user,audit_2_user,audit_3_user,audit_4_user,audit_1_time,audit_2_time,audit_3_time,audit_4_time,audit_1_status,audit_2_status,audit_3_status,audit_4_status,fee_type)");
			strSql.Append(" values (");
			strSql.Append("@p_id,@fee,@reason,@add_time,@add_user,@status,@audit_1_user,@audit_2_user,@audit_3_user,@audit_4_user,@audit_1_time,@audit_2_time,@audit_3_time,@audit_4_time,@audit_1_status,@audit_2_status,@audit_3_status,@audit_4_status,@fee_type)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@p_id", SqlDbType.Int,4),
					new SqlParameter("@fee", SqlDbType.VarChar,50),
					new SqlParameter("@reason", SqlDbType.VarChar,200),
					new SqlParameter("@add_time", SqlDbType.DateTime),
					new SqlParameter("@add_user", SqlDbType.VarChar,50),
					new SqlParameter("@status", SqlDbType.Int,4),
					new SqlParameter("@audit_1_user", SqlDbType.VarChar,50),
					new SqlParameter("@audit_2_user", SqlDbType.VarChar,50),
					new SqlParameter("@audit_3_user", SqlDbType.VarChar,50),
					new SqlParameter("@audit_4_user", SqlDbType.VarChar,50),
					new SqlParameter("@audit_1_time", SqlDbType.DateTime),
					new SqlParameter("@audit_2_time", SqlDbType.DateTime),
					new SqlParameter("@audit_3_time", SqlDbType.DateTime),
					new SqlParameter("@audit_4_time", SqlDbType.DateTime),
					new SqlParameter("@audit_1_status", SqlDbType.Int,4),
					new SqlParameter("@audit_2_status", SqlDbType.Int,4),
					new SqlParameter("@audit_3_status", SqlDbType.Int,4),
					new SqlParameter("@audit_4_status", SqlDbType.Int,4),
					new SqlParameter("@fee_type", SqlDbType.VarChar,50)};
			parameters[0].Value = model.p_id;
			parameters[1].Value = model.fee;
			parameters[2].Value = model.reason;
			parameters[3].Value = model.add_time;
			parameters[4].Value = model.add_user;
			parameters[5].Value = model.status;
			parameters[6].Value = model.audit_1_user;
			parameters[7].Value = model.audit_2_user;
			parameters[8].Value = model.audit_3_user;
			parameters[9].Value = model.audit_4_user;
			parameters[10].Value = model.audit_1_time;
			parameters[11].Value = model.audit_2_time;
			parameters[12].Value = model.audit_3_time;
			parameters[13].Value = model.audit_4_time;
			parameters[14].Value = model.audit_1_status;
			parameters[15].Value = model.audit_2_status;
			parameters[16].Value = model.audit_3_status;
			parameters[17].Value = model.audit_4_status;
			parameters[18].Value = model.fee_type;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTcms.Model.ky_applyfee model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update ky_applyfee set ");
			strSql.Append("p_id=@p_id,");
			strSql.Append("fee=@fee,");
			strSql.Append("reason=@reason,");
			strSql.Append("add_time=@add_time,");
			strSql.Append("add_user=@add_user,");
			strSql.Append("status=@status,");
			strSql.Append("audit_1_user=@audit_1_user,");
			strSql.Append("audit_2_user=@audit_2_user,");
			strSql.Append("audit_3_user=@audit_3_user,");
			strSql.Append("audit_4_user=@audit_4_user,");
			strSql.Append("audit_1_time=@audit_1_time,");
			strSql.Append("audit_2_time=@audit_2_time,");
			strSql.Append("audit_3_time=@audit_3_time,");
			strSql.Append("audit_4_time=@audit_4_time,");
			strSql.Append("audit_1_status=@audit_1_status,");
			strSql.Append("audit_2_status=@audit_2_status,");
			strSql.Append("audit_3_status=@audit_3_status,");
			strSql.Append("audit_4_status=@audit_4_status,");
			strSql.Append("fee_type=@fee_type");
			strSql.Append(" where applyfee_id=@applyfee_id");
			SqlParameter[] parameters = {
					new SqlParameter("@p_id", SqlDbType.Int,4),
					new SqlParameter("@fee", SqlDbType.VarChar,50),
					new SqlParameter("@reason", SqlDbType.VarChar,200),
					new SqlParameter("@add_time", SqlDbType.DateTime),
					new SqlParameter("@add_user", SqlDbType.VarChar,50),
					new SqlParameter("@status", SqlDbType.Int,4),
					new SqlParameter("@audit_1_user", SqlDbType.VarChar,50),
					new SqlParameter("@audit_2_user", SqlDbType.VarChar,50),
					new SqlParameter("@audit_3_user", SqlDbType.VarChar,50),
					new SqlParameter("@audit_4_user", SqlDbType.VarChar,50),
					new SqlParameter("@audit_1_time", SqlDbType.DateTime),
					new SqlParameter("@audit_2_time", SqlDbType.DateTime),
					new SqlParameter("@audit_3_time", SqlDbType.DateTime),
					new SqlParameter("@audit_4_time", SqlDbType.DateTime),
					new SqlParameter("@audit_1_status", SqlDbType.Int,4),
					new SqlParameter("@audit_2_status", SqlDbType.Int,4),
					new SqlParameter("@audit_3_status", SqlDbType.Int,4),
					new SqlParameter("@audit_4_status", SqlDbType.Int,4),
					new SqlParameter("@fee_type", SqlDbType.VarChar,50),
					new SqlParameter("@applyfee_id", SqlDbType.Int,4)};
			parameters[0].Value = model.p_id;
			parameters[1].Value = model.fee;
			parameters[2].Value = model.reason;
			parameters[3].Value = model.add_time;
			parameters[4].Value = model.add_user;
			parameters[5].Value = model.status;
			parameters[6].Value = model.audit_1_user;
			parameters[7].Value = model.audit_2_user;
			parameters[8].Value = model.audit_3_user;
			parameters[9].Value = model.audit_4_user;
			parameters[10].Value = model.audit_1_time;
			parameters[11].Value = model.audit_2_time;
			parameters[12].Value = model.audit_3_time;
			parameters[13].Value = model.audit_4_time;
			parameters[14].Value = model.audit_1_status;
			parameters[15].Value = model.audit_2_status;
			parameters[16].Value = model.audit_3_status;
			parameters[17].Value = model.audit_4_status;
			parameters[18].Value = model.fee_type;
			parameters[19].Value = model.applyfee_id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int applyfee_id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from ky_applyfee ");
			strSql.Append(" where applyfee_id=@applyfee_id");
			SqlParameter[] parameters = {
					new SqlParameter("@applyfee_id", SqlDbType.Int,4)
			};
			parameters[0].Value = applyfee_id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string applyfee_idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from ky_applyfee ");
			strSql.Append(" where applyfee_id in ("+applyfee_idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.ky_applyfee GetModel(int applyfee_id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 applyfee_id,p_id,fee,reason,add_time,add_user,status,audit_1_user,audit_2_user,audit_3_user,audit_4_user,audit_1_time,audit_2_time,audit_3_time,audit_4_time,audit_1_status,audit_2_status,audit_3_status,audit_4_status,fee_type from ky_applyfee ");
			strSql.Append(" where applyfee_id=@applyfee_id");
			SqlParameter[] parameters = {
					new SqlParameter("@applyfee_id", SqlDbType.Int,4)
			};
			parameters[0].Value = applyfee_id;

			DTcms.Model.ky_applyfee model=new DTcms.Model.ky_applyfee();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.ky_applyfee DataRowToModel(DataRow row)
		{
			DTcms.Model.ky_applyfee model=new DTcms.Model.ky_applyfee();
			if (row != null)
			{
				if(row["applyfee_id"]!=null && row["applyfee_id"].ToString()!="")
				{
					model.applyfee_id=int.Parse(row["applyfee_id"].ToString());
				}
				if(row["p_id"]!=null && row["p_id"].ToString()!="")
				{
					model.p_id=int.Parse(row["p_id"].ToString());
				}
				if(row["fee"]!=null)
				{
					model.fee=row["fee"].ToString();
				}
				if(row["reason"]!=null)
				{
					model.reason=row["reason"].ToString();
				}
				if(row["add_time"]!=null && row["add_time"].ToString()!="")
				{
					model.add_time=DateTime.Parse(row["add_time"].ToString());
				}
				if(row["add_user"]!=null)
				{
					model.add_user=row["add_user"].ToString();
				}
				if(row["status"]!=null && row["status"].ToString()!="")
				{
					model.status=int.Parse(row["status"].ToString());
				}
				if(row["audit_1_user"]!=null)
				{
					model.audit_1_user=row["audit_1_user"].ToString();
				}
				if(row["audit_2_user"]!=null)
				{
					model.audit_2_user=row["audit_2_user"].ToString();
				}
				if(row["audit_3_user"]!=null)
				{
					model.audit_3_user=row["audit_3_user"].ToString();
				}
				if(row["audit_4_user"]!=null)
				{
					model.audit_4_user=row["audit_4_user"].ToString();
				}
				if(row["audit_1_time"]!=null && row["audit_1_time"].ToString()!="")
				{
					model.audit_1_time=DateTime.Parse(row["audit_1_time"].ToString());
				}
				if(row["audit_2_time"]!=null && row["audit_2_time"].ToString()!="")
				{
					model.audit_2_time=DateTime.Parse(row["audit_2_time"].ToString());
				}
				if(row["audit_3_time"]!=null && row["audit_3_time"].ToString()!="")
				{
					model.audit_3_time=DateTime.Parse(row["audit_3_time"].ToString());
				}
				if(row["audit_4_time"]!=null && row["audit_4_time"].ToString()!="")
				{
					model.audit_4_time=DateTime.Parse(row["audit_4_time"].ToString());
				}
				if(row["audit_1_status"]!=null && row["audit_1_status"].ToString()!="")
				{
					model.audit_1_status=int.Parse(row["audit_1_status"].ToString());
				}
				if(row["audit_2_status"]!=null && row["audit_2_status"].ToString()!="")
				{
					model.audit_2_status=int.Parse(row["audit_2_status"].ToString());
				}
				if(row["audit_3_status"]!=null && row["audit_3_status"].ToString()!="")
				{
					model.audit_3_status=int.Parse(row["audit_3_status"].ToString());
				}
				if(row["audit_4_status"]!=null && row["audit_4_status"].ToString()!="")
				{
					model.audit_4_status=int.Parse(row["audit_4_status"].ToString());
				}
				if(row["fee_type"]!=null)
				{
					model.fee_type=row["fee_type"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select applyfee_id,p_id,fee,reason,add_time,add_user,status,audit_1_user,audit_2_user,audit_3_user,audit_4_user,audit_1_time,audit_2_time,audit_3_time,audit_4_time,audit_1_status,audit_2_status,audit_3_status,audit_4_status,fee_type ");
			strSql.Append(" FROM ky_applyfee ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" applyfee_id,p_id,fee,reason,add_time,add_user,status,audit_1_user,audit_2_user,audit_3_user,audit_4_user,audit_1_time,audit_2_time,audit_3_time,audit_4_time,audit_1_status,audit_2_status,audit_3_status,audit_4_status,fee_type ");
			strSql.Append(" FROM ky_applyfee ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM ky_applyfee ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.applyfee_id desc");
			}
			strSql.Append(")AS Row, T.*  from ky_applyfee T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "ky_applyfee";
			parameters[1].Value = "applyfee_id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod
		public DataSet GetList(int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
		{
			StringBuilder strSql = new StringBuilder();
			strSql.Append("select * FROM View_applyfeelist ");
			if (strWhere.Trim() != "")
			{
				strSql.Append(" where " + strWhere);
			}
			recordCount = Convert.ToInt32(DbHelperSQL.GetSingle(PagingHelper.CreateCountingSql(strSql.ToString())));
			return DbHelperSQL.Query(PagingHelper.CreatePagingSql(recordCount, pageSize, pageIndex, strSql.ToString(), filedOrder));
		}
		#endregion  ExtensionMethod
	}
}

