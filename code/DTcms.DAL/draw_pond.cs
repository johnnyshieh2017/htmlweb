﻿/**  版本信息模板在安装目录下，可自行修改。
* draw_pond.cs
*
* 功 能： N/A
* 类 名： draw_pond
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2020/4/1 15:19:15   N/A    初版
*
* Copyright (c) 2019  Corporation. All rights reserved.
*/
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DTcms.DBUtility;
using DTcms.Common;

namespace DTcms.DAL
{
	/// <summary>
	/// 数据访问类:draw_pond
	/// </summary>
	public partial class draw_pond
	{
		public draw_pond()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("id", "draw_pond"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from draw_pond");
			strSql.Append(" where id=@id");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
			parameters[0].Value = id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(Model.draw_pond model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into draw_pond(");
            strSql.Append("Idnumber,telephone,truename,lotteryid,Prizeid,createdate,updatetime,status,hospital,department,linkmobile,address)");
            strSql.Append(" values (");
            strSql.Append("@Idnumber,@telephone,@truename,@lotteryid,@Prizeid,@createdate,@updatetime,@status,@hospital,@department,@linkmobile,@address)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
                    new SqlParameter("@Idnumber", SqlDbType.VarChar,50),
                    new SqlParameter("@telephone", SqlDbType.VarChar,50),
                    new SqlParameter("@truename", SqlDbType.VarChar,50),
                    new SqlParameter("@lotteryid", SqlDbType.Int,4),
                    new SqlParameter("@Prizeid", SqlDbType.Int,4),
                    new SqlParameter("@createdate", SqlDbType.DateTime),
                    new SqlParameter("@updatetime", SqlDbType.DateTime),
                    new SqlParameter("@status", SqlDbType.Int,4),
                    new SqlParameter("@hospital", SqlDbType.NVarChar,100),
                    new SqlParameter("@department", SqlDbType.NVarChar,100),
                    new SqlParameter("@linkmobile", SqlDbType.VarChar,50),
                    new SqlParameter("@address", SqlDbType.NVarChar,100)};
            parameters[0].Value = model.Idnumber;
            parameters[1].Value = model.telephone;
            parameters[2].Value = model.truename;
            parameters[3].Value = model.lotteryid;
            parameters[4].Value = model.Prizeid;
            parameters[5].Value = model.createdate;
            parameters[6].Value = model.updatetime;
            parameters[7].Value = model.status;
            parameters[8].Value = model.hospital;
            parameters[9].Value = model.department;
            parameters[10].Value = model.linkmobile;
            parameters[11].Value = model.address;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Model.draw_pond model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update draw_pond set ");
            strSql.Append("Idnumber=@Idnumber,");
            strSql.Append("telephone=@telephone,");
            strSql.Append("truename=@truename,");
            strSql.Append("lotteryid=@lotteryid,");
            strSql.Append("Prizeid=@Prizeid,");
            strSql.Append("createdate=@createdate,");
            strSql.Append("updatetime=@updatetime,");
            strSql.Append("status=@status,");
            strSql.Append("hospital=@hospital,");
            strSql.Append("department=@department,");
            strSql.Append("linkmobile=@linkmobile,");
            strSql.Append("address=@address");
            strSql.Append(" where id=@id");
            SqlParameter[] parameters = {
                    new SqlParameter("@Idnumber", SqlDbType.VarChar,50),
                    new SqlParameter("@telephone", SqlDbType.VarChar,50),
                    new SqlParameter("@truename", SqlDbType.VarChar,50),
                    new SqlParameter("@lotteryid", SqlDbType.Int,4),
                    new SqlParameter("@Prizeid", SqlDbType.Int,4),
                    new SqlParameter("@createdate", SqlDbType.DateTime),
                    new SqlParameter("@updatetime", SqlDbType.DateTime),
                    new SqlParameter("@status", SqlDbType.Int,4),
                    new SqlParameter("@hospital", SqlDbType.NVarChar,100),
                    new SqlParameter("@department", SqlDbType.NVarChar,100),
                    new SqlParameter("@linkmobile", SqlDbType.VarChar,50),
                    new SqlParameter("@address", SqlDbType.NVarChar,100),
                    new SqlParameter("@id", SqlDbType.Int,4)};
            parameters[0].Value = model.Idnumber;
            parameters[1].Value = model.telephone;
            parameters[2].Value = model.truename;
            parameters[3].Value = model.lotteryid;
            parameters[4].Value = model.Prizeid;
            parameters[5].Value = model.createdate;
            parameters[6].Value = model.updatetime;
            parameters[7].Value = model.status;
            parameters[8].Value = model.hospital;
            parameters[9].Value = model.department;
            parameters[10].Value = model.linkmobile;
            parameters[11].Value = model.address;
            parameters[12].Value = model.id;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from draw_pond ");
			strSql.Append(" where id=@id");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
			parameters[0].Value = id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from draw_pond ");
			strSql.Append(" where id in ("+idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.draw_pond GetModel(int id)
		{
			
			StringBuilder strSql=new StringBuilder();
            strSql.Append("select  top 1 id,Idnumber,telephone,truename,lotteryid,Prizeid,createdate,updatetime,status,hospital,department,linkmobile,address from draw_pond ");
            strSql.Append(" where id=@id");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
			parameters[0].Value = id;

			DTcms.Model.draw_pond model=new DTcms.Model.draw_pond();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.draw_pond DataRowToModel(DataRow row)
		{
            DTcms.Model.draw_pond model = new DTcms.Model.draw_pond();
            if (row != null)
            {
                if (row["id"] != null && row["id"].ToString() != "")
                {
                    model.id = int.Parse(row["id"].ToString());
                }
                if (row["Idnumber"] != null)
                {
                    model.Idnumber = row["Idnumber"].ToString();
                }
                if (row["telephone"] != null)
                {
                    model.telephone = row["telephone"].ToString();
                }
                if (row["truename"] != null)
                {
                    model.truename = row["truename"].ToString();
                }
                if (row["lotteryid"] != null && row["lotteryid"].ToString() != "")
                {
                    model.lotteryid = int.Parse(row["lotteryid"].ToString());
                }
                if (row["Prizeid"] != null && row["Prizeid"].ToString() != "")
                {
                    model.Prizeid = int.Parse(row["Prizeid"].ToString());
                }
                if (row["createdate"] != null && row["createdate"].ToString() != "")
                {
                    model.createdate = DateTime.Parse(row["createdate"].ToString());
                }
                if (row["updatetime"] != null && row["updatetime"].ToString() != "")
                {
                    model.updatetime = DateTime.Parse(row["updatetime"].ToString());
                }
                if (row["status"] != null && row["status"].ToString() != "")
                {
                    model.status = int.Parse(row["status"].ToString());
                }
                if (row["hospital"] != null)
                {
                    model.hospital = row["hospital"].ToString();
                }
                if (row["department"] != null)
                {
                    model.department = row["department"].ToString();
                }
                if (row["linkmobile"] != null)
                {
                    model.linkmobile = row["linkmobile"].ToString();
                }
                if (row["address"] != null)
                {
                    model.address = row["address"].ToString();
                }
            }
            return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("select id,Idnumber,telephone,truename,lotteryid,Prizeid,createdate,updatetime,status,hospital,department,linkmobile,address ");
            strSql.Append(" FROM draw_pond ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
            strSql.Append("select id,Idnumber,telephone,truename,lotteryid,Prizeid,createdate,updatetime,status,hospital,department,linkmobile,address ");
            strSql.Append(" FROM draw_pond ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM draw_pond ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.id desc");
			}
			strSql.Append(")AS Row, T.*  from draw_pond T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

        /*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "draw_pond";
			parameters[1].Value = "id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

        #endregion  BasicMethod
        #region  ExtensionMethod
        public bool Deletewhere(string where)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from draw_pond ");
            strSql.Append(" where  " + where + "");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 获得查询分页数据
        /// </summary>
        public DataSet GetList(int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * FROM draw_pond");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            recordCount = Convert.ToInt32(DbHelperSQL.GetSingle(PagingHelper.CreateCountingSql(strSql.ToString())));
            return DbHelperSQL.Query(PagingHelper.CreatePagingSql(recordCount, pageSize, pageIndex, strSql.ToString(), filedOrder));
        }
        #endregion  ExtensionMethod
    }
}

