﻿/**  版本信息模板在安装目录下，可自行修改。
* draw_zg_data.cs
*
* 功 能： N/A
* 类 名： draw_zg_data
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2020/4/1 15:19:16   N/A    初版
*
* Copyright (c) 2019  Corporation. All rights reserved.
*/
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DTcms.DBUtility;
namespace DTcms.DAL
{
	/// <summary>
	/// 数据访问类:draw_zg_data
	/// </summary>
	public partial class draw_zg_data
	{
		public draw_zg_data()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(DTcms.Model.draw_zg_data model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into draw_zg_data(");
			strSql.Append("sheng,shi,gongsi,xingming,zhiwu,wxnicheng,shouji,beizhu)");
			strSql.Append(" values (");
			strSql.Append("@sheng,@shi,@gongsi,@xingming,@zhiwu,@wxnicheng,@shouji,@beizhu)");
			SqlParameter[] parameters = {
					new SqlParameter("@sheng", SqlDbType.NVarChar,255),
					new SqlParameter("@shi", SqlDbType.NVarChar,255),
					new SqlParameter("@gongsi", SqlDbType.NVarChar,255),
					new SqlParameter("@xingming", SqlDbType.NVarChar,255),
					new SqlParameter("@zhiwu", SqlDbType.NVarChar,255),
					new SqlParameter("@wxnicheng", SqlDbType.NVarChar,255),
					new SqlParameter("@shouji", SqlDbType.NVarChar,255),
					new SqlParameter("@beizhu", SqlDbType.NVarChar,255)};
			parameters[0].Value = model.sheng;
			parameters[1].Value = model.shi;
			parameters[2].Value = model.gongsi;
			parameters[3].Value = model.xingming;
			parameters[4].Value = model.zhiwu;
			parameters[5].Value = model.wxnicheng;
			parameters[6].Value = model.shouji;
			parameters[7].Value = model.beizhu;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTcms.Model.draw_zg_data model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update draw_zg_data set ");
			strSql.Append("sheng=@sheng,");
			strSql.Append("shi=@shi,");
			strSql.Append("gongsi=@gongsi,");
			strSql.Append("xingming=@xingming,");
			strSql.Append("zhiwu=@zhiwu,");
			strSql.Append("wxnicheng=@wxnicheng,");
			strSql.Append("shouji=@shouji,");
			strSql.Append("beizhu=@beizhu");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
					new SqlParameter("@sheng", SqlDbType.NVarChar,255),
					new SqlParameter("@shi", SqlDbType.NVarChar,255),
					new SqlParameter("@gongsi", SqlDbType.NVarChar,255),
					new SqlParameter("@xingming", SqlDbType.NVarChar,255),
					new SqlParameter("@zhiwu", SqlDbType.NVarChar,255),
					new SqlParameter("@wxnicheng", SqlDbType.NVarChar,255),
					new SqlParameter("@shouji", SqlDbType.NVarChar,255),
					new SqlParameter("@beizhu", SqlDbType.NVarChar,255)};
			parameters[0].Value = model.sheng;
			parameters[1].Value = model.shi;
			parameters[2].Value = model.gongsi;
			parameters[3].Value = model.xingming;
			parameters[4].Value = model.zhiwu;
			parameters[5].Value = model.wxnicheng;
			parameters[6].Value = model.shouji;
			parameters[7].Value = model.beizhu;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete()
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from draw_zg_data ");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
			};

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.draw_zg_data GetModel()
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 sheng,shi,gongsi,xingming,zhiwu,wxnicheng,shouji,beizhu from draw_zg_data ");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
			};

			DTcms.Model.draw_zg_data model=new DTcms.Model.draw_zg_data();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.draw_zg_data DataRowToModel(DataRow row)
		{
			DTcms.Model.draw_zg_data model=new DTcms.Model.draw_zg_data();
			if (row != null)
			{
				if(row["sheng"]!=null)
				{
					model.sheng=row["sheng"].ToString();
				}
				if(row["shi"]!=null)
				{
					model.shi=row["shi"].ToString();
				}
				if(row["gongsi"]!=null)
				{
					model.gongsi=row["gongsi"].ToString();
				}
				if(row["xingming"]!=null)
				{
					model.xingming=row["xingming"].ToString();
				}
				if(row["zhiwu"]!=null)
				{
					model.zhiwu=row["zhiwu"].ToString();
				}
				if(row["wxnicheng"]!=null)
				{
					model.wxnicheng=row["wxnicheng"].ToString();
				}
				if(row["shouji"]!=null)
				{
					model.shouji=row["shouji"].ToString();
				}
				if(row["beizhu"]!=null)
				{
					model.beizhu=row["beizhu"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select sheng,shi,gongsi,xingming,zhiwu,wxnicheng,shouji,beizhu ");
			strSql.Append(" FROM draw_zg_data ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" sheng,shi,gongsi,xingming,zhiwu,wxnicheng,shouji,beizhu ");
			strSql.Append(" FROM draw_zg_data ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM draw_zg_data ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.id desc");
			}
			strSql.Append(")AS Row, T.*  from draw_zg_data T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "draw_zg_data";
			parameters[1].Value = "id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

