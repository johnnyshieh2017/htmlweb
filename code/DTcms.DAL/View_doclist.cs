﻿using System; 
using System.Text;
using System.Data.SqlClient;
using System.Collections.Generic; 
using System.Data;
using DTcms.DBUtility;
using DTcms.Common;
namespace DTcms.DAL  
{
	 	//View_doclist
		public partial class View_doclist
	{
   		     
		public bool Exists()
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from View_doclist");
			strSql.Append(" where ");
						SqlParameter[] parameters = {
			};

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}
		
				
		
		/// <summary>
		/// 增加一条数据
		/// </summary>
		public void Add(DTcms.Model.View_doclist model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into View_doclist(");			
            strSql.Append("id,name,title,gender,groupid,intro,logoimg,departmentid,createtime,status,isdelete,sort,siteid,mobile,groupname,titlename,department_name,hospitalname,hospitalid,idcard,idcardpic_front,idcardpic_back,doc_license,doc_qualification,doc_titlepic,speciality,position_id,hoslevel,rate,bookcount,responsetime");
			strSql.Append(") values (");
            strSql.Append("@id,@name,@title,@gender,@groupid,@intro,@logoimg,@departmentid,@createtime,@status,@isdelete,@sort,@siteid,@mobile,@groupname,@titlename,@department_name,@hospitalname,@hospitalid,@idcard,@idcardpic_front,@idcardpic_back,@doc_license,@doc_qualification,@doc_titlepic,@speciality,@position_id,@hoslevel,@rate,@bookcount,@responsetime");            
            strSql.Append(") ");            
            		
			SqlParameter[] parameters = {
			            new SqlParameter("@id", SqlDbType.Int,4) ,            
                        new SqlParameter("@name", SqlDbType.VarChar,50) ,            
                        new SqlParameter("@title", SqlDbType.Int,4) ,            
                        new SqlParameter("@gender", SqlDbType.VarChar,50) ,            
                        new SqlParameter("@groupid", SqlDbType.Int,4) ,            
                        new SqlParameter("@intro", SqlDbType.Text) ,            
                        new SqlParameter("@logoimg", SqlDbType.VarChar,100) ,            
                        new SqlParameter("@departmentid", SqlDbType.Int,4) ,            
                        new SqlParameter("@createtime", SqlDbType.DateTime) ,            
                        new SqlParameter("@status", SqlDbType.Int,4) ,            
                        new SqlParameter("@isdelete", SqlDbType.Int,4) ,            
                        new SqlParameter("@sort", SqlDbType.Int,4) ,            
                        new SqlParameter("@siteid", SqlDbType.Int,4) ,            
                        new SqlParameter("@mobile", SqlDbType.VarChar,50) ,            
                        new SqlParameter("@groupname", SqlDbType.VarChar,100) ,            
                        new SqlParameter("@titlename", SqlDbType.VarChar,50) ,            
                        new SqlParameter("@department_name", SqlDbType.VarChar,100) ,            
                        new SqlParameter("@hospitalname", SqlDbType.NVarChar,50) ,            
                        new SqlParameter("@hospitalid", SqlDbType.Int,4) ,            
                        new SqlParameter("@idcard", SqlDbType.VarChar,50) ,            
                        new SqlParameter("@idcardpic_front", SqlDbType.VarChar,100) ,            
                        new SqlParameter("@idcardpic_back", SqlDbType.VarChar,100) ,            
                        new SqlParameter("@doc_license", SqlDbType.VarChar,100) ,            
                        new SqlParameter("@doc_qualification", SqlDbType.VarChar,100) ,            
                        new SqlParameter("@doc_titlepic", SqlDbType.VarChar,100) ,            
                        new SqlParameter("@speciality", SqlDbType.NVarChar,-1) ,            
                        new SqlParameter("@position_id", SqlDbType.Int,4) ,            
                        new SqlParameter("@hoslevel", SqlDbType.VarChar,50) ,            
                        new SqlParameter("@rate", SqlDbType.VarChar,4) ,            
                        new SqlParameter("@bookcount", SqlDbType.Int,4) ,            
                        new SqlParameter("@responsetime", SqlDbType.VarChar,2)             
              
            };
			            
            parameters[0].Value = model.id;                        
            parameters[1].Value = model.name;                        
            parameters[2].Value = model.title;                        
            parameters[3].Value = model.gender;                        
            parameters[4].Value = model.groupid;                        
            parameters[5].Value = model.intro;                        
            parameters[6].Value = model.logoimg;                        
            parameters[7].Value = model.departmentid;                        
            parameters[8].Value = model.createtime;                        
            parameters[9].Value = model.status;                        
            parameters[10].Value = model.isdelete;                        
            parameters[11].Value = model.sort;                        
            parameters[12].Value = model.siteid;                        
            parameters[13].Value = model.mobile;                        
            parameters[14].Value = model.groupname;                        
            parameters[15].Value = model.titlename;                        
            parameters[16].Value = model.department_name;                        
            parameters[17].Value = model.hospitalname;                        
            parameters[18].Value = model.hospitalid;                        
            parameters[19].Value = model.idcard;                        
            parameters[20].Value = model.idcardpic_front;                        
            parameters[21].Value = model.idcardpic_back;                        
            parameters[22].Value = model.doc_license;                        
            parameters[23].Value = model.doc_qualification;                        
            parameters[24].Value = model.doc_titlepic;                        
            parameters[25].Value = model.speciality;                        
            parameters[26].Value = model.position_id;                        
            parameters[27].Value = model.hoslevel;                        
            parameters[28].Value = model.rate;                        
            parameters[29].Value = model.bookcount;                        
            parameters[30].Value = model.responsetime;                        
			            DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
            			
		}
		
		
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTcms.Model.View_doclist model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update View_doclist set ");
			                        
            strSql.Append(" id = @id , ");                                    
            strSql.Append(" name = @name , ");                                    
            strSql.Append(" title = @title , ");                                    
            strSql.Append(" gender = @gender , ");                                    
            strSql.Append(" groupid = @groupid , ");                                    
            strSql.Append(" intro = @intro , ");                                    
            strSql.Append(" logoimg = @logoimg , ");                                    
            strSql.Append(" departmentid = @departmentid , ");                                    
            strSql.Append(" createtime = @createtime , ");                                    
            strSql.Append(" status = @status , ");                                    
            strSql.Append(" isdelete = @isdelete , ");                                    
            strSql.Append(" sort = @sort , ");                                    
            strSql.Append(" siteid = @siteid , ");                                    
            strSql.Append(" mobile = @mobile , ");                                    
            strSql.Append(" groupname = @groupname , ");                                    
            strSql.Append(" titlename = @titlename , ");                                    
            strSql.Append(" department_name = @department_name , ");                                    
            strSql.Append(" hospitalname = @hospitalname , ");                                    
            strSql.Append(" hospitalid = @hospitalid , ");                                    
            strSql.Append(" idcard = @idcard , ");                                    
            strSql.Append(" idcardpic_front = @idcardpic_front , ");                                    
            strSql.Append(" idcardpic_back = @idcardpic_back , ");                                    
            strSql.Append(" doc_license = @doc_license , ");                                    
            strSql.Append(" doc_qualification = @doc_qualification , ");                                    
            strSql.Append(" doc_titlepic = @doc_titlepic , ");                                    
            strSql.Append(" speciality = @speciality , ");                                    
            strSql.Append(" position_id = @position_id , ");                                    
            strSql.Append(" hoslevel = @hoslevel , ");                                    
            strSql.Append(" rate = @rate , ");                                    
            strSql.Append(" bookcount = @bookcount , ");                                    
            strSql.Append(" responsetime = @responsetime  ");            			
			strSql.Append(" where  ");
						
SqlParameter[] parameters = {
			            new SqlParameter("@id", SqlDbType.Int,4) ,            
                        new SqlParameter("@name", SqlDbType.VarChar,50) ,            
                        new SqlParameter("@title", SqlDbType.Int,4) ,            
                        new SqlParameter("@gender", SqlDbType.VarChar,50) ,            
                        new SqlParameter("@groupid", SqlDbType.Int,4) ,            
                        new SqlParameter("@intro", SqlDbType.Text) ,            
                        new SqlParameter("@logoimg", SqlDbType.VarChar,100) ,            
                        new SqlParameter("@departmentid", SqlDbType.Int,4) ,            
                        new SqlParameter("@createtime", SqlDbType.DateTime) ,            
                        new SqlParameter("@status", SqlDbType.Int,4) ,            
                        new SqlParameter("@isdelete", SqlDbType.Int,4) ,            
                        new SqlParameter("@sort", SqlDbType.Int,4) ,            
                        new SqlParameter("@siteid", SqlDbType.Int,4) ,            
                        new SqlParameter("@mobile", SqlDbType.VarChar,50) ,            
                        new SqlParameter("@groupname", SqlDbType.VarChar,100) ,            
                        new SqlParameter("@titlename", SqlDbType.VarChar,50) ,            
                        new SqlParameter("@department_name", SqlDbType.VarChar,100) ,            
                        new SqlParameter("@hospitalname", SqlDbType.NVarChar,50) ,            
                        new SqlParameter("@hospitalid", SqlDbType.Int,4) ,            
                        new SqlParameter("@idcard", SqlDbType.VarChar,50) ,            
                        new SqlParameter("@idcardpic_front", SqlDbType.VarChar,100) ,            
                        new SqlParameter("@idcardpic_back", SqlDbType.VarChar,100) ,            
                        new SqlParameter("@doc_license", SqlDbType.VarChar,100) ,            
                        new SqlParameter("@doc_qualification", SqlDbType.VarChar,100) ,            
                        new SqlParameter("@doc_titlepic", SqlDbType.VarChar,100) ,            
                        new SqlParameter("@speciality", SqlDbType.NVarChar,-1) ,            
                        new SqlParameter("@position_id", SqlDbType.Int,4) ,            
                        new SqlParameter("@hoslevel", SqlDbType.VarChar,50) ,            
                        new SqlParameter("@rate", SqlDbType.VarChar,4) ,            
                        new SqlParameter("@bookcount", SqlDbType.Int,4) ,            
                        new SqlParameter("@responsetime", SqlDbType.VarChar,2)             
              
            };
						            
            parameters[0].Value = model.id;                        
            parameters[1].Value = model.name;                        
            parameters[2].Value = model.title;                        
            parameters[3].Value = model.gender;                        
            parameters[4].Value = model.groupid;                        
            parameters[5].Value = model.intro;                        
            parameters[6].Value = model.logoimg;                        
            parameters[7].Value = model.departmentid;                        
            parameters[8].Value = model.createtime;                        
            parameters[9].Value = model.status;                        
            parameters[10].Value = model.isdelete;                        
            parameters[11].Value = model.sort;                        
            parameters[12].Value = model.siteid;                        
            parameters[13].Value = model.mobile;                        
            parameters[14].Value = model.groupname;                        
            parameters[15].Value = model.titlename;                        
            parameters[16].Value = model.department_name;                        
            parameters[17].Value = model.hospitalname;                        
            parameters[18].Value = model.hospitalid;                        
            parameters[19].Value = model.idcard;                        
            parameters[20].Value = model.idcardpic_front;                        
            parameters[21].Value = model.idcardpic_back;                        
            parameters[22].Value = model.doc_license;                        
            parameters[23].Value = model.doc_qualification;                        
            parameters[24].Value = model.doc_titlepic;                        
            parameters[25].Value = model.speciality;                        
            parameters[26].Value = model.position_id;                        
            parameters[27].Value = model.hoslevel;                        
            parameters[28].Value = model.rate;                        
            parameters[29].Value = model.bookcount;                        
            parameters[30].Value = model.responsetime;                        
            int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete()
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from View_doclist ");
			strSql.Append(" where ");
						SqlParameter[] parameters = {
			};


			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
				
		
		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.View_doclist GetModel()
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select id, name, title, gender, groupid, intro, logoimg, departmentid, createtime, status, isdelete, sort, siteid, mobile, groupname, titlename, department_name, hospitalname, hospitalid, idcard, idcardpic_front, idcardpic_back, doc_license, doc_qualification, doc_titlepic, speciality, position_id, hoslevel, rate, bookcount, responsetime  ");			
			strSql.Append("  from View_doclist ");
			strSql.Append(" where ");
						SqlParameter[] parameters = {
			};

			
			DTcms.Model.View_doclist model=new DTcms.Model.View_doclist();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			
			if(ds.Tables[0].Rows.Count>0)
			{
												if(ds.Tables[0].Rows[0]["id"].ToString()!="")
				{
					model.id=int.Parse(ds.Tables[0].Rows[0]["id"].ToString());
				}
																																				model.name= ds.Tables[0].Rows[0]["name"].ToString();
																												if(ds.Tables[0].Rows[0]["title"].ToString()!="")
				{
					model.title=int.Parse(ds.Tables[0].Rows[0]["title"].ToString());
				}
																																				model.gender= ds.Tables[0].Rows[0]["gender"].ToString();
																												if(ds.Tables[0].Rows[0]["groupid"].ToString()!="")
				{
					model.groupid=int.Parse(ds.Tables[0].Rows[0]["groupid"].ToString());
				}
																																				model.intro= ds.Tables[0].Rows[0]["intro"].ToString();
																																model.logoimg= ds.Tables[0].Rows[0]["logoimg"].ToString();
																												if(ds.Tables[0].Rows[0]["departmentid"].ToString()!="")
				{
					model.departmentid=int.Parse(ds.Tables[0].Rows[0]["departmentid"].ToString());
				}
																																if(ds.Tables[0].Rows[0]["createtime"].ToString()!="")
				{
					model.createtime=DateTime.Parse(ds.Tables[0].Rows[0]["createtime"].ToString());
				}
																																if(ds.Tables[0].Rows[0]["status"].ToString()!="")
				{
					model.status=int.Parse(ds.Tables[0].Rows[0]["status"].ToString());
				}
																																if(ds.Tables[0].Rows[0]["isdelete"].ToString()!="")
				{
					model.isdelete=int.Parse(ds.Tables[0].Rows[0]["isdelete"].ToString());
				}
																																if(ds.Tables[0].Rows[0]["sort"].ToString()!="")
				{
					model.sort=int.Parse(ds.Tables[0].Rows[0]["sort"].ToString());
				}
																																if(ds.Tables[0].Rows[0]["siteid"].ToString()!="")
				{
					model.siteid=int.Parse(ds.Tables[0].Rows[0]["siteid"].ToString());
				}
																																				model.mobile= ds.Tables[0].Rows[0]["mobile"].ToString();
																																model.groupname= ds.Tables[0].Rows[0]["groupname"].ToString();
																																model.titlename= ds.Tables[0].Rows[0]["titlename"].ToString();
																																model.department_name= ds.Tables[0].Rows[0]["department_name"].ToString();
																																model.hospitalname= ds.Tables[0].Rows[0]["hospitalname"].ToString();
																												if(ds.Tables[0].Rows[0]["hospitalid"].ToString()!="")
				{
					model.hospitalid=int.Parse(ds.Tables[0].Rows[0]["hospitalid"].ToString());
				}
																																				model.idcard= ds.Tables[0].Rows[0]["idcard"].ToString();
																																model.idcardpic_front= ds.Tables[0].Rows[0]["idcardpic_front"].ToString();
																																model.idcardpic_back= ds.Tables[0].Rows[0]["idcardpic_back"].ToString();
																																model.doc_license= ds.Tables[0].Rows[0]["doc_license"].ToString();
																																model.doc_qualification= ds.Tables[0].Rows[0]["doc_qualification"].ToString();
																																model.doc_titlepic= ds.Tables[0].Rows[0]["doc_titlepic"].ToString();
																																model.speciality= ds.Tables[0].Rows[0]["speciality"].ToString();
																												if(ds.Tables[0].Rows[0]["position_id"].ToString()!="")
				{
					model.position_id=int.Parse(ds.Tables[0].Rows[0]["position_id"].ToString());
				}
																																				model.hoslevel= ds.Tables[0].Rows[0]["hoslevel"].ToString();
																																model.rate= ds.Tables[0].Rows[0]["rate"].ToString();
																												if(ds.Tables[0].Rows[0]["bookcount"].ToString()!="")
				{
					model.bookcount=int.Parse(ds.Tables[0].Rows[0]["bookcount"].ToString());
				}
																																				model.responsetime= ds.Tables[0].Rows[0]["responsetime"].ToString();
																										
				return model;
			}
			else
			{
				return null;
			}
		}
		
		
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select * ");
			strSql.Append(" FROM View_doclist ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}
		
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" * ");
			strSql.Append(" FROM View_doclist ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得查询分页数据
		/// </summary>
		public DataSet GetList(int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
		 {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * FROM View_doclist ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            recordCount = Convert.ToInt32(DbHelperSQL.GetSingle(PagingHelper.CreateCountingSql(strSql.ToString())));
            return DbHelperSQL.Query(PagingHelper.CreatePagingSql(recordCount, pageSize, pageIndex, strSql.ToString(), filedOrder));
		}

   
	}
}

