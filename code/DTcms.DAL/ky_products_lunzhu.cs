﻿/**  版本信息模板在安装目录下，可自行修改。
* ky_products_lunzhu.cs
*
* 功 能： N/A
* 类 名： ky_products_lunzhu
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2021/7/14 16:21:21   N/A    初版
*

*/
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DTcms.DBUtility;//Please add references
using DTcms.Common;

namespace DTcms.DAL
{
	/// <summary>
	/// 数据访问类:ky_products_lunzhu
	/// </summary>
	public partial class ky_products_lunzhu
	{
		public ky_products_lunzhu()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("pd_id", "ky_products_lunzhu"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int pd_id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from ky_products_lunzhu");
			strSql.Append(" where pd_id=@pd_id");
			SqlParameter[] parameters = {
					new SqlParameter("@pd_id", SqlDbType.Int,4)
			};
			parameters[0].Value = pd_id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(DTcms.Model.ky_products_lunzhu model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into ky_products_lunzhu(");
			strSql.Append("add_time,add_user,p_id,shu_ji_ming_cheng,chu_ban_she,dptid,chu_ban_ri_qi,zuo_zhe,zuo_zhe_lei_xing,bian_xie_zhang_jie,beizhu)");
			strSql.Append(" values (");
			strSql.Append("@add_time,@add_user,@p_id,@shu_ji_ming_cheng,@chu_ban_she,@dptid,@chu_ban_ri_qi,@zuo_zhe,@zuo_zhe_lei_xing,@bian_xie_zhang_jie,@beizhu)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@add_time", SqlDbType.DateTime),
					new SqlParameter("@add_user", SqlDbType.VarChar,50),
					new SqlParameter("@p_id", SqlDbType.Int,4),
					new SqlParameter("@shu_ji_ming_cheng", SqlDbType.VarChar,50),
					new SqlParameter("@chu_ban_she", SqlDbType.VarChar,50),
					new SqlParameter("@dptid", SqlDbType.Int,4),
					new SqlParameter("@chu_ban_ri_qi", SqlDbType.VarChar,50),
					new SqlParameter("@zuo_zhe", SqlDbType.VarChar,50),
					new SqlParameter("@zuo_zhe_lei_xing", SqlDbType.VarChar,50),
					new SqlParameter("@bian_xie_zhang_jie", SqlDbType.VarChar,50),
					new SqlParameter("@beizhu", SqlDbType.VarChar,200)};
			parameters[0].Value = model.add_time;
			parameters[1].Value = model.add_user;
			parameters[2].Value = model.p_id;
			parameters[3].Value = model.shu_ji_ming_cheng;
			parameters[4].Value = model.chu_ban_she;
			parameters[5].Value = model.dptid;
			parameters[6].Value = model.chu_ban_ri_qi;
			parameters[7].Value = model.zuo_zhe;
			parameters[8].Value = model.zuo_zhe_lei_xing;
			parameters[9].Value = model.bian_xie_zhang_jie;
			parameters[10].Value = model.beizhu;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTcms.Model.ky_products_lunzhu model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update ky_products_lunzhu set ");
			strSql.Append("add_time=@add_time,");
			strSql.Append("add_user=@add_user,");
			strSql.Append("p_id=@p_id,");
			strSql.Append("shu_ji_ming_cheng=@shu_ji_ming_cheng,");
			strSql.Append("chu_ban_she=@chu_ban_she,");
			strSql.Append("dptid=@dptid,");
			strSql.Append("chu_ban_ri_qi=@chu_ban_ri_qi,");
			strSql.Append("zuo_zhe=@zuo_zhe,");
			strSql.Append("zuo_zhe_lei_xing=@zuo_zhe_lei_xing,");
			strSql.Append("bian_xie_zhang_jie=@bian_xie_zhang_jie,");
			strSql.Append("beizhu=@beizhu");
			strSql.Append(" where pd_id=@pd_id");
			SqlParameter[] parameters = {
					new SqlParameter("@add_time", SqlDbType.DateTime),
					new SqlParameter("@add_user", SqlDbType.VarChar,50),
					new SqlParameter("@p_id", SqlDbType.Int,4),
					new SqlParameter("@shu_ji_ming_cheng", SqlDbType.VarChar,50),
					new SqlParameter("@chu_ban_she", SqlDbType.VarChar,50),
					new SqlParameter("@dptid", SqlDbType.Int,4),
					new SqlParameter("@chu_ban_ri_qi", SqlDbType.VarChar,50),
					new SqlParameter("@zuo_zhe", SqlDbType.VarChar,50),
					new SqlParameter("@zuo_zhe_lei_xing", SqlDbType.VarChar,50),
					new SqlParameter("@bian_xie_zhang_jie", SqlDbType.VarChar,50),
					new SqlParameter("@beizhu", SqlDbType.VarChar,200),
					new SqlParameter("@pd_id", SqlDbType.Int,4)};
			parameters[0].Value = model.add_time;
			parameters[1].Value = model.add_user;
			parameters[2].Value = model.p_id;
			parameters[3].Value = model.shu_ji_ming_cheng;
			parameters[4].Value = model.chu_ban_she;
			parameters[5].Value = model.dptid;
			parameters[6].Value = model.chu_ban_ri_qi;
			parameters[7].Value = model.zuo_zhe;
			parameters[8].Value = model.zuo_zhe_lei_xing;
			parameters[9].Value = model.bian_xie_zhang_jie;
			parameters[10].Value = model.beizhu;
			parameters[11].Value = model.pd_id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int pd_id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from ky_products_lunzhu ");
			strSql.Append(" where pd_id=@pd_id");
			SqlParameter[] parameters = {
					new SqlParameter("@pd_id", SqlDbType.Int,4)
			};
			parameters[0].Value = pd_id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string pd_idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from ky_products_lunzhu ");
			strSql.Append(" where pd_id in ("+pd_idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.ky_products_lunzhu GetModel(int pd_id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 pd_id,add_time,add_user,p_id,shu_ji_ming_cheng,chu_ban_she,dptid,chu_ban_ri_qi,zuo_zhe,zuo_zhe_lei_xing,bian_xie_zhang_jie,beizhu from ky_products_lunzhu ");
			strSql.Append(" where pd_id=@pd_id");
			SqlParameter[] parameters = {
					new SqlParameter("@pd_id", SqlDbType.Int,4)
			};
			parameters[0].Value = pd_id;

			DTcms.Model.ky_products_lunzhu model=new DTcms.Model.ky_products_lunzhu();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.ky_products_lunzhu DataRowToModel(DataRow row)
		{
			DTcms.Model.ky_products_lunzhu model=new DTcms.Model.ky_products_lunzhu();
			if (row != null)
			{
				if(row["pd_id"]!=null && row["pd_id"].ToString()!="")
				{
					model.pd_id=int.Parse(row["pd_id"].ToString());
				}
				if(row["add_time"]!=null && row["add_time"].ToString()!="")
				{
					model.add_time=DateTime.Parse(row["add_time"].ToString());
				}
				if(row["add_user"]!=null)
				{
					model.add_user=row["add_user"].ToString();
				}
				if(row["p_id"]!=null && row["p_id"].ToString()!="")
				{
					model.p_id=int.Parse(row["p_id"].ToString());
				}
				if(row["shu_ji_ming_cheng"]!=null)
				{
					model.shu_ji_ming_cheng=row["shu_ji_ming_cheng"].ToString();
				}
				if(row["chu_ban_she"]!=null)
				{
					model.chu_ban_she=row["chu_ban_she"].ToString();
				}
				if(row["dptid"]!=null && row["dptid"].ToString()!="")
				{
					model.dptid=int.Parse(row["dptid"].ToString());
				}
				if(row["chu_ban_ri_qi"]!=null)
				{
					model.chu_ban_ri_qi=row["chu_ban_ri_qi"].ToString();
				}
				if(row["zuo_zhe"]!=null)
				{
					model.zuo_zhe=row["zuo_zhe"].ToString();
				}
				if(row["zuo_zhe_lei_xing"]!=null)
				{
					model.zuo_zhe_lei_xing=row["zuo_zhe_lei_xing"].ToString();
				}
				if(row["bian_xie_zhang_jie"]!=null)
				{
					model.bian_xie_zhang_jie=row["bian_xie_zhang_jie"].ToString();
				}
				if(row["beizhu"]!=null)
				{
					model.beizhu=row["beizhu"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select pd_id,add_time,add_user,p_id,shu_ji_ming_cheng,chu_ban_she,dptid,chu_ban_ri_qi,zuo_zhe,zuo_zhe_lei_xing,bian_xie_zhang_jie,beizhu ");
			strSql.Append(" FROM ky_products_lunzhu ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" pd_id,add_time,add_user,p_id,shu_ji_ming_cheng,chu_ban_she,dptid,chu_ban_ri_qi,zuo_zhe,zuo_zhe_lei_xing,bian_xie_zhang_jie,beizhu ");
			strSql.Append(" FROM ky_products_lunzhu ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM ky_products_lunzhu ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.pd_id desc");
			}
			strSql.Append(")AS Row, T.*  from ky_products_lunzhu T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "ky_products_lunzhu";
			parameters[1].Value = "pd_id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod
		public DataSet GetList(int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
		{
			StringBuilder strSql = new StringBuilder();
			strSql.Append("select * FROM View_ky_products_lunzhu ");
			if (strWhere.Trim() != "")
			{
				strSql.Append(" where " + strWhere);
			}
			recordCount = Convert.ToInt32(DbHelperSQL.GetSingle(PagingHelper.CreateCountingSql(strSql.ToString())));
			return DbHelperSQL.Query(PagingHelper.CreatePagingSql(recordCount, pageSize, pageIndex, strSql.ToString(), filedOrder));
		}
		#endregion  ExtensionMethod
	}
}

