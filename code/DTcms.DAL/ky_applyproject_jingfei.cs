﻿/**  版本信息模板在安装目录下，可自行修改。
* ky_applyproject_jingfei.cs
*
* 功 能： N/A
* 类 名： ky_applyproject_jingfei
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2021/8/2 17:33:21   N/A    初版

*/
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DTcms.DBUtility;//Please add references
using DTcms.Common;

namespace DTcms.DAL
{
	/// <summary>
	/// 数据访问类:ky_applyproject_jingfei
	/// </summary>
	public partial class ky_applyproject_jingfei
	{
		public ky_applyproject_jingfei()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("id", "ky_applyproject_jingfei"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from ky_applyproject_jingfei");
			strSql.Append(" where id=@id");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
			parameters[0].Value = id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(DTcms.Model.ky_applyproject_jingfei model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into ky_applyproject_jingfei(");
			strSql.Append("pa_id,laiyuan_zhuanxiang,laiyuan_qita,zhijie_shebei,zhijie_sb_gouzhi,zhijie_sb_shizhi,zhijie_sb_gaizao,zhijie_cailiao,zhijie_ceshi,zhijie_ranliao,zhijie_chailv,zhijie_huiyi,zhijie_hezuojiaoliu,zhijie_xinxi,zhijie_zixun,zhijie_laowu,zhijie_qita,jianjie_guanli,jianjie_jixiao,zhijie_shebei1,zhijie_sb_gouzhi1,zhijie_sb_shizhi1,zhijie_sb_gaizao1,zhijie_cailiao1,zhijie_ceshi1,zhijie_ranliao1,zhijie_chailv1,zhijie_huiyi1,zhijie_hezuojiaoliu1,zhijie_xinxi1,zhijie_zixun1,zhijie_laowu1,zhijie_qita1,jianjie_guanli1,jianjie_jixiao1,zhijie_shebei2,zhijie_sb_gouzhi2,zhijie_sb_shizhi2,zhijie_sb_gaizao2,zhijie_cailiao2,zhijie_ceshi2,zhijie_ranliao2,zhijie_chailv2,zhijie_huiyi2,zhijie_hezuojiaoliu2,zhijie_xinxi2,zhijie_zixun2,zhijie_laowu2,zhijie_qita2,jianjie_guanli2,jianjie_jixiao2,add_time,add_user,status,zhijie1,zhijie2,zhijie,jianjie,jianjie1,jianjie2,heji,heji1,heji2)");
			strSql.Append(" values (");
			strSql.Append("@pa_id,@laiyuan_zhuanxiang,@laiyuan_qita,@zhijie_shebei,@zhijie_sb_gouzhi,@zhijie_sb_shizhi,@zhijie_sb_gaizao,@zhijie_cailiao,@zhijie_ceshi,@zhijie_ranliao,@zhijie_chailv,@zhijie_huiyi,@zhijie_hezuojiaoliu,@zhijie_xinxi,@zhijie_zixun,@zhijie_laowu,@zhijie_qita,@jianjie_guanli,@jianjie_jixiao,@zhijie_shebei1,@zhijie_sb_gouzhi1,@zhijie_sb_shizhi1,@zhijie_sb_gaizao1,@zhijie_cailiao1,@zhijie_ceshi1,@zhijie_ranliao1,@zhijie_chailv1,@zhijie_huiyi1,@zhijie_hezuojiaoliu1,@zhijie_xinxi1,@zhijie_zixun1,@zhijie_laowu1,@zhijie_qita1,@jianjie_guanli1,@jianjie_jixiao1,@zhijie_shebei2,@zhijie_sb_gouzhi2,@zhijie_sb_shizhi2,@zhijie_sb_gaizao2,@zhijie_cailiao2,@zhijie_ceshi2,@zhijie_ranliao2,@zhijie_chailv2,@zhijie_huiyi2,@zhijie_hezuojiaoliu2,@zhijie_xinxi2,@zhijie_zixun2,@zhijie_laowu2,@zhijie_qita2,@jianjie_guanli2,@jianjie_jixiao2,@add_time,@add_user,@status,@zhijie1,@zhijie2,@zhijie,@jianjie,@jianjie1,@jianjie2,@heji,@heji1,@heji2)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@pa_id", SqlDbType.VarChar,50),
					new SqlParameter("@laiyuan_zhuanxiang", SqlDbType.VarChar,50),
					new SqlParameter("@laiyuan_qita", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_shebei", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_sb_gouzhi", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_sb_shizhi", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_sb_gaizao", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_cailiao", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_ceshi", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_ranliao", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_chailv", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_huiyi", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_hezuojiaoliu", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_xinxi", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_zixun", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_laowu", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_qita", SqlDbType.VarChar,50),
					new SqlParameter("@jianjie_guanli", SqlDbType.VarChar,50),
					new SqlParameter("@jianjie_jixiao", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_shebei1", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_sb_gouzhi1", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_sb_shizhi1", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_sb_gaizao1", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_cailiao1", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_ceshi1", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_ranliao1", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_chailv1", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_huiyi1", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_hezuojiaoliu1", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_xinxi1", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_zixun1", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_laowu1", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_qita1", SqlDbType.VarChar,50),
					new SqlParameter("@jianjie_guanli1", SqlDbType.VarChar,50),
					new SqlParameter("@jianjie_jixiao1", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_shebei2", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_sb_gouzhi2", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_sb_shizhi2", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_sb_gaizao2", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_cailiao2", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_ceshi2", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_ranliao2", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_chailv2", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_huiyi2", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_hezuojiaoliu2", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_xinxi2", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_zixun2", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_laowu2", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_qita2", SqlDbType.VarChar,50),
					new SqlParameter("@jianjie_guanli2", SqlDbType.VarChar,50),
					new SqlParameter("@jianjie_jixiao2", SqlDbType.VarChar,50),
					new SqlParameter("@add_time", SqlDbType.DateTime),
					new SqlParameter("@add_user", SqlDbType.VarChar,50),
					new SqlParameter("@status", SqlDbType.Int,4),
					new SqlParameter("@zhijie1", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie2", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie", SqlDbType.VarChar,50),
					new SqlParameter("@jianjie", SqlDbType.VarChar,50),
					new SqlParameter("@jianjie1", SqlDbType.VarChar,50),
					new SqlParameter("@jianjie2", SqlDbType.VarChar,50),
					new SqlParameter("@heji", SqlDbType.VarChar,50),
					new SqlParameter("@heji1", SqlDbType.VarChar,50),
					new SqlParameter("@heji2", SqlDbType.VarChar,50)};
			parameters[0].Value = model.pa_id;
			parameters[1].Value = model.laiyuan_zhuanxiang;
			parameters[2].Value = model.laiyuan_qita;
			parameters[3].Value = model.zhijie_shebei;
			parameters[4].Value = model.zhijie_sb_gouzhi;
			parameters[5].Value = model.zhijie_sb_shizhi;
			parameters[6].Value = model.zhijie_sb_gaizao;
			parameters[7].Value = model.zhijie_cailiao;
			parameters[8].Value = model.zhijie_ceshi;
			parameters[9].Value = model.zhijie_ranliao;
			parameters[10].Value = model.zhijie_chailv;
			parameters[11].Value = model.zhijie_huiyi;
			parameters[12].Value = model.zhijie_hezuojiaoliu;
			parameters[13].Value = model.zhijie_xinxi;
			parameters[14].Value = model.zhijie_zixun;
			parameters[15].Value = model.zhijie_laowu;
			parameters[16].Value = model.zhijie_qita;
			parameters[17].Value = model.jianjie_guanli;
			parameters[18].Value = model.jianjie_jixiao;
			parameters[19].Value = model.zhijie_shebei1;
			parameters[20].Value = model.zhijie_sb_gouzhi1;
			parameters[21].Value = model.zhijie_sb_shizhi1;
			parameters[22].Value = model.zhijie_sb_gaizao1;
			parameters[23].Value = model.zhijie_cailiao1;
			parameters[24].Value = model.zhijie_ceshi1;
			parameters[25].Value = model.zhijie_ranliao1;
			parameters[26].Value = model.zhijie_chailv1;
			parameters[27].Value = model.zhijie_huiyi1;
			parameters[28].Value = model.zhijie_hezuojiaoliu1;
			parameters[29].Value = model.zhijie_xinxi1;
			parameters[30].Value = model.zhijie_zixun1;
			parameters[31].Value = model.zhijie_laowu1;
			parameters[32].Value = model.zhijie_qita1;
			parameters[33].Value = model.jianjie_guanli1;
			parameters[34].Value = model.jianjie_jixiao1;
			parameters[35].Value = model.zhijie_shebei2;
			parameters[36].Value = model.zhijie_sb_gouzhi2;
			parameters[37].Value = model.zhijie_sb_shizhi2;
			parameters[38].Value = model.zhijie_sb_gaizao2;
			parameters[39].Value = model.zhijie_cailiao2;
			parameters[40].Value = model.zhijie_ceshi2;
			parameters[41].Value = model.zhijie_ranliao2;
			parameters[42].Value = model.zhijie_chailv2;
			parameters[43].Value = model.zhijie_huiyi2;
			parameters[44].Value = model.zhijie_hezuojiaoliu2;
			parameters[45].Value = model.zhijie_xinxi2;
			parameters[46].Value = model.zhijie_zixun2;
			parameters[47].Value = model.zhijie_laowu2;
			parameters[48].Value = model.zhijie_qita2;
			parameters[49].Value = model.jianjie_guanli2;
			parameters[50].Value = model.jianjie_jixiao2;
			parameters[51].Value = model.add_time;
			parameters[52].Value = model.add_user;
			parameters[53].Value = model.status;
			parameters[54].Value = model.zhijie1;
			parameters[55].Value = model.zhijie2;
			parameters[56].Value = model.zhijie;
			parameters[57].Value = model.jianjie;
			parameters[58].Value = model.jianjie1;
			parameters[59].Value = model.jianjie2;
			parameters[60].Value = model.heji;
			parameters[61].Value = model.heji1;
			parameters[62].Value = model.heji2;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTcms.Model.ky_applyproject_jingfei model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update ky_applyproject_jingfei set ");
			strSql.Append("pa_id=@pa_id,");
			strSql.Append("laiyuan_zhuanxiang=@laiyuan_zhuanxiang,");
			strSql.Append("laiyuan_qita=@laiyuan_qita,");
			strSql.Append("zhijie_shebei=@zhijie_shebei,");
			strSql.Append("zhijie_sb_gouzhi=@zhijie_sb_gouzhi,");
			strSql.Append("zhijie_sb_shizhi=@zhijie_sb_shizhi,");
			strSql.Append("zhijie_sb_gaizao=@zhijie_sb_gaizao,");
			strSql.Append("zhijie_cailiao=@zhijie_cailiao,");
			strSql.Append("zhijie_ceshi=@zhijie_ceshi,");
			strSql.Append("zhijie_ranliao=@zhijie_ranliao,");
			strSql.Append("zhijie_chailv=@zhijie_chailv,");
			strSql.Append("zhijie_huiyi=@zhijie_huiyi,");
			strSql.Append("zhijie_hezuojiaoliu=@zhijie_hezuojiaoliu,");
			strSql.Append("zhijie_xinxi=@zhijie_xinxi,");
			strSql.Append("zhijie_zixun=@zhijie_zixun,");
			strSql.Append("zhijie_laowu=@zhijie_laowu,");
			strSql.Append("zhijie_qita=@zhijie_qita,");
			strSql.Append("jianjie_guanli=@jianjie_guanli,");
			strSql.Append("jianjie_jixiao=@jianjie_jixiao,");
			strSql.Append("zhijie_shebei1=@zhijie_shebei1,");
			strSql.Append("zhijie_sb_gouzhi1=@zhijie_sb_gouzhi1,");
			strSql.Append("zhijie_sb_shizhi1=@zhijie_sb_shizhi1,");
			strSql.Append("zhijie_sb_gaizao1=@zhijie_sb_gaizao1,");
			strSql.Append("zhijie_cailiao1=@zhijie_cailiao1,");
			strSql.Append("zhijie_ceshi1=@zhijie_ceshi1,");
			strSql.Append("zhijie_ranliao1=@zhijie_ranliao1,");
			strSql.Append("zhijie_chailv1=@zhijie_chailv1,");
			strSql.Append("zhijie_huiyi1=@zhijie_huiyi1,");
			strSql.Append("zhijie_hezuojiaoliu1=@zhijie_hezuojiaoliu1,");
			strSql.Append("zhijie_xinxi1=@zhijie_xinxi1,");
			strSql.Append("zhijie_zixun1=@zhijie_zixun1,");
			strSql.Append("zhijie_laowu1=@zhijie_laowu1,");
			strSql.Append("zhijie_qita1=@zhijie_qita1,");
			strSql.Append("jianjie_guanli1=@jianjie_guanli1,");
			strSql.Append("jianjie_jixiao1=@jianjie_jixiao1,");
			strSql.Append("zhijie_shebei2=@zhijie_shebei2,");
			strSql.Append("zhijie_sb_gouzhi2=@zhijie_sb_gouzhi2,");
			strSql.Append("zhijie_sb_shizhi2=@zhijie_sb_shizhi2,");
			strSql.Append("zhijie_sb_gaizao2=@zhijie_sb_gaizao2,");
			strSql.Append("zhijie_cailiao2=@zhijie_cailiao2,");
			strSql.Append("zhijie_ceshi2=@zhijie_ceshi2,");
			strSql.Append("zhijie_ranliao2=@zhijie_ranliao2,");
			strSql.Append("zhijie_chailv2=@zhijie_chailv2,");
			strSql.Append("zhijie_huiyi2=@zhijie_huiyi2,");
			strSql.Append("zhijie_hezuojiaoliu2=@zhijie_hezuojiaoliu2,");
			strSql.Append("zhijie_xinxi2=@zhijie_xinxi2,");
			strSql.Append("zhijie_zixun2=@zhijie_zixun2,");
			strSql.Append("zhijie_laowu2=@zhijie_laowu2,");
			strSql.Append("zhijie_qita2=@zhijie_qita2,");
			strSql.Append("jianjie_guanli2=@jianjie_guanli2,");
			strSql.Append("jianjie_jixiao2=@jianjie_jixiao2,");
			strSql.Append("add_time=@add_time,");
			strSql.Append("add_user=@add_user,");
			strSql.Append("status=@status,");
			strSql.Append("zhijie1=@zhijie1,");
			strSql.Append("zhijie2=@zhijie2,");
			strSql.Append("zhijie=@zhijie,");
			strSql.Append("jianjie=@jianjie,");
			strSql.Append("jianjie1=@jianjie1,");
			strSql.Append("jianjie2=@jianjie2,");
			strSql.Append("heji=@heji,");
			strSql.Append("heji1=@heji1,");
			strSql.Append("heji2=@heji2");
			strSql.Append(" where id=@id");
			SqlParameter[] parameters = {
					new SqlParameter("@pa_id", SqlDbType.VarChar,50),
					new SqlParameter("@laiyuan_zhuanxiang", SqlDbType.VarChar,50),
					new SqlParameter("@laiyuan_qita", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_shebei", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_sb_gouzhi", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_sb_shizhi", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_sb_gaizao", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_cailiao", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_ceshi", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_ranliao", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_chailv", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_huiyi", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_hezuojiaoliu", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_xinxi", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_zixun", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_laowu", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_qita", SqlDbType.VarChar,50),
					new SqlParameter("@jianjie_guanli", SqlDbType.VarChar,50),
					new SqlParameter("@jianjie_jixiao", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_shebei1", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_sb_gouzhi1", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_sb_shizhi1", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_sb_gaizao1", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_cailiao1", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_ceshi1", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_ranliao1", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_chailv1", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_huiyi1", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_hezuojiaoliu1", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_xinxi1", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_zixun1", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_laowu1", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_qita1", SqlDbType.VarChar,50),
					new SqlParameter("@jianjie_guanli1", SqlDbType.VarChar,50),
					new SqlParameter("@jianjie_jixiao1", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_shebei2", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_sb_gouzhi2", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_sb_shizhi2", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_sb_gaizao2", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_cailiao2", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_ceshi2", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_ranliao2", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_chailv2", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_huiyi2", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_hezuojiaoliu2", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_xinxi2", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_zixun2", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_laowu2", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie_qita2", SqlDbType.VarChar,50),
					new SqlParameter("@jianjie_guanli2", SqlDbType.VarChar,50),
					new SqlParameter("@jianjie_jixiao2", SqlDbType.VarChar,50),
					new SqlParameter("@add_time", SqlDbType.DateTime),
					new SqlParameter("@add_user", SqlDbType.VarChar,50),
					new SqlParameter("@status", SqlDbType.Int,4),
					new SqlParameter("@zhijie1", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie2", SqlDbType.VarChar,50),
					new SqlParameter("@zhijie", SqlDbType.VarChar,50),
					new SqlParameter("@jianjie", SqlDbType.VarChar,50),
					new SqlParameter("@jianjie1", SqlDbType.VarChar,50),
					new SqlParameter("@jianjie2", SqlDbType.VarChar,50),
					new SqlParameter("@heji", SqlDbType.VarChar,50),
					new SqlParameter("@heji1", SqlDbType.VarChar,50),
					new SqlParameter("@heji2", SqlDbType.VarChar,50),
					new SqlParameter("@id", SqlDbType.Int,4)};
			parameters[0].Value = model.pa_id;
			parameters[1].Value = model.laiyuan_zhuanxiang;
			parameters[2].Value = model.laiyuan_qita;
			parameters[3].Value = model.zhijie_shebei;
			parameters[4].Value = model.zhijie_sb_gouzhi;
			parameters[5].Value = model.zhijie_sb_shizhi;
			parameters[6].Value = model.zhijie_sb_gaizao;
			parameters[7].Value = model.zhijie_cailiao;
			parameters[8].Value = model.zhijie_ceshi;
			parameters[9].Value = model.zhijie_ranliao;
			parameters[10].Value = model.zhijie_chailv;
			parameters[11].Value = model.zhijie_huiyi;
			parameters[12].Value = model.zhijie_hezuojiaoliu;
			parameters[13].Value = model.zhijie_xinxi;
			parameters[14].Value = model.zhijie_zixun;
			parameters[15].Value = model.zhijie_laowu;
			parameters[16].Value = model.zhijie_qita;
			parameters[17].Value = model.jianjie_guanli;
			parameters[18].Value = model.jianjie_jixiao;
			parameters[19].Value = model.zhijie_shebei1;
			parameters[20].Value = model.zhijie_sb_gouzhi1;
			parameters[21].Value = model.zhijie_sb_shizhi1;
			parameters[22].Value = model.zhijie_sb_gaizao1;
			parameters[23].Value = model.zhijie_cailiao1;
			parameters[24].Value = model.zhijie_ceshi1;
			parameters[25].Value = model.zhijie_ranliao1;
			parameters[26].Value = model.zhijie_chailv1;
			parameters[27].Value = model.zhijie_huiyi1;
			parameters[28].Value = model.zhijie_hezuojiaoliu1;
			parameters[29].Value = model.zhijie_xinxi1;
			parameters[30].Value = model.zhijie_zixun1;
			parameters[31].Value = model.zhijie_laowu1;
			parameters[32].Value = model.zhijie_qita1;
			parameters[33].Value = model.jianjie_guanli1;
			parameters[34].Value = model.jianjie_jixiao1;
			parameters[35].Value = model.zhijie_shebei2;
			parameters[36].Value = model.zhijie_sb_gouzhi2;
			parameters[37].Value = model.zhijie_sb_shizhi2;
			parameters[38].Value = model.zhijie_sb_gaizao2;
			parameters[39].Value = model.zhijie_cailiao2;
			parameters[40].Value = model.zhijie_ceshi2;
			parameters[41].Value = model.zhijie_ranliao2;
			parameters[42].Value = model.zhijie_chailv2;
			parameters[43].Value = model.zhijie_huiyi2;
			parameters[44].Value = model.zhijie_hezuojiaoliu2;
			parameters[45].Value = model.zhijie_xinxi2;
			parameters[46].Value = model.zhijie_zixun2;
			parameters[47].Value = model.zhijie_laowu2;
			parameters[48].Value = model.zhijie_qita2;
			parameters[49].Value = model.jianjie_guanli2;
			parameters[50].Value = model.jianjie_jixiao2;
			parameters[51].Value = model.add_time;
			parameters[52].Value = model.add_user;
			parameters[53].Value = model.status;
			parameters[54].Value = model.zhijie1;
			parameters[55].Value = model.zhijie2;
			parameters[56].Value = model.zhijie;
			parameters[57].Value = model.jianjie;
			parameters[58].Value = model.jianjie1;
			parameters[59].Value = model.jianjie2;
			parameters[60].Value = model.heji;
			parameters[61].Value = model.heji1;
			parameters[62].Value = model.heji2;
			parameters[63].Value = model.id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from ky_applyproject_jingfei ");
			strSql.Append(" where id=@id");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
			parameters[0].Value = id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from ky_applyproject_jingfei ");
			strSql.Append(" where id in ("+idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.ky_applyproject_jingfei GetModel(int id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 id,pa_id,laiyuan_zhuanxiang,laiyuan_qita,zhijie_shebei,zhijie_sb_gouzhi,zhijie_sb_shizhi,zhijie_sb_gaizao,zhijie_cailiao,zhijie_ceshi,zhijie_ranliao,zhijie_chailv,zhijie_huiyi,zhijie_hezuojiaoliu,zhijie_xinxi,zhijie_zixun,zhijie_laowu,zhijie_qita,jianjie_guanli,jianjie_jixiao,zhijie_shebei1,zhijie_sb_gouzhi1,zhijie_sb_shizhi1,zhijie_sb_gaizao1,zhijie_cailiao1,zhijie_ceshi1,zhijie_ranliao1,zhijie_chailv1,zhijie_huiyi1,zhijie_hezuojiaoliu1,zhijie_xinxi1,zhijie_zixun1,zhijie_laowu1,zhijie_qita1,jianjie_guanli1,jianjie_jixiao1,zhijie_shebei2,zhijie_sb_gouzhi2,zhijie_sb_shizhi2,zhijie_sb_gaizao2,zhijie_cailiao2,zhijie_ceshi2,zhijie_ranliao2,zhijie_chailv2,zhijie_huiyi2,zhijie_hezuojiaoliu2,zhijie_xinxi2,zhijie_zixun2,zhijie_laowu2,zhijie_qita2,jianjie_guanli2,jianjie_jixiao2,add_time,add_user,status,zhijie1,zhijie2,zhijie,jianjie,jianjie1,jianjie2,heji,heji1,heji2 from ky_applyproject_jingfei ");
			strSql.Append(" where id=@id");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
			parameters[0].Value = id;

			DTcms.Model.ky_applyproject_jingfei model=new DTcms.Model.ky_applyproject_jingfei();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.ky_applyproject_jingfei DataRowToModel(DataRow row)
		{
			DTcms.Model.ky_applyproject_jingfei model=new DTcms.Model.ky_applyproject_jingfei();
			if (row != null)
			{
				if(row["id"]!=null && row["id"].ToString()!="")
				{
					model.id=int.Parse(row["id"].ToString());
				}
				if(row["pa_id"]!=null)
				{
					model.pa_id=row["pa_id"].ToString();
				}
				if(row["laiyuan_zhuanxiang"]!=null)
				{
					model.laiyuan_zhuanxiang=row["laiyuan_zhuanxiang"].ToString();
				}
				if(row["laiyuan_qita"]!=null)
				{
					model.laiyuan_qita=row["laiyuan_qita"].ToString();
				}
				if(row["zhijie_shebei"]!=null)
				{
					model.zhijie_shebei=row["zhijie_shebei"].ToString();
				}
				if(row["zhijie_sb_gouzhi"]!=null)
				{
					model.zhijie_sb_gouzhi=row["zhijie_sb_gouzhi"].ToString();
				}
				if(row["zhijie_sb_shizhi"]!=null)
				{
					model.zhijie_sb_shizhi=row["zhijie_sb_shizhi"].ToString();
				}
				if(row["zhijie_sb_gaizao"]!=null)
				{
					model.zhijie_sb_gaizao=row["zhijie_sb_gaizao"].ToString();
				}
				if(row["zhijie_cailiao"]!=null)
				{
					model.zhijie_cailiao=row["zhijie_cailiao"].ToString();
				}
				if(row["zhijie_ceshi"]!=null)
				{
					model.zhijie_ceshi=row["zhijie_ceshi"].ToString();
				}
				if(row["zhijie_ranliao"]!=null)
				{
					model.zhijie_ranliao=row["zhijie_ranliao"].ToString();
				}
				if(row["zhijie_chailv"]!=null)
				{
					model.zhijie_chailv=row["zhijie_chailv"].ToString();
				}
				if(row["zhijie_huiyi"]!=null)
				{
					model.zhijie_huiyi=row["zhijie_huiyi"].ToString();
				}
				if(row["zhijie_hezuojiaoliu"]!=null)
				{
					model.zhijie_hezuojiaoliu=row["zhijie_hezuojiaoliu"].ToString();
				}
				if(row["zhijie_xinxi"]!=null)
				{
					model.zhijie_xinxi=row["zhijie_xinxi"].ToString();
				}
				if(row["zhijie_zixun"]!=null)
				{
					model.zhijie_zixun=row["zhijie_zixun"].ToString();
				}
				if(row["zhijie_laowu"]!=null)
				{
					model.zhijie_laowu=row["zhijie_laowu"].ToString();
				}
				if(row["zhijie_qita"]!=null)
				{
					model.zhijie_qita=row["zhijie_qita"].ToString();
				}
				if(row["jianjie_guanli"]!=null)
				{
					model.jianjie_guanli=row["jianjie_guanli"].ToString();
				}
				if(row["jianjie_jixiao"]!=null)
				{
					model.jianjie_jixiao=row["jianjie_jixiao"].ToString();
				}
				if(row["zhijie_shebei1"]!=null)
				{
					model.zhijie_shebei1=row["zhijie_shebei1"].ToString();
				}
				if(row["zhijie_sb_gouzhi1"]!=null)
				{
					model.zhijie_sb_gouzhi1=row["zhijie_sb_gouzhi1"].ToString();
				}
				if(row["zhijie_sb_shizhi1"]!=null)
				{
					model.zhijie_sb_shizhi1=row["zhijie_sb_shizhi1"].ToString();
				}
				if(row["zhijie_sb_gaizao1"]!=null)
				{
					model.zhijie_sb_gaizao1=row["zhijie_sb_gaizao1"].ToString();
				}
				if(row["zhijie_cailiao1"]!=null)
				{
					model.zhijie_cailiao1=row["zhijie_cailiao1"].ToString();
				}
				if(row["zhijie_ceshi1"]!=null)
				{
					model.zhijie_ceshi1=row["zhijie_ceshi1"].ToString();
				}
				if(row["zhijie_ranliao1"]!=null)
				{
					model.zhijie_ranliao1=row["zhijie_ranliao1"].ToString();
				}
				if(row["zhijie_chailv1"]!=null)
				{
					model.zhijie_chailv1=row["zhijie_chailv1"].ToString();
				}
				if(row["zhijie_huiyi1"]!=null)
				{
					model.zhijie_huiyi1=row["zhijie_huiyi1"].ToString();
				}
				if(row["zhijie_hezuojiaoliu1"]!=null)
				{
					model.zhijie_hezuojiaoliu1=row["zhijie_hezuojiaoliu1"].ToString();
				}
				if(row["zhijie_xinxi1"]!=null)
				{
					model.zhijie_xinxi1=row["zhijie_xinxi1"].ToString();
				}
				if(row["zhijie_zixun1"]!=null)
				{
					model.zhijie_zixun1=row["zhijie_zixun1"].ToString();
				}
				if(row["zhijie_laowu1"]!=null)
				{
					model.zhijie_laowu1=row["zhijie_laowu1"].ToString();
				}
				if(row["zhijie_qita1"]!=null)
				{
					model.zhijie_qita1=row["zhijie_qita1"].ToString();
				}
				if(row["jianjie_guanli1"]!=null)
				{
					model.jianjie_guanli1=row["jianjie_guanli1"].ToString();
				}
				if(row["jianjie_jixiao1"]!=null)
				{
					model.jianjie_jixiao1=row["jianjie_jixiao1"].ToString();
				}
				if(row["zhijie_shebei2"]!=null)
				{
					model.zhijie_shebei2=row["zhijie_shebei2"].ToString();
				}
				if(row["zhijie_sb_gouzhi2"]!=null)
				{
					model.zhijie_sb_gouzhi2=row["zhijie_sb_gouzhi2"].ToString();
				}
				if(row["zhijie_sb_shizhi2"]!=null)
				{
					model.zhijie_sb_shizhi2=row["zhijie_sb_shizhi2"].ToString();
				}
				if(row["zhijie_sb_gaizao2"]!=null)
				{
					model.zhijie_sb_gaizao2=row["zhijie_sb_gaizao2"].ToString();
				}
				if(row["zhijie_cailiao2"]!=null)
				{
					model.zhijie_cailiao2=row["zhijie_cailiao2"].ToString();
				}
				if(row["zhijie_ceshi2"]!=null)
				{
					model.zhijie_ceshi2=row["zhijie_ceshi2"].ToString();
				}
				if(row["zhijie_ranliao2"]!=null)
				{
					model.zhijie_ranliao2=row["zhijie_ranliao2"].ToString();
				}
				if(row["zhijie_chailv2"]!=null)
				{
					model.zhijie_chailv2=row["zhijie_chailv2"].ToString();
				}
				if(row["zhijie_huiyi2"]!=null)
				{
					model.zhijie_huiyi2=row["zhijie_huiyi2"].ToString();
				}
				if(row["zhijie_hezuojiaoliu2"]!=null)
				{
					model.zhijie_hezuojiaoliu2=row["zhijie_hezuojiaoliu2"].ToString();
				}
				if(row["zhijie_xinxi2"]!=null)
				{
					model.zhijie_xinxi2=row["zhijie_xinxi2"].ToString();
				}
				if(row["zhijie_zixun2"]!=null)
				{
					model.zhijie_zixun2=row["zhijie_zixun2"].ToString();
				}
				if(row["zhijie_laowu2"]!=null)
				{
					model.zhijie_laowu2=row["zhijie_laowu2"].ToString();
				}
				if(row["zhijie_qita2"]!=null)
				{
					model.zhijie_qita2=row["zhijie_qita2"].ToString();
				}
				if(row["jianjie_guanli2"]!=null)
				{
					model.jianjie_guanli2=row["jianjie_guanli2"].ToString();
				}
				if(row["jianjie_jixiao2"]!=null)
				{
					model.jianjie_jixiao2=row["jianjie_jixiao2"].ToString();
				}
				if(row["add_time"]!=null && row["add_time"].ToString()!="")
				{
					model.add_time=DateTime.Parse(row["add_time"].ToString());
				}
				if(row["add_user"]!=null)
				{
					model.add_user=row["add_user"].ToString();
				}
				if(row["status"]!=null && row["status"].ToString()!="")
				{
					model.status=int.Parse(row["status"].ToString());
				}
				if(row["zhijie1"]!=null)
				{
					model.zhijie1=row["zhijie1"].ToString();
				}
				if(row["zhijie2"]!=null)
				{
					model.zhijie2=row["zhijie2"].ToString();
				}
				if(row["zhijie"]!=null)
				{
					model.zhijie=row["zhijie"].ToString();
				}
				if(row["jianjie"]!=null)
				{
					model.jianjie=row["jianjie"].ToString();
				}
				if(row["jianjie1"]!=null)
				{
					model.jianjie1=row["jianjie1"].ToString();
				}
				if(row["jianjie2"]!=null)
				{
					model.jianjie2=row["jianjie2"].ToString();
				}
				if(row["heji"]!=null)
				{
					model.heji=row["heji"].ToString();
				}
				if(row["heji1"]!=null)
				{
					model.heji1=row["heji1"].ToString();
				}
				if(row["heji2"]!=null)
				{
					model.heji2=row["heji2"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select id,pa_id,laiyuan_zhuanxiang,laiyuan_qita,zhijie_shebei,zhijie_sb_gouzhi,zhijie_sb_shizhi,zhijie_sb_gaizao,zhijie_cailiao,zhijie_ceshi,zhijie_ranliao,zhijie_chailv,zhijie_huiyi,zhijie_hezuojiaoliu,zhijie_xinxi,zhijie_zixun,zhijie_laowu,zhijie_qita,jianjie_guanli,jianjie_jixiao,zhijie_shebei1,zhijie_sb_gouzhi1,zhijie_sb_shizhi1,zhijie_sb_gaizao1,zhijie_cailiao1,zhijie_ceshi1,zhijie_ranliao1,zhijie_chailv1,zhijie_huiyi1,zhijie_hezuojiaoliu1,zhijie_xinxi1,zhijie_zixun1,zhijie_laowu1,zhijie_qita1,jianjie_guanli1,jianjie_jixiao1,zhijie_shebei2,zhijie_sb_gouzhi2,zhijie_sb_shizhi2,zhijie_sb_gaizao2,zhijie_cailiao2,zhijie_ceshi2,zhijie_ranliao2,zhijie_chailv2,zhijie_huiyi2,zhijie_hezuojiaoliu2,zhijie_xinxi2,zhijie_zixun2,zhijie_laowu2,zhijie_qita2,jianjie_guanli2,jianjie_jixiao2,add_time,add_user,status,zhijie1,zhijie2,zhijie,jianjie,jianjie1,jianjie2,heji,heji1,heji2 ");
			strSql.Append(" FROM ky_applyproject_jingfei ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" id,pa_id,laiyuan_zhuanxiang,laiyuan_qita,zhijie_shebei,zhijie_sb_gouzhi,zhijie_sb_shizhi,zhijie_sb_gaizao,zhijie_cailiao,zhijie_ceshi,zhijie_ranliao,zhijie_chailv,zhijie_huiyi,zhijie_hezuojiaoliu,zhijie_xinxi,zhijie_zixun,zhijie_laowu,zhijie_qita,jianjie_guanli,jianjie_jixiao,zhijie_shebei1,zhijie_sb_gouzhi1,zhijie_sb_shizhi1,zhijie_sb_gaizao1,zhijie_cailiao1,zhijie_ceshi1,zhijie_ranliao1,zhijie_chailv1,zhijie_huiyi1,zhijie_hezuojiaoliu1,zhijie_xinxi1,zhijie_zixun1,zhijie_laowu1,zhijie_qita1,jianjie_guanli1,jianjie_jixiao1,zhijie_shebei2,zhijie_sb_gouzhi2,zhijie_sb_shizhi2,zhijie_sb_gaizao2,zhijie_cailiao2,zhijie_ceshi2,zhijie_ranliao2,zhijie_chailv2,zhijie_huiyi2,zhijie_hezuojiaoliu2,zhijie_xinxi2,zhijie_zixun2,zhijie_laowu2,zhijie_qita2,jianjie_guanli2,jianjie_jixiao2,add_time,add_user,status,zhijie1,zhijie2,zhijie,jianjie,jianjie1,jianjie2,heji,heji1,heji2 ");
			strSql.Append(" FROM ky_applyproject_jingfei ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM ky_applyproject_jingfei ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.id desc");
			}
			strSql.Append(")AS Row, T.*  from ky_applyproject_jingfei T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "ky_applyproject_jingfei";
			parameters[1].Value = "id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod
		public DataSet GetList(int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
		{
			StringBuilder strSql = new StringBuilder();
			strSql.Append("select * FROM ky_applyproject_jingfei ");
			if (strWhere.Trim() != "")
			{
				strSql.Append(" where " + strWhere);
			}
			recordCount = Convert.ToInt32(DbHelperSQL.GetSingle(PagingHelper.CreateCountingSql(strSql.ToString())));
			return DbHelperSQL.Query(PagingHelper.CreatePagingSql(recordCount, pageSize, pageIndex, strSql.ToString(), filedOrder));
		}
		#endregion  ExtensionMethod
	}
}

