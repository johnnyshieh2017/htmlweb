﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    abstract class abstract_class
    {
        public int  create()
        {
            return 1;
        }
        public virtual void Func() // 注意virtual,表明这是一个虚拟函数
        {
            Console.WriteLine("Func In A");
        }
        abstract public void add();
        
    }

    class creater : abstract_class
    {
        public override void add()
        {
            throw new NotImplementedException();
        }

        public override void Func()
        {
            Console.WriteLine("Func In creater 虚方法");
            base.Func();
        }
    }

    class tester
    {
        private string test()
        {
            creater cre = new creater();
            cre.add();
            cre.create();

            return "";
        }
    }

}
