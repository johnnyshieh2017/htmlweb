﻿using DTcms.Model;
using Newtonsoft.Json;
using NLog;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace yjr.Service
{
    public class Program
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public static void Main(string[] args)
        {
            RunProgramAsync().GetAwaiter().GetResult();

            //Console.WriteLine("Press any key to close the application");
            //Console.ReadKey();
            Console.WriteLine("yjr.Service 启动");
            Console.ReadLine();
        }

        private static async Task RunProgramAsync()
        {
            try
            {
                StdSchedulerFactory factory = new StdSchedulerFactory();

                IScheduler scheduler = await factory.GetScheduler();

                //开启调度器
                await scheduler.Start();
                              
            }
            catch (SchedulerException se)
            {
                logger.Error("执行错误",se);
            }
        }

    }
}
