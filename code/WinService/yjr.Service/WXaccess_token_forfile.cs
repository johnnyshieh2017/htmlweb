﻿using DTcms.API.Payment.wxpay;
using NLog;
using Quartz;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace yjr.Service
{
    [DisallowConcurrentExecution]
    class WXaccess_token_forfile: IJob
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public Task Execute(IJobExecutionContext context)
        {
            return Task.Run(() =>
            {

                Console.WriteLine("定时获取accesstoken_forfile" + System.DateTime.Now.ToString());

                //https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET

                var model = new DTcms.Model.yjr_zx_wx_config();
                model.appid = System.Configuration.ConfigurationManager.AppSettings["wx_appid"].ToString();
                model.secret = System.Configuration.ConfigurationManager.AppSettings["wx_appsecret"].ToString(); ;

                string url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + model.appid + "&secret=" + model.secret;

                // string result = HttpService.Get(url);

                var result = Senparc.Weixin.MP.CommonAPIs.CommonApi.GetToken(model.appid, model.secret);

                if (result.access_token != "")
                {
                    model.access_token = result.access_token;
                    model.updatetime = System.DateTime.Now;
                    // bll.Update(model);
                    WriteTxtToFile(Path.GetFullPath("config/"), "config.txt", model.access_token + "|" + model.updatetime);
                }
                else
                {
                    logger.Error("获取access_token异常：" + result);
                }
            });
        }

        /// <summary>
        /// 将文本写入txt文件中
        /// </summary>
        /// <param name="DirPath">文件路径</param>
        /// <param name="FileName">文件名称</param>
        /// <param name="Strs">字符串</param>
        /// <param name="IsCleanFile">是否先清空文件</param>
        public void WriteTxtToFile(string DirPath, string FileName, string Strs, bool IsCleanFile = false)
        {
            try
            {
                if (string.IsNullOrEmpty(Strs))
                    return;
                if (!Directory.Exists(DirPath))  //如果不存在就创建file文件夹
                {
                    Directory.CreateDirectory(DirPath);
                }
                if (!File.Exists(DirPath + FileName))
                {
                   var myfile =  File.Create(DirPath + FileName);
                    myfile.Close();
                }
                FileStream fs = new FileStream((DirPath + FileName), FileMode.Open, FileAccess.ReadWrite);
                if (File.Exists(DirPath + FileName) && IsCleanFile)
                {
                    fs.SetLength(0); //先清空文件
                }
                StreamWriter sw = new StreamWriter(fs);
                sw.Write(Strs);   //写入字符串
                sw.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
