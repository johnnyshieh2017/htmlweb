﻿using DawnXZ.NuclearUtility;
using DTcms.Model;
using Newtonsoft.Json;
using NLog;
using Quartz;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace yjr.Service
{

    [DisallowConcurrentExecution]
    public class EmailJob: IJob
    {
        DTcms.BLL.vip_email emailbll = new DTcms.BLL.vip_email();
        DTcms.BLL.dt_users ubll = new DTcms.BLL.dt_users();
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public Task Execute(IJobExecutionContext context)
        {
            return Task.Run(() =>
            {
                logger.Info("开始执行 vip email 任务");
                Console.WriteLine("emailsend开始" + System.DateTime.Now.ToString());
                vipemailsend();
            });
        }

        private void vipemailsend()
        {
            var list = emailbll.GetModelList(" mail_status=0 and send_date < GETDATE() ");
            List<int> ids = new List<int>();

            List<int> ids_sucess = new List<int>();
            foreach (var item in list)
            {
                if (!SendEmail(item))
                {
                    ids.Add(item.id);
                }
                else
                {
                    ids_sucess.Add(item.id);
                }
            }

            if (ids.Count > 0)
            {
                logger.Error("发送微信模板消息出现异常，异常id：" + ids.ToArray().ToString());//
            }
            foreach (var item in list)
            {
                //成功更新状态
                if (ids_sucess.Contains(item.id))
                {
                    item.mail_status = 1;
                    item.complate_date = System.DateTime.Now;
                    emailbll.Update(item);
                }
            }
        }

         bool SendEmail(vip_email mail)
        {
            EMailSrvInfo serverInfo = new EMailSrvInfo();
            serverInfo.SMTPServer = ConfigurationManager.AppSettings["SMTPServer"].ToString();// "smtp.163.com";
            serverInfo.SMTPPort = int.Parse(ConfigurationManager.AppSettings["SMTPPort"].ToString()); //25;
            serverInfo.UserName = ConfigurationManager.AppSettings["UserName"].ToString();// "johnnyshieh@163.com";
            serverInfo.Password = ConfigurationManager.AppSettings["Password"].ToString();// "sx1999sx";
            serverInfo.IsSSL = false;
            serverInfo.DisplayName = ConfigurationManager.AppSettings["DisplayName"].ToString();// "johnnyshieh";

            Queue<string> rec = new Queue<string>();
            rec.Clear();
            var list = mail.mail_to.Split('|');
            foreach (var item in list)
            {
                rec.Enqueue(item);
            }
            // rec.Enqueue("44558077@qq.com");

            EMailInfo MailInfo = new EMailInfo();
            MailInfo.IsHTML = true;
            MailInfo.Recipient = rec;
            MailInfo.Subject = mail.mail_title;
            MailInfo.Body = mail.mail_content;
          
            EMailHelper emailhelp = new EMailHelper(serverInfo);


            emailhelp.Send(MailInfo);

            logger.Info(emailhelp.IsSuccessful.ToString() + emailhelp.LastErrorMessage);

            return emailhelp.IsSuccessful;
        }
    }
}
