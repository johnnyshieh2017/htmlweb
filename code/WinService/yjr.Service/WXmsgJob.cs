﻿using DTcms.Model;
using Newtonsoft.Json;
using NLog;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace yjr.Service
{
    public class WXmsgJob : IJob
    {
        DTcms.BLL.yjr_zx_wx_config bll = new DTcms.BLL.yjr_zx_wx_config();
        DTcms.BLL.yjr_zx_messages zx_msgbll = new DTcms.BLL.yjr_zx_messages();
        DTcms.BLL.yjr_user_uservsopenid uvoBll = new DTcms.BLL.yjr_user_uservsopenid();

        DTcms.BLL.vip_sms vipsmsbll = new DTcms.BLL.vip_sms();
        //DTcms.BLL.users ubll = new DTcms.BLL.users();
        DTcms.BLL.dt_users ubll = new DTcms.BLL.dt_users();
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public Task Execute(IJobExecutionContext context)
        {
            return Task.Run(() =>
            {
                logger.Info("开始执行vip短信消息任务");
                Console.WriteLine("send开始"+System.DateTime.Now.ToString());
                //send();
                //checkmsg();

                vipsmssend();
            });
        }

        private void vipsmssend()
        {
            var list = vipsmsbll.GetModelList(" msg_status=0 and send_date < GETDATE() ");
            List<int> ids = new List<int>();

            List<int> ids_sucess = new List<int>();
            foreach (var item in list)
            {
                if (!template_sendsms(item))
                {
                    ids.Add(item.id);
                }
                else
                {
                    ids_sucess.Add(item.id);
                }
            }

            if (ids.Count > 0)
            {
                logger.Error("发送微信模板消息出现异常，异常id：" + ids.ToArray().ToString());//
            }
            foreach (var item in list)
            {
                //成功更新状态
                if (ids_sucess.Contains(item.id))
                {
                    item.msg_status = 1;
                    item.complate_date = System.DateTime.Now;
                    vipsmsbll.Update(item);
                }
            }
        }

        private bool template_sendsms(vip_sms model)
        {
            string template_id = System.Configuration.ConfigurationManager.AppSettings["SMS_templateid_vip"];
            string[] content = model.msg_content.Split('|');
            
            if (commonfunction.qcloud_smsvcode_send(model.msg_mobile, template_id, content) > 0)

                return true;
            else
                return false;
        }


        /// <summary>
        /// 检查待发消息
        /// </summary>
        private void checkmsg()
        {
            var list = zx_msgbll.GetModelList(" status=1 and read_status=0  ");//status 标识进行中的会话，下单后未支付前提交的为 0，支付后改为1，结束后改为2
            List<int> ids = new List<int>();

            List<int> ids_sucess = new List<int>();
            foreach (var item in list)
            {
                //ids.Add(item.id);
                //
                if (!sendWXmessage(item))
                {
                    ids.Add(item.id);
                }
                else
                {
                    ids_sucess.Add(item.id);
                }

            }
            if (ids.Count > 0)
            {
                logger.Error("发送微信模板消息出现异常，异常id：" + ids.ToArray().ToString());//
            }
            foreach (var item in list)
            {
                //成功更新状态
                if (ids_sucess.Contains(item.id))
                {
                    item.read_status = 1;
                    zx_msgbll.Update(item);
                }
            }
        }



        /// <summary>
        /// 发送微信模板消息
        /// </summary>
        /// <param name="message"></param>
        private bool sendWXmessage(DTcms.Model.yjr_zx_messages message)
        {
            bool result = false;
            if (message.owner == 1)
            {//患者的提问 to doctor
                var docuser = uvoBll.GetModel(int.Parse(new DTcms.BLL.yjr_doctor_vsuser().GetModelList("docid='"+message.doctorid+"'").FirstOrDefault().userid));
                if (docuser != null)
                {
                    string doc_url = System.Configuration.ConfigurationManager.AppSettings["wxmsg_doc_url"]; ;//
                    string template_id = System.Configuration.ConfigurationManager.AppSettings["template_id"];//template_id
                    var user = ubll.GetModel(message.userid);
                   
                    result= send(docuser.openid, template_id, doc_url, message.createtime, user.nick_name);

                    var doc = new DTcms.BLL.yjr_doctor().GetModel(message.doctorid);
                    sendsms(doc.mobile,message.createtime.ToString(), user.nick_name);
                }
            }
            else
            { // to member
                var useropenid = uvoBll.GetModel(message.userid);
                if (useropenid != null)
                {
                    string user_url = System.Configuration.ConfigurationManager.AppSettings["wxmsg_user_url"];
                    string template_id = System.Configuration.ConfigurationManager.AppSettings["template_id"];//template_id
                    var user = ubll.GetModel(message.userid);
                    result= send(useropenid.openid, template_id, user_url, message.createtime, user.nick_name);


                    //var doc = new DTcms.BLL.yjr_doctor().GetModel(message.doctorid); //医师回复
                    //sendsms(doc.mobile, message.createtime.ToString(), user.nick_name);
                }
            }

            return result;
        }

        #region 发送微信模板短信
        private bool send(string touser, string template_id, string url, DateTime infortime, string sender)
        {
            // 微信配置信息
            var model = bll.GetModelList("").FirstOrDefault();

            WxTemplate wxTemp = new WxTemplate();
            wxTemp.touser = touser;// "o-3QW0tUV2dpbRdLE9ziLTKgS5ts";
            wxTemp.template_id = template_id;// "KFQtMgF8WDqNfylEgutoBbOawHBnQL1TXWficdpOZas";
            wxTemp.topcolor = "#FF0000";
            wxTemp.url = url;// "http://www.hyyjk.cn/?id=" + System.DateTime.Now.ToString("yyyyMMddHHmmssfff");

            MemZXTemplate memRecTemp = new MemZXTemplate();
            memRecTemp.title = new TemplateDataItem("您有一条新的咨询未读信息");
            memRecTemp.infortime = new TemplateDataItem(infortime.ToString("yyyy/MM/dd HH:mm:ss"));

            memRecTemp.type = new TemplateDataItem("图文咨询");


            memRecTemp.member = new TemplateDataItem(sender);

            wxTemp.data = memRecTemp;

            string res = PushTempleModel(wxTemp, model.access_token);
            if (res.Contains("ok"))
            {
                logger.Info("定时发送微信模板消息，PushTempleModel发送成功");

                return true;
            }
            else
            {
                logger.Error("定时发送微信模板消息，PushTempleModel，未正常发送成功");
                return false;
            }
        }
        #endregion

        #region 发送手机短信
        private bool sendsms(string mobile,string infortime,string sender)
        {
            string template_id = System.Configuration.ConfigurationManager.AppSettings["SMS_templateid_zx"];
            string[] content = {System.DateTime.Now.ToString()};
            if (commonfunction.qcloud_smsvcode_send(mobile, template_id, content) > 0)

                return true;
            else
                return false;
        }
        #endregion


        private static string PushTempleModel(WxTemplate tm, string access_token)
        {
            string url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=" + access_token;


            string postData = JsonConvert.SerializeObject(tm);//JsonConvert.SerializeObject

            string response = DTcms.Common.Utils.HttpPost(url, postData);// httpRequest.Reqeust(url, postData);//调用HTTP通信接口提交数据  
            return response;
        }
    }
}
