﻿using NLog;
using qcloudsms_csharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace yjr.Service
{
    public static class commonfunction
    {

        private static Logger logger = LogManager.GetCurrentClassLogger();


        #region 腾讯云——短信发送
        /// <summary>
        /// 腾讯云——短信发送
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="strcode"></param>
        /// <returns></returns>
        public static int qcloud_smsvcode_send(string mobile, string _templateid,string[] msgcontent)
        {
            int appid = int.Parse(System.Configuration.ConfigurationManager.AppSettings["SMS_appid"].ToString()); //1400239746;
            // 短信应用 SDK AppKey
            string appkey = System.Configuration.ConfigurationManager.AppSettings["SMS_appkey"].ToString();// "05db653f1ac1c553025fab31c983ee4d";
            string templateid = _templateid;// System.Configuration.ConfigurationManager.AppSettings["SMS_templateid"].ToString();
            string smsSign = System.Configuration.ConfigurationManager.AppSettings["SMS_sign"].ToString();//SMS_sign

            SmsSingleSender ssender = new SmsSingleSender(appid, appkey);


            //string[] msgcontent = { strcode, userConfig.regsmsexpired.ToString() };
            var sresult = ssender.sendWithParam("86", mobile, int.Parse(templateid), msgcontent
                , smsSign, "", "");

            if (sresult.result != 0)
            {
                logger.Debug("qcloud_smsvcode_send_ssender_error" + sresult.errMsg);
                return 0;
            }
            else
            {
                return 1;
            }

        }
        #endregion
    }
}
