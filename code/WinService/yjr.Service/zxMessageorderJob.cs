﻿using NLog;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace yjr.Service
{
    [DisallowConcurrentExecution]
    class zxMessageorderJob : IJob
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        DTcms.BLL.yjr_zx_order orderbll = new DTcms.BLL.yjr_zx_order();
        readonly string service_time_interval = System.Configuration.ConfigurationManager.AppSettings["service_time_interval"] != "" ? System.Configuration.ConfigurationManager.AppSettings["service_time_interval"] : "2880";//分钟
        public Task Execute(IJobExecutionContext context)
        {
            return Task.Run(() =>
            {
                logger.Info("开始执行咨询会话超时检查任务");
                //Console.WriteLine("send开始" + System.DateTime.Now.ToString());

                var list = orderbll.GetModelList(" (status=1 or status=2) and pay_status=1 and  DATEDIFF(n, pay_time, '"+System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")+"')>"+ service_time_interval + "  ");
                if (list.Count > 0)
                {
                    logger.Info("开始执行一次关闭订单任务");
                    DTcms.BLL.yjr_zx_messages bll = new DTcms.BLL.yjr_zx_messages();
                    foreach (var item in list)
                    {
                        item.status = 9;// 0:未开始 、1或2 进行中 9：超时关闭  //咨询订单表
                        orderbll.Update(item);

                        bll.UpdateList(" sessionid='"+ item .ms_sessionid+ "'", 9);//zx_message 咨询会话表 status为9时 表示关闭
                    }
                }

                var list_free = orderbll.GetModelList(" status=1 and service_type=-1 and  DATEDIFF(n, createtime, '" + System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "')>" + service_time_interval + "  ");
                if (list_free.Count > 0)
                {
                    logger.Info("开始执行一次关闭义诊订单任务");
                    DTcms.BLL.yjr_zx_messages bll = new DTcms.BLL.yjr_zx_messages();
                    foreach (var item in list_free)
                    {
                        item.status = 9;// 0:未开始 、1或2 进行中 9：超时关闭  //咨询订单表
                        orderbll.Update(item);

                        bll.UpdateList(" sessionid='" + item.ms_sessionid + "'", 9);//zx_message 咨询会话表 status为9时 表示关闭
                    }
                }
            });
            //throw new NotImplementedException();
        }
    }
}
