﻿using DTcms.Model;
using Newtonsoft.Json;
using NLog;
using Quartz;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace yjr.Service
{
    [DisallowConcurrentExecution]
    public class wxMsgJobForlive : IJob
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();


        public Task Execute(IJobExecutionContext context)
        {
            return Task.Run(() =>
            {
              
                Console.WriteLine("wxMsgJobForlive.send开始" + System.DateTime.Now.ToString());

                smssend();
            });
        }

        private void smssend()
        {
           
            List<int> ids = new List<int>();

            List<int> ids_sucess = new List<int>();

            for (int i = 0; i < 10; i++)
            {
                // 发送方法   组织模板内容 按条件发送
                //send("o-3QW0tUV2dpbRdLE9ziLTKgS5ts", "", "http://www.hyyjk.cn/?id=" + System.DateTime.Now.ToString("yyyyMMddHHmmss"), System.DateTime.Now, "hyy");

                Thread.Sleep(25000);
            }

            if (ids.Count > 0)
            {
                logger.Error("发送微信模板消息出现异常，异常id：" + ids.ToArray().ToString());
            }
            
        }

        #region 发送微信模板短信
        private bool send(string touser, string template_id, string url, DateTime infortime, string sender)
        {
            var access_token =  System.IO.File.ReadAllText(Path.GetFullPath("config/config.txt")).Split('|')[0];
            // 微信配置信息
            //var model = bll.GetModelList("").FirstOrDefault();

            WxTemplate wxTemp = new WxTemplate();
            wxTemp.touser = touser;// "o-3QW0tUV2dpbRdLE9ziLTKgS5ts";
            wxTemp.template_id = "K2KEpQUyOmyDFn-1Nz7Hc4tTS4UMkilPyT9p4FRCq_g";// "KFQtMgF8WDqNfylEgutoBbOawHBnQL1TXWficdpOZas";
            wxTemp.topcolor = "#FF0000";
            wxTemp.url = url;// "http://www.hyyjk.cn/?id=" + System.DateTime.Now.ToString("yyyyMMddHHmmssfff");

            wxTemplate01 memRecTemp = new wxTemplate01();
          
            memRecTemp.createtime = new TemplateDataItem(infortime.ToString("yyyy/MM/dd HH:mm:ss"));

            memRecTemp.sendtype = new TemplateDataItem("行业消息");


            memRecTemp.sender = new TemplateDataItem(sender);

            wxTemp.data = memRecTemp;

            string res =  PushTempleModel(wxTemp, access_token);
            if (res.Contains("ok"))
            {
                logger.Info("定时发送微信模板消息，PushTempleModel发送成功");

                return true;
            }
            else
            {
                logger.Error("定时发送微信模板消息，PushTempleModel，未正常发送成功");
                return false;
            }
        }

        private static string PushTempleModel(WxTemplate tm, string access_token)
        {
            string url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=" + access_token;


            string postData = JsonConvert.SerializeObject(tm);//JsonConvert.SerializeObject

            string response = DTcms.Common.Utils.HttpPost(url, postData);// httpRequest.Reqeust(url, postData);//调用HTTP通信接口提交数据  
            return response;
        }
        #endregion


    }
}
