﻿using DTcms.API.Payment.wxpay;
using NLog;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace yjr.Service
{
    [DisallowConcurrentExecution]
    class WXaccess_token : IJob
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        DTcms.BLL.yjr_zx_wx_config bll = new DTcms.BLL.yjr_zx_wx_config();
        public Task Execute(IJobExecutionContext context)
        {
            return Task.Run(() =>
            {
                
                Console.WriteLine("定时获取accesstoken" + System.DateTime.Now.ToString());

                //https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET

                var model = bll.GetModelList("").FirstOrDefault();
                string url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid="+model.appid+"&secret="+model.secret;

                // string result = HttpService.Get(url);

                var result = Senparc.Weixin.MP.CommonAPIs.CommonApi.GetToken(model.appid, model.secret);

                if (result.access_token!="")
                {
                    model.access_token = result.access_token;
                    model.updatetime = System.DateTime.Now;
                    bll.Update(model);
                }
                else
                {
                    logger.Error("获取access_token异常：" + result);
                }
            });
        }
    }
}
