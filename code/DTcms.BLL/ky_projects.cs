﻿using System; 
using System.Text;
using System.Collections.Generic; 
using System.Data;
using DTcms.Common;
using DTcms.Model;
namespace DTcms.BLL {
	//ky_projects
	public partial class ky_projects
	{

		private readonly DTcms.DAL.ky_projects dal = new DTcms.DAL.ky_projects();
		public ky_projects()
		{ }

		#region  Method
		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int p_id)
		{
			return dal.Exists(p_id);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(DTcms.Model.ky_projects model)
		{
			return dal.Add(model);

		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTcms.Model.ky_projects model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int p_id)
		{

			return dal.Delete(p_id);
		}
		/// <summary>
		/// 批量删除一批数据
		/// </summary>
		public bool DeleteList(string p_idlist)
		{
			return dal.DeleteList(p_idlist);
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.ky_projects GetModel(int p_id)
		{

			return dal.GetModel(p_id);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public DTcms.Model.ky_projects GetModelByCache(int p_id)
		{

			string CacheKey = "ky_projectsModel-" + p_id;
			object objModel = DTcms.Common.CacheHelper.Get(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(p_id);
					if (objModel != null)
					{
						int ModelCache = 120;// DTcms.Common.CacheHelper.Get("ModelCache");
						DTcms.Common.CacheHelper.Insert(CacheKey, objModel, ModelCache);
					}
				}
				catch { }
			}
			return (DTcms.Model.ky_projects)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top, string strWhere, string filedOrder)
		{
			return dal.GetList(Top, strWhere, filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<DTcms.Model.ky_projects> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<DTcms.Model.ky_projects> DataTableToList(DataTable dt)
		{
			List<DTcms.Model.ky_projects> modelList = new List<DTcms.Model.ky_projects>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				DTcms.Model.ky_projects model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new DTcms.Model.ky_projects();
					if (dt.Rows[n]["p_id"].ToString() != "")
					{
						model.p_id = int.Parse(dt.Rows[n]["p_id"].ToString());
					}
					model.project_number = dt.Rows[n]["project_number"].ToString();
					model.project_title = dt.Rows[n]["project_title"].ToString();
					if (dt.Rows[n]["dptid"].ToString() != "")
					{
						model.dptid = int.Parse(dt.Rows[n]["dptid"].ToString());
					}
					model.owner = dt.Rows[n]["owner"].ToString();
					model.member = dt.Rows[n]["member"].ToString();
					model.tel = dt.Rows[n]["tel"].ToString();
					model.start_time = dt.Rows[n]["start_time"].ToString();
					model.end_time = dt.Rows[n]["end_time"].ToString();
					if (dt.Rows[n]["project_type"].ToString() != "")
					{
						model.project_type = int.Parse(dt.Rows[n]["project_type"].ToString());
					}
					if (dt.Rows[n]["add_time"].ToString() != "")
					{
						model.add_time = DateTime.Parse(dt.Rows[n]["add_time"].ToString());
					}
					if (dt.Rows[n]["status"].ToString() != "")
					{
						model.status = int.Parse(dt.Rows[n]["status"].ToString());
					}
					model.fee_xiada = dt.Rows[n]["fee_xiada"].ToString();
					model.fee_peitao = dt.Rows[n]["fee_peitao"].ToString();
					model.fee_zongji = dt.Rows[n]["fee_zongji"].ToString();
					model.remark = dt.Rows[n]["remark"].ToString();


					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		public DataSet GetList(int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
		{
			return dal.GetList(pageSize, pageIndex, strWhere, filedOrder, out recordCount);
		}
		#endregion

	}
}