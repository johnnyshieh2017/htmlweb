﻿using System; 
using System.Text;
using System.Collections.Generic; 
using System.Data;
using DTcms.Common;
using DTcms.Model;
namespace DTcms.BLL {
	 	//View_zx_order
		public partial class View_zx_order
	{
   		     
		private readonly DTcms.DAL.View_zx_order dal=new DTcms.DAL.View_zx_order();
		public View_zx_order()
		{}
		
		#region  Method
		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists()
		{
			return dal.Exists();
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public void  Add(DTcms.Model.View_zx_order model)
		{
						dal.Add(model);
						
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTcms.Model.View_zx_order model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete()
		{
			
			return dal.Delete();
		}
		
		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.View_zx_order GetModel()
		{
			
			return dal.GetModel();
		}

        public int Getrecordcount(string strWhere)
        {
            return dal.Getrecordcount(strWhere);
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<DTcms.Model.View_zx_order> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<DTcms.Model.View_zx_order> DataTableToList(DataTable dt)
        {
            List<DTcms.Model.View_zx_order> modelList = new List<DTcms.Model.View_zx_order>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                DTcms.Model.View_zx_order model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new DTcms.Model.View_zx_order();
                    if (dt.Rows[n]["id"].ToString() != "")
                    {
                        model.id = int.Parse(dt.Rows[n]["id"].ToString());
                    }
                    if (dt.Rows[n]["userid"].ToString() != "")
                    {
                        model.userid = int.Parse(dt.Rows[n]["userid"].ToString());
                    }
                    if (dt.Rows[n]["service_type"].ToString() != "")
                    {
                        model.service_type = int.Parse(dt.Rows[n]["service_type"].ToString());
                    }
                    if (dt.Rows[n]["businessconfigid"].ToString() != "")
                    {
                        model.businessconfigid = int.Parse(dt.Rows[n]["businessconfigid"].ToString());
                    }
                    if (dt.Rows[n]["businessappointmentid"].ToString() != "")
                    {
                        model.businessappointmentid = int.Parse(dt.Rows[n]["businessappointmentid"].ToString());
                    }
                    model.question_desc = dt.Rows[n]["question_desc"].ToString();
                    if (dt.Rows[n]["doctor_id"].ToString() != "")
                    {
                        model.doctor_id = int.Parse(dt.Rows[n]["doctor_id"].ToString());
                    }
                    if (dt.Rows[n]["createtime"].ToString() != "")
                    {
                        model.createtime = DateTime.Parse(dt.Rows[n]["createtime"].ToString());
                    }
                    if (dt.Rows[n]["service_fee"].ToString() != "")
                    {
                        model.service_fee = int.Parse(dt.Rows[n]["service_fee"].ToString());
                    }
                    if (dt.Rows[n]["pay_status"].ToString() != "")
                    {
                        model.pay_status = int.Parse(dt.Rows[n]["pay_status"].ToString());
                    }
                    if (dt.Rows[n]["status"].ToString() != "")
                    {
                        model.status = int.Parse(dt.Rows[n]["status"].ToString());
                    }
                    if (dt.Rows[n]["pay_time"].ToString() != "")
                    {
                        model.pay_time = DateTime.Parse(dt.Rows[n]["pay_time"].ToString());
                    }
                    model.ms_sessionid = dt.Rows[n]["ms_sessionid"].ToString();
                    model.business_name = dt.Rows[n]["business_name"].ToString();
                    model.name = dt.Rows[n]["name"].ToString();
                    model.logoimg = dt.Rows[n]["logoimg"].ToString();
                    model.titlename = dt.Rows[n]["titlename"].ToString();
                    model.avatar = dt.Rows[n]["avatar"].ToString();
                    model.nick_name = dt.Rows[n]["nick_name"].ToString();
                    model.user_name = dt.Rows[n]["user_name"].ToString();
                    if (dt.Rows[n]["patientid"].ToString() != "")
                    {
                        model.patientid = int.Parse(dt.Rows[n]["patientid"].ToString());
                    }
                    model.patient_name = dt.Rows[n]["patient_name"].ToString();
                    model.patient_gender = dt.Rows[n]["patient_gender"].ToString();
                    if (dt.Rows[n]["patient_age"].ToString() != "")
                    {
                        model.patient_age = int.Parse(dt.Rows[n]["patient_age"].ToString());
                    }
                    model.hospitalname = dt.Rows[n]["hospitalname"].ToString();
                    model.hoslevel = dt.Rows[n]["hoslevel"].ToString();
                    model.groupname = dt.Rows[n]["groupname"].ToString();
                    model.diagnosis_result = dt.Rows[n]["diagnosis_result"].ToString();
                    model.visited_department = dt.Rows[n]["visited_department"].ToString();
                    model.visited_hospital = dt.Rows[n]["visited_hospital"].ToString();
                    if (dt.Rows[n]["visited_status"].ToString() != "")
                    {
                        model.visited_status = int.Parse(dt.Rows[n]["visited_status"].ToString());
                    }
                    model.sessionid = dt.Rows[n]["sessionid"].ToString();
                    model.reportimg = dt.Rows[n]["reportimg"].ToString();

                    modelList.Add(model);
                }
            }
            return modelList;
        }

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		public DataSet GetList(int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
		{
			return dal.GetList(pageSize, pageIndex, strWhere, filedOrder, out recordCount);
		}

        public List<DTcms.Model.View_zx_order> GetModelList(int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
        {
            DataSet ds =  dal.GetList(pageSize, pageIndex, strWhere, filedOrder, out recordCount);
             return DataTableToList(ds.Tables[0]);
        }
        #endregion

    }
}