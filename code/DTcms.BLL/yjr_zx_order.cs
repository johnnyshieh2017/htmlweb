﻿using System; 
using System.Text;
using System.Collections.Generic; 
using System.Data;
using DTcms.Common;
using DTcms.Model;
namespace DTcms.BLL {
    //yjr_zx_order
    public partial class yjr_zx_order
    {

        private readonly DTcms.DAL.yjr_zx_order dal = new DTcms.DAL.yjr_zx_order();
        public yjr_zx_order()
        { }

        #region  Method
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int id)
        {
            return dal.Exists(id);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(DTcms.Model.yjr_zx_order model)
        {
            return dal.Add(model);

        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(DTcms.Model.yjr_zx_order model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int id)
        {

            return dal.Delete(id);
        }
        /// <summary>
        /// 批量删除一批数据
        /// </summary>
        public bool DeleteList(string idlist)
        {
            return dal.DeleteList(idlist);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public DTcms.Model.yjr_zx_order GetModel(int id)
        {

            return dal.GetModel(id);
        }



        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<DTcms.Model.yjr_zx_order> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<DTcms.Model.yjr_zx_order> DataTableToList(DataTable dt)
        {
            List<DTcms.Model.yjr_zx_order> modelList = new List<DTcms.Model.yjr_zx_order>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                DTcms.Model.yjr_zx_order model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new DTcms.Model.yjr_zx_order();
                    if (dt.Rows[n]["id"].ToString() != "")
                    {
                        model.id = int.Parse(dt.Rows[n]["id"].ToString());
                    }
                    if (dt.Rows[n]["userid"].ToString() != "")
                    {
                        model.userid = int.Parse(dt.Rows[n]["userid"].ToString());
                    }
                    if (dt.Rows[n]["service_type"].ToString() != "")
                    {
                        model.service_type = int.Parse(dt.Rows[n]["service_type"].ToString());
                    }
                    if (dt.Rows[n]["businessconfigid"].ToString() != "")
                    {
                        model.businessconfigid = int.Parse(dt.Rows[n]["businessconfigid"].ToString());
                    }
                    if (dt.Rows[n]["businessappointmentid"].ToString() != "")
                    {
                        model.businessappointmentid = int.Parse(dt.Rows[n]["businessappointmentid"].ToString());
                    }
                    model.question_desc = dt.Rows[n]["question_desc"].ToString();
                    if (dt.Rows[n]["doctor_id"].ToString() != "")
                    {
                        model.doctor_id = int.Parse(dt.Rows[n]["doctor_id"].ToString());
                    }
                    if (dt.Rows[n]["createtime"].ToString() != "")
                    {
                        model.createtime = DateTime.Parse(dt.Rows[n]["createtime"].ToString());
                    }
                    if (dt.Rows[n]["service_fee"].ToString() != "")
                    {
                        model.service_fee = int.Parse(dt.Rows[n]["service_fee"].ToString());
                    }
                    if (dt.Rows[n]["pay_status"].ToString() != "")
                    {
                        model.pay_status = int.Parse(dt.Rows[n]["pay_status"].ToString());
                    }
                    if (dt.Rows[n]["status"].ToString() != "")
                    {
                        model.status = int.Parse(dt.Rows[n]["status"].ToString());
                    }
                    if (dt.Rows[n]["pay_time"].ToString() != "")
                    {
                        model.pay_time = DateTime.Parse(dt.Rows[n]["pay_time"].ToString());
                    }
                    model.ms_sessionid = dt.Rows[n]["ms_sessionid"].ToString();

                    if (dt.Rows[n]["business_name"] != null)
                    {
                        model.business_name = dt.Rows[n]["business_name"].ToString();
                    }

                    if (dt.Rows[n]["name"] != null)
                    {
                        model.name = dt.Rows[n]["name"].ToString();
                    }
                    
                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        public DataSet GetList(int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
        {
            return dal.GetList(pageSize, pageIndex, strWhere, filedOrder, out recordCount);
        }
        #endregion


        /// <summary>
		/// 获得数据列表
		/// </summary>
		public List<DTcms.Model.yjr_zx_order> GetModelList(int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
        {
            DataSet ds = dal.GetList(pageSize, pageIndex, strWhere, filedOrder, out recordCount);
            return DataTableToList(ds.Tables[0]);
        }
    }
}