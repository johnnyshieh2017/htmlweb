﻿using System; 
using System.Text;
using System.Collections.Generic; 
using System.Data;
using DTcms.Common;
using DTcms.Model;
namespace DTcms.BLL {
    //yjr_zx_messages
    public partial class yjr_zx_messages
    {

        private readonly DTcms.DAL.yjr_zx_messages dal = new DTcms.DAL.yjr_zx_messages();
        public yjr_zx_messages()
        { }

        #region  Method
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int id)
        {
            return dal.Exists(id);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(DTcms.Model.yjr_zx_messages model)
        {
            return dal.Add(model);

        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(DTcms.Model.yjr_zx_messages model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int id)
        {

            return dal.Delete(id);
        }
        /// <summary>
        /// 批量删除一批数据
        /// </summary>
        public bool DeleteList(string idlist)
        {
            return dal.DeleteList(idlist);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public DTcms.Model.yjr_zx_messages GetModel(int id)
        {

            return dal.GetModel(id);
        }



        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<DTcms.Model.yjr_zx_messages> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<DTcms.Model.yjr_zx_messages> DataTableToList(DataTable dt)
        {
            List<DTcms.Model.yjr_zx_messages> modelList = new List<DTcms.Model.yjr_zx_messages>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                DTcms.Model.yjr_zx_messages model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new DTcms.Model.yjr_zx_messages();
                    if (dt.Rows[n]["id"].ToString() != "")
                    {
                        model.id = int.Parse(dt.Rows[n]["id"].ToString());
                    }
                    if (dt.Rows[n]["userid"].ToString() != "")
                    {
                        model.userid = int.Parse(dt.Rows[n]["userid"].ToString());
                    }
                    model.sessionid = dt.Rows[n]["sessionid"].ToString();
                    if (dt.Rows[n]["doctorid"].ToString() != "")
                    {
                        model.doctorid = int.Parse(dt.Rows[n]["doctorid"].ToString());
                    }
                    if (dt.Rows[n]["zx_number"].ToString() != "")
                    {
                        model.zx_number = int.Parse(dt.Rows[n]["zx_number"].ToString());
                    }
                    model.zx_content = dt.Rows[n]["zx_content"].ToString();
                    if (dt.Rows[n]["createtime"].ToString() != "")
                    {
                        model.createtime = DateTime.Parse(dt.Rows[n]["createtime"].ToString());
                    }
                    if (dt.Rows[n]["status"].ToString() != "")
                    {
                        model.status = int.Parse(dt.Rows[n]["status"].ToString());
                    }
                    if (dt.Rows[n]["visited_status"].ToString() != "")
                    {
                        model.visited_status = int.Parse(dt.Rows[n]["visited_status"].ToString());
                    }
                    model.visited_hospital = dt.Rows[n]["visited_hospital"].ToString();
                    model.visited_department = dt.Rows[n]["visited_department"].ToString();
                    model.diagnosis_result = dt.Rows[n]["diagnosis_result"].ToString();
                    if (dt.Rows[n]["patientid"].ToString() != "")
                    {
                        model.patientid = int.Parse(dt.Rows[n]["patientid"].ToString());
                    }
                    if (dt.Rows[n]["owner"].ToString() != "")
                    {
                        model.owner = int.Parse(dt.Rows[n]["owner"].ToString());
                    }
                    if (dt.Rows[n]["read_status"].ToString() != "")
                    {
                        model.read_status = int.Parse(dt.Rows[n]["read_status"].ToString());
                    }

                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        public DataSet GetList(int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
        {
            return dal.GetList(pageSize, pageIndex, strWhere, filedOrder, out recordCount);
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<DTcms.Model.yjr_zx_messages> GetModelList(int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
        {
            DataSet ds = dal.GetList(pageSize, pageIndex, strWhere, filedOrder, out recordCount);
            return DataTableToList(ds.Tables[0]);
        }


        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            return dal.GetRecordCount(strWhere);
        }
        #endregion


        #region 扩展方法
        /// <summary>
        /// 批量设置状态一批数据
        /// </summary>
        public void UpdateList(string where,int status)
        {
            var list = GetModelList(where);

            foreach (var item in list)
            {
                item.status = status;
                dal.Update(item);
            }
        }
        #endregion
    }
}