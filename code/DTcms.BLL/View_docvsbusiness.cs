﻿using System; 
using System.Text;
using System.Collections.Generic; 
using System.Data;
using DTcms.Common;
using DTcms.Model;
namespace DTcms.BLL {
	 	//View_docvsbusiness
		public partial class View_docvsbusiness
	{
   		     
		private readonly DTcms.DAL.View_docvsbusiness dal=new DTcms.DAL.View_docvsbusiness();
		public View_docvsbusiness()
		{}
		
		#region  Method
		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists()
		{
			return dal.Exists();
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public void  Add(DTcms.Model.View_docvsbusiness model)
		{
						dal.Add(model);
						
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTcms.Model.View_docvsbusiness model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete()
		{
			
			return dal.Delete();
		}
		
		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.View_docvsbusiness GetModel()
		{
			
			return dal.GetModel();
		}

		

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<DTcms.Model.View_docvsbusiness> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<DTcms.Model.View_docvsbusiness> DataTableToList(DataTable dt)
		{
			List<DTcms.Model.View_docvsbusiness> modelList = new List<DTcms.Model.View_docvsbusiness>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				DTcms.Model.View_docvsbusiness model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new DTcms.Model.View_docvsbusiness();					
													if(dt.Rows[n]["id"].ToString()!="")
				{
					model.id=int.Parse(dt.Rows[n]["id"].ToString());
				}
																																if(dt.Rows[n]["doctorid"].ToString()!="")
				{
					model.doctorid=int.Parse(dt.Rows[n]["doctorid"].ToString());
				}
																																if(dt.Rows[n]["businessid"].ToString()!="")
				{
					model.businessid=int.Parse(dt.Rows[n]["businessid"].ToString());
				}
																																if(dt.Rows[n]["add_time"].ToString()!="")
				{
					model.add_time=DateTime.Parse(dt.Rows[n]["add_time"].ToString());
				}
																																if(dt.Rows[n]["cost_type"].ToString()!="")
				{
					model.cost_type=int.Parse(dt.Rows[n]["cost_type"].ToString());
				}
																																if(dt.Rows[n]["cost_interval"].ToString()!="")
				{
					model.cost_interval=int.Parse(dt.Rows[n]["cost_interval"].ToString());
				}
																																if(dt.Rows[n]["price"].ToString()!="")
				{
					model.price=int.Parse(dt.Rows[n]["price"].ToString());
				}
																																				model.business_name= dt.Rows[n]["business_name"].ToString();
																																model.hospitalname= dt.Rows[n]["hospitalname"].ToString();
																																model.name= dt.Rows[n]["name"].ToString();
																																model.mobile= dt.Rows[n]["mobile"].ToString();
																																model.idcard= dt.Rows[n]["idcard"].ToString();
																												if(dt.Rows[n]["status"].ToString()!="")
				{
					model.status=int.Parse(dt.Rows[n]["status"].ToString());
				}
																																				model.business_intro= dt.Rows[n]["business_intro"].ToString();
																						
				
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		public DataSet GetList(int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
		{
			return dal.GetList(pageSize, pageIndex, strWhere, filedOrder, out recordCount);
		}
#endregion
   
	}
}