﻿using System; 
using System.Text;
using System.Collections.Generic; 
using System.Data;
using DTcms.Common;
using DTcms.Model;
namespace DTcms.BLL {
	 	//yjr_doctor_vsbusinessconfig
		public partial class yjr_doctor_vsbusinessconfig
	{
   		     
		private readonly DTcms.DAL.yjr_doctor_vsbusinessconfig dal=new DTcms.DAL.yjr_doctor_vsbusinessconfig();
		public yjr_doctor_vsbusinessconfig()
		{}
		
		#region  Method
		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int id)
		{
			return dal.Exists(id);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(DTcms.Model.yjr_doctor_vsbusinessconfig model)
		{

           

            return dal.Add(model);
						
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTcms.Model.yjr_doctor_vsbusinessconfig model)
		{
           

            return dal.Update(model);
		}

        #region 扩展

        /// <summary>
		/// 增加一条数据
		/// </summary>
		public int Addext(DTcms.Model.yjr_doctor_vsbusinessconfig model)
        {

            BLL.yjr_doctor_vsbusiness dvb = new yjr_doctor_vsbusiness();
            if (!dvb.Exists(model.doctorid, model.businessid))
            {
                Model.yjr_doctor_vsbusiness model_business = new Model.yjr_doctor_vsbusiness();
                model_business.doctorid = model.doctorid;
                model_business.businessid = model.businessid;
                model_business.add_time = System.DateTime.Now;
                model_business.status = 0;

                dvb.Add(model_business);
            }

            return dal.Add(model);

        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Updateext(DTcms.Model.yjr_doctor_vsbusinessconfig model)
        {
            BLL.yjr_doctor_vsbusiness dvb = new yjr_doctor_vsbusiness();
            if (!dvb.Exists(model.doctorid, model.businessid))
            {
                Model.yjr_doctor_vsbusiness model_business = new Model.yjr_doctor_vsbusiness();
                model_business.doctorid = model.doctorid;
                model_business.businessid = model.businessid;
                model_business.add_time = System.DateTime.Now;
                model_business.status = 0;

                dvb.Add(model_business);
            }

            return dal.Update(model);
        }

        #endregion


        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int id)
		{
			
			return dal.Delete(id);
		}
				/// <summary>
		/// 批量删除一批数据
		/// </summary>
		public bool DeleteList(string idlist )
		{
			return dal.DeleteList(idlist );
		}
		
		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.yjr_doctor_vsbusinessconfig GetModel(int id)
		{
			
			return dal.GetModel(id);
		}

		

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<DTcms.Model.yjr_doctor_vsbusinessconfig> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<DTcms.Model.yjr_doctor_vsbusinessconfig> DataTableToList(DataTable dt)
		{
			List<DTcms.Model.yjr_doctor_vsbusinessconfig> modelList = new List<DTcms.Model.yjr_doctor_vsbusinessconfig>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				DTcms.Model.yjr_doctor_vsbusinessconfig model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new DTcms.Model.yjr_doctor_vsbusinessconfig();					
													if(dt.Rows[n]["id"].ToString()!="")
				{
					model.id=int.Parse(dt.Rows[n]["id"].ToString());
				}
																																if(dt.Rows[n]["doctorid"].ToString()!="")
				{
					model.doctorid=int.Parse(dt.Rows[n]["doctorid"].ToString());
				}
																																if(dt.Rows[n]["businessid"].ToString()!="")
				{
					model.businessid=int.Parse(dt.Rows[n]["businessid"].ToString());
				}
																																if(dt.Rows[n]["add_time"].ToString()!="")
				{
					model.add_time=DateTime.Parse(dt.Rows[n]["add_time"].ToString());
				}
																																if(dt.Rows[n]["cost_type"].ToString()!="")
				{
					model.cost_type=int.Parse(dt.Rows[n]["cost_type"].ToString());
				}
																																if(dt.Rows[n]["cost_interval"].ToString()!="")
				{
					model.cost_interval=int.Parse(dt.Rows[n]["cost_interval"].ToString());
				}
																																if(dt.Rows[n]["price"].ToString()!="")
				{
					model.price=int.Parse(dt.Rows[n]["price"].ToString());
				}
																										
				
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		public DataSet GetList(int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
		{
			return dal.GetList(pageSize, pageIndex, strWhere, filedOrder, out recordCount);
		}
#endregion
   
	}
}