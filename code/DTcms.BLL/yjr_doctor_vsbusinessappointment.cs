﻿using System; 
using System.Text;
using System.Collections.Generic; 
using System.Data;
using DTcms.Common;
using DTcms.Model;
namespace DTcms.BLL {
	 	//yjr_doctor_vsbusinessappointment
		public partial class yjr_doctor_vsbusinessappointment
	{
   		     
		private readonly DTcms.DAL.yjr_doctor_vsbusinessappointment dal=new DTcms.DAL.yjr_doctor_vsbusinessappointment();
		public yjr_doctor_vsbusinessappointment()
		{}
		
		#region  Method
		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int id)
		{
			return dal.Exists(id);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public void  Add(DTcms.Model.yjr_doctor_vsbusinessappointment model)
		{
						dal.Add(model);
						
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTcms.Model.yjr_doctor_vsbusinessappointment model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int id)
		{
			
			return dal.Delete(id);
		}
		
		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.yjr_doctor_vsbusinessappointment GetModel(int id)
		{
			
			return dal.GetModel(id);
		}

		

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<DTcms.Model.yjr_doctor_vsbusinessappointment> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<DTcms.Model.yjr_doctor_vsbusinessappointment> DataTableToList(DataTable dt)
		{
			List<DTcms.Model.yjr_doctor_vsbusinessappointment> modelList = new List<DTcms.Model.yjr_doctor_vsbusinessappointment>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				DTcms.Model.yjr_doctor_vsbusinessappointment model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new DTcms.Model.yjr_doctor_vsbusinessappointment();					
													if(dt.Rows[n]["id"].ToString()!="")
				{
					model.id=int.Parse(dt.Rows[n]["id"].ToString());
				}
																																if(dt.Rows[n]["doctorid"].ToString()!="")
				{
					model.doctorid=int.Parse(dt.Rows[n]["doctorid"].ToString());
				}
																																if(dt.Rows[n]["busines_sconfig_id"].ToString()!="")
				{
					model.busines_sconfig_id=int.Parse(dt.Rows[n]["busines_sconfig_id"].ToString());
				}
																																				model.clinic_date= dt.Rows[n]["clinic_date"].ToString();
																																model.clinic_time= dt.Rows[n]["clinic_time"].ToString();
																												if(dt.Rows[n]["add_time"].ToString()!="")
				{
					model.add_time=DateTime.Parse(dt.Rows[n]["add_time"].ToString());
				}
																										
				
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		public DataSet GetList(int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
		{
			return dal.GetList(pageSize, pageIndex, strWhere, filedOrder, out recordCount);
		}
#endregion
   
	}
}