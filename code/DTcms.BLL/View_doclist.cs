﻿using System; 
using System.Text;
using System.Collections.Generic; 
using System.Data;
using DTcms.Common;
using DTcms.Model;
namespace DTcms.BLL {
	 	//View_doclist
		public partial class View_doclist
	{
   		     
		private readonly DTcms.DAL.View_doclist dal=new DTcms.DAL.View_doclist();
		public View_doclist()
		{}
		
		#region  Method
		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists()
		{
			return dal.Exists();
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public void  Add(DTcms.Model.View_doclist model)
		{
						dal.Add(model);
						
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTcms.Model.View_doclist model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete()
		{
			
			return dal.Delete();
		}
		
		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.View_doclist GetModel()
		{
			
			return dal.GetModel();
		}

		

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<DTcms.Model.View_doclist> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<DTcms.Model.View_doclist> DataTableToList(DataTable dt)
		{
			List<DTcms.Model.View_doclist> modelList = new List<DTcms.Model.View_doclist>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				DTcms.Model.View_doclist model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new DTcms.Model.View_doclist();					
													if(dt.Rows[n]["id"].ToString()!="")
				{
					model.id=int.Parse(dt.Rows[n]["id"].ToString());
				}
																																				model.name= dt.Rows[n]["name"].ToString();
																												if(dt.Rows[n]["title"].ToString()!="")
				{
					model.title=int.Parse(dt.Rows[n]["title"].ToString());
				}
																																				model.gender= dt.Rows[n]["gender"].ToString();
																												if(dt.Rows[n]["groupid"].ToString()!="")
				{
					model.groupid=int.Parse(dt.Rows[n]["groupid"].ToString());
				}
																																				model.intro= dt.Rows[n]["intro"].ToString();
																																model.logoimg= dt.Rows[n]["logoimg"].ToString();
																												if(dt.Rows[n]["departmentid"].ToString()!="")
				{
					model.departmentid=int.Parse(dt.Rows[n]["departmentid"].ToString());
				}
																																if(dt.Rows[n]["createtime"].ToString()!="")
				{
					model.createtime=DateTime.Parse(dt.Rows[n]["createtime"].ToString());
				}
																																if(dt.Rows[n]["status"].ToString()!="")
				{
					model.status=int.Parse(dt.Rows[n]["status"].ToString());
				}
																																if(dt.Rows[n]["isdelete"].ToString()!="")
				{
					model.isdelete=int.Parse(dt.Rows[n]["isdelete"].ToString());
				}
																																if(dt.Rows[n]["sort"].ToString()!="")
				{
					model.sort=int.Parse(dt.Rows[n]["sort"].ToString());
				}
																																if(dt.Rows[n]["siteid"].ToString()!="")
				{
					model.siteid=int.Parse(dt.Rows[n]["siteid"].ToString());
				}
																																				model.mobile= dt.Rows[n]["mobile"].ToString();
																																model.groupname= dt.Rows[n]["groupname"].ToString();
																																model.titlename= dt.Rows[n]["titlename"].ToString();
																																model.department_name= dt.Rows[n]["department_name"].ToString();
																																model.hospitalname= dt.Rows[n]["hospitalname"].ToString();
																												if(dt.Rows[n]["hospitalid"].ToString()!="")
				{
					model.hospitalid=int.Parse(dt.Rows[n]["hospitalid"].ToString());
				}
																																				model.idcard= dt.Rows[n]["idcard"].ToString();
																																model.idcardpic_front= dt.Rows[n]["idcardpic_front"].ToString();
																																model.idcardpic_back= dt.Rows[n]["idcardpic_back"].ToString();
																																model.doc_license= dt.Rows[n]["doc_license"].ToString();
																																model.doc_qualification= dt.Rows[n]["doc_qualification"].ToString();
																																model.doc_titlepic= dt.Rows[n]["doc_titlepic"].ToString();
																																model.speciality= dt.Rows[n]["speciality"].ToString();
																												if(dt.Rows[n]["position_id"].ToString()!="")
				{
					model.position_id=int.Parse(dt.Rows[n]["position_id"].ToString());
				}
																																				model.hoslevel= dt.Rows[n]["hoslevel"].ToString();
																																model.rate= dt.Rows[n]["rate"].ToString();
																												if(dt.Rows[n]["bookcount"].ToString()!="")
				{
					model.bookcount=int.Parse(dt.Rows[n]["bookcount"].ToString());
				}
																																				model.responsetime= dt.Rows[n]["responsetime"].ToString();
																						
				
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		public DataSet GetList(int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
		{
			return dal.GetList(pageSize, pageIndex, strWhere, filedOrder, out recordCount);
		}
#endregion
   
	}
}