﻿using System; 
using System.Text;
using System.Collections.Generic; 
using System.Data;
using DTcms.Common;
using DTcms.Model;
namespace DTcms.BLL {
	 	//yjr_doctor_group
		public partial class yjr_doctor_group
	{
   		     
		private readonly DTcms.DAL.yjr_doctor_group dal=new DTcms.DAL.yjr_doctor_group();
		public yjr_doctor_group()
		{}
		
		#region  Method
		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int id)
		{
			return dal.Exists(id);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(DTcms.Model.yjr_doctor_group model)
		{
						return dal.Add(model);
						
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTcms.Model.yjr_doctor_group model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int id)
		{
			
			return dal.Delete(id);
		}
				/// <summary>
		/// 批量删除一批数据
		/// </summary>
		public bool DeleteList(string idlist )
		{
			return dal.DeleteList(idlist );
		}
		
		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.yjr_doctor_group GetModel(int id)
		{
			
			return dal.GetModel(id);
		}

		

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<DTcms.Model.yjr_doctor_group> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<DTcms.Model.yjr_doctor_group> DataTableToList(DataTable dt)
		{
			List<DTcms.Model.yjr_doctor_group> modelList = new List<DTcms.Model.yjr_doctor_group>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				DTcms.Model.yjr_doctor_group model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new DTcms.Model.yjr_doctor_group();
                    if (dt.Rows[n]["id"].ToString() != "")
                    {
                        model.id = int.Parse(dt.Rows[n]["id"].ToString());
                    }
                    model.groupname = dt.Rows[n]["groupname"].ToString();
                    if (dt.Rows[n]["sort"].ToString() != "")
                    {
                        model.sort = int.Parse(dt.Rows[n]["sort"].ToString());
                    }
                    if (dt.Rows[n]["status"].ToString() != "")
                    {
                        model.status = int.Parse(dt.Rows[n]["status"].ToString());
                    }
                    if (dt.Rows[n]["siteid"].ToString() != "")
                    {
                        model.siteid = int.Parse(dt.Rows[n]["siteid"].ToString());
                    }
                    if (dt.Rows[n]["hospatilid"].ToString() != "")
                    {
                        model.hospatilid = int.Parse(dt.Rows[n]["hospatilid"].ToString());
                    }
                    model.description = dt.Rows[n]["description"].ToString();
                    model.leader_desc = dt.Rows[n]["leader_desc"].ToString();
                    model.logo_url = dt.Rows[n]["logo_url"].ToString();


                    modelList.Add(model);
                }
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		public DataSet GetList(int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
		{
			return dal.GetList(pageSize, pageIndex, strWhere, filedOrder, out recordCount);
		}
#endregion
   
	}
}