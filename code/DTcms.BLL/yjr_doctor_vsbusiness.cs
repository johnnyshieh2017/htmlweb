﻿using System; 
using System.Text;
using System.Collections.Generic; 
using System.Data;
using DTcms.Common;
using DTcms.Model;
namespace DTcms.BLL {
	 	//yjr_doctor_vsbusiness
		public partial class yjr_doctor_vsbusiness
	{
   		     
		private readonly DTcms.DAL.yjr_doctor_vsbusiness dal=new DTcms.DAL.yjr_doctor_vsbusiness();
		public yjr_doctor_vsbusiness()
		{}
		
		#region  Method
		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int doctorid,int businessid)
		{
			return dal.Exists(doctorid,businessid);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public void  Add(DTcms.Model.yjr_doctor_vsbusiness model)
		{
						dal.Add(model);
						
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTcms.Model.yjr_doctor_vsbusiness model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int doctorid,int businessid)
		{
			
			return dal.Delete(doctorid,businessid);
		}
		
		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.yjr_doctor_vsbusiness GetModel(int doctorid,int businessid)
		{
			
			return dal.GetModel(doctorid,businessid);
		}

		

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<DTcms.Model.yjr_doctor_vsbusiness> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<DTcms.Model.yjr_doctor_vsbusiness> DataTableToList(DataTable dt)
		{
			List<DTcms.Model.yjr_doctor_vsbusiness> modelList = new List<DTcms.Model.yjr_doctor_vsbusiness>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				DTcms.Model.yjr_doctor_vsbusiness model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new DTcms.Model.yjr_doctor_vsbusiness();					
													if(dt.Rows[n]["doctorid"].ToString()!="")
				{
					model.doctorid=int.Parse(dt.Rows[n]["doctorid"].ToString());
				}
																																if(dt.Rows[n]["businessid"].ToString()!="")
				{
					model.businessid=int.Parse(dt.Rows[n]["businessid"].ToString());
				}
																																if(dt.Rows[n]["add_time"].ToString()!="")
				{
					model.add_time=DateTime.Parse(dt.Rows[n]["add_time"].ToString());
				}
																																if(dt.Rows[n]["status"].ToString()!="")
				{
					model.status=int.Parse(dt.Rows[n]["status"].ToString());
				}
																										
				
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		public DataSet GetList(int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
		{
			return dal.GetList(pageSize, pageIndex, strWhere, filedOrder, out recordCount);
		}
#endregion
   
	}
}