﻿using System; 
using System.Text;
using System.Collections.Generic; 
using System.Data;
using DTcms.Common;
using DTcms.Model;
namespace DTcms.BLL {
	 	//yjr_zx_business
		public partial class yjr_zx_business
	{
   		     
		private readonly DTcms.DAL.yjr_zx_business dal=new DTcms.DAL.yjr_zx_business();
		public yjr_zx_business()
		{}
		
		#region  Method
		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int id)
		{
			return dal.Exists(id);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(DTcms.Model.yjr_zx_business model)
		{
						return dal.Add(model);
						
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTcms.Model.yjr_zx_business model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int id)
		{
			
			return dal.Delete(id);
		}
				/// <summary>
		/// 批量删除一批数据
		/// </summary>
		public bool DeleteList(string idlist )
		{
			return dal.DeleteList(idlist );
		}
		
		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.yjr_zx_business GetModel(int id)
		{
			
			return dal.GetModel(id);
		}

		

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<DTcms.Model.yjr_zx_business> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<DTcms.Model.yjr_zx_business> DataTableToList(DataTable dt)
		{
			List<DTcms.Model.yjr_zx_business> modelList = new List<DTcms.Model.yjr_zx_business>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				DTcms.Model.yjr_zx_business model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new DTcms.Model.yjr_zx_business();					
													if(dt.Rows[n]["id"].ToString()!="")
				{
					model.id=int.Parse(dt.Rows[n]["id"].ToString());
				}
																																				model.business_name= dt.Rows[n]["business_name"].ToString();
																																model.business_intro= dt.Rows[n]["business_intro"].ToString();
																												if(dt.Rows[n]["sort"].ToString()!="")
				{
					model.sort=int.Parse(dt.Rows[n]["sort"].ToString());
				}
																																if(dt.Rows[n]["status"].ToString()!="")
				{
					model.status=int.Parse(dt.Rows[n]["status"].ToString());
				}
																										
				
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		public DataSet GetList(int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
		{
			return dal.GetList(pageSize, pageIndex, strWhere, filedOrder, out recordCount);
		}
#endregion
   
	}
}