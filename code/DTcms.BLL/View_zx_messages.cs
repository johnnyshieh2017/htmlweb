﻿using System; 
using System.Text;
using System.Collections.Generic; 
using System.Data;
using DTcms.Common;
using DTcms.Model;
namespace DTcms.BLL {
	 	//View_zx_messages
		public partial class View_zx_messages
	{
   		     
		private readonly DTcms.DAL.View_zx_messages dal=new DTcms.DAL.View_zx_messages();
		public View_zx_messages()
		{}
		
		#region  Method
		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists()
		{
			return dal.Exists();
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public void  Add(DTcms.Model.View_zx_messages model)
		{
						dal.Add(model);
						
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTcms.Model.View_zx_messages model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete()
		{
			
			return dal.Delete();
		}
		
		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.View_zx_messages GetModel()
		{
			
			return dal.GetModel();
		}

		

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<DTcms.Model.View_zx_messages> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<DTcms.Model.View_zx_messages> DataTableToList(DataTable dt)
        {
            List<DTcms.Model.View_zx_messages> modelList = new List<DTcms.Model.View_zx_messages>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                DTcms.Model.View_zx_messages model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new DTcms.Model.View_zx_messages();
                    if (dt.Rows[n]["id"].ToString() != "")
                    {
                        model.id = int.Parse(dt.Rows[n]["id"].ToString());
                    }
                    if (dt.Rows[n]["userid"].ToString() != "")
                    {
                        model.userid = int.Parse(dt.Rows[n]["userid"].ToString());
                    }
                    model.sessionid = dt.Rows[n]["sessionid"].ToString();
                    if (dt.Rows[n]["doctorid"].ToString() != "")
                    {
                        model.doctorid = int.Parse(dt.Rows[n]["doctorid"].ToString());
                    }
                    if (dt.Rows[n]["zx_number"].ToString() != "")
                    {
                        model.zx_number = int.Parse(dt.Rows[n]["zx_number"].ToString());
                    }
                    model.zx_content = dt.Rows[n]["zx_content"].ToString();
                    if (dt.Rows[n]["createtime"].ToString() != "")
                    {
                        model.createtime = DateTime.Parse(dt.Rows[n]["createtime"].ToString());
                    }
                    if (dt.Rows[n]["status"].ToString() != "")
                    {
                        model.status = int.Parse(dt.Rows[n]["status"].ToString());
                    }
                    if (dt.Rows[n]["visited_status"].ToString() != "")
                    {
                        model.visited_status = int.Parse(dt.Rows[n]["visited_status"].ToString());
                    }
                    model.visited_hospital = dt.Rows[n]["visited_hospital"].ToString();
                    model.visited_department = dt.Rows[n]["visited_department"].ToString();
                    model.diagnosis_result = dt.Rows[n]["diagnosis_result"].ToString();
                    if (dt.Rows[n]["patientid"].ToString() != "")
                    {
                        model.patientid = int.Parse(dt.Rows[n]["patientid"].ToString());
                    }
                    if (dt.Rows[n]["owner"].ToString() != "")
                    {
                        model.owner = int.Parse(dt.Rows[n]["owner"].ToString());
                    }
                    if (dt.Rows[n]["read_status"].ToString() != "")
                    {
                        model.read_status = int.Parse(dt.Rows[n]["read_status"].ToString());
                    }
                    model.name = dt.Rows[n]["name"].ToString();
                    model.logoimg = dt.Rows[n]["logoimg"].ToString();
                    model.avatar = dt.Rows[n]["avatar"].ToString();
                    model.user_name = dt.Rows[n]["user_name"].ToString();
                    model.nick_name = dt.Rows[n]["nick_name"].ToString();
                    model.reportimg = dt.Rows[n]["reportimg"].ToString();

                    modelList.Add(model);
                }
            }
            return modelList;
        }

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		public DataSet GetList(int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
		{
			return dal.GetList(pageSize, pageIndex, strWhere, filedOrder, out recordCount);
		}
        #endregion


        public List<DTcms.Model.View_zx_messages> GetModelList(int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
        {
            DataSet ds = dal.GetList(pageSize, pageIndex, strWhere, filedOrder, out recordCount);
            return DataTableToList(ds.Tables[0]);
        }

    }
}