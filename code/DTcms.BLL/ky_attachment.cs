﻿using System; 
using System.Text;
using System.Collections.Generic; 
using System.Data;
using DTcms.Common;
using DTcms.Model;
namespace DTcms.BLL {
	 	//ky_attachment
		public partial class ky_attachment
	{
   		     
		private readonly DTcms.DAL.ky_attachment dal=new DTcms.DAL.ky_attachment();
		public ky_attachment()
		{}
		
		#region  Method
		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int am_id)
		{
			return dal.Exists(am_id);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public void  Add(DTcms.Model.ky_attachment model)
		{
						dal.Add(model);
						
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTcms.Model.ky_attachment model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int am_id)
		{
			
			return dal.Delete(am_id);
		}

		/// 批量删除一批数据
		/// </summary>
		public bool DeleteList(string am_idlist)
		{
			return dal.DeleteList(am_idlist);
		}
		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.ky_attachment GetModel(int am_id)
		{
			
			return dal.GetModel(am_id);
		}

		///// <summary>
		///// 得到一个对象实体，从缓存中
		///// </summary>
		//public Maticsoft.Model.ky_attachment GetModelByCache(int am_id)
		//{
			
		//	string CacheKey = "ky_attachmentModel-" + am_id;
		//	object objModel = Maticsoft.Common.DataCache.GetCache(CacheKey);
		//	if (objModel == null)
		//	{
		//		try
		//		{
		//			objModel = dal.GetModel(am_id);
		//			if (objModel != null)
		//			{
		//				int ModelCache = Maticsoft.Common.ConfigHelper.GetConfigInt("ModelCache");
		//				Maticsoft.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
		//			}
		//		}
		//		catch{}
		//	}
		//	return (Maticsoft.Model.ky_attachment)objModel;
		//}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<DTcms.Model.ky_attachment> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<DTcms.Model.ky_attachment> DataTableToList(DataTable dt)
		{
			List<DTcms.Model.ky_attachment> modelList = new List<DTcms.Model.ky_attachment>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				DTcms.Model.ky_attachment model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new DTcms.Model.ky_attachment();
					if (dt.Rows[n]["am_id"].ToString() != "")
					{
						model.am_id = int.Parse(dt.Rows[n]["am_id"].ToString());
					}
					model.file_name = dt.Rows[n]["file_name"].ToString();
					model.file_path = dt.Rows[n]["file_path"].ToString();
					if (dt.Rows[n]["p_id"].ToString() != "")
					{
						model.p_id = int.Parse(dt.Rows[n]["p_id"].ToString());
					}
					if (dt.Rows[n]["sort"].ToString() != "")
					{
						model.sort = int.Parse(dt.Rows[n]["sort"].ToString());
					}
					if (dt.Rows[n]["add_time"].ToString() != "")
					{
						model.add_time = DateTime.Parse(dt.Rows[n]["add_time"].ToString());
					}
					if (dt.Rows[n]["status"].ToString() != "")
					{
						model.status = int.Parse(dt.Rows[n]["status"].ToString());
					}
					model.apply_id = dt.Rows[n]["apply_id"].ToString();

					if (dt.Rows[n]["file_size"].ToString() != "")
					{
						model.file_size = int.Parse(dt.Rows[n]["file_size"].ToString());
					}
					model.file_ext = dt.Rows[n]["file_ext"].ToString();

					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// 批量删除一批数据
		/// </summary>
		public bool DeletebyP_id(string p_id)
		{
			return dal.DeletebyP_id(p_id);
		}

		public bool Deletebyapply_id(string apply_id)
		{
			return dal.Deletebyapply_id(apply_id);
		}
		#endregion

	}
}