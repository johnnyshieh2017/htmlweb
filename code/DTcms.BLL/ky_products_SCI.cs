﻿/**  版本信息模板在安装目录下，可自行修改。
* ky_products_SCI.cs
*
* 功 能： N/A
* 类 名： ky_products_SCI
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2021/7/14 16:21:21   N/A    初版
*

*/
using System;
using System.Data;
using System.Collections.Generic;
using DTcms.Common;
using DTcms.Model;
namespace DTcms.BLL
{
	/// <summary>
	/// ky_products_SCI
	/// </summary>
	public partial class ky_products_SCI
	{
		private readonly DTcms.DAL.ky_products_SCI dal = new DTcms.DAL.ky_products_SCI();
		public ky_products_SCI()
		{ }
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int pd_id)
		{
			return dal.Exists(pd_id);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(DTcms.Model.ky_products_SCI model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTcms.Model.ky_products_SCI model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int pd_id)
		{

			return dal.Delete(pd_id);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string pd_idlist)
		{
			return dal.DeleteList(pd_idlist);
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTcms.Model.ky_products_SCI GetModel(int pd_id)
		{

			return dal.GetModel(pd_id);
		}



		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top, string strWhere, string filedOrder)
		{
			return dal.GetList(Top, strWhere, filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<DTcms.Model.ky_products_SCI> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<DTcms.Model.ky_products_SCI> DataTableToList(DataTable dt)
		{
			List<DTcms.Model.ky_products_SCI> modelList = new List<DTcms.Model.ky_products_SCI>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				DTcms.Model.ky_products_SCI model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = dal.DataRowToModel(dt.Rows[n]);
					if (model != null)
					{
						modelList.Add(model);
					}
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage(strWhere, orderby, startIndex, endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
		//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  BasicMethod
		#region  ExtensionMethod
		public DataSet GetList(int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
		{
			return dal.GetList(pageSize, pageIndex, strWhere, filedOrder, out recordCount);
		}
		#endregion  ExtensionMethod
	}
}

