﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
using DTcms.Common;
using DTcms.Model;
namespace DTcms.BLL
{
    //yjr_doc_hospital
    public partial class yjr_doc_hospital
    {

        private readonly DTcms.DAL.yjr_doc_hospital dal = new DTcms.DAL.yjr_doc_hospital();
        public yjr_doc_hospital()
        { }

        #region  Method
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int id)
        {
            return dal.Exists(id);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(DTcms.Model.yjr_doc_hospital model)
        {
            return dal.Add(model);

        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(DTcms.Model.yjr_doc_hospital model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int id)
        {

            return dal.Delete(id);
        }
        /// <summary>
        /// 批量删除一批数据
        /// </summary>
        public bool DeleteList(string idlist)
        {
            return dal.DeleteList(idlist);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public DTcms.Model.yjr_doc_hospital GetModel(int id)
        {

            return dal.GetModel(id);
        }



        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<DTcms.Model.yjr_doc_hospital> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<DTcms.Model.yjr_doc_hospital> DataTableToList(DataTable dt)
        {
            List<DTcms.Model.yjr_doc_hospital> modelList = new List<DTcms.Model.yjr_doc_hospital>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                DTcms.Model.yjr_doc_hospital model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new DTcms.Model.yjr_doc_hospital();
                    if (dt.Rows[n]["id"].ToString() != "")
                    {
                        model.id = int.Parse(dt.Rows[n]["id"].ToString());
                    }
                    model.hospitalname = dt.Rows[n]["hospitalname"].ToString();
                    model.hoslevel = dt.Rows[n]["hoslevel"].ToString();
                    model.address = dt.Rows[n]["address"].ToString();
                    model.tel = dt.Rows[n]["tel"].ToString();
                    model.map = dt.Rows[n]["map"].ToString();
                    model.description = dt.Rows[n]["description"].ToString();
                    model.logo_url = dt.Rows[n]["logo_url"].ToString();


                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        public DataSet GetList(int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
        {
            return dal.GetList(pageSize, pageIndex, strWhere, filedOrder, out recordCount);
        }
        #endregion

    }
}