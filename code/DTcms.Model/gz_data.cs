﻿/**  版本信息模板在安装目录下，可自行修改。
* gz_data.cs
*
* 功 能： N/A
* 类 名： gz_data
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2020/3/14 10:52:31   N/A    初版
*
* Copyright (c) 2019  Corporation. All rights reserved.
*/
using System;
namespace DTcms.Model
{
	/// <summary>
	/// gz_data:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class gz_data
	{
		public gz_data()
		{}
		#region Model
		private string _观众id;
		private string _观众名;
		private string _第三方id;
		private string _地址;
		private DateTime? _观看总时长;
		private decimal? _手机号;
		private string _访问来源;
		private decimal? _访问次数;
		private decimal? _分享次数;
		private decimal? _聊天次数;
		private DateTime? _在线时长;
		private DateTime? _首次进入时间;
		private DateTime? _最后离开时间;
		private string _最后观看设备;
		private string _最后观看方式;
		private string _最后登录ip;
		private string _是否职工;
		private string _省;
		private string _市;
		private string _公司;
		/// <summary>
		/// 
		/// </summary>
		public string 观众ID
		{
			set{ _观众id=value;}
			get{return _观众id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string 观众名
		{
			set{ _观众名=value;}
			get{return _观众名;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string 第三方ID
		{
			set{ _第三方id=value;}
			get{return _第三方id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string 地址
		{
			set{ _地址=value;}
			get{return _地址;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? 观看总时长
		{
			set{ _观看总时长=value;}
			get{return _观看总时长;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? 手机号
		{
			set{ _手机号=value;}
			get{return _手机号;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string 访问来源
		{
			set{ _访问来源=value;}
			get{return _访问来源;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? 访问次数
		{
			set{ _访问次数=value;}
			get{return _访问次数;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? 分享次数
		{
			set{ _分享次数=value;}
			get{return _分享次数;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? 聊天次数
		{
			set{ _聊天次数=value;}
			get{return _聊天次数;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? 在线时长
		{
			set{ _在线时长=value;}
			get{return _在线时长;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? 首次进入时间
		{
			set{ _首次进入时间=value;}
			get{return _首次进入时间;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? 最后离开时间
		{
			set{ _最后离开时间=value;}
			get{return _最后离开时间;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string 最后观看设备
		{
			set{ _最后观看设备=value;}
			get{return _最后观看设备;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string 最后观看方式
		{
			set{ _最后观看方式=value;}
			get{return _最后观看方式;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string 最后登录ip
		{
			set{ _最后登录ip=value;}
			get{return _最后登录ip;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string 是否职工
		{
			set{ _是否职工=value;}
			get{return _是否职工;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string 省
		{
			set{ _省=value;}
			get{return _省;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string 市
		{
			set{ _市=value;}
			get{return _市;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string 公司
		{
			set{ _公司=value;}
			get{return _公司;}
		}
		#endregion Model

	}
}

