﻿using System; 
using System.Text;
using System.Collections.Generic; 
using System.Data;
namespace DTcms.Model{
	 	//yjr_doctor_vsbusiness
		public class yjr_doctor_vsbusiness
	{
   		     
      	/// <summary>
		/// doctorid
        /// </summary>		
		private int _doctorid;
        public int doctorid
        {
            get{ return _doctorid; }
            set{ _doctorid = value; }
        }        
		/// <summary>
		/// businessid
        /// </summary>		
		private int _businessid;
        public int businessid
        {
            get{ return _businessid; }
            set{ _businessid = value; }
        }        
		/// <summary>
		/// add_time
        /// </summary>		
		private DateTime _add_time;
        public DateTime add_time
        {
            get{ return _add_time; }
            set{ _add_time = value; }
        }        
		/// <summary>
		/// status
        /// </summary>		
		private int _status;
        public int status
        {
            get{ return _status; }
            set{ _status = value; }
        }        
		   
	}
}

