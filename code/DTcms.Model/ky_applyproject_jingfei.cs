﻿/**  版本信息模板在安装目录下，可自行修改。
* ky_applyproject_jingfei.cs
*
* 功 能： N/A
* 类 名： ky_applyproject_jingfei
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2021/8/2 17:33:21   N/A    初版
*

*/
using System;
namespace DTcms.Model
{
	/// <summary>
	/// ky_applyproject_jingfei:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class ky_applyproject_jingfei
	{
		public ky_applyproject_jingfei()
		{}
		#region Model
		private int _id;
		private string _pa_id;
		private string _laiyuan_zhuanxiang;
		private string _laiyuan_qita;
		private string _zhijie_shebei;
		private string _zhijie_sb_gouzhi;
		private string _zhijie_sb_shizhi;
		private string _zhijie_sb_gaizao;
		private string _zhijie_cailiao;
		private string _zhijie_ceshi;
		private string _zhijie_ranliao;
		private string _zhijie_chailv;
		private string _zhijie_huiyi;
		private string _zhijie_hezuojiaoliu;
		private string _zhijie_xinxi;
		private string _zhijie_zixun;
		private string _zhijie_laowu;
		private string _zhijie_qita;
		private string _jianjie_guanli;
		private string _jianjie_jixiao;
		private string _zhijie_shebei1;
		private string _zhijie_sb_gouzhi1;
		private string _zhijie_sb_shizhi1;
		private string _zhijie_sb_gaizao1;
		private string _zhijie_cailiao1;
		private string _zhijie_ceshi1;
		private string _zhijie_ranliao1;
		private string _zhijie_chailv1;
		private string _zhijie_huiyi1;
		private string _zhijie_hezuojiaoliu1;
		private string _zhijie_xinxi1;
		private string _zhijie_zixun1;
		private string _zhijie_laowu1;
		private string _zhijie_qita1;
		private string _jianjie_guanli1;
		private string _jianjie_jixiao1;
		private string _zhijie_shebei2;
		private string _zhijie_sb_gouzhi2;
		private string _zhijie_sb_shizhi2;
		private string _zhijie_sb_gaizao2;
		private string _zhijie_cailiao2;
		private string _zhijie_ceshi2;
		private string _zhijie_ranliao2;
		private string _zhijie_chailv2;
		private string _zhijie_huiyi2;
		private string _zhijie_hezuojiaoliu2;
		private string _zhijie_xinxi2;
		private string _zhijie_zixun2;
		private string _zhijie_laowu2;
		private string _zhijie_qita2;
		private string _jianjie_guanli2;
		private string _jianjie_jixiao2;
		private DateTime _add_time;
		private string _add_user;
		private int _status;
		private string _zhijie1;
		private string _zhijie2;
		private string _zhijie;
		private string _jianjie;
		private string _jianjie1;
		private string _jianjie2;
		private string _heji;
		private string _heji1;
		private string _heji2;
		/// <summary>
		/// 
		/// </summary>
		public int id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string pa_id
		{
			set{ _pa_id=value;}
			get{return _pa_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string laiyuan_zhuanxiang
		{
			set{ _laiyuan_zhuanxiang=value;}
			get{return _laiyuan_zhuanxiang;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string laiyuan_qita
		{
			set{ _laiyuan_qita=value;}
			get{return _laiyuan_qita;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhijie_shebei
		{
			set{ _zhijie_shebei=value;}
			get{return _zhijie_shebei;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhijie_sb_gouzhi
		{
			set{ _zhijie_sb_gouzhi=value;}
			get{return _zhijie_sb_gouzhi;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhijie_sb_shizhi
		{
			set{ _zhijie_sb_shizhi=value;}
			get{return _zhijie_sb_shizhi;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhijie_sb_gaizao
		{
			set{ _zhijie_sb_gaizao=value;}
			get{return _zhijie_sb_gaizao;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhijie_cailiao
		{
			set{ _zhijie_cailiao=value;}
			get{return _zhijie_cailiao;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhijie_ceshi
		{
			set{ _zhijie_ceshi=value;}
			get{return _zhijie_ceshi;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhijie_ranliao
		{
			set{ _zhijie_ranliao=value;}
			get{return _zhijie_ranliao;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhijie_chailv
		{
			set{ _zhijie_chailv=value;}
			get{return _zhijie_chailv;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhijie_huiyi
		{
			set{ _zhijie_huiyi=value;}
			get{return _zhijie_huiyi;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhijie_hezuojiaoliu
		{
			set{ _zhijie_hezuojiaoliu=value;}
			get{return _zhijie_hezuojiaoliu;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhijie_xinxi
		{
			set{ _zhijie_xinxi=value;}
			get{return _zhijie_xinxi;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhijie_zixun
		{
			set{ _zhijie_zixun=value;}
			get{return _zhijie_zixun;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhijie_laowu
		{
			set{ _zhijie_laowu=value;}
			get{return _zhijie_laowu;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhijie_qita
		{
			set{ _zhijie_qita=value;}
			get{return _zhijie_qita;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string jianjie_guanli
		{
			set{ _jianjie_guanli=value;}
			get{return _jianjie_guanli;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string jianjie_jixiao
		{
			set{ _jianjie_jixiao=value;}
			get{return _jianjie_jixiao;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhijie_shebei1
		{
			set{ _zhijie_shebei1=value;}
			get{return _zhijie_shebei1;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhijie_sb_gouzhi1
		{
			set{ _zhijie_sb_gouzhi1=value;}
			get{return _zhijie_sb_gouzhi1;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhijie_sb_shizhi1
		{
			set{ _zhijie_sb_shizhi1=value;}
			get{return _zhijie_sb_shizhi1;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhijie_sb_gaizao1
		{
			set{ _zhijie_sb_gaizao1=value;}
			get{return _zhijie_sb_gaizao1;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhijie_cailiao1
		{
			set{ _zhijie_cailiao1=value;}
			get{return _zhijie_cailiao1;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhijie_ceshi1
		{
			set{ _zhijie_ceshi1=value;}
			get{return _zhijie_ceshi1;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhijie_ranliao1
		{
			set{ _zhijie_ranliao1=value;}
			get{return _zhijie_ranliao1;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhijie_chailv1
		{
			set{ _zhijie_chailv1=value;}
			get{return _zhijie_chailv1;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhijie_huiyi1
		{
			set{ _zhijie_huiyi1=value;}
			get{return _zhijie_huiyi1;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhijie_hezuojiaoliu1
		{
			set{ _zhijie_hezuojiaoliu1=value;}
			get{return _zhijie_hezuojiaoliu1;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhijie_xinxi1
		{
			set{ _zhijie_xinxi1=value;}
			get{return _zhijie_xinxi1;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhijie_zixun1
		{
			set{ _zhijie_zixun1=value;}
			get{return _zhijie_zixun1;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhijie_laowu1
		{
			set{ _zhijie_laowu1=value;}
			get{return _zhijie_laowu1;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhijie_qita1
		{
			set{ _zhijie_qita1=value;}
			get{return _zhijie_qita1;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string jianjie_guanli1
		{
			set{ _jianjie_guanli1=value;}
			get{return _jianjie_guanli1;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string jianjie_jixiao1
		{
			set{ _jianjie_jixiao1=value;}
			get{return _jianjie_jixiao1;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhijie_shebei2
		{
			set{ _zhijie_shebei2=value;}
			get{return _zhijie_shebei2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhijie_sb_gouzhi2
		{
			set{ _zhijie_sb_gouzhi2=value;}
			get{return _zhijie_sb_gouzhi2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhijie_sb_shizhi2
		{
			set{ _zhijie_sb_shizhi2=value;}
			get{return _zhijie_sb_shizhi2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhijie_sb_gaizao2
		{
			set{ _zhijie_sb_gaizao2=value;}
			get{return _zhijie_sb_gaizao2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhijie_cailiao2
		{
			set{ _zhijie_cailiao2=value;}
			get{return _zhijie_cailiao2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhijie_ceshi2
		{
			set{ _zhijie_ceshi2=value;}
			get{return _zhijie_ceshi2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhijie_ranliao2
		{
			set{ _zhijie_ranliao2=value;}
			get{return _zhijie_ranliao2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhijie_chailv2
		{
			set{ _zhijie_chailv2=value;}
			get{return _zhijie_chailv2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhijie_huiyi2
		{
			set{ _zhijie_huiyi2=value;}
			get{return _zhijie_huiyi2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhijie_hezuojiaoliu2
		{
			set{ _zhijie_hezuojiaoliu2=value;}
			get{return _zhijie_hezuojiaoliu2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhijie_xinxi2
		{
			set{ _zhijie_xinxi2=value;}
			get{return _zhijie_xinxi2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhijie_zixun2
		{
			set{ _zhijie_zixun2=value;}
			get{return _zhijie_zixun2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhijie_laowu2
		{
			set{ _zhijie_laowu2=value;}
			get{return _zhijie_laowu2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhijie_qita2
		{
			set{ _zhijie_qita2=value;}
			get{return _zhijie_qita2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string jianjie_guanli2
		{
			set{ _jianjie_guanli2=value;}
			get{return _jianjie_guanli2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string jianjie_jixiao2
		{
			set{ _jianjie_jixiao2=value;}
			get{return _jianjie_jixiao2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime add_time
		{
			set{ _add_time=value;}
			get{return _add_time;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string add_user
		{
			set{ _add_user=value;}
			get{return _add_user;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int status
		{
			set{ _status=value;}
			get{return _status;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhijie1
		{
			set{ _zhijie1=value;}
			get{return _zhijie1;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhijie2
		{
			set{ _zhijie2=value;}
			get{return _zhijie2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhijie
		{
			set{ _zhijie=value;}
			get{return _zhijie;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string jianjie
		{
			set{ _jianjie=value;}
			get{return _jianjie;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string jianjie1
		{
			set{ _jianjie1=value;}
			get{return _jianjie1;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string jianjie2
		{
			set{ _jianjie2=value;}
			get{return _jianjie2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string heji
		{
			set{ _heji=value;}
			get{return _heji;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string heji1
		{
			set{ _heji1=value;}
			get{return _heji1;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string heji2
		{
			set{ _heji2=value;}
			get{return _heji2;}
		}
		#endregion Model

	}
}

