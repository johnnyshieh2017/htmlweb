﻿/**  版本信息模板在安装目录下，可自行修改。
* yjr_docvsuser.cs
*
* 功 能： N/A
* 类 名： yjr_docvsuser
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2020/6/23 9:34:34   N/A    初版
*
* Copyright (c) 2019  Corporation. All rights reserved.
*/
using System;
namespace DTcms.Model
{
    /// <summary>
    /// （弃用）医生基本信息与账户信息对应关系
    /// </summary>
    [Serializable]
	public partial class yjr_docvsuser
	{
		public yjr_docvsuser()
		{}
		#region Model
		private string _docid;
		private string _userid;
		/// <summary>
		/// 
		/// </summary>
		public string docid
		{
			set{ _docid=value;}
			get{return _docid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string userid
		{
			set{ _userid=value;}
			get{return _userid;}
		}
		#endregion Model

	}
}

