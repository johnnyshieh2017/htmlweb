﻿/**  版本信息模板在安装目录下，可自行修改。
* yjr_doctor_vstag.cs
*
* 功 能： N/A
* 类 名： yjr_doctor_vstag
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2020/8/5 11:06:43   N/A    初版
*
* Copyright (c) 2019  Corporation. All rights reserved.
*/
using System;
namespace DTcms.Model
{
	/// <summary>
	/// yjr_doctor_vstag:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class yjr_doctor_vstag
	{
		public yjr_doctor_vstag()
		{}
		#region Model
		private int _doctor_id;
		private int _tagid;
		private DateTime? _add_time;
		/// <summary>
		/// 
		/// </summary>
		public int doctor_id
		{
			set{ _doctor_id=value;}
			get{return _doctor_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int tagid
		{
			set{ _tagid=value;}
			get{return _tagid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? add_time
		{
			set{ _add_time=value;}
			get{return _add_time;}
		}
		#endregion Model

	}
}

