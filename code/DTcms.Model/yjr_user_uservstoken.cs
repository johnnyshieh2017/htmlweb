﻿/**  版本信息模板在安装目录下，可自行修改。
* yjr_user_uservstoken.cs
*
* 功 能： N/A
* 类 名： yjr_user_uservstoken
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2020/8/27 9:21:27   N/A    初版
*
* Copyright (c) 2019  Corporation. All rights reserved.
*/
using System;
namespace DTcms.Model
{
	/// <summary>
	/// yjr_user_uservstoken:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class yjr_user_uservstoken
	{
		public yjr_user_uservstoken()
		{}
		#region Model
		private int _userid;
		private string _token;
		private DateTime _expires;
		/// <summary>
		/// 
		/// </summary>
		public int userid
		{
			set{ _userid=value;}
			get{return _userid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string token
		{
			set{ _token=value;}
			get{return _token;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime expires
		{
			set{ _expires=value;}
			get{return _expires;}
		}
		#endregion Model

	}
}

