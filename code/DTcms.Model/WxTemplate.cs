﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTcms.Model
{
    public class WxTemplate
    {
        /// <summary>
        /// 目标用户OpenId
        /// </summary>
        public string touser { get; set; }
        /// <summary>
        /// 模板ID
        /// </summary>
        public string template_id { get; set; }
        /// <summary>
        /// 模板消息顶部颜色（16进制），默认为#FF0000
        /// </summary>
        public string topcolor { get; set; }
        /// <summary>
        /// 模板跳转链接
        /// </summary>
        public string url { get; set; }
        /// <summary>
        /// 数据
        /// </summary>
        public object data { get; set; }
        public WxTemplate()
        {
            topcolor = "#FF0000";
        }
    }

    public class TemplateDataItem
    {
        /// <summary>
        /// 项目值
        /// </summary>
        public string value { get; set; }
        /// <summary>
        /// 16进制颜色代码，如：#FF0000
        /// </summary>
        public string color { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v">value</param>
        /// <param name="c">color</param>
        public TemplateDataItem(string v, string c = "#173177")
        {
            value = v;
            color = c;
        }
    }


    public class MemZXTemplate
    {
        /// <summary>
        /// 标题
        /// </summary>
        public TemplateDataItem title { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        public TemplateDataItem type { get; set; }
        /// <summary>
        /// 日期
        /// </summary>
        public TemplateDataItem infortime { get; set; }
     
      
      
        /// <summary>
        /// 咨询人
        /// </summary>
        public TemplateDataItem member { get; set; }
    }

    public class wxTemplate01
    {
        /// <summary>
        /// 标题
        /// </summary>
        public TemplateDataItem createtime { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        public TemplateDataItem sender { get; set; }
        /// <summary>
        /// 日期
        /// </summary>
        public TemplateDataItem sendtype { get; set; }



      
    }

}
