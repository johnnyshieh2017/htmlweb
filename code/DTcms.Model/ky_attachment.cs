﻿using System; 
using System.Text;
using System.Collections.Generic; 
using System.Data;
namespace DTcms.Model{
	 	//ky_attachment
		public class ky_attachment
	{
   		     
      	/// <summary>
		/// am_id
        /// </summary>		
		private int _am_id;
        public int am_id
        {
            get{ return _am_id; }
            set{ _am_id = value; }
        }        
		/// <summary>
		/// file_name
        /// </summary>		
		private string _file_name;
        public string file_name
        {
            get{ return _file_name; }
            set{ _file_name = value; }
        }        
		/// <summary>
		/// file_path
        /// </summary>		
		private string _file_path;
        public string file_path
        {
            get{ return _file_path; }
            set{ _file_path = value; }
        }        
		/// <summary>
		/// file_size
        /// </summary>		
		private int _file_size;
        public int file_size
        {
            get{ return _file_size; }
            set{ _file_size = value; }
        }        
		/// <summary>
		/// file_ext
        /// </summary>		
		private string _file_ext;
        public string file_ext
        {
            get{ return _file_ext; }
            set{ _file_ext = value; }
        }        
		/// <summary>
		/// p_id
        /// </summary>		
		private int _p_id;
        public int p_id
        {
            get{ return _p_id; }
            set{ _p_id = value; }
        }        
		/// <summary>
		/// sort
        /// </summary>		
		private int _sort;
        public int sort
        {
            get{ return _sort; }
            set{ _sort = value; }
        }        
		/// <summary>
		/// add_time
        /// </summary>		
		private DateTime _add_time;
        public DateTime add_time
        {
            get{ return _add_time; }
            set{ _add_time = value; }
        }        
		/// <summary>
		/// status
        /// </summary>		
		private int _status;
        public int status
        {
            get{ return _status; }
            set{ _status = value; }
        }        
		/// <summary>
		/// apply_id
        /// </summary>		
		private string _apply_id;
        public string apply_id
        {
            get{ return _apply_id; }
            set{ _apply_id = value; }
        }        
		   
	}
}

