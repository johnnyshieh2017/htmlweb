﻿/**  版本信息模板在安装目录下，可自行修改。
* zg_data.cs
*
* 功 能： N/A
* 类 名： zg_data
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2020/3/14 10:52:32   N/A    初版
*
* Copyright (c) 2019  Corporation. All rights reserved.
*/
using System;
namespace DTcms.Model
{
	/// <summary>
	/// zg_data:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class zg_data
	{
		public zg_data()
		{}
		#region Model
		private string _省区;
		private string _城市;
		private string _公司名称;
		private string _姓名;
		private string _职务;
		private string _微信昵称;
		private string _手机号;
		private string _f8;
		/// <summary>
		/// 
		/// </summary>
		public string 省区
		{
			set{ _省区=value;}
			get{return _省区;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string 城市
		{
			set{ _城市=value;}
			get{return _城市;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string 公司名称
		{
			set{ _公司名称=value;}
			get{return _公司名称;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string 姓名
		{
			set{ _姓名=value;}
			get{return _姓名;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string 职务
		{
			set{ _职务=value;}
			get{return _职务;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string 微信昵称
		{
			set{ _微信昵称=value;}
			get{return _微信昵称;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string 手机号
		{
			set{ _手机号=value;}
			get{return _手机号;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string F8
		{
			set{ _f8=value;}
			get{return _f8;}
		}
		#endregion Model

	}
}

