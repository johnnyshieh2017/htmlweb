﻿/**  版本信息模板在安装目录下，可自行修改。
* yjr_zx_books.cs
*
* 功 能： N/A
* 类 名： yjr_zx_books
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2020/8/5 11:06:45   N/A    初版
*
* Copyright (c) 2019  Corporation. All rights reserved.
*/
using System;
namespace DTcms.Model
{
	/// <summary>
	/// yjr_zx_books:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class yjr_zx_books
	{
		public yjr_zx_books()
		{}
		#region Model
		private int _id;
		private int _business_type;
		private int _userid;
		private DateTime _bookdate;
		private string _timepart;
		private int _doctorid;
		private DateTime _createtime;
		/// <summary>
		/// 
		/// </summary>
		public int id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int business_type
		{
			set{ _business_type=value;}
			get{return _business_type;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int userid
		{
			set{ _userid=value;}
			get{return _userid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime bookdate
		{
			set{ _bookdate=value;}
			get{return _bookdate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string timepart
		{
			set{ _timepart=value;}
			get{return _timepart;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int doctorid
		{
			set{ _doctorid=value;}
			get{return _doctorid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime createtime
		{
			set{ _createtime=value;}
			get{return _createtime;}
		}
		#endregion Model

	}
}

