﻿using System; 
using System.Text;
using System.Collections.Generic; 
using System.Data;
namespace DTcms.Model{
	 	//View_zx_order
		public class View_zx_order
	{
   		     
      	/// <summary>
		/// id
        /// </summary>		
		private int _id;
        public int id
        {
            get{ return _id; }
            set{ _id = value; }
        }        
		/// <summary>
		/// userid
        /// </summary>		
		private int _userid;
        public int userid
        {
            get{ return _userid; }
            set{ _userid = value; }
        }        
		/// <summary>
		/// service_type
        /// </summary>		
		private int _service_type;
        public int service_type
        {
            get{ return _service_type; }
            set{ _service_type = value; }
        }        
		/// <summary>
		/// businessconfigid
        /// </summary>		
		private int _businessconfigid;
        public int businessconfigid
        {
            get{ return _businessconfigid; }
            set{ _businessconfigid = value; }
        }        
		/// <summary>
		/// businessappointmentid
        /// </summary>		
		private int _businessappointmentid;
        public int businessappointmentid
        {
            get{ return _businessappointmentid; }
            set{ _businessappointmentid = value; }
        }        
		/// <summary>
		/// question_desc
        /// </summary>		
		private string _question_desc;
        public string question_desc
        {
            get{ return _question_desc; }
            set{ _question_desc = value; }
        }        
		/// <summary>
		/// doctor_id
        /// </summary>		
		private int _doctor_id;
        public int doctor_id
        {
            get{ return _doctor_id; }
            set{ _doctor_id = value; }
        }        
		/// <summary>
		/// createtime
        /// </summary>		
		private DateTime _createtime;
        public DateTime createtime
        {
            get{ return _createtime; }
            set{ _createtime = value; }
        }        
		/// <summary>
		/// service_fee
        /// </summary>		
		private int _service_fee;
        public int service_fee
        {
            get{ return _service_fee; }
            set{ _service_fee = value; }
        }        
		/// <summary>
		/// pay_status
        /// </summary>		
		private int _pay_status;
        public int pay_status
        {
            get{ return _pay_status; }
            set{ _pay_status = value; }
        }        
		/// <summary>
		/// status
        /// </summary>		
		private int _status;
        public int status
        {
            get{ return _status; }
            set{ _status = value; }
        }        
		/// <summary>
		/// pay_time
        /// </summary>		
		private DateTime _pay_time;
        public DateTime pay_time
        {
            get{ return _pay_time; }
            set{ _pay_time = value; }
        }        
		/// <summary>
		/// ms_sessionid
        /// </summary>		
		private string _ms_sessionid;
        public string ms_sessionid
        {
            get{ return _ms_sessionid; }
            set{ _ms_sessionid = value; }
        }        
		/// <summary>
		/// business_name
        /// </summary>		
		private string _business_name;
        public string business_name
        {
            get{ return _business_name; }
            set{ _business_name = value; }
        }        
		/// <summary>
		/// name
        /// </summary>		
		private string _name;
        public string name
        {
            get{ return _name; }
            set{ _name = value; }
        }        
		/// <summary>
		/// logoimg
        /// </summary>		
		private string _logoimg;
        public string logoimg
        {
            get{ return _logoimg; }
            set{ _logoimg = value; }
        }        
		/// <summary>
		/// titlename
        /// </summary>		
		private string _titlename;
        public string titlename
        {
            get{ return _titlename; }
            set{ _titlename = value; }
        }        
		/// <summary>
		/// avatar
        /// </summary>		
		private string _avatar;
        public string avatar
        {
            get{ return _avatar; }
            set{ _avatar = value; }
        }        
		/// <summary>
		/// nick_name
        /// </summary>		
		private string _nick_name;
        public string nick_name
        {
            get{ return _nick_name; }
            set{ _nick_name = value; }
        }        
		/// <summary>
		/// user_name
        /// </summary>		
		private string _user_name;
        public string user_name
        {
            get{ return _user_name; }
            set{ _user_name = value; }
        }        
		/// <summary>
		/// patientid
        /// </summary>		
		private int _patientid;
        public int patientid
        {
            get{ return _patientid; }
            set{ _patientid = value; }
        }        
		/// <summary>
		/// patient_name
        /// </summary>		
		private string _patient_name;
        public string patient_name
        {
            get{ return _patient_name; }
            set{ _patient_name = value; }
        }        
		/// <summary>
		/// patient_gender
        /// </summary>		
		private string _patient_gender;
        public string patient_gender
        {
            get{ return _patient_gender; }
            set{ _patient_gender = value; }
        }        
		/// <summary>
		/// patient_age
        /// </summary>		
		private int _patient_age;
        public int patient_age
        {
            get{ return _patient_age; }
            set{ _patient_age = value; }
        }        
		/// <summary>
		/// hospitalname
        /// </summary>		
		private string _hospitalname;
        public string hospitalname
        {
            get{ return _hospitalname; }
            set{ _hospitalname = value; }
        }        
		/// <summary>
		/// hoslevel
        /// </summary>		
		private string _hoslevel;
        public string hoslevel
        {
            get{ return _hoslevel; }
            set{ _hoslevel = value; }
        }        
		/// <summary>
		/// groupname
        /// </summary>		
		private string _groupname;
        public string groupname
        {
            get{ return _groupname; }
            set{ _groupname = value; }
        }        
		/// <summary>
		/// diagnosis_result
        /// </summary>		
		private string _diagnosis_result;
        public string diagnosis_result
        {
            get{ return _diagnosis_result; }
            set{ _diagnosis_result = value; }
        }        
		/// <summary>
		/// visited_department
        /// </summary>		
		private string _visited_department;
        public string visited_department
        {
            get{ return _visited_department; }
            set{ _visited_department = value; }
        }        
		/// <summary>
		/// visited_hospital
        /// </summary>		
		private string _visited_hospital;
        public string visited_hospital
        {
            get{ return _visited_hospital; }
            set{ _visited_hospital = value; }
        }        
		/// <summary>
		/// visited_status
        /// </summary>		
		private int _visited_status;
        public int visited_status
        {
            get{ return _visited_status; }
            set{ _visited_status = value; }
        }

        /// <summary>
		/// sessionid
        /// </summary>		
		private string _sessionid;
        public string sessionid
        {
            get { return _sessionid; }
            set { _sessionid = value; }
        }

        /// <summary>
        /// reportimg
        /// </summary>		
        private string _reportimg;
        public string reportimg
        {
            get { return _reportimg; }
            set { _reportimg = value; }
        }
    }
}

