﻿/**  版本信息模板在安装目录下，可自行修改。
* ky_products_lunzhu.cs
*
* 功 能： N/A
* 类 名： ky_products_lunzhu
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2021/7/14 16:21:21   N/A    初版
*

*/
using System;
namespace DTcms.Model
{
	/// <summary>
	/// ky_products_lunzhu:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class ky_products_lunzhu
	{
		public ky_products_lunzhu()
		{}
		#region Model
		private int _pd_id;
		private DateTime _add_time;
		private string _add_user;
		private int _p_id;
		private string _shu_ji_ming_cheng;
		private string _chu_ban_she;
		private int _dptid;
		private string _chu_ban_ri_qi;
		private string _zuo_zhe;
		private string _zuo_zhe_lei_xing;
		private string _bian_xie_zhang_jie;
		private string _beizhu;
		/// <summary>
		/// 
		/// </summary>
		public int pd_id
		{
			set{ _pd_id=value;}
			get{return _pd_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime add_time
		{
			set{ _add_time=value;}
			get{return _add_time;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string add_user
		{
			set{ _add_user=value;}
			get{return _add_user;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int p_id
		{
			set{ _p_id=value;}
			get{return _p_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string shu_ji_ming_cheng
		{
			set{ _shu_ji_ming_cheng=value;}
			get{return _shu_ji_ming_cheng;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string chu_ban_she
		{
			set{ _chu_ban_she=value;}
			get{return _chu_ban_she;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int dptid
		{
			set{ _dptid=value;}
			get{return _dptid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string chu_ban_ri_qi
		{
			set{ _chu_ban_ri_qi=value;}
			get{return _chu_ban_ri_qi;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zuo_zhe
		{
			set{ _zuo_zhe=value;}
			get{return _zuo_zhe;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zuo_zhe_lei_xing
		{
			set{ _zuo_zhe_lei_xing=value;}
			get{return _zuo_zhe_lei_xing;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string bian_xie_zhang_jie
		{
			set{ _bian_xie_zhang_jie=value;}
			get{return _bian_xie_zhang_jie;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string beizhu
		{
			set{ _beizhu=value;}
			get{return _beizhu;}
		}
		#endregion Model

	}
}

