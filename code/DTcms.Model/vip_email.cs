﻿/**  版本信息模板在安装目录下，可自行修改。
* vip_email.cs
*
* 功 能： N/A
* 类 名： vip_email
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2020/12/29 10:57:33   N/A    初版
*
* Copyright (c) 2019  Corporation. All rights reserved.
*/
using System;
namespace DTcms.Model
{
	/// <summary>
	/// vip_email:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class vip_email
	{
		public vip_email()
		{}
		#region Model
		private int _id;
		private int _mail_type;
		private int _mail_status;
		private DateTime _add_time;
		private string _mail_title;
		private string _mail_content;
		private string _templateid;
		private string _mail_to;
		private DateTime _send_date;
		private DateTime? _complate_date;
		private string _mail_ext;
		/// <summary>
		/// 
		/// </summary>
		public int id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int mail_type
		{
			set{ _mail_type=value;}
			get{return _mail_type;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int mail_status
		{
			set{ _mail_status=value;}
			get{return _mail_status;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime add_time
		{
			set{ _add_time=value;}
			get{return _add_time;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string mail_title
		{
			set{ _mail_title=value;}
			get{return _mail_title;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string mail_content
		{
			set{ _mail_content=value;}
			get{return _mail_content;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string templateid
		{
			set{ _templateid=value;}
			get{return _templateid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string mail_to
		{
			set{ _mail_to=value;}
			get{return _mail_to;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime send_date
		{
			set{ _send_date=value;}
			get{return _send_date;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? complate_date
		{
			set{ _complate_date=value;}
			get{return _complate_date;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string mail_ext
		{
			set{ _mail_ext=value;}
			get{return _mail_ext;}
		}
		#endregion Model

	}
}

