﻿/**  版本信息模板在安装目录下，可自行修改。
* yjr_user_uservsopenid.cs
*
* 功 能： N/A
* 类 名： yjr_user_uservsopenid
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2020/9/17 10:21:38   N/A    初版
*
* Copyright (c) 2019  Corporation. All rights reserved.
*/
using System;
namespace DTcms.Model
{
	/// <summary>
	/// yjr_user_uservsopenid:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class yjr_user_uservsopenid
	{
		public yjr_user_uservsopenid()
		{}
		#region Model
		private int _userid;
		private string _openid;
		private DateTime _add_time;
		/// <summary>
		/// 
		/// </summary>
		public int userid
		{
			set{ _userid=value;}
			get{return _userid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string openid
		{
			set{ _openid=value;}
			get{return _openid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime add_time
		{
			set{ _add_time=value;}
			get{return _add_time;}
		}
		#endregion Model

	}
}

