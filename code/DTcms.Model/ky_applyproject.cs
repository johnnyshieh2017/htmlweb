﻿/**  版本信息模板在安装目录下，可自行修改。
* ky_applyproject.cs
*
* 功 能： N/A
* 类 名： ky_applyproject
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2021/8/3 18:01:39   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace DTcms.Model
{
	/// <summary>
	/// ky_applyproject:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class ky_applyproject
	{
		public ky_applyproject()
		{}
		#region Model
		private int _pa_id;
		private string _pa_title;
		private string _pa_code;
		private string _pa_type;
		private string _pa_dptid;
		private string _pa_dptname;
		private string _pa_user;
		private string _dian_zi_you_xiang;
		private string _shou_ji_hao_ma;
		private string _shen_qing_ri_qi;
		private string _add_user;
		private DateTime? _add_time;
		private int? _status=0;
		private string _beizhu;
		private string _dan_wei_ji_ben_xin_xi;
		private string _xiang_mu_ji_ben_xin_xi;
		private string _xiang_mu_tuan_dui;
		private string _xiang_mu_gai_kuang;
		private string _xiang_mu_shi_shi;
		private string _xiang_mu_tou_zi;
		private string _xiang_mu_ji_xiao;
		private string _base_didian;
		private string _base_yanjiu_xingshi;
		private string _base_qizhishijian;
		private string _base_hezuo_danwei;
		private string _base_hezuo_dizhi;
		private string _base_hezuo_lianxiren;
		private string _base_hezuo_lianxidianhua;
		private string _base_xmgk_zhaiyao;
		private string _base_xmgk_neirongmubiao;
		private string _base_xmgk_jishuluxian;
		private string _base_xmgk_chuangxindian;
		private string _base_xmgk_jichutiaojian;
		private string _base_xmgk_xiaoyi;
		private string _base_xmgk_zhibiao;
		private string _team_owner_name;
		private string _team_owner_sex;
		private string _team_owner_chusheng;
		private string _team_owner_xueli;
		private string _team_owner_zhiwu;
		private string _team_owner_zhicheng;
		private string _team_owner_shouji;
		private string _team_owner_zhuanye;
		private string _team_owner_jianjie;
		private string _jindu_jihua;
		private string _keyan_baogao;
		/// <summary>
		/// 
		/// </summary>
		public int pa_id
		{
			set{ _pa_id=value;}
			get{return _pa_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string pa_title
		{
			set{ _pa_title=value;}
			get{return _pa_title;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string pa_code
		{
			set{ _pa_code=value;}
			get{return _pa_code;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string pa_type
		{
			set{ _pa_type=value;}
			get{return _pa_type;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string pa_dptid
		{
			set{ _pa_dptid=value;}
			get{return _pa_dptid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string pa_dptname
		{
			set{ _pa_dptname=value;}
			get{return _pa_dptname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string pa_user
		{
			set{ _pa_user=value;}
			get{return _pa_user;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string dian_zi_you_xiang
		{
			set{ _dian_zi_you_xiang=value;}
			get{return _dian_zi_you_xiang;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string shou_ji_hao_ma
		{
			set{ _shou_ji_hao_ma=value;}
			get{return _shou_ji_hao_ma;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string shen_qing_ri_qi
		{
			set{ _shen_qing_ri_qi=value;}
			get{return _shen_qing_ri_qi;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string add_user
		{
			set{ _add_user=value;}
			get{return _add_user;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? add_time
		{
			set{ _add_time=value;}
			get{return _add_time;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? status
		{
			set{ _status=value;}
			get{return _status;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string beizhu
		{
			set{ _beizhu=value;}
			get{return _beizhu;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string dan_wei_ji_ben_xin_xi
		{
			set{ _dan_wei_ji_ben_xin_xi=value;}
			get{return _dan_wei_ji_ben_xin_xi;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string xiang_mu_ji_ben_xin_xi
		{
			set{ _xiang_mu_ji_ben_xin_xi=value;}
			get{return _xiang_mu_ji_ben_xin_xi;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string xiang_mu_tuan_dui
		{
			set{ _xiang_mu_tuan_dui=value;}
			get{return _xiang_mu_tuan_dui;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string xiang_mu_gai_kuang
		{
			set{ _xiang_mu_gai_kuang=value;}
			get{return _xiang_mu_gai_kuang;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string xiang_mu_shi_shi
		{
			set{ _xiang_mu_shi_shi=value;}
			get{return _xiang_mu_shi_shi;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string xiang_mu_tou_zi
		{
			set{ _xiang_mu_tou_zi=value;}
			get{return _xiang_mu_tou_zi;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string xiang_mu_ji_xiao
		{
			set{ _xiang_mu_ji_xiao=value;}
			get{return _xiang_mu_ji_xiao;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string base_didian
		{
			set{ _base_didian=value;}
			get{return _base_didian;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string base_yanjiu_xingshi
		{
			set{ _base_yanjiu_xingshi=value;}
			get{return _base_yanjiu_xingshi;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string base_qizhishijian
		{
			set{ _base_qizhishijian=value;}
			get{return _base_qizhishijian;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string base_hezuo_danwei
		{
			set{ _base_hezuo_danwei=value;}
			get{return _base_hezuo_danwei;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string base_hezuo_dizhi
		{
			set{ _base_hezuo_dizhi=value;}
			get{return _base_hezuo_dizhi;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string base_hezuo_lianxiren
		{
			set{ _base_hezuo_lianxiren=value;}
			get{return _base_hezuo_lianxiren;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string base_hezuo_lianxidianhua
		{
			set{ _base_hezuo_lianxidianhua=value;}
			get{return _base_hezuo_lianxidianhua;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string base_xmgk_zhaiyao
		{
			set{ _base_xmgk_zhaiyao=value;}
			get{return _base_xmgk_zhaiyao;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string base_xmgk_neirongmubiao
		{
			set{ _base_xmgk_neirongmubiao=value;}
			get{return _base_xmgk_neirongmubiao;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string base_xmgk_jishuluxian
		{
			set{ _base_xmgk_jishuluxian=value;}
			get{return _base_xmgk_jishuluxian;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string base_xmgk_chuangxindian
		{
			set{ _base_xmgk_chuangxindian=value;}
			get{return _base_xmgk_chuangxindian;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string base_xmgk_jichutiaojian
		{
			set{ _base_xmgk_jichutiaojian=value;}
			get{return _base_xmgk_jichutiaojian;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string base_xmgk_xiaoyi
		{
			set{ _base_xmgk_xiaoyi=value;}
			get{return _base_xmgk_xiaoyi;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string base_xmgk_zhibiao
		{
			set{ _base_xmgk_zhibiao=value;}
			get{return _base_xmgk_zhibiao;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string team_owner_name
		{
			set{ _team_owner_name=value;}
			get{return _team_owner_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string team_owner_sex
		{
			set{ _team_owner_sex=value;}
			get{return _team_owner_sex;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string team_owner_chusheng
		{
			set{ _team_owner_chusheng=value;}
			get{return _team_owner_chusheng;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string team_owner_xueli
		{
			set{ _team_owner_xueli=value;}
			get{return _team_owner_xueli;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string team_owner_zhiwu
		{
			set{ _team_owner_zhiwu=value;}
			get{return _team_owner_zhiwu;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string team_owner_zhicheng
		{
			set{ _team_owner_zhicheng=value;}
			get{return _team_owner_zhicheng;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string team_owner_shouji
		{
			set{ _team_owner_shouji=value;}
			get{return _team_owner_shouji;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string team_owner_zhuanye
		{
			set{ _team_owner_zhuanye=value;}
			get{return _team_owner_zhuanye;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string team_owner_jianjie
		{
			set{ _team_owner_jianjie=value;}
			get{return _team_owner_jianjie;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string jindu_jihua
		{
			set{ _jindu_jihua=value;}
			get{return _jindu_jihua;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string keyan_baogao
		{
			set{ _keyan_baogao=value;}
			get{return _keyan_baogao;}
		}
		#endregion Model

	}
}

