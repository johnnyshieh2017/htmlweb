﻿using System;
namespace DTcms.Model
{
    /// <summary>
    /// vote_item_log:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class vote_item_log
    {
        public vote_item_log()
        { }
        #region Model
        private int _id;
        private int _vote_item_id = 0;
        private string _ip = "";
        private int _user_id = 0;
        private DateTime _add_time = DateTime.Now;
        private string _openid;
        private int _vote_subject_id;
        private string _vote_item_value;
        private int? _vote_id;
        /// <summary>
        /// 
        /// </summary>
        public int id
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int vote_item_id
        {
            set { _vote_item_id = value; }
            get { return _vote_item_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ip
        {
            set { _ip = value; }
            get { return _ip; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int user_id
        {
            set { _user_id = value; }
            get { return _user_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime add_time
        {
            set { _add_time = value; }
            get { return _add_time; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string openid
        {
            set { _openid = value; }
            get { return _openid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int vote_subject_id
        {
            set { _vote_subject_id = value; }
            get { return _vote_subject_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string vote_item_value
        {
            set { _vote_item_value = value; }
            get { return _vote_item_value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? vote_id
        {
            set { _vote_id = value; }
            get { return _vote_id; }
        }
        #endregion Model

    }
}


