﻿/**  版本信息模板在安装目录下，可自行修改。
* yjr_doctor_position.cs
*
* 功 能： N/A
* 类 名： yjr_doctor_position
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2020/9/4 16:44:36   N/A    初版
*
* Copyright (c) 2019  Corporation. All rights reserved.
*/
using System;
namespace DTcms.Model
{
	/// <summary>
	/// yjr_doctor_position:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class yjr_doctor_position
	{
		public yjr_doctor_position()
		{}
		#region Model
		private int _id;
		private string _position_name;
		/// <summary>
		/// 
		/// </summary>
		public int id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string position_name
		{
			set{ _position_name=value;}
			get{return _position_name;}
		}
		#endregion Model

	}
}

