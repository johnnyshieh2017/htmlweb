﻿/**  版本信息模板在安装目录下，可自行修改。
* ky_projects.cs
*
* 功 能： N/A
* 类 名： ky_projects
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2021/8/4 14:45:12   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace DTcms.Model
{
	/// <summary>
	/// ky_projects:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class ky_projects
	{
		public ky_projects()
		{}
		#region Model
		private int _p_id;
		private string _project_number;
		private string _project_title;
		private int _dptid;
		private string _owner;
		private string _member;
		private string _tel;
		private string _start_time;
		private string _end_time;
		private int? _project_type;
		private DateTime _add_time;
		private int _status=0;
		private string _fee_xiada;
		private string _fee_peitao;
		private string _fee_zongji;
		private string _fee_jieyu;
		private string _remark;
		private string _pa_id;
		/// <summary>
		/// 
		/// </summary>
		public int p_id
		{
			set{ _p_id=value;}
			get{return _p_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string project_number
		{
			set{ _project_number=value;}
			get{return _project_number;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string project_title
		{
			set{ _project_title=value;}
			get{return _project_title;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int dptid
		{
			set{ _dptid=value;}
			get{return _dptid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string owner
		{
			set{ _owner=value;}
			get{return _owner;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string member
		{
			set{ _member=value;}
			get{return _member;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string tel
		{
			set{ _tel=value;}
			get{return _tel;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string start_time
		{
			set{ _start_time=value;}
			get{return _start_time;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string end_time
		{
			set{ _end_time=value;}
			get{return _end_time;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? project_type
		{
			set{ _project_type=value;}
			get{return _project_type;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime add_time
		{
			set{ _add_time=value;}
			get{return _add_time;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int status
		{
			set{ _status=value;}
			get{return _status;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string fee_xiada
		{
			set{ _fee_xiada=value;}
			get{return _fee_xiada;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string fee_peitao
		{
			set{ _fee_peitao=value;}
			get{return _fee_peitao;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string fee_zongji
		{
			set{ _fee_zongji=value;}
			get{return _fee_zongji;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string fee_jieyu
		{
			set{ _fee_jieyu=value;}
			get{return _fee_jieyu;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string remark
		{
			set{ _remark=value;}
			get{return _remark;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string pa_id
		{
			set{ _pa_id=value;}
			get{return _pa_id;}
		}
		#endregion Model

	}
}

