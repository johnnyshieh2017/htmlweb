﻿/**  版本信息模板在安装目录下，可自行修改。
* vip_sms.cs
*
* 功 能： N/A
* 类 名： vip_sms
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2020/12/28 13:53:54   N/A    初版
*
* Copyright (c) 2019  Corporation. All rights reserved.
*/
using System;
namespace DTcms.Model
{
	/// <summary>
	/// vip_sms:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class vip_sms
	{
		public vip_sms()
		{}
		#region Model
		private int _id;
		private int _msg_type;
		private int _msg_status;
		private DateTime? _add_time;
		private string _msg_content;
		private string _templateid;
		private string _msg_mobile;
		private DateTime _send_date;
		private DateTime? _complate_date;
		private string _msg_ext;
		/// <summary>
		/// 
		/// </summary>
		public int id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int msg_type
		{
			set{ _msg_type=value;}
			get{return _msg_type;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int msg_status
		{
			set{ _msg_status=value;}
			get{return _msg_status;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? add_time
		{
			set{ _add_time=value;}
			get{return _add_time;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string msg_content
		{
			set{ _msg_content=value;}
			get{return _msg_content;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string templateid
		{
			set{ _templateid=value;}
			get{return _templateid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string msg_mobile
		{
			set{ _msg_mobile=value;}
			get{return _msg_mobile;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime send_date
		{
			set{ _send_date=value;}
			get{return _send_date;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? complate_date
		{
			set{ _complate_date=value;}
			get{return _complate_date;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string msg_ext
		{
			set{ _msg_ext=value;}
			get{return _msg_ext;}
		}
		#endregion Model

	}
}

