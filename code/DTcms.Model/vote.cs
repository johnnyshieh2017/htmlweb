﻿using System;
namespace DTcms.Model
{
	/// <summary>
	/// vote:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class vote
	{
		public vote()
		{}
		#region Model
		private int _id;
		private string _title="";
		private string _content="";
		private int _type=1;
		private DateTime _start_time= DateTime.Now;
		private DateTime _end_time= DateTime.Now;
		private int _sort_id=99;
		private int _user_id=0;
		private int _is_user=0;
		private string _img_url="";
		private int _click=0;
		private DateTime _add_time= DateTime.Now;
        private int? _status;
        /// <summary>
        /// 
        /// </summary>
        public int id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 调查标题
		/// </summary>
		public string title
		{
			set{ _title=value;}
			get{return _title;}
		}
		/// <summary>
		/// 调查内容
		/// </summary>
		public string content
		{
			set{ _content=value;}
			get{return _content;}
		}
		/// <summary>
		/// 调用显示
		/// </summary>
		public int type
		{
			set{ _type=value;}
			get{return _type;}
		}
		/// <summary>
		/// 开始时间
		/// </summary>
		public DateTime start_time
		{
			set{ _start_time=value;}
			get{return _start_time;}
		}
		/// <summary>
		/// 结束时间
		/// </summary>
		public DateTime end_time
		{
			set{ _end_time=value;}
			get{return _end_time;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int sort_id
		{
			set{ _sort_id=value;}
			get{return _sort_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int user_id
		{
			set{ _user_id=value;}
			get{return _user_id;}
		}
		/// <summary>
		/// 是否需要会员才能调查
		/// </summary>
		public int is_user
		{
			set{ _is_user=value;}
			get{return _is_user;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string img_url
		{
			set{ _img_url=value;}
			get{return _img_url;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int click
		{
			set{ _click=value;}
			get{return _click;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime add_time
		{
			set{ _add_time=value;}
			get{return _add_time;}
		}

        /// <summary>
		/// 
		/// </summary>
		public int? status
        {
            set { _status = value; }
            get { return _status; }
        }
        #endregion Model

    }
}

