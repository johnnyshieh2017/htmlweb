﻿/**  版本信息模板在安装目录下，可自行修改。
* ky_products_SCI.cs
*
* 功 能： N/A
* 类 名： ky_products_SCI
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2021/7/14 16:21:21   N/A    初版
*

*/
using System;
namespace DTcms.Model
{
	/// <summary>
	/// ky_products_SCI:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class ky_products_SCI
	{
		public ky_products_SCI()
		{}
		#region Model
		private int _pd_id;
		private DateTime _add_time;
		private string _add_user;
		private int _p_id;
		private string _lun_wen_ming_cheng;
		private string _kan_wu_ming_cheng;
		private string _zi_lan_mu;
		private string _qi_kan_ji_bie;
		private int _dptid;
		private string _zuo_zhe;
		private string _zuo_zhe_lei_xing;
		private string _shi_fou_ben_yuan;
		private DateTime? _chu_ban_ri_qi;
		private string _juan;
		private string _qi;
		private string _ye;
		private string _beizhu;
		private string _sci;
		/// <summary>
		/// 
		/// </summary>
		public int pd_id
		{
			set{ _pd_id=value;}
			get{return _pd_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime add_time
		{
			set{ _add_time=value;}
			get{return _add_time;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string add_user
		{
			set{ _add_user=value;}
			get{return _add_user;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int p_id
		{
			set{ _p_id=value;}
			get{return _p_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string lun_wen_ming_cheng
		{
			set{ _lun_wen_ming_cheng=value;}
			get{return _lun_wen_ming_cheng;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string kan_wu_ming_cheng
		{
			set{ _kan_wu_ming_cheng=value;}
			get{return _kan_wu_ming_cheng;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zi_lan_mu
		{
			set{ _zi_lan_mu=value;}
			get{return _zi_lan_mu;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string qi_kan_ji_bie
		{
			set{ _qi_kan_ji_bie=value;}
			get{return _qi_kan_ji_bie;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int dptid
		{
			set{ _dptid=value;}
			get{return _dptid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zuo_zhe
		{
			set{ _zuo_zhe=value;}
			get{return _zuo_zhe;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zuo_zhe_lei_xing
		{
			set{ _zuo_zhe_lei_xing=value;}
			get{return _zuo_zhe_lei_xing;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string shi_fou_ben_yuan
		{
			set{ _shi_fou_ben_yuan=value;}
			get{return _shi_fou_ben_yuan;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? chu_ban_ri_qi
		{
			set{ _chu_ban_ri_qi=value;}
			get{return _chu_ban_ri_qi;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string juan
		{
			set{ _juan=value;}
			get{return _juan;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string qi
		{
			set{ _qi=value;}
			get{return _qi;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ye
		{
			set{ _ye=value;}
			get{return _ye;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string beizhu
		{
			set{ _beizhu=value;}
			get{return _beizhu;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string SCI
		{
			set{ _sci=value;}
			get{return _sci;}
		}
		#endregion Model

	}
}

