﻿using System;
namespace DTcms.Model
{
	/// <summary>
	/// vote_subject:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class vote_subject
	{
		public vote_subject()
		{}
		#region Model
		private int _id;
		private int _vote_id=0;
		private string _title="";
		private int _type=1;
		private int _is_img=0;
		private int _sort_id=99;
		private int _is_user=0;
		private DateTime _add_time= DateTime.Now;
		/// <summary>
		/// 
		/// </summary>
		public int id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int vote_id
		{
			set{ _vote_id=value;}
			get{return _vote_id;}
		}
		/// <summary>
		/// 投票标题
		/// </summary>
		public string title
		{
			set{ _title=value;}
			get{return _title;}
		}
		/// <summary>
		/// 多选、单选
		/// </summary>
		public int type
		{
			set{ _type=value;}
			get{return _type;}
		}
		/// <summary>
		/// 人数
		/// </summary>
		public int is_img
		{
			set{ _is_img=value;}
			get{return _is_img;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int sort_id
		{
			set{ _sort_id=value;}
			get{return _sort_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int is_user
		{
			set{ _is_user=value;}
			get{return _is_user;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime add_time
		{
			set{ _add_time=value;}
			get{return _add_time;}
		}
		#endregion Model

	}
}

