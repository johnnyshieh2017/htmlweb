﻿using System; 
using System.Text;
using System.Collections.Generic; 
using System.Data;
namespace DTcms.Model{
	 	//View_docvsbusiness
		public class View_docvsbusiness
	{
   		     
      	/// <summary>
		/// id
        /// </summary>		
		private int _id;
        public int id
        {
            get{ return _id; }
            set{ _id = value; }
        }        
		/// <summary>
		/// doctorid
        /// </summary>		
		private int _doctorid;
        public int doctorid
        {
            get{ return _doctorid; }
            set{ _doctorid = value; }
        }        
		/// <summary>
		/// businessid
        /// </summary>		
		private int _businessid;
        public int businessid
        {
            get{ return _businessid; }
            set{ _businessid = value; }
        }        
		/// <summary>
		/// add_time
        /// </summary>		
		private DateTime _add_time;
        public DateTime add_time
        {
            get{ return _add_time; }
            set{ _add_time = value; }
        }        
		/// <summary>
		/// cost_type
        /// </summary>		
		private int _cost_type;
        public int cost_type
        {
            get{ return _cost_type; }
            set{ _cost_type = value; }
        }        
		/// <summary>
		/// cost_interval
        /// </summary>		
		private int _cost_interval;
        public int cost_interval
        {
            get{ return _cost_interval; }
            set{ _cost_interval = value; }
        }        
		/// <summary>
		/// price
        /// </summary>		
		private int _price;
        public int price
        {
            get{ return _price; }
            set{ _price = value; }
        }        
		/// <summary>
		/// business_name
        /// </summary>		
		private string _business_name;
        public string business_name
        {
            get{ return _business_name; }
            set{ _business_name = value; }
        }        
		/// <summary>
		/// hospitalname
        /// </summary>		
		private string _hospitalname;
        public string hospitalname
        {
            get{ return _hospitalname; }
            set{ _hospitalname = value; }
        }        
		/// <summary>
		/// name
        /// </summary>		
		private string _name;
        public string name
        {
            get{ return _name; }
            set{ _name = value; }
        }        
		/// <summary>
		/// mobile
        /// </summary>		
		private string _mobile;
        public string mobile
        {
            get{ return _mobile; }
            set{ _mobile = value; }
        }        
		/// <summary>
		/// idcard
        /// </summary>		
		private string _idcard;
        public string idcard
        {
            get{ return _idcard; }
            set{ _idcard = value; }
        }        
		/// <summary>
		/// status
        /// </summary>		
		private int _status;
        public int status
        {
            get{ return _status; }
            set{ _status = value; }
        }        
		/// <summary>
		/// business_intro
        /// </summary>		
		private string _business_intro;
        public string business_intro
        {
            get{ return _business_intro; }
            set{ _business_intro = value; }
        }        
		   
	}
}

