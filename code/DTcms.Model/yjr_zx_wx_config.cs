﻿/**  版本信息模板在安装目录下，可自行修改。
* yjr_zx_wx_config.cs
*
* 功 能： N/A
* 类 名： yjr_zx_wx_config
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2020/9/15 17:27:32   N/A    初版
*
* Copyright (c) 2019  Corporation. All rights reserved.
*/
using System;
namespace DTcms.Model
{
	/// <summary>
	/// yjr_zx_wx_config:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class yjr_zx_wx_config
	{
		public yjr_zx_wx_config()
		{}
		#region Model
		private string _appid;
		private string _secret;
		private string _token;
		private string _access_token;
		private DateTime _updatetime;
		/// <summary>
		/// 
		/// </summary>
		public string appid
		{
			set{ _appid=value;}
			get{return _appid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string secret
		{
			set{ _secret=value;}
			get{return _secret;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string token
		{
			set{ _token=value;}
			get{return _token;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string access_token
		{
			set{ _access_token=value;}
			get{return _access_token;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime updatetime
		{
			set{ _updatetime=value;}
			get{return _updatetime;}
		}
		#endregion Model

	}
}

