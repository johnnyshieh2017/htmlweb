﻿using System; 
using System.Text;
using System.Collections.Generic; 
using System.Data;
namespace DTcms.Model{
	 	//yjr_doctor_vsbusinessconfig
		public class yjr_doctor_vsbusinessconfig
	{
   		     
      	/// <summary>
		/// id
        /// </summary>		
		private int _id;
        public int id
        {
            get{ return _id; }
            set{ _id = value; }
        }        
		/// <summary>
		/// doctorid
        /// </summary>		
		private int _doctorid;
        public int doctorid
        {
            get{ return _doctorid; }
            set{ _doctorid = value; }
        }        
		/// <summary>
		/// businessid
        /// </summary>		
		private int _businessid;
        public int businessid
        {
            get{ return _businessid; }
            set{ _businessid = value; }
        }        
		/// <summary>
		/// add_time
        /// </summary>		
		private DateTime _add_time;
        public DateTime add_time
        {
            get{ return _add_time; }
            set{ _add_time = value; }
        }        
		/// <summary>
		/// cost_type
        /// </summary>		
		private int _cost_type;
        public int cost_type
        {
            get{ return _cost_type; }
            set{ _cost_type = value; }
        }        
		/// <summary>
		/// cost_interval
        /// </summary>		
		private int _cost_interval;
        public int cost_interval
        {
            get{ return _cost_interval; }
            set{ _cost_interval = value; }
        }        
		/// <summary>
		/// price
        /// </summary>		
		private int _price;
        public int price
        {
            get{ return _price; }
            set{ _price = value; }
        }        
		   
	}
}

