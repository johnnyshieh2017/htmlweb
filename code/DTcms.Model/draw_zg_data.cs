﻿/**  版本信息模板在安装目录下，可自行修改。
* draw_zg_data.cs
*
* 功 能： N/A
* 类 名： draw_zg_data
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2020/4/1 15:19:16   N/A    初版
*
* Copyright (c) 2019  Corporation. All rights reserved.
*/
using System;
namespace DTcms.Model
{
	/// <summary>
	/// draw_zg_data:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class draw_zg_data
	{
		public draw_zg_data()
		{}
		#region Model
		private string _sheng;
		private string _shi;
		private string _gongsi;
		private string _xingming;
		private string _zhiwu;
		private string _wxnicheng;
		private string _shouji;
		private string _beizhu;
		/// <summary>
		/// 
		/// </summary>
		public string sheng
		{
			set{ _sheng=value;}
			get{return _sheng;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string shi
		{
			set{ _shi=value;}
			get{return _shi;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string gongsi
		{
			set{ _gongsi=value;}
			get{return _gongsi;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string xingming
		{
			set{ _xingming=value;}
			get{return _xingming;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhiwu
		{
			set{ _zhiwu=value;}
			get{return _zhiwu;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string wxnicheng
		{
			set{ _wxnicheng=value;}
			get{return _wxnicheng;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string shouji
		{
			set{ _shouji=value;}
			get{return _shouji;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string beizhu
		{
			set{ _beizhu=value;}
			get{return _beizhu;}
		}
		#endregion Model

	}
}

