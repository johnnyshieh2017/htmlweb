﻿using System; 
using System.Text;
using System.Collections.Generic; 
using System.Data;
namespace DTcms.Model{
	 	//yjr_doctor_vsbusinessappointment
		public class yjr_doctor_vsbusinessappointment
	{
   		     
      	/// <summary>
		/// id
        /// </summary>		
		private int _id;
        public int id
        {
            get{ return _id; }
            set{ _id = value; }
        }        
		/// <summary>
		/// doctorid
        /// </summary>		
		private int _doctorid;
        public int doctorid
        {
            get{ return _doctorid; }
            set{ _doctorid = value; }
        }        
		/// <summary>
		/// busines_sconfig_id
        /// </summary>		
		private int _busines_sconfig_id;
        public int busines_sconfig_id
        {
            get{ return _busines_sconfig_id; }
            set{ _busines_sconfig_id = value; }
        }        
		/// <summary>
		/// clinic_date
        /// </summary>		
		private string _clinic_date;
        public string clinic_date
        {
            get{ return _clinic_date; }
            set{ _clinic_date = value; }
        }        
		/// <summary>
		/// clinic_time
        /// </summary>		
		private string _clinic_time;
        public string clinic_time
        {
            get{ return _clinic_time; }
            set{ _clinic_time = value; }
        }        
		/// <summary>
		/// add_time
        /// </summary>		
		private DateTime _add_time;
        public DateTime add_time
        {
            get{ return _add_time; }
            set{ _add_time = value; }
        }        
		   
	}
}

