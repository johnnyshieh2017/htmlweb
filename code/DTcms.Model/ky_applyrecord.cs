﻿/**  版本信息模板在安装目录下，可自行修改。
* ky_applyrecord.cs
*
* 功 能： N/A
* 类 名： ky_applyrecord
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2021/7/5 13:57:53   N/A    初版

*/
using System;
namespace DTcms.Model
{
	/// <summary>
	/// ky_applyrecord:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class ky_applyrecord
	{
		public ky_applyrecord()
		{}
		#region Model
		private int _id;
		private int _p_id;
		private string _add_user;
		private DateTime _add_time;
		private string _remark;
		private string _audit_user;
		private string _audit_status;
		private DateTime? _audit_time;
		private string _audit_type;
		private string _audit_remark;
		/// <summary>
		/// 
		/// </summary>
		public int id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int p_id
		{
			set{ _p_id=value;}
			get{return _p_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string add_user
		{
			set{ _add_user=value;}
			get{return _add_user;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime add_time
		{
			set{ _add_time=value;}
			get{return _add_time;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string remark
		{
			set{ _remark=value;}
			get{return _remark;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string audit_user
		{
			set{ _audit_user=value;}
			get{return _audit_user;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string audit_status
		{
			set{ _audit_status=value;}
			get{return _audit_status;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? audit_time
		{
			set{ _audit_time=value;}
			get{return _audit_time;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string audit_type
		{
			set{ _audit_type=value;}
			get{return _audit_type;}
		}

		/// <summary>
		/// 
		/// </summary>
		public string audit_remark
		{
			set { _audit_remark = value; }
			get { return _audit_remark; }
		}
		#endregion Model

	}
}

