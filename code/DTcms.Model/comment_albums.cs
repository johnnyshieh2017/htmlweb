﻿/**  版本信息模板在安装目录下，可自行修改。
* comment_albums.cs
*
* 功 能： N/A
* 类 名： comment_albums
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2020/2/21 10:51:35   N/A    初版
*
* Copyright (c) 2019  Corporation. All rights reserved.
*/
using System;
namespace DTcms.Model
{
	/// <summary>
	/// comment_albums:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class comment_albums
	{
		public comment_albums()
		{}
		#region Model
		private int _id;
		private int? _channel_id;
		private int? _article_comment_id;
		private string _original_path;
		private string _remark;
		private DateTime? _add_time;
		/// <summary>
		/// 
		/// </summary>
		public int id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? channel_id
		{
			set{ _channel_id=value;}
			get{return _channel_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? article_comment_id
		{
			set{ _article_comment_id=value;}
			get{return _article_comment_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string original_path
		{
			set{ _original_path=value;}
			get{return _original_path;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string remark
		{
			set{ _remark=value;}
			get{return _remark;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? add_time
		{
			set{ _add_time=value;}
			get{return _add_time;}
		}
		#endregion Model

	}
}

