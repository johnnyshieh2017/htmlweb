﻿/**  版本信息模板在安装目录下，可自行修改。
* yjr_doctor_vsuser.cs
*
* 功 能： N/A
* 类 名： yjr_doctor_vsuser
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2020/8/5 11:06:43   N/A    初版
*
* Copyright (c) 2019  Corporation. All rights reserved.
*/
using System;
namespace DTcms.Model
{
	/// <summary>
	/// yjr_doctor_vsuser:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class yjr_doctor_vsuser
	{
		public yjr_doctor_vsuser()
		{}
		#region Model
		private string _docid;
		private string _userid;
		private DateTime? _add_time;
		/// <summary>
		/// 
		/// </summary>
		public string docid
		{
			set{ _docid=value;}
			get{return _docid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string userid
		{
			set{ _userid=value;}
			get{return _userid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? add_time
		{
			set{ _add_time=value;}
			get{return _add_time;}
		}
		#endregion Model

	}
}

