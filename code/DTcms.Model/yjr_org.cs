﻿/**  版本信息模板在安装目录下，可自行修改。
* yjr_org.cs
*
* 功 能： N/A
* 类 名： yjr_org
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2020/8/5 11:06:43   N/A    初版
*
* Copyright (c) 2019  Corporation. All rights reserved.
*/
using System;
namespace DTcms.Model
{
	/// <summary>
	/// yjr_org:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class yjr_org
	{
		public yjr_org()
		{}
		#region Model
		private int _id;
		private string _orgname;
		private string _intro;
		private string _address;
		private string _telephone;
		private string _remark;
		/// <summary>
		/// 
		/// </summary>
		public int id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string orgname
		{
			set{ _orgname=value;}
			get{return _orgname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string intro
		{
			set{ _intro=value;}
			get{return _intro;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string address
		{
			set{ _address=value;}
			get{return _address;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string telephone
		{
			set{ _telephone=value;}
			get{return _telephone;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string remark
		{
			set{ _remark=value;}
			get{return _remark;}
		}
		#endregion Model

	}
}

