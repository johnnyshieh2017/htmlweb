﻿/**  版本信息模板在安装目录下，可自行修改。
* draw_pond.cs
*
* 功 能： N/A
* 类 名： draw_pond
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2020/4/10 14:30:21   N/A    初版
*
* Copyright (c) 2019  Corporation. All rights reserved.
*/
using System;
namespace DTcms.Model
{
	/// <summary>
	/// draw_pond:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class draw_pond
	{
		public draw_pond()
		{}
		#region Model
		private int _id;
		private string _idnumber;
		private string _telephone;
		private string _truename;
		private int _lotteryid;
		private int _prizeid=0;
		private DateTime _createdate;
		private DateTime? _updatetime;
		private int _status;
		private string _hospital;
		private string _department;
		private string _linkmobile;
		private string _address;
		/// <summary>
		/// 
		/// </summary>
		public int id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Idnumber
		{
			set{ _idnumber=value;}
			get{return _idnumber;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string telephone
		{
			set{ _telephone=value;}
			get{return _telephone;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string truename
		{
			set{ _truename=value;}
			get{return _truename;}
		}
		/// <summary>
		/// 活动id
		/// </summary>
		public int lotteryid
		{
			set{ _lotteryid=value;}
			get{return _lotteryid;}
		}
		/// <summary>
		/// 奖项id
		/// </summary>
		public int Prizeid
		{
			set{ _prizeid=value;}
			get{return _prizeid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime createdate
		{
			set{ _createdate=value;}
			get{return _createdate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? updatetime
		{
			set{ _updatetime=value;}
			get{return _updatetime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int status
		{
			set{ _status=value;}
			get{return _status;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string hospital
		{
			set{ _hospital=value;}
			get{return _hospital;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string department
		{
			set{ _department=value;}
			get{return _department;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string linkmobile
		{
			set{ _linkmobile=value;}
			get{return _linkmobile;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string address
		{
			set{ _address=value;}
			get{return _address;}
		}
		#endregion Model

	}
}

