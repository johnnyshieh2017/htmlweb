﻿/**  版本信息模板在安装目录下，可自行修改。
* ky_applyproject_jixiao.cs
*
* 功 能： N/A
* 类 名： ky_applyproject_jixiao
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2021/8/3 17:06:58   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace DTcms.Model
{
	/// <summary>
	/// ky_applyproject_jixiao:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class ky_applyproject_jixiao
	{
		public ky_applyproject_jixiao()
		{}
		#region Model
		private int _id;
		private string _pa_id;
		private string _zhuanli_shuliang;
		private string _zhuanli_shuliang_faming;
		private string _zhuanli_shuliang_shiyongxinxing;
		private string _zhuanli_shuliang_waiguan;
		private string _zhuanli_shouquan_shuliang;
		private string _zhuanli_shouquan_faming;
		private string _zhuanli_shouquan_shiyongxinxing;
		private string _zhuanli_shouquan_waiguan;
		private string _ruanzhu_shuliang;
		private string _lunwen_shuliang;
		private string _lunwen_shuliang_sci;
		private string _lunwen_shuliang_ei;
		private string _zhuzuo_shuliang;
		private string _biaozhun_shuliang;
		private string _biaozhun_shuliang_guoji;
		private string _biaozhun_shuliang_guojia;
		private string _biaozhun_shuliang_hangye;
		private string _biaozhun_shuliang_difang;
		private string _biaozhun_shuliang_qiye;
		private string _qita_tianbujishukongbai_shuliang;
		private string _qita_tianbujishukongbai_shuliang_guoji;
		private string _qita_tianbujishukongbai_shuliang_guojia;
		private string _qita_tianbujishukongbai_shuliang_shengji;
		private string _qita_huojiang_shuliang;
		private string _qita_huojiang_shuliang_guojia;
		private string _qita_huojiang_shuliang_sheng;
		private string _qita_huojiang_shuliang_difang;
		private string _qita_qita_xingongyi;
		private string _qita_qita_xinchanping;
		private string _qita_qita_xincailiao;
		private string _qita_qita_xinzhuangbei;
		private string _qita_qita_pingtai;
		private string _qita_qita_kejichengguo;
		private string _rencai_yj_shuliang;
		private string _rencai_yj_shuliang_boshi;
		private string _rencai_yj_shuliang_shuoshi;
		private string _jingjixiaoyi;
		private string _shehuixiaoyi;
		private string _qitashuoming;
		private DateTime _add_time;
		private string _add_user;
		private int _status;
		private string _rencai_py_shuliang;
		private string _rencai_py_shuliang_boshi;
		private string _rencai_py_shuliang_shuoshi;
		/// <summary>
		/// 
		/// </summary>
		public int id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string pa_id
		{
			set{ _pa_id=value;}
			get{return _pa_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhuanli_shuliang
		{
			set{ _zhuanli_shuliang=value;}
			get{return _zhuanli_shuliang;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhuanli_shuliang_faming
		{
			set{ _zhuanli_shuliang_faming=value;}
			get{return _zhuanli_shuliang_faming;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhuanli_shuliang_shiyongxinxing
		{
			set{ _zhuanli_shuliang_shiyongxinxing=value;}
			get{return _zhuanli_shuliang_shiyongxinxing;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhuanli_shuliang_waiguan
		{
			set{ _zhuanli_shuliang_waiguan=value;}
			get{return _zhuanli_shuliang_waiguan;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhuanli_shouquan_shuliang
		{
			set{ _zhuanli_shouquan_shuliang=value;}
			get{return _zhuanli_shouquan_shuliang;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhuanli_shouquan_faming
		{
			set{ _zhuanli_shouquan_faming=value;}
			get{return _zhuanli_shouquan_faming;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhuanli_shouquan_shiyongxinxing
		{
			set{ _zhuanli_shouquan_shiyongxinxing=value;}
			get{return _zhuanli_shouquan_shiyongxinxing;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhuanli_shouquan_waiguan
		{
			set{ _zhuanli_shouquan_waiguan=value;}
			get{return _zhuanli_shouquan_waiguan;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ruanzhu_shuliang
		{
			set{ _ruanzhu_shuliang=value;}
			get{return _ruanzhu_shuliang;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string lunwen_shuliang
		{
			set{ _lunwen_shuliang=value;}
			get{return _lunwen_shuliang;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string lunwen_shuliang_sci
		{
			set{ _lunwen_shuliang_sci=value;}
			get{return _lunwen_shuliang_sci;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string lunwen_shuliang_ei
		{
			set{ _lunwen_shuliang_ei=value;}
			get{return _lunwen_shuliang_ei;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhuzuo_shuliang
		{
			set{ _zhuzuo_shuliang=value;}
			get{return _zhuzuo_shuliang;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string biaozhun_shuliang
		{
			set{ _biaozhun_shuliang=value;}
			get{return _biaozhun_shuliang;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string biaozhun_shuliang_guoji
		{
			set{ _biaozhun_shuliang_guoji=value;}
			get{return _biaozhun_shuliang_guoji;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string biaozhun_shuliang_guojia
		{
			set{ _biaozhun_shuliang_guojia=value;}
			get{return _biaozhun_shuliang_guojia;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string biaozhun_shuliang_hangye
		{
			set{ _biaozhun_shuliang_hangye=value;}
			get{return _biaozhun_shuliang_hangye;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string biaozhun_shuliang_difang
		{
			set{ _biaozhun_shuliang_difang=value;}
			get{return _biaozhun_shuliang_difang;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string biaozhun_shuliang_qiye
		{
			set{ _biaozhun_shuliang_qiye=value;}
			get{return _biaozhun_shuliang_qiye;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string qita_tianbujishukongbai_shuliang
		{
			set{ _qita_tianbujishukongbai_shuliang=value;}
			get{return _qita_tianbujishukongbai_shuliang;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string qita_tianbujishukongbai_shuliang_guoji
		{
			set{ _qita_tianbujishukongbai_shuliang_guoji=value;}
			get{return _qita_tianbujishukongbai_shuliang_guoji;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string qita_tianbujishukongbai_shuliang_guojia
		{
			set{ _qita_tianbujishukongbai_shuliang_guojia=value;}
			get{return _qita_tianbujishukongbai_shuliang_guojia;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string qita_tianbujishukongbai_shuliang_shengji
		{
			set{ _qita_tianbujishukongbai_shuliang_shengji=value;}
			get{return _qita_tianbujishukongbai_shuliang_shengji;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string qita_huojiang_shuliang
		{
			set{ _qita_huojiang_shuliang=value;}
			get{return _qita_huojiang_shuliang;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string qita_huojiang_shuliang_guojia
		{
			set{ _qita_huojiang_shuliang_guojia=value;}
			get{return _qita_huojiang_shuliang_guojia;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string qita_huojiang_shuliang_sheng
		{
			set{ _qita_huojiang_shuliang_sheng=value;}
			get{return _qita_huojiang_shuliang_sheng;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string qita_huojiang_shuliang_difang
		{
			set{ _qita_huojiang_shuliang_difang=value;}
			get{return _qita_huojiang_shuliang_difang;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string qita_qita_xingongyi
		{
			set{ _qita_qita_xingongyi=value;}
			get{return _qita_qita_xingongyi;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string qita_qita_xinchanping
		{
			set{ _qita_qita_xinchanping=value;}
			get{return _qita_qita_xinchanping;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string qita_qita_xincailiao
		{
			set{ _qita_qita_xincailiao=value;}
			get{return _qita_qita_xincailiao;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string qita_qita_xinzhuangbei
		{
			set{ _qita_qita_xinzhuangbei=value;}
			get{return _qita_qita_xinzhuangbei;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string qita_qita_pingtai
		{
			set{ _qita_qita_pingtai=value;}
			get{return _qita_qita_pingtai;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string qita_qita_kejichengguo
		{
			set{ _qita_qita_kejichengguo=value;}
			get{return _qita_qita_kejichengguo;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string rencai_yj_shuliang
		{
			set{ _rencai_yj_shuliang=value;}
			get{return _rencai_yj_shuliang;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string rencai_yj_shuliang_boshi
		{
			set{ _rencai_yj_shuliang_boshi=value;}
			get{return _rencai_yj_shuliang_boshi;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string rencai_yj_shuliang_shuoshi
		{
			set{ _rencai_yj_shuliang_shuoshi=value;}
			get{return _rencai_yj_shuliang_shuoshi;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string jingjixiaoyi
		{
			set{ _jingjixiaoyi=value;}
			get{return _jingjixiaoyi;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string shehuixiaoyi
		{
			set{ _shehuixiaoyi=value;}
			get{return _shehuixiaoyi;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string qitashuoming
		{
			set{ _qitashuoming=value;}
			get{return _qitashuoming;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime add_time
		{
			set{ _add_time=value;}
			get{return _add_time;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string add_user
		{
			set{ _add_user=value;}
			get{return _add_user;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int status
		{
			set{ _status=value;}
			get{return _status;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string rencai_py_shuliang
		{
			set{ _rencai_py_shuliang=value;}
			get{return _rencai_py_shuliang;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string rencai_py_shuliang_boshi
		{
			set{ _rencai_py_shuliang_boshi=value;}
			get{return _rencai_py_shuliang_boshi;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string rencai_py_shuliang_shuoshi
		{
			set{ _rencai_py_shuliang_shuoshi=value;}
			get{return _rencai_py_shuliang_shuoshi;}
		}
		#endregion Model

	}
}

