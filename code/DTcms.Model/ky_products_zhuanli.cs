﻿/**  版本信息模板在安装目录下，可自行修改。
* ky_products_zhuanli.cs
*
* 功 能： N/A
* 类 名： ky_products_zhuanli
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2021/7/14 16:21:22   N/A    初版
*

*/
using System;
namespace DTcms.Model
{
	/// <summary>
	/// ky_products_zhuanli:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class ky_products_zhuanli
	{
		public ky_products_zhuanli()
		{}
		#region Model
		private int _pd_id;
		private DateTime _add_time;
		private string _add_user;
		private int _p_id;
		private string _zhuanli_mingcheng;
		private string _zhuanli_leibie;
		private int? _dptid;
		private string _fa_ming_ren;
		private string _zhuan_li_quan_ren;
		private string _shou_quan_gong_gao_ri;
		private string _zhuan_li_hao;
		private string _beizhu;
		/// <summary>
		/// 
		/// </summary>
		public int pd_id
		{
			set{ _pd_id=value;}
			get{return _pd_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime add_time
		{
			set{ _add_time=value;}
			get{return _add_time;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string add_user
		{
			set{ _add_user=value;}
			get{return _add_user;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int p_id
		{
			set{ _p_id=value;}
			get{return _p_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhuanli_mingcheng
		{
			set{ _zhuanli_mingcheng=value;}
			get{return _zhuanli_mingcheng;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhuanli_leibie
		{
			set{ _zhuanli_leibie=value;}
			get{return _zhuanli_leibie;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? dptid
		{
			set{ _dptid=value;}
			get{return _dptid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string fa_ming_ren
		{
			set{ _fa_ming_ren=value;}
			get{return _fa_ming_ren;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhuan_li_quan_ren
		{
			set{ _zhuan_li_quan_ren=value;}
			get{return _zhuan_li_quan_ren;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string shou_quan_gong_gao_ri
		{
			set{ _shou_quan_gong_gao_ri=value;}
			get{return _shou_quan_gong_gao_ri;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhuan_li_hao
		{
			set{ _zhuan_li_hao=value;}
			get{return _zhuan_li_hao;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string beizhu
		{
			set{ _beizhu=value;}
			get{return _beizhu;}
		}
		#endregion Model

	}
}

