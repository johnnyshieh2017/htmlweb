﻿using System;
namespace DTcms.Model
{
	/// <summary>
	/// vote_item:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class vote_item
	{
		public vote_item()
		{}
		#region Model
		private int _id;
		private int _vote_subject_id=0;
		private string _title="";
		private string _img_url="";
		private string _content="";
		private int _sort_id=99;
		private int _num=0;
		private DateTime _add_time= DateTime.Now;
		/// <summary>
		/// 
		/// </summary>
		public int id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 调查标题ID
		/// </summary>
		public int vote_subject_id
		{
			set{ _vote_subject_id=value;}
			get{return _vote_subject_id;}
		}
		/// <summary>
		/// 投票标题
		/// </summary>
		public string title
		{
			set{ _title=value;}
			get{return _title;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string img_url
		{
			set{ _img_url=value;}
			get{return _img_url;}
		}
		/// <summary>
		/// 选项内容
		/// </summary>
		public string content
		{
			set{ _content=value;}
			get{return _content;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int sort_id
		{
			set{ _sort_id=value;}
			get{return _sort_id;}
		}
		/// <summary>
		/// 票数
		/// </summary>
		public int num
		{
			set{ _num=value;}
			get{return _num;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime add_time
		{
			set{ _add_time=value;}
			get{return _add_time;}
		}
		#endregion Model

	}
}

