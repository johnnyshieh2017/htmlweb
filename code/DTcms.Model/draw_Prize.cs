﻿/**  版本信息模板在安装目录下，可自行修改。
* draw_Prize.cs
*
* 功 能： N/A
* 类 名： draw_Prize
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2020/4/1 15:19:15   N/A    初版
*
* Copyright (c) 2019  Corporation. All rights reserved.
*/
using System;
namespace DTcms.Model
{
	/// <summary>
	/// draw_Prize:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class draw_Prize
	{
		public draw_Prize()
		{}
		#region Model
		private int _id;
		private int _lotteryid;
		private string _prize_name;
		private string _prize_title;
		private int _sort;
		private string _imgurl;
		private int _prize_count=1;
		private string _remark;
		/// <summary>
		/// 
		/// </summary>
		public int id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 活动id
		/// </summary>
		public int lotteryid
		{
			set{ _lotteryid=value;}
			get{return _lotteryid;}
		}
		/// <summary>
		/// 奖项名称
		/// </summary>
		public string Prize_name
		{
			set{ _prize_name=value;}
			get{return _prize_name;}
		}
		/// <summary>
		/// 奖项标题
		/// </summary>
		public string Prize_title
		{
			set{ _prize_title=value;}
			get{return _prize_title;}
		}
		/// <summary>
		/// 排序字段
		/// </summary>
		public int sort
		{
			set{ _sort=value;}
			get{return _sort;}
		}
		/// <summary>
		/// 奖项图片
		/// </summary>
		public string imgurl
		{
			set{ _imgurl=value;}
			get{return _imgurl;}
		}
		/// <summary>
		/// 奖品数量
		/// </summary>
		public int Prize_count
		{
			set{ _prize_count=value;}
			get{return _prize_count;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string remark
		{
			set{ _remark=value;}
			get{return _remark;}
		}
		#endregion Model

	}
}

