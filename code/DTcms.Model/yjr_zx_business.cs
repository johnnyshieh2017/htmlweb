﻿using System; 
using System.Text;
using System.Collections.Generic; 
using System.Data;
namespace DTcms.Model{
	 	//yjr_zx_business
		public class yjr_zx_business
	{
   		     
      	/// <summary>
		/// id
        /// </summary>		
		private int _id;
        public int id
        {
            get{ return _id; }
            set{ _id = value; }
        }        
		/// <summary>
		/// business_name
        /// </summary>		
		private string _business_name;
        public string business_name
        {
            get{ return _business_name; }
            set{ _business_name = value; }
        }        
		/// <summary>
		/// business_intro
        /// </summary>		
		private string _business_intro;
        public string business_intro
        {
            get{ return _business_intro; }
            set{ _business_intro = value; }
        }        
		/// <summary>
		/// sort
        /// </summary>		
		private int _sort;
        public int sort
        {
            get{ return _sort; }
            set{ _sort = value; }
        }        
		/// <summary>
		/// status
        /// </summary>		
		private int _status;
        public int status
        {
            get{ return _status; }
            set{ _status = value; }
        }        
		   
	}
}

