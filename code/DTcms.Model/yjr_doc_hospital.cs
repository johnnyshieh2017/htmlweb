﻿using System; 
using System.Text;
using System.Collections.Generic; 
using System.Data;
namespace DTcms.Model{
	 	//yjr_doc_hospital
		public class yjr_doc_hospital
	{
   		     
      	/// <summary>
		/// id
        /// </summary>		
		private int _id;
        public int id
        {
            get{ return _id; }
            set{ _id = value; }
        }        
		/// <summary>
		/// hospitalname
        /// </summary>		
		private string _hospitalname;
        public string hospitalname
        {
            get{ return _hospitalname; }
            set{ _hospitalname = value; }
        }        
		/// <summary>
		/// hoslevel
        /// </summary>		
		private string _hoslevel;
        public string hoslevel
        {
            get{ return _hoslevel; }
            set{ _hoslevel = value; }
        }        
		/// <summary>
		/// address
        /// </summary>		
		private string _address;
        public string address
        {
            get{ return _address; }
            set{ _address = value; }
        }        
		/// <summary>
		/// tel
        /// </summary>		
		private string _tel;
        public string tel
        {
            get{ return _tel; }
            set{ _tel = value; }
        }        
		/// <summary>
		/// map
        /// </summary>		
		private string _map;
        public string map
        {
            get{ return _map; }
            set{ _map = value; }
        }        
		/// <summary>
		/// description
        /// </summary>		
		private string _description;
        public string description
        {
            get{ return _description; }
            set{ _description = value; }
        }        
		/// <summary>
		/// logo_url
        /// </summary>		
		private string _logo_url;
        public string logo_url
        {
            get{ return _logo_url; }
            set{ _logo_url = value; }
        }        
		   
	}
}

