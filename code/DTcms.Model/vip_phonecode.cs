﻿/**  版本信息模板在安装目录下，可自行修改。
* vip_phonecode.cs
*
* 功 能： N/A
* 类 名： vip_phonecode
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2020/12/24 15:34:58   N/A    初版
*
* Copyright (c) 2019  Corporation. All rights reserved.
*/
using System;
namespace DTcms.Model
{
	/// <summary>
	/// vip_phonecode:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class vip_phonecode
	{
		public vip_phonecode()
		{}
		#region Model
		private string _table_name;
		private string _table_pkvalue;
		private string _code;
		private int _id;
		private DateTime _add_time;
		/// <summary>
		/// 
		/// </summary>
		public string table_name
		{
			set{ _table_name=value;}
			get{return _table_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string table_pkvalue
		{
			set{ _table_pkvalue=value;}
			get{return _table_pkvalue;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string code
		{
			set{ _code=value;}
			get{return _code;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime add_time
		{
			set{ _add_time=value;}
			get{return _add_time;}
		}
		#endregion Model

	}
}

