﻿/**  版本信息模板在安装目录下，可自行修改。
* yjr_doctor_title.cs
*
* 功 能： N/A
* 类 名： yjr_doctor_title
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2020/8/5 11:06:42   N/A    初版
*
* Copyright (c) 2019  Corporation. All rights reserved.
*/
using System;
namespace DTcms.Model
{
	/// <summary>
	/// yjr_doctor_title:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class yjr_doctor_title
	{
		public yjr_doctor_title()
		{}
		#region Model
		private int _id;
		private string _titlename;
		private int _sort;
		private int _status;
		private int _siteid;
		/// <summary>
		/// 
		/// </summary>
		public int id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string titlename
		{
			set{ _titlename=value;}
			get{return _titlename;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int sort
		{
			set{ _sort=value;}
			get{return _sort;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int status
		{
			set{ _status=value;}
			get{return _status;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int siteid
		{
			set{ _siteid=value;}
			get{return _siteid;}
		}
		#endregion Model

	}
}

