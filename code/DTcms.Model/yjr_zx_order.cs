﻿using System; 
using System.Text;
using System.Collections.Generic; 
using System.Data;
namespace DTcms.Model{
	 	//yjr_zx_order
		public class yjr_zx_order
	{
   		     
      	/// <summary>
		/// id
        /// </summary>		
		private int _id;
        public int id
        {
            get{ return _id; }
            set{ _id = value; }
        }        
		/// <summary>
		/// userid
        /// </summary>		
		private int _userid;
        public int userid
        {
            get{ return _userid; }
            set{ _userid = value; }
        }        
		/// <summary>
		/// 服务类型 1：图文 2：电话 3：视频 
        /// </summary>		
		private int _service_type;
        public int service_type
        {
            get{ return _service_type; }
            set{ _service_type = value; }
        }        
		/// <summary>
		/// businessconfigid
        /// </summary>		
		private int _businessconfigid;
        public int businessconfigid
        {
            get{ return _businessconfigid; }
            set{ _businessconfigid = value; }
        }        
		/// <summary>
		/// businessappointmentid
        /// </summary>		
		private int _businessappointmentid;
        public int businessappointmentid
        {
            get{ return _businessappointmentid; }
            set{ _businessappointmentid = value; }
        }        
		/// <summary>
		/// question_desc
        /// </summary>		
		private string _question_desc;
        public string question_desc
        {
            get{ return _question_desc; }
            set{ _question_desc = value; }
        }        
		/// <summary>
		/// doctor_id
        /// </summary>		
		private int _doctor_id;
        public int doctor_id
        {
            get{ return _doctor_id; }
            set{ _doctor_id = value; }
        }        
		/// <summary>
		/// createtime
        /// </summary>		
		private DateTime _createtime;
        public DateTime createtime
        {
            get{ return _createtime; }
            set{ _createtime = value; }
        }        
		/// <summary>
		/// service_fee
        /// </summary>		
		private int _service_fee;
        public int service_fee
        {
            get{ return _service_fee; }
            set{ _service_fee = value; }
        }        
		/// <summary>
		/// pay_status
        /// </summary>		
		private int _pay_status;
        public int pay_status
        {
            get{ return _pay_status; }
            set{ _pay_status = value; }
        }        
		/// <summary>
		/// status
        /// </summary>		
		private int _status;
        public int status
        {
            get{ return _status; }
            set{ _status = value; }
        }        
		/// <summary>
		/// pay_time
        /// </summary>		
		private DateTime _pay_time;
        public DateTime pay_time
        {
            get{ return _pay_time; }
            set{ _pay_time = value; }
        }        
		/// <summary>
		/// ms_sessionid
        /// </summary>		
		private string _ms_sessionid;
        public string ms_sessionid
        {
            get{ return _ms_sessionid; }
            set{ _ms_sessionid = value; }
        }

        private string _business_name;
        public string business_name
        {
            get { return _business_name; }
            set { _business_name = value; }
        }

        private string _name;
        public string name
        {
            get { return _name; }
            set { _name = value; }
        }


      

    }
}

