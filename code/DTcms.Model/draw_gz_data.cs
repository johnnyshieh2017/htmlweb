﻿/**  版本信息模板在安装目录下，可自行修改。
* draw_gz_data.cs
*
* 功 能： N/A
* 类 名： draw_gz_data
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2020/4/2 17:27:21   N/A    初版
*
* Copyright (c) 2019  Corporation. All rights reserved.
*/
using System;
namespace DTcms.Model
{
	/// <summary>
	/// draw_gz_data:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class draw_gz_data
	{
		public draw_gz_data()
		{}
		#region Model
		private int _id;
		private string _memid;
		private string _memname;
		private string _thirdid;
		private string _memlink;
		private DateTime? _guankanshichang;
		private string _mobile;
		private string _fangwenlaiyuan;
		private decimal? _fangwencishu;
		private decimal? _fenxiangcishu;
		private decimal? _liaotiancishu;
		private DateTime? _zaixianshichang;
		private DateTime? _firsttime;
		private DateTime? _lasttime;
		private string _shebei;
		private string _guankanfangshi;
		private string _dengluip;
		private string _shifouzhigong;
		private string _sheng;
		private string _shi;
		private string _gongsi;
		private string _xingming;
		private int? _lotteryid;
		/// <summary>
		/// 
		/// </summary>
		public int id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string MemID
		{
			set{ _memid=value;}
			get{return _memid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string MemName
		{
			set{ _memname=value;}
			get{return _memname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ThirdID
		{
			set{ _thirdid=value;}
			get{return _thirdid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Memlink
		{
			set{ _memlink=value;}
			get{return _memlink;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? guankanshichang
		{
			set{ _guankanshichang=value;}
			get{return _guankanshichang;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string mobile
		{
			set{ _mobile=value;}
			get{return _mobile;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string fangwenlaiyuan
		{
			set{ _fangwenlaiyuan=value;}
			get{return _fangwenlaiyuan;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? fangwencishu
		{
			set{ _fangwencishu=value;}
			get{return _fangwencishu;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? fenxiangcishu
		{
			set{ _fenxiangcishu=value;}
			get{return _fenxiangcishu;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? liaotiancishu
		{
			set{ _liaotiancishu=value;}
			get{return _liaotiancishu;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? zaixianshichang
		{
			set{ _zaixianshichang=value;}
			get{return _zaixianshichang;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? firsttime
		{
			set{ _firsttime=value;}
			get{return _firsttime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? lasttime
		{
			set{ _lasttime=value;}
			get{return _lasttime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string shebei
		{
			set{ _shebei=value;}
			get{return _shebei;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string guankanfangshi
		{
			set{ _guankanfangshi=value;}
			get{return _guankanfangshi;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string dengluIP
		{
			set{ _dengluip=value;}
			get{return _dengluip;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string shifouzhigong
		{
			set{ _shifouzhigong=value;}
			get{return _shifouzhigong;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string sheng
		{
			set{ _sheng=value;}
			get{return _sheng;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string shi
		{
			set{ _shi=value;}
			get{return _shi;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string gongsi
		{
			set{ _gongsi=value;}
			get{return _gongsi;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string xingming
		{
			set{ _xingming=value;}
			get{return _xingming;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? lotteryid
		{
			set{ _lotteryid=value;}
			get{return _lotteryid;}
		}
		#endregion Model

	}
}

