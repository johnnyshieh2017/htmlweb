﻿using System; 
using System.Text;
using System.Collections.Generic; 
using System.Data;
namespace DTcms.Model{
	 	//View_doctor_group
		public class View_doctor_group
	{
   		     
      	/// <summary>
		/// id
        /// </summary>		
		private int _id;
        public int id
        {
            get{ return _id; }
            set{ _id = value; }
        }        
		/// <summary>
		/// groupname
        /// </summary>		
		private string _groupname;
        public string groupname
        {
            get{ return _groupname; }
            set{ _groupname = value; }
        }        
		/// <summary>
		/// sort
        /// </summary>		
		private int _sort;
        public int sort
        {
            get{ return _sort; }
            set{ _sort = value; }
        }        
		/// <summary>
		/// status
        /// </summary>		
		private int _status;
        public int status
        {
            get{ return _status; }
            set{ _status = value; }
        }        
		/// <summary>
		/// siteid
        /// </summary>		
		private int _siteid;
        public int siteid
        {
            get{ return _siteid; }
            set{ _siteid = value; }
        }        
		/// <summary>
		/// hospatilid
        /// </summary>		
		private int _hospatilid;
        public int hospatilid
        {
            get{ return _hospatilid; }
            set{ _hospatilid = value; }
        }        
		/// <summary>
		/// description
        /// </summary>		
		private string _description;
        public string description
        {
            get{ return _description; }
            set{ _description = value; }
        }        
		/// <summary>
		/// leader_desc
        /// </summary>		
		private string _leader_desc;
        public string leader_desc
        {
            get{ return _leader_desc; }
            set{ _leader_desc = value; }
        }        
		/// <summary>
		/// logo_url
        /// </summary>		
		private string _logo_url;
        public string logo_url
        {
            get{ return _logo_url; }
            set{ _logo_url = value; }
        }        
		/// <summary>
		/// hospitalname
        /// </summary>		
		private string _hospitalname;
        public string hospitalname
        {
            get{ return _hospitalname; }
            set{ _hospitalname = value; }
        }        
		   
	}
}

