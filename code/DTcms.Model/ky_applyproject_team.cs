﻿/**  版本信息模板在安装目录下，可自行修改。
* ky_applyproject_team.cs
*
* 功 能： N/A
* 类 名： ky_applyproject_team
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2021/7/27 10:34:25   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace DTcms.Model
{
	/// <summary>
	/// ky_applyproject_team:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class ky_applyproject_team
	{
		public ky_applyproject_team()
		{}
		#region Model
		private int _id;
		private string _pa_id;
		private string _name;
		private string _sex;
		private string _chusheng;
		private string _zhuanye;
		private string _zhicheng;
		private string _xueli;
		private string _danwei;
		private string _fengong;
		private DateTime _add_time;
		private string _add_user;
		private int _status;
		/// <summary>
		/// 
		/// </summary>
		public int id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string pa_id
		{
			set{ _pa_id=value;}
			get{return _pa_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string name
		{
			set{ _name=value;}
			get{return _name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string sex
		{
			set{ _sex=value;}
			get{return _sex;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string chusheng
		{
			set{ _chusheng=value;}
			get{return _chusheng;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhuanye
		{
			set{ _zhuanye=value;}
			get{return _zhuanye;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zhicheng
		{
			set{ _zhicheng=value;}
			get{return _zhicheng;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string xueli
		{
			set{ _xueli=value;}
			get{return _xueli;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string danwei
		{
			set{ _danwei=value;}
			get{return _danwei;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string fengong
		{
			set{ _fengong=value;}
			get{return _fengong;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime add_time
		{
			set{ _add_time=value;}
			get{return _add_time;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string add_user
		{
			set{ _add_user=value;}
			get{return _add_user;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int status
		{
			set{ _status=value;}
			get{return _status;}
		}
		#endregion Model

	}
}

