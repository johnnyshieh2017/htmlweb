﻿using System; 
using System.Text;
using System.Collections.Generic; 
using System.Data;
namespace DTcms.Model{
	 	//yjr_zx_messages
		public class yjr_zx_messages
	{
   		     
      	/// <summary>
		/// id
        /// </summary>		
		private int _id;
        public int id
        {
            get{ return _id; }
            set{ _id = value; }
        }        
		/// <summary>
		/// userid
        /// </summary>		
		private int _userid;
        public int userid
        {
            get{ return _userid; }
            set{ _userid = value; }
        }        
		/// <summary>
		/// sessionid
        /// </summary>		
		private string _sessionid;
        public string sessionid
        {
            get{ return _sessionid; }
            set{ _sessionid = value; }
        }        
		/// <summary>
		/// doctorid
        /// </summary>		
		private int _doctorid;
        public int doctorid
        {
            get{ return _doctorid; }
            set{ _doctorid = value; }
        }        
		/// <summary>
		/// 咨询会话序号
        /// </summary>		
		private int _zx_number;
        public int zx_number
        {
            get{ return _zx_number; }
            set{ _zx_number = value; }
        }        
		/// <summary>
		/// zx_content
        /// </summary>		
		private string _zx_content;
        public string zx_content
        {
            get{ return _zx_content; }
            set{ _zx_content = value; }
        }        
		/// <summary>
		/// createtime
        /// </summary>		
		private DateTime _createtime;
        public DateTime createtime
        {
            get{ return _createtime; }
            set{ _createtime = value; }
        }        
		/// <summary>
		/// 0：待回答 1：已回答可提问 9：已完成
        /// </summary>		
		private int _status;
        public int status
        {
            get{ return _status; }
            set{ _status = value; }
        }        
		/// <summary>
		/// visited_status
        /// </summary>		
		private int _visited_status;
        public int visited_status
        {
            get{ return _visited_status; }
            set{ _visited_status = value; }
        }        
		/// <summary>
		/// visited_hospital
        /// </summary>		
		private string _visited_hospital;
        public string visited_hospital
        {
            get{ return _visited_hospital; }
            set{ _visited_hospital = value; }
        }        
		/// <summary>
		/// visited_department
        /// </summary>		
		private string _visited_department;
        public string visited_department
        {
            get{ return _visited_department; }
            set{ _visited_department = value; }
        }        
		/// <summary>
		/// diagnosis_result
        /// </summary>		
		private string _diagnosis_result;
        public string diagnosis_result
        {
            get{ return _diagnosis_result; }
            set{ _diagnosis_result = value; }
        }

        /// <summary>
        /// reportimg
        /// </summary>		
        private string _reportimg;
        public string reportimg
        {
            get { return _reportimg; }
            set { _reportimg = value; }
        }

        /// <summary>
        /// 就诊人
        /// </summary>		
        private int _patientid;
        public int patientid
        {
            get{ return _patientid; }
            set{ _patientid = value; }
        }        
		/// <summary>
		/// owner
        /// </summary>		
		private int _owner;
        public int owner
        {
            get{ return _owner; }
            set{ _owner = value; }
        }        
		/// <summary>
		/// 0未发送 1已发送 2已读
        /// </summary>		
		private int _read_status;
        public int read_status
        {
            get{ return _read_status; }
            set{ _read_status = value; }
        }        
		   
	}
}

