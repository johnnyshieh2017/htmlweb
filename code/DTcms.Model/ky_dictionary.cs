﻿/**  版本信息模板在安装目录下，可自行修改。
* ky_dictionary.cs
*
* 功 能： N/A
* 类 名： ky_dictionary
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2021/7/15 10:35:44   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace DTcms.Model
{
	/// <summary>
	/// ky_dictionary:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class ky_dictionary
	{
		public ky_dictionary()
		{}
		#region Model
		private string _dic_value;
		private string _dic_text;
		private string _dic_group;
		private int _dic_sort=99;
		private int _dic_status=1;
		/// <summary>
		/// 
		/// </summary>
		public string dic_value
		{
			set{ _dic_value=value;}
			get{return _dic_value;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string dic_text
		{
			set{ _dic_text=value;}
			get{return _dic_text;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string dic_group
		{
			set{ _dic_group=value;}
			get{return _dic_group;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int dic_sort
		{
			set{ _dic_sort=value;}
			get{return _dic_sort;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int dic_status
		{
			set{ _dic_status=value;}
			get{return _dic_status;}
		}
		#endregion Model

	}
}

