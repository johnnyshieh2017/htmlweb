﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
namespace DTcms.Model
{
    //yjr_doctor
    public class yjr_doctor
    {

        /// <summary>
        /// id
        /// </summary>		
        private int _id;
        public int id
        {
            get { return _id; }
            set { _id = value; }
        }
        /// <summary>
        /// 姓名
        /// </summary>		
        private string _name;
        public string name
        {
            get { return _name; }
            set { _name = value; }
        }
        /// <summary>
        /// 职称id
        /// </summary>		
        private int _title;
        public int title
        {
            get { return _title; }
            set { _title = value; }
        }
        /// <summary>
        /// 性别
        /// </summary>		
        private string _gender;
        public string gender
        {
            get { return _gender; }
            set { _gender = value; }
        }
        /// <summary>
        /// 医生组别id
        /// </summary>		
        private int _groupid;
        public int groupid
        {
            get { return _groupid; }
            set { _groupid = value; }
        }
        /// <summary>
        /// intro
        /// </summary>		
        private string _intro;
        public string intro
        {
            get { return _intro; }
            set { _intro = value; }
        }
        /// <summary>
        /// speciality
        /// </summary>		
        private string _speciality;
        public string speciality
        {
            get { return _speciality; }
            set { _speciality = value; }
        }
        /// <summary>
        /// logoimg
        /// </summary>		
        private string _logoimg;
        public string logoimg
        {
            get { return _logoimg; }
            set { _logoimg = value; }
        }
        /// <summary>
        /// hospitalid
        /// </summary>		
        private int _hospitalid;
        public int hospitalid
        {
            get { return _hospitalid; }
            set { _hospitalid = value; }
        }
        /// <summary>
        /// 科室id
        /// </summary>		
        private int _departmentid;
        public int departmentid
        {
            get { return _departmentid; }
            set { _departmentid = value; }
        }
        /// <summary>
        /// department_name
        /// </summary>		
        private string _department_name;
        public string department_name
        {
            get { return _department_name; }
            set { _department_name = value; }
        }
        /// <summary>
        /// createtime
        /// </summary>		
        private DateTime _createtime;
        public DateTime createtime
        {
            get { return _createtime; }
            set { _createtime = value; }
        }
        /// <summary>
        /// 状态
        /// </summary>		
        private int _status;
        public int status
        {
            get { return _status; }
            set { _status = value; }
        }
        /// <summary>
        /// 逻辑删除
        /// </summary>		
        private int _isdelete;
        public int isdelete
        {
            get { return _isdelete; }
            set { _isdelete = value; }
        }
        /// <summary>
        /// sort
        /// </summary>		
        private int _sort;
        public int sort
        {
            get { return _sort; }
            set { _sort = value; }
        }
        /// <summary>
        /// siteid
        /// </summary>		
        private int _siteid;
        public int siteid
        {
            get { return _siteid; }
            set { _siteid = value; }
        }
        /// <summary>
        /// mobile
        /// </summary>		
        private string _mobile;
        public string mobile
        {
            get { return _mobile; }
            set { _mobile = value; }
        }
        /// <summary>
        /// idcard
        /// </summary>		
        private string _idcard;
        public string idcard
        {
            get { return _idcard; }
            set { _idcard = value; }
        }
        /// <summary>
        /// idcardpic_front
        /// </summary>		
        private string _idcardpic_front;
        public string idcardpic_front
        {
            get { return _idcardpic_front; }
            set { _idcardpic_front = value; }
        }
        /// <summary>
        /// idcardpic_back
        /// </summary>		
        private string _idcardpic_back;
        public string idcardpic_back
        {
            get { return _idcardpic_back; }
            set { _idcardpic_back = value; }
        }
        /// <summary>
        /// doc_license
        /// </summary>		
        private string _doc_license;
        public string doc_license
        {
            get { return _doc_license; }
            set { _doc_license = value; }
        }
        /// <summary>
        /// doc_qualification
        /// </summary>		
        private string _doc_qualification;
        public string doc_qualification
        {
            get { return _doc_qualification; }
            set { _doc_qualification = value; }
        }
        /// <summary>
        /// doc_titlepic
        /// </summary>		
        private string _doc_titlepic;
        public string doc_titlepic
        {
            get { return _doc_titlepic; }
            set { _doc_titlepic = value; }
        }
        /// <summary>
        /// position_id
        /// </summary>		
        private int _position_id;
        public int position_id
        {
            get { return _position_id; }
            set { _position_id = value; }
        }


        public string hospitalname
        {
            get; set;
        }

        public string hospitallevel
        {
            get; set;
        }

        public string titlename
        {
            get; set;
        }

        public string rate
        {
            get;set;
        }
        public string bookcount
        {
            get; set;
        }
        public string responsetime
        {
            get; set;
        }


    }
}

