﻿/**  版本信息模板在安装目录下，可自行修改。
* ky_applyfee.cs
*
* 功 能： N/A
* 类 名： ky_applyfee
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2021-07-18 16:55:12   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace DTcms.Model
{
	/// <summary>
	/// ky_applyfee:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class ky_applyfee
	{
		public ky_applyfee()
		{}
		#region Model
		private int _applyfee_id;
		private int _p_id;
		private string _fee;
		private string _reason;
		private DateTime _add_time;
		private string _add_user;
		private int _status;
		private string _audit_1_user;
		private string _audit_2_user;
		private string _audit_3_user;
		private string _audit_4_user;
		private DateTime? _audit_1_time;
		private DateTime? _audit_2_time;
		private DateTime? _audit_3_time;
		private DateTime? _audit_4_time;
		private int _audit_1_status;
		private int _audit_2_status;
		private int _audit_3_status;
		private int _audit_4_status;
		private string _fee_type;
		/// <summary>
		/// 
		/// </summary>
		public int applyfee_id
		{
			set{ _applyfee_id=value;}
			get{return _applyfee_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int p_id
		{
			set{ _p_id=value;}
			get{return _p_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string fee
		{
			set{ _fee=value;}
			get{return _fee;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string reason
		{
			set{ _reason=value;}
			get{return _reason;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime add_time
		{
			set{ _add_time=value;}
			get{return _add_time;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string add_user
		{
			set{ _add_user=value;}
			get{return _add_user;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int status
		{
			set{ _status=value;}
			get{return _status;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string audit_1_user
		{
			set{ _audit_1_user=value;}
			get{return _audit_1_user;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string audit_2_user
		{
			set{ _audit_2_user=value;}
			get{return _audit_2_user;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string audit_3_user
		{
			set{ _audit_3_user=value;}
			get{return _audit_3_user;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string audit_4_user
		{
			set{ _audit_4_user=value;}
			get{return _audit_4_user;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? audit_1_time
		{
			set{ _audit_1_time=value;}
			get{return _audit_1_time;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? audit_2_time
		{
			set{ _audit_2_time=value;}
			get{return _audit_2_time;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? audit_3_time
		{
			set{ _audit_3_time=value;}
			get{return _audit_3_time;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? audit_4_time
		{
			set{ _audit_4_time=value;}
			get{return _audit_4_time;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int audit_1_status
		{
			set{ _audit_1_status=value;}
			get{return _audit_1_status;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int audit_2_status
		{
			set{ _audit_2_status=value;}
			get{return _audit_2_status;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int audit_3_status
		{
			set{ _audit_3_status=value;}
			get{return _audit_3_status;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int audit_4_status
		{
			set{ _audit_4_status=value;}
			get{return _audit_4_status;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string fee_type
		{
			set{ _fee_type=value;}
			get{return _fee_type;}
		}
		#endregion Model

	}
}

