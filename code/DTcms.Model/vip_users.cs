﻿/**  版本信息模板在安装目录下，可自行修改。
* vip_users.cs
*
* 功 能： N/A
* 类 名： vip_users
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2020/12/21 10:20:23   N/A    初版
*
* Copyright (c) 2019  Corporation. All rights reserved.
*/
using System;
namespace DTcms.Model
{
	/// <summary>
	/// vip_users:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class vip_users
	{
		public vip_users()
		{}
		#region Model
		private int _id;
		private string _user_truename;
		private string _user_phone;
		private string _user_wx_openid;
		private DateTime _add_time;
		private int _status=0;
		private int _user_type;
		private string _user_email;
		/// <summary>
		/// 
		/// </summary>
		public int id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string user_truename
		{
			set{ _user_truename=value;}
			get{return _user_truename;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string user_phone
		{
			set{ _user_phone=value;}
			get{return _user_phone;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string user_wx_openid
		{
			set{ _user_wx_openid=value;}
			get{return _user_wx_openid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime add_time
		{
			set{ _add_time=value;}
			get{return _add_time;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int status
		{
			set{ _status=value;}
			get{return _status;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int user_type
		{
			set{ _user_type=value;}
			get{return _user_type;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string user_email
		{
			set{ _user_email=value;}
			get{return _user_email;}
		}
		#endregion Model

	}
}

