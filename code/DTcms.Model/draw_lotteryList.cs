﻿/**  版本信息模板在安装目录下，可自行修改。
* draw_lotteryList.cs
*
* 功 能： N/A
* 类 名： draw_lotteryList
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2020/4/1 15:19:15   N/A    初版
*
* Copyright (c) 2019  Corporation. All rights reserved.
*/
using System;
namespace DTcms.Model
{
	/// <summary>
	/// draw_lotteryList:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class draw_lotteryList
	{
		public draw_lotteryList()
		{}
		#region Model
		private int _id;
		private string _name;
		private DateTime _begin_date;
		private DateTime _end_date;
		private string _description;
		private int _draw_count=1;
		private int _status;
		private string _default_msg;
		private DateTime _createtime;
		private string _remark;
		/// <summary>
		/// 
		/// </summary>
		public int id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 活动名称
		/// </summary>
		public string name
		{
			set{ _name=value;}
			get{return _name;}
		}
		/// <summary>
		/// 开始时间
		/// </summary>
		public DateTime begin_date
		{
			set{ _begin_date=value;}
			get{return _begin_date;}
		}
		/// <summary>
		/// 结束时间
		/// </summary>
		public DateTime end_date
		{
			set{ _end_date=value;}
			get{return _end_date;}
		}
		/// <summary>
		/// 说明
		/// </summary>
		public string description
		{
			set{ _description=value;}
			get{return _description;}
		}
		/// <summary>
		/// 抽奖次数
		/// </summary>
		public int draw_count
		{
			set{ _draw_count=value;}
			get{return _draw_count;}
		}
		/// <summary>
		/// 状态 0：未发布 1：已发布 2：已结束 -1：暂停
		/// </summary>
		public int status
		{
			set{ _status=value;}
			get{return _status;}
		}
		/// <summary>
		/// 未中奖提示
		/// </summary>
		public string default_msg
		{
			set{ _default_msg=value;}
			get{return _default_msg;}
		}
		/// <summary>
		/// 创建日期
		/// </summary>
		public DateTime createtime
		{
			set{ _createtime=value;}
			get{return _createtime;}
		}
		/// <summary>
		/// 备注
		/// </summary>
		public string remark
		{
			set{ _remark=value;}
			get{return _remark;}
		}
		#endregion Model

	}
}

