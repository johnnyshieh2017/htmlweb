﻿/**  版本信息模板在安装目录下，可自行修改。
* ky_applyproject_shebei.cs
*
* 功 能： N/A
* 类 名： ky_applyproject_shebei
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2021/7/27 10:34:25   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace DTcms.Model
{
	/// <summary>
	/// ky_applyproject_shebei:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class ky_applyproject_shebei
	{
		public ky_applyproject_shebei()
		{}
		#region Model
		private int _id;
		private string _pa_id;
		private string _shebei;
		private string _xinghao;
		private string _jiage;
		private string _tianjia_liyou;
		private DateTime _add_time;
		private string _add_user;
		private int _status;
		/// <summary>
		/// 
		/// </summary>
		public int id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string pa_id
		{
			set{ _pa_id=value;}
			get{return _pa_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string shebei
		{
			set{ _shebei=value;}
			get{return _shebei;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string xinghao
		{
			set{ _xinghao=value;}
			get{return _xinghao;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string jiage
		{
			set{ _jiage=value;}
			get{return _jiage;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string tianjia_liyou
		{
			set{ _tianjia_liyou=value;}
			get{return _tianjia_liyou;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime add_time
		{
			set{ _add_time=value;}
			get{return _add_time;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string add_user
		{
			set{ _add_user=value;}
			get{return _add_user;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int status
		{
			set{ _status=value;}
			get{return _status;}
		}
		#endregion Model

	}
}

