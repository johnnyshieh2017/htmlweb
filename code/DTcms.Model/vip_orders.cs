﻿/**  版本信息模板在安装目录下，可自行修改。
* vip_orders.cs
*
* 功 能： N/A
* 类 名： vip_orders
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2020/12/21 15:37:28   N/A    初版
*
* Copyright (c) 2019  Corporation. All rights reserved.
*/
using System;
namespace DTcms.Model
{
	/// <summary>
	/// vip_orders:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class vip_orders
	{
		public vip_orders()
		{}
		#region Model
		private int _id;
		private string _source;
		private int _service_typeid;
		private string _customer;
		private string _telephone;
		private string _gender;
		private string _contact_name;
		private string _contact_telephone;
		private string _customer_manager;
		private string _customer_telephone;
		private DateTime? _book_date;
		private string _clinic_department;
		private string _clinic_doctor;
		private DateTime _clinic_date;
		private string _clinic_diagnosis;
		private int _servicer_id;
		private DateTime? _service_begin;
		private DateTime? _service_end;
		private int _status;
		private int _pay_status;
		private string _remark;
		private DateTime _add_time;
		private DateTime? _return_visit_date;
		private int _parent_id=0;
		private string _order_number;
		private int _user_id;
		private DateTime? _clinic_begin_notic;
		private DateTime? _clinic_end_notic;
        private string _clinic_confirm;
        private string _verifycode;
        /// <summary>
        /// 
        /// </summary>
        public int id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string source
		{
			set{ _source=value;}
			get{return _source;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int service_typeid
		{
			set{ _service_typeid=value;}
			get{return _service_typeid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string customer
		{
			set{ _customer=value;}
			get{return _customer;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string telephone
		{
			set{ _telephone=value;}
			get{return _telephone;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string gender
		{
			set{ _gender=value;}
			get{return _gender;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string contact_name
		{
			set{ _contact_name=value;}
			get{return _contact_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string contact_telephone
		{
			set{ _contact_telephone=value;}
			get{return _contact_telephone;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string customer_manager
		{
			set{ _customer_manager=value;}
			get{return _customer_manager;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string customer_telephone
		{
			set{ _customer_telephone=value;}
			get{return _customer_telephone;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? book_date
		{
			set{ _book_date=value;}
			get{return _book_date;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string clinic_department
		{
			set{ _clinic_department=value;}
			get{return _clinic_department;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string clinic_doctor
		{
			set{ _clinic_doctor=value;}
			get{return _clinic_doctor;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime clinic_date
		{
			set{ _clinic_date=value;}
			get{return _clinic_date;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string clinic_diagnosis
		{
			set{ _clinic_diagnosis=value;}
			get{return _clinic_diagnosis;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int servicer_id
		{
			set{ _servicer_id=value;}
			get{return _servicer_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? service_begin
		{
			set{ _service_begin=value;}
			get{return _service_begin;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? service_end
		{
			set{ _service_end=value;}
			get{return _service_end;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int status
		{
			set{ _status=value;}
			get{return _status;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int pay_status
		{
			set{ _pay_status=value;}
			get{return _pay_status;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string remark
		{
			set{ _remark=value;}
			get{return _remark;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime add_time
		{
			set{ _add_time=value;}
			get{return _add_time;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? return_visit_date
		{
			set{ _return_visit_date=value;}
			get{return _return_visit_date;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int parent_id
		{
			set{ _parent_id=value;}
			get{return _parent_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string order_number
		{
			set{ _order_number=value;}
			get{return _order_number;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int user_id
		{
			set{ _user_id=value;}
			get{return _user_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? clinic_begin_notic
		{
			set{ _clinic_begin_notic=value;}
			get{return _clinic_begin_notic;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? clinic_end_notic
		{
			set{ _clinic_end_notic=value;}
			get{return _clinic_end_notic;}
		}


        /// <summary>
		/// 
		/// </summary>
		public string clinic_confirm
        {
            set { _clinic_confirm = value; }
            get { return _clinic_confirm; }
        }

        /// <summary>
		/// 验证码
		/// </summary>
		public string verifycode
        {
            set { _verifycode = value; }
            get { return _verifycode; }
        }
        #endregion Model

    }
}

